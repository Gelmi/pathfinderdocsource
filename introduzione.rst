
.. _intro:

=================================================
Introduzione
=================================================

.. contents:: Contenuti

Creare un Personaggio
======================

La prima cosa che dovresti fare prima di creare un personaggio è pensare a cosa vorresti essere: un combattente fiero che si lancia nella mischia? Agire nell'ombra e cogliere di sorpresa il vostro nemico? Evocare antiche forze e mostri per combattere al vostro fianco?

.. figure:: Immagini/Intro1.png
    :align: right
    :width: 400

Di conseguenza fatevi un'idea di che classe potrebbe interessarvi, date un occhio a quali statistiche potrebbero essegli utili e procedete con la creazione del personaggio, annotando le informazioni che raccogliete nella :ref:`Scheda Personaggio<scheda_personaggio>`. Il mio consiglio è quello di utilizzare una scheda il più completa possibile, come il :ref:`Player's Booklet<player_booklet>`, in modo da poter ridurre al minimo il numero di volte che dovrai infilare il naso nel manuale per trovare le informazioni che ti servono.

.. contents:: Riassumendo:
	:local:

------------------------------------------
I - Determinare le Caratteristiche
------------------------------------------
Si inizia determinando i sei :ref:`punteggi caratteristica<caratteristiche>` del personaggio che ne stabiliscono gli attributi di base più importanti e sono usati per
decidere altri dettagli e statistiche. Alcune selezioni di classe richiedono che un personaggio abbia dei punteggi di caratteristica superiori alla media.

Anche se raramente un personaggio effettua una prova usando soltanto una sua caratteristica, le caratteristiche, ed i modificatori associati, influiscono praticamente su ogni aspetto delle capacità e delle abilità del personaggio. 

Ogni punteggio di caratteristica in genere va da 3 a 18, anche se i bonus e le penalità razziali possano alterarli un punteggio medio è 10.

Generare i Punteggi
--------------------
Ci sono diversi metodi per generare i punteggi caratteristica, ciascuno con un livello
differente di flessibilità e casualità alla creazione del personaggio. 

Una volta generati i punteggi si possono aggiustare con i modificatori razziali, bonus o malus dati dalla razza del personaggio.

.. rubric:: Standard

Tirare 4d6 per scartarne il risultato più basso e sommare gli altri tre assieme.
Ripetere fino a generare sei numeri che possono essere assegnati ai punteggi caratteristica liberamente. 

Questo metodo è meno casuale di quello classico e contribuisce a creare personaggi con punteggi di caratteristica superiori alla media.

.. rubric:: Classico

Tirare 3d6 per sommarne i risultati e ripetere il procedimento fino ad ottenere sei numeri. Assegnare questi risultati ai punteggi caratteristica liberamente. 

Questo metodo è abbastanza casuale ed alcuni personaggi avranno caratteristiche chiaramente superiori. 

Questa casualità può andare ancora oltre, con i totali assegnati ai punteggi di caratteristica specifici nell’ordine in cui sono tirati. I personaggi generati con questo metodo sono difficili da conformarsi a concetti predeterminati, poiché i loro punteggi potrebbero non essere sufficienti per alcune classi o personalità, e dovrebbero essere ideati in base ai punteggi di caratteristica.

.. rubric:: Eroico

Tiraee 2d6 e aggiungere 6 alla somma dei dadi. Ripetere il procedimento fino a generare sei numeri e assegnarli liberamente ai punteggi di caratteristica. 

Questo metodo è meno casuale di quello standard e genera personaggi con punteggi decisamente superiori alla media.

.. rubric:: Riserva di dadi

Ogni personaggio ha una riserva di 24d6 da assegnare alle sue caratteristiche. 

Prima di tirare i dadi, il giocatore seleziona il numero di dadi da abbinare ad ogni punteggio, con un minimo di 3d6 per ogni caratteristica. Una volta assegnati i dadi, il giocatore tira i dadi per ogni gruppo e somma i totali dei **tre risultati migliori**. 

Se si prediligono personaggi più potenti, il GM dovrebbe aumentare il numero totale dei dadi a 28. 

Questo metodo crea personaggi simili a quelli del metodo standard.

.. rubric:: Acquisto

Ogni personaggio riceve un certo numero di punti da spendere per aumentare le sue caratteristiche di base. 

.. figure:: Immagini/CostiCaratteristiche.png
	:align: right
	:width: 300

	Costo delle caratteristiche per il metodo dell'**acquisto**.

In questo metodo, tutte le caratteristiche partono da una base di 10. Un personaggio può aumentare un punteggio determinato spendendo alcuni dei suoi punti. Inoltre, può guadagnare più punti da spendere su altri punteggi diminuendone altri. I prezzi sono riportati in tabella.

Nessun punteggio può essere diminuito a meno di 7 o aumentato oltre a 18 usando questo metodo. 

.. figure:: Immagini/PuntiCaratteristica.png
    :align: right
    :width: 300

    Punti spendibili assegnati per il metodo dell'**acquisto**.

Il numero di punti che dovete spendere usando questo metodo dipende dal tipo di campagna che state giocando, come riportato in tabella, tenendo presente che i personaggi non giocanti medi (PNG) sono creati usando soltanto 3 punti. 

Questo metodo dà risalto alla scelta del giocatore e crea personaggi ben bilanciati ed è il sistema utilizzato per gli eventi di gioco organizzato, come la *Pathfinder Society*.

------------------------
II - Scegliere la Razza
------------------------

Il secondo passo è scegliere la razza del personaggio, prendendo nota dei modificatori razziali alle caratteristiche e degli altri tratti razziali, come riportato nel :ref:`Capitolo sulle Razze<razze>`.

Determinare i Modificatori
--------------------------

.. _incantesimi_bonus:

.. figure:: Immagini/ModificatoriCaratteristica.png
	
	Modificatori caratteristica e incantesimi bonus.

Ogni caratteristica, dopo aver applicato le modifiche per la razza, avrà un modificatore variabile da –5 a +5. 

La :ref:`Tabella<incantesimi_bonus>` mostra il modificatore per ogni punteggio di caratteristica, cioè il numero che deve essere applicato al tiro di dado quando il personaggio tenta di compiere qualcosa di strettamente legato a quella caratteristica. Il modificatore a volte si applica anche ad alcuni valori che non sono tiri di dado. 

Caratteristiche e Incantatori
------------------------------
Nella :ref:`Tabella<incantesimi_bonus>` sono anche riportati gli incantesimi bonus che il personaggio conosce se è un incantatore.

La caratteristica chiave degli incantesimi bonus dipende da che tipo di incantatore è il personaggio: Intelligenza per :ref:`Maghi<mago>`; Saggezza per :ref:`Chierici<chierico>`, :ref:`Druidi<druido>` e :ref:`Ranger<ranger>`; Carisma per :ref:`Bardi<bardo>`, :ref:`Paladini<paladino>` e :ref:`Stregoni<stregone>`. 

Oltre ad avere un alto punteggio di caratteristica, l’incantatore deve essere di un
livello di classe sufficiente per poter lanciare incantesimi bonus di un certo livello, come descritto nell':ref:`apposito capitolo<classi>`.

Scegliere i Linguaggi
----------------------
Ogni razza elenca dei linguaggi che il personaggio conosce automaticamente, così
come alcuni linguaggi bonus. 

Un personaggio conosce un numero di linguaggi bonus addizionali pari al proprio
modificatore di Intelligenza.

---------------------------
III - Scegliere la Classe
---------------------------
La classe del personaggio rappresenta una professione, come il :ref:`Guerriero<guerriero>` o il :ref:`Mago<mago>`.

Un nuovo personaggio, comincia al 1° livello nella classe scelta e mentre guadagna punti di esperienza (PE), per esempio sconfiggendo dei mostri, avanza di livello e acquisisce nuovi poteri e capacità.

Si può trovare una descrizione completa di ciascuna classe nell':ref:`apposito capitolo<classi>`.

--------------------------------
IV - Scegliere abilità e talenti
--------------------------------

Determinate il numero dei gradi di abilità posseduti dal personaggio, in base
alla sua classe e al Mod Int (e a qualsiasi altro bonus, come quello degli umani). 

Poi spendete questi gradi nelle abilità, senza superare i gradi del proprio livello (ad esempio, massimo 1 grado per un personaggio di 1° livello). 

Concluse le abilità, determinate quanti talenti il personaggio riceve tenendo conto che:

*	Tutti i personaggi iniziano con 1 Talento.
*	Se si è :ref:`Umani<umani>` si ottiene 1 Talento extra.
*	Se si è :ref:`Guerrieri<guerriero>` si ottiene 1 Talento extra.

Potete scegliere i talenti del vostro personaggio dalla :ref:`sezione dedicata<talenti>`.

-------------------------------
V - Equipaggiamento
-------------------------------
Ogni nuovo personaggio comincia il gioco con una quantità di monete d’oro, in base alla classe, che può essere spesa per acquistare il proprio equipaggiamento d’avventura, dall’armatura allo zaino. 

Questo equipaggiamento aiuta il personaggio a sopravvivere durante le avventure. In linea generale, non si può usare il denaro iniziale per acquistare oggetti magici
senza il consenso del GM.

Tutte le informazioni relative all'equipaggiamento è contenuto nell':ref:`apposito capitolo<equipaggiamento>`, compresa la quantità di oro di partenza di ciascun personaggio.

-----------------------------------
VI - Dettagli Finali
-----------------------------------
Per concludere, bisogna determinare tutti i particolari del personaggio:

*	Punti Ferita iniziali (pf). Al primo livello si ottiene il valore massimo del proprio dato vita + Mod Cos (quando si sale di livello si deve tirare il dado, sommare il Mod Cos ed eventualmente aggiungere un pf se si sceglie la :ref:`Classe Preferita<classe_preferita>`).
*	Classe Armatura (CA). 10 + Armatura + Scudo + Mod Des + Altri modificatori
*	Tiri Salvezza. Dipendenti dalla :ref:`classe<classi>` scelta.
*	Iniziativa. Generalmente uguale al Mod Des, ma può essere influenzato da diversi talenti.
*	Attacco. Melee = Bonus Attacco Base (che dipende dalla :ref:`classe<classi>`) + Mod For + Mod Taglia. Ranged = Bonus Attacco Base + Mod Des + Mod Taglia + Penalità gittata.
*	Incantesimi. Vedere la descrizione della propria :ref:`classe<classi>` in proposito.

.. Note::
	Tutti questi numeri sono determinati dalle decisioni prese nei passi precedenti.

Oltr a ciò, dovete decidere il nome del personaggio, l'allineamento e l’aspetto fisico, magari annotando anche alcune peculiarità del carattere e della personalità, che vi serviranno da linee guida per interpretare il personaggio durante il gioco.

:ref:`Regole aggiuntive<regole_aggiuntive>` possono essere utili per definire l'influenza di alcuni particolari quali età, allineamento, ecc...

.. _caratteristiche:

Le Caratteristiche
=====================

Le caratteristiche descrivono parzialmente il giocatore e ne influenzano le azioni.

.. _forza:

---------------------
Forza (For)
---------------------
La Forza misura la prestanza e la potenza fisica del personaggio. 

Importante per :ref:`Barbari<barbaro>`, :ref:`Guerrieri<guerriero>`, :ref:`Monaci<monaco>`, :ref:`Paladini<paladino>` e :ref:`Ranger<ranger>` aiutandoli nei combattimenti in mischia. 

.. figure:: Immagini/Strength.png
    :align: right
    :width: 300

La Forza determina la quantità di equipaggiamento che il personaggio può portare. Un personaggio con **Forza 0** è troppo debole per muoversi in alcun modo ed è privo di sensi. 

Alcune creature non hanno un punteggio di Forza che significa che il loro modificatore è +0 per qualsiasi prova di Forza o di abilità basate su di essa.

Il modificatore di Forza del personaggio si applica a:

• Tiri per colpire in mischia.
• Tiri per i danni delle armi in mischia e da lancio, incluse le fionde.
• Prove di :ref:`Nuotare<nuotare>` e :ref:`Scalare<scalare>`.
• Prove di Forza (per sfondare porte e simili).

.. Note::

	Si applicano le seguenti eccezioni: gli attacchi effettuati con la mano secondaria ricevono solo la metà del bonus di Forza, e quelli a due mani una volta e mezzo. Le penalità dovute alla Forza, e non i bonus, si applicano agli attacchi effettuati con archi che non siano compositi).

---------------------
Destrezza (Des)
---------------------
La Destrezza misura la coordinazione, l’agilità, i riflessi e l’equilibrio del personaggio. 

Caratteristica importante per :ref:`Ladri<ladro>`, e personaggi che solitamente indossano armature leggere o medie o che non ne indossano nessuna, nonché quelli che vogliono diventare esperti con le armi a distanza. 

Un personaggio con **Destrezza 0**  è incapace di muoversi ed è effettivamente immobile (ma non privo di sensi).

.. figure:: Immagini/Agility.png
    :align: left
    :width: 300

Il modificatore di Destrezza si applica a:

• Tiri per colpire a distanza, come con archi, balestre, asce da lancio e altre armi a distanza, così come molti incantesimi d’attacco a distanza come :ref:`Raggio Rovente<inc_raggio_rovente>` o :ref:`Luce Incandescente<inc_luce_incandescente>`.
• Classe Armatura (CA), se è possibile ragire all’attacco. 
• Tiri salvezza su Riflessi per evitare palle di fuoco e altri attacchi che possono essere schivati muovendosi velocemente.
• Prove di :ref:`Acrobazia<acrobazia>`, :ref:`Artista della Fuga<artista_della_fuga>`, :ref:`Cavalcare<cavalcare>`, :ref:`Disattivare Congegni<disattivare_congegni>`, :ref:`Furtività<furtività>`, :ref:`Rapidità di Mano<rapidità_di_mano>` e :ref:`Volare<volare>`.

-------------------
Costituzione (Cos)
-------------------
La Costituzione rappresenta la salute e la resistenza del personaggio. 

Un bonus di Costituzione incrementa i punti ferita di un personaggio e per questo è importante per **qualsiasi classe**. 

Un personaggio con **Costituzione 0** è morto.

Alcune creature non hanno un punteggio di Costituzione quindi hanno modificatore +0 per ogni prova basata su di essa.

.. figure:: Immagini/Endurance.png
    :align: right
    :width: 300

Il modificatore di Costituzione si applica a:

• Ogni tiro di Dado Vita.
• Tiri salvezza su Tempra per resistere a veleni, malattie e simili minacce.

.. Note::

	Una penalità non può mai far scendere il risultato di un tiro per i Dadi Vita al di sotto di 1, il che significa che un personaggio guadagna sempre almeno 1 punto ferita ogni volta che avanza di livello.

Se la Costituzione di un personaggio cambia abbastanza da alterare il modificatore di Costituzione, anche i suoi punti ferita aumentano o diminuiscono di conseguenza.

-----------------------------
Intelligenza (Int)
-----------------------------
L’Intelligenza determina la capacità di apprendimento e di ragionamento del personaggio. 

L’Intelligenza è importante per i :ref:`Maghi<mago>` poiché influisce sui loro incantesimi in molti modi. 

Le creature che hanno un istinto animale hanno un punteggio di Intelligenza di 1 o 2. Qualsiasi creatura in grado di capire qualcuno che parla ha un punteggio di almeno 3. Un personaggio con **Intelligenza 0** è in stato di coma. 

Alcune creature non hanno un punteggio di Intelligenza. Il loro modificatore è +0 per qualsiasi prova di Intelligenza o di abilità basata su di essa.

.. figure:: Immagini/Intelligence.png
    :align: left
    :width: 300

Il modificatore di Intelligenza si applica a:

• Il numero di linguaggi che il personaggio conosce all’inizio del gioco, in aggiunta a qualsiasi altro linguaggio razziale di partenza e al Comune. Se si ha una penalità, si è sempre in grado di leggere e parlare i propri linguaggi razziali a meno che il proprio punteggio di Intelligenza non sia inferiore a 3.
• Il numero di gradi di abilità acquisiti ad ogni livello. Comunque il personaggio ottiene sempre almeno 1 grado di abilità per livello.
• Prove di :ref:`Artigianato<artigianato>`, :ref:`Conoscenze<conoscenze>`, :ref:`Linguistica<linguistica>`, :ref:`Sapienza Magica<sapienza_magica>` e :ref:`Valutare<valutare>`.

.. Note::
	I :ref:`Maghi<mago>` guadagnano incantesimi bonus in base all'Intelligenza e il punteggio minimo per lanciare un incantesimo da mago è 10 + Liv Incantesimo.

---------------
Saggezza (Sag)
---------------
La Saggezza indica la forza di volontà, il buon senso, la perspicacia e l’intuito del personaggio. 

È la caratteristica più importante per :ref:`Chierici<chierico>` e :ref:`Druidi<druido>`, ma è rilevante anche per :ref:`Paladini<paladino>` e :ref:`Ranger<ranger>`. 

Se si desidera che il proprio personaggio sia sempre attento a ciò che gli accade attorno, è necessario assegnargli un alto punteggio di Saggezza. Ogni creatura possiede un punteggio di Saggezza. 

Un personaggio con **Saggezza 0** è incapace di pensiero razionale ed è privo di sensi.

.. figure:: Immagini/Wisdom.png
    :align: right
    :width: 200

Il modificatore di Saggezza si applica a:

• Tiri salvezza su Volontà (per annullare gli effetti di charme su persone e altri incantesimi).
• Prove di :ref:`Guarire<guarire>`, :ref:`Intuizione<intuizione>`, :ref:`Percezione<percezione>`, :ref:`Professione<professione>` e :ref:`Sopravvivenza<sopravvivenza>`.

.. Note::
	:ref:`Chierici<chierico>`, :ref:`Druidi<druido>` e :ref:`Ranger<ranger>` acquisiscono incantesimi bonus in base alla Saggezza e il minimo per lanciare un loro incantesimo è 10 + Liv Incantesimo.

--------------
Carisma (Car)
--------------
Il Carisma misura la forza della personalità, la capacità di persuasione, il magnetismo personale, la predisposizione al comando e il fascino di un personaggio. 

È molto importante per :ref:`Paladini<paladino>`, :ref:`Stregoni<stregone>` e :ref:`Bardi<bardo>`, ma anche per i :ref:`Chierici<chierico>` poiché influisce sulla loro capacità di incanalare energia. 

Per le creature non morte il Carisma è la misura della loro innaturale “forza vitale”. Ogni creatura possiede un punteggio di Carisma. 

Un personaggio con **Carisma 0** non è in grado di sforzarsi in alcun modo ed è privo di sensi.

.. figure:: Immagini/Charisma.png
    :align: left
    :width: 300

Il modificatore di Carisma si applica a:

• Prove di :ref:`Addestrare Animali<addestrare_animali>`, :ref:`Camuffare<camuffare>`, :ref:`Diplomazia<diplomazia>`, :ref:`Intimidire<intimidire>`, :ref:`Intrattenere<intrattenere>`, :ref:`Raggirare<raggirare>` e :ref:`Utilizzare Congegni Magici<utilizzare_congegni_magici>`.
• Prove che rappresentano il tentativo di influenzare gli altri.
• CD di incanalare energia per :ref:`Chierici<chierico>` e :ref:`Paladini<paladino>` che tentano di ferire avversari non morti.

.. Note::
	:ref:`Bardi<bardo>`, :ref:`Paladini<paladino>` e :ref:`Stregoni<stregone>` acquisiscono incantesimi bonus in base al Carisma e il minimo per lanciare un loro incantesimo è 10 + Liv Incantesimo