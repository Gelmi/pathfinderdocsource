
.. _atlante_intro:

=================================================
Introduzione
=================================================

.. sidebar:: Nota alla "edizione"

    Tenete presente, leggendo, che i contenuti sono tradotti dalla *Inner Sea World Guide* e non di tutti i termini e/o nomi conosco la traduzione "ufficiale". Laddove in dubbio, ho preferito non tradurre ma tenere il nome originale.

.. contents:: Contenuti

Solo pochi secoli fa, il dio dell'umanità mori. 

Il suo nome era Aroden, e non solo solo aveva sollevato l'uomo dalle cenere dell'Era Oscura (che seguì il cataclisma meteorico conosciuto come *EarthFall*), ma fondò anche la più grande città del mondo: Absalom.

Sconfisse il malefico re-mago *Tar-Baphon* e scacciò i signori dei demoni delle Locuste della nazione di Sarkoris. 

Egli infine lascio il mondo per unirsi alle divinità dopo aver posizionato l'umanità sulla strada di un grande desstino. Le profezie dicevano che quando l'umanità fosse stata pronta per ritornare ad ascendere al pinnacolo che presidiavano negli antichi tempi degli Azlanti, Aroden sarebbe ritornato nel mondo per accompagnarla in una nuova Era di Gloria. Ma invece che ritornare al momento prestabilito, Aroden, il dio degli umani, morì.

La morte del dio degli umani segnò l'inizio di una nuova era. Laddove le precedenti erano nominate in modo da infondere e ispirare lo spirito (l'Era del Destino, l'Era delle Incoronazioni), questa non è un periodo di fertilità, è l'Era degli Uomini Persi, poichè se non può un dio compiere la sua profezia, che probabilità hanno gli altri?

La morte di Aroden ha marchiato il mondo con tempeste e pazzia. Al Nord, il mondo si è spezzato e le armate infette degli Abissi si riversano nel mondo attraverso uno squarcio nella realtà conosciuto al giorno d'oggi come *Worldwound*. 

Al Sud, l'idilliaco golfo di Abendego fu consumato dall'uragano perpetuo i cui venti e onde hanno annegato intere nazioni. 

E nel cuore della regione del Mare Interno, dove fu profetizzato il ritorno di Aroden, la guerra civile scoppiò e a migliaia morirono prima che la diabolica casa di Thrune riuscisse a conquistare il potere.

.. figure:: Immagini/Atlante/AtlanteIntro1.png

L'Era degli Uomoni Persi è ora entrata nel suo secondo secolo, e nelle undici decadi dalla morte di Aroden il mondo è diventato un luogo più oscuro, dove antichi e malvagi maghi, conosciuti come i signori delle rune, minacciano di svegliarsi da un sonno durato 10000 anni.

Un luogo dove nazioni sono governati da criminali, adoratori dei demoni o peggio, dove nazioni un tempo gloriose ora si crogiolano in paranoie auto commiserative o sanguinose ed infinite rivoluzioni. Un luogo dove nulla è predetto ed ogni cosa può accadere. Un luogo che ha bisogno di eroi come mai prima d'ora. 

Benvenuto nel *Mare Interno* di Golarion.

--------------------------------
La Regione del Mare Interno
--------------------------------

La Regione del *Mare Interno* è il centro mercantile e culturale di due imponenti continenti: Avinstan e Garund. 

Nel cuore delle tiepide acque del Mare Interno si trova Absalom, la città al centro del mondo. Fondata dal dio vivente Aroden, questa antica isola città stato è sopravvissuta a quasi cinque millenni di regni rovesciati, per prosperare e diventare oasi per mercanti e canaglie. 
Ad Ovest il *Mare Interno* passa attraverso lo stretto Arco di Aroden, uno stretto tenacemente contestato, il cui nome deriva dal monolitico ponte di pietra in rovina che una volta connetteva i due continenti nel loro punto di massima vicinanza. 
Ad Est il mare si apre sul vasto Oceano di *Obari*. 

I due continenti che incorniciano il *Mare Interno* sono profondamente diversi l'uno dall'altro. 
*Avistan*, a Nord, è sede di imperi una volta possenti quali *Cheliax* e *Taldor*, e luogo delle rovine di *Lost Thassilion* nel reame di frontiera di *Varisia*. A Sud, oltre lo specchio d'acqua del *Mare Interno*, giacciono i segreti di *Garund*, un aspro continente di aridi deserti e giungle feconde, dove i potenti faraoni di *Osirion* sono emersi dell'Era Oscura per tracciare un nuovo destino per l'umanità.

In generale, la civiltà si concentra sul *Mare Interno*, con barbarie e ferocità concentrate laddove la benefica influenza marittima declina. Sussistono delle eccezioni, ovviamente, e le sparse luci della civiltà si ergono al di sopra delle oscure lande selvatiche e selvagge frontiere lontano a Nord di *Avistan* e Sud di *Garund*. Allo stesso modo, oscure e feroci aree esistono all'interno dell'altrimenti civilizzate terre vicino al *Mare Interno*. 

.. figure:: Immagini/Atlante/MappaGolarion.jpg
    :width: 800
    
    Mappa del mondo di *Golarion*

Mercinari e aspiranti eroi cercano fortuna e gloria lungo questa regione, scoprendo tesori perduti, sedando terribili pericoli, e trovando ignobili morti in ogni ignobile landa, regno e impero di *Avistan* e *Garund*.

A Nord di *Avistan* si estende la *Corona del Mondo*, un'ammasso di terra congelata che connette il continente con *Tian-Xia*. Dove i due si incontrano, feroci barbarie tendono a dominare. Persino nei regni più a Nord che lottano per lo sviluppo della civiltà, come le *Terre dei Re Linnorm* e il *Regno dei Signori dei Mammuth*, l'uso e la conoscenza della magia arcana rimane relativamente sconosciuta e sicuramente diffidata. Anche *Mendev*, una nazione relativamente avanzata e popolata di pii (e non così pii) crociati, tende a rimanere a distanza dalla magia arcana.

La magia diventa più comune nelle nazioni meridionali di *Avistan*, in particolare nel diabolico impero di *Cheliax* e le sue ex colonie e stati vassallo. La casta regnante di *Nidal* è suffusa di forze magiche proibite, mentre gli elfi di *Kyonin* pratica riti alieni che risalgono a millenni addietro. 

Sulle coste rocciose a Nord-Ovest di *Avistan*, la frontiera *Varisiana* vanta le perlopiù intatte rovine e magie perdute dell'antica *Thassilon*, un impero svanito da 10000 anni che fu governato da dei sadici re-maghi noti come signori delle rune.

L'uso della magia e la comparsa del fantastico e bizzarro è molto più diffuso nella zona meridionale del continente di *Garund*. Nei deserti di *Osirion* si trovano innumerevoli monumenti a faraoni quasi dimenticati, esseri dalle facenze divine che hanno cresciuto i loro popoli dalle barbarie a livelli imperiali.

Lungo la costa orientale giacciono i resti di *Nex* e *Geb*, due regni creati per servire re-maghi rivali nel lontano passato. Ad oggi, *Geb* fa uso di cadaveri animati per raccogliere cibo per i propri abitanti viventi, mentre le corti di *Nex* vantano le più avanzate e meno comprese scuole di apprendimento arcano dell'intero pianeta. 

Tra questi ex-avversari si estende un tratto magicamente morto di deserto conosciuto come *Lande di Mana*, all'interno della quale esiste una città stato basata su tecnologia e ingegneria in un mondo dipendente dal soprannaturale.

Nel profondo cuore di *Garund*, oltre le montagne della *Catena Infranta*, antiche rovine dalle sconosciute origini emergono tra selvagge e incivilizzate giungle. Frammentati tra le montagne circostanti la vasta giungla della *Distesa di Mwangi* giacciono le rovine della fu miracolosa città volante di *Shory*, da tempo precipitata sulle discese rocciose dove ora riposano.

Ognuna di queste fantastiche località fanno da sfondo alle eccitandi avventure delle campagne di *Pathfinder*. Il mondo di *Golarion* e la sua miriade di segreti sono pronte per essere esplorate da te e i tuoi giocatori.

.. figure:: Immagini/Atlante/Mappa.png
    :width: 800

    Mappa della Regione del Mare Interno

-----------------------
La Pathfider Society
-----------------------
I più grandi eroi della regione del *Mare Interno* registrano le loro vittorie in una crescente serie di racconti conosciuti come le *Pathfinder Chronicles*. 

Le stupefacenti e spesso incredibili racconti raccolti in questi volumi racconta di dei dimenticati e continenti sepolti, di creature più antiche del mondo stesso caduti dalle stelle nei giorni più antichi e delle fantastiche rovine che si sono lasciati alle spalle.

Gli autori di queste storie appartengono alla *Pathfinder Society*, un disparato gruppo di esploratori, archeologi e avventurieri che girano il globo in cerca di conoscenze perdute e antichi tesori. Alcuni hanno l'obbiettivo di rivelare la storia segreta del mondo, incollandone i pezzi uno alla volta. Altri sono alla ricerca di denaro, spargendo inestimabili tesori attraverso una serie di mercanti senza scrupuli per arricchirsi oltre ogni misura. Altri si danno al mestiere perchè trovano l'emozione del rischio nelle loro vite più esaltante di qualsiasi droga o vizio. 

Una nascosta cerchia di capi mascherati conosciuti come *Decemvirato* governano la *Pathfinder Society* dalla trafficata società do *Absalom*, la così detta città al centro del mondo. Lì, in un gigantesco complesso di fortezze chiamato la *Gran Loggia*, i *Dieci* gestiscono una vasta organizzazione di agenti diffusi in tutta la regione del *Mare Interno* e oltre.

Questi ufficiali, noti come Capitani di Ventura, coordinano i gruppi di *PathFinder* nelle loro regioni, suggerendogli antiche leggende, fornendogli mappe appena ritrovate e appoggiando i loro sforzi nel campo. I Capitani di Ventura sono delle fonti ideali "nel mondo" di piste d'avventura, rendendoli NPC indispendabili nelle campagne di *PathFinder*. 

Questo tuttavia non rende necessariamente ogni Capitano di Ventura un incrollabile alleato: gli scopi finali del *Decemvirato* sono imperscrutabili e non tutti i Capitani di Ventura capiscono appieno il disegno di ciò che la *PathFinder Society* realizza con le informazioni che raccoglie.
Ogni Capitano di Ventura sovrintende le attività di diversi affiatati gruppi di agenti *PathFinder*, che conducono la maggior parte delle attività esplorative e avventuriere che muovono l'intera società. Magari i vostri giocatori sono uno di questi gruppi, che si muovono da luogo a luogo per scoprire i segreti perduti di civiltà morte ed i fulgidi tesori che si sono lasciati alle spalle.

Gli agenti *PathFinder* forniscono rapporti dettagliati delle loro imprese ai loro Capitani di Ventura, che poi inoltrano i racconti più interessanti alla *Grande Loggia* di *Absalom* perchè il *Decemvirato* lo tenga in considerazione. Periodicamente i capi mascherati della società raccolgono e pubblicano le più grandi avventure in nuovi volumi della *PathFinder Chronicles*, che rimandano ai Capitani di Ventura per distribuirli tra gli agenti sul campo. Ogni volta che un nuovo volume raggiunge il campo, dozzine di avventureri si affollano sui siti che vi sono descritti per approfondirne l'esplorazione. 

Benchè appartengano alla stessa organizzazione, gruppi individuali di *PathFinder* possono trovarsi con scopi opposti nel campo, specialmente se fanno rapporto a Capitani di Ventura differenti. La competizione tra gruppi diversi di rado sfocia in battaglie aperte, ma alcuni agenti potrebbero far crollare passaggi, innescare antiche trappole o vendere i propri rivali agli ostili locali; tutto nel nome della sana e sportiva competizione ovviamente.

I giocatori di una campagna di *PathFinder* non devono essere membri della *PathFinder Society* perchè la società giochi un ruolo critico nelle loro vite. 
Benchè i volumi del *PathFinder Chronicles* stessi siano instesi solo per gli occhi degli agenti *PathFinder*, ci sono avventurieri non affiliati, studiosi corrotti e ambiziosi antiquari che rintracciano ogni volume e lo utilizzano come mappe. Persino i più antichi volumi, i cui soggetti sono stati depredati più e più volte, contengono spesso indizi per tesori nascosti. 

Oltre questi libri, un giocatore potrebbe anche incontrare un gruppo di *PathFinder* sul campo, inimicandosi o alleandosi di lì in poi con la società.

--------------------
Usare questo Libro
--------------------
Questo libro fornisce una larga panoramica della regione del *Mare Interno* del mondo di *Golarion*, lo sfondo ufficiale delle campagne di *PathFinder*.

È una comoda risorsa per giocatori e GM che desiderano approfondire il mondo in cui si svolgono le avventure e contiene centinaia di affascinanti agganci che portano ad altrettante avventure. 

*Paizo* riempirà alcuni di questi vuoti col passare degli anni, ma altri sono creati perchè voi stessi possiate esporarli. La regione del *Mare Interno* è dotata di un soprendente numero di nazioni, lande, società, segreti e meraviglie; la domanda per un nuovo arrivato in questa regione è semplice: da dove cominciare?

Abbiamo fatto un tentativo di presentare le informazioni in questo libro nel formato il più logico possibile, tentando di introdurre gli argomenti prima che questi vengano menzionati casualmente, tuttavia, come per ogni lavoro di questa natura, ci sono numerose nuove informazioni da digerire.

Non avere quindi paura alla tua prima esplorazione del *Mare Interno* di vagare per il libro. I disegni e le mappe sono intesi per trasmettere i temi di queste campagne tanto quanto fanno le parole e se vedi qualcosa che attira il tuo interesse non aver paura di fermarti a leggere!