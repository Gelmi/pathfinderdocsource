.. _equip:
.. _equipaggiamento:

***************
Equipaggiamento
***************

.. figure:: Immagini/ManualeDiGioco/Equipaggiamento.png

Un personaggio ben equipaggiato può intraprendere qualsiasi sfida, che sia sopravvivere nelle terre selvagge o fare bella figura ad un banchetto regale. 

Questo capitolo descrive tutti i tipi di oggetti comuni o esotici che i personaggi possono voler comprare e usare, dalle armi alle armature, dagli oggetti alchemici agli attrezzi perfetti, dai cibi prelibati alle razioni da viaggio.

L’equipaggiamento presentato qui si può trovare e comprare facilmente in tutte le città, sebbene il GM possa restringere la disponibilità degli oggetti più costosi ed esotici. Gli :ref:`oggetti magici<oggetti_magici>` sono più difficili da comprare.

.. Note::
    Qualsiasi oggetto citato è presente nel Manuale di Gioco a meno che sia diversamente specificato nella descrizione.


.. contents:: Contenuti
    :local:
    :depth: 2


Ricchezza e Denaro
==================

.. contents:: Contenuti
    :local:
    :depth: 3

Un personaggio che inizia a giocare generalmente ha monete d’oro sufficienti per acquistare gli elementi di base: qualche arma, un’armatura adatta alla sua classe ed un po’ di attrezzatura varia. 

.. _tabella_denaro_iniziale:

.. figure:: Immagini/ManualeDiGioco/EquipTabDenaroIniziale.png
    :width: 300
    :align: right

Man mano che il personaggio intraprende avventure e accumula bottino può permettersi un equipaggiamento migliore e oggetti magici. La :ref:`Tabella<tabella_denaro_iniziale>` elenca le monete d’oro di partenza in base alla classe. Inoltre, ogni personaggio inizia il gioco con un abito del valore di 10 mo o meno. 

.. Note::
    Per personaggi di livello superiore al 1°, vedi la :ref:`Tabella del Denaro per Livello<tabella_denaro_per_livello>`.

Monete
------
.. figure:: Immagini/ManualeDiGioco/EquipTabMonete.png
    :width: 300
    :align: left

La moneta più comune è la moneta d’oro (mo), che vale 10 monete d’argento (ma). Ogni moneta d’argento vale 10 monete di rame (mr). 

Esistono anche monete più preziose come quelle di platino (mp), del valore di 10 monete d’oro ciascuna. 

.. Important::
    La moneta standard pesa circa 10 gr, quindi 100 monete sono 1Kg.

Altre Ricchezze
---------------

.. _tabella_merci_di_scambio:

.. figure:: Immagini/ManualeDiGioco/EquipTabMerciDiScambio.png
    :width: 300
    :align: right

I mercanti di solito scambiano merci anche senza l’uso di monete.

Per farsi un’idea delle transazioni commerciali, alcune merci
di scambio sono descritte in :ref:`Tabella<tabella_merci_di_scambio>`.

Vendere il Bottino
------------------
In generale, è possibile vendere qualsiasi cosa alla metà del
prezzo di mercato, comprese armi, armature, equipaggiamento, oggetti magici e oggetti creati dai personaggi. 

.. Important::
    Le merci di scambio costituiscono l’eccezione alla regola del metà prezzo. Una merce di scambio, in questo senso, è un bene di valore che può essere facilmente scambiato quasi come fosse equivalente ai contanti.

.. _armi:

Armi
====
Dalla comune `Spada Lunga`_ all’esotico `Urgrosh Nanico`_, le
armi variano ampiamente in forma e taglia, ma tutte infliggono punti ferita di danno. Questo danno è sottratto dai punti ferita correnti della creatura colpita. 

Quando il risultato del tiro di dado per un attacco è un 20 naturale (cioè si ottiene 20 con il dado), si segna una minaccia di critico. In tal caso, viene effettuato un altro tiro di attacco, usando gli stessi modificatori del tiro di attacco originale. Se quest'ultimo supera la CA del bersaglio, il colpo è critico ed infligge danni aggiuntivi.

.. Note:: 
    Alcune armi possono segnare una minaccia di critico con un risultato inferiore a 20.

.. contents:: Contenuti
    :local:
    :depth: 3

Tipi
----
Le armi sono raggruppate in categorie, che sono pertinenti al tipo di addestramento necessario per diventare competenti nel loro :ref:`utilizzo<equip_armi_semplici_guerra_esotiche>`, all’utilizzo dell’arma nel combattimento in :ref:`mischia o a distanza<equip_armi_mischia_distanza>` (sia armi da lancio che da tiro), il suo :ref:`ingombro relativo<equip_armi_ingombro_relativo>` (leggera, ad una mano o a due mani) e la sua :ref:`Taglia<equip_taglia_dellarma>` (Piccola, Media o Grande).

.. _equip_armi_semplici_guerra_esotiche:

Semplici, da guerra e Esotiche
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Tranne :ref:`Druidi<druido>`, :ref:`Maghi<mago>` o :ref:`Monaci<monaco>`, tutti sono competenti in tutte le armi semplici.

:ref:`Barbari<barbaro>`, :ref:`Guerrieri<guerriero>`, :ref:`Paladini<paladino>` e :ref:`Ranger` sono competenti anche in tutte le armi da guerra. 

I personaggi di altre classi sono competenti nell’uso di una varietà di armi tra cui anche alcune da guerra o esotiche. 

Tutti i personaggi sono competenti con i colpi senz’armi e con qualsiasi arma naturale possiedano in base alla loro razza. 

.. Warning::
    Se si utilizza un’arma in cui non è si competenti, si subisce penalità –4 al tiro per colpire.

.. _equip_armi_mischia_distanza:

Da Mischia e a Distanza
^^^^^^^^^^^^^^^^^^^^^^^
Le armi da mischia sono utilizzate per compiere attacchi ravvicinati, sebbene alcune possano anche essere lanciate; quelle a distanza sono o da lancio o da tiro e non particolarmente efficaci in mischia.

Con Portata
"""""""""""

Corsesca_, Falcione_, Frusta_, Giusarma_, Lancia_ e `Lancia Lunga`_ sono armi con portata. Ciò significa che è possibile colpire un avversario non adiacente al personaggio. 

La maggior parte di queste armi permettono al personaggio di
raddoppiare la sua portata naturale, quindi un personaggio di taglia Piccola o Media può sferrare un attacco contro una creatura che si trova a 3 metri di distanza, mentre non può colpire un avversario adiacente. Un personaggio di taglia Grande, invece, può attaccare una creatura distante 4, 5 o 6 metri, ma non avversari adiacenti o entro 3 metri di distanza, quando usa un'arma con portata.

Doppie
""""""
`Doppia Ascia Orchesca`_, `Bastone Ferrato`_, `Martello-Picca Gnomesco`_, `Mazzafrusto Doppio`_, `Spada a Due Lame`_ e `Urgrosh Nanico`_ sono armi doppie. 

Un personaggio può utilizzare entrambe le estremità di queste armi come se combattesse con due armi, ma in questo modo incorre in tutte le normali :ref:`penalità<tabella_penalità_due_armi>` di attacco associate al combattere con due armi, come se usasse un’arma a una mano e un’arma leggera. 

Un personaggio può anche scegliere di utilizzare non utilizzare un’arma doppia a due mani, attaccando con una sola estremità e in questo caso può combattere solo con una delle estremità dell’arma per round.

Da Lancio
"""""""""
`Ascia da Lancio`_, Dardo_, Giavellotto_, Lancia_,
`Lancia Corta`_, `Martello Leggero`_, Pugnale_, Randello_, Rete_,
Shuriken_ e Tridente_ sono armi da lancio. 

In tal caso il personaggio può applicare il bonus di Forza ai danni inflitti, tranne per le armi a spargimento. 

È possibile lanciare anche un’arma che non è stata creata per essere lanciata o, in altre parole, che non possiede un valore numerico di “Gittata”. Questi lanci subiscono penalità –4 al tiro per colpire. 

Lanciare un’arma leggera o a una mano corrisponde a un’:ref:`azione_standard`, mentre lanciare un’arma a due mani richiede un’:ref:`azione_di_round_completo`. 

A prescindere dall’arma utilizzata, un attacco di questo tipo segna una minaccia (un possibile colpo critico) soltanto con un 20 naturale, e infligge danni raddoppiati in caso di colpo critico. Le armi da lancio improvvisate hanno un incremento di gittata di 3 metri.

Da Tiro
"""""""
`Arco Corto`_, `Arco Corto Composito`_, `Arco Lungo`_, `Arco Lungo Composito`_, `Balestra a Mano`_, `Balestra a Ripetizione Leggera`_, `Balestra a Ripetizione Pesante`_, `Balestra Leggera`_, `Balestra Pesante`_, `Bastone Fionda Halfling`_, Cerbottana_ e Fionda_ sono armi da tiro e la maggior parte di queste armi richiede due mani. 

Un personaggio non ottiene bonus di Forza ai danni con un’arma da tiro, a meno che non sia un `Arco Corto Composito`_ o un `Arco Lungo Composito`_ appositamente costruito o una Fionda_. 

.. Warning::
    Se un personaggio ha una penalità per un basso punteggio di Forza, la si applica al tiro dei danni quando si usa un arco o una Fionda_.

Munizioni
"""""""""
Le armi da tiro utilizzano munizioni: Frecce (archi), Quadrelli (balestre), dardi (cerbottane) o proiettili (fionda e bastone fionda halfling). 

Quando un personaggio utilizza un arco può incoccare una freccia come :ref:`azione_gratuita`, mentre balestre e fionde richiedono un’azione di diverso tipo per essere ricaricate.

In termini generali, le munizioni che vanno a segno su un bersaglio sono da considerarsi distrutte o inservibili, mentre
le munizioni normali che mancano il bersaglio hanno il 50%
di probabilità di rompersi o andare perdute.

Sebbene appartengano alla categoria delle armi da lancio, gli Shuriken_ vengono considerati munizioni per quanto riguarda i tempi di ricarica e la creazione di `Armi Perfette`_ o altre versioni speciali e per quanto succede quando vengono lanciati.

.. _equip_armi_ingombro_relativo:

Da Mischia Leggere, a una mano e a due mani
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Questa caratteristica corrisponde allo sforzo necessario per utilizzare l’arma in combattimento. 

Quando un personaggio impugna un’arma della sua stessa categoria di taglia, l’arma può essere considerata :ref:`Leggera<equip_armi_leggere>`, `a una mano`_ o `a due mani`_.

.. _equip_armi_leggere:

Leggere
"""""""
Un’arma leggera è usata con una mano. È più facile da usare nella mano secondaria di un’arma `a una mano`_ e può essere utilizzata in una :ref:`Lotta<azione_lottare>`.

Va applicato il modificatore di Forza agli attacchi in mischia
quando l’arma viene usata con la mano primaria, oppure la metà quando viene impugnata con la mano secondaria. 

Usare un’arma leggera a due mani non conferisce alcun vantaggio ai danni inflitti

.. Note::
    Un attacco senz’armi viene sempre considerato come un’arma leggera.

A una mano
""""""""""
Un’arma a una mano può essere impugnata sia nella mano primaria che in quella secondaria. 

Va applicato il modificatore di Forza ai danni per gli attacchi in mischia quando l’arma viene usata con la mano primaria e la metà quando viene impugnata con la mano secondaria. 

Se si usa un’arma a una mano con due mani, è possibile applicare una volta e mezzo il bonus di Forza ai danni.

A due mani
""""""""""
Un’arma a due mani richiede l’utilizzo di due mani per essere usata con efficacia e in tal caso va applicato una volta e mezzo il bonus di Forza ai danni.

.. _equip_taglia_dellarma:

Taglia dell'arma
^^^^^^^^^^^^^^^^
Qualsiasi arma ha una taglia che corrisponde alla taglia della creatura per la quale è stata fabbricata, ma non equivale in alcun modo alla sue qualità. 

In termini generali, un’arma leggera è un oggetto più piccolo di due categorie di taglia rispetto a chi lo utilizza, un’arma a una mano è un oggetto più piccolo di una categoria di taglia rispetto a chi lo utilizza, e un’arma a due mani è un oggetto della stessa taglia di chi lo utilizza.

Taglia inadeguata
"""""""""""""""""
Una creatura non può ottenere la massima efficienza da un’arma costruita per una taglia differente dalla sua, ma subisce una penalità cumulativa di –2 al tiro per colpire per ogni categoria di differenza tra la propria taglia e quella dell'arma. 

.. Note::
    Se la creatura non è competente nell’uso di un’arma aggiunge anche la penalità –4 dovuta alla mancanza dell’adeguato addestramento.

La misura dello sforzo necessario per utilizzare un’arma viene modificata di un livello per ogni categoria di differenza tra la taglia della creatura e quella dell'arma. Per esempio: un’arma ad una mano Media sarebbe un’arma a due mani per una creatura Piccola. 

.. Danger::
    Se la modifica apportata all’arma va oltre alla suddivisione in leggera, a una mano o a due mani, la creatura non può in nessun modo utilizzare quell’arma. È difficile figurarsi uno gnomo che impugna un martello di taglia gigante!

.. _equip_arma_improvvisata:

Armi Improvvisate
^^^^^^^^^^^^^^^^^
Talvolta oggetti che non sono stati creati per essere armi hanno comunque una certa efficacia in combattimento. Dal momento che non si tratta di oggetti pensati per questo utilizzo, la creatura che attacca con uno di essi viene considerata non competente e subisce penalità –4 al tiro per colpire. 

Per determinare la categoria di taglia e il danno di un’arma improvvisata, occorre confrontare la grandezza dell’oggetto e la sua potenziale pericolosità con quella delle armi elencate, cercando di avvicinarsi il più possibile.

Un’arma improvvisata può segnare una minaccia (un possibile colpo
critico) soltanto con un 20 naturale, e infligge danni raddoppiati in caso di colpo critico. Un’arma da lancio improvvisata ha un incremento di gittata di 3 metri.

Caratteristiche delle Armi	
--------------------------
Questo è il formato delle voci delle armi indicate nelle colonne delle :ref:`Tabelle Descrittive<equip_tabelle_armi>`.

Costo
^^^^^
Prezzo dell’arma in monete d’oro (mo) o monete d’argento (ma). 

Il costo comprende oggetti vari associati all’arma, come un fodero o una faretra e rimane lo stesso per le versioni di taglia Piccola e Media, ma un’arma di taglia Grande ha il prezzo raddoppiato.

Danni
^^^^^

.. _equip_tabella_danni_armi_taglia:

.. figure:: Immagini/ManualeDiGioco/EquipTabDanniArmiTaglia.png
    :width: 300
    :align: right

Questa colonna indica i danni che si infliggono con un’arma quando si colpisce. Si noti che a seconda della taglia dell'arma i danni inflitti sono diversi: sotto “Danni (P)” i danni delle armi Piccole, mentre sotto “Danni (M)” quelli delle armi Medie. 

Se sono indicate due entità di danni, allora l’arma è doppia e il secondo danno è quello dell’attacco extra. 

Per i danni di armi di taglia Minuscola e Grande si può consultare la :ref:`Tabella<equip_tabella_danni_armi_taglia>` a lato.

Critico
^^^^^^^
Questa colonna indica come viene considerata l’arma nelle regole per i colpi critici. 

Quando si porta a segno un colpo critico bisogna tirare il danno con i modificatori x2, x3 o x4 come indicato dai suoi moltiplicatori critici (usando tutti i modificatori applicabili ad ogni tiro), e sommare tutti i risultati.

I danni extra non vengono moltiplicati quando si porta a segno un colpo critico.

×2
    L’arma infligge danni raddoppiati con un colpo critico.

×3
    L’arma infligge danni triplicati con un colpo critico.

×3/×4
    Un’estremità di quest’arma doppia infligge danni triplicati con un colpo critico, mentre l’altra quadruplicati.

×4
    L’arma infligge danni quadruplicati con un colpo critico.

19–20/×2
    L’arma segna una minaccia (un possibile colpo critico) con un 19 o 20 naturale, anziché solo un 20, e infligge danni raddoppiati con un colpo critico.

18–20/×2
    L’arma segna una minaccia con un 18, 19 o 20 naturale, anziché solo un 20, e infligge danni raddoppiati con un colpo critico.

Gittata
^^^^^^^
Qualsiasi attacco a meno di questa distanza non è penalizzato per la gittata. 

Oltre questa distanza, ogni incremento di gittata piena comporta invece penalità cumulativa –2 al tiro per colpire. 

.. admonition:: Per Esempio

    Se si lancia un pugnale (con gittata 3 metri) ad un bersaglio che è distante 7,5 metri si subirà penalità –4 al tiro.

Le armi da lancio hanno una gittata massima di cinque incrementi, mentre quelle da tiro dieci.

Peso
^^^^
Questa colonna indica il peso dell’arma in una versione di taglia Media. Le armi Piccole pesano la metà, quelle Grandi il doppio. 

Alcune armi hanno un peso speciale, la cui spiegazione viene riportata nella descrizione dell'oggetto.

Tipo
^^^^
Le armi sono classificate in base al tipo di danno che infliggono:

    * C = Contundente
    * P = Perforante
    * T = Tagliente

.. Important::
    Alcuni mostri possono essere resistenti o immuni agli attacchi di un certo tipo.

Diverse armi infliggono danni di più tipi. In tal caso tutti i danni inflitti sono di entrambi i tipi e una creatura deve essere immune ad entrambi per poter ignorarne il danno.

Esistono anche delle armi che offrono la possibilità di scegliere tra due tipi di danno. In tal caso il personaggio può scegliere tra l’una o l’altra delle opzioni a disposizione.

Speciale
^^^^^^^^
Alcune armi hanno caratteristiche particolari, spesso chiarite nella relativa descrizione.

Puntare
"""""""
Se si usa un’azione preparata per puntare un’arma di questo tipo contro una :ref:`azione_carica`, si infliggono danni raddoppiati in caso di colpo riuscito contro l'attaccante.

Disarmare
"""""""""
Si riceve bonus +2 alla prova di manovra in combattimento per :ref:`azione_disarmare` l’avversario.

Doppia
""""""
Si possono usare armi doppie per combattere come se si combattesse con due armi, ma in questo caso si subiscono tutte le normali penalità associate al combattere con due armi, come se si usasse un’arma ad una mano e un’arma leggera. 

Un’arma doppia può essere impugnata come un’arma a una mano, ma in tal caso solo una delle sue estremità può essere usata ad ogni round.

Monaco
""""""
Un’arma da monaco può essere utilizzata da un :ref:`monaco` per compiere una :ref:`Raffica di Colpi<raffica_di_colpi_monaco>`.

Non Letale
""""""""""
Queste armi infliggono :ref:`Danni Non Letali<danno_non_letale>`.

Portata
"""""""
Un’arma con portata permette di colpire avversari distanti 3 metri, ma non quelli adiacenti.

.. _equip_sbilanciare:

Sbilanciare
"""""""""""
Si può usare un’arma di questo tipo per effettuare attacchi di :ref:`azione_sbilanciare`. 

Al contrario, se si rischia di essere sbilanciati, si può decidere di lasciar cadere l’arma per evitarlo.

Descrizione delle Armi
----------------------
Di seguito vengono descritte ed elencate le armi disponibili. 

.. Warning:: 
    Le armi a spargimento sono descritte nella sezione `Oggetti e sostanze speciali`_.

.. _equip_tabelle_armi:

Tabella Armi Semplici
^^^^^^^^^^^^^^^^^^^^^

+-------------------------+---------------------------+-------+-----------+-----------+----------+---------+----------+------+---------------+
| Classe                  | Nome                      | Costo | Danni (P) | Danni (M) | Critico  | Gittata | Peso (M) | Tipo | Speciale      |
+=========================+===========================+=======+===========+===========+==========+=========+==========+======+===============+
| Attacchi Senz'Armi      |                           |       |           |           |          |         |          |      |               |
+-------------------------+---------------------------+-------+-----------+-----------+----------+---------+----------+------+---------------+
|                         | `Colpo Senz'Armi`_        |       | 1d2       | 1d3       | x2       |         |          | C    | `Non Letale`_ |
+-------------------------+---------------------------+-------+-----------+-----------+----------+---------+----------+------+---------------+
|                         | `Guanto d'Arme`_          | 2mo   | 1d2       | 1d3       | x2       |         | 0.5Kg    | C    |               |
+-------------------------+---------------------------+-------+-----------+-----------+----------+---------+----------+------+---------------+
| Armi da Mischia Leggere |                           |       |           |           |          |         |          |      |               |
+-------------------------+---------------------------+-------+-----------+-----------+----------+---------+----------+------+---------------+
|                         | Falcetto_                 | 6mo   | 1d4       | 1d6       | x2       |         | 1Kg      | T    | Sbilanciare_  |
+-------------------------+---------------------------+-------+-----------+-----------+----------+---------+----------+------+---------------+
|                         | `Guanto d'Arme Chiodato`_ | 5mo   | 1d3       | 1d4       | x2       |         | 0.5Kg    | P    |               |
+-------------------------+---------------------------+-------+-----------+-----------+----------+---------+----------+------+---------------+
|                         | `Mazza Leggera`_          | 5mo   | 1d4       | 1d6       | x2       |         | 2Kg      | C    |               |
+-------------------------+---------------------------+-------+-----------+-----------+----------+---------+----------+------+---------------+
|                         | Pugnale_                  | 2mo   | 1d3       | 1d4       | 19-20/x2 | 3m      | 0.5Kg    | PoT  |               |
+-------------------------+---------------------------+-------+-----------+-----------+----------+---------+----------+------+---------------+
|                         | `Pugnale da Mischia`_     | 2mo   | 1d3       | 1d4       | x3       |         | 0.5Kg    | P    |               |
+-------------------------+---------------------------+-------+-----------+-----------+----------+---------+----------+------+---------------+
| Armi da Mischia a una   |                           |       |           |           |          |         |          |      |               |
| Mano                    |                           |       |           |           |          |         |          |      |               |
+-------------------------+---------------------------+-------+-----------+-----------+----------+---------+----------+------+---------------+
|                         | `Lancia Corta`_           | 1mo   | 1d4       | 1d6       | x2       | 6m      | 1.5Kg    | P    |               |
+-------------------------+---------------------------+-------+-----------+-----------+----------+---------+----------+------+---------------+
|                         | `Mazza Pesante`_          | 12mo  | 1d6       | 1d8       | x2       |         | 4Kg      | C    |               |
+-------------------------+---------------------------+-------+-----------+-----------+----------+---------+----------+------+---------------+
|                         | `Morning Star`_           | 8mo   | 1d6       | 1d8       | x2       |         | 3Kg      | CeP  |               |
+-------------------------+---------------------------+-------+-----------+-----------+----------+---------+----------+------+---------------+
|                         | Randello_                 |       | 1d4       | 1d6       | x2       | 3m      | 1.5Kg    | C    |               |
+-------------------------+---------------------------+-------+-----------+-----------+----------+---------+----------+------+---------------+
| Armi da Mischia a due   |                           |       |           |           |          |         |          |      |               |
| Mani                    |                           |       |           |           |          |         |          |      |               |
+-------------------------+---------------------------+-------+-----------+-----------+----------+---------+----------+------+---------------+
|                         | `Bastone Ferrato`_        |       | 1d4/1d4   | 1d6/1d6   | x2       |         | 2Kg      | C    | Doppia_,      |
|                         |                           |       |           |           |          |         |          |      | Monaco_       |
+-------------------------+---------------------------+-------+-----------+-----------+----------+---------+----------+------+---------------+
|                         | Lancia_                   | 2mo   | 1d6       | 1d8       | x3       | 6m      | 3Kg      | P    | Puntare_      |
+-------------------------+---------------------------+-------+-----------+-----------+----------+---------+----------+------+---------------+
|                         | `Lancia Lunga`_           | 5mo   | 1d6       | 1d8       | x3       |         | 4.5Kg    | P    | Puntare_,     |
|                         |                           |       |           |           |          |         |          |      | Portata_      |
+-------------------------+---------------------------+-------+-----------+-----------+----------+---------+----------+------+---------------+
| Armi a Distanza         |                           |       |           |           |          |         |          |      |               |
+-------------------------+---------------------------+-------+-----------+-----------+----------+---------+----------+------+---------------+
|                         | `Balestra Leggera`_       | 35mo  | 1d6       | 1d8       | 19-20/x2 | 24m     | 2Kg      | P    |               |
+-------------------------+---------------------------+-------+-----------+-----------+----------+---------+----------+------+---------------+
|                         | `Balestra Pesante`_       | 50mo  | 1d8       | 1d10      | 19-20/x2 | 36m     | 4Kg      | P    |               |
+-------------------------+---------------------------+-------+-----------+-----------+----------+---------+----------+------+---------------+
|                         | Cerbottana_               | 2mo   | 1         | 1d2       | x2       | 6m      | 0.5Kg    | P    |               |
+-------------------------+---------------------------+-------+-----------+-----------+----------+---------+----------+------+---------------+
|                         | Dardo_                    | 5ma   | 1d3       | 1d4       | x2       | 6m      | 0.25Kg   | P    |               |
+-------------------------+---------------------------+-------+-----------+-----------+----------+---------+----------+------+---------------+
|                         | Fionda_                   | 1ma   | 1d3       | 1d4       | x2       | 15m     | 2.5Kg    | C    |               |
+-------------------------+---------------------------+-------+-----------+-----------+----------+---------+----------+------+---------------+
|                         | Giavellotto_              | 1mo   | 1d4       | 1d6       | x2       | 9m      | 1Kg      | P    |               |
+-------------------------+---------------------------+-------+-----------+-----------+----------+---------+----------+------+---------------+

Tabella Armi da Guerra
^^^^^^^^^^^^^^^^^^^^^^

+-----------------+---------------------------+----------+-----------+-----------+----------+---------+----------+------+---------------+
| Classe          | Nome                      | Costo    | Danni (P) | Danni (M) | Critico  | Gittata | Peso (M) | Tipo | Speciale      |
+=================+===========================+==========+===========+===========+==========+=========+==========+======+===============+
| Armi da Mischia |                           |          |           |           |          |         |          |      |               |
| Leggere         |                           |          |           |           |          |         |          |      |               |
+-----------------+---------------------------+----------+-----------+-----------+----------+---------+----------+------+---------------+
|                 | `Armatura Chiodata`_      | Speciale | 1d4       | 1d6       | x2       |         | Speciale | P    |               |
+-----------------+---------------------------+----------+-----------+-----------+----------+---------+----------+------+---------------+
|                 | Ascia_                    | 6mo      | 1d4       | 1d6       | x3       |         | 1.5Kg    | T    |               |
+-----------------+---------------------------+----------+-----------+-----------+----------+---------+----------+------+---------------+
|                 | `Ascia da Lancio`_        | 8mo      | 1d4       | 1d6       | x2       | 3m      | 1Kg      | T    |               |
+-----------------+---------------------------+----------+-----------+-----------+----------+---------+----------+------+---------------+
|                 | Astrum_                   | 24mo     | 1d3       | 1d4       | x3       | 6m      | 1.5Kg    | P    |               |
+-----------------+---------------------------+----------+-----------+-----------+----------+---------+----------+------+---------------+
|                 | Kukri_                    | 8mo      | 1d3       | 1d4       | 18-20/x2 |         | 1Kg      | T    |               |
+-----------------+---------------------------+----------+-----------+-----------+----------+---------+----------+------+---------------+
|                 | Manganello_               | 1mo      | 1d4       | 1d6       | x2       |         | 1Kg      | C    | `Non Letale`_ |
+-----------------+---------------------------+----------+-----------+-----------+----------+---------+----------+------+---------------+
|                 | `Martello Leggero`_       | 1mo      | 1d3       | 1d4       | x2       | 6m      | 1Kg      | C    |               |
+-----------------+---------------------------+----------+-----------+-----------+----------+---------+----------+------+---------------+
|                 | `Piccone Leggero`_        | 4mo      | 1d3       | 1d4       | x4       |         | 1.5Kg    | P    |               |
+-----------------+---------------------------+----------+-----------+-----------+----------+---------+----------+------+---------------+
|                 | `Scudo Chiodato Leggero`_ | Speciale | 1d3       | 1d4       | x2       |         | Speciale | P    |               |
+-----------------+---------------------------+----------+-----------+-----------+----------+---------+----------+------+---------------+
|                 | `Scudo Leggero`_          | Speciale | 1d2       | 1d3       | x2       |         | Speciale | C    |               |
+-----------------+---------------------------+----------+-----------+-----------+----------+---------+----------+------+---------------+
|                 | `Spada Corta`_            | 10mo     | 1d4       | 1d6       | 19-20/x2 |         | 1Kg      | P    |               |
+-----------------+---------------------------+----------+-----------+-----------+----------+---------+----------+------+---------------+
| Armi da Mischia |                           |          |           |           |          |         |          |      |               |
| a una  Mano     |                           |          |           |           |          |         |          |      |               |
+-----------------+---------------------------+----------+-----------+-----------+----------+---------+----------+------+---------------+
|                 | `Ascia da Battaglia`_     | 10mo     | 1d6       | 1d8       | x3       |         | 3Kg      | T    |               |
+-----------------+---------------------------+----------+-----------+-----------+----------+---------+----------+------+---------------+
|                 | `Martello da Guerra`_     | 12mo     | 1d6       | 1d8       | x3       |         | 2.5Kg    | C    |               |
+-----------------+---------------------------+----------+-----------+-----------+----------+---------+----------+------+---------------+
|                 | Mazzafrusto_              | 8mo      | 1d6       | 1d8       | x2       |         | 2.5Kg    | C    | Disarmare_,   |
|                 |                           |          |           |           |          |         |          |      | Sbilanciare_  |
+-----------------+---------------------------+----------+-----------+-----------+----------+---------+----------+------+---------------+
|                 | `Piccone Pesante`_        | 8mo      | 1d4       | 1d6       | x4       |         | 3Kg      | P    |               |
+-----------------+---------------------------+----------+-----------+-----------+----------+---------+----------+------+---------------+
|                 | Scimitarra_               | 15mo     | 1d4       | 1d6       | 18-20/x2 |         | 2Kg      | T    |               |
+-----------------+---------------------------+----------+-----------+-----------+----------+---------+----------+------+---------------+
|                 | `Scudo Chiodato Pesante`_ | Speciale | 1d4       | 1d6       | x2       |         | Speciale | P    |               |
+-----------------+---------------------------+----------+-----------+-----------+----------+---------+----------+------+---------------+
|                 | `Scudo Pesante`_          | Speciale | 1d3       | 1d4       | x2       |         | Speciale | C    |               |
+-----------------+---------------------------+----------+-----------+-----------+----------+---------+----------+------+---------------+
|                 | `Spada Lunga`_            | 15mo     | 1d6       | 1d8       | 19-20/x2 |         | 2Kg      | T    |               |
+-----------------+---------------------------+----------+-----------+-----------+----------+---------+----------+------+---------------+
|                 | Stocco_                   | 20mo     | 1d4       | 1d6       | 18-20/x2 |         | 1Kg      | P    |               |
+-----------------+---------------------------+----------+-----------+-----------+----------+---------+----------+------+---------------+
|                 | Tridente_                 | 15mo     | 1d6       | 1d8       | x2       | 3m      | 2Kg      | P    | Puntare_      |
+-----------------+---------------------------+----------+-----------+-----------+----------+---------+----------+------+---------------+
| Armi da Mischia |                           |          |           |           |          |         |          |      |               |
| a due Mani      |                           |          |           |           |          |         |          |      |               |
+-----------------+---------------------------+----------+-----------+-----------+----------+---------+----------+------+---------------+
|                 | Alabarda_                 | 10mo     | 1d8       | 1d10      | x3       |         | 6Kg      | PoT  | Puntare_,     |
|                 |                           |          |           |           |          |         |          |      | Sbilanciare_  |
+-----------------+---------------------------+----------+-----------+-----------+----------+---------+----------+------+---------------+
|                 | `Ascia Bipenne`_          | 20mo     | 1d10      | 1d12      | x3       |         | 6Kg      | T    |               |
+-----------------+---------------------------+----------+-----------+-----------+----------+---------+----------+------+---------------+
|                 | Corsesca_                 | 10mo     | 1d6       | 2d4       | x3       |         | 6Kg      | P    | Disarmare_,   |
|                 |                           |          |           |           |          |         |          |      | Portata_      |
+-----------------+---------------------------+----------+-----------+-----------+----------+---------+----------+------+---------------+
|                 | Falce_                    | 18mo     | 1d6       | 2d4       | x4       |         | 5Kg      | PoT  | Sbilanciare_  |
+-----------------+---------------------------+----------+-----------+-----------+----------+---------+----------+------+---------------+
|                 | Falchion_                 | 75mo     | 1d6       | 2d4       | 18-20/x2 |         | 4Kg      | T    |               |
+-----------------+---------------------------+----------+-----------+-----------+----------+---------+----------+------+---------------+
|                 | Falcione_                 | 8mo      | 1d8       | 1d10      | x3       |         | 5Kg      | T    | Portata_      |
+-----------------+---------------------------+----------+-----------+-----------+----------+---------+----------+------+---------------+
|                 | Giusarma_                 | 9mo      | 1d6       | 2d4       | x3       |         | 6Kg      | T    | Portata_,     |
|                 |                           |          |           |           |          |         |          |      | Sbilanciare_  |
+-----------------+---------------------------+----------+-----------+-----------+----------+---------+----------+------+---------------+
|                 | `Lancia da Cavaliere`_    | 10mo     | 1d6       | 1d8       | x3       |         | 5Kg      | P    | Portata_      |
+-----------------+---------------------------+----------+-----------+-----------+----------+---------+----------+------+---------------+
|                 | `Mazzafrusto Pesante`_    | 15mo     | 1d8       | 1d10      | 19-20/x2 |         | 5Kg      | C    | Disarmare_,   |
|                 |                           |          |           |           |          |         |          |      | Sbilanciare_  |
+-----------------+---------------------------+----------+-----------+-----------+----------+---------+----------+------+---------------+
|                 | `Randello Pesante`_       | 5mo      | 1d8       | 1d10      | x2       |         | 4Kg      | C    |               |
+-----------------+---------------------------+----------+-----------+-----------+----------+---------+----------+------+---------------+
|                 | Spadone_                  | 50mo     | 1d10      | 2d6       | 19-20/x2 |         | 4Kg      | T    |               |
+-----------------+---------------------------+----------+-----------+-----------+----------+---------+----------+------+---------------+
| Armi a Distanza |                           |          |           |           |          |         |          |      |               |
+-----------------+---------------------------+----------+-----------+-----------+----------+---------+----------+------+---------------+
|                 | `Arco Corto`_             | 30mo     | 1d4       | 1d6       | x3       | 18m     | 1Kg      | P    |               |
+-----------------+---------------------------+----------+-----------+-----------+----------+---------+----------+------+---------------+
|                 | `Arco Corto Composito`_   | 75mo     | 1d4       | 1d6       | x3       | 21m     | 1Kg      | P    |               |
+-----------------+---------------------------+----------+-----------+-----------+----------+---------+----------+------+---------------+
|                 | `Arco Lungo`_             | 75mo     | 1d6       | 1d8       | x3       | 30m     | 1.5Kg    | P    |               |
+-----------------+---------------------------+----------+-----------+-----------+----------+---------+----------+------+---------------+
|                 | `Arco Lungo Composito`_   | 100mo    | 1d6       | 1d8       | x3       | 33m     | 1.5Kg    | P    |               |
+-----------------+---------------------------+----------+-----------+-----------+----------+---------+----------+------+---------------+

Tabella Armi Esotiche
^^^^^^^^^^^^^^^^^^^^^

+-----------------+-----------------------------------+-------+-----------+-----------+----------+---------+----------+------+----------------+
| Classe          | Nome                              | Costo | Danni (P) | Danni (M) | Critico  | Gittata | Peso (M) | Tipo | Speciale       |
+=================+===================================+=======+===========+===========+==========+=========+==========+======+================+
| Armi da Mischia |                                   |       |           |           |          |         |          |      |                |
| Leggere         |                                   |       |           |           |          |         |          |      |                |
+-----------------+-----------------------------------+-------+-----------+-----------+----------+---------+----------+------+----------------+
|                 | Kama_                             | 2mo   | 1d4       | 1d6       | x2       |         | 1Kg      | T    | Monaco_,       |
|                 |                                   |       |           |           |          |         |          |      | Sbilanciare_   |
+-----------------+-----------------------------------+-------+-----------+-----------+----------+---------+----------+------+----------------+
|                 | Nunchaku_                         | 2mo   | 1d4       | 1d6       | x2       |         | 1Kg      | C    | Disarmare_,    |
|                 |                                   |       |           |           |          |         |          |      | Monaco_        |
+-----------------+-----------------------------------+-------+-----------+-----------+----------+---------+----------+------+----------------+
|                 | Sai_                              | 1mo   | 1d3       | 1d4       | x2       |         | 0.5Kg    | C    | Disarmare_,    |
|                 |                                   |       |           |           |          |         |          |      | Monaco_        |
+-----------------+-----------------------------------+-------+-----------+-----------+----------+---------+----------+------+----------------+
|                 | Siangham_                         | 3mo   | 1d4       | 1d6       | x2       |         | 0.5Kg    | P    | Monaco_        |
+-----------------+-----------------------------------+-------+-----------+-----------+----------+---------+----------+------+----------------+
| Armi da Mischia |                                   |       |           |           |          |         |          |      |                |
| a una  Mano     |                                   |       |           |           |          |         |          |      |                |
+-----------------+-----------------------------------+-------+-----------+-----------+----------+---------+----------+------+----------------+
|                 | `Ascia da Guerra Nanica`_         | 30mo  | 1d8       | 1d10      | x3       |         | 4Kg      | T    |                |
+-----------------+-----------------------------------+-------+-----------+-----------+----------+---------+----------+------+----------------+
|                 | Frusta_                           | 1mo   | 1d2       | 1d3       | x2       |         | 1Kg      | T    | Disarmare_,    |
|                 |                                   |       |           |           |          |         |          |      | `Non Letale`_, |
|                 |                                   |       |           |           |          |         |          |      | Portata_,      |
|                 |                                   |       |           |           |          |         |          |      | Sbilanciare_   |
+-----------------+-----------------------------------+-------+-----------+-----------+----------+---------+----------+------+----------------+
|                 | `Spada Bastarda`_                 | 35mo  | 1d8       | 1d10      | 19-20/x2 |         | 3Kg      | T    |                |
+-----------------+-----------------------------------+-------+-----------+-----------+----------+---------+----------+------+----------------+
| Armi da Mischia |                                   |       |           |           |          |         |          |      |                |
| a due Mani      |                                   |       |           |           |          |         |          |      |                |
+-----------------+-----------------------------------+-------+-----------+-----------+----------+---------+----------+------+----------------+
|                 | `Catena Chiodata`_                | 25mo  | 1d6       | 2d4       | x2       |         | 5Kg      | P    | Disarmare_,    |
|                 |                                   |       |           |           |          |         |          |      | Sbilanciare_   |
+-----------------+-----------------------------------+-------+-----------+-----------+----------+---------+----------+------+----------------+
|                 | `Doppia Ascia Orchesca`_          | 60mo  | 1d6/1d6   | 1d8/1d8   | x3       |         | 7.5Kg    | T    | Doppia_        |
+-----------------+-----------------------------------+-------+-----------+-----------+----------+---------+----------+------+----------------+
|                 | `Martello-Picca Gnomesco`_        | 20mo  | 1d6/1d4   | 1d8/1d6   | x3/x4    |         | 3Kg      | CoP  | Doppia_,       |
|                 |                                   |       |           |           |          |         |          |      | Sbilanciare_   |
+-----------------+-----------------------------------+-------+-----------+-----------+----------+---------+----------+------+----------------+
|                 | `Mazzafrusto Doppio`_             | 90mo  | 1d6/1d6   | 1d8/1d8   | x2       |         | 5Kg      | C    | Disarmare_,    |
|                 |                                   |       |           |           |          |         |          |      | Doppia_,       |
|                 |                                   |       |           |           |          |         |          |      | Sbilanciare_   |
+-----------------+-----------------------------------+-------+-----------+-----------+----------+---------+----------+------+----------------+
|                 | `Sciabola Elfica`_                | 80mo  | 1d8       | 1d10      | 18-20/x2 |         | 3.5Kg    | T    |                |
+-----------------+-----------------------------------+-------+-----------+-----------+----------+---------+----------+------+----------------+
|                 | `Spada a due Lame`_               | 100mo | 1d6/1d6   | 1d8/1d8   | 19-20/x2 |         | 5Kg      | T    | Doppia_        |
+-----------------+-----------------------------------+-------+-----------+-----------+----------+---------+----------+------+----------------+
|                 | `Urgrosh Nanico`_                 | 50mo  | 1d6/1d4   | 1d8/1d6   | x3       |         | 6Kg      | PoT  | Doppia_,       |
|                 |                                   |       |           |           |          |         |          |      | Puntare_       |
+-----------------+-----------------------------------+-------+-----------+-----------+----------+---------+----------+------+----------------+
| Armi a Distanza |                                   |       |           |           |          |         |          |      |                |
+-----------------+-----------------------------------+-------+-----------+-----------+----------+---------+----------+------+----------------+
|                 | `Balestra a Mano`_                | 100mo | 1d3       | 1d4       | 19-20/x2 | 9m      | 1Kg      | P    |                |
+-----------------+-----------------------------------+-------+-----------+-----------+----------+---------+----------+------+----------------+
|                 | `Balestra a Ripetizione Leggera`_ | 250mo | 1d6       | 1d8       | 19-20/x2 | 24m     | 3Kg      | P    |                |
+-----------------+-----------------------------------+-------+-----------+-----------+----------+---------+----------+------+----------------+
|                 | `Balestra a Ripetizione Pesante`_ | 400mo | 1d8       | 1d10      | 19-20/x2 | 36m     | 6Kg      | P    |                |
+-----------------+-----------------------------------+-------+-----------+-----------+----------+---------+----------+------+----------------+
|                 | `Bastone Fionda Halfling`_        | 20mo  | 1d6       | 1d8       | x3       | 80m     | 1.5Kg    | C    |                |
+-----------------+-----------------------------------+-------+-----------+-----------+----------+---------+----------+------+----------------+
|                 | `Bolas`_                          | 5mo   | 1d3       | 1d4       | x2       | 3m      | 1Kg      | C    | `Non Letale`_, |
|                 |                                   |       |           |           |          |         |          |      | Sbilanciare_   |
+-----------------+-----------------------------------+-------+-----------+-----------+----------+---------+----------+------+----------------+
|                 | `Rete`_                           | 20mo  |           |           |          | 3m      | 3Kg      |      |                |
+-----------------+-----------------------------------+-------+-----------+-----------+----------+---------+----------+------+----------------+
|                 | Shuriken_ (5)                     | 1mo   | 1         | 1d2       | x2       | 3m      | 0.25Kg   | P    | Monaco_        |
+-----------------+-----------------------------------+-------+-----------+-----------+----------+---------+----------+------+----------------+

Tabella Proiettili
^^^^^^^^^^^^^^^^^^^^^

+-----------------------+-------+-------+
| Nome                  | Costo | Peso  |
+=======================+=======+=======+
| Dardi per Cerbottana  | 5ma   |       |
| (10)                  |       |       |
+-----------------------+-------+-------+
| Frecce (20)           | 1mo   | 1.5Kg |
+-----------------------+-------+-------+
| Proiettili per Fionda | 1ma   | 2.5Kg |
| (10)                  |       |       |
+-----------------------+-------+-------+
| Quadrelli_ Standard   | 1mo   | 0.5Kg |
| (10)                  |       |       |
+-----------------------+-------+-------+
| Quadrelli_ per        | 1mo   | 0.5Kg |
| Ripetizione (5)       |       |       |
+-----------------------+-------+-------+

Elenco Alfabetico
^^^^^^^^^^^^^^^^^

Qui vengono riportate le armi in ordine alfabetico, con le loro statistiche e una descrizione, laddove necessaria. 

.. contents:: Armi
    :local:
    :depth: 3

.. _equip_alabarda:

Alabarda
""""""""

:Tipo di Arma: Da Guerra
:Classe: Da Mischia a due Mani
:Costo: 10 mo
:Danni(P): 1d8
:Danni(M): 1d10
:Critico: x3
:Gittata: N/A
:Peso(M): 6 Kg
:Tipo di Danno: Perforante o Tagliente
:Speciale: Puntare_, Sbilanciare_

È simile ad una lancia di 1,5 metri, ma è dotata di un uncino montato in punta.

.. _equip_arco_corto:

Arco Corto
""""""""""
:Tipo di Arma: Da Guerra
:Classe: A Distanza
:Costo: 30 mo
:Danni(P): 1d4
:Danni(M): 1d6
:Critico: x3
:Gittata: 18 m
:Peso(M): 1 Kg
:Tipo di Danno: Perforante
:Speciale: N/A
 
Un arco corto è costituito da un unico pezzo di legno di circa 90 cm di lunghezza e indipendentemente dalla taglia sono necessarie almeno due mani per farne uso, ma si può utilizzare mentre si è in sella. 

In caso si abbia una penalità per un basso punteggio di Forza, questa si applica al tiro per i danni. Al contrario, se si ha un bonus per un alto punteggio di Forza, questo si applica al tiro per i danni soltanto quando si utilizza un `Arco Corto Composito`_, ma non quello normale.

Arco Corto Composito
""""""""""""""""""""
:Tipo di Arma: Da Guerra
:Classe: A Distanza
:Costo: 75 mo
:Danni(P): 1d4
:Danni(M): 1d6
:Critico: x3
:Gittata: 21 m
:Peso(M): 1 Kg
:Tipo di Danno: Perforante
:Speciale: N/A
 
Necessarie almeno due mani per usarlo, si può utilizzare mentre si è in sella. 

Gli archi compositi vengono tarati su un particolare punteggio di Forza: richiedono un Mod For minimo affinché si possa utilizzarli con efficacia. Se il bonus è inferiore a quello necessario si subisce penalità –2 al tiro per colpire. 

Il modello standard richiede bonus Forza +0 oppure superiore e può essere costruito con tiranti particolarmente pesanti per avvantaggiarsi di punteggi di Forza superiori alla media: in questo modo è possibile aggiungere il bonus di Forza ai danni fino al bonus massimo elencato. Anche le penalità di Forza si applica ai danni quando si usa questo tipo di archi.

.. Important::
    Ogni punto di bonus di Forza aumenta il suo costo di 75 mo.

L’arco corto composito viene considerato come fosse un `Arco Corto`_ per quanto riguarda :ref:`Arma Focalizzata<tal_arma_focalizzata>`, :ref:`Competenza nelle Armi<tal_competenza_nelle_armi>` e talenti simili.

Arco Lungo
""""""""""
.. figure:: Immagini/ManualeDiGioco/EquipArcoLungo.png
    :width: 100
    :align: right

:Tipo di Arma: Da Guerra
:Classe: A Distanza
:Costo: 75 mo
:Danni(P): 1d6
:Danni(M): 1d8
:Critico: x3
:Gittata: 30 m
:Peso(M): 1.5 Kg
:Tipo di Danno: Perforante
:Speciale: N/A

Un arco lungo è costituito da un resistente pezzo di legno accuratamente curvato di almeno 150 cm di lunghezza e sono necessarie almeno due mani per usarlo indipendentemente dalla taglia. Questo arco, inoltre, è troppo ingombrante per essere usato mentre si è in sella.

Se si ha una penalità per un basso punteggio di Forza, questa si applica al tiro per i danni, ma se si ha un bonus per un alto punteggio di Forza, questo si applica soltanto quando si utilizza un `Arco Lungo Composito`_, non con quello standard.

Arco Lungo Composito
""""""""""""""""""""
:Tipo di Arma: Da Guerra
:Classe: A Distanza
:Costo: 100 mo
:Danni(P): 1d6
:Danni(M): 1d8
:Critico: x3
:Gittata: 33 m
:Peso(M): 1.5 Kg
:Tipo di Danno: Perforante
:Speciale: N/A

Sono necessarie almeno due mani per usarlo a prescindere dalla taglia e non è possibile utilizzarlo mentre si è in sella. 

Gli archi compositi vengono tarati su un particolare punteggio di Forza: richiedono un Mod For minimo affinché si possa utilizzarli con efficacia. Se il bonus è inferiore a quello necessario si subisce penalità –2 al tiro per colpire. 

Il modello standard richiede bonus Forza +0 oppure superiore e può essere costruito con tiranti particolarmente pesanti per avvantaggiarsi di punteggi di Forza superiori alla media: in questo modo è possibile aggiungere il bonus di Forza ai danni fino al bonus massimo elencato. Anche le penalità di Forza si applica ai danni quando si usa questo tipo di archi.

.. Important::
    Ogni punto di bonus di Forza aumenta il suo costo di 100 mo.

L’arco lungo composito viene considerato come fosse un `Arco Lungo`_ per quanto riguarda :ref:`Arma Focalizzata<tal_arma_focalizzata>`, :ref:`Competenza nelle Armi<tal_competenza_nelle_armi>` e talenti simili.

.. _armatura_chiodata:

Armatura Chiodata
"""""""""""""""""
:Tipo di Arma: Da Guerra
:Classe: Da Mischia Leggera
:Costo: Speciale
:Danni(P): 1d4
:Danni(M): 1d6
:Critico: x2
:Gittata: N/A
:Peso(M): Speciale
:Tipo di Danno: Perforante
:Speciale: N/A
 
È possibile aggiungere una `Chiodatura per Armatura`_ a qualsiasi armatura, che infliggono danni agli avversari durante una lotta o con un attacco separato. I dettagli sono riportati nella sezione dedicata.

Ascia
"""""
:Tipo di Arma: Da Guerra
:Classe: Da Mischia Leggera
:Costo: 6 mo
:Danni(P): 1d4
:Danni(M): 1d6
:Critico: x3
:Gittata: N/A
:Peso(M): 1.5 Kg
:Tipo di Danno: Tagliente
:Speciale: N/A

Ascia Bipenne
"""""""""""""
.. figure:: Immagini/ManualeDiGioco/EquipAsciaBipenne.png
    :width: 180
    :align: right

:Tipo di Arma: Da Guerra
:Classe: Da Mischia a Due Mani
:Costo: 20 mo
:Danni(P): 1d10
:Danni(M): 1d12
:Critico: x3
:Gittata: N/A
:Peso(M): 6 Kg
:Tipo di Danno: Tagliente
:Speciale: N/A

Ascia da Battaglia
""""""""""""""""""
.. figure:: Immagini/ManualeDiGioco/EquipAsciaDaBattaglia.png
    :width: 210
    :align: right

:Tipo di Arma: Da Guerra
:Classe: Da Mischia a Una Mano
:Costo: 10 mo
:Danni(P): 1d6
:Danni(M): 1d8
:Critico: x3
:Gittata: N/A
:Peso(M): 3 Kg
:Tipo di Danno: Tagliente
:Speciale: N/A

Ascia da Guerra Nanica
""""""""""""""""""""""
:Tipo di Arma: Esotica
:Classe: Da Mischia a Una Mano
:Costo: 30 mo
:Danni(P): 1d8
:Danni(M): 1d10
:Critico: x3
:Gittata: N/A
:Peso(M): 4 Kg
:Tipo di Danno: Tagliente
:Speciale: N/A
 
Dotata di una lama spessa e decorata infilata su un manico massiccio, è troppo grande per essere usata in una mano senza un addestramento speciale, quindi viene considerata un’arma esotica. 

Un personaggio di taglia Media può usare quest'arma con due mani come arma da guerra, ma una creatura di taglia Grande può usarla in una mano allo stesso modo. Per i nani è un’arma da guerra anche quando la utilizzano a una mano.

Ascia da Lancio
"""""""""""""""
:Tipo di Arma: Da Guerra
:Classe: Da Mischia Leggera
:Costo: 8 mo
:Danni(P): 1d4
:Danni(M): 1d6
:Critico: x2
:Gittata: 3 m
:Peso(M): 1 Kg
:Tipo di Danno: Tagliente
:Speciale: N/A

Astrum
""""""
.. figure:: Immagini/ManualeDiGioco/EquipAstrum.png
    :width: 250
    :align: right

:Tipo di Arma: Da Guerra
:Classe: Da Mischia Leggera
:Costo: 24 mo
:Danni(P): 1d3
:Danni(M): 1d4
:Critico: x3
:Gittata: 6 m
:Peso(M): 1.5 Kg
:Tipo di Danno: Perforante
:Speciale: N/A

Da un anello centrale di metallo, si estendono quattro lame di metallo affusolate come le punte della rosa di una bussola. Si può pugnalare con l’astrum o lanciarlo.

Balestra a Mano
"""""""""""""""
:Tipo di Arma: Esotica
:Classe: A Distanza
:Costo: 100 mo
:Danni(P): 1d3
:Danni(M): 1d4
:Critico: 19-20/x2
:Gittata: 9 m
:Peso(M): 1 Kg
:Tipo di Danno: Perforante
:Speciale: N/A

È possibile caricare una balestra a mano con le mani usando un’:ref:`azione_di_movimento` che provoca :ref:`Attacchi di Opportunità<attacco_di_opportunità>`. 

È possibile sparare, ma non ricaricare, utilizzando una mano sola, senza penalità, così come sparare con due balestre tenendole una per mano. In tal caso, però, il personaggio subisce penalità al tiro per colpire come se :ref:`attaccasse con due armi leggere<combattere_con_due_armi>`.

Balestra a Ripetizione Leggera
""""""""""""""""""""""""""""""
:Tipo di Arma: Esotica
:Classe: A Distanza
:Costo: 250 mo
:Danni(P): 1d6
:Danni(M): 1d8
:Critico: 19-20/x2
:Gittata: 24 m
:Peso(M): 3 Kg
:Tipo di Danno: Perforante
:Speciale: N/A

La balestra a ripetizione (sia leggera che pesante) contiene 5 quadrelli e, quando è carica, si può continuare a sparare con la balestra tirando la leva di ricarica (:ref:`azione_gratuita`). 

Caricare un nuovo astuccio con 5 quadrelli è un’:ref:`azione_di_round_completo` che provoca :ref:`Attacchi di Opportunità<attacco_di_opportunità>`. 

Si può sparare con una mano sola o con due come fosse una normale balestra della stessa taglia, tuttavia sono necessarie due mani per utilizzare la leva di ricarica che permette il fuoco a ripetizione, così come per caricare un nuovo astuccio di quadrelli.

Balestra a Ripetizione Pesante
""""""""""""""""""""""""""""""

:Tipo di Arma: Esotica
:Classe: A Distanza
:Costo: 400 mo
:Danni(P): 1d8
:Danni(M): 1d10
:Critico: 19-20/x2
:Gittata: 36 m
:Peso(M): 6 Kg
:Tipo di Danno: Perforante
:Speciale: N/A

Vedi `Balestra a Ripetizione Leggera`_


Balestra Leggera
""""""""""""""""
.. figure:: Immagini/ManualeDiGioco/EquipBalestraLeggera.png
    :width: 350
    :align: right

:Tipo di Arma: Semplice
:Classe: A Distanza
:Costo: 35 mo
:Danni(P): 1d6
:Danni(M): 1d8
:Critico: 19-20/x2
:Gittata: 24 m
:Peso(M): 2 Kg
:Tipo di Danno: Perforante
:Speciale: N/A

Quest'arma si carica tirando una leva al costo di un’:ref:`azione_di_movimento` che provoca :ref:`Attacchi di Opportunità<attacco_di_opportunità>`.

Normalmente richiede due mani per essere utilizzata, ma si può comunque sparare (non caricare) con una sola pagando una penalità –2 al tiro per colpire. 

È anche possibile sparare con due balestre leggere tenendole una in ogni mano, ma si subiscono penalità al tiro per colpire come se si :ref:`attaccasse con due armi leggere<combattere_con_due_armi>`, penalità cumulativa con la penalità applicata quando si spara con una mano sola.

Balestra Pesante
""""""""""""""""

:Tipo di Arma: Semplice
:Classe: A Distanza
:Costo: 50 mo
:Danni(P): 1d8
:Danni(M): 1d10
:Critico: 19-20/x2
:Gittata: 36 m
:Peso(M): 4 Kg
:Tipo di Danno: Perforante
:Speciale: N/A

Quest'arma si carica girando una piccola manovella al costo di un’:ref:`azione_di_round_completo` che provoca :ref:`Attacchi di Opportunità<attacco_di_opportunità>`.

Normalmente richiede due mani per essere utilizzata, ma si può comunque sparare (non caricare) con una sola pagando una penalità –4 al tiro per colpire. 

È anche possibile sparare con due balestre pesanti tenendole una in ogni mano, ma si subiscono penalità al tiro per colpire come se si :ref:`attaccasse con due armi leggere<combattere_con_due_armi>`, penalità cumulativa con la penalità applicata quando si spara con una mano sola.

Bastone Ferrato
"""""""""""""""
.. figure:: Immagini/ManualeDiGioco/EquipBastoneFerrato.png
    :width: 20
    :align: right

:Tipo di Arma: Semplice
:Classe: Da Mischia a Due Mani
:Costo: N/A
:Danni(P): 1d4/1d4
:Danni(M): 1d6/1d6
:Critico: x2
:Gittata: N/A
:Peso(M): 2 Kg
:Tipo di Danno: Contundente
:Speciale: Doppia_, Monaco_

È un semplice bastone di legno rinforzato di circa 50 cm di lunghezza.

Bastone Fionda Halfling
"""""""""""""""""""""""

:Tipo di Arma: Esotica
:Classe: A Distanza
:Costo: 20 mo
:Danni(P): 1d6
:Danni(M): 1d8
:Critico: x3
:Gittata: 80 m
:Peso(M): 1.5 Kg
:Tipo di Danno: Contundente
:Speciale: N/A

Ricavato da una fionda appositamente ideata attaccata ad un corto manico, il bastone fionda può essere usato da chi è competente con effetti devastanti.

Si applica il bonus di Forza ai danni quando si attacca con un bastone fionda, come per tutte le armi da lancio, ma è possibile lanciare (non ricaricare) con una mano sola. Caricare è un’:ref:`Azione di Movimento<azione_movimento>` che richiede due mani e provoca :ref:`Attacchi di Opportunità<attacco_opportunità>`.

.. Note:: Si possono scagliare pietre con un bastone fionda halfling, ma le pietre non sono cosi compatte o rotonde come i proiettili, quindi l’attacco viene trattato come se l’arma fosse adatta a una creatura più piccola di una categoria di taglia e si subisce penalità –1 al tiro per colpire.

Un bastone fionda halfling può essere usato come arma semplice che infligge danni contundenti pari a quelli che infliggerebbe un randello della stessa taglia.

Gli :ref:`Halfling` considerano il bastone fionda halfling come arma da guerra.

Bolas
"""""
.. figure:: Immagini/ManualeDiGioco/EquipBolas.png
    :width: 200
    :align: right

:Tipo di Arma: Esotica
:Classe: A Distanza
:Costo: 5 mo
:Danni(P): 1d3
:Danni(M): 1d4
:Critico: x2
:Gittata: 3 m
:Peso(M): 1 Kg
:Tipo di Danno: Contundente
:Speciale: `Non Letale`_, Sbilanciare_

Le bolas sono composte da due o tre sfere pesanti, tenute insieme da stringhe o corda.

È possibile usare le bolas per compiere attacchi a distanza per :ref:`Sbilanciare` un avversario e durante questa azione non si può essere sbilanciati a propria volta.

Catena Chiodata
"""""""""""""""
.. figure:: Immagini/ManualeDiGioco/EquipCatenaChiodata.png
    :width: 250
    :align: right

:Tipo di Arma: Esotica
:Classe: Da Mischia a Due Mani
:Costo: 25 mo
:Danni(P): 1d6
:Danni(M): 2d4
:Critico: x2
:Gittata: N/A
:Peso(M): 5 Kg
:Tipo di Danno: Perforante
:Speciale: Disarmare_, Sbilanciare_

Una catena chiodata è di circa 120 cm di lunghezza ed è dotata di affilati uncini.

È possibile usare :ref:`Arma Accurata<tal_arma_accurata>` per applicare il modificatore di Destrezza al posto del modificatore di Forza al tiro per colpire, anche quando essa non costituisce un’arma leggera.

Cerbottana
""""""""""

:Tipo di Arma: Semplice
:Classe: A Distanza
:Costo: 2 mo
:Danni(P): 1
:Danni(M): 1d2
:Critico: x2
:Gittata: 6 m
:Peso(M): 0.5 Kg
:Tipo di Danno: Perforante
:Speciale: N/A

Le cerbottane sono usate per scagliare dardi avvelenati debilitanti (ma raramente mortali) da lontano ed è praticamente impossibile sentire quando il dardo viene scagliato.

Per una lista di veleni appropriati, si veda la :ref:`Sezione Dedicata<veleni>`.

Colpo Senz'Armi
"""""""""""""""

:Tipo di Arma: Semplice
:Classe: Senz'Armi
:Costo: N/A
:Danni(P): 1d2
:Danni(M): 1d3
:Critico: x2
:Gittata: N/A
:Peso(M): N/A
:Tipo di Danno: Contundente
:Speciale: `Non Letale`_

Un personaggio di taglia Media infligge 1d3 danni non letali con un colpo senz’armi, mentre uno di taglia Piccola 1d2.

Un :ref:`Monaco` o qualsiasi altro personaggio che possiede :ref:`Colpo Senz’Armi Migliorato<tal_colpo_senzarmi_migliorato>` può scegliere di infliggere danni letali o danni non letali.
I danni da un colpo senz’armi sono considerati danni da arma per quanto riguarda gli effetti che forniscono un bonus ai danni da arma ed è sempre considerato come fosse un’arma leggera.
È quindi possibile usare :ref:`Arma Accurata<tal_arma_accurata>` per applicare il modificatore di Destrezza al posto di quello Forza al tiro per colpire.

.. Note:: I colpi senz’armi non contano come :ref:`Armi Naturali<attacchi_naturali>`.

.. _equip_corsesca:

Corsesca
""""""""

.. figure:: Immagini/ManualeDiGioco/EquipCorsesca.png
    :width: 50
    :align: right

:Tipo di Arma: Da Guerra
:Classe: Da Mischia a Due Mani
:Costo: 10 mo
:Danni(P): 1d6
:Danni(M): 2d4
:Critico: x3
:Gittata: N/A
:Peso(M): 6 Kg
:Tipo di Danno: Perforante
:Speciale: Disarmare_, Portata_

La corsesca è simile al Tridente_, ma ad una sola punta in cima e sul fianco un paio di corte lame ricurve.

Dardo
"""""

:Tipo di Arma: Semplice
:Classe: A Distanza
:Costo: 5 ma
:Danni(P): 1d3
:Danni(M): 1d4
:Critico: x2
:Gittata: 6m
:Peso(M): 0.25 Kg
:Tipo di Danno: Perforante
:Speciale: N/A

Dardi per Cerbottana
""""""""""""""""""""

:Tipo di Arma: Proiettile
:Costo: 5 ma
:Peso(M): N/A
:Quantità: 10

Doppia Ascia Orchesca
"""""""""""""""""""""

:Tipo di Arma: Esotica
:Classe: Da Mischia a Due Mani
:Costo: 60 mo
:Danni(P): 1d6/1d6
:Danni(M): 1d8/1d8
:Critico: x2
:Gittata: N/A
:Peso(M): 7.5 Kg
:Tipo di Danno: Tagliente
:Speciale: Doppia_

Arma terribile, dotata di spesse lame montate ai lati opposti di un lungo manico.

Falce
"""""
.. figure:: Immagini/ManualeDiGioco/EquipFalce.png
    :width: 250
    :align: right

:Tipo di Arma: Da Guerra
:Classe: Da Mischia a Due Mani
:Costo: 18 mo
:Danni(P): 1d6
:Danni(M): 2d4
:Critico: x4
:Gittata: N/A
:Peso(M): 5 Kg
:Tipo di Danno: Penetrante o Tagliente
:Speciale: Sbilanciare_

Falcetto
""""""""

:Tipo di Arma: Semplice
:Classe: Da Mischia Leggera
:Costo: 6 mo
:Danni(P): 1d4
:Danni(M): 1d6
:Critico: x2
:Gittata: N/A
:Peso(M): 1 Kg
:Tipo di Danno: Tagliente
:Speciale: Sbilanciare_

Falchion
""""""""

:Tipo di Arma: Da Guerra
:Classe: Da Mischia a Due Mani
:Costo: 75 mo
:Danni(P): 1d6
:Danni(M): 2d4
:Critico: 18-20/x2
:Gittata: N/A
:Peso(M): 4 Kg
:Tipo di Danno: Tagliente
:Speciale: N/A

Falcione
""""""""
.. figure:: Immagini/ManualeDiGioco/EquipFalcione.png
    :width: 40
    :align: right

:Tipo di Arma: Da Guerra
:Classe: Da Mischia a Due Mani
:Costo: 8 mo
:Danni(P): 1d8
:Danni(M): 1d10
:Critico: x3
:Gittata: N/A
:Peso(M): 5 Kg
:Tipo di Danno: Tagliente
:Speciale: Portata_

Un falcione è una semplice lama, montata all'estremità di un'asta di circa 210 cm di lunghezza.

Fionda
""""""

:Tipo di Arma: Semplice
:Classe: A Distanza
:Costo: 1 ma
:Danni(P): 1d3
:Danni(M): 1d4
:Critico: x2
:Gittata: 15 m
:Peso(M): 2.5 Kg
:Tipo di Danno: Contundente
:Speciale: N/A

La fionda è un semplice pezzo di cuoio attaccato ad un paio di stringhe e, come per tutte le armi da lancio, si applica il bonus di Forza ai danni.
È possibile lanciare, ma non ricaricare, con una mano sola, ma caricare una fionda è un’:ref:`Azione di Movimento<azione_movimento>` che richiede due mani e provoca :ref:`Attacchi di Opportunità<attacco_opportunità>`.

.. Note::
    Si possono scagliare pietre con una fionda, ma le pietre non sono cosi compatte o rotonde come i proiettili, quindi l’attacco viene trattato come se l’arma fosse adatta a una creatura più piccola di una categoria di taglia e si subisce penalità –1 al tiro per colpire.

Frecce
""""""

:Tipo di Arma: Proiettile
:Costo: 1 mo
:Peso(M): 1.5
:Quantità: 20

Una freccia utilizzata come arma da mischia è considerata un’arma leggera improvvisata (penalità –4 al tiro per colpire) e infligge danni come un Pugnale_ della stessa taglia con critico ×2.

Le frecce sono trasportate in faretre di cuoio in grado di contenerne 20.

Frusta
""""""
.. figure:: Immagini/ManualeDiGioco/EquipFrusta.png
    :width: 200
    :align: right

:Tipo di Arma: Esotica
:Classe: A Mischia a Una Mano
:Costo: 1 mo
:Danni(P): 1d2
:Danni(M): 1d3
:Critico: x2
:Gittata: N/A
:Peso(M): 1 Kg
:Tipo di Danno: Tagliente
:Speciale: Disarmare_, `Non Letale`_, Portata_, Sbilanciare_

.. Warning::
    La frusta **non** infligge danni a creature che abbiano almeno bonus di armatura +1 o bonus di armatura naturale +3.

Una frusta è considerata un’arma da mischia con una portata di 4,5 metri, ma non si minaccia l’area in cui si può sferrare l’attacco e, diversamente da altre armi con portata, è possibile usarla contro qualsiasi nemico entro la portata, inclusi quelli adiacenti.
Usarla provoca :ref:`Attacchi di Opportunità<attacco_opportunità>`, come se si trattasse di un’arma a distanza.

È possibile usare :ref:`Arma Accurata<tal_arma_accurata>` per applicare il modificatore di Destrezza al posto del modificatore di Forza al tiro per colpire con una frusta della propria misura, anche quando essa non costituisce un’arma leggera.

Giavellotto
"""""""""""

:Tipo di Arma: Semplice
:Classe: A Distanza
:Costo: 1 mo
:Danni(P): 1d4
:Danni(M): 1d6
:Critico: x2
:Gittata: 9 m
:Peso(M): 1 Kg
:Tipo di Danno: Perforante
:Speciale: N/A

Quest’arma è una lancia leggera e flessibile concepita per essere lanciata.

Dal momento che non è concepita per l’uso in mischia, si è considerati come non competenti e si subisce penalità –4 al tiro per colpire se si utilizza in un attacco ravvicinato.

Giusarma
""""""""

:Tipo di Arma: Da Guerra
:Classe: Da Mischia a Due Mani
:Costo: 9 mo
:Danni(P): 1d6
:Danni(M): 2d4
:Critico: x3
:Gittata: N/A
:Peso(M): 3 Kg
:Tipo di Danno: Tagliente
:Speciale: Portata_, Sbilanciare_

Una giusarma è un’arma ad asta di 240 cm di lunghezza, con una lama ed un uncino montati in cima.

.. _guanto_darme:

Guanto d'Arme
"""""""""""""

:Tipo di Arma: Semplice
:Classe: Senz'Armi
:Costo: 2 mo
:Danni(P): 1d2
:Danni(M): 1d3
:Critico: x2
:Gittata: N/A
:Peso(M): 0.5 Kg
:Tipo di Danno: Contundente
:Speciale: N/A

Questi guanti metallici proteggono le mani e permettono di infliggere danni letali con colpi senz’armi. Un colpo con un guanto d’arme è altrimenti considerato un attacco senz’armi.

.. Note:: Il costo e il peso indicati sono per un singolo guanto d’arme.

Armature medie e pesanti (tranne la `Corazza di Piastre`_) comprendono i guanti d’arme.

Un avversario non può usare :ref:`Disarmare<azione_disarmare>` su un personaggio che indossa dei guanti d’arme.

Guanto d'Arme Chiodato
""""""""""""""""""""""

:Tipo di Arma: Semplice
:Classe: Da Mischia Leggera
:Costo: 5 mo
:Danni(P): 1d3
:Danni(M): 1d4
:Critico: x2
:Gittata: N/A
:Peso(M): 0.5 Kg
:Tipo di Danno: Perforante
:Speciale: N/A

.. Note:: Il costo e il peso indicati sono per un singolo guanto d’arme.

Un attacco con un guanto d’arme chiodato è considerato un attacco armato.

Un avversario non può usare :ref:`Disarmare<azione_disarmare>` su un personaggio che indossa dei guanti d’arme.

Kama
""""

.. figure:: Immagini/ManualeDiGioco/EquipKukri.png
    :width: 50
    :align: right

:Tipo di Arma: Esotica
:Classe: Da Mischia Leggera
:Costo: 2 mo
:Danni(P): 1d4
:Danni(M): 1d6
:Critico: x2
:Gittata: N/A
:Peso(M): 1 Kg
:Tipo di Danno: Tagliente
:Speciale: Monaco_, Sbilanciare_

Simile al Falcetto_, il kama è dotato di una lama corta e ricurva, montata su una semplice impugnatura.

Kukri
"""""

:Tipo di Arma: Da Guerra
:Classe: Da Mischia Leggera
:Costo: 8 mo
:Danni(P): 1d3
:Danni(M): 1d4
:Critico: 18-20/x2
:Gittata: N/A
:Peso(M): 1 Kg
:Tipo di Danno: Tagliente
:Speciale: N/A

Un pesante pugnale ricurvo di circa 30 cm.

Lancia
""""""

:Tipo di Arma: Semplice
:Classe: Da Mischia a Due Mani
:Costo: 2 mo
:Danni(P): 1d6
:Danni(M): 1d8
:Critico: x3
:Gittata: 6 m
:Peso(M): 3 Kg
:Tipo di Danno: Perforante
:Speciale: Puntare_

La lancia è un’arma di circa 1,5 m di lunghezza che può essere scagliata.

Lancia Corta
""""""""""""

:Tipo di Arma: Semplice
:Classe: Da Mischia a Una Mano
:Costo: 1 mo
:Danni(P): 1d4
:Danni(M): 1d6
:Critico: x2
:Gittata: 6 m
:Peso(M): 1.5 Kg
:Tipo di Danno: Perforante
:Speciale: N/A

È una Lancia_ di misura ridotta (circa 90 cm) adatta ad essere usata come arma da lancio.

Lancia da Cavaliere
"""""""""""""""""""
.. figure:: Immagini/ManualeDiGioco/EquipLanciaDaCavaliere.png
    :width: 40
    :align: right

:Tipo di Arma: Da Guerra
:Classe: Da Mischia a Due Mani
:Costo: 10 mo
:Danni(P): 1d6
:Danni(M): 1d8
:Critico: x3
:Gittata: N/A
:Peso(M): 5 Kg
:Tipo di Danno: Perforante
:Speciale: Portata_

Una lancia da cavaliere infligge danni raddoppiati quando viene utilizzata dalla sella di una cavalcatura in carica.

.. Nota::
    Mentre si è in sella si può imbracciarla con una mano.

Lancia Lunga
""""""""""""
.. figure:: Immagini/ManualeDiGioco/EquipLanciaLunga.png
    :width: 40
    :align: right

:Tipo di Arma: Semplice
:Classe: Da Mischia a Due Mani
:Costo: 5 mo
:Danni(P): 1d6
:Danni(M): 1d8
:Critico: x3
:Gittata: N/A
:Peso(M): 4.5 Kg
:Tipo di Danno: Perforante
:Speciale: Puntare_, Portata_

Una lancia lunga raggiunge i 2,4 m di lunghezza.

Manganello
""""""""""

:Tipo di Arma: Da Guerra
:Classe: Da Mischia Leggera
:Costo: 1 mo
:Danni(P): 1d4
:Danni(M): 1d6
:Critico: x2
:Gittata: N/A
:Peso(M): 1 Kg
:Tipo di Danno: Contundente
:Speciale: `Non Letale`_

Martello Leggero
""""""""""""""""

:Tipo di Arma: Da Guerra
:Classe: Da Mischia Leggera
:Costo: 1 mo
:Danni(P): 1d3
:Danni(M): 1d4
:Critico: x2
:Gittata: 6 m
:Peso(M): 1 Kg
:Tipo di Danno: Contundente
:Speciale: N/A

Martello da Guerra
""""""""""""""""""
.. figure:: Immagini/ManualeDiGioco/EquipMartelloDaGuerra.png
    :width: 150
    :align: right


:Tipo di Arma: Da Guerra
:Classe: Da Mischia a Una Mano
:Costo: 12 mo
:Danni(P): 1d6
:Danni(M): 1d8
:Critico: x3
:Gittata: N/A
:Peso(M): 2.5 Kg
:Tipo di Danno: Contundente
:Speciale: N/A


Martello-Picca Gnomesco
"""""""""""""""""""""""

:Tipo di Arma: Esotica
:Classe: Da Mischia a Due Mani
:Costo: 20 mo
:Danni(P): 1d6/1d4
:Danni(M): 1d8/1d6
:Critico: x3/x4
:Gittata: N/A
:Peso(M): 3 Kg
:Tipo di Danno: Contundente o Penetrante
:Speciale: Doppia_, Sbilanciare_

Un martello-picca gnomesco è un’arma doppia: un ingegnoso attrezzo con la testa di martello alla fine del manico e una picca ricurva sul lato opposto.
L’estremità contundente del martello è un’arma contundente che infligge 1d6 danni (critico ×3) mentre la picca è un’arma perforante che infligge 1d4 danni (critico ×4).

È possibile usare una delle due estremità come estremità primaria dell’arma. Gli :ref:`Gnomi` considerano il martello-picca gnomesco come un’arma da guerra.

.. _equip_mazza_leggera:

Mazza Leggera
"""""""""""""
.. figure:: Immagini/ManualeDiGioco/EquipMazza.png
    :width: 80
    :align: right

:Tipo di Arma: Semplice
:Classe: Da Mischia Leggera
:Costo: 5 mo
:Danni(P): 1d4
:Danni(M): 1d6
:Critico: x2
:Gittata: N/A
:Peso(M): 2 Kg
:Tipo di Danno: Contundente
:Speciale: N/A

Una mazza è costituita da una testa di metallo ornata, attaccata ad un manico di legno o di metallo di varie dimensioni e peso.

Mazza Pesante
"""""""""""""

:Tipo di Arma: Semplice
:Classe: Da Mischia a Una Mano
:Costo: 12 mo
:Danni(P): 1d6
:Danni(M): 1d8
:Critico: x2
:Gittata: N/A
:Peso(M): 4 Kg
:Tipo di Danno: Contundente
:Speciale: N/A

Una mazza è costituita da una testa di metallo ornata, attaccata ad un manico di legno o di metallo di varie dimensioni e peso.

Mazzafrusto
"""""""""""

:Tipo di Arma: Da Guerra
:Classe: Da Mischia a Una Mano
:Costo: 8 mo
:Danni(P): 1d6
:Danni(M): 1d8
:Critico: x2
:Gittata: N/A
:Peso(M): 2.5 Kg
:Tipo di Danno: Contundente
:Speciale: Disarmare_, Sbilanciare_

Un mazzafrusto consiste in una sfera chiodata di metallo appesa ad una robusta catena che pende da un grosso manico, di varie dimensioni e peso.

Mazzafrusto Doppio
""""""""""""""""""

.. figure:: Immagini/ManualeDiGioco/EquipMazzafrusto.png
    :width: 150
    :align: right

:Tipo di Arma: Esotiche
:Classe: Da Mischia a Due Mani
:Costo: 90 mo
:Danni(P): 1d6/1d6
:Danni(M): 1d8/1d8
:Critico: x2
:Gittata: N/A
:Peso(M): 5 Kg
:Tipo di Danno: Contundente
:Speciale: Disarmare_, Doppia_, Sbilanciare_

Il mazzafrusto doppio consiste in due sfere chiodate di metallo appese a delle catene che pendono da una delle estremità del lungo manico.

Mazzafrusto Pesante
"""""""""""""""""""

:Tipo di Arma: Da Guerra
:Classe: Da Mischia a Due Mani
:Costo: 15 mo
:Danni(P): 1d8
:Danni(M): 1d10
:Critico: 19-20/x2
:Gittata: N/A
:Peso(M): 5 Kg
:Tipo di Danno: Contundente
:Speciale: Disarmare_, Sbilanciare_

Un mazzafrusto consiste in una sfera chiodata di metallo appesa ad una robusta catena che pende da un grosso manico, di varie dimensioni e peso.

Morning Star
""""""""""""
.. figure:: Immagini/ManualeDiGioco/EquipMorningStar.png
    :width: 80
    :align: right

:Tipo di Arma: Semplice
:Classe: Da Mischia a Una Mano
:Costo: 8 mo
:Danni(P): 1d6
:Danni(M): 1d8
:Critico: x2
:Gittata: N/A
:Peso(M): 3 Kg
:Tipo di Danno: Contundente e Penetrante
:Speciale: N/A

Quest’arma è una sfera di metallo chiodato fissata in cima ad un lungo manico.

Nunchaku
""""""""
.. figure:: Immagini/ManualeDiGioco/EquipNunchaku.png
    :width: 50
    :align: right

:Tipo di Arma: Esotica
:Classe: Da Mischia Leggera
:Costo: 2 mo
:Danni(P): 1d4
:Danni(M): 1d6
:Critico: x2
:Gittata: N/A
:Peso(M): 1 Kg
:Tipo di Danno: Contundente
:Speciale: Disarmare_, Monaco_

Il nunchaku è un’arma fatta di due barre di legno o metallo legate insieme da una corda o catena sottile.

Piccone Leggero
"""""""""""""""

:Tipo di Arma: Da Guerra
:Classe: Da Mischia Leggera
:Costo: 4 mo
:Danni(P): 1d3
:Danni(M): 1d4
:Critico: x4
:Gittata: N/A
:Peso(M): 1.5 Kg
:Tipo di Danno: Penetrante
:Speciale: N/A

Piccone Pesante
"""""""""""""""

:Tipo di Arma: Da Guerra
:Classe: Da Mischia a Una Mano
:Costo: 8 mo
:Danni(P): 1d4
:Danni(M): 1d6
:Critico: x4
:Gittata: N/A
:Peso(M): 3 Kg
:Tipo di Danno: Penetrante
:Speciale: N/A

Proiettili per Fionda
"""""""""""""""""""""

:Tipo di Arma: Proiettile
:Costo: 1 ma
:Peso(M): 2.5 Kg
:Quantità: 10

I proiettili per Fionda_ o per il `Bastone Fionda Halfling`_ sono delle semplici sfere di piombo.

Sono trasportati in borse di cuoio in grado di contenerne 10.

Pugnale
"""""""
.. figure:: Immagini/ManualeDiGioco/EquipPugnale.png
    :width: 40
    :align: right

:Tipo di Arma: Semplice
:Classe: Da Mischia Leggera
:Costo: 2 mo
:Danni(P): 1d3
:Danni(M): 1d4
:Critico: 19-20/x2
:Gittata: 3 m
:Peso(M): 0.5 Kg
:Tipo di Danno: Penetrante o Tagliente
:Speciale: N/A

Il pugnale è dotato di una lama di circa 30 cm di lunghezza.

Se lo si vuole nascondere su di sé, si applica bonus +2 alle prove di :ref:`Rapidità di Mano<rapidità_di_mano>`.

Pugnale da Mischia
""""""""""""""""""

:Tipo di Arma: Semplice
:Classe: Da Mischia Leggera
:Costo: 2 mo
:Danni(P): 1d3
:Danni(M): 1d4
:Critico: x3
:Gittata: N/A
:Peso(M): 0.5 Kg
:Tipo di Danno: Penetrante
:Speciale: N/A

Questo pugnale ha la lama attaccata ad un’impugnatura orizzontale che fuoriesce dalla mano quando impugnato.

Quadrelli
"""""""""
.. figure:: Immagini/ManualeDiGioco/EquipQuadrelliDaBalestra.png
    :width: 150
    :align: right

:Tipo di Arma: Proiettile
:Costo: 1 mo
:Peso(M): 0.5 Kg
:Quantità: 10 (5 a Ripetizione)

Un quadrello da balestra usato come arma da mischia è considerato un’arma leggera improvvisata (penalità –4 al tiro per colpire) e infligge danni come un Pugnale_ della stessa taglia con critico ×2.

I quadrelli sono trasportati in astucci di legno in grado di contenerne 10 (5 per le balestre a ripetizione).

Randello
""""""""
.. figure:: Immagini/ManualeDiGioco/EquipRandello.png
    :width: 60
    :align: right

:Tipo di Arma: Semplice
:Classe: Da Mischia a Una Mano
:Costo: N/A
:Danni(P): 1d4
:Danni(M): 1d6
:Critico: x2
:Gittata: 3 m
:Peso(M): 1.5 Kg
:Tipo di Danno: Contundente
:Speciale: N/A

Randello Pesante
""""""""""""""""

:Tipo di Arma: Da Guerra
:Classe: Da Mischia a Due Mani
:Costo: 5 mo
:Danni(P): 1d8
:Danni(M): 1d10
:Critico: x2
:Gittata: N/A
:Peso(M): 4 Kg
:Tipo di Danno: Contundente
:Speciale: N/A

Rete
""""

:Tipo di Arma: Esotica
:Classe: A Distanza
:Costo: 20 mo
:Danni(P): N/A
:Danni(M): N/A
:Critico: N/A
:Gittata: 3 m
:Peso(M): 3 Kg
:Tipo di Danno: N/A
:Speciale: N/A

Una rete si usa per intralciare gli avversari: quando la si lancia, si compie un :ref:`Attacco di Contatto a Distanza<azione_attacco_di_contatto_a_distanza>` contro il bersaglio con gittata massima di 3 metri.
Se colpisce, il bersaglio è :ref:`Intralciato<intralciato>` e subisce penalità –2 al tiro per colpire e –4 alla Destrezza. La creatura intralciata può muoversi solo a velocità dimezzata e non può :ref:`Caricare<azione_carica>` o :ref:`Correre<azione_correre>`.

Se si controlla la corda regolabile, superando una prova di Forza contrapposta mentre la si tiene la creatura intralciata può muoversi solo entro i limiti consentiti dalla corda.

In caso la creatura intralciata tenti di lanciare un incantesimo, deve effettuare con successo una prova di concentrazione con *CD = 15 + Livello Incantesimo* o fallisce.

La creatura intralciata può liberarsi con una prova di :ref:`Artista della Fuga<artista_della_fuga>` con CD 20 in un’:ref:`Azione di Round Completo<azione_di_round_completo>`.
La rete ha 5 punti ferita e può essere sfondata con una prova di Forza con CD 25, anch'essa :ref:`Azione di Round Completo<azione_di_round_completo>`.

.. Warning::
    La rete è utile solo contro creature entro una categoria di taglia di differenza.

Una rete deve essere piegata per essere lanciata efficacemente e la prima volta che si lancia in combattimento, si effettua un normale tiro per colpire di contatto a distanza.
Una volta spiegata, si subisce penalità –4 al tiro per colpire e per piegarla occorrono 2 round a un personaggio competente e il doppio a uno non competente.

Sai
"""

:Tipo di Arma: Esotica
:Classe: Da Mischia Leggera
:Costo: 1 mo
:Danni(P): 1d3
:Danni(M): 1d4
:Critico: x2
:Gittata: N/A
:Peso(M): 0.5 Kg
:Tipo di Danno: Contundente
:Speciale: Disarmare_, Monaco_

Le dentellature, simili a quelle di una forca, lo rendono particolarmente adatto a bloccare l’arma dell’avversario per disarmarlo. Inoltre, con un sai si ottiene bonus +4 alla prova di manovra in combattimento per :ref:`Spezzare<azione_spezzare>` l’arma di un avversario.

Anche se a punta, un sai è usato principalmente per colpire gli avversari in modo contundente o per :ref:`Disarmare<azione_disarmare>`.

Sciabola Elfica
"""""""""""""""

:Tipo di Arma: Esotica
:Classe: Da Mischia a Due Mani
:Costo: 80 mo
:Danni(P): 1d8
:Danni(M): 1d10
:Critico: 18-20/x2
:Gittata: N/A
:Peso(M): 3.5 Kg
:Tipo di Danno: Tagliente
:Speciale: N/A

Essenzialmente una versione più lunga della Scimitarra_, ma con una lama più sottile, la sciabola elfica è molto rara.

Si riceve bonus di circostanza +2 alla propria DMC (Difesa da Manovra in Combattimento) ogni volta che un nemico cerca di :ref:`Spezzare<azione_spezzare>` la sciabola elfica grazie alla flessibilità del metallo.

.. Note::
    È possibile utilizzare il talento :ref:`Arma Accurata<tal_arma_accurata>` per applicare il modificatore di Destrezza al posto del modificatore di Forza al tiro per colpire con una sciabola elfica della propria misura, anche se non è considerata un’arma leggera.

Scimitarra
""""""""""
.. figure:: Immagini/ManualeDiGioco/EquipScimitarra.png
    :width: 60
    :align: right

:Tipo di Arma: Da Guerra
:Classe: Da Mischia a Una Mani
:Costo: 15 mo
:Danni(P): 1d4
:Danni(M): 1d6
:Critico: 18-20/x2
:Gittata: N/A
:Peso(M): 2 Kg
:Tipo di Danno: Tagliente
:Speciale: N/A

Scudo Chiodato Leggero
""""""""""""""""""""""

:Tipo di Arma: Da Guerra
:Classe: Da Mischia Leggera
:Costo: Speciale
:Danni(P): 1d3
:Danni(M): 1d4
:Critico: x2
:Gittata: N/A
:Peso(M): Speciale
:Tipo di Danno: Penetrante
:Speciale: N/A

È possibile colpire con lo scudo invece di utilizzarlo per difendersi.

Vedi le voci `Scudo Leggero di Legno`_, `Scudo Leggero di Metallo`_ e `Chiodatura per Scudo`_ per informazioni su costo e peso.

Scudo Chiodato Pesante
""""""""""""""""""""""

:Tipo di Arma: Da Guerra
:Classe: Da Mischia a Una Mano
:Costo: Speciale
:Danni(P): 1d4
:Danni(M): 1d6
:Critico: x2
:Gittata: N/A
:Peso(M): Speciale
:Tipo di Danno: Penetrante
:Speciale: N/A

È possibile colpire con lo scudo invece di utilizzarlo per difendersi.

Vedi le voci `Scudo Pesante di Legno`_, `Scudo Pesante di Metallo`_ e `Chiodatura per Scudo`_ per informazioni su costo e peso.

Scudo Leggero
"""""""""""""

:Tipo di Arma: Da Guerra
:Classe: Da Mischia Leggera
:Costo: Speciale
:Danni(P): 1d2
:Danni(M): 1d3
:Critico: x2
:Gittata: N/A
:Peso(M): Speciale
:Tipo di Danno: Contundente
:Speciale: N/A

È possibile colpire con lo scudo invece di utilizzarlo per difendersi.

Vedi la voce `Scudo Leggero di Legno`_ o `Scudo Leggero di Metallo`_ anche per informazioni su costo e peso.

Scudo Pesante
"""""""""""""

:Tipo di Arma: Da Guerra
:Classe: Da Mischia a Una Mano
:Costo: Speciale
:Danni(P): 1d3
:Danni(M): 1d4
:Critico: x2
:Gittata: N/A
:Peso(M): Speciale
:Tipo di Danno: Contundente
:Speciale: N/A

È possibile colpire con lo scudo invece di utilizzarlo per difendersi.

Vedi la voce `Scudo Pesante di Legno`_ o `Scudo Pesante di Metallo`_ anche per informazioni su costo e peso.

Shuriken
""""""""

:Tipo di Arma: Esotica
:Classe: A Distanza
:Costo: 1 mo (x5)
:Danni(P): 1
:Danni(M): 1d2
:Critico: x2
:Gittata: 3 m
:Peso(M): 0.25 Kg (x5)
:Tipo di Danno: Penetrante
:Speciale: N/A

Lo shuriken è un piccolo pezzo di metallo con le estremità affilate, ideato per essere lanciato, quindi non può essere usato come arma in mischia.

Sebbene appartengano alla categoria delle armi da lancio, gli shuriken vengono considerati munizioni per quanto riguarda i tempi di estrazione, la creazione di `Armi Perfette`_ o altre versioni speciali delle stesse e quel che succede dopo che sono stati lanciati.

Siangham
""""""""

:Tipo di Arma: Esotica
:Classe: Da Mischia Leggera
:Costo: 3 mo
:Danni(P): 1d4
:Danni(M): 1d6
:Critico: x2
:Gittata: N/A
:Peso(M): 0.5 Kg
:Tipo di Danno: Penetrante
:Speciale: Monaco_

Il siangham è un’arma simile ad una freccia usato per pugnalare i nemici.

Spada a Due Lame
""""""""""""""""

:Tipo di Arma: Esotica
:Classe: Da Mischia a Due Mani
:Costo: 100 mo
:Danni(P): 1d6/1d6
:Danni(M): 1d8/1d8
:Critico: 19-20/x2
:Gittata: N/A
:Peso(M): 5 Kg
:Tipo di Danno: Tagliente
:Speciale: Doppia_

Una spada a due lame è un’arma doppia con lame gemelle che si estendono dai lati opposti di una corta impugnatura centrale, permettendo a chi la impugna di attaccare con grazia letale.

Spada Bastarda
""""""""""""""
.. figure:: Immagini/ManualeDiGioco/EquipSpadaBastarda.png
    :width: 100
    :align: right

:Tipo di Arma: Esotica
:Classe: Da Mischia a Una Mani
:Costo: 35 mo
:Danni(P): 1d8
:Danni(M): 1d10
:Critico: 19-20/x2
:Gittata: N/A
:Peso(M): 3 Kg
:Tipo di Danno: Tagliente
:Speciale: N/A

Le spade bastarde sono lunghe circa 120 cm, il che le rende troppo grandi per essere usate ad una mano senza uno speciale addestramento, quindi sono considerate armi esotiche.

.. Note::
    Si può usare una spada bastarda con due mani come arma da guerra.

.. _equip_spada_corta:

Spada Corta
"""""""""""
.. figure:: Immagini/ManualeDiGioco/EquipSpadaCorta.png
    :width: 60
    :align: right

:Tipo di Arma: Da Guerra
:Classe: Da Mischia Leggera
:Costo: 10 mo
:Danni(P): 1d4
:Danni(M): 1d6
:Critico: 19-20/x2
:Gittata: N/A
:Peso(M): 1 Kg
:Tipo di Danno: Perforante
:Speciale: N/A

Questa spada è lunga circa 60 cm.

Spada Lunga
"""""""""""
.. figure:: Immagini/ManualeDiGioco/EquipSpadaLunga.png
    :width: 70
    :align: right

:Tipo di Arma: Da Guerra
:Classe: Da Mischia a Una Mano
:Costo: 15 mo
:Danni(P): 1d6
:Danni(M): 1d8
:Critico: 19-20/x2
:Gittata: N/A
:Peso(M): 2 Kg
:Tipo di Danno: Tagliente
:Speciale: N/A

Questa spada è lunga circa 105 cm.

Spadone
"""""""

:Tipo di Arma: Da Guerra
:Classe: Da Mischia a Due Mani
:Costo: 50 mo
:Danni(P): 1d10
:Danni(M): 2d6
:Critico: 19-20/x2
:Gittata: N/A
:Peso(M): 4 Kg
:Tipo di Danno: Tagliente
:Speciale: N/A

Questa immensa spada a due mani è lunga circa 150 cm.

Stocco
""""""
.. figure:: Immagini/ManualeDiGioco/EquipStocco.png
    :width: 70
    :align: right

:Tipo di Arma: Da Guerra
:Classe: Da Mischia a Una Mano
:Costo: 20 mo
:Danni(P): 1d4
:Danni(M): 1d6
:Critico: 18-20/x2
:Gittata: N/A
:Peso(M): 1 Kg
:Tipo di Danno: Penetrante
:Speciale: N/A

È possibile utilizzare il :ref:`Arma Accurata<tal_arma_accurata>` per applicare il modificatore di Destrezza al posto del modificatore di Forza al tiro per colpire con uno stocco della propria misura, anche se non è un’arma leggera.

.. Warning::
    Non si può utilizzare lo stocco con due mani per aggiungere una volta e mezzo il bonus di Forza ai danni.

Tridente
""""""""
.. figure:: Immagini/ManualeDiGioco/EquipTridente.png
    :width: 80
    :align: right

:Tipo di Arma: Da Guerra
:Classe: Da Mischia a Una Mano
:Costo: 15 mo
:Danni(P): 1d6
:Danni(M): 1d8
:Critico: x2
:Gittata: 3 m
:Peso(M): 2 Kg
:Tipo di Danno: Penetrante
:Speciale: Puntare_

Quest’arma perforante di metallo è dotata di tre punte alla fine di un’asta lunga circa 120 cm.

.. Note::
    Il tridente può essere lanciato.

Urgrosh Nanico
""""""""""""""

:Tipo di Arma: Da Guerra
:Classe: Da Mischia a Due Mani
:Costo: 50 mo
:Danni(P): 1d6/1d4
:Danni(M): 1d8/1d6
:Critico: x3
:Gittata: N/A
:Peso(M): 6 Kg
:Tipo di Danno: Penetrante o Tagliente
:Speciale: Doppia_, Puntare_

Un urgrosh nanico è un’arma doppia con una testa d’ascia e una punta di lancia alle estremità opposte di un lungo manico.

L’estremità ascia è un’arma tagliente che infligge 1d8 danni, mentre quella lancia è un’arma perforante che infligge 1d6 danni. Si possono usare
entrambe le estremità come estremità primaria dell’arma. L’altra conta come arma secondaria.

.. Note::
    Se si utilizza l’urgrosh nanico contro una :ref:`Carica<azione_carica>`, l’estremità lancia è la parte dell’arma che infligge i danni.

I :ref:`Nani` considerano l’urgrosh nanico un’arma da guerra.

.. _equip_armi_perfette:

Armi Perfette
-------------

Un’arma perfetta è una versione di ottima fattura di un’arma normale. Impugnarne una permette di aggiungere bonus di potenziamento +1 al tiro per colpire.

Un’arma non può ottenere una qualità perfetta dopo che è stata creata, ma deve essere forgiata come tale già dall’inizio (vedi :ref:`Artigianato<artigianato>`).
La qualità perfetta aggiunge 300 mo al prezzo dell’arma normale (o 6 mo per ogni singola munizione, come una freccia) e per un’arma doppia significa raddoppiare il costo aggiuntivo dell’arma (+600 mo).

.. Warning::
    Le munizioni perfette vengono danneggiate (effettivamente distrutte) quando le si usa e il bonus di potenziamento di una munizione perfetta non è cumulativo con il bonus di potenziamento dell’arma che ne fa uso.

Tutte le armi magiche sono automaticamente considerate perfette, ma il bonus di potenziamento dovuto alla qualità di arma perfetta non è cumulativo con il bonus di potenziamento derivato dalla magia dell’arma.

Sebbene alcune armature e scudi possano essere utilizzati come armi, non è possibile creare versioni perfette di questi oggetti che aggiungano un bonus di potenziamento al tiro per colpire. Le versioni perfette di armature e scudi beneficiano
invece di una riduzione alle penalità di armatura alla prova (vedi `Armature Perfette`_).

.. _armature:		
.. _equip_armature:

Armature
========

Il tipo di armatura indossato è per lo più un modo per proteggersi dai pericoli.

Molti personaggi possono indossare solo le armature più semplici, e solo alcuni possono usare gli scudi.
Per indossare armature più pesanti efficacemente si può selezionare i :ref:`Competenza nelle Armature<tal_competenza_nelle_armature>`, ma molte classi sono automaticamente competenti nelle armature che funzionano meglio per loro.

.. contents:: Contenuti
    :local:
    :depth: 3

Proprietà
---------
Questo è il formato per le voci delle armature, così come indicate in :ref:`Tabella<tabella_armature>`.

Costo
^^^^^
Il costo dell’armatura in mo per creature umanoidi di taglia Piccola e Media, ma per le altre taglie è necessario consultare la relativa :ref:`Tabella<tabella_armature_insolite>`.

Bonus di Armatura/Scudo
^^^^^^^^^^^^^^^^^^^^^^^
Le armature forniscono un bonus di armatura alla CA, mentre gli scudi forniscono un bonus di scudo alla CA.

.. Warning::
    Il bonus di armatura fornito da un’armatura non è cumulativo con altri effetti e oggetti che conferiscono un bonus di armatura e allo stesso modo il bonus di scudo fornito da uno scudo non è cumulativo con altri effetti che conferiscono un bonus di scudo.

Bonus Des Max
^^^^^^^^^^^^^^^^^^^^^^^
Questo valore è il bonus di Destrezza massima alla CA che questo tipo di armatura permette.
I bonus di Destrezza oltre questo valore sono ridotti a questo valore al fine di determinare la CA di chi indossa l’armatura.

Le armature più pesanti limitano la mobilità, riducendo la capacità di schivare colpi, ma questa restrizione non si applica ad altre capacità legate alla Destrezza.

.. Note::
    Anche se il bonus di Destrezza di un personaggio scende a 0 a causa dell’armatura, non si considera che abbia perso il bonus di Destrezza alla CA.

Anche l’ingombro del personaggio (l’equipaggiamento che porta con sé, compresa l’armatura) può abbassare il bonus di Destrezza massimo che viene applicato alla CA.

Scudi
    Gli scudi non influenzano il bonus di Destrezza massima, tranne gli :ref:`Scudi Torre<equip_scudo_torre>`.

Penalità di Armatura alla Prova
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Qualsiasi armatura più pesante di quella di Cuoio_, così come qualsiasi scudo, danneggia la capacità di utilizzare certe abilità basate su Forza e Destrezza.
La penalità di armatura alla prova è la penalità che si applica alle prove di abilità basate su Forza e Destrezza mentre si indossa un certo tipo di armatura.

.. Note::
    Anche l’ingombro del personaggio può comportare una penalità di armatura alla prova.

Scudi
    Se si indossa l’armatura e si utilizza lo scudo, si applicano entrambe le penalità.

Non Competente con l’Armatura
"""""""""""""""""""""""""""""
Se si indossa un’armatura o si utilizza uno scudo in cui non si è competenti, si subisce la penalità di armatura anche alla prova al tiro per colpire.

Questa penalità è cumulativa per armatura e scudo.

Dormire in Armatura
""""""""""""""""""""
Se si dorme in un’armatura media o pesante, il giorno seguente si è automaticamente :ref:`Affaticati<affaticato>`: si subisce penalità –2 a Forza e Destrezza e non si può :ref:`azione_carica` o :ref:`azione_correre`.

Dormire in un’armatura leggera non provoca affaticamento.

Fallimento Incantesimi Arcani
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
L’armatura interferisce con i gesti che devono essere compiuti per lanciare un incantesimo arcano che ha una componente somatica quindi gli incantatori arcani affrontano la possibilità di fallimento degli incantesimi arcani se indossano un’armatura.

.. Note::
    I :ref:`Bardi<bardo>` possono indossare armature leggere e usare scudi senza subire la possibilità di fallire incantesimi arcani quando lanciano incantesimi da bardo.

Scudi
    Se si indossa un’armatura e si utilizza uno scudo, si devono **sommare** i due valori per avere una singola probabilità di fallimento.

Lanciare Incantesimi con l'Armatura
"""""""""""""""""""""""""""""""""""
Quando si lancia un incantesimo arcano indossando un’armatura, si deve effettuare un tiro di fallimento degli incantesimi arcani, a meno che l’incantesimo sia privo della componente somatica.

Velocità
^^^^^^^^
Le armature medie e pesanti rallentano i personaggi.

:ref:`Umani`, :ref:`Elfi`, :ref:`Mezzelfi` e :ref:`Mezzorchi` hanno una velocità senza ingombro di 9 metri, quindi utilizzano la prima colonna.
:ref:`Nani`, :ref:`Gnomi` e :ref:`Halfling` hanno una velocità senza ingombro di 6 metri, quindi utilizzano la seconda.

.. Note::
    La velocità sul terreno dei :ref:`Nani` rimane, sempre di 6 metri anche quando indossano un’armatura media o pesante e quando hanno un carico medio o pesante.

Scudi
    Gli scudi non modificano la velocità.

Peso
^^^^
Il peso indicato si riferisce alla versione per personaggi di taglia Media. Le armature adattate per personaggi di taglia Piccola pesano la metà, mentre per quelli di taglia Grande il doppio.

Descrizioni delle Armature
--------------------------

Di seguito vengono descritte ed elencate le armature disponibili. 

.. _tabella_armature:
.. _equip_tabella_armature:

Tabella Armature
^^^^^^^^^^^^^^^^^^^^^

+------------------+-------------------------------+--------+----------+---------------+---------------------+-----------------+---------------+---------------+---------+
| Classe           | Nome                          | Costo  | Bonus CA | Bonus Dex Max | Penalità alla prova | Fallimento inc. | Velocità (9m) | Velocità (6m) | Peso    |
+==================+===============================+========+==========+===============+=====================+=================+===============+===============+=========+
| Armature Leggere |                               |        |          |               |                     |                 |               |               |         |
+------------------+-------------------------------+--------+----------+---------------+---------------------+-----------------+---------------+---------------+---------+
|                  | Imbottita_                    | 5mo    | +1       | +8            | 0                   | 5%              | 9m            | 6m            | 5Kg     |
+------------------+-------------------------------+--------+----------+---------------+---------------------+-----------------+---------------+---------------+---------+
|                  | Cuoio_                        | 10mo   | +2       | +6            | 0                   | 10%             | 9m            | 6m            | 7.5Kg   |
+------------------+-------------------------------+--------+----------+---------------+---------------------+-----------------+---------------+---------------+---------+
|                  | `Cuoio Borchiato`_            | 25mo   | +3       | +5            | -1                  | 15%             | 9m            | 6m            | 10Kg    |
+------------------+-------------------------------+--------+----------+---------------+---------------------+-----------------+---------------+---------------+---------+
|                  | `Giaco di Maglia`_            | 100mo  | +4       | +4            | -2                  | 20%             | 9m            | 6m            | 12.5Kg  |
+------------------+-------------------------------+--------+----------+---------------+---------------------+-----------------+---------------+---------------+---------+
| Armature Medie   |                               |        |          |               |                     |                 |               |               |         |
+------------------+-------------------------------+--------+----------+---------------+---------------------+-----------------+---------------+---------------+---------+
|                  | Pelle_                        | 15mo   | +4       | +4            | -3                  | 20%             | 6m            | 4.5m          | 12.5Kg  |
+------------------+-------------------------------+--------+----------+---------------+---------------------+-----------------+---------------+---------------+---------+
|                  | `Corazza a Scaglie`_          | 50mo   | +5       | +3            | -4                  | 25%             | 6m            | 4.5m          | 15Kg    |
+------------------+-------------------------------+--------+----------+---------------+---------------------+-----------------+---------------+---------------+---------+
|                  | `Cotta di Maglia`_            | 150mo  | +6       | +2            | -5                  | 30%             | 6m            | 4.5m          | 20Kg    |
+------------------+-------------------------------+--------+----------+---------------+---------------------+-----------------+---------------+---------------+---------+
|                  | `Corazza di Piastre`_         | 200mo  | +6       | +3            | -4                  | 25%             | 6m            | 4.5m          | 15Kg    |
+------------------+-------------------------------+--------+----------+---------------+---------------------+-----------------+---------------+---------------+---------+
| Armature Pesanti |                               |        |          |               |                     |                 |               |               |         |
+------------------+-------------------------------+--------+----------+---------------+---------------------+-----------------+---------------+---------------+---------+
|                  | `Corazza a Strisce`_          | 200mo  | +7       | +0            | -7                  | 40%             | 6m*           | 4.5m*         | 22.5Kg  |
+------------------+-------------------------------+--------+----------+---------------+---------------------+-----------------+---------------+---------------+---------+
|                  | `Corazza di Bande`_           | 250mo  | +7       | +1            | -6                  | 35%             | 6m*           | 4.5m*         | 17.5Kg  |
+------------------+-------------------------------+--------+----------+---------------+---------------------+-----------------+---------------+---------------+---------+
|                  | `Mezza Armatura`_             | 600mo  | +8       | +0            | -7                  | 40%             | 6m*           | 4.5m*         | 25Kg    |
+------------------+-------------------------------+--------+----------+---------------+---------------------+-----------------+---------------+---------------+---------+
|                  | `Armatura Completa`_          | 1500mo | +9       | +1            | -6                  | 35%             | 6m*           | 4.5m*         | 25Kg    |
+------------------+-------------------------------+--------+----------+---------------+---------------------+-----------------+---------------+---------------+---------+
| Scudi            |                               |        |          |               |                     |                 |               |               |         |
+------------------+-------------------------------+--------+----------+---------------+---------------------+-----------------+---------------+---------------+---------+
|                  | Buckler_                      | 15mo   | +1       |               | -1                  | 5%              |               |               | 2.5Kg   |
+------------------+-------------------------------+--------+----------+---------------+---------------------+-----------------+---------------+---------------+---------+
|                  | `Scudo Leggero di Legno`_     | 3mo    | +1       |               | -1                  | 5%              |               |               | 2.5Kg   |
+------------------+-------------------------------+--------+----------+---------------+---------------------+-----------------+---------------+---------------+---------+
|                  | `Scudo Leggero di Metallo`_   | 9mo    | +1       |               | -1                  | 5%              |               |               | 3Kg     |
+------------------+-------------------------------+--------+----------+---------------+---------------------+-----------------+---------------+---------------+---------+
|                  | `Scudo Pesante di Legno`_     | 7mo    | +2       |               | -2                  | 15%             |               |               | 5Kg     |
+------------------+-------------------------------+--------+----------+---------------+---------------------+-----------------+---------------+---------------+---------+
|                  | `Scudo Pesante di Metallo`_   | 20mo   | +2       |               | -2                  | 15%             |               |               | 7.5Kg   |
+------------------+-------------------------------+--------+----------+---------------+---------------------+-----------------+---------------+---------------+---------+
|                  | `Scudo Torre`_                | 30mo   | +4       | +2            | -10                 | 50%             |               |               | 22.5Kg  |
+------------------+-------------------------------+--------+----------+---------------+---------------------+-----------------+---------------+---------------+---------+
| Extra            |                               |        |          |               |                     |                 |               |               |         |
+------------------+-------------------------------+--------+----------+---------------+---------------------+-----------------+---------------+---------------+---------+
|                  | `Chiodatura per Armatura`_    | +50mo  |          |               |                     |                 |               |               | +5 Kg   |
+------------------+-------------------------------+--------+----------+---------------+---------------------+-----------------+---------------+---------------+---------+
|                  | `Chiodatura per Scudo`_       | +10mo  |          |               |                     |                 |               |               | +2.5 Kg |
+------------------+-------------------------------+--------+----------+---------------+---------------------+-----------------+---------------+---------------+---------+
|                  | `Guanto d'Arme con Chiusura`_ | 8mo    |          |               | Speciale            | Speciale        |               |               | +2.5 Kg |
+------------------+-------------------------------+--------+----------+---------------+---------------------+-----------------+---------------+---------------+---------+

.. Note::
    Quando si :ref:`corre<azione_correre>` con un'armatura pesante ci si muove solo al triplo della velocità, non al quadruplo.

Armatura Completa
^^^^^^^^^^^^^^^^^
.. figure:: Immagini/ManualeDiGioco/EquipArmaturaCompleta.png
    :width: 200
    :align: right

:Classe: Armatura Pesante
:Bonus CA: +9
:Bonus Dex Massimo: +1
:Costo: 1500 mo
:Fallimento Incantesimi: 35%
:Penalità alla Prova: -6
:Peso: 25 Kg
:Velocità 9m: 6m
:Velocità 6m: 4.5m

Questa armatura comprende :ref:`Guanti d’Arme<guanto_darme>`, pesanti calzari di cuoio, un elmo con visiera e uno spesso strato di imbottitura che si indossa sotto.

.. Important::
    Ogni armatura completa deve essere adattata al suo proprietario da un esperto fabbricante di armature, anche se un’armatura conquistata può essere modificata per un nuovo possessore a un costo variabile tra le 200 e le 800 (2d4 × 100) monete d’oro.

Buckler
^^^^^^^
.. figure:: Immagini/ManualeDiGioco/EquipBuckler.png
    :width: 150
    :align: right

:Classe: Scudo
:Bonus CA: +1
:Bonus Des Max: N/A
:Costo: 15 mo
:Penalità alla Prova: -1
:Fallimento Incantesimi: 5%
:Peso(M): 2.5 Kg

Questo piccolo scudo di metallo è attaccato all’avambraccio con delle cinghie.

Si può usare un arco o una balestra senza penalità mentre lo si porta ed è anche possibile utilizzare il braccio protetto dallo scudo per impugnare un’arma, comprese quelle secondarie o a due mani, ma si subisce penalità –1 al tiro per colpire a causa del peso extra sul braccio.
Questa penalità si somma a quelle per il combattimento con l’arma secondaria o per il combattimento con due armi. I

.. Warning::
    se si usa un’arma nella mano secondaria, non si ottiene il bonus alla CA del buckler per il resto del round così come quando si lancia un incantesimo con componenti somatiche usando il braccio con lo scudo.

.. Note::
    Non si può effettuare un attacco con lo scudo con il buckler.

Chiodatura per Armatura
^^^^^^^^^^^^^^^^^^^^^^^

:Classe: Extra
:Costo: +50 mo
:Penalità alla Prova: N/A
:Fallimento Incantesimi: N/A
:Peso(M): +5 Kg

È possibile aggiungere chiodature all’armatura che permettono di infliggere :ref:`danni perforanti aggiuntivi<armatura_chiodata>` con un attacco in :ref:`Lotta<azione_lottare>` riuscito.

Le chiodature contano come arma da guerra e se non si è competenti nel loro uso, si subisce penalità –4 alle prove di :ref:`Lottare<azione_lottare>` quando si tenta di usarle.

È anche possibile compiere un normale attacco in mischia (o un attacco con mano secondaria) con le chiodature, e in questo caso devono essere considerate come armi leggere, ma non è possibile farlo se si è già effettuato un attacco con un’altra arma secondaria, e viceversa.

Un bonus di potenziamento su un’`Armatura Completa`_ non migliora l’efficacia delle chiodature, ma le chiodature stesse possono essere rese armi magiche.

Chiodatura per Scudo
^^^^^^^^^^^^^^^^^^^^

:Classe: Extra
:Costo: +10 mo
:Penalità alla Prova: N/A
:Fallimento Incantesimi: N/A
:Peso(M): +2.5 Kg

Quando vengono aggiunte allo scudo, queste chiodature lo rendono un’arma da guerra perforante che aumenta i danni inflitti da un colpo violento come se lo scudo fosse adatto a una creatura di una categoria di taglia superiore alla propria (vedi `Scudo Chiodato Leggero`_ e `Scudo Chiodato Pesante`_).

Non si possono mettere chiodature su un Buckler_ o su uno `Scudo Torre`_. Per il resto, attaccare con uno scudo chiodato è come un attacco con lo scudo.

Un bonus di potenziamento su uno scudo chiodato non migliora l’efficacia del colpo con lo scudo, ma lo scudo chiodato stesso può essere reso un’arma magica.

Corazza a Scaglie
^^^^^^^^^^^^^^^^^
.. figure:: Immagini/ManualeDiGioco/EquipCorazzaAScaglie.png
    :width: 200
    :align: right

:Classe: Armatura Media
:Bonus CA: +5
:Bonus Dex Massimo: +3
:Costo: 50 mo
:Fallimento Incantesimi: 25%
:Penalità alla Prova: -4
:Peso: 15 Kg
:Velocità 9m: 6m
:Velocità 6m: 4.5m

Questa armatura è coperta di piastre di metallo sovrapposte. Comprende i :ref:`Guanti d’Arme<guanto_darme>`.

Corazza a Strisce
^^^^^^^^^^^^^^^^^
.. figure:: Immagini/ManualeDiGioco/EquipCorazzaAStrisce.png
    :width: 200
    :align: right

:Classe: Armatura Pesante
:Bonus CA: +7
:Bonus Dex Massimo: +0
:Costo: 200 mo
:Fallimento Incantesimi: 40%
:Penalità alla Prova: -7
:Peso: 22.5 Kg
:Velocità 9m: 6m
:Velocità 6m: 4.5m

Questa corazza è fatta di strette strisce metalliche verticali, come quella di :ref:`Bande<equip_corazza_di_bande>`.

Comprende i :ref:`Guanti d’Arme<guanto_darme>`.

.. _equip_corazza_di_bande:

Corazza di Bande
^^^^^^^^^^^^^^^^
.. figure:: Immagini/ManualeDiGioco/EquipCorazzaDiBande.png
    :width: 200
    :align: right

:Classe: Armatura Pesante
:Bonus CA: +7
:Bonus Dex Massimo: +1
:Costo: 250 mo
:Fallimento Incantesimi: 35%
:Penalità alla Prova: -6
:Peso: 17.5 Kg
:Velocità 9m: 6m
:Velocità 6m: 4.5m

Questa armatura è fatta di strisce di metallo cucite a uno schienale di cuoio.

Comprende i :ref:`Guanti d’Arme<guanto_darme>`.

Corazza di Piastre
^^^^^^^^^^^^^^^^^^
.. figure:: Immagini/ManualeDiGioco/EquipCorazzaDiPiastre.png
    :width: 200
    :align: right

:Classe: Armatura Media
:Bonus CA: +6
:Bonus Dex Massimo: +3
:Costo: 200 mo
:Fallimento Incantesimi: 25%
:Penalità alla Prova: -4
:Peso: 15 Kg
:Velocità 9m: 6m
:Velocità 6m: 4.5m

Una corazza di piastre protegge il torso, ed è in genere ricavata da un unico pezzo di metallo lavorato.

Cotta di Maglia
^^^^^^^^^^^^^^^
.. figure:: Immagini/ManualeDiGioco/EquipCottaDiMaglia.png
    :width: 200
    :align: right

:Classe: Armatura Media
:Bonus CA: +6
:Bonus Dex Massimo: +2
:Costo: 150 mo
:Fallimento Incantesimi: 30%
:Penalità alla Prova: -5
:Peso: 20 Kg
:Velocità 9m: 6m
:Velocità 6m: 4.5m

Questa armatura a differenza del `Giaco di Maglia`_ protegge anche gambe e braccia.

Comprende i :ref:`Guanti d’Arme<guanto_darme>`.

.. _equip_armatura_cuoio:
.. _equip_cuoio:

Cuoio
^^^^^
.. figure:: Immagini/ManualeDiGioco/EquipCuoio.png
    :width: 200
    :align: right

:Classe: Armatura Leggera
:Bonus CA: +2
:Bonus Des Max: +6
:Costo: 10 mo
:Penalità alla Prova: 0
:Fallimento Incantesimi: 10%
:Peso(M): 7.5 Kg
:Velocità 9m: 9m
:Velocità 6m: 6m

Questa corazza è fatta di strati di cuoio induriti tramite bollitura in olio e tessuti insieme.

Cuoio Borchiato
^^^^^^^^^^^^^^^
.. figure:: Immagini/ManualeDiGioco/EquipCuoioBorchiato.png
    :width: 200
    :align: right

:Classe: Armatura Leggera
:Bonus CA: +3
:Bonus Des Max: +5
:Costo: 25 mo
:Penalità alla Prova: -1
:Fallimento Incantesimi: 15%
:Peso(M): 10 Kg
:Velocità 9m: 9m
:Velocità 6m: 6m

Questa corazza è simile a quella di Cuoio_, ma è rinforzata con rivetti metallici.

Giaco di Maglia
^^^^^^^^^^^^^^^
.. figure:: Immagini/ManualeDiGioco/EquipGiacoDiMaglia.png
    :width: 200
    :align: right

:Classe: Armatura Leggera
:Bonus CA: +4
:Bonus Des Max: +4
:Costo: 100 mo
:Penalità alla Prova: -2
:Fallimento Incantesimi: 20%
:Peso(M): 12.5 Kg
:Velocità 9m: 9m
:Velocità 6m: 6m

Un giaco di maglia è fatto di anelli metallici intrecciati che proteggono il torso.

Guanto d'Arme con Chiusura
^^^^^^^^^^^^^^^^^^^^^^^^^^

:Classe: Extra
:Costo: 8 mo
:Penalità alla Prova: Speciale
:Fallimento Incantesimi: Speciale
:Peso(M): +2.5 Kg

Questo guanto corazzato ha piccole catene e bracciali che permettono a chi lo indossa di agganciarvi l’arma in modo che non possa cadere.

Aggiunge +10 alla propria Difesa da Manovra in Combattimento per evitare di essere disarmati in combattimento.

Sganciare o agganciare un’arma da un guanto con sicura è un’:ref:`azione_di_round_completo` che provoca :ref:`Attacchi di Opportunità<attacco_di_opportunità>`.

Il prezzo indicato è per un singolo guanto d’arme con sicura e il peso indicato si applica solo se si indossa una `Corazza di Piastre`_, un’armatura leggera o se non si indossa alcuna armatura.
Diversamente, il guanto con sicura sostituisce un guanto che fa già parte dell’armatura.

.. Warning::
    Mentre il guanto è chiuso con la sicura, non si può usare la mano che lo indossa per lanciare incantesimi o usare abilità.

Come un normale guanto d’arme, un guanto d’arme con sicura permette di infliggere danni letali piuttosto che danni non letali con un colpo senz’armi.

Imbottita
^^^^^^^^^

.. figure:: Immagini/ManualeDiGioco/EquipImbottita.png
    :width: 200
    :align: right

:Classe: Armatura Leggera
:Bonus CA: +1
:Bonus Des Max: +8
:Costo: 5 mo
:Penalità alla Prova: 0
:Fallimento Incantesimi: 5%
:Peso(M): 5 Kg
:Velocità 9m: 9m
:Velocità 6m: 6m

Formata per lo più da strati imbottiti di stoffa e cotone, questa armatura offre una minima protezione.

Mezza Armatura
^^^^^^^^^^^^^^
.. figure:: Immagini/ManualeDiGioco/EquipMezzaArmatura.png
    :width: 200
    :align: right

:Classe: Armatura Pesante
:Bonus CA: +8
:Bonus Des Max: +0
:Costo: 600 mo
:Penalità alla Prova: -7
:Fallimento Incantesimi: 40%
:Peso(M): 25 Kg
:Velocità 9m: 6m
:Velocità 6m: 4.5m

Questa armatura è una combinazione di una `Cotta di Maglia`_ con piastre metalliche.

Comprende :ref:`Guanti d’Arme<guanto_darme>` e un elmo.

.. _equip_armatura_di_pelle:

Pelle
^^^^^
.. figure:: Immagini/ManualeDiGioco/EquipPelle.png
    :width: 200
    :align: right

:Classe: Armatura Media
:Bonus CA: +4
:Bonus Des Max: +4
:Costo: 15 mo
:Penalità alla Prova: -3
:Fallimento Incantesimi: 20%
:Peso(M): 12.5 Kg
:Velocità 9m: 6m
:Velocità 6m: 4.5m

Questa corazza è preparata con molteplici strati di cuoio e pelli di animali.

.. _equip_scudo_leggero_legno:

Scudo Leggero di Legno
^^^^^^^^^^^^^^^^^^^^^^

:Classe: Scudi
:Bonus CA: +1
:Bonus Des Max: N/A
:Costo: 3 mo
:Penalità alla Prova: -1
:Fallimento Incantesimi: 5%
:Peso(M): 2.5 Kg

Uno scudo si aggancia all’avambraccio e si tiene con la mano.
Il peso scarso di uno scudo leggero permette di trasportare altri oggetti in quella mano, benchè non si possano usare armi.

Legno o Metallo
   Gli scudi di legno o di metallo generalmente offrono la stessa protezione base, anche se rispondono in modo diverso ad
   alcuni incantesimi ed effetti.

Attacchi con Scudo
    È possibile colpire violentemente un avversario con uno scudo leggero usandolo come arma secondaria, si veda `Scudo Leggero`_.

    Usato in questo modo, lo scudo leggero è un’arma da guerra contundente che va considerata leggera per quanto riguarda le
    penalità di attacco. Si perde inoltre il bonus alla CA fino al round successivo.

    .. Note::
        Un bonus di potenziamento su uno scudo non ne migliora l’efficacia del colpo, ma lo scudo stesso può essere
        reso un’arma magica.

Scudo Leggero di Metallo
^^^^^^^^^^^^^^^^^^^^^^^^
.. figure:: Immagini/ManualeDiGioco/EquipScudoDiMetallo.png
    :width: 200
    :align: right

:Classe: Scudi
:Bonus CA: +1
:Bonus Des Max: N/A
:Costo: 9 mo
:Penalità alla Prova: -1
:Fallimento Incantesimi: 5%
:Peso(M): 3 Kg

Uno scudo si aggancia all’avambraccio e si tiene con la mano.
Il peso scarso di uno scudo leggero permette di trasportare altri oggetti in quella mano, benchè non si possano usare armi.

Legno o Metallo
   Gli scudi di legno o di metallo generalmente offrono la stessa protezione base, anche se rispondono in modo diverso ad
   alcuni incantesimi ed effetti.

Attacchi con Scudo
    È possibile colpire violentemente un avversario con uno scudo leggero usandolo come arma secondaria, si veda `Scudo Leggero`_.

    Usato in questo modo, lo scudo leggero è un’arma da guerra contundente che va considerata leggera per quanto riguarda le
    penalità di attacco. Si perde inoltre il bonus alla CA fino al round successivo.

    .. Note::
        Un bonus di potenziamento su uno scudo non ne migliora l’efficacia del colpo, ma lo scudo stesso può essere
        reso un’arma magica.

.. _equip_scudo_pesante_legno:

Scudo Pesante di Legno
^^^^^^^^^^^^^^^^^^^^^^

:Classe: Scudi
:Bonus CA: +2
:Bonus Des Max: N/A
:Costo: 7 mo
:Penalità alla Prova: -2
:Fallimento Incantesimi: 15%
:Peso(M): 5 Kg

Uno scudo si aggancia all’avambraccio e si tiene con la mano.
Uno scudo di questo tipo è troppo pesante per poter usare la mano dello scudo per qualcos'altro.

Legno o Metallo
   Gli scudi di legno o di metallo generalmente offrono la stessa protezione base, anche se rispondono in modo diverso ad
   alcuni incantesimi ed effetti.

Attacchi con Scudo
    È possibile colpire violentemente un avversario con uno scudo pesante usandolo come arma secondaria, si veda `Scudo Pesante`_.

    Usato in questo modo, lo scudo pesante è un’arma da guerra contundente e per quanto riguarda le penalità di attacco bisogna considerare lo scudo pesante come un’arma a una mano.
    Se si usa lo scudo come un’arma, si perde il suo bonus alla CA fino al round successivo.

    .. Note::
        Un bonus di potenziamento su uno scudo non ne migliora l’efficacia del colpo, ma lo scudo stesso può essere
        reso un’arma magica.

Scudo Pesante di Metallo
^^^^^^^^^^^^^^^^^^^^^^^^

:Classe: Scudi
:Bonus CA: +2
:Bonus Des Max: N/A
:Costo: 20 mo
:Penalità alla Prova: -2
:Fallimento Incantesimi: 15%
:Peso(M): 7.5 Kg

Uno scudo si aggancia all’avambraccio e si tiene con la mano.
Uno scudo di questo tipo è troppo pesante per poter usare la mano dello scudo per qualcos'altro.

Legno o Metallo
   Gli scudi di legno o di metallo generalmente offrono la stessa protezione base, anche se rispondono in modo diverso ad
   alcuni incantesimi ed effetti.

.. _attacco_con_scudo:

Attacchi con Scudo
    È possibile colpire violentemente un avversario con uno scudo pesante usandolo come arma secondaria, si veda `Scudo Pesante`_.

    Usato in questo modo, lo scudo pesante è un’arma da guerra contundente e per quanto riguarda le penalità di attacco bisogna considerare lo scudo pesante come un’arma a una mano.
    Se si usa lo scudo come un’arma, si perde il suo bonus alla CA fino al round successivo.

    .. Note::
        Un bonus di potenziamento su uno scudo non ne migliora l’efficacia del colpo, ma lo scudo stesso può essere
        reso un’arma magica.

.. _equip_scudo_torre:

Scudo Torre
^^^^^^^^^^^
.. figure:: Immagini/ManualeDiGioco/EquipScudoTorre.png
    :width: 200
    :align: right

:Classe: Scudi
:Bonus CA: +4
:Bonus Des Max: +2
:Costo: 30 mo
:Penalità alla Prova: -10
:Fallimento Incantesimi: 50%
:Peso(M): 22.5 Kg

Questo massiccio scudo di legno è alto quasi quanto colui che lo tiene.

In situazioni normali fornisce il bonus di scudo alla CA indicato, ma come :ref:`azione_standard` lo si può anche utilizzare per ottenere una :ref:`copertura` totale fino al proprio turno successivo.
Per farlo bisogna scegliere un lato del proprio spazio che verrà trattato come un muro solido per tutti gli attacchi diretti soltanto contro di sè e solo attraverso questo lato.

Uno scudo torre, comunque, non protegge da incantesimi con un bersaglio specifico: un incantatore può lanciare un incantesimo sull’utilizzatore sfruttando come bersaglio lo scudo che sta tenendo.

Non si può colpire violentemente con uno scudo torre, e la mano che tiene lo scudo non può fare nient’altro.

.. Note::
    Quando si utilizza uno scudo torre in combattimento si subisce penalità –2 al tiro per colpire a causa dell’ingombro dello scudo.

Armature Perfette
-----------------

Proprio come per le :ref:`armi<equip_armi_perfette>`, è possibile comprare o costruire una versione perfetta di armature e scudi.

Questi oggetti di ottima fattura funzionano come le versioni normali tranne che le loro penalità di armatura alla prova vengono ridotte di 1 e hanno un costo extra di 150 mo rispetto al normale.

.. Note::
    La qualità perfetta di un’armatura o di uno scudo non fornisce un bonus al tiro per colpire o ai danni, anche se l’armatura o lo scudo vengono usati come armi.

Tutte le armature e gli scudi magici sono automaticamente considerati armature o scudi perfetti.

Un’armatura o uno scudo non può ottenere una qualità perfetta dopo che è stato creato; deve essere costruito come oggetto perfetto già dall’inizio.

Armature per Creature Insolite
------------------------------

.. _tabella_armature_insolite:

.. figure:: Immagini/ManualeDiGioco/EquipTabArmatureCreatureInsolite.png
    :width: 300
    :align: right

Armature e scudi per creature insolitamente grandi o piccole, o per creature non umanoidi (come i cavalli), hanno prezzi e pesi differenti da quelli tradizionali, che si possono ricavare dai modificatori riportati in :ref:`Tabella<tabella_armature_insolite>`.

.. _equip_indossare_armatura:

Indossare e Togliere l'Armatura
-------------------------------

.. figure:: Immagini/ManualeDiGioco/EquipTabIndossareArmatura.png
    :width: 700

Il tempo necessario a indossare un’armatura dipende dal tipo.

Indossare
^^^^^^^^^
Questa colonna indica quanto tempo ci vuole per indossare un’armatura. (1 minuto sono 10 round).

Preparare uno scudo (allacciarlo) richiede soltanto un’:ref:`azione_movimento`.

Indossare in Fretta
^^^^^^^^^^^^^^^^^^^
Questa colonna indica quanto tempo ci vuole per indossare l’armatura quando si ha fretta.

L’armatura indossata in fretta ha una penalità di armatura alla prova e un bonus di armatura entrambi di 1 punto peggiore del normale.

Togliere
^^^^^^^^
Questa colonna indica quanto tempo ci vuole per togliersi l’armatura.

Togliere lo scudo dall’avambraccio e lasciarlo cadere richiede soltanto un’:ref:`azione_movimento`.

Materiali Speciali
==================

Le armature e le armi si possono costruire con materiali che possiedono delle innate qualità speciali.
Se si costruisce un’armatura o arma con più di un materiale speciale, si ricevono i benefici solo del materiale prevalente.

.. Note::
    Si costruire un’arma doppia con ogni testa fatta di un materiale speciale diverso.

Ognuno dei materiali speciali descritti in questa sezione ha un preciso effetto di gioco.
Alcune creature hanno una riduzione del danno che le rendono resistenti a tutto tranne ad un tipo speciale di danno, come quello inflitto dalle armi allineate con il male o le armi contundenti.
Altri sono vulnerabili alle armi di un particolare materiale.

I personaggi possono anche decidere di trasportare diversi tipi di armi, a seconda della campagna e del tipo di creature incontrate normalmente.

.. contents:: Contenuti
    :local:
    :depth: 3

Adamantio
---------

.. _equip_tabella_adamantio:

.. figure:: Immagini/ManualeDiGioco/EquipTabModificatoriCostoAdamantio.png
    :width: 300
    :align: right

Questo metallo durissimo si trova solo nei meteoriti e contribuisce alla qualità di un’arma o di un’armatura.

Le armi in adamantio hanno una capacità naturale nel superare la durezza quando spaccano le armi o rompono gli oggetti, ignorando la :ref:`Durezza<durezza>` inferiore a 20.
Una corazza in adamantio offre a chi la indossa riduzione del danno 1/— se leggera, 2/— se media e 3/— se pesante.

L’adamantio è tanto costoso che le armi e le armature fatte in questo materiale sono sempre perfette e il costo della qualità perfetta è incluso nei riportati in :ref:`Tabella<equip_tabella_adamantio>`,
armi e munizioni hanno bonus di potenziamento +1 ai tiri per colpire, e la penalità di armatura alla prova delle armature viene diminuita.

.. Warning::
    Gli oggetti senza parti metalliche non possono essere costruiti con l’adamantio.
    Una freccia può essere in adamantio, ma un `Bastone Ferrato`_ no.

Armi, armature e scudi fatti normalmente d’acciaio e costruiti con l’adamantio hanno un terzo dei punti ferita in più del normale.

In generale, l’adamantio ha 40 punti ferita per 2,5 cm di spessore e durezza 20.

Argento Alchemico
-----------------

.. _equip_tabella_argento_alchemico:

.. figure:: Immagini/ManualeDiGioco/EquipTabModificatoriCostoArgentoAlchemico.png
    :width: 300
    :align: right

Un complesso processo che coinvolge la metallurgia e l’alchimia può legare l’argento ad un’arma fatta d’acciaio in modo che oltrepassi la riduzione del danno di creature come i licantropi.

Superando un tiro per colpire con un’arma d’argento, chi la impugna subisce penalità –1 ai tiri per il danno (con il solito minimo di 1 danno).

.. Warning::
    Il processo di argentatura alchemica non può essere applicato alle armi non metalliche, e non funziona sui metalli rari come l’adamantio, il ferro freddo e il mithral.

L’argento alchemico ha 10 punti ferita per ogni 2,5 cm di spessore e durezza 8.

Ferro Freddo
------------
Noto per la sua efficacia contro demoni e folletti, questo ferro viene estratto nelle profondità del sottosuolo ed è forgiato ad una temperatura inferiore per conservare le sue delicate proprietà.

Costruire armi fatte di ferro freddo costa il doppio rispetto alle loro normali controparti e qualsiasi potenziamento magico costa 2.000 mo addizionali. Tale aumento viene applicato solo la prima volta che l’oggetto viene potenziato.
Un’arma doppia che è fatta solo per metà di ferro freddo aumenta il suo costo del 50%.

.. Warning::
    Gli oggetti senza parti di metallo non possono essere costruiti in ferro freddo.
    Una freccia potrebbe essere fatta di ferro freddo ma un Randello_ no.

Il ferro freddo ha 30 punti ferita per 2,5 cm di spessore e durezza 10.

Legnoscuro
----------
Questo raro legno magico è duro come il legno normale, ma molto leggero.
Qualsiasi oggetto interamente o per la maggior parte in legno (come un arco, una freccia o una Lancia_) che sia fatto di legnoscuro è considerato un oggetto perfetto e pesa solo la metà di un oggetto di legno normale di quel tipo.
Le penalità di armatura alla prova per gli scudi di legnoscuro sono ridotte di 2 rispetto ad un normale scudo dello stesso tipo.

Il prezzo di questi oggetti è pari a quello di una versione perfetta con l'aggiunta di 10mo per ogni 0.5 Kg.

.. Warning::
    Gli oggetti che non sono normalmente fatti di legno o che sono di legno solo parzialmente (come un’`Ascia da Battaglia`_ o una :ref:`Mazza<equip_mazza_leggera>`) possono essere fatti di legnoscuro, ma non ne guadagnano alcun beneficio speciale.

Il legnoscuro ha 10 punti ferita per ogni 2,5 cm di spessore e durezza 5.

Mithral
-------

.. _equip_tabella_mithral:

.. figure:: Immagini/ManualeDiGioco/EquipTabModificatoriCostoMithral.png
    :width: 300
    :align: right

Il mithral è un metallo molto raro, luccicante, simile all’argento, più leggero del ferro ma altrettanto duro.
Quando viene lavorato come l’acciaio, diventa un meraviglioso materiale con cui creare armature, e occasionalmente viene usato anche per altri oggetti.

La maggior parte delle armature in mithral è più leggera di una categoria del normale, ed è più agevole per il movimento e le altre limitazioni: armature pesanti sono trattate come medie e le medie come leggere, ma le leggere restano tali.
Questa diminuzione non si applica alla competenza necessaria per indossare l’armatura in questione: occorre essere competenti nel tipo di armatura appropriato, altrimenti si incorre nelle relative penalità come di norma.

Le probabilità di fallimento di un incantesimo per armature e scudi in mithral diminuiscono del 10%, il bonus massimo di Destrezza aumenta di 2 e le penalità di armatura alla prova diminuiscono di 3 (con minimo di 0).
Gli oggetti in mithral pesano la metà dello stesso oggetto fatto di altri metalli, ma le armi non cambiano la categoria di taglia dell’arma o la facilità con cui viene impugnata.

Le armi o le armature fatte di mithral vanno trattate come l’argento per superare la riduzione del danno e sono oggetti perfetti, con prezzo già compreso nei prezzi riportati in :ref:`Tabella<equip_tabella_mithral>`.

.. Warning::
    Tutto ciò non vale per oggetti che hanno soltanto una parte in metallo: una `Spada Lunga`_ è influenzata, un `Bastone Ferrato`_ no.

Il mithral ha una durezza di 15 e 30 punti ferita per ogni 2,5 cm di spessore.

Pelle di Drago
--------------
I fabbricanti di armature possono lavorare le pelli dei draghi per produrre armature o scudi di qualità perfetta.

Un drago fornisce pelle sufficiente per una singola :ref:`Armatura di Pelle<equip_armatura_di_pelle>` per una creatura di una taglia più piccola del drago.
Selezionando solo le scaglie e le parti di pelle migliori, un fabbricante di armature può produrre una `Corazza di Bande`_ per una creatura di due taglie più piccola, una `Mezza Armatura`_ tre e una `Corazza di Piastre`_ o
un’`Armatura Completa`_ quattro.

C’è pelle sufficiente per produrre uno :ref:`Scudo Leggero<equip_scudo_leggero_legno>` o :ref:`Pesante<equip_scudo_pesante_legno>` in aggiunta all’armatura, purché il drago sia Grande o Maggiore.
Se la pelle di drago proviene da un drago che ha immunità ad un tipo di energia, l’armatura ne eredità l'immunità, sebbene non conferisca alcuna protezione a chi la indossa.
Se allo scudo o all’armatura viene conferita in seguito la capacità di proteggere chi la indossa da un tipo di energia specifico, il costo di questo potenziamento viene ridotto del 25%.

.. Note::
    Le armature di pelle di drago non sono metalliche quindi i druidi possono indossarle senza penalità.

Le armature di pelle di drago costano il doppio di un’armatura perfetta di quel tipo, ma non richiedono più tempo per essere costruite (si raddoppino tutti i risultati di :ref:`Artigianato<artigianato>`).

La pelle di drago ha 10 punti ferita per 2,5 cm di spessore e durezza 10. Solitamente è spessa da 1,25 a 2,5 cm.

Merci e Servizi
===============

.. contents:: Contenuti
    :local:
    :depth: 1

Oltre ad armi ed armature un personaggio può avere una notevole varietà di attrezzature a disposizione, dalle razioni da viaggio, alle corde (che possono essere utili in molte circostanze).

Ciascuna categoria di oggetti ha una tabella che elenca gli oggetti che un personaggio può portare con se così come i loro prezzi.

.. _note_merci_servizi:

.. Warning::
    Il significato delle note nelle tabelle sono i seguenti: 

        1.  Questi oggetti pesano un quarto del valore se vengono fatti per personaggi di taglia Piccola e i contenitori hanno capacità di un quarto rispetto al normale.
        2.  Relativo ad armature simili fatte per umanoidi di taglia Media.
        3.  Vedi la descrizione dell'incantesimo per i costi aggiuntivi. Se i costi aggiuntivi fanno eccedere il costo totale dell'incantesimo oltre 3000mo, quell'incantesimo non è generalmente disponibile. Usare un livello di incantesimo di 1/2 per gli incantesimi di livello 0 o per calcolarne il costo

.. _tab_equip_avventura:

Equipaggiamento d'avventura
---------------------------

+-----------------------------------------------------------------+--------+--------+---------------------------------+
| Nome                                                            | Costo  | Peso   | :ref:`Note<note_merci_servizi>` |
+=================================================================+========+========+=================================+
| `Acciarino e pietra Focaia`_                                    | 1mo    |        |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| `Ago da cucito`_                                                | 5ma    |        |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| `Amo da pesca`_                                                 | 1ma    |        |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| `Ampolla (vuota)`_                                              | 3mr    | 0.75Kg |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| `Anello con sigillo`_                                           | 5mo    |        |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| `Ariete portatile`_                                             | 10 mo  | 10Kg   |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| `Asta (3m)`_                                                    | 5mr    | 4Kg    |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| `Barile (vuoto)`_                                               | 2mo    | 15Kg   |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| `Boccale di ceramica`_                                          | 2mr    | 0.5Kg  |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| `Boccetta di inchiostro o pozione`_                             | 1mo    |        |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| `Borsa da cintura (vuota)`_                                     | 1mo    | 0.25Kg | 1                               |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| `Bottiglia di vetro`_                                           | 2mo    | 0.5Kg  |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| `Brocca di ceramica`_                                           | 3mr    | 4.5Kg  |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| Campanella_                                                     | 1mo    |        |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| Candela_                                                        | 1mr    |        |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| Cannocchiale_                                                   | 1000mo | 0.5Kg  |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| `Caraffa di ceramica`_                                          | 2mr    | 2.5Kg  |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| `Carrucola e paranco`_                                          | 5mo    | 2.5Kg  |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| `Carta (foglio)`_                                               | 4ma    |        |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| `Cassa (vuota)`_                                                | 2mo    | 12.5Kg |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| `Catena (3m)`_                                                  | 30mo   | 1Kg    |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| Ceralacca_                                                      | 1mo    | 0.5Kg  |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| `Cesto (vuoto)`_                                                | 4ma    | 0.5Kg  |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| `Chiodo da rocciatore`_                                         | 1ma    | 0.25Kg |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| Clessidra_                                                      | 25mo   | 0.5Kg  |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| `Coperta invernale`_                                            | 5ma    | 1.5Kg  | 1                               |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| `Corda di Canapa (15m)`_                                        | 1mo    | 5Kg    |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| `Corda di Seta (15m)`_                                          | 10mo   | 2.5Kg  |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| `Cote per affilare`_                                            | 2mr    | 0.5Kg  |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| `Custodia per mappe o pergamene`_                               | 1mo    | 0.25Kg |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| Fischietto_                                                     | 8ma    |        |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| `Gessetto (1 pezzo)`_                                           | 1mr    |        |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| Giaciglio_                                                      | 1ma    | 2.5Kg  | 1                               |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| `Inchiostro (Boccetta da 30g)`_                                 | 8mo    |        |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| `Lampada comune`_                                               | 1ma    | 0.5Kg  |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| `Lanterna a lente sporgente`_                                   | 12mo   | 1.5Kg  |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| `Lanterna schermabile`_                                         | 7mo    | 1Kg    |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| `Legna da ardere (per giorno)`_                                 | 1mr    | 10Kg   |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| Maglio_                                                         | 1mo    | 5Kg    |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| Manette_                                                        | 15mo   | 1Kg    |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| `Manette perfette`_                                             | 50mo   | 1Kg    |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| Martello_                                                       | 5ma    | 1Kg    |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| `Olio (ampolla da 0.5l)`_                                       | 1ma    | 0.5Kg  |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| `Orologio ad Acqua`_                                            | 1000mo | 100Kg  |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| Otre_                                                           | 1mo    | 2Kg    | 1                               |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| Pennino_                                                        | 1ma    |        |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| `Pentola di ferro`_                                             | 8ma    | 2Kg    |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| `Pala o badile`_                                                | 2mo    | 4Kg    |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| `Pergamena (Foglio)`_                                           | 2ma    |        |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| `Piccone da minatore`_                                          | 3mo    | 5Kg    |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| `Piede di porco`_                                               | 2mo    | 2.5Kg  |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| Rampino_                                                        | 1mo    | 2Kg    |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| `Razioni da viaggio (al giorno)`_                               | 5ma    | 0.5Kg  | 1                               |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| `Rete da pesca (2.25m)`_                                        | 4mo    | 2.5Kg  |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| `Sacco (vuoto)`_                                                | 1ma    | 0.25Kg | 1                               |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| `Sapone (per 0.5Kg)`_                                           | 5ma    | 0.5Kg  |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| `Scala a pioli (3m)`_                                           | 2ma    | 10Kg   |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| `Secchio (vuoto)`_                                              | 5ma    | 1Kg    |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| :ref:`Serratura/Lucchetto Semplice<equip_serratura_semplice>`   | 20mo   | 0.5Kg  |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| :ref:`Serratura/Lucchetto Media<equip_serratura_media>`         | 40mo   | 0.5Kg  |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| :ref:`Serratura/Lucchetto Buona<equip_serratura_buona>`         | 80mo   | 0.5Kg  |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| :ref:`Serratura/Lucchetto Superiore<equip_serratura_superiore>` | 150mo  | 0.5Kg  |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| `Specchio piccolo di metallo`_                                  | 10mo   | 0.25Kg |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| :ref:`Tela (m2)<equip_tela>`                                    | 1ma    | 0.5Kg  |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| Tenda_                                                          | 10mo   | 10Kg   | 1                               |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| Torcia_                                                         | 1mr    | 0.5Kg  |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| Triboli_                                                        | 1mo    | 1Kg    |                                 |
+-----------------------------------------------------------------+--------+--------+---------------------------------+
| Zaino_                                                          | 2mo    | 1Kg    | 1                               |
+-----------------------------------------------------------------+--------+--------+---------------------------------+

Acciarino e Pietra Focaia
^^^^^^^^^^^^^^^^^^^^^^^^^

:Costo: 1mo

Accendere una torcia con acciarino e pietra focaia è un’:ref:`azione_di_round_completo` e accendere qualsiasi altro fuoco in questo modo richiede almeno
altrettanto.

Ago da Cucito
^^^^^^^^^^^^^

:Costo: 5ma

Amo da Pesca
^^^^^^^^^^^^

:Costo: 5ma

Ampolla (vuota)
^^^^^^^^^^^^^^^

:Costo: 1ma
:Peso: 0.75Kg

Anello con Sigillo
^^^^^^^^^^^^^^^^^^

:Costo: 5mo

Ariete Portatile
^^^^^^^^^^^^^^^^

:Costo: 10mo
:Peso: 10Kg

Questa trave di legno rivestita di metallo fornisce bonus +2 alle prove di Forza per sfondare porte, ma permette a una seconda persona di aiutare senza dover
effettuare alcun tiro, aggiungendo un altro +2 alla prova.

Asta (3m)
^^^^^^^^^

:Costo: 5mr
:Peso: 4Kg

Barile (vuoto)
^^^^^^^^^^^^^^

:Costo: 2mo
:Peso: 15Kg

Boccale di Ceramica
^^^^^^^^^^^^^^^^^^^

:Costo: 2mr
:Peso: 0.5Kg

Boccetta di Inchiostro o Pozione
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:Costo: 1mo

Un contenitore di vetro o metallo che contiene 30 grammi di liquido.

.. figure:: Immagini/ManualeDiGioco/EquipBorsaDaCintura.png
    :width: 150
    :align: right

Borsa da Cintura (Vuota)
^^^^^^^^^^^^^^^^^^^^^^^^

:Costo: 1mo
:Peso: 0.25Kg

.. Note::
    Pesa e ha una capacità di un quarto del valore se viene fatto per personaggi di taglia Piccola.

Bottiglia di Vetro
^^^^^^^^^^^^^^^^^^

:Costo: 2mo
:Peso: 0.5Kg

Brocca di Ceramica
^^^^^^^^^^^^^^^^^^

:Costo: 3mr
:Peso: 4.5Kg

Una semplice brocca di ceramica chiusa con un tappo che contiene 4.5l di liquido.

Campanella
^^^^^^^^^^

:Costo: 1mo

Candela
^^^^^^^

:Costo: 1mr

Una candela illumina con luce tenue un’area piccola, aumentando la luminosità di un livello in un raggio di 1,5 metri (vedi :ref:`visibilità_luce`).

..Note::
    Buio diventa luce fioca e luce fioca diventa luce normale.

Una candela non può aumentare il livello della luce oltre quella normale e brucia per 1 ora.

.. figure:: Immagini/ManualeDiGioco/EquipCannocchiale.png
    :width: 300
    :align: right

Cannocchiale
^^^^^^^^^^^^

:Costo: 1000mo
:Peso: 0.5Kg

Gli oggetti visti attraverso un cannocchiale sono ingranditi al doppio della loro misura.

I personaggi che usano un cannocchiale subiscono penalità –1 alle prove di :ref:`Percezione` per 6 metri di distanza dal bersaglio, se il bersaglio è visibile.

Caraffa di Ceramica
^^^^^^^^^^^^^^^^^^^

:Costo: 2mr
:Peso: 2.5Kg

.. figure:: Immagini/ManualeDiGioco/EquipParanco.png
    :width: 120
    :align: right

Carrucola e Paranco
^^^^^^^^^^^^^^^^^^^

:Costo: 5mo
:Peso: 2.5Kg

Carta (Foglio)
^^^^^^^^^^^^^^

:Costo: 4ma

Cassa (Vuota)
^^^^^^^^^^^^^

:Costo: 2mo
:Peso: 12.5Kg

.. figure:: Immagini/ManualeDiGioco/EquipCatena.png
    :width: 250
    :align: right

Catena (3m)
^^^^^^^^^^^

:Costo: 30mo
:Peso: 1Kg

La catena ha durezza 10 e 5 punti ferita, ma puà essere spezzata con una prova di Forza con CD 26.

Ceralacca
^^^^^^^^^

:Costo: 1mo
:Peso: 0.5Kg

Cesto (Vuoto)
^^^^^^^^^^^^^

:Costo: 1mo
:Peso: 0.5Kg

.. _equip_chiodi_da_rocciatore:

Chiodo da Rocciatore
^^^^^^^^^^^^^^^^^^^^

:Costo: 1ma
:Peso: 0.25Kg

.. figure:: Immagini/ManualeDiGioco/EquipClessidra.png
    :width: 150
    :align: right

Clessidra
^^^^^^^^^

:Costo: 25mo
:Peso: 0.5Kg

Coperta Invernale
^^^^^^^^^^^^^^^^^

:Costo: 5ma
:Peso: 1.5Kg

.. Note::
    Pesa e ha una capacità di un quarto del valore se viene fatto per personaggi di taglia Piccola.

.. figure:: Immagini/ManualeDiGioco/EquipCordaDiCanapa.png
    :width: 200
    :align: right

Corda di Canapa (15m)
^^^^^^^^^^^^^^^^^^^^^

:Costo: 1mo
:Peso: 5Kg

Questa corda ha 2 punti ferita e può essere spezzata con una prova di Forza con CD 23.

Corda di Seta (15m)
^^^^^^^^^^^^^^^^^^^

:Costo: 10mo
:Peso: 2.5Kg

Questa corda ha 4 punti ferita e può essere spezzata con una prova di Forza con CD 24.

Cote per Affilare
^^^^^^^^^^^^^^^^^

:Costo: 2mr
:Peso: 0.5Kg

Custodia per Mappe o Pergamene
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:Costo: 1mo
:Peso: 0.25Kg

Fischietto
^^^^^^^^^^

:Costo: 8ma

Gessetto (1 pezzo)
^^^^^^^^^^^^^^^^^^

:Costo: 1mr

.. figure:: Immagini/ManualeDiGioco/EquipGiaciglio.png
    :width: 200
    :align: right

Giaciglio
^^^^^^^^^

:Costo: 1ma
:Peso: 2.5Kg

.. Note::
    Pesa e ha una capacità di un quarto del valore se viene fatto per personaggi di taglia Piccola.

Inchiostro (Boccetta da 30g)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:Costo: 8mo

Un inchiostro diverso dal nero costa il doppio.

Lampada Comune
^^^^^^^^^^^^^^

:Costo: 1ma
:Peso: 0.5Kg

Una lampada illumina con luce normale un’area piccola di 4,5 metri di raggio, aumentando ulteriormente la luminosità di un
livello (vedi :ref:`visibilità_luce`) in un raggio di 4,5 metri oltre quell’area.

Una lampada non può aumentare il livello della luce oltre quella normale o intensa e brucia per 6 ore con 0,5 litri d’olio.

È possibile trasportarla con una mano.

Lanterna a Lente Sporgente
^^^^^^^^^^^^^^^^^^^^^^^^^^

:Costo: 12mo
:Peso: 1.5Kg

Una lanterna a lente sporgente illumina con luce normale un cono di 18 metri, aumentando ulteriormente la luminosità di un
livello (vedi :ref:`visibilità_luce`) in un cono di 36 metri oltre quell’area.

Una lampada a lente sporgente non può aumentare il livello della luce oltre intensa e brucia per 6 ore con 0,5 litri d’olio.

È possibile trasportarla con una mano.

.. figure:: Immagini/ManualeDiGioco/EquipLanternaSchermabile.png
    :width: 200
    :align: right

Lanterna Schermabile
^^^^^^^^^^^^^^^^^^^^

:Costo: 7mo
:Peso: 1Kg

Una lanterna schermabile illumina con luce normale un’area di 9 metri di raggio, aumentando ulteriormente la luminosità di un
livello (vedi :ref:`visibilità_luce`) in un raggio di 9 metri oltre quell’area.

Una lampada schermabile non può aumentare il livello della luce oltre quella intensa e brucia per 6 ore con 0,5 litri d’olio.

È possibile trasportarla con una mano.

Legna da Ardere (per Giorno)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:Costo: 1mr
:Peso: 10Kg

Maglio
^^^^^^

:Costo: 1mo
:Peso: 5Kg

Manette
^^^^^^^

.. figure:: Immagini/ManualeDiGioco/EquipManette.png
    :width: 250
    :align: right

:Costo: 15mo
:Peso: 1Kg

Queste manette possono legare una creatura di taglia Media.

Il personaggio ammanettato può usare :ref:`artista_della_fuga` per liberarsi con CD 30 o 35 per le `Manette Perfette`_ oppure
tenteare di romperle effettuando con successo una prova di Forza con CD 26 o 28 per le `Manette Perfette`_.

Le manette hanno durezza 10 e 10 punti ferita.

.. Note::
    Molte manette hanno serrature. Per il prezzo totale bisogna aggiungere il costo della serratura desiderata al costo delle manette.

Allo stesso prezzo si possono comprare manette per creature di taglia Piccola, ma quelle di taglia Grande costano 10 volte tanto.
Le creature di taglia Enorme, Mastodontica, Colossale, Minuscola, Minuta e Piccolissima possono essere trattenute solo con manette costruite appositamente che costano 100 volte tanto.

Manette Perfette
^^^^^^^^^^^^^^^^

:Costo: 50mo
:Peso: 1Kg

Vedi Manette_.

Martello
^^^^^^^^

:Costo: 5ma
:Peso: 1Kg

Se viene usato in combattimento, il martello viene considerato un’arma a una mano :ref:`improvvisata<equip_arma_improvvisata>` che
infligge danni contundenti come fosse un `Guanto d'Arme Chiodato`_ della stessa taglia.

Olio (Ampolla da 0.5l)
^^^^^^^^^^^^^^^^^^^^^^

:Costo: 1ma
:Peso: 0.5Kg

In una lanterna 0,5 litri d’olio bruciano per 6 ore.

È possibile utilizzare un’ampolla d’olio come arma a spargimento, con delle regole simili a quelle del `Fuoco dell'Alchimista (Ampolla)`_,
tranne per il fatto che occorre un’:ref:`azione_di_round_completo` per preparare un’ampolla con una miccia e che, una volta lanciata,
c’è solo una probabilità del 50% che l’ampolla prenda fuoco.

È possibile versare 0,5 litri d’olio sul terreno per coprire un’area quadrata con lato di 1,5 metri, purché la superficie sia liscia.
Se incendiato, l’olio brucia per 2 round e infligge 1d3 danni a ogni creatura nell’area.

Orologio ad Acqua
^^^^^^^^^^^^^^^^^

:Costo: 1000mo
:Peso: 100Kg

Questo grande congegno ingombrante fornisce l’ora esatta con lo scarto di mezz’ora per giorno da quando è stato regolato l’ultima volta.

Richiede una fonte d’acqua e deve essere tenuto immobile poiché segna il tempo con il flusso regolare delle gocce d’acqua.

.. figure:: Immagini/ManualeDiGioco/EquipOtre.png
    :width: 150
    :align: right

Otre
^^^^

:Costo: 1mo
:Peso: 2Kg

.. Note::
    Pesa e ha una capacità di un quarto del valore se viene fatto per personaggi di taglia Piccola.

.. figure:: Immagini/ManualeDiGioco/EquipInchiostroEPennino.png
    :width: 150
    :align: left

Pennino
^^^^^^^

:Costo: 1ma

Pentola di Ferro
^^^^^^^^^^^^^^^^

:Costo: 8ma
:Peso: 2Kg

.. figure:: Immagini/ManualeDiGioco/EquipPala.png
    :width: 80
    :align: right

Pala o Badile
^^^^^^^^^^^^^

:Costo: 2mo
:Peso: 4Kg

Se una pala è usata in combattimento, viene considerata come un’:ref:`arma improvvisata<equip_arma_improvvisata>` ad una mano che infligge
danni contundenti pari a quelli di un Randello_ della stessa taglia.

Pergamena (Foglio)
^^^^^^^^^^^^^^^^^^

:Costo: 2ma

.. figure:: Immagini/ManualeDiGioco/EquipPicconeDaMinatore.png
    :width: 150
    :align: left

Piccone da Minatore
^^^^^^^^^^^^^^^^^^^

:Costo: 3mo
:Peso: 5Kg

Se un piccone da minatore è usato in combattimento, viene considerato come un’:ref:`arma improvvisata<equip_arma_improvvisata>` ad una mano
che infligge danni penetranti pari a quelli di un `Piccone Pesante`_ della stessa taglia.

.. figure:: Immagini/ManualeDiGioco/EquipPiedeDiPorco.png
    :width: 100
    :align: right

Piede di Porco
^^^^^^^^^^^^^^

:Costo: 2mo
:Peso: 2.5Kg

Un piede di porco fornisce bonus di circostanza +2 alle prove di Forza effettuate per forzare una porta o uno scrigno.

Se usato in combattimento, il piede di porco viene considerato un’:ref:`arma improvvisata<equip_arma_improvvisata>` a una mano
che infligge danni contundenti pari a quelli di un `Randello`_ della stessa taglia.

.. figure:: Immagini/ManualeDiGioco/EquipRampino.png
    :width: 150
    :align: left

Rampino
^^^^^^^

:Costo: 1mo
:Peso: 2Kg

Lanciare efficacemente il rampino da scalata richiede un’:ref:`attacco a distanza<azione_attacco_a_distanza>`,
considerandolo un’arma da lancio con incremento di gittata di 3 metri.

Gli oggetti con ampio spazio per ricevere l’aggancio di un rampino hanno CA 5.

.. figure:: Immagini/ManualeDiGioco/EquipRazioniDaViaggio.png
    :width: 250
    :align: right

Razioni da Viaggio (al Giorno)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:Costo: 5ma
:Peso: 0.5Kg

.. Note::
    Pesa un quarto del valore se viene fatto per personaggi di taglia Piccola.

Rete da Pesca (2.25m)
^^^^^^^^^^^^^^^^^^^^^

:Costo: 4mo
:Peso: 2.5Kg

Sacco (Vuoto)
^^^^^^^^^^^^^

:Costo: 1ma
:Peso: 0.25Kg

.. Note::
    Pesa e ha una capacità di un quarto del valore se viene fatto per personaggi di taglia Piccola.

Sapone (per 0.5kg)
^^^^^^^^^^^^^^^^^^

:Costo: 5ma
:Peso: 0.5Kg

Scala a Pioli (3m)
^^^^^^^^^^^^^^^^^^

:Costo: 2ma
:Peso: 10Kg

.. figure:: Immagini/ManualeDiGioco/EquipSecchio.png
    :width: 150
    :align: left

Secchio (Vuoto)
^^^^^^^^^^^^^^^

:Costo: 5ma
:Peso: 1Kg

.. figure:: Immagini/ManualeDiGioco/EquipLucchetto.png
    :width: 150
    :align: right

Serratura o Lucchetto
^^^^^^^^^^^^^^^^^^^^^

La CD per aprire una serratura (o un lucchetto) con :ref:`disattivare_congegni` dipende dalla sue qualità:

    * Semplice CD 20
    * Media CD 25
    * Buona CD 30
    * Superiore CD 40

.. _equip_serratura_semplice:

Semplice
""""""""

:Costo: 20mo
:Peso: 0.5Kg

.. _equip_serratura_media:

Media
"""""

:Costo: 40mo
:Peso: 0.5Kg

.. _equip_serratura_buona:

Buona
"""""

:Costo: 80mo
:Peso: 0.5Kg

.. _equip_serratura_superiore:

Superiore
"""""""""

:Costo: 150mo
:Peso: 0.5Kg

Specchio Piccolo di Metallo
^^^^^^^^^^^^^^^^^^^^^^^^^^^

:Costo: 10mo
:Peso: 0.25Kg

.. _equip_tela:

Tela (m2)
^^^^^^^^^^^^^^^^

:Costo: 1ma
:Peso: 0.5Kg

Tenda
^^^^^

:Costo: 10mo
:Peso: 10Kg

.. Note::
    Pesa e ha una capacità di un quarto del valore se viene fatto per personaggi di taglia Piccola.

.. figure:: Immagini/ManualeDiGioco/EquipTorcia.png
    :width: 60
    :align: right

Torcia
^^^^^^

:Costo: 1mr
:Peso: 0.5Kg

Una torcia brucia per 1 ora ed illumina con luce normale un’area di 6 metri di raggio, aumentando
ulteriormente la luminosità di un livello (vedi :ref:`visibilità_luce`) in un raggio di 6 metri oltre quell’area, ma
non può aumentare il livello della luce oltre quella normale o intensa.

Se usata in combattimento, la torcia viene considerata un’:ref:`arma improvvisata<equip_arma_improvvisata>` a una mano
che infligge danni contundenti pari a quelli di un `Guanto d'Arme`_ della stessa taglia, più 1 danno da fuoco.

.. figure:: Immagini/ManualeDiGioco/EquipTriboli.png
    :width: 150
    :align: left

.. _equip_triboli:

Triboli
^^^^^^^

:Costo: 1mo
:Peso: 1Kg

I triboli sono chiodi di ferro a quattro punte costruiti in modo da avere sempre una punta rivolta verso l’alto
e si spargono sul terreno nella speranza che i nemici ci camminino sopra o almeno rallentino per evitarli.

Una borsa contenente 1 kg di triboli copre un’area quadrata con lato di 1,5 metri.
Ogni volta che ci si muove in un’area coperta con i triboli (o si passa un round combattendo mentre ci si trova nell’area),
si rischia di pestarne uno.
In tal caso i triboli effettuano un tiro per colpire (con BAB +0) contro la creatura senza considerare scudo, armatura e bonus di deviazione.
Se la creatura indossa le scarpe o qualche altra copertura per i piedi, ha bonus di armatura +2 alla CA.

Se i triboli riescono a colpire significa che la creatura ne ha pestato uno, subisce 1 danno e la sue velocità è dimezzata a causa del piede ferito.
Questa penalità al movimento dura 24 ore, fino a quando la creatura non viene curata con successo con una prova di :ref:`guarire` con CD 15
oppure riceve almeno 1 punto di cure magiche.

Una creatura alla carica o che sta correndo deve fermarsi immediatamente se pesta un tribolo.

.. Important::
    Qualsiasi creatura che si muova a velocità dimezzata o più lentamente può camminare attraverso una distesa di triboli senza problemi.

I triboli potrebbero essere inefficaci contro avversari insoliti.

.. figure:: Immagini/ManualeDiGioco/EquipZaino.png
    :width: 150
    :align: right

Zaino
^^^^^

:Costo: 2mo
:Peso: 1Kg

.. Note::
    Pesa e ha una capacità di un quarto del valore se viene fatto per personaggi di taglia Piccola.

Oggetti e Sostanze Speciali
---------------------------

Tutti gli oggetti e sostanze speciali, fatta eccezione per la `Torcia Inestinguibile`_ e l’`Acqua Santa (Ampolla)`_, possono essere
fabbricati da un personaggio con l’abilità :ref:`artigianato` (alchimia).

+------------------------------------+-------+--------+
| Nome                               | Costo | Peso   |
+====================================+=======+========+
| `Acido (Ampolla)`_                 | 10mo  | 0.5Kg  |
+------------------------------------+-------+--------+
| `Acqua Santa (Ampolla)`_           | 25mo  | 0.5Kg  |
+------------------------------------+-------+--------+
| `Antitossina (Boccetta)`_          | 50mo  |        |
+------------------------------------+-------+--------+
| `Bastone di Fumo`_                 | 20mo  | 0.25Kg |
+------------------------------------+-------+--------+
| `Borsa dell'Impedimento`_          | 50mo  | 2Kg    |
+------------------------------------+-------+--------+
| `Fuoco dell'Alchimista (Ampolla)`_ | 20 mo | 0.5Kg  |
+------------------------------------+-------+--------+
| `Pietra del Tuono`_                | 30mo  | 0.5Kg  |
+------------------------------------+-------+--------+
| `Tizzone Ardente`_                 | 1mo   |        |
+------------------------------------+-------+--------+
| `Torcia Inestinguibile`_           | 110mo | 0.5Kg  |
+------------------------------------+-------+--------+
| `Verga del Sole`_                  | 2mo   | 0.5Kg  |
+------------------------------------+-------+--------+


Acido (Ampolla)
^^^^^^^^^^^^^^^

:Costo: 10mo
:Peso: 0.5Kg

È possibile lanciare un’ampolla d’acido come :ref:`arma a spargimento<armi_a_spargimento>`.

Si consideri l’attacco come un :ref:`Attacco di Contatto a Distanza<attacco_contatto>` con incremento di gittata di 3 metri.

Il colpo diretto provoca 1d6 danni da acido e tutte le creature entro 1,5 metri dal punto in cui è caduto subiscono
1 danno da acido come effetto dello spargimento.

Acqua Santa (Ampolla)
^^^^^^^^^^^^^^^^^^^^^

:Costo: 25mo
:Peso: 0.5Kg

L’acquasanta infligge danni ai non morti e agli esterni malvagi quasi come se fosse acido e può essere
lanciata come un':ref:`arma a spargimento<armi_a_spargimento>`.

Si consideri l’attacco come un :ref:`Attacco di Contatto a Distanza<attacco_contatto>` con incremento di gittata di 3 metri.

.. Note::
    Un’ampolla si rompe se scagliata contro il corpo di una creatura corporea, ma contro una creatura incorporea l’ampolla deve essere
    aperta e versata sulla creatura.

    Di conseguenza, si può spruzzare una creatura incorporea con l’acquasanta solo se si è adiacenti ad essa e farlo è un
    :ref:`Attacco di Contatto a Distanza<attacco_contatto>` che non provoca :ref:`Attacchi di Opportunità<attacco_di_opportunità>`.

Il colpo diretto di un’ampolla di acquasanta provoca 2d4 danni ai non morti e agli esterni malvagi e tutte le creature di questo tipo entro 1,5 metri dal punto in cui è caduta l’ampolla subiscono 1 danno come effetto dello spargimento.

I templi di divinità buone vendono acquasanta a prezzo di costo (senza guadagno). L’acquasanta si ottiene usando :ref:`Benedire Acqua<inc_benedire_acqua>`.

Antitossina (Boccetta)
^^^^^^^^^^^^^^^^^^^^^^

:Costo: 50mo

Se si beve l’antitossina, si ottiene bonus alchemico +5 a tutti i tiri salvezza su Tempra contro veleni per 1 ora.

Bastone di Fumo
^^^^^^^^^^^^^^^

:Costo: 20mo
:Peso: 0.25Kg

Questo bastone di legno trattato con procedimento alchemico crea istantaneamente un denso fumo opaco quando viene infiammato.

Il fumo riempie un cubo con spigolo di 3 metri, come per l’effetto di :ref:`Nube di Nebbia<inc_nube_di_nebbia>`, tranne che il fumo viene dissipato in 1 round da un vento moderato o più intenso.

Il bastone è consumato dopo 1 round e il fumo si dissolve naturalmente.

Borsa dell'Impedimento
^^^^^^^^^^^^^^^^^^^^^^

:Costo: 50mo
:Peso: 2Kg

Questa borsa di cuoio rotonda è piena di melassa, resina o altra sostanza appiccicosa.

Quando si scaglia la borsa contro una creatura come :ref:`Attacco di Contatto a Distanza<attacco_contatto>` con incremento di gittata di 3 metri,
la borsa si apre e la sostanza contenuta invischia la vittima, diventando resistente ed elastica con l’esposizione all’aria.

Una creatura intralciata subisce –2 al tiro per colpire, –4 alla Destrezza e deve effettuare un tiro salvezza su Riflessi con CD 15 o
resta appiccicata al pavimento, incapace di muoversi. Anche con un tiro salvezza riuscito, può solo muoversi a velocità dimezzata.

.. Note::
    La sostanza non agisce su creature di taglia Enorme o superiore e una creatura volante non viene appiccicata al pavimento, ma deve effettuare un tiro salvezza su Riflessi con CD 15 o perde la capacità di volare (sempre che usi le ali per farlo), cadendo a terra.

La borsa dell’impedimento non funziona sott’acqua.

Una creatura colpita può liberarsi con una prova di Forza riuscita con CD 17 oppure infliggendo 15 danni alla sostanza con un’arma tagliente.
In caso tenti di sfregare via la sostanza da sè o da un’altra creatura che assiste non ha bisogno di effettuare un tiro per colpire poiché colpire la sostanza
è automatico, poi la creatura che colpisce effettua un tiro per i danni per vedere quanta sostanza è riuscita a sfregare via.

Una volta libera, la creatura si muove a velocità dimezzata, anche volando.

.. Important::
    Una creatura può lanciare incantesimi invischiata dalla sostanza ma deve superare una prova di concentrazione con CD 15 + Livello Incantesimo per riuscirci.

La sostanza diventa fragile dopo 2d4 round, staccandosi da sola e perdendo ogni effetto, ma un’applicazione di solvente universale su una creatura appiccicata dissolve la sostanza alchemica immediatamente.

Fuoco dell'Alchimista (Ampolla)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:Costo: 20mo
:Peso: 0.5Kg

Si può lanciare un’ampolla di fuoco dell’alchimista come :ref:`arma a spargimento<armi_a_spargimento>` con un :ref:`Attacco di Contatto a Distanza<attacco_contatto>` con incremento di gittata di 3 metri.

Il colpo diretto provoca 1d6 danni da fuoco e tutte le creature entro 1,5 metri dal punto in cui è caduta l’ampolla subiscono 1 danno da fuoco come effetto dello spargimento.

.. Important::
    Nel round successivo al colpo diretto la vittima subisce 1d6 danni aggiuntivi, ma può sfruttare un’:ref:`Azione di Round Completo<azione_di_round_completo>` per tentare di spegnere le fiamme prima di subire questi danni aggiuntivi con un tiro salvezza su Riflessi con CD 15.

    Rotolarsi per terra dà al personaggio bonus +2 al tiro salvezza, mentre tuffarsi in un lago o smorzare le fiamme con mezzi magici spegne automaticamente le fiamme.

Pietra del Tuono
^^^^^^^^^^^^^^^^

:Costo: 30mo
:Peso: 0.5Kg

Si può scagliare questa pietra con un :ref:`Attacco a Distanza<azione_attacco_a_distanza>` con incremento di gittata di 6 metri.

Quando colpisce una superficie dura (o è colpita con forza), crea un rumore assordante che equivale a un attacco sonoro: le creature presenti entro un raggio di 3 metri devono effettuare un tiro salvezza su Tempra con CD 15 o restano
:ref:`Assordate<assordato>` per 1 ora.

Le creature assordate, oltre alle ovvie conseguenze, subiscono penalità –4 all’iniziativa e una probabilità del 20% di sbagliare a lanciare e perdere qualsiasi incantesimo con una componente verbale che cercano di
lanciare.

.. Note::
    Dal momento che non è necessario colpire uno specifico bersaglio, si può mirare su un determinato quadretto con lato di 1,5 metri considerandolo come se avesse CA 5.

.. figure:: Immagini/ManualeDiGioco/EquipTizzoniArdenti.png
    :width: 150
    :align: right

Tizzone Ardente
^^^^^^^^^^^^^^^

:Costo: 1mo

La sostanza alchemica sulla punta di questo piccolo bastone di legno si infiamma quando viene sfregata contro una superficie ruvida.

Creare una fiamma con un tizzone ardente è molto più rapido che crearla con acciarino, pietra focaia (o lente d’ingrandimento) e esca: accendere una Torcia_ con un tizzone ardente è un’:ref:`azione_standard` invece che di :ref:`round completo<azione_di_round_completo>` e per accendere qualsiasi altro
fuoco occorre almeno un’:ref:`azione_standard`.

Torcia Inestinguibile
^^^^^^^^^^^^^^^^^^^^^

:Costo: 110mo
:Peso: 0.5Kg

Si tratta di una normale Torcia_ su cui è stato lanciato :ref:`Fiamma Perenne<inc_fiamma_perenne>`.

Illumina distintamente come una comune Torcia_, ma non emette calore e non infligge danni da fuoco se usata come arma.

.. figure:: Immagini/ManualeDiGioco/EquipVergaDelSole.png
    :width: 75
    :align: right

Verga del Sole
^^^^^^^^^^^^^^

:Costo: 2mo
:Peso: 0.5Kg

Questa verga di ferro lunga 30 cm e con la punta dorata risplende vivacemente quando viene percossa con un’:ref:`azione_standard` e
illumina con luce normale un’area di 9 metri di raggio, aumentando ulteriormente la luminosità di un livello in un raggio di 9 metri oltre quell’area.

Una verga del sole non può aumentare il livello della luce oltre quella intensa e brilla per 6 ore, dopodiché la punta dorata si consuma e diventa inutile.

Attrezzi di Classe e Abilità
----------------------------

Questo equipaggiamento è particolarmente utile se si possiedono certe abilità o se si appartiene a una certa classe.

+----------------------------------------+-------+-------+---------------------------------+
| Nome                                   | Costo | Peso  | :ref:`Note<note_merci_servizi>` |
+========================================+=======+=======+=================================+
| `Agrifoglio e Vischio`_                |       |       |                                 |
+----------------------------------------+-------+-------+---------------------------------+
| `Arnesi da Artigiano`_                 | 5mo   | 2.5Kg |                                 |
+----------------------------------------+-------+-------+---------------------------------+
| `Arnesi da Artigiano Perfetti`_        | 55mo  | 2.5Kg |                                 |
+----------------------------------------+-------+-------+---------------------------------+
| `Arnesi da Scasso`_                    | 30mo  | 0.5Kg |                                 |
+----------------------------------------+-------+-------+---------------------------------+
| `Arnesi da Scasso Perfetti`_           | 100mo | 1Kg   |                                 |
+----------------------------------------+-------+-------+---------------------------------+
| `Attrezzi da Scalatore`_               | 80mo  | 2.5Kg |                                 |
+----------------------------------------+-------+-------+---------------------------------+
| `Attrezzi Perfetti`_                   | 50mo  | 0.5Kg |                                 |
+----------------------------------------+-------+-------+---------------------------------+
| `Bilancia da Mercante`_                | 2mo   | 0.5Kg |                                 |
+----------------------------------------+-------+-------+---------------------------------+
| `Borsa del Guaritore`_                 | 50mo  | 0.5Kg |                                 |
+----------------------------------------+-------+-------+---------------------------------+
| `Borsa per Componenti di Incantesimi`_ | 5mo   | 1Kg   |                                 |
+----------------------------------------+-------+-------+---------------------------------+
| `Laboratorio da Alchimista`_           | 200mo | 20Kg  |                                 |
+----------------------------------------+-------+-------+---------------------------------+
| `Lente d'ingrandimento`_               | 100mo |       |                                 |
+----------------------------------------+-------+-------+---------------------------------+
| `Libro degli Incantesimi (vuoto)`_     | 15mo  | 1.5Kg |                                 |
+----------------------------------------+-------+-------+---------------------------------+
| `Simbolo Sacro d'Argento`_             | 25mo  | 0.5Kg |                                 |
+----------------------------------------+-------+-------+---------------------------------+
| `Simbolo Sacro di Legno`_              | 1mo   |       |                                 |
+----------------------------------------+-------+-------+---------------------------------+
| `Strumento Musicale Comune`_           | 5mo   | 1.5Kg | 1                               |
+----------------------------------------+-------+-------+---------------------------------+
| `Strumento Musicale Perfetto`_         | 100mo | 1.5Kg | 1                               |
+----------------------------------------+-------+-------+---------------------------------+
| `Trucchi per il Camuffamento`_         | 50mo  | 4Kg   | 1                               |
+----------------------------------------+-------+-------+---------------------------------+

Agrifoglio e Vischio
^^^^^^^^^^^^^^^^^^^^
I :ref:`druidi<druido>` usano comunemente queste piante come focus divino quando lanciano gli incantesimi.

.. _equip_arnesi_da_artigiano:

Arnesi da Artigiano
^^^^^^^^^^^^^^^^^^^

:Costo: 5mo
:Peso: 2.5Kg

Questo è un set di arnesi speciali necessari per qualsiasi lavoro artigianale.

Senza questi arnesi bisogna usare arnesi improvvisati (penalità –2 alle prove di :ref:`Artigianato`) se si e costretti a realizzare comunque il lavoro.

Arnesi da Artigiano Perfetti
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:Costo: 55mo
:Peso: 2.5Kg

Come gli `Arnesi da Artigiano`_  questi sono gli arnesi perfetti per il lavoro, quindi forniscono bonus di circostanza +2 alle prove di :ref:`Artigianato` effettuate usandoli.

.. figure:: Immagini/ManualeDiGioco/EquipArnesiDaScasso.png
    :width: 250
    :align: right

.. _equip_arnesi_da_scasso:

Arnesi da Scasso
^^^^^^^^^^^^^^^^

:Costo: 30mo
:Peso: 0.5Kg

Il kit include grimaldelli e altri attrezzi da impiegare quando si usa :ref:`disattivare_congegni`.
Senza questi arnesi se ne devono utilizzare di improvvisati e si subisce penalità di circostanza –2 alle prove di :ref:`disattivare_congegni`.

.. _equip_arnesi_da_scasso_perfetti:

Arnesi da Scasso Perfetti
^^^^^^^^^^^^^^^^^^^^^^^^^

:Costo: 100mo
:Peso: 1Kg

Questo kit contiene arnesi di fattura migliore che garantiscono bonus di circostanza +2 alle prove di :ref:`disattivare_congegni`.

Attrezzi da Scalatore
^^^^^^^^^^^^^^^^^^^^^

:Costo: 80mo
:Peso: 2.5Kg

Questi chiodi, corde e ramponi forniscono bonus di circostanza +2 alle prove di :ref:`scalare`.

Attrezzi Perfetti
^^^^^^^^^^^^^^^^^

:Costo: 50mo
:Peso: 0.5Kg

Questo oggetto di ottima fattura è l’attrezzo ideale per il lavoro richiesto e aggiunge bonus di circostanza +2 alla relativa prova di abilità (se necessaria).

I bonus forniti da molteplici oggetti perfetti utilizzati per la stessa prova di abilità non si sommano.

.. figure:: Immagini/ManualeDiGioco/EquipBilanciaDaMercante.png
    :width: 200
    :align: right

Bilancia da Mercante
^^^^^^^^^^^^^^^^^^^^

:Costo: 2mo
:Peso: 0.5Kg

Una bilancia da mercante garantisce bonus di circostanza +2 alle prove di :ref:`Valutare` per gli oggetti la cui stima avviene in base al peso, compresa qualsiasi cosa fatta di metallo prezioso.

.. _equip_borsa_del_guaritore:

Borsa del Guaritore
^^^^^^^^^^^^^^^^^^^

:Costo: 50mo
:Peso: 0.5Kg

Questa borsa piena di erbe, pomate e bende fornisce bonus di circostanza +2 alle prove di :ref:`Guarire`.

Viene consumata dopo dieci utilizzi.

.. figure:: Immagini/ManualeDiGioco/EquipLaboratorioDaAlchimista.png
    :width: 250
    :align: right

.. _equip_borsa_componenti:

Borsa per Componenti di Incantesimi
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:Costo: 5mo
:Peso: 1Kg

Si presume che un incantatore con una borsa per componenti di incantesimi abbia tutte le componenti materiali e focus di cui ha bisogno, tranne quelli che hanno un costo elencato, focus divini o focus che non starebbero in una borsa.

.. _equip_lab_alchimista:

Laboratorio da Alchimista
^^^^^^^^^^^^^^^^^^^^^^^^^

:Costo: 200mo
:Peso: 20Kg

Questa è l’attrezzatura perfetta per creare oggetti alchemici e fornisce bonus di circostanza +2 a qualsiasi prova di :ref:`Artigianato` (alchimia), ma non ha peso sui suoi costi.

Senza questo laboratorio, un personaggio con :ref:`Artigianato` (alchimia) ha comunque abbastanza attrezzi per utilizzare l’abilità, ma senza bonus.

Lente d'Ingrandimento
^^^^^^^^^^^^^^^^^^^^^

:Costo: 100mo

Questa semplice lente consente di osservare oggetti piccoli.

È utile come sostituto di `Acciarino e pietra Focaia`_ e esca quando si accendono fuochi.
Per appiccare un fuoco ci vuole luce luminosa come la luce del sole diretta per focalizzare, esca da infiammare e almeno un’:ref:`azione_di_round_completo`.

Fornisce bonus di circostanza +2 alle prove di :ref:`Valutare` qualsiasi oggetto che sia piccolo o molto dettagliato, come una pietra preziosa.

.. figure:: Immagini/ManualeDiGioco/EquipLibroDegliIncantesimi.png
    :width: 250
    :align: right

Libro degli Incantesimi (Vuoto)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:Costo: 15mo
:Peso: 1.5Kg

Un libro degli incantesimi ha 100 pagine di pergamena e ogni incantesimo occupa due pagine per livello. Una, per gli incantesimi di livello 0.

Simbolo Sacro d'Argento
^^^^^^^^^^^^^^^^^^^^^^^

:Costo: 25mo
:Peso: 0.5Kg

Un simbolo sacro focalizza energia positiva ed è usato dai :ref:`chierici<chierico>` buoni (o neutrali che vogliono lanciare :ref:`incantesimi buoni<incantesimi_buoni_caotici_etc>` o :ref:`incanalare energia positiva<incanalare_energia>`) e dai :ref:`paladini<paladino>`.

.. Note::
    Ogni religione ha il proprio simbolo sacro.

.. rubric:: Simboli Sacrileghi

Un simbolo sacrilego è come un simbolo sacro tranne che focalizza energia negativa e viene utilizzato da chierici malvagi (o neutrali che vogliono lanciare :ref:`incantesimi malvagi<incantesimi_buoni_caotici_etc>` o :ref:`incanalare energia negativa<incanalare_energia>`).

Simbolo Sacro di Legno
^^^^^^^^^^^^^^^^^^^^^^

:Costo: 1mo

Equivalente a un `Simbolo Sacro d'Argento`_.

Strumento Musicale Comune
^^^^^^^^^^^^^^^^^^^^^^^^^

:Costo: 5mo
:Peso: 1.5Kg

Strumento necessario per effettuare prove di :ref:`Intrattenere` basate su strumenti.

.. Note::
    Pesa un quarto del valore se viene fatto per personaggi di taglia Piccola.

.. figure:: Immagini/ManualeDiGioco/EquipStrumentoMusicale.png
    :width: 100
    :align: right

Strumento Musicale Perfetto
^^^^^^^^^^^^^^^^^^^^^^^^^^^

:Costo: 100mo
:Peso: 1.5Kg

Uno strumento perfetto fornisce bonus di circostanza +2 alle prove di :ref:`Intrattenere` in cui viene utilizzato.

.. Note::
    Pesa un quarto del valore se viene fatto per personaggi di taglia Piccola.

Trucchi per il Camuffamento
^^^^^^^^^^^^^^^^^^^^^^^^^^^

:Costo: 50mo
:Peso: 4Kg

Questa è l’attrezzatura perfetta per camuffarsi e fornisce bonus di circostanza +2 alle prove di :ref:`Camuffare`.

Viene consumata dopo dieci utilizzi.

.. Note::
    Pesa un quarto del valore se viene fatto per personaggi di taglia Piccola.

Vestiario
---------
Si presuppone che un personaggio inizi il gioco con un abito del valore di 10mo o meno. 

Abiti addizionali possono essere comprati normalmente.

.. Note::
    Tutti gli abiti pesano un quarto del valore se vengono fatti per personaggi di taglia Piccola.

+----------------------------+-------+-------+---------------------------------+
| Nome                       | Costo | Peso  | :ref:`Note<note_merci_servizi>` |
+============================+=======+=======+=================================+
| `Abito da Artigiano`_      | 1mo   | 2Kg   | 1                               |
+----------------------------+-------+-------+---------------------------------+
| `Abito da Contadino`_      | 1ma   | 1Kg   | 1                               |
+----------------------------+-------+-------+---------------------------------+
| `Abito da Cortigiano`_     | 30mo  | 3Kg   | 1                               |
+----------------------------+-------+-------+---------------------------------+
| `Abito da Esploratore`_    | 10mo  | 4Kg   | 1                               |
+----------------------------+-------+-------+---------------------------------+
| `Abito da Intrattenitore`_ | 3mo   | 2Kg   | 1                               |
+----------------------------+-------+-------+---------------------------------+
| `Abito da Monaco`_         | 5mo   | 1Kg   | 1                               |
+----------------------------+-------+-------+---------------------------------+
| `Abito da Nobile`_         | 75mo  | 5Kg   | 1                               |
+----------------------------+-------+-------+---------------------------------+
| `Abito da Studioso`_       | 5mo   | 3Kg   | 1                               |
+----------------------------+-------+-------+---------------------------------+
| `Abito da Viaggiatore`_    | 1mo   | 2.5Kg | 1                               |
+----------------------------+-------+-------+---------------------------------+
| `Abito Invernale`_         | 8mo   | 3.5Kg | 1                               |
+----------------------------+-------+-------+---------------------------------+
| `Abito Reale`_             | 200mo | 7.5Kg | 1                               |
+----------------------------+-------+-------+---------------------------------+
| `Veste da Chierico`_       | 5mo   | 3Kg   | 1                               |
+----------------------------+-------+-------+---------------------------------+

Abito da Artigiano
^^^^^^^^^^^^^^^^^^

:Costo: 1mo
:Peso: 2Kg

Una camicia con bottoni, una gonna o pantaloni con i lacci, scarpe e forse un cappello o un berretto.

Quest’abito può includere anche una cintura o un grembiule di pelle o di stoffa per tenere gli attrezzi

.. Note::
    Pesa un quarto del valore se viene fatto per personaggi di taglia Piccola.

Abito da Contadino
^^^^^^^^^^^^^^^^^^

:Costo: 1ma
:Peso: 1Kg

Un’ampia camicia e calzoni sformati di stoffa oppure un’ampia camicia e una gonna o sopravveste.

Fasce di stoffa usate come scarpe.

.. Note::
    Pesa un quarto del valore se viene fatto per personaggi di taglia Piccola.

Abito da Cortigiano
^^^^^^^^^^^^^^^^^^^

:Costo: 30mo
:Peso: 3Kg

Eleganti abiti di sartoria in qualsiasi moda o qualunque sia lo stile diffuso nelle corti dei nobili.

Chiunque tenti di influenzare nobili o cortigiani, mentre indossa abiti da strada, incontrerà difficoltà che si manifestano in una penalità –2 a tutte le prove basate su Carisma per esercitare influenza su queste persone.

Senza gioielli (che costano circa 50mo aggiuntivi) si ha l’apparenza di una persona comune fuori posto.

.. Note::
    Pesa un quarto del valore se viene fatto per personaggi di taglia Piccola.

Abito da Esploratore
^^^^^^^^^^^^^^^^^^^^

:Costo: 10mo
:Peso: 4Kg

Questo è un corredo completo di abiti per qualcuno che non sa mai cosa lo aspetta.

Comprende stivali robusti, calzoni o gonna di pelle, una cintura, una camicia, magari un panciotto o una giubba, guanti e un mantello.
Piuttosto che una gonna di pelle, si può indossare una sopravveste di pelle sopra la gonna di stoffa.

Gli abiti hanno parecchie tasche, soprattutto il mantello e il corredo include anche qualsiasi accessorio che possa essere utile, come una sciarpa o un cappello a tesa larga.

.. Note::
    Pesa un quarto del valore se viene fatto per personaggi di taglia Piccola.

Abito da Intrattenitore
^^^^^^^^^^^^^^^^^^^^^^^

:Costo: 3mo
:Peso: 2Kg

Un corredo di abiti vistosi e forse anche appariscenti per fare spettacolo.

Anche se sembrano stravaganti, il loro taglio decisamente pratico permette di compiere acrobazie, ballare, camminare sulla corda o anche solo correre, magari perché il pubblico diventa minaccioso.

.. Note::
    Pesa un quarto del valore se viene fatto per personaggi di taglia Piccola.

Abito da Monaco
^^^^^^^^^^^^^^^

:Costo: 5mo
:Peso: 1Kg

Questi semplici abiti comprendono sandali, calzoni larghi e una camicia ampia, tenuti insieme da fasce.

Sono ideati per dare massima mobilità e sono fatti con stoffa di alta qualità.
Si possono inoltre nascondere piccole armi nelle tasche celate nelle pieghe e le fasce sono abbastanza resistenti da servire come corde corte.

.. Note::
    Pesa un quarto del valore se viene fatto per personaggi di taglia Piccola.

Abito da Nobile
^^^^^^^^^^^^^^^

:Costo: 75mo
:Peso: 5Kg

Questo corredo di abiti è disegnato specificamente per essere costoso e per essere esibito, con metalli e pietre preziose lavorati nella stoffa.

Per inserirsi in un ambiente nobiliare, ogni aspirante nobile ha bisogno anche di un anello con sigillo e di gioielli (del valore di almeno 100 mo).

.. Note::
    Pesa un quarto del valore se viene fatto per personaggi di taglia Piccola.

Abito da Studioso
^^^^^^^^^^^^^^^^^

:Costo: 5mo
:Peso: 3Kg

Un abito lungo, una cintura, un cappello, scarpe morbide e possibilmente un mantello, sono perfettamente adatti per chi studia.

.. Note::
    Pesa un quarto del valore se viene fatto per personaggi di taglia Piccola.

Abito da Viaggiatore
^^^^^^^^^^^^^^^^^^^^

:Costo: 1mo
:Peso: 2.5Kg

Stivali, una gonna o pantaloni di lana, una robusta cintura, una camicia, magari con panciotto o giubba, e un ampio mantello con cappuccio.

.. Note::
    Pesa un quarto del valore se viene fatto per personaggi di taglia Piccola.

Abito Invernale
^^^^^^^^^^^^^^^

:Costo: 8mo
:Peso: 3.5Kg

Un soprabito di lana, camicia di lino, cappello di lana, mantello pesante, pantaloni o gonna pesanti e stivali.

Quando si indossano abiti invernali, si aggiunge bonus di circostanza +5 ai tiri salvezza su Tempra contro l’esposizione al freddo.

.. Note::
    Pesa un quarto del valore se viene fatto per personaggi di taglia Piccola.

Abito Reale
^^^^^^^^^^^

:Costo: 200mo
:Peso: 7.5Kg

Questi sono solo gli abiti, non lo scettro, la corona, l’anello e altri oggetti reali.

Gli abiti regali sono ostentati, con pietre preziose, oro, seta e pelliccia in abbondanza.

.. Note::
    Pesa un quarto del valore se viene fatto per personaggi di taglia Piccola.

Veste da Chierico
^^^^^^^^^^^^^^^^^

:Costo: 5mo
:Peso: 3Kg

Abiti ecclesiastici per adempiere alle funzioni sacerdotali, non per andare all’avventura.

Comprendono tonaca, cotta e stola.

.. Note::
    Pesa un quarto del valore se viene fatto per personaggi di taglia Piccola.

Vitto e Alloggio
----------------

Questi prezzi sono per vitto e alloggio nei locali commerciali di una città di media grandezza.

+---------------------------------------------------------------+-------+--------+
| Nome                                                          | Costo | Peso   |
+===============================================================+=======+========+
| `Banchetto (a Persona)`_                                      | 10mo  |        |
+---------------------------------------------------------------+-------+--------+
| :ref:`Birra (Boccale)<equip_birra_boccale>`                   | 4mr   | 0.5Kg  |
+---------------------------------------------------------------+-------+--------+
| :ref:`Birra (Caraffa)<equip_birra_caraffa>`                   | 2ma   | 4Kg    |
+---------------------------------------------------------------+-------+--------+
| `Carne (1 Pezzo)`_                                            | 3ma   | 0.25Kg |
+---------------------------------------------------------------+-------+--------+
| `Formaggio (1 Pezzo)`_                                        | 1ma   | 0.25Kg |
+---------------------------------------------------------------+-------+--------+
| :ref:`Locanda (al Giorno) - Buona<equip_locanda_buona>`       | 2mo   |        |
+---------------------------------------------------------------+-------+--------+
| :ref:`Locanda (al Giorno) - Normale<equip_locanda_normale>`   | 5ma   |        |
+---------------------------------------------------------------+-------+--------+
| :ref:`Locanda (al Giorno) - Scadente<equip_locanda_scadente>` | 2ma   |        |
+---------------------------------------------------------------+-------+--------+
| `Pane (a Pagnotta)`_                                          | 2mr   | 0.25Kg |
+---------------------------------------------------------------+-------+--------+
| :ref:`Pasti (al Giorno) - Buono<equip_pasto_buono>`           | 5ma   |        |
+---------------------------------------------------------------+-------+--------+
| :ref:`Pasti (al Giorno) - Normale<equip_pasto_normale>`       | 3ma   |        |
+---------------------------------------------------------------+-------+--------+
| :ref:`Pasti (al Giorno) - Scadente<equip_pasto_scadente>`     | 1ma   |        |
+---------------------------------------------------------------+-------+--------+
| :ref:`Vino - Buono (Bottiglia)<equip_vino_buono>`             | 10mo  | 0.25Kg |
+---------------------------------------------------------------+-------+--------+
| :ref:`Vino - Comune (Caraffa)<equip_vino_comune>`             | 2ma   | 3Kg    |
+---------------------------------------------------------------+-------+--------+

Banchetto (a Persona)
^^^^^^^^^^^^^^^^^^^^^

:Costo: 10mo

Birra
^^^^^

.. figure:: Immagini/ManualeDiGioco/EquipBoccale.png
    :width: 200
    :align: right

.. _equip_birra_boccale:

Boccale
"""""""

:Costo: 4mr
:Peso: 0.5Kg

.. _equip_birra_caraffa:

Caraffa
"""""""

:Costo: 2ma
:Peso: 4Kg

Carne (1 Pezzo)
^^^^^^^^^^^^^^^

:Costo: 3ma
:Peso: 0.25Kg

Formaggio (1 Pezzo)
^^^^^^^^^^^^^^^^^^^

:Costo: 1ma
:Peso: 0.25Kg

Locanda (al Giorno)
^^^^^^^^^^^^^^^^^^^

.. _equip_locanda_buona:

Buona
"""""

:Costo: 2mo

Una piccola stanza privata con un letto, qualche comodità e un vaso da notte coperto in un angolo.

.. _equip_locanda_normale:

Normale
"""""""

:Costo: 5ma

Un posto sul pavimento sollevato e riscaldato, con una coperta e un cuscino.

.. _equip_locanda_scadente:

Scadente
""""""""

:Costo: 2ma

Un posto sul pavimento vicino al camino.

Pane (a Pagnotta)
^^^^^^^^^^^^^^^^^

:Costo: 2mr
:Peso: 0.25Kg

Pasti (al Giorno)
^^^^^^^^^^^^^^^^^

.. _equip_pasto_buono:

Buono
"""""

:Costo: 5ma

Pane e dolci, manzo, piselli e birra o vino.

.. _equip_pasto_normale:

Normale
"""""""

:Costo: 3ma

Pane, stufato di pollo, carote e birra o vino annacquati.

.. _equip_pasto_scadente:

Scadente
""""""""

:Costo: 1ma

Pane, rape cotte, cipolle e acqua.

Vino
^^^^

.. _equip_vino_buono:

Buono (Bottiglia)
"""""""""""""""""

:Costo: 10mo
:Peso: 0.25Kg

.. _equip_vino_comune:

Comune (Caraffa)
""""""""""""""""

:Costo: 2ma
:Peso: 3Kg

Cavalcature e Equipaggiamento
-----------------------------

Queste sono le cavalcature comuni che si possono trovare nelle città. 

Alcune città potrebbero avere delle cavalcature in più, come cammelli o perfino grifoni, in base alla zona in cui si trovano, 
ma queste scelte addizionali sono a discrezione del GM.

.. Note::
    Le regole per le altre creature si trovano nel :ref:`Bestiario<bestiario>`.

+-----------------------------------------------------------------+-------+--------+---------------------------------+
| Nome                                                            | Costo | Peso   | :ref:`Note<note_merci_servizi>` |
+=================================================================+=======+========+=================================+
| `Asino o Mulo`_                                                 | 8mo   |        |                                 |
+-----------------------------------------------------------------+-------+--------+---------------------------------+
| :ref:`Bardatura - Creatura Media<equip_bardatura_media>`        | x2    | x1     | 2                               |
+-----------------------------------------------------------------+-------+--------+---------------------------------+
| :ref:`Bardatura - Creatura Grande<equip_bardatura_grande>`      | x4    | x2     | 2                               |
+-----------------------------------------------------------------+-------+--------+---------------------------------+
| `Cane da Galoppo`_                                              | 150mo |        |                                 |
+-----------------------------------------------------------------+-------+--------+---------------------------------+
| `Cane da Guardia`_                                              | 25mo  |        |                                 |
+-----------------------------------------------------------------+-------+--------+---------------------------------+
| `Cavallo Leggero`_                                              | 75mo  |        |                                 |
+-----------------------------------------------------------------+-------+--------+---------------------------------+
| `Cavallo Leggero (Addestrato al Combattimento)`_                | 110mo |        |                                 |
+-----------------------------------------------------------------+-------+--------+---------------------------------+
| `Cavallo Pesante`_                                              | 200mo |        |                                 |
+-----------------------------------------------------------------+-------+--------+---------------------------------+
| `Cavallo Pesante (Addestrato al Combattimento)`_                | 300mo |        |                                 |
+-----------------------------------------------------------------+-------+--------+---------------------------------+
| `Pony`_                                                         | 30mo  |        |                                 |
+-----------------------------------------------------------------+-------+--------+---------------------------------+
| `Pony (Addestrato al Combattimento)`_                           | 45mo  |        |                                 |
+-----------------------------------------------------------------+-------+--------+---------------------------------+
| `Morso e Briglie`_                                              | 2mo   | 0.5Kg  |                                 |
+-----------------------------------------------------------------+-------+--------+---------------------------------+
| `Nutrimento (al Giorno)`_                                       | 5mr   | 5Kg    |                                 |
+-----------------------------------------------------------------+-------+--------+---------------------------------+
| `Sacche da Sella`_                                              | 4mo   | 4Kg    |                                 |
+-----------------------------------------------------------------+-------+--------+---------------------------------+
| :ref:`Sella da Carico<equip_sella_da_carico>`                   | 5mo   | 7.5Kg  |                                 |
+-----------------------------------------------------------------+-------+--------+---------------------------------+
| :ref:`Sella da Galoppo<equip_sella_da_galoppo>`                 | 10mo  | 12.5Kg |                                 |
+-----------------------------------------------------------------+-------+--------+---------------------------------+
| :ref:`Sella Militare<equip_sella_militare>`                     | 20mo  | 15Kg   |                                 |
+-----------------------------------------------------------------+-------+--------+---------------------------------+
| :ref:`Sella Esotica da Carico<equip_sella_esotica_da_carico>`   | 15mo  | 10Kg   |                                 |
+-----------------------------------------------------------------+-------+--------+---------------------------------+
| :ref:`Sella Esotica da Galoppo<equip_sella_esotica_da_galoppo>` | 30mo  | 15Kg   |                                 |
+-----------------------------------------------------------------+-------+--------+---------------------------------+
| :ref:`Sella Esotica Militare<equip_sella_esotica_militare>`     | 60mo  | 20Kg   |                                 |
+-----------------------------------------------------------------+-------+--------+---------------------------------+
| `Stallaggio (al Giorno)`_                                       | 5ma   |        |                                 |
+-----------------------------------------------------------------+-------+--------+---------------------------------+

Asino o Mulo
^^^^^^^^^^^^

:Costo: 8mo

L’asino e il mulo sono imperturbabili di fronte al pericolo, coraggiosi, dal piede fermo e capaci di trasportare carichi pesanti per grandi distanze.

.. Important::
    Diversamente dai cavalli, sono disposti (ma non impazienti) ad entrare nei dungeon o in altri posti strani o minacciosi.

Bardatura
^^^^^^^^^

.. _equip_tabella_bardature:

.. figure:: Immagini/ManualeDiGioco/EquipTabVelocitàBardatura.png
    :width: 300
    :align: left

La bardatura è semplicemente un tipo di armatura che copre la testa, il collo, l’addome, il corpo e possibilmente le zampe di un cavallo o di un’altra cavalcatura.

Tipi più pesanti forniscono una migliore protezione a discapito della velocità e sono realizzate con :ref:`ogni tipo di armatura<equip_tabella_armature>`.
Come per qualsiasi creatura non umanoide di taglia Grande, un’armatura per un cavallo costa quattro volte il costo di quella di un :ref:`umano<umani>`, che è una creatura umanoide di taglia Media, e pesa il doppio.
Se la bardatura è per un pony o per un’altra creatura di taglia Media, il costo è solo il doppio e il peso è lo stesso di un’armatura Media indossata da un umanoide.

Bardature medie o pesanti rallentano le cavalcature come mostrato in :ref:`Tabella<equip_tabella_bardature>`, ma cavalcature volanti non possono volare con bardature medie o pesanti.

.. Warning::
    Per mettere e togliere la bardatura occorre cinque volte il tempo :ref:`normale<equip_indossare_armatura>`.

Gli animali bardati non possono essere usati per trasportare carichi che non siano il cavaliere e le normali sacche da sella.

.. _equip_bardatura_media:

Creatura Media
""""""""""""""

:Costo: x2
:Peso: x1

.. Note::
    Il moltiplicatore è relativo ad armature simili fatte per umanoidi di taglia Media.

.. _equip_bardatura_grande:

Creatura Grande
"""""""""""""""

:Costo: x4
:Peso: x2

.. Note::
    Il moltiplicatore è relativo ad armature simili fatte per umanoidi di taglia Media.

Cane da Galoppo
^^^^^^^^^^^^^^^

:Costo: 150mo

Questo cane di taglia Media è addestrato in modo particolare per trasportare un cavaliere umanoide Piccolo.

È coraggioso in combattimento come un cavallo da guerra e, data la statura, non si subiscono danni quando si cade di sella.

Cane da Guardia
^^^^^^^^^^^^^^^

:Costo: 25mo

Cavalli
^^^^^^^
Un cavallo è adatto come cavalcatura per :ref:`umani`, :ref:`elfi`, :ref:`mezzelfi`, :ref:`mezzorchi` e :ref:`nani`,
mentre un pony è più piccolo ed è una cavalcatura adatta per :ref:`gnomi` e :ref:`halfling`.

I cavalli da guerra e i pony da guerra possono essere cavalcati facilmente in combattimento
(vedi :ref:`addestrare_animali` per una lista di comandi che possono conoscere se addestrati per il combattimento).

Cavallo Leggero
"""""""""""""""

:Costo: 75mo

Cavallo Leggero (Addestrato al Combattimento)
"""""""""""""""""""""""""""""""""""""""""""""

:Costo: 110mo

Cavallo Pesante
"""""""""""""""

:Costo: 200mo

Cavallo Pesante (Addestrato al Combattimento)
"""""""""""""""""""""""""""""""""""""""""""""

:Costo: 300mo

Pony
""""

:Costo: 30mo

Pony (Addestrato al Combattimento)
""""""""""""""""""""""""""""""""""

:Costo: 45mo

Morso e Briglie
^^^^^^^^^^^^^^^

:Costo: 2mo
:Peso: 0.5Kg

Nutrimento (al Giorno)
^^^^^^^^^^^^^^^^^^^^^^

:Costo: 5mr
:Peso: 5Kg

Cavalli, asini, muli e pony possono pascolare per nutrirsi, ma è molto meglio procurare loro il cibo.

Se si possiede un cane da galoppo, bisogna nutrirlo almeno con un po’ di carne.

Sacche da Sella
^^^^^^^^^^^^^^^^^^^^^^

:Costo: 4mo
:Peso: 4Kg

Selle
^^^^^

.. _equip_sella_da_carico:

Da Carico
"""""""""

:Costo: 5mo
:Peso: 7.5Kg

Una sella da carico porta equipaggiamento e provviste, non un cavaliere e tiene tanto equipaggiamento quanto la cavalcatura può trasportare.

.. _equip_sella_da_galoppo:

Da Galoppo
""""""""""

:Costo: 10mo
:Peso: 12.5Kg

Se si viene colpiti e si perdono i sensi mentre si è su una sella da galoppo, si ha una probabilità del 50% di rimanere in sella.

.. _equip_sella_militare:

Militare
""""""""

:Costo: 20mo
:Peso: 15Kg

Una sella militare cinge il cavaliere aggiungendo bonus di circostanza +2 alle prove di :ref:`cavalcare` legate al fatto di rimanere in sella.

Se si viene colpiti e si perdono i sensi mentre si è su una sella militare, si ha una probabilità del 75% di rimanere in sella.

Selle Esotiche
^^^^^^^^^^^^^^

.. _equip_sella_esotica_da_carico:

Una sella esotica è ideata per una cavalcatura insolita e si trova in stile militare, da carico e da galoppo.

Da Carico
"""""""""

:Costo: 15mo
:Peso: 10Kg

Vedi :ref:`l'equivalente normale<equip_sella_da_carico>`.

.. _equip_sella_esotica_da_galoppo:

Da Galoppo
""""""""""

:Costo: 30mo
:Peso: 15Kg

Vedi :ref:`l'equivalente normale<equip_sella_da_galoppo>`.

.. _equip_sella_esotica_militare:

Militare
""""""""

:Costo: 60mo
:Peso: 20Kg

Vedi :ref:`l'equivalente normale<equip_sella_militare>`.

Stallaggio (al Giorno)
^^^^^^^^^^^^^^^^^^^^^^

:Costo: 5ma

Trasporti
---------

I prezzi indicati sono per comprare il veicolo, escluso ciurme o animali.

+-------------------+----------+-------+
| Nome              | Costo    | Peso  |
+===================+==========+=======+
| `Barca a Remi`_   | 50mo     | 50Kg  |
+-------------------+----------+-------+
| Barcone_          | 3000mo   |       |
+-------------------+----------+-------+
| Carretto_         | 15mo     | 100Kg |
+-------------------+----------+-------+
| Carro_            | 35mo     | 200Kg |
+-------------------+----------+-------+
| Carrozza_         | 100mo    | 300Kg |
+-------------------+----------+-------+
| Galea_            | 30000mo  |       |
+-------------------+----------+-------+
| `Nave a Vela`_    | 10000 mo |       |
+-------------------+----------+-------+
| `Nave da Guerra`_ | 25000mo  |       |
+-------------------+----------+-------+
| `Nave Lunga`_     | 10000mo  |       |
+-------------------+----------+-------+
| Remo_             | 2mo      | 5Kg   |
+-------------------+----------+-------+
| Slitta_           | 20mo     | 150Kg |
+-------------------+----------+-------+

Barca a Remi
^^^^^^^^^^^^

:Costo: 50mo
:Peso: 50Kg

Una barca lunga tra i 2.4 e i 3.6 metri a due remi, per due o tre persone di taglia Media.

Si muove alla velocità di 2.25 km/h.

Barcone
^^^^^^^

:Costo: 3000mo

Una barca lunga tra i 15 e i 22.5 metri e larga tra i 4.5 e i 6 metri.

Dotata di pochi remi per integrare il suo unico albero con vela quadrata, ha un equipaggio variabile
dalle 8 alle 15 unità e può trasportare dalle 40 alle 50 tonnellate di carico oppure 100 soldati.

.. Note::
    Grazie alla chiglia piatta può sia compiere traversate per mare che viaggiare lungo il corso dei fiumi.

Viaggia a una velocità di 1.5 km/h.

Carretto
^^^^^^^^

:Costo: 15mo
:Peso: 100Kg

Un veicolo a due ruote trainato da un solo cavallo o altro animale da traino.

Comprende anche i finimenti.

Carro
^^^^^

:Costo: 35mo
:Peso: 200Kg

Questo è un veicolo aperto a quattro ruote per trasportare carichi pesanti.

In genere, lo tirano due cavalli o altre bestie da soma equivalenti.

Comprende anche i finimenti necessari per tirarlo.

Carrozza
^^^^^^^^

:Costo: 100mo
:Peso: 300Kg

Questo veicolo a quattro ruote può trasportare fino a quattro persone in una cabina chiusa più i due conducenti.

In genere, lo tirano due cavalli o altre bestie da soma equivalenti.

Comprende anche i finimenti necessari per tirarlo.

Galea
^^^^^

:Costo: 30000mo

Una nave a tre alberi con 70 remi su ciascun lato e un equipaggio totale di 200 unità e una lunghezza di 39 metri per una larghezza di 6 metri.

Può trasportare fino a 150 tonnellate di carico o 250 soldati e, con l’aggiunta di 8.000mo,
può essere dotata di sperone e castelli con piattaforme di lancio a prua, a poppa e a metà scafo.

.. Note::
    Questa nave non può affrontare viaggi in mare aperto e si mantiene vicina alla costa.

Viaggia alla velocità di circa 6 km/h, se si impiegano i remi o le vele.

Nave a Vela
^^^^^^^^^^^

:Costo: 10000mo

Questa nave ha una lunghezza compresa tra i 22.5 e i 27 metri per una larghezza di 6 metri, un equipaggio
di 20 unità e capacità di carico fino a 150 tonnellate.

Dotata di due alberi con vele quadrate, può compiere traversate per mare con una velocità di circa 3 km/h.

Nave da Guerra
^^^^^^^^^^^^^^

:Costo: 25000mo

Una nave della lunghezza di 30 metri, ad albero unico, e possibilità di usare i remi.

Ha un equipaggio variabile dai 60 agli 80 rematori e può trasportare fino 160 soldati, ma non per lunghe distanze, poiché non vi è lo spazio
sufficiente a stivare le provviste che sarebbero necessarie a un tal numero di soldati. Non viene usata per il trasporto merci.

.. Note::
    Questa nave non può affrontare viaggi in mare aperto e si mantiene vicina alla costa.

Viaggia alla velocità di 3.75 km/h se si usano i remi o la vela.

Nave Lunga
^^^^^^^^^^

:Costo: 10000mo

Una nave della lunghezza di 22.5 metri, con 50 remi e un equipaggio totale di 50 unità.

Ha un unico albero con una vela quadrata e può trasportare fino a 50 tonnellate di carico oppure 120 soldati.

.. Note::
    Può compiere traversate in mare aperto.

Viaggia alla velocità di 4.5 km/h se si impiegano i remi o le vele.

Remo
^^^^

:Costo: 2mo
:Peso: 5Kg

Slitta
^^^^^^

:Costo: 20mo
:Peso: 150Kg

Si tratta di un carro su pattini adatto per muoversi sulla neve e sul ghiaccio.

Di solito, la tirano due cavalli, o altre bestie da traino equivalenti, e comprende anche dei finimenti necessari per
trascinarla.

Incantesimi e Servizi
---------------------

Talvolta la migliore soluzione a un problema è affidarsi a qualcun altro che lo possa risolvere.

+-----------------------------------+------------------------------------------+---------------------------------+
| Nome                              | Costo                                    | :ref:`Note<note_merci_servizi>` |
+===================================+==========================================+=================================+
| `Diligenza Pubblica (1.5Km)`_     | 3mr                                      |                                 |
+-----------------------------------+------------------------------------------+---------------------------------+
| Incantesimo_                      | Liv Incantatore x Liv Incantesimo x 10mo | 3                               |
+-----------------------------------+------------------------------------------+---------------------------------+
| `Mercenario Esperto (al Giorno)`_ | 3ma                                      |                                 |
+-----------------------------------+------------------------------------------+---------------------------------+
| `Mercenario Normale (al Giorno)`_ | 1ma                                      |                                 |
+-----------------------------------+------------------------------------------+---------------------------------+
| `Messaggero (1.5Km)`_             | 2mr                                      |                                 |
+-----------------------------------+------------------------------------------+---------------------------------+
| `Pedaggio Stradale o d'Ingresso`_ | 1mr                                      |                                 |
+-----------------------------------+------------------------------------------+---------------------------------+
| `Passaggio in Nave (1.5Km)`_      | 1ma                                      |                                 |
+-----------------------------------+------------------------------------------+---------------------------------+

Diligenza Pubblica (1.5Km)
^^^^^^^^^^^^^^^^^^^^^^^^^^

:Costo: 3mr

Il prezzo indicato vale per una corsa su di una diligenza che trasporta persone e bagagli leggeri tra due città.

Su una diligenza che trasporti persone entro la medesima città, una corsa costa 1 mr, e permette solitamente di arrivare ovunque si voglia.

Incantesimo
^^^^^^^^^^^

:Costo: Liv Incantatore x Liv Incantesimo x 10mo

Questo è il costo per avere un incantatore che lancia incantesimi e presuppone che si possa andare dall’incantatore e
far lanciare l’incantesimo a suo piacimento.

.. Note::
    Solitamente a un incantatore servono almeno 24 ore per prepararsi a lanciare l’incantesimo.

Se si vuole portare da qualche parte l’incantatore per fargli lanciare un incantesimo è necessario negoziare con l’incantatore,
ma la risposta di base è “no”.

Il costo indicato è per un incantesimo senza costi per componente materiale;
in caso contrario bisogna aggiungere il costo della componente a quello dell’incantesimo.
Se il componente è di tipo focus (diversa da un focus divino), bisogna aggiungere 1/10 del costo del focus a quello dell’incantesimo.

Se l’incantesimo ha conseguenze pericolose, l’incantatore deve ricevere delle prove certe che il personaggio ha la
possibilità di pagare e che non mancherà di farlo nel caso queste conseguenze si verifichino; sempre che accetti di lanciare
l’incantesimo, cosa nient’affatto sicura.

Quando si tratta di incantesimi che trasportano il personaggio e l’incantatore lungo una distanza, è necessario pagare l’incantesimo due
volte anche se il personaggio non desidera tornare indietro con l’incantatore.

Non tutti i villaggi e i paesi hanno un incantatore di livello sufficiente a lanciare qualsiasi incantesimo. Come regola
generale, è necessario spostarsi almeno in un piccolo paese per essere abbastanza sicuri di trovare un incantatore capace di
lanciare incantesimi di 1° livello, in uno grande per quelli di 2°, in una piccola città per quelli di 3° e di 4°,
in una grande per quelli di 5° e di 6°, in una metropoli per quelli di 7° e 8° livello.
Nemmeno in una metropoli si è certi di trovare incantatori capaci di lanciare incantesimi di 9° livello.

.. Warning::
    Vedi la descrizione dell'incantesimo per i costi aggiuntivi: se questi fanno eccedere il costo totale dell'incantesimo oltre 3000mo, quell'incantesimo non è generalmente disponibile. 

    Usare un livello di incantesimo di 1/2 per gli incantesimi di livello 0 o per calcolarne il costo.

Mercenario Esperto (al Giorno)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:Costo: 3ma

Il prezzo indicato è la paga giornaliera di soldati di ventura, muratori, artigiani, carrettieri, scrivani
e altri aiutanti abili in un mestiere.

Il valore rappresenta la paga minima, perché alcuni aiutanti addestrati chiedono molto di più.

Mercenario Normale (al Giorno)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:Costo: 1ma

Il prezzo indicato è la paga giornaliera di operai, portatori, cuochi, cameriere e altri semplici lavoratori.

Messaggero (1.5Km)
^^^^^^^^^^^^^^^^^^

:Costo: 2mr

Questo termine include sia i messaggeri a cavallo che quelli a piedi.

Se accettano di consegnare un messaggio perché il destinatario si trova in un luogo dove erano comunque diretti,
potrebbero chiedere metà della somma indicata.

Pedaggio Stradale o d'Ingresso
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:Costo: 1mr

Può capitare di dover pagare una tassa per transitare su una strada molto frequentata, ben
sorvegliata e tenuta in buone condizioni per il pattugliamento e la manutenzione.

Occasionalmente le grandi città fortificate richiedono il pagamento di un pedaggio all’ingresso o
all’uscita della città stessa. A volte solo all’ingresso.

Passaggio in Nave (1.5Km)
^^^^^^^^^^^^^^^^^^^^^^^^^

:Costo: 1ma

La maggioranza delle navi non è attrezzata per il trasporto passeggeri,
ma molte hanno la capienza per imbarcarne alcuni a bordo mentre trasportano le merci.

.. Note::
    Raddoppiare il costo indicato per creature di taglia superiore a quella Media o che sono difficili da stivare nella nave.