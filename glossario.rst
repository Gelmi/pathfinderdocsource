
.. _glossario:

=================================================
Glossario
=================================================

Alcune versioni dei vocaboli in inglese sono stati aggiunti con la versione della sigla in italiano. 

.. glossary::
	:sorted:

	AC
		Armor Class (CA)

	AR
		Absalom Reckoning (CA)

	BMC
		:ref:`Bonus da Manovra in Combattimento<bmc>`

	BAB
		Bonus di Attacco Base

	Cos
	COS
		Costituzione

	CA
		Classe Armatura o Computo di Absalom

	Car
	CAR
		Carisma

	CD
		Classe Difficoltà

	Des
	DES
		Destrezza

	DMC
		:ref:`Difesa da Manovra in Combattimento<dmc>`

	DR
		Damage Resistance (RD)
	
	DV
		Dadi Vita

	F
		Focus

	FD
		Focus Divino

	For
	FOR
		:ref:`Forza<forza>`

	GM
		Game Master

	Int
	INT
		Intelligenza

	LI
		Livello Incantatore

	M
		Materiale

	Mag
		Capacità Magiche

	mo
		Monete d'oro

	Mod
		Modificatore	

	PE
		Punti Esperienza

	PG
		Personaggio Giocante

	pf
		Punti Ferita

	PNG
	NPC
		Personaggio non Giocante (Non-Playable Character)

	RD
		Riduzione del Danno

	RI
		Resistenza Incantesimi

	S
		Somatico

	Sag
	SAG
		Saggezza

	SR
		Spell Resistance (RI)

	Sop
		Capacità Soprannaturali

	Str
		Capacità Straordinarie

	V
		Verbale
