
.. _incantesimi:

=================================================
Incantesimi
=================================================

:Stato della Pagina: Incompleta

.. contents:: Contenuti
    :local:
    :depth: 1

.. _incantesimi_bardo:

Bardo
=========

.. _incantesimi_chierico:

Chierico
=========

.. _incantesimi_druido:

Druido
========

.. _incantesimi_mago:

Mago/Stregone
==============

.. _incantesimi_paladino:

Paladino
==============

.. _incantesimi_ranger:

Ranger
======


Tutti gli Incantesimi
========================
.. contents:: Lista
    :local:

.. _inc_anatema:

------------------------------
Anatema
------------------------------

:Scuola: Ammaliamento - Compulsione - Influenza Mentale, Paura
:Livello:
    |	Chierico: 1
:Tempo: :ref:`azione_standard`
:Componenti: V, S, FD
:Raggio: 15m
:Area: Emanazione del raggio di 15m, centrata su se stessi.
:Durata: 1 minuto/livello
:Tiro Salvezza: Volontà nega
:Resistenza: Sì

*Anatema* colma i nemici di dubbi e paure.

Ciascuna creatura interessata subisce penalità -1 ai tiri per colpire e ai tiri salvezza contro gli effetti di paura.

.. Important::
    *Anatema* contrasta e dissolve :ref:`inc_benedizione`.

.. _inc_animale_messaggero:

------------------------------
Animale Messaggero
------------------------------

:Scuola: Ammaliamento - Compulsione - Influenza Mentale, Paura
:Livello:
    |	Bardo: 2
    |   Druido: 2
    |   Ranger: 1
:Tempo: 1 minuto
:Componenti: V, S, M (un pezzetto di cibo gradito all'animale)
:Raggio: Vicino = 7.5 m + 1.5m * 0.5 * Liv
:Bersaglio: Un animale Minuscolo
:Durata: 1 giorno/livello
:Tiro Salvezza: Nessuno
:Resistenza: Sì

Si costringe un animale Minuscolo a raggiungere un luogo stabilito.

L'uso più comune di questo incantesimo consiste nel far portare all'animale un messaggio ai propri alleati.
L'animale non può essere addomesticato o addestrato da nessun altro, come fossero famigli e compagni animali.

Usando del cibo gradito all'animale, lo si attira a sé. La creatura si farà avanti e aspetterà gli ordini.
Si può imprimere mentalmente nell'animale un certo posto conosciuto o un paesaggio facilmente riconoscibile.

.. Note::
    Le indicazioni devono essere semplici perchè l'animale dipende dalle indicazioni e non dalle sue conoscenze.

Si può dotare l'animale di un biglietto o si può fissargli addosso un piccolo oggetto.
L'animale parte per la destinazione indicata e, raggiuntala, vi resta in attesa fino a quando l'effetto dell'incantesimo non svanisce, dopodiché riprende le sue normali attività.

Durante questo periodo di attesa, il messaggero consente agli altri di avvicinarsi e di rimuovere il messaggio o l'oggetto trasportato.

.. Note::
    Il soggetto il cui messaggio è destinato non ottiene alcuna speciale capacità di comunicare con l'animale o per leggere il messaggio accluso (se non conosce la lingua, ad esempio).

.. _inc_benedizione:

------------------------------
Benedizione
------------------------------

:Scuola: Ammaliamento - Compulsione - Influenza Mentale
:Livello:
    |	Chierico: 1
    |   Paladino: 1
:Tempo: :ref:`azione_standard`
:Componenti: V, S, FD
:Raggio: 15m
:Area: Su se stessi e tutti gli alleati entro un'esplosione di 15m centrata su di sé.
:Durata: 1 minuto/livello
:Tiro Salvezza: nessuno
:Resistenza: Sì (innocuo)

*Benedizione* infonde coraggio nei propri alleati, che ottengono bonus morale +1 ai tiri per colpire e ai tiri salvezza contro effetti di paura.

.. Important::
    *Benedizione* contrasta e dissolve :ref:`inc_anatema`.

.. _inc_blocca_persone:

------------------------------
Blocca Persone
------------------------------

:Scuola: Ammaliamento - Compulsione - Influenza Mentale
:Livello:
    |   Bardo: 2
    |	Chierico: 2
    |   Mago: 3
    |   Stregone: 3
:Tempo: :ref:`azione_standard`
:Componenti: V, S, F/FD (un piccolo pezzo dritto di ferro)
:Raggio: Medio = 30m + 3m * Liv
:Bersaglio: 1 Creatura Umanoide
:Durata: 1 round/livello (D)
:Tiro Salvezza: Volontà Nega
:Resistenza: Sì

Il soggetto rimane :ref:`Paralizzato<paralizzato>` e bloccato sul posto. È cosciente e respira normalmente, ma non è in grado di compiere nessuna azione, nemmeno parlare.

Ad ogni round nel suo turno, il soggetto può tentare un nuovo Tiro Salvezza per porre fine all’effetto, come :ref:`azione_di_round_completo` che non provoca :ref:`Attacchi di Opportunità<attacco_di_opportunità>`.

.. Warning::
    Una creatura alata che viene Paralizzata non può battere le ali e quindi cade. Un nuotatore non è in grado di Nuotare e può annegare.

.. _inc_buone_speranze:

------------------------------
Buone Speranze
------------------------------
:Scuola: Ammaliamento - Compulsione - Influenza Mentale
:Livello:
    |	Bardo: 3
:Tempo: :ref:`azione_standard`
:Componenti: V, S
:Raggio: Medio = 30 m + 3 m/Liv
:Bersagli: 1 creatura vivente per livello, due delle quali non possono trovarsi a più di 9 metri l'una dall'altra.
:Durata: 1 minuto/livello
:Tiro Salvezza: Volotà nega (innocuo)
:Resistenza: Sì (innocuo)

Questo incantesimo instilla una grande speranza nei soggetti.

Ogni creatura influenzata ottiene bonus morale +2 a tiri salvezza, tiri per colpire, per i danni da arma, prove di abilità e prove di caratteristica.

.. Important::
    *Buone Speranze* contrasta e dissolve :ref:`inc_disperazione_opprimente`.

.. _inc_calmare_emozioni:

------------------------------
Calmare Emozioni
------------------------------

:Scuola: Ammaliamento - Compulsione - Influenza Mentale
:Livello:
    |	Bardo: 2
    |	Chierico: 2
:Tempo: :ref:`azione_standard`
:Componenti: V, S, FD
:Raggio: Medio = 30 m + 3 m/Liv
:Area: Creature in una propagazione con raggio 6 m.
:Durata: Concentrazione, fino a 1 round/livello
:Tiro Salvezza: Volontà - Nega
:Resistenza: Sì

Questo incantesimo tranquillizza le creature agitate.

Non si ha un controllo diretto sulle creature che ne subiscono l’effetto, ma si può impedire alle creature infuriate di combattere o a quelle euforiche di eccedere.
Le creature che ne subiscono l’effetto non possono intraprendere azioni violente (anche se possono difendersi) o fare alcunché di distruttivo.

.. Note::
    Qualsiasi azione aggressiva che comporti danni contro le creature calmate spezza immediatamente l’incantesimo sulle creature minacciate.

Questo incantesimo sopprime immediatamente (ma non dissolve) qualsiasi Bonus Morale garantito da incantesimi come :ref:`inc_benedizione`, :ref:`inc_buone_speranze` e :ref:`inc_ira`, oltre a negare a un :ref:`Bardo` la capacità di :ref:`Ispirare Coraggio<bardo_ispirare_coraggio>` o a un :ref:`Barbaro` la sua :ref:`Ira<ira>`.
Sopprime anche qualsiasi effetto di Paura e rimuove la condizione :ref:`Confuso<confuso>` da tutti i bersagli.

Per tutta la durata di *Calmare Emozioni*, l’incantesimo soppresso non ha effetto, ma quando termina l’incantesimo originario torna a funzionare sulla creatura, purché la sua durata non si sia esaurita nel frattempo.

.. _inc_charme_su_animali:

------------------------------
Charme su Animali
------------------------------

:Scuola: Ammaliamento - Charme - Influenza Mentale
:Livello:
    |	Druido: 1
    |	Ranger: 1
:Bersaglio: 1 animale

Funziona come :ref:`inc_charme_su_persone`, tranne che ha effetto su una creatura animale.

.. _inc_charme_su_persone:

------------------------------
Charme su Persone
------------------------------

:Scuola: Ammaliamento - Charme - Influenza Mentale
:Livello:
    |	Bardo: 1
    |	Mago: 1
    |	Stregone: 1
:Tempo: :ref:`azione_standard`
:Componenti: V, S
:Raggio: Vicino = 7.5 m + 1.5m * 0.5 * Liv
:Bersaglio: 1 Creatura Umanoide
:Durata: 1 ora/livello
:Tiro Salvezza: Volontà - Nega
:Resistenza: Sì

Questa forma di charme fa sì che una creatura umanoide consideri l’incantatore un suo fidato amico e alleato, quindi assume atteggiamento amichevole.
Se la creatura è attualmente minacciata o attaccata dall’incantatore o dai suoi alleati, tuttavia, riceve bonus +5 al tiro salvezza.

Questo incantesimo non permette di controllare la persona soggetta a charme come se fosse un automa, che semplicemente interpreta azioni e parole dell’incantatore nel modo più benevolo possibile:
si può tentare di darle degli ordini, ma si dovrà superare una prova contrapposta di Carisma per convincerla a fare cose che normalmente non farebbe e ulteriori tentativi non sono concessi.

.. Note::
    Bisogna essere in grado di parlare il linguaggio del soggetto per comunicare i propri comandi, oppure bisogna essere molto bravi a farsi capire a gesti.

Una creatura soggetta a charme non obbedisce mai a ordini suicidi o palesemente nocivi, ma potrebbe essere convinta che valga la pena compiere qualcosa anche se ciò dovesse essere molto pericoloso.

.. Warning::
    Qualsiasi atto da parte dell’incantatore o dei suoi evidenti alleati che minacci la persona soggetta a charme ha l’effetto di spezzare l’incantesimo.

.. _inc_comando:

------------------------------
Comando
------------------------------

:Scuola: Ammaliamento - Compulsione - Dipendente dal Linguaggio, Influenza Mentale
:Livello:
    |	Chierico: 1
:Tempo: :ref:`azione_standard`
:Componenti: V
:Raggio: Vicino = 7.5 m + 1.5m * 0.5 * Liv
:Bersaglio: 1 Creatura Vivente
:Durata: 1 round
:Tiro Salvezza: Volontà - Nega
:Resistenza: Sì

Si impartisce al soggetto un singolo comando, e lui tenterà di eseguirlo al meglio delle sue possibilità alla prima opportunità.

Si può scegliere tra le seguenti opzioni:

Avvicinati
    Nel suo turno, il soggetto si muove verso di voi il più velocemente e direttamente possibile per 1 round. La creatura potrebbe non fare altro che muoversi durante il suo turno, e provoca :ref:`Attacchi d'Opportunità<attacco_opportunità>` per questo movimento come di norma.
Lascia Cadere
    Nel suo turno, il soggetto lascia cadere qualsiasi cosa abbia in mano. Non può raccogliere nessun oggetto caduto fino al suo turno successivo.
A Terra
    Nel suo turno, il soggetto si butta a terra e rimane :ref:`Prono<prono>` per 1 round. Può agire normalmente mentre è :ref:`Prono<prono>` ma subisce tutte le penalità del caso.
Fuggi
    Nel suo turno, il soggetto si allontana da voi il più velocemente possibile per 1 round. Potrebbe non fare altro che muoversi durante il suo turno, e provoca :ref:`Attacchi d'Opportunità<attacco_opportunità>` per questo movimento come di norma.
Fermati
    Il soggetto rimane fermo per 1 round. Non può compiere alcuna azione, ma non è considerato :ref:`Indifeso<indifeso>`.

Se il soggetto non è in grado di eseguire l’ordine nel suo turno successivo, l’incantesimo fallisce automaticamente.

.. _inc_cura_ferite_gravi_di_massa:

------------------------------
Cura Ferite Gravi di Massa
------------------------------

:Scuola: Evocazione - Guarigione
:Livello: 
    |   Chierico: 7
    |	Druido : 8

Funziona come :ref:`cura ferite leggere di massa<inc_cura_ferite_leggere_di_massa>`, ma cura 3d8 + (max 15) Liv Inc danni.

.. _inc_cura_ferite_leggere:

------------------------------
Cura Ferite Leggere
------------------------------
:Scuola: Evocazione - Guarigione
:Livello: 
    |	Bardo: 1
    |	Chierico: 1
    |	Druido: 1
    |	Paladino: 1
    |	Ranger: 2
:Tempo: :ref:`azione_standard`
:Componenti: V, S
:Raggio: Contatto
:Bersaglio: 
    1 creatura toccata.
:Durata: Istantaneo
:Tiro Salvezza: Volontà - Dimezza i danni ai non morti
:Resistenza: Sì

Appoggiando la mano su una creatura vivente si infonde energia positiva che cura 1d8 danni + (max 5) LI. 

Dal momento che i non morti sono animati da energia negativa, questo incantesimo infligge loro danni piuttosto che curare le loro ferite.

.. _inc_cura_ferite_leggere_di_massa:

------------------------------
Cura Ferite Leggere di Massa
------------------------------
:Scuola: Evocazione - Guarigione
:Livello: 
    |	Bardo: 5
    |	Chierico: 5
    |	Druido: 6
:Tempo: :ref:`azione_standard`
:Componenti: V, S
:Raggio: Vicino - 7.5m + 1.5m * 0.5 Liv
:Bersaglio: 
    1 creatura per livello, due delle quali non possono trovarsi a più di 9m l'uno dall'altro.
:Durata: Istantaneo
:Tiro Salvezza: Volontà - Dimezza i danni ai non morti
:Resistenza: Sì

Si incanala energia per curare 1d8 + (max 25) Liv Inc ad ogni creatura selezionata. 

Come tutti gli incantesimi di cura infligge danni ai non morti anzichè curarli. 

.. _inc_cura_ferite_moderate:

------------------------------
Cura Ferite Moderate
------------------------------
:Scuola: Evocazione - Guarigione
:Livello: 
    |	Bardo: 2
    |	Chierico: 2
    |	Druido: 3
    |	Paladino: 3
    |	Ranger: 3

Funziona come :ref:`inc_cura_ferite_leggere`, ma cura 2d8 + 1 * LI (massimo +10) danni

.. _inc_disperazione_opprimente:

------------------------------
Disperazione Opprimente
------------------------------
:Scuola: Ammaliamento - Compulsione - Influenza Mentale
:Livello:
    |	Bardo: 3
    |   Mago: 4
    |   Stregone: 4
:Tempo: :ref:`azione_standard`
:Componenti: V, S, M
:Raggio: 9 m
:Area: Esplosione a forma di cono
:Durata: 1 minuto/livello
:Tiro Salvezza: Volotà nega
:Resistenza: Sì

Un cono invisibile di disperazione produce grande tristezza nei soggetti.

Ogni creatura soggetta all’effetto subisce penalità –2 a Tiri per Colpire, per i danni da arma, Tiri Salvezza, Prove di Abilità e prove di Caratteristica.

.. Important::
    *Disperazione Opprimente* contrasta e dissolve :ref:`inc_buone_speranze`.

.. _inc_dissanguare:

------------------------------
Dissanguare
------------------------------
:Scuola: Necromanzia
:Livello:
    |	Chierico: 0
    |   Mago: 0
    |   Stregone: 0
:Tempo: :ref:`azione_standard`
:Componenti: V, S
:Raggio: Vicino = 7.5m + 1.5m * 0.5 * Liv
:Bersaglio: 1 Creatura Vivente
:Durata: Istantaneo
:Tiro Salvezza: Volotà nega
:Resistenza: Sì

Una creatura :ref:`Stabilizzata<stabilizzato>`, a meno di 0 Punti Ferita, ricomincia a morire.

Quando si lancia l’incantesimo si prende come bersaglio una creatura vivente che ha –1 o meno Punti Ferita. La creatura ricomincia a morire, subendo 1 danno a round. La creatura può essere :ref:`Stabilizzata<stabilizzato>` in seguito come di consueto.

Con questo incantesimo si infligge ad una creatura morente e non :ref:`Stabilizzata<stabilizzato>` 1 danno.

.. _inc_dissolvi_magie:

------------------------------
Dissolvi Magie
------------------------------
:Scuola: Abiurazione
:Livello:
    |   Bardo: 3
    |	Chierico: 3
    |	Druido: 4
    |	Paladino: 3
    |	Mago: 3
    |	Stregone: 3
:Tempo: :ref:`azione_standard`
:Componenti: V, S
:Raggio: Medio = 30 m + 3 m/Liv
:Bersaglio o Area: 1 Incantatore, 1 Creatura o 1 Oggetto
:Durata: Istantaneo
:Tiro Salvezza: Nessuno
:Resistenza: No

Permette di porre fine a incantesimi in	atto su una creatura o un oggetto, per sopprimere temporaneamente le capacità di un oggetto magico, oppure per contrastare gli incantesimi di un altro incantatore. 
Un incantesimo dissolto termina come se il suo tempo di durata si fosse esaurito. 

Gli incatesimi che non possono essere annullati da *dissolvi magie* lo esplicitano nella loro descrizione.

Questo incantesimo può dissolvere anche gli effetti magici così come gli incantesimi. L’effetto di un incantesimo che ha durata istantanea non può essere dissolto.

Si può scegliere di usare dissolvi magie in due modi: 

:Dissolvere mirato:
    Il bersaglio dell’incantesimo è un oggetto, una	creatura o un incantesimo e occorre effettuare una prova di dissolvere con 1d20 + Liv Incantatore e **CD = 11 + Liv Incantatore dell’Incantesimo**.

    Se ci sono più incantesimi attivi si parte da quello con CD maggiore.

    Se si ha successo, l’incantesimo termina. Se no si prosegue gradualmente a quello di livello inferiore e ripetere questo processo fino a che non sarà stato dissolto un incantesimo o nessuno.

    Per esempio, un incantatore di 7° livello lancia dissolvi magie, prendendo come bersaglio una creatura sotto l’effetto di :ref:`Pelle di Pietra<inc_pelle_di_pietra>` (12° livello dell’incantatore) e :ref:`Volare<inc_volare>` (6° livello dell’incantatore).
    La prova di livello dell’incantatore dà come risultato 19 e non è abbastanza alta per porre fine a :ref:`Pelle di Pietra<inc_pelle_di_pietra>` (CD = 23), ma è abbastanza alto per :ref:`Volare<inc_volare>` (CD = 17). Se la prova avesse avuto come risultato	almeno 23, :ref:`Pelle di Pietra<inc_pelle_di_pietra>` sarebbe stato dissolto, lasciando intatto :ref:`Volare<inc_volare>`. Se avesse avuto come risultato 16 o meno, non sarebbe stato	dissolto alcun incantesimo.

    Si può anche bersagliare uno specifico incantesimo che interessa il bersaglio o un’area (come :ref:`Muro di Fuoco<inc_muro_di_fuoco>`). Occorre indicare specificamente l’effetto dell’incantesimo che si bersaglia in questo modo.

    Se la prova di livello dell’incantatore è uguale o superiore alla CD di	quell’incantesimo, esso termina. Non viene bersagliato alcun altro	incantesimo od effetto sul bersaglio, nel caso la prova non sia abbastanza alta da porre fine all’effetto specificamente dichiarato.

    Se si prende a bersaglio un oggetto o una creatura che è effetto di un incantesimo in corso (come un mostro evocato da :ref:`Evoca Mostri<inc_evoca_mostri>`), si effettua una prova di *dissolvere* per terminare l’incantesimo che ha evocato l’oggetto o la creatura.

    Se l’oggetto scelto come bersaglio è un oggetto magico, occorre effettuare una prova di *dissolvere* contro il livello dell’incantatore	dell’oggetto, con **CD = 11 + Liv Incantatore dell’Oggetto**. Se si supera, tutte le proprietà magiche dell’oggetto vengono soppresse per 1d4 round. Un oggetto soppresso diventa non magico per la durata di questo effetto e un’apertura extradimensionale viene temporaneamente chiusa.

    Le proprietà fisiche di un oggetto magico rimangono immutate: una spada magica soppressa rimane sempre una spada (perfetta, oltretutto).

    Gli artefatti e le divinità non sono influenzati dalle magie dei mortali	come questa.

    Si ottiene un successo automatico in ogni prova di dissolvere contro un incantesimo lanciato da se stessi.

:Controincantesimo: 
    Agisce su un incantatore scelto come bersaglio e viene lanciato come un :ref:`Controincantesimo<controincantesimo>`. A differenza di un vero :ref:`Controincantesimo<controincantesimo>`, tuttavia, c’è il rischio che non funzioni.

    Occorre superare una prova di dissolvere per contrastare l’incantesimo.

.. _inc_espiazione:

------------------------------
Espiazione
------------------------------
:Scuola: Abiurazione
:Livello:
    |   Chierico: 5
    |	Druido: 5
:Tempo: 1 ora
:Componenti: 
    V, S, M (incenso che brucia), F (un rosario o altri oggetti per la preghiera del valore di almeno 500 mo), FD
:Raggio: Contatto
:Bersaglio: 1 creatura vivente toccata
:Durata: istantaneo
:Tiro Salvezza: No
:Resistenza: Sì

Questo incantesimo rimuove il peso di atti malvagi o colpevoli che gravano sull’anima di un soggetto in cerca di espiazione sinceramente pentita e esiderosa di porre rimedio al suo errore. 

Se la creatura in cerca di espiazione ha commesso l’atto malvagio involontariamente o sotto qualche forma di coercizione, espiazione funziona senza alcun costo. 

Nel caso di una creatura che vuole espiare per crimini deliberati, occorre intercedere per lei presso la propria divinità (al costo di 2.500 mo in incensi rari ed offerte di varia natura).
Espiazione può essere lanciato per uno degli scopi seguenti.

* Invertire un cambio magico di allineamento: 

    Se una creatura ha subito un cambio dell’allineamento tramite mezzi magici, espiazione riporta l’allineamento al suo stato originario senza costi aggiuntivi.

* Ripristinare una classe: 

    Un paladino, o un’altra classe, che abbia perso i suoi privilegi di classe per aver violato le restrizioni del precedente allineamento, può recuperare il suo status di paladino mediante questo incantesimo.

* Ripristinare i poteri magia di un chierico o di un druido: 

    Un chierico o un druido che abbiano perso la loro capacità di lanciare incantesimi per essere incorsi nell’ira della loro divinità possono recuperare i propri poteri cercando espiazione presso un altro chierico della stessa divinità o un altro druido.

* Redenzione o tentazione: 

    Si può lanciare questo incantesimo su una creatura di allineamento opposto per offrirle l’opportunità di cambiare allineamento e adottarne uno equivalente al proprio.

    Il soggetto deve essere presente per tutto il tempo di lancio dell’incantesimo. Al termine, spetta comunque a lui scegliere liberamente se mantenere il suo allineamento originale o accettare l’offerta e diventare dell’allineamento dell’incantatore. Nessuna violenza, coercizione o influenza magica possono costringere il soggetto ad accettare l’opportunità offerta, se egli non desidera abbandonare il suo vecchio allineamento.

    Quest’uso dell’incantesimo non funziona sugli esterni o su qualsiasi altra creatura impossibilitata a cambiare naturalmente il proprio allineamento.

Sebbene la descrizione dell’incantesimo faccia riferimento ad atti malvagi, espiazione può essere usato anche su qualsiasi creatura che abbia compiuto atti contro il proprio allineamento, indipendentemente dall’allineamento in questione.

:Nota: 
    Normalmente cambiare l’allineamento è una scelta che spetta al giocatore. Quest’uso di espiazione offre semplicemente un metodo per un personaggio per cambiare il suo allineamento in maniera drastica, improvvisa e definitiva.

.. _inc_evoca_alleato_naturale:
.. _inc_evoca_alleato_naturale_I:

------------------------------
Evoca Alleato Naturale I
------------------------------

.. figure:: Immagini/ManualeDiGioco/Incantesimi/EvocaAlleatoNaturaleI.png
    :align: right
    :width: 400

    Alleati Naturali evocabili di primo livello.

:Scuola: Evocazione [convocazione]
:Livello:
    |   Druido: 1
    |   Ranger: 1
:Tempo: 1 Round
:Componenti: V, S, FD
:Raggio: Vicino = 7.5m + 1.5m * 0.5 Liv
:Effetto: Una creatura evocata
:Durata: 1 round/liv (I)
:Tiro Salvezza: Nessuno
:Resistenza: No

Questo incantesimo convoca una creatura naturale che compare al proprio fianco (normalmente un :ref:`animale<tipo_animale>`, un :ref:`folletto<tipo_folletto>`, una :ref:`bestia magica<tipo_bestia_magica>`,
un :ref:`esterno<tipo_esterno>` con sottotipo :ref:`elementale<sottotipo_elementale>` o un :ref:`gigante<sottotipo_gigante>`).

Convoca una creatura :ref:`extraplanare<sottotipo_extraplanare>` (generalmente un :ref:`esterno<tipo_esterno>`, un :ref:`elementale<sottotipo_elementale>` o una :ref:`bestia magica<tipo_bestia_magica>` originaria di un altro piano),
che appare nel punto designato e agisce immediatamente, nel corso del turno dell’incantatore. Attacca i suoi avversari al meglio delle sue capacità.
Se si è in grado di comunicare con la creatura, si può ordinarle di non attaccare, di attaccare alcuni nemici in particolare o di compiere qualche altra azione.

Questo incantesimo evoca una delle creature dell’elenco di 1° livello a scelta, che si può cambiare ogni volta che si lancia l’incantesimo.

.. Note::
    Le creature nella tabella sono neutrali, se non specificato diversamente.

Una creatura evocata non può convocare o in altro modo evocare un’altra creatura, né può utilizzare alcun teletrasporto o capacità di viaggio planare, né usare incantesimi o capacità magiche che replichino incantesimi con componenti materiali costose (es. :ref:`Desiderio<inc_desiderio>`).


Quando si usa l’incantesimo per convocare una creatura con un allineamento o un sottotipo :ref:`elementale<sottotipo_elementale>`, diventa un incantesimo di quel tipo.

.. Note::
    Le creature evocate con questo incantesimo senza sottotipo o allineamento hanno sempre un allineamento che si combina con quello dell’incantatore,
    indipendentemente dal loro allineamento normale ed evocare queste creature fa sì che il tipo dell’incantesimo si adatti all’allineamento dell’incantatore.

.. _inc_evoca_mostri:
.. _inc_evoca_mostri_i:

------------------------------
Evoca Mostri I
------------------------------

.. figure:: Immagini/ManualeDiGioco/Incantesimi/EvocaMostriI.png
    :align: right
    :width: 400

    Mostri evocabili di primo livello.

:Scuola: Evocazione [convocazione]
:Livello:
    |   Bardo: 1
    |   Chierico: 1
    |	Mago: 1
    |	Stregone: 1
:Tempo: 1 Round
:Componenti: V, S, F/FD (una borsa minuscola e una piccola candela)
:Raggio: Vicino = 7.5m + 1.5m * 0.5 Liv
:Effetto: Una creatura evocata
:Durata: 1 round/liv (I)
:Tiro Salvezza: Nessuno
:Resistenza: No

Convoca una creatura :ref:`extraplanare<sottotipo_extraplanare>` (generalmente un :ref:`esterno<tipo_esterno>`, un :ref:`elementale<sottotipo_elementale>` o una :ref:`bestia magica<tipo_bestia_magica>` originaria di un altro piano),
che appare nel punto designato e agisce immediatamente, nel corso del turno dell’incantatore. Attacca i suoi avversari al meglio delle sue capacità.
Se si è in grado di comunicare con la creatura, si può ordinarle di non attaccare, di attaccare alcuni nemici in particolare o di compiere qualche altra azione.

Questo incantesimo evoca una delle creature dell’elenco di 1° livello a scelta, che si può cambiare ogni volta che si lancia l’incantesimo.

Un mostro evocato non può convocare o in altro modo evocare un’altra creatura, né può utilizzare alcun teletrasporto o capacità di viaggio planare, né usare incantesimi o capacità magiche che replichino incantesimi con componenti materiali costose (es. :ref:`Desiderio<inc_desiderio>`).

Le creature non possono essere evocate in un ambiente che non sia in grado di sostenerle.

Quando si usa l’incantesimo per convocare una creatura con un allineamento o un sottotipo elementale, diventa un incantesimo di quel
tipo.

.. Note::
    Tutte le creature con * nella tabella vengono evocate con archetipo :ref:`Celestiale<archetipo_celestiale>` se si è buoni o :ref:`Immondo<archetipo_immondo>` se si è malvagi.
    Se si è neutrali si può scegliere.
    Queste creature, così come l'incantesimo stesso, hanno sempre un allineamento che si combina con quello dell'incantatore, indipendentemente dal loro allineamento usuale.

.. _inc_evoca_mostri_II:

------------------------------
Evoca Mostri II
------------------------------

.. figure:: Immagini/ManualeDiGioco/Incantesimi/EvocaMostriII.png
    :align: right
    :width: 400

    Mostri evocabili di secondo livello.

:Scuola: Evocazione [convocazione]
:Livello:
    |   Bardo: 2
    |   Chierico: 2
    |	Mago: 2
    |	Stregone: 2

Funziona come :ref:`inc_evoca_mostri`, tranne che si può convocare una creatura di 2° livello o 1d3 creature dello stesso tipo di 1° livello.

.. _inc_folata_di_vento:

------------------------------
Folata di Vento
------------------------------

:Scuola: Invocazione - Aria
:Livello: 
    |   Druido: 2
    |	Mago: 2
    |	Stregone: 2
:Tempo: :ref:`azione_standard`
:Componenti: V, S
:Raggio: 18 m
:Effetto: 
    Soffio di vento in forma lineare emanato da sè fino all’estremità del raggio di azione
:Durata: 1 round
:Tiro Salvezza: Tempra nega
:Resistenza: Sì

Questo incantesimo crea un potente getto d’aria (di circa 75 km/h) che ha origine da sè e ha effetto su tutte le creature nel suo percorso. Tutte le creature volanti nell’area ottengono penalità –4 alle prove di Volare. 

Una creatura di taglia Minuscola o inferiore che non superi una prova su Volare con CD 25, viene spinta indietro per 2d6 × 3 metri e subisce 2d6 danni. Le creature di taglia Piccola o inferiore che stanno volando debbono superare una prova di Volare con CD 20 per potersi muovere contro la forza del vento.

Una creatura Minuscola o inferiore che è sul terreno è abbattuta e trascinata per 1d4 × 3 metri, subisce 1d4 danni non letali.
Le creature di taglia Piccola vengono rese prone dalla forza del vento.
Le creature di taglia Media o inferiore non sono in grado di muoversi in avanti contro la forza, del vento a meno che non superino una prova di Forza con CD 15.
Le creature di taglia Grande o superiore possono muoversi normalmente all’interno di un effetto di folata di vento.

Una folata di vento non può spostare una creatura oltre i limiti del suo raggio di azione.

Qualsiasi creatura, indipendentemente dalla taglia, subisce penalità –4 agli attacchi a distanza e alle prove di Percezione nell’area di una folata di vento.

La forza della folata estingue automaticamente candele, torce e altre fiamme simili non protette. Le fiamme protette, come quelle delle lanterne, invece cominciano a tremare violentemente e hanno una probabilità del 50% di spegnersi.

Una *folata di vento* può ottenere gli stessi effetti di un improvviso colpo di vento. Può creare una nuvola accecante di sabbia o di polvere, infiammare un grosso fuoco esistente, capovolgere supporti o tendoni fragili, rovesciare una piccola imbarcazione o spingere gas e vapori fino al limite del suo raggio di azione.

Folata di vento può essere reso permanente con un incantesimo :ref:`permanenza<inc_permanenza>`.

.. _inc_forma_ferina_I:

------------------------------
Forma Ferina I
------------------------------

:Scuola: Trasmutazione - Metamorfosi
:Livello:
    |   Mago: 3
    |	Stregone: 3
:Tempo: :ref:`azione_standard`
:Componenti: V, S, M (un pezzo della creatura di cui si vuole assumere la forma)
:Raggio: Personale
:Bersaglio: Se stessi
:Durata: 1 min/liv (I)

Quando si lancia questo incantesimo, si può assumere la forma di una creatura del tipo animale di taglia Piccola o Media.

Se questa forma ha una delle seguenti capacità, la si ottiene: :ref:`Fiuto<bestiario_fiuto>`, :ref:`nuotare` 9m, :ref:`scalare` 9m, :ref:`Scurovisione<scurovisione>` 18m, :ref:`Visione Crepuscolare<bestiario_visione_crepuscolare>` e :ref:`volare` 9m (media).

.. rubric:: Animale Piccolo

Se la forma è quella di un animale di taglia Piccola, si ottiene bonus di taglia +2 a Destrezza e bonus di armatura naturale +1.

.. rubric:: Animale Medio

Se la forma è quella di un animale di taglia Media, si ottiene bonus di taglia +2 a Forza e bonus di armatura naturale +2.

.. _inc_foschia_occultante:

------------------------------
Foschia Occultante
------------------------------	

:Scuola: Evocazione - Creazione
:Livello:
    |   Chierico: 1
    |	Druido: 1
    |	Mago: 1
    |	Stregone: 1
:Tempo: :ref:`azione_standard`
:Componenti: V, S
:Raggio: 6 m
:Effetto: 
    Nube centrata su di sè che si propaga per 6 m ed è alta 6 m
:Durata: 1 min/liv (I)
:Tiro Salvezza: No
:Resistenza: No

Si viene avvolti da un denso vapore, che resta immobile ed impedisce ogni tipo di visuale oltre 1,5 metri, inclusa la scurovisione.

Una creatura a 1,5 metri di distanza è occultata (gli attacchi hanno il 20% di probabilità di mancare il bersaglio). Le creature ancora più distanti godono di occultamento totale (probabilità del 50% di mancare il bersaglio, e l’attaccante non può usare la vista per localizzare il bersaglio).

Un vento di intensità moderata (16,5 o più km/h), come quello generato dall’incantesimo :ref:`folata di vento<inc_folata_di_vento>`, disperde la nebbia in 4 round. Un forte vento (31,5 o più km/h), disperde la nebbia in 1 round. :ref:`Palla di fuoco<inc_palla_di_fuoco>`, :ref:`colpo infuocato<inc_colpo_infuocato>` o un incantesimo simile disperdono la nebbia nell’area dell’esplosione o delle fiamme. Un muro di fuoco disperde la nebbia nell’area in cui può causare danni.

Questo incantesimo non funziona sott’acqua.

.. _inc_frastornare:

------------------------------
Frastornare
------------------------------	
:Scuola: Ammaliamento - Compulsione - Influenza Mentale
:Livello:
    |   Bardo: 0
    |	Mago: 0
    |	Stregone: 0
:Tempo: :ref:`azione_standard`
:Componenti: V, S, M (un ciuffo di lana o una sostanza simile)
:Raggio: Vicino - 7.5m + 1.5m * 0.5 Liv
:Bersaglio: 1 Creatura umanoide con 4 DV o meno
:Durata: 1 Round
:Tiro Salvezza: Volontà nega
:Resistenza: Sì

Questo ammaliamento annebbia la mente di una creatura umanoide con 4 DV o meno in modo da non farle compiere alcuna azione. 

Il soggetto frastornato non è stordito, quindi chi lo attacca non ottiene alcun vantaggio speciale contro di lui. Dopo che una creatura è rimasta frastornata da questo incantesimo, ne è immune per 1 minuto.

.. _inc_guida:

------------------------------
Guida
------------------------------

:Scuola: Divinazione
:Livello:
    |   Chierico: 0
    |	Druido: 0
:Tempo: :ref:`azione_standard`
:Componenti: V, S
:Raggio: Contatto
:Bersaglio: 1 Creatura Toccata
:Durata: 1 minuto o finché non viene scaricato
:Tiro Salvezza: Volontà - Nega (Innocuo)
:Resistenza: Sì

Questo incantesimo infonde nel soggetto un tocco divino che lo guida nelle sue azioni.

La creatura ottiene bonus di competenza +1 su un unico tiro per colpire, un tiro salvezza o una prova di abilità.

.. Important::
    La creatura deve scegliere come utilizzare il bonus prima di effettuare il tiro a cui intende applicarlo.

.. _inc_incuti_paura:

------------------------------
Incuti Paura
------------------------------

:Scuola: Necromanzia - Influenza Mentale - Paura
:Livello:
    |   Bardo: 1
    |   Chierico: 1
    |   Mago: 1
    |   Stregone: 1
:Tempo: :ref:`azione_standard`
:Componenti: V, S
:Raggio: Vicino = 7.5m + 1.5m * 0.5 * Liv
:Bersaglio: 1 creatura vivente con 5 o meno DV
:Durata: 1d4 round o 1 round
:Tiro Salvezza: Volontà - Parziale
:Resistenza: Sì

La creatura interessata diventa :ref:`Spaventata<spaventato>`. Se il soggetto supera un Tiro Salvezza su Volontà è :ref:`Scosso<scosso>` per 1 round. Le creature con 6 o più DV sono immuni a questo effetto.

.. Important::
    *Incuti Paura* contrasta e dissolve :ref:`Rimuovi Paura<inc_rimuovi_paura>`.

.. _individuazione_auree:
.. _inc_individuazione_del_bene:
.. _inc_individuazione_del_caos:
.. _inc_individuazione_della_legge:

.. figure:: Immagini/IndividuazioneAuree.png
    :align: center

    Individuazione del Bene/Caos/Legge/Male

.. _inc_individuazione_del_male:

------------------------------
Individuazione del Male
------------------------------
:Scuola: Divinazione
:Livello: Chierico: 1
:Tempo: :ref:`azione_standard`
:Componenti: V, S, FD
:Raggio: 18 m
:Area: Emanazione a forma di cono
:Durata: Concentrazione - fino a 10 min/Liv (I)
:Tiro salvezza: No
:Resistenza: No

Si è in grado di percepire la presenza del male. 

La quantità di informazioni che ottiene varia in base al tempo impiegato per studiare un’area o un soggetto particolari.

    *	1° round: Presenza o assenza del male.
    *	2° round: Numero di auree malvagie (creature, oggetti o incantesimi) nell’area e forza dell’aura più potente.
        Se si è di allineamento buono, e la forza dell’aura malvagia più potente è :ref:`schiacciante<individuazione_auree>` e i DV o il livello della fonte dell’aura e almeno il doppio del proprio livello del personaggio, si rimane storditi per 1 round e l’incantesimo termina.
    *	3° round: La forza e la posizione di ogni aura presente. Se un’aura è al di fuori della propria linea visuale, si riesce a capirne la direzione ma non la posizione precisa.

:Potere dell’aura: 

    Il :ref:`potere di un’aura malvagia<individuazione_auree>` dipende dal tipo di oggetto o creatura malvagia individuata, dai suoi DV, dal suo livello dell’incantatore o livello di classe (chierico).

    Se un’aura ricade in più di una categoria, l’incantesimo indica la più forte delle due.

:Protrarsi dell’aura: 

    Un’aura malvagia si protrae anche dopo che la sua fonte originaria si disperde o viene distrutta.

    Se individuazione del male viene diretto verso quella locazione, l’incantesimo indica un’aura flebile (ancora meno forte di un’aura debole) che dura:

.. _tabella_potenza_auree_ind_male:

.. image:: Immagini/PotenzaAureeIndMale.png
    :width: 400
    :align: right

Animali, trappole, veleni e altri potenziali pericoli non sono malvagi di per se stessi e dunque questo incantesimo non li individua. Le creature debbono nutrire attivamente delle cattive intenzioni per gli scopi di questo incantesimo.

Ogni round, ci si può voltare per individuare la presenza del male in una nuova area. L’incantesimo è in grado di penetrare barriere, ma viene bloccato da 30 cm di pietra, 2,5 cm di metallo comune, una sottile lamina di piombo o 90 cm di legno o detriti.

.. _inc_intralciare:

------------------------------
Intralciare
------------------------------

:Scuola: Trasmutazione
:Livello:
    |   Druido: 1
    |   Ranger: 1
:Tempo: :ref:`azione_standard`
:Componenti: V, S, FD
:Raggio: Lungo = 120m + 12m * Liv
:Area: Vegetali in una propagazione del raggio di 12m
:Durata: 1 minuto/livello (I)
:Tiro Salvezza: Riflessi - Parziale
:Resistenza: No

Questo incantesimo fa in modo che erba alta, erbacce ed altre piante si avviluppino intorno alle creature che si trovano nell’area d’effetto o che entrano nell’area.

Le creature che falliscono il tiro salvezza assumono la condizione di :ref:`intralciate<intralciato>`, mentre quelle che superano il tiro salvezza possono muoversi normalmente,
ma se restano nell’area debbono effettuare il tiro salvezza un’altra volta alla fine del turno.

.. Warning::
    Le creature che entrano nell’area debbono effettuare subito il tiro salvezza e chi lo fallisce termina il suo movimento assumendo la condizione di :ref:`Intralciato<intralciato>`.

Le creature intralciate possono tentare di liberarsi come :ref:`azione_di_movimento`, effettuando una prova di Forza o Artista della Fuga con CD pare a quella dell’incantesimo.

.. Note::
    L’intera area d’effetto è considerata terreno difficile mentre dura l’effetto.

Se le piante nell’area sono coperte di spine, quelli nell’area subiscono 1 danno ogni volta che falliscono il tiro salvezza contro intralciare o la prova fatta per liberarsi.

A seconda della vegetazione locale potrebbero verificarsi altri effetti, a discrezione del GM.

.. _inc_ira:

------------------------------
Ira
------------------------------

:Scuola: Ammaliamento - Compulsione - Influenza Mentale
:Livello:
    |	Bardo: 2
    |   Mago: 3
    |   Stregone 3
:Tempo: :ref:`azione_standard`
:Componenti: V, S
:Raggio: Medio = 30 m + 3 m/Liv
:Bersagli: 1 creatura vivente consenziente ogni 3 livelli, due delle quali non possono trovarsi a più di 9 metri l'una dall'altra.
:Durata: Concentrazione, 1 minuto/livello
:Tiro Salvezza: Nessuno
:Resistenza: Sì

Ogni creatura soggetta all’effetto riceve Bonus Morale +2 a Forza e Costituzione, Bonus Morale +1 ai Tiri Salvezza su Volontà e penalità –2 alla CA.

.. Important::
    Questo effetto è identico all’:ref:`Ira<ira>` del Barbaro, ma i soggetti non risultano :ref:`Affaticati<affaticato>` al suo termine.

.. _inc_lama_infuocata:

------------------------------
Lama Infuocata
------------------------------

:Scuola: Invocazione - Fuoco
:Livello:
    |	Druido: 2
:Tempo: :ref:`azione_standard`
:Componenti: V, S, FD
:Raggio: 0m
:Effetto: Raggio a forma di spada.
:Durata: 1 minuto/livello
:Tiro Salvezza: Nessuno
:Resistenza: Sì

Un raggio abbagliante della lunghezza di 90 cm, composto da una fiamma rossa incandescente, scaturisce dalla propria mano.
Si può impugnare questo bagliore simile a una lama come se fosse una scimitarra.

.. Note::
    Gli attacchi con quest'arma sono considerati :ref:`attacchi di contatto in mischia<attacco_contatto>`.

La lama infligge 1d8 + 0.5 * (Liv Incantatore, max 10) danni da fuoco.

.. Important::
    Dal momento che è immateriale, il modificatore Forza non viene applicato ai danni.

La lama infuocata può incendiare eventuali materiali combustibili come paglia, legno asciutto, tessuto e pergamena.

.. _inc_lampo:

------------------------------
Lampo
------------------------------

:Scuola: Invocazione - Luce
:Livello:
    |	Bardo: 0
    |	Druido: 0
    |	Mago: 0
    |	Stregone: 0
:Tempo: :ref:`azione_standard`
:Componenti: V
:Raggio: Vicino = 7.5m + 1.5m * 0.5 * Liv
:Effetto: Esplosione di luce.
:Durata: Istantaneo
:Tiro Salvezza: Tempra - Nega
:Resistenza: Sì

Questo trucchetto crea un’esplosione di luce.

Se si orienta il fascio di luce direttamente in viso a una creatura, quest’ultima rimane :ref:`abbagliata<abbagliato>` per 1 minuto a meno che non superi un tiro salvezza su Tempra.

.. Warning::
    Le creature prive di vista, così come quelle già :ref:`abbagliate<abbagliato>`, non sono influenzate da lampo.

.. _inc_luci_danzanti:

------------------------------
Luci Danzanti
------------------------------

:Scuola: Invocazione - Luce
:Livello:
    |   Bardo: 0
    |   Mago: 0
    |   Stregone: 0
:Tempo: :ref:`azione_standard`
:Componenti: V, S
:Raggio: Medio = 30m + 3m * Liv
:Effetto: fino a 4 Luci, tutte entro un'area del raggio 3m
:Durata: 1 minuto
:Tiro Salvezza: Nessuno
:Resistenza: No

In base alla versione scelta, si possono creare fino a 4 luci simili a lanterne o torce (che proiettano una pari quantità di luce), oppure fino a 4 sfere luminose (simili a fuochi fatui), oppure una forma luminescente vagamente umanoide.

Le *Luci Danzanti* devono rimanere entro un’area del raggio di 3 metri le une dalle altre, ma per il resto possono muoversi come si desidera (non è necessaria alcuna Concentrazione): avanti, indietro, su, giù, dritte o dietro un angolo e così via.

.. Warning::
    Le luci possono raggiungere una velocità di 30 metri per round, ma una luce scompare, se si allontana dall’incantatore oltre il raggio di azione dell’incantesimo.

Si può avere solo un incantesimo *Luci Danzanti* attivo di volta in volta. Se si lancia questo incantesimo mentre un altro uguale è in effetto il precedente viene dissolto. Se il precedente è reso permanente, non conta per questa limitazione.

.. Note::
    *Luci Danzanti* può essere reso permanente con un incantesimo :ref:`inc_permanenza`.

.. _inc_mano_magica:

------------------------------
Mano Magica
------------------------------

:Scuola: Trasmutazione
:Livello:
    |   Bardo: 0
    |   Mago: 0
    |   Stregone: 0
:Tempo: :ref:`azione_standard`
:Componenti: V, S
:Raggio: Vicino = 7.5m + 1.5m * 0.5 Liv
:Bersaglio: 1 Oggetto non magico incustodito che pesa fino a 2.5 kg
:Durata: Concentrazione
:Tiro Salvezza: Nessuno
:Resistenza: No

Puntando un dito verso un oggetto, si può sollevarlo e muoverlo a distanza a piacimento. Come :ref:`Azione_movimento` si può spostarlo fino a 4.5 metri in qualsiasi direzione, sebbene l'incantesimo finisca non appena la distanza fra sè e l'oggetto non supera il raggio di azione dell'incantesimo.

.. _inc_messaggio:

------------------------------
Messaggio
------------------------------

:Scuola: Trasmutazione - Dipendente dal Linguaggio
:Livello:
    |   Bardo: 0
    |   Mago: 0
    |	Stregone: 0
:Tempo: :ref:`azione_standard`
:Componenti: V, S, F (un pezzetto di filo di rame)
:Raggio: Medio - 30m + 3m * Liv
:Bersagli: 1 Creatura per livello
:Durata: 10 minuti/livello
:Tiro Salvezza: Nessuno
:Resistenza: No

Si possono sussurrare messaggi e ricevere risposte sussurrate, con la probabilità che chi vi sta vicino senta il messaggio con una prova di :ref:`Percezione<percezione>` con CD 25. Occorre puntare il dito verso ogni creatura che si vuole riceva il messaggio.

Quando si sussurra il messaggio è percepibile a tutte le creature bersaglio entro il raggio di azione. :ref:`Silenzio<inc_silenzio>`, 30 cm di roccia, 2.5 cm di metallo comune, una sottile lamina di piombo o 90 cm di legno o detriti bloccano l'incantesimo. 

Il messaggio non deve necessariamente viaggiare il linea retta, ma può aggirare un barriera fintato che ci sia una via aperta fra sè e il bersaglio, e che la lunghezza del percorso compiuto rientri nel raggio di azione dell'incantesimo.

Le creature che ricevono il messaggio possono sussurrare una risposta che l'incantatore sente. 

.. Note::
    Questo incantesimo trasmette suoni, non significati. Non trascende eventuali barriere linguistiche.

    Per pronunciare un messaggio, occorre muovere le labbra e sussurrare.

.. _inc_parlare_con_gli_animali:

------------------------------
Parlare con gli Animali
------------------------------

:Scuola: Divinazione
:Livello:
    |   Bardo: 3
    |   Druido: 1
    |	Ranger: 1
:Tempo: :ref:`azione_standard`
:Componenti: V, S
:Raggio: Personale
:Bersaglio: Se stessi
:Durata: 1 minuto/livello

Si possono fare domande e ricevere risposte dagli animali, anche se l’incantesimo non li rende amichevoli o necessariamente più disposti a collaborare rispetto al normale.

Gli animali più svegli e intelligenti tendono ad essere concisi ed evasivi, mentre quelli più stupidi a dire cose insensate.

Se l’animale è amichevole nei vostri confronti, può fare alcuni favori o svolgere qualche servizio per vostro conto

.. _inc_permanenza:

------------------------------
Permanenza
------------------------------

.. _tabella_permanenza:

.. figure:: Immagini/IncantesimoPermanenza.png
    :align: center

    Costi incantesimi di permanenza. Su oggetti o aree (sinistra), se stessi (in alto a destra) e gli altri (basso a destra)

:Scuola: Universale
:Livello: 
    |	Mago: 5
    |	Stregone: 5
:Tempo: 2 round
:Componenti: V, S, M (vedi :ref:`tabella<tabella_permanenza>`)
:Raggio: Vedi Descrizione
:Bersaglio: Vedi Descrizione
:Durata: Permanente - Vedi Descrizione
:Tiro salvezza: No
:Resistenza: No

Questo incantesimo ne rende altri permanenti. 

Per prima cosa occorre lanciare l’incantesimo desiderato e, di seguito, l’incantesimo permanenza. A seconda dell’incantesimo, bisogna essere di un certo livello e spendere un certo numero di mo o di polvere di diamanti come componente materiale.

Si possono rendere permanenti su se stessi gli incantesimi in alto a destra nella :ref:`tabella di riferimento<tabella_permanenza>`, e non sugli altri.
Questa applicazione di *permanenza* può essere dissolta solo da un incantatore di livello più alto del livello che si aveva quando lo si ha lanciato.

Si possono rendere permanenti sugli altri gli incantesimi in basso a destra nella :ref:`tabella di riferimento<tabella_permanenza>`. I restanti nella tabella sono lanciabili solo su oggetti o aree e resi permanenti.

Gli incantesimi lanciati su altre creature, oggetti o luoghi sono vulnerabili a :ref:`dissolvi magie<inc_dissolvi_magie>` in modo normale. 

Il GM può permettere di rendere altri incantesimi permanenti.

.. _inc_raggio_di_indebolimento:

------------------------------
Raggio di Indebolimento
------------------------------
:Scuola: Necromanzia
:Livello:
    |	Mago: 1
    |	Stregone: 1
:Tempo: :ref:`azione_standard`
:Componenti: V, S
:Raggio: Vicino = 7.5m + 1.5m * 0.5 * Liv
:Effetto: Raggio
:Durata: 1 Round/Livello
:Tiro salvezza: Tempra dimezza
:Resistenza: Sì

Un raggio di energia brillante scaturisce dalla propria mano.

Occorre effettuare con successo un :ref:`Attacco di Contatto a Distanza<attacco_contatto>` per colpire il bersaglio: il soggetto subisce una penalità alla Forza pari a 1d6+1 ogni 2 livelli dell’incantatore (massimo 1d6+5).

Il punteggio di Forza del soggetto non può scendere a meno di 1 e se il soggetto supera un Tiro Salvezza su Tempra la penalità viene dimezzata.

.. Note::
    Questa penalità non si cumula con se stessa: si applica invece la penalità più alta.

.. _inc_resistere_allenergia:

------------------------------
Resistere all'Energia
------------------------------

:Scuola: Abiurazione
:Livello: 
    |	Chierico: 2
    |	Druido: 2
    |	Paladino: 2
    |	Mago: 2
    |	Stregone: 2
    |	Ranger: 1
:Tempo: :ref:`azione_standard`
:Componenti: V, S, FD
:Raggio: Contatto
:Bersaglio: 1 Creatura Toccata
:Durata: 10 minuti/livello
:Tiro salvezza: Tempra - Nega (Innocuo)
:Resistenza: Sì (Innocuo)

Questa abiurazione fornisce ad una creatura una protezione limitata dai danni di uno qualunque dei cinque tipi di energia selezionato: acido, elettricità, freddo, fuoco o sonora. Il soggetto ottiene resistenza all'energia 10 contro il tipo di energia scelto, il che significa che ogni volta che la creatura è soggetta a quel tipo di danni (sia da una fonte naturale che da una fonte magica), quei danno sono ridotti di 10 prima di essere applicati ai punti ferita della creatura.Il valore della resistenza all'energia fornita aumenta a 20 al 7° livello fino ad un massimo di 30 all'11° livello. L'incantesimo protegge allo stesso modo anche l'equipaggiamento del soggetto. 

*Resistere all'Energia* assorbe solo i danni. Il soggetto potrebbe comunque subire sfortunati eventi collaterali nocivi.

*Resistere all'Energia* si sostituisce (senza sommarsi) a :ref:`Protezione dall'Energia<inc_protezione_dallenergia>`. Se un personaggio è sotto l'effetto di *Protezione dall'Energia* e di *Resistere all'Energia*, è *Protezione dall'Energia* ad assorbire i danni finchè non si esaurisce il suo potere.

.. _inc_rimuovi_maledizione:

------------------------------
Rimuovi Maledizione
------------------------------

:Scuola: Abiurazione
:Livello:
    |   Bardo: 3
    |   Chierico: 3
    |   Mago: 4
    |   Stregone: 4
    |   Paladino: 3
:Tempo: :ref:`azione_standard`
:Componenti: V, S
:Raggio: Contatto
:Bersaglio: 1 Creatura o 1 Oggetto
:Durata: Istantaneo
:Tiro Salvezza: Volontà - Nega (innocuo)
:Resistenza: Sì (innocuo)

*Rimuovi Maledizione* rimuove tutte le Maledizioni presenti su un oggetto o su una creatura.

Se il bersaglio è una creatura, occorre superare una prova di livello dell’incantatore (1d20 + livello dell’incantatore) contro la CD di ciascuna Maledizione che affligge il bersaglio, per rimuovere la maledizione.
Quest'incantesimo non rimuove la maledizione presente su uno scudo, un’arma o un’armatura maledetti, ma generalmente permette alla creatura afflitta da un simile oggetto maledetto, superando una prova di livello dell’incantatore, di liberarsene.

.. Important::
    *Rimuovi Maledizione* contrasta e dissolve :ref:`inc_scagliare_maledizione`.

.. _inc_riparare:

------------------------------
Riparare
------------------------------

:Scuola: Trasmutazione
:Livello:
    |	Bardo: 0
    |	Chierico: 0
    |	Druido: 0
    |	Mago: 0
    |	Stregone: 0
:Tempo: 10 minuti
:Componenti: V, S
:Raggio: 3m
:Bersaglio: 1 oggetto fino a (0.5 * Liv)Kg
:Durata: Istantaneo
:Tiro salvezza: Volontà - Nega (Innocuo, Oggetto)
:Resistenza: Sì (Innocuo, Oggetto)

Questo incantesimo ripara gli oggetti danneggiati, ripristinando 1d4 danni.

Se l’oggetto ha la condizione :ref:`Rotto<rotto>`, questa condizione viene rimossa e l’oggetto viene riportato alla metà dei suoi punti ferita originari,
ma occorre avere tutti i pezzi dell’oggetto perché l’incantesimo funzioni.

Gli oggetti magici possono benissimo essere riparati, ma occorre avere un Liv Incantatore uguale o superiore a quello dell’oggetto.

.. Important::
    Gli oggetti magici che sono stati :ref:`distrutti<distrutto>` possono essere riparati ma non è possibile reintegrare le loro proprietà magiche.

Questo incantesimo non ha effetto sulle creature, inclusi i costrutti, e nemmeno su oggetti che sono stati deformati o trasmutati in altro modo, anche se può riparare comunque i danni che hanno subito

.. _inc_risata_incontenibile:

------------------------------
Risata Incontenibile
------------------------------

:Scuola: Ammaliamento - Compulsione - Influenza Mentale
:Livello:
    |   Bardo: 1
    |   Mago: 2
    |   Stregone: 2
:Tempo: :ref:`azione_standard`
:Componenti: V, S, M (minuscole crostate alla frutta e una piuma)
:Raggio: Vicino - 7.5m + 1.5m * 0.5 Liv
:Bersaglio: 1 Creatura
:Durata: 1 round/liv
:Tiro Salvezza: Volontà nega
:Resistenza: Sì

Questo incantesimo provoca nel soggetto una risata incontrollabile, costringendolo a crollare a terra prono preda ad un riso convulso. Finché ride, il soggetto non può compiere azioni, ma non è considerato indifeso. Non appena l'incantesimo si esaurisce, può tornare ad agire normalmente. Il turno successivo al lancio dell'incantesimo, il bersaglio può tentare un TS per mettere fine all'effetto. Questa è un':ref:`Azione_di_Round_Completo` che non provoca :ref:`Attacchi di Opportunità<attacco_di_opportunità>`. Se il tiro ha successo, l'effetto termina, altrimenti la creatura continua a ridere per l'intera durata.

Creature con un punteggio di Intelligenze pari a 2 o meno non ne subiscono l'effetto. Una creatura il cui tipo sia diverso dal proprio riceve +4 al suo TS, poiché l'umorismo non viene tradotto molto bene.

.. _inc_ristorare_inferiore:

------------------------------
Ristorare Inferiore
------------------------------

:Scuola: Evocazione - Guarigione
:Livello:
    |   Chierico: 2
    |   Druido: 2
    |   Paladino: 2
:Tempo: 3 Round
:Componenti: V, S
:Raggio: Contatto
:Bersaglio: 1 Creatura Toccata
:Durata: Istantaneo
:Tiro Salvezza: Volontà - Nega (innocuo)
:Resistenza: Sì (innocuo)

Questo incantesimo dissolve qualunque effetto magico riduca uno dei punteggi caratteristica del bersaglio oppure cura 1d4 danni temporanei a uno dei punteggi caratteristica del bersaglio;
inoltre, elimina qualsiasi affaticamento sofferto dal soggetto, e migliora una condizione da esausto ad affaticato.

.. Important::
    Non può ripristinare un risucchio di caratteristica permanente.

.. _inc_scagliare_maledizione:

------------------------------
Scagliare Maledizione
------------------------------
:Scuola: Necromanzia
:Livello:
    |   Chierico: 3
    |   Mago: 4
    |   Stregone: 4
:Tempo: :ref:`azione_standard`
:Componenti: V, S
:Raggio: Contatto
:Bersaglio: 1 Creatura
:Durata: Permanente
:Tiro Salvezza: Volontà nega
:Resistenza: Sì

Si pone sul soggetto una maledizione, a scelta tra le seguenti:

    * –6 a un punteggio di caratteristica (minimo 1).
    * –4 ai Tiri per Colpire, Tiri Salvezza, prove di caratteristica e di Abilità.
    * Ad ogni turno, il soggetto ha una probabilità del 50% di agire normalmente; altrimenti non compie alcuna azione.

È anche possibile inventarsi una maledizione personale, che non deve però essere più potente di quelle sopra descritte.

La maledizione non può essere dissolta, ma può essere rimossa con :ref:`Desiderio<inc_desiderio>`, :ref:`Desiderio Limitato<inc_desiderio_limitato>`, :ref:`Miracolo<inc_miracolo>`, :ref:`inc_rimuovi_maledizione` o :ref:`Spezzare Incantamento<inc_spezzare_incantamento>`.

.. Important::
    *Scagliare Maledizione* contrasta e dissolve :ref:`inc_rimuovi_maledizione`.

.. _inc_sonno:

------------------------------
Sonno
------------------------------

:Scuola: Ammaliamento - Compulsione - Influenza Mentale
:Livello:
    |   Bardo: 1
    |   Mago: 1
    |   Stregone: 1
:Tempo: 1 Round
:Componenti: V, S, M (sabbia fine, petali di rosa, un grillo vivo)
:Raggio: Medio = 30m + 3m * Liv
:Area: 1 o più creature viventi entro un'esplosione di raggio 3 m
:Durata: 1 minuto/Livello
:Tiro Salvezza: Volontà - Nega
:Resistenza: Sì

L'incantesimo Sonno fa cadere in un sonno magico 4 DV di creature.

Le creature con meno DV vengono colpite per prime e tra creature con lo stesso numero di DV, vengono colpite per prime quelle più vicine al punto di origine dell’incantesimo.

.. Note::
    I DV che non sono sufficienti ad influenzare una creatura vanno sprecati.

Le creature addormentate sono :ref:`Indifese<indifeso>`: se vengono ferite o schiaffeggiate si risvegliano, ma i normali rumori non sono sufficienti.
Svegliare una creatura è un’:ref:`azione_standard` (un’applicazione di :ref:`azione_aiutare`).

Sonno non ha effetto su creature Prive di Sensi, i Costrutti o i Non Morti.

.. _inc_stabilizzare:

------------------------------
Stabilizzare
------------------------------

:Scuola: Evocazione - Guarigione
:Livello:
    |   Chierico: 0
    |   Druido: 0
:Tempo: :ref:`azione_standard`
:Componenti: V, S
:Raggio: Vicino = 7.5m + 1.5m * 0.5 Liv
:Bersaglio: 1 Creatura Vivente
:Durata: Istantaneo
:Tiro Salvezza: Volontà - Nega (Innocuo)
:Resistenza: Sì (Innocuo)

Con il lancio di questo incantesimo, una creatura vivente bersaglio che ha –1 punti ferita o meno si stabilizza automaticamente e non perde ulteriori punti.

.. Warning::
    Se in seguito subisce nuovamente danno, continua a morire come di consueto.

.. _inc_suggestione:

------------------------------
Suggestione
------------------------------

:Scuola: Ammaliamento (compulsione) - Dipendente dal linguaggio, Influenza Mentale
:Livello:
    |	Bardo: 2
    |	Mago: 3
    |	Stregone: 3
:Tempo: :ref:`azione_standard`
:Componenti: V, M (testa di serpente, pezzo di alveare)
:Raggio: Vicino = 7.5m + 1.5m * 0.5 Liv
:Bersaglio: Creatura Vivente
:Durata: 1 h/liv o finchè viene completata
:Tiro Salvezza: Volontà - Nega
:Resistenza: Sì

Si influenzano le azioni di una creatura ammaliata suggerendole una serie di ragionevoli azioni da compiere (una o due frasi). 

Chiedere a una creatura di compiere gesti palesemente nocivi nega gli effetti.

Un suggerimento decisamente ragionevole implica un tiro salvezza con una penalità (-1 o -2).

.. _inc_suono_fantasma:

------------------------------
Suono Fantasma
------------------------------

:Scuola:
    Illusione (Finzione)
:Livello:
    |	Bardo: 0
    |	Mago: 0
    |	Stregone: 0
:Tempo: :ref:`azione_standard`
:Componenti: V, S, M (un pezzo di lana o una pallina di cera)
:Raggio: Vicino = 7.5m + 1.5m * 0.5 Liv
:Effetto: Suoni Illusori
:Durata: 1 round/liv
:Tiro Salvezza: Volontà - Dubita
:Resistenza: No

*Suono Fantasma* permette di creare un suono continuato che può salire di tono, scendere, allontanarsi, avvicinarsi o rimanere fisso sul posto. Si può scegliere che tipo di suono l'incantesimo creerà al momento di lanciarlo, e non si potranno cambiare le sue caratteristiche di base in seguito.

Il volume del suono creato dipende dal livello dell'incantatore. Si è in grado di produrre un rumore pari a quello generato da 4 umani normali per livello (fino a un massimo di 40 umani). Si possono quindi ricreare suono che simulino il parlare, il cantare, il gridare, il camminare, il marciare e il correre. Il rumore generato dal *suono fantasma* può produrre praticamente qualsiasi tipo di suono entro i limiti di volume definiti. Il rumore di un'orda di topi che corrono e squittiscono è pari più o meno al volume di 8 umani che corrono e gridano. Il ruggito di un leone è pari al rumore prodotto da 16 umani, e quello di un drago ruggente è il boato di 32 umani. Chi sente *suono fantasma* riceve un TS su Volontà per dubitare.

*Suono fantasma* può potenziare l'efficacia di un incantesimo :ref:`Immagine Silenziosa<inc_immagine_silenziosa>` o essere reso permanente da un incantesimo :ref:`Inc_Permanenza`.