
.. _combattimento:

=================================================
Combattimento
=================================================

.. figure:: Immagini/Combattimento.png

 Nelle regioni selvagge del mondo dove i mostri dominano, una spada affilata ed un robusto scudo sono mezzi di comunicazione più efficaci delle parole. 

 Il combattimento è una parte comune di Pathfinder GdR, e le regole relative ad esso vengono spiegate qui di seguito.

.. contents:: Contenuti
	:local:
	:depth: 3

Sequenza
============================
.. contents:: In breve:
	:local:

Il combattimento è ciclico: ciascuno agisce al proprio turno in un ciclo ripetuto di round, come descritto di seguito:

#. All’inizio del combattimento tutti tirano per l’:ref:`Iniziativa`.
#. Si stabilisce quali personaggi sono consapevoli dei loro avversari all’inizio della battaglia.  

	I combattenti consapevoli dei loro avversari possono agire nel :ref:`round_sorpresa` e se tutti cominciano la battaglia consapevoli dei propri avversari si procede con i round normali.
#. Dopo il :ref:`round_di_sorpresa` (se c’è), tutti i combattenti sono in grado di iniziare il loro primo round normale di combattimento.
#. I combattenti agiscono nell’ordine di :ref:`Iniziativa`, dal più alto al più basso.
#. Quando ciascuno ha completato il proprio turno, si ripetono i punti 4 e 5 fino al termine del combattimento.

-----------------------
Round di Combattimento
-----------------------
**Un round rappresenta 6 secondi** nel mondo di gioco: ci sono 10 round in un minuto di combattimento. Un round è l’occasione per ciascun personaggio coinvolto in una situazione di combattimento di compiere un’azione.

L’attività di ogni round prende il via dal personaggio che ha ottenuto il risultato di :ref:`iniziativa` più alto e poi procede in ordine fino a quello più basso. A ogni round di combattimento si ripete lo stesso ordine di :ref:`iniziativa`. 

Al proprio turno un personaggio può compiere tutte le azioni a sua disposizione in quel round, con alcune eccezioni come :ref:`Attacchi di Opportunità<attacco_di_opportunità>` e :ref:`azioni_speciali_iniziativa`.

Quando le regole si riferiscono ad un *Round Completo*, significa una frazione del tempo che intercorre a partire da un particolare conteggio di :ref:`iniziativa` in un round fino allo
stesso conteggio del round successivo. Gli effetti che durano un certo numero di round terminano **prima** dello stesso numero di :ref:`iniziativa` in cui sono cominciati.

.. _iniziativa:

----------------
Iniziativa
----------------
Ciascun combattente all’inizio del combattimento effettua una prova di :ref:`iniziativa`. 

Questa è una prova di Destrezza, a cui i personaggi applicano il proprio modificatore di Destrezza, così come altri modificatori dati da talenti, incantesimi ed altri effetti. 
I personaggi agiscono poi in ordine di :ref:`iniziativa`, dal risultato più alto a quello più basso. Ad ogni round successivo, il personaggio agisce sempre nello stesso ordine (a meno che il personaggio non intraprenda un’:ref:`Azione Speciale di Iniziativa<azioni_speciali_iniziativa>` che ne comporti il cambiamento).

Se due più combattenti hanno la stessa :ref:`iniziativa`, essi agiscono in base al modificatore totale di :ref:`iniziativa` (prima il più alto) e, se fossero ancora pari, i personaggi dovranno ritirare per determinare chi di essi debba agire per primo.

.. _impreparati:
.. _impreparato:

Impreparato
-------------
All’inizio di una battaglia, prima di aver avuto la possibilità di agire (ossia: prima del proprio primo turno normale nell’ordine dell’:ref:`iniziativa`) si è :ref:`Impreparati<impreparato>`.

Quando si è :ref:`Impreparati<impreparato>` non si può usare il proprio bonus di Destrezza alla CA. 

:ref:`Barbari<barbaro>` e :ref:`Ladri<ladro>` possiedono la capacità straordinaria di :ref:`schivare_prodigioso`, che evita loro di essere colti impreparati: chi possiede questa capacità  non perde il bonus di Destrezza alla CA e può effettuare :ref:`Attacchi di Opportunità<attacco_di_opportunità>` anche prima di aver agito nel primo round di combattimento. 

Un personaggio :ref:`impreparato` non può infatti compiere :ref:`Attacchi di Opportunità<attacco_di_opportunità>`, a meno che non abbia il :ref:`Riflessi in Combattimento<tal_riflessi_in_combattimento>`.

Inazione
---------
Se non si può agire non si perde comunque il proprio punteggio di :ref:`iniziativa` per la durata dell’incontro.

----------------
Sorpresa
----------------
Quando comincia una battaglia, se si è inconsapevoli della presenza dei propri nemici dai quali si è stati però individuati, si viene sorpresi.

A volte tutti i combattenti della stessa fazione sono consapevoli della presenza degli avversari, a volte nessuno lo è e a volte lo sono solo alcuni. A volte alcuni combattenti
di entrambe le fazioni sono consapevoli, mentre altri sono sorpresi. 

Per determinare se si è consapevoli o meno della presenza degli avversari occorre una prova di :ref:`Percezione<percezione>` o altri tipi di prove.

.. _round_di_sorpresa: 
.. _round_sorpresa:

Round di Sorpresa
------------------
Se alcuni ma non tutti i combattenti sono consapevoli della presenza dei loro nemici, si svolge
un round di sorpresa prima dell’inizio dei round normali.

In ordine di :ref:`iniziativa`, ciascuno dei combattenti che ha iniziato la battaglia consapevole dei propri nemici compie un’:ref:`azione_standard` o un’:ref:`azione_di_movimento` nel corso del round di sorpresa, insieme ad una o più :ref:`Azioni Gratuite<azione_gratuita>`.

Se nessuno o tutti sono sorpresi, non si svolge alcun round di sorpresa.

Combattenti Inconsapevoli
--------------------------
I combattenti che sono inconsapevoli all’inizio della battaglia, non possono agire nel round di sorpresa. 

I combattenti inconsapevoli sono :ref:`Impreparati<impreparato>` poiché non hanno ancora agito, e quindi perdono il loro bonus di Destrezza alla CA.

Statistiche
============================
Questa sezione riassume le statistiche indispensabili per avere successo in battaglia e spiega come utilizzarle.

.. contents:: In breve:
	:local:

.. _tiro_colpire:

----------------
Tiro per Colpire
----------------
Un :ref:`tiro_colpire` rappresenta il tentativo di ferire un avversario durante il proprio turno in un round. Quando lo si effettua si lancia 1d20 e si aggiunge il proprio :ref:`bonus_attacco`. 

.. Note::
	A questo tiro si possono comunque applicare altri modificatori. 

Se questo risultato è pari o superiore alla CA del bersaglio, si colpisce e si infliggono danni.

Mancare e Colpire in Automatico
--------------------------------
Un 1 naturale (sul d20 esce un 1) al :ref:`tiro_colpire` è sempre un colpo mancato. 

Un 20 naturale (sul d20 esce un 20) è sempre un colpo messo a segno e costituisce una minaccia per un possibile :ref:`colpo_critico`.

.. _bonus_attacco:

--------------------------------
Bonus di Attacco
--------------------------------

.. _tabella_modificatori_taglia:

.. figure:: Immagini/ModificatoriTaglia.png
	:width: 300
	:align: right

	Modificatori di Taglia, utilizzati per calcolare diversi parametri (es: Bonus di Attacco).

Il proprio bonus di attacco con un’**arma da mischia** è **BAB + Mod For + Mod Taglia**.

Con un’**arma a distanza** invece è **BAB + Mod Des + Mod Taglia + Penalità di Gittata**.

.. _classe_armatura:

----------------
Classe Armatura
----------------
La Classe Armatura (CA) rappresenta le difficoltà a cui vanno incontro gli avversari per assestare un colpo preciso che vi infligga danno: è il risultato del tiro per colpire che un avversario deve ottenere per mettere a segno un colpo contro il personaggio. 

La propria CA è pari a **10 + Bonus Armatura + Bonus Scudo + Mod Des + Mod Taglia**.

.. Caution::
	L’armatura limita il bonus di Destrezza, perciò se si indossa un’armatura si potrebbe non essere in grado di applicare l’intero bonus di Destrezza alla CA a seconda di quanto riportato nella :ref:`Tabella Armature<tabella_armature>`.

Se non si è in grado di reagire a un colpo non si può sfruttare il bonus di Destrezza alla CA e se non si possiede alcun bonus di Destrezza, la CA non cambia.

Altri Modificatori
-------------------
Molti altri fattori modificano la CA.

Bonus di Potenziamento
	Gli effetti di potenziamento migliorano il bonus alla CA conferito dall’armatura.
Bonus di Deviazione
	Gli effetti di deviazione magica parano gli attacchi e migliorano la CA.
Armatura Naturale
	Se la propria razza è dotata di scaglie, pelle coriacea o altro si ottiene un bonus alla CA.
Bonus di Schivare
	Questi bonus rappresentano lo sforzo attivo di evitare i colpi, ma qualsiasi situazione che nega il bonus di Destrezza, nega altresì i bonus di schivare. A differenza di altri tipi di bonus, i bonus di schivare sono cumulabili.

	.. Note:: 
		Indossare un’armatura non penalizza questo tipo di bonus, come invece succede per quelli di Destrezza alla CA.
Modificatore di Taglia
	Si riceve un bonus o una penalità alla CA :ref:`in base alla propria taglia<tabella_modificatori_taglia>`.

.. _attacco_contatto:
.. _azione_attacco_di_contatto_in_mischia:
.. _azione_attacco_di_contatto_a_distanza:

Attacco di Contatto
---------------------
Alcuni attacchi ignorano l’armatura, compresi scudi e armatura naturale, e l’aggressore ha
bisogno solo di toccare un avversario perché l’attacco abbia effetto. 

In questi casi chi attacca effettua un :ref:`tiro_colpire` di contatto (a distanza o in mischia). Quando si subisce questo tipo di attacchi, la propria CA non comprende alcun bonus
di armatura, di scudo o di armatura naturale. Tutti gli altri modificatori, quali ad esempio il Mod Taglia, il Mod Des e il bonus di deviazione si applicano normalmente.

Alcune creature hanno la capacità di effettuare **attacchi di contatto incorporei**. Questi attacchi passano attraverso gli oggetti solidi, come armature e scudi, rendendoli inefficaci contro di essi. Gli attacchi di contatto incorporei funzionano allo stesso modo degli altri attacchi di contatto tranne per il fatto che ignorano anche i bonus di :ref:`copertura`. Questi attacchi non ignorano tuttavia i bonus all’armatura dati da effetti di forza, come :ref:`Armatura Magica<inc_armatura_magica>` e bracciali dell’armatura.

----------------
Danno
----------------
Quando si mette a segno un attacco, si infliggono danni. Il tipo di arma utilizzato determina l’ammontare di danni inflitti che riducono i punti ferita attuali del bersaglio.

Danni minimi
------------
Se le penalità riducono i danni totali a meno di 1, un colpo messo a segno infligge comunque 1
:ref:`Danno_Non_Letale`.

Bonus di Forza
----------------
Quando si colpisce con un’arma in mischia o un’arma da lancio, compresa una fionda, si aggiunge il Mod For al risultato dei danni.

Una penalità di Forza si applica invece agli attacchi compiuti con tutti gli archi che non siano compositi.

:Mano Secondaria: 
	Quando si infliggono danni con un’arma impugnata nella mano secondaria, si aggiunge soltanto la metà del bonus di Forza. 

	Se si ha una penalità in compenso la si aggiunge per intero.
:Arma a Due Mani: 
	Quando si infliggono danni con un’arma impugnata a due mani si aggiunge una volta e mezzo il proprio bonus di Forza, mentre la penalità di Forza non viene moltiplicata. 

	.. Note::
		Non si ottiene questo bonus, quando si impugna a due mani un’arma leggera.

Moltiplicare i Danni
------------------------
Talvolta, è possibile moltiplicare i danni per qualche fattore, come nel caso di un :ref:`Colpo_critico`: si tirano i danni (con tutti i modificatori) più volte, e poi si
sommano i risultati.

.. Important::
	Quando si moltiplica il danno per più di una volta, ogni moltiplicatore utilizza i danni originali non moltiplicati: se si deve raddoppiare due volte il danno, il risultato finale sarà di tre volte il normale danno.

:Eccezione:
	I danni extra in aggiunta (o superiori) ai normali danni dell’arma non vanno mai moltiplicati.

Danni alle Caratteristiche
-----------------------------
Alcune creature ed effetti magici sono in grado di provocare danni temporanei o permanenti
alle caratteristiche: la diminuzione di un punteggio di caratteristica. 

Le regole a riguardo si trovano nell’:ref:`Appendice<danno_caratteristica>`.

----------------
Punti Ferita
----------------
Quando il totale dei punti ferita scende a 0, si diventa :ref:`Inabili<inabile>`. A –1 si diventa :ref:`Morenti<morente>`. Quando si giunge ad un totale negativo pari al proprio valore di Costituzione si :ref:`Muore<morto>`. 

.. _azione_attacco_di_opportunità:
.. _attacco_di_opportunità:
.. _attacco_opportunità:

--------------------------------
Attacco di Opportunità
--------------------------------
A volte un combattente in mischia abbassa lo stesso la guardia. In quel caso, gli avversari che lo circondano possono approfittare di questa breccia nella sua difesa e ottenere attacchi gratuiti in mischia, i cosiddetti **Attacchi di Opportunità (AdO)**.

.. figure:: Immagini/AttacchiDiOpportunita.png
	:width: 700

Quadretti Minacciati
-----------------------
Si minacciano tutti i quadretti in cui è possibile compiere un attacco in mischia, anche quando non è il proprio turno. In generale, questo comprende tutto ciò che si trova nei quadretti adiacenti al proprio spazio, anche in diagonale. 

Un avversario che compie certe azioni in un quadretto minacciato, incorre negli :ref:`Attacchi di Opportunità<attacco_di_opportunità>` del personaggio. 

.. Note::
	Se si è senz’armi, in generale non si minaccia alcun quadretto e pertanto non si possono compiere :ref:`Attacchi di Opportunità<attacco_di_opportunità>`.

****************
Armi con Portata
****************
La maggior parte delle creature di taglia Media o più piccola ha una portata di soli 1,5 metri: possono compiere attacchi in mischia solo contro creature posizionate entro un raggio di 1,5 metri (1 quadretto). 

Ad ogni modo, le creature di taglia Media e Piccola che impugnano armi con portata minacciano più
quadretti del normale e la maggior parte delle creature di taglia superiore a quella Media ha
una portata naturale di 3 metri o più.

Provocare un AdO
-----------------------------------
In generale, sono due i tipi di azioni che provocano :ref:`Attacchi di Opportunità<attacco_di_opportunità>`: uscire da un quadretto minacciato e compiere un’azione all’interno di un quadretto minacciato.

**********
Movimento
**********
Chi esce da un quadretto minacciato di solito incorre negli :ref:`Attacchi di Opportunità<attacco_di_opportunità>` da parte degli avversari che minacciano quel quadretto, ma è possibile evitare questo tipo di attacco grazie a due tecniche piuttosto comuni: un :ref:`Passo<passo>` o una :ref:`Ritirata<azione_ritirata>`.

**************************
Azioni che Fanno Distrarre
**************************
Alcune azioni, se eseguite all’interno di un quadretto minacciato, provocano un :ref:attacco_di_opportunità` perché si distoglie la propria attenzione dalla battaglia. 

In :ref:`Tabella<tabella_azioni>` si trova l'indicazione delle azioni che in genere provocano :ref:`Attacchi di Opportunità<attacco_di_opportunità>`, ma queste stesse azioni a volte fanno eccezione a questa regola.

Compiere un AdO
----------------------------------
Un :ref:`attacco_di_opportunità` è un singolo attacco in mischia aggiuntivo, ed è possibile farne **soltanto uno a round**, ma non è obbligatorio se non si desidera farlo.

Si compie l’:ref:`attacco_di_opportunità` al proprio normale bonus di attacco, anche se si ha già attaccato in quel round. Questi attacchi “spezzano” la normale sequenza delle azioni nel round: un :ref:`attacco_di_opportunità` va risolto subito, non appena viene provocato, per poi proseguire con il turno successivo del personaggio o completare l'azione corrente.

******************************************
Riflessi in Combattimento e AdO Aggiuntivi
******************************************
Se si possiede :ref:`Riflessi in Combattimento<tal_riflessi_in_combattimento>` si aggiunge il proprio Mod Des al numero di :ref:`Attacchi di Opportunità<attacco_di_opportunità>` effettuabili in un round. 

Questo talento non consente di compiere più di un attacco per ogni opportunità, ma se lo stesso avversario provoca due :ref:`Attacchi di Opportunità<attacco_di_opportunità>`, si possono finalizzare entrambi separatamente. 

.. Note::
	Uscire da uno o più quadretti minacciati dallo stesso avversario nello stesso round conta a tutti gli effetti come una sola opportunità. 

Tutti gli :ref:`Attacchi di Opportunità<attacco_di_opportunità>` vengono effettuati al normale bonus di attacco pieno del personaggio.

----------------
Velocità
----------------
La velocità indica la massima distanza che si può coprire in un round e fare ancora qualcosa, come attaccare o lanciare un incantesimo. 

La propria velocità dipende soprattutto dalla razza e dall’armatura che si indossa.

:ref:`Nani`, :ref:`gnomi` e :ref:`halfling` hanno una velocità di 6 metri, o di 4,5 metri se indossano armature medie o pesanti. I :ref:`nani` fanno eccezione avendo una velocità di 6 metri con qualsiasi armatura. 

:ref:`Umani`, :ref:`elfi`, :ref:`mezzelfi` e :ref:`mezzorchi` hanno una velocità di 9 metri, o di 6 metri se indossano armature medie o pesanti.

È possibile compiere due azioni di movimento in un round con un **Movimento_Doppio**, muovendosi
fino al doppio della propria velocità normale, o :ref:`Correre` per tutto il round, muovendosi fino al quadruplo della propria velocità normale o al triplo, in caso si indossi un’armatura pesante.

.. _tiri_salvezza:
.. _ts:

----------------
Tiri Salvezza
----------------
Quando si è influenzati da un attacco insolito o magico in genere si può effettuare un **Tiro Salvezza (TS)** per evitarlo o ridurre l’effetto. 

Un TS, alla pari di un tiro per colpire, è il tiro di un d20 più un bonus in funzione della :ref:`Classe<classi>`, del livello e del punteggio di caratteristica associato. Il modificatore al TS è: **Bonus al TS base + Mod Caratteristica**.

Tipi di TS
------------
I tre tipi diversi di tiri salvezza sono Riflessi, Tempra e Volontà.

Tempra
	Misura il grado di sopportazione contro le sofferenze fisiche o attacchi contro la propria vitalità e salute. A questi tiri si aggiunge il Mod Cos.
Riflessi
	Mettono alla prova la bravura contro gli attacchi ad area. A questi tiri si applica il Mod Des.
Volontà
	Rappresenta la resistenza sia contro l’influenza mentale che contro molti altri effetti magici. A questi tiri si applica il Mod Sag.

CD di un TS
------------
La CD di un :ref:`TS` è determinata dall’attacco stesso. 

Fallimenti e Successi in Automatico
------------------------------------
Un 1 naturale (sul d20 esce 1) su un :ref:`TS<ts>` è sempre un fallimento è può causare :ref:`danni agli oggetti esposti<ts_magia>`. 

Al contrario, un 20 naturale (sul d20 esce 20) è sempre un successo.

Azioni in Combattimento
==========================
Durante un turno, ci sono molteplici azioni che si possono compiere, dal brandire una spada a lanciare un incantesimo.

.. contents:: In breve:
	:local:

----------------
Tipi di Azioni
----------------
Il tipo di azione in sostanza indica quanto tempo occorre per compiere un’azione e quali sono i movimenti possibili, entro il limite di 6 secondi del round di combattimento. 

Esistono sei possibili tipi di azione: :ref:`Standard<azione_standard>`, :ref:`di Movimento<azione_movimento>`, :ref:`Azioni di Round Completo<azione_di_round_completo>`, :ref:`Azioni Veloci<azione_veloce>`, :ref:`Azioni Immediate<azione_immediata>` e :ref:`Azioni Gratuite<azione_gratuita>`.

In un round qualsiasi, si possono compiere un’:ref:`azione_standard` e un’:ref:`azione_movimento`, oppure un’:ref:`azione_di_round_completo`. Si possono inoltre compiere un’:ref:`azione_veloce` e alcune :ref:`Azioni Gratuite<azione_gratuita>`. Per ciascun tipo la :ref:`Tabella<tabella_azioni>` riporta un elenco di cosa è possibile fare.

È possibile sempre sostituire un’:ref:`azione_standard` con un’:ref:`azione_movimento`, ma in alcuni casi, come in un :ref:`round_sorpresa`, si può compiere soltanto un’:ref:`azione_movimento` o un’:ref:`azione_standard`.

Azione Standard
	Permette di fare qualcosa, di solito effettuare un attacco o lanciare un incantesimo.
Azione di Movimento
	Permette di muoversi alla propria velocità o di compiere un’azione che richiede una pari quantità di tempo.

	Si può compiere un’:ref:`azione_movimento` in sostituzione di un’:ref:`azione_standard` e se non si percorre alcuna distanza in un round (di solito perché il proprio movimento è stato scambiato con una o più azioni equivalenti) si può fare solo un passo di 1,5 metri prima, dopo o durante la propria azione.
Azione di Round Completo
	Richiede la completa attenzione per un intero round. L’unico movimento che si può fare durante è un passo di 1,5 metri prima, dopo o durante l’azione. 

	Si possono inoltre compiere :ref:`Azioni Gratuite<azione_gratuita>` e :ref:`Azioni Veloci<azione_veloce>`. 

	.. Note::
		Alcune :ref:`Azioni di Round Completo<azione_di_round_completo>` non permettono di effettuare un passo di 1,5 metri, altre sono effettuabili come :ref:`Azioni Standard<azione_standard>`, ma soltanto nelle situazioni in cui non si può compiere altro che un’:ref:`azione_standard` nel corso del round. Le descrizioni delle azioni specifiche indicano quali azioni ammettono questa opzione.
Azione Gratuita
	Si effettua in poco tempo e con uno sforzo limitato. Si possono compiere una o più :ref:`Azioni Gratuite<azione_gratuita>` mentre si compie un’altra azione normalmente.

	In ogni caso, il GM pone dei limiti sensati a ciò che si può altrimenti fare senza impedimenti.
Azione Veloce
	Consuma una breve frazione di tempo, ma rappresenta un maggior sforzo ed una maggior spesa di energia di un’:ref:`azione_gratuita`, quindi se ne può compiere solo una per turno.
Azione Immediata
	Molto simile ad un’:ref:`azione_veloce`, ma può essere compiuta in qualsiasi momento anche non durante il proprio turno.
Non un’Azione
	Alcuni gesti sono cosi trascurabili da non contare neppure come :ref:`Azioni Gratuite<azione_gratuita>`. A tutti gli effetti, sono istantanei e parte integrante di qualche altra attività, come incoccare una freccia è parte integrante di un’azione di attacco con l’arco.
Attività Limitata
	In alcune situazioni non si può usare l’intero round per compiere tutte le proprie azioni: è possibile compiere soltanto un’:ref:`azione_standard` o un’:ref:`azione_movimento` ed eventualmente le :ref:`Gratuite<azione_gratuita>` e :ref:`Veloci<azione_veloce>`. 

	Non è quindi possibile compiere un’`azione_di_round_completo`, ma si può :ref:`iniziarla o portarla a termine<iniziare_arc>` mediante un’:ref:`azione_standard`.

.. _tabella_azioni:

.. figure:: Immagini/AzioniInCombattimento.png
	:width: 700

	Elenco delle diverse azioni possibili in un combattimento.

.. _azione_standard:

----------------
Azione Standard
----------------
Molte delle azioni comuni che un personaggio può compiere, a parte il movimento, cadono nella categoria delle **Azioni Standard**.

.. contents:: In breve:
	:local:

Attacco
----------------
Effettuare un attacco è un’:ref:`azione_standard`.

.. _azione_attacco_in_mischia:

*******************
Attacchi in Mischia
*******************
Con un’arma da mischia normale, si può colpire un avversario entro 1,5 metri, considerato adiacente, ma alcune armi da mischia hanno una portata, come indicato nelle loro descrizioni. 

Con una tipica arma a portata, è possibile colpire avversari fino a 3 metri di distanza, ma non quelli adiacenti.

******************
Attacchi Senz’Armi
******************
Colpire per infliggere danni con pugni, calci e testate è come attaccare con un’arma da mischia, con le seguenti eccezioni:

Attacchi di Opportunità
	Attaccare senz’armi provoca un :ref:`attacco_di_opportunità` da parte di chi viene attaccato, purché quest’ultimo sia armato.Tale attacco viene effettuato prima del proprio.

	.. Note::
		 Un personaggio che attacca senz’armi non incorre negli :ref:`Attacchi di Opportunità<attacco_di_opportunità>` di altri avversari, e non provoca un :ref:`attacco_di_opportunità` se attacca un avversario disarmato.

		 In generale un personaggio senz’armi non può effettuare  :ref:`Attacchi di Opportunità<attacco_di_opportunità>` a meno che siano ":ref:`Armati<attacchi_armati>`".

.. _attacchi_armati:

Attacchi Senz’Armi “Armati”
	A volte l’attacco senz’armi di un personaggio o di una creatura ha la stessa efficacia di un attacco armato. 

	Un :ref:`monaco`, un personaggio con :ref:`Colpo Senz’Armi Migliorato<tal_colpo_senzarmi_migliorato>`, un incantatore che sprigiona un incantesimo a contatto e una creatura dotata di :ref:`Armi Naturali<attacchi_naturali>` è come se fossero tutti armati.

	.. Note::
		Essere armati conta tanto in attacco quanto in difesa.

Danni da Colpi Senz’Armi
	Un colpo senz’armi assestato da un personaggio di taglia Media infligge 1d3 + Mod For danni contundenti. Il dado utilizzato cambia a seconda della Taglia: 1d2 per la Piccola e 1d4 per quella Grande.

	Tutti i danni inflitti dai colpi senz’armi sono in generale danni non letali e si considerano come armi leggere ai fini delle penalità per gli attacchi con due armi e così via.

Infliggere Danni Letali
	Si può dichiarare che il proprio colpo senz’armi infliggerà danni letali prima di effettuare il tiro per colpire, ma si subisce penalità –4 al tiro per colpire. 

	Se si possiede :ref:`Colpo Senz’Armi Migliorato<tal_colpo_senzarmi_migliorato>` è possibile infliggere danni letali con un colpo senz’armi senza però subire alcuna penalità al tiro per colpire.

.. _azione_attacco_a_distanza:
.. _azione_attacco_a_distanza_con_arma_da_lancio:

*******************
Attacchi a Distanza
*******************
Con un’arma a distanza si può tirare o lanciare contro qualsiasi bersaglio che si trovi entro la gittata massima dell’arma e lungo la linea di visuale. 

La gittata massima di un’arma da lancio è di cinque incrementi di gittata, mentre quella delle armi da tiro è di dieci incrementi. Per ogni incremento necessario si subisce -2 al tiro per colpire.

.. Caution::
	Alcune armi a distanza hanno una gittata massima inferiore, come specificato nelle loro descrizioni.

.. _attacchi_naturali:

******************
Attacchi Naturali
******************
Gli attacchi effettuati con armi naturali, come artigli e morso, sono attacchi in mischia che possono essere compiuti contro qualsiasi creatura che sia a portata, di solito 1,5 m. Questi attacchi sono effettuati usando l’intero BAB ed infliggono una quantità di danno in relazione al loro tipo sommato al Mod For come usuale. 

Non si ricevono attacchi naturali aggiuntivi in base ad un alto BAB, ma se si hanno più arti e parti del corpo in grado di portare un attacco, come indicato dalla razza o dalla capacità che concede questi attacchi. 

.. Note::
	Se si possiede solo un attacco naturale, come il morso (due attacchi con gli artigli non si qualificano come tale), si aggiunge 1.5 x Mod For ai danni inflitti con l’attacco.

.. _attacchi_secondari:

Alcuni attacchi naturali sono qualificati come **attacchi naturali secondari**, come code o ali, e  sono effettuati usando il BAB - 5 e gli si aggiunge solo 0.5 x Mod For al fanno.
 
Si possono effettuare attacchi con armi naturali insieme ad attacchi con un’arma da mischia o con colpi senz’armi, sempre che vengano usati diversi arti per ciascun attacco. Per esempio: non si può effettuare un attacco con l’artiglio ed usare anche la stessa mano per attaccare con una spada lunga. 

Quando si effettuano attacchi addizionali in questo modo, tutti i propri attacchi naturali sono considerati come secondari e quelli effettuati con armi da mischia e colpi senz’armi sono compiuti come se si stesse combattendo con due armi. I propri attacchi naturali sono considerati come armi leggere nella mano secondaria anche ai fini di determinare la penalità agli altri attacchi. 

Talenti come :ref:`Combattere con Due Armi<tal_combattere_con_due_armi>` e :ref:`Multiattacco<tal_multiattacco>` possono ridurre queste penalità.

******************
Attacchi Multipli
******************
Un personaggio che può effettuare più di un attacco per round, deve effettuare un’:ref:`azione_di_attacco_completo` allo scopo di ottenere più di un attacco.

****************************
Tirare o Lanciare in Mischia
****************************
Se si tira o lancia con un’arma a distanza contro un bersaglio impegnato in mischia con un alleato, si subisce penalità –4 al tiro per colpire.

Due personaggi si considerano impegnati in mischia se sono nemici tra loro e se si minacciano l’un l’altro. 

.. Note:: 
	Un personaggio privo di sensi o altrimenti immobilizzato non si considera impegnato in mischia a meno che non sia effettivamente attaccato.

Se il bersaglio o la parte a cui si sta mirando si trova a più di 3 metri di distanza dall’alleato più vicino, il tiratore può evitare la penalità di –4 anche se la creatura a cui sta mirando è impegnata in mischia con un alleato.

in caso il bersaglio sia di due categorie di Taglia più grande dell’alleato con cui è impegnato in mischia, la penalità si riduce a –2 e non si subiscono penalità per tirare ad una creatura che è di tre categorie di Taglia superiore.

.. Caution::
	Se si possiede :ref:`Tiro Preciso<tal_tiro_preciso>` non si subisce questa penalità.

.. _combattere_sulla_difensiva:

****************************
Combattere sulla Difensiva
****************************
Si può scegliere di combattere sulla difensiva quando si attacca. In questo caso, si subisce penalità – 4 a tutti gli attacchi in un round e si ottiene bonus di schivare +2 alla CA nel medesimo round.

.. _colpo_critico:

**************
Colpo Critico
**************
Quando si effettua un tiro per colpire e si ottiene un 20 naturale (sul d20 esce 20), si colpisce a prescindere dalla CA del bersaglio e si mette a segno una minaccia: il colpo potrebbe essere un **Colpo Critico** (o “Critico”). 

Per scoprire se si tratta di un :ref:`colpo_critico`, bisogna effettuare subito un tiro per il critico, ossia un altro tiro per colpire con tutti gli stessi modificatori di quello appena effettuato. Se anche questo va a segno contro la CA del bersaglio, allora il colpo originale è un :ref:`colpo_critico`. Se il tiro per il critico non va a segno, allora il proprio colpo è solo un colpo normale.

.. Note:: 
	Il tiro per il critico deve soltanto colpire per segnare un critico: non è necessario ottenere un altro 20. 

Un :ref:`colpo_critico` significa che occorre tirare il danno più di una volta, con gli stessi bonus, e sommare tutti i risultati.

A meno che non sia indicato diversamente, l’intervallo di minaccia di un colpo critico sul tiro per colpire è 20, e il moltiplicatore è ×2.

Eccezione
	I danni da precisione, come quelli derivanti da un :ref:`Attacco Furtivo<attacco_furtivo_ladro>` del :ref:`ladro`, e i dadi di danno addizionale dati dalle capacità speciali di un’arma, come :ref:`Infuocata<armi_infuocata>`), non vanno mai moltiplicati in un colpo critico.
Intervallo di Minaccia Aumentato
	A volte il proprio intervallo di minaccia è superiore a 20: si può segnare una minaccia con un punteggio inferiore. 

	In questi casi, un tiro minore di 20 non è un colpo automatico: qualsiasi tiro per colpire che non va a segno non è una minaccia.
Moltiplicatore Aumentato
	Alcune armi infliggono più del doppio dei danni con un colpo critico.
Incantesimi e Colpi Critici
	Un incantesimo che richiede un tiro per colpire può mettere a segno un colpo critico e viceversa. 

	Se l’incantesimo causa un danno alle caratteristiche o un :ref:`Risucchio di Caratteristica<risucchio_caratteristica>`, questi sono raddoppiati in caso di colpo critico.

Attivare un Oggetto Magico
--------------------------------
Molti oggetti magici non hanno bisogno di essere attivati, ma altri, come pozioni, pergamene, bacchette, verghe e bastoni, devono essere attivati.

Attivare un oggetto magico è un’:ref:`azione_standard` a meno che la descrizione dell’oggetto non indichi altrimenti.

***************************************
A Completamento
***************************************
Attivare un :ref:`Oggetto a Completamento di Incantesimo<oggetti_completamento_incantesimo>` equivale a lanciare un incantesimo: richiede concentrazione e provoca :ref:`Attacchi di Opportunità<attacco_di_opportunità>`. 

Se viene interrotta la concentrazione, si perde l’incantesimo e si può tentare di attivare l’oggetto :ref:`sulla difensiva<incantare_sulla_difensiva>`, come un incantesimo.

********************************************************
Ad Attivazione, con Parola di Comando o Attivati ad Uso
********************************************************
Attivare uno qualsiasi di questi tipi di oggetti non richiede concentrazione nè provoca :ref:`Attacchi di Opportunità<attacco_di_opportunità>`. 

.. _azione_difesa_totale:
.. _difesa_totale:

Difesa Totale
----------------
Ci si può difendere con un’:ref:`azione_standard`: si ottiene bonus di schivare +4 alla CA per 1 round all’inizio di questa azione. 

Non si può combinare una :ref:`difesa_totale` nè con :ref:`combattere_sulla_difensiva` né con i benefici derivanti da :ref:`Maestria in Combattimento<tal_maestria_in_combattimento>`. Non si possono inoltre compiere :ref:`Attacchi di Opportunità<attacco_di_opportunità>` mentre si compie questa azione.

.. _iniziare_arc:

Iniziare/Completare un'Azione di Round Completo
------------------------------------------------
**Iniziare un’Azione di Round Completo** permette di cominciare un’:ref:`azione_di_round_completo` e finirla il round successivo con un’altra :ref:`azione_standard`. 

Non si può utilizzare questa azione per iniziare o completare un :ref:`attacco_completo`, una :ref:`azione_carica`, :ref:`azione_correre` o :ref:`Ritirarsi<azione_ritirata>`.

.. _lanciare_inc_standard:

Lanciare un Incantesimo
--------------------------------
La maggior parte degli incantesimi richiede un tempo di lancio di un':ref:`azione_standard`: si può lanciare questo tipo di incantesimo, prima o dopo aver effettuato un’:ref:`azione_movimento`.

.. Note::
	Si conserva il proprio bonus di Destrezza alla CA mentre si lancia un incantesimo. 

Informazioni aggiuntive su quanto trattato di seguito si possono trovare nel Capitolo sulla :ref:`magia`

***********	
Componenti 
***********
Per lanciare un incantesimo con una componente **verbale** (V), bisogna parlare con voce forte e se si è imbavagliati o nell’area di un incantesimo :ref:`Silenzio<inc_silenzio>`, non si può lanciare questo tipo di incantesimo. Un incantatore assordato ha una probabilità del 20% di sprecare qualsiasi incantesimo che tenti di lanciare, se quell’incantesimo ha una componente verbale.

Per lanciare un incantesimo con una componente **somatica** (S), bisogna avere almeno una mano libera: non si può lanciare un incantesimo di questo tipo mentre si è legati, in :ref:`Lotta<azione_lottare>` o con entrambe le mani piene o occupate.

Per lanciare un incantesimo con una componente **materiale** (M), **focus** (F) o **focus divina** (FD) bisogna avere a disposizione i materiali adatti, come descritto nell’incantesimo. A meno
che questi materiali non siano complessi, la preparazione di tali materiali è un’:ref:`azione_gratuita`. Per componenti materiali e focus, i cui costi non sono elencati nella descrizione dell’incantesimo, si presume che si abbiano con sé nella propria :ref:`Borsa per Componenti<equip_borsa_componenti>` degli incantesimi.

**************
Concentrazione
**************
Bisogna essere concentrati per lanciare un incantesimo: se non ci si può concentrare, non si può lanciare un incantesimo.

In caso si inizi a lanciare un incantesimo, ma qualcosa interferisca con la concentrazione, bisogna superare una prova di concentrazione o si perde l’incantesimo. La CD della prova :ref:`dipende<concentrazione>` dalla causa che disturba la concentrazione e in caso di fallimento, l’incantesimo non produce alcun effetto: se si preparano gli incantesimi l’incantesimo scompare dalla preparazione, mentre se si lanciano a volontà l’incantesimo conta sul totale giornaliero di incantesimi.

*********************************************
Concentrarsi per Tenere Attivo un Incantesimo
*********************************************
A volte bisogna restare concentrati per tenere attivi alcuni incantesimi con un’:ref:`azione_standard` che non provoca alcun :ref:`attacco_di_opportunità`.

Qualsiasi cosa spezzi la concentrazione di chi lancia un incantesimo può, allo stesso modo, spezzare la concentrazione per tenerlo attivo. Se la concentrazione viene interrotta l ’incantesimo si spezza.

****************
Tempo di Lancio
****************
La maggior parte degli incantesimi ha un tempo di lancio di un':ref:`azione_standard`. Un incantesimo lanciato in questo modo ha effetto istantaneo.

************************
Attacchi di Opportunità
************************
In generale, chi lancia un incantesimo incorre negli :ref:`Attacchi di Opportunità<attacco_di_opportunità>` di tutti gli avversari che lo minacciano. 

In caso l’incantatore subisca danni da uno di questi attacchi, deve superare una prova di concentrazione con **CD = 10 + danni subiti + Liv Incantesimo** o perde l’incantesimo. 

.. Caution::
	Gli incantesimi che si lanciano con un’:ref:`azione_gratuita` non provocano alcun :ref:`attacco_di_opportunità`.

.. _azione_lanciare_incantesimi_sulla_difensiva:
.. _incantare_sulla_difensiva:

*************************
Incantare sulla Difensiva
*************************
Lanciare un incantesimo sulla difensiva non provoca alcun :ref:`attacco_di_opportunità`, ma ad ogni modo, occorre una prova di concentrazione con **CD = 15 + 2 x Liv Incantesimo** per riuscire nell’intento. 

Un fallimento significa che l’incantesimo viene perso. 

**********************
Incantesimi a Contatto
**********************
Molti incantesimi hanno un raggio di azione a “contatto”: bisogna lanciare l’incantesimo
e poi toccare il soggetto. 

Nello stesso round in cui si lancia l’incantesimo, occorre toccare (o meglio, provarci) il soggetto come :ref:`azione_gratuita`. Si può toccare un alleato in automatico o usare l’incantesimo su se stessi, ma per toccare un avversario bisogna mettere a segno un tiro per colpire.
 
Ci si può muovere prima di lanciare l’incantesimo, dopo aver toccato il bersaglio o in qualsiasi
istante tra il lancio e il contatto. 

Attacchi di Contatto
	Toccare un avversario con un incantesimo a contatto equivale ad un attacco armato e come tale non provoca :ref:`Attacchi di Opportunità<attacco_di_opportunità>`, ciò nonostante il lancio in sé lo provoca.

	Gli attacchi di contatto sono di due tipi: in mischia e a distanza. Si può mettere a segno colpi critici con entrambi i tipi di attacco, sempre che gli incantesimi infliggano danno. 

	La CA di un avversario contro un attacco di contatto non comprende alcun bonus di armatura, scudo o armatura naturale, mentre il modificatore di Taglia e Destrezza e il bonus di deviazione si applicano tutti normalmente.
Conservare la Carica
	Se non si scarica l’incantesimo nel round in cui lo si lancia, è possibile conservarne la carica per un tempo indeterminato. Si può continuare a effettuare attacchi di contatto un round dopo l’altro. 

	Se si tocca qualcosa o qualcuno mentre si conserva la carica, anche non intenzionalmente, l’incantesimo si scarica; se si lancia un altro incantesimo, l’incantesimo a contatto si spezza. Si può toccare un alleato come :ref:`azione_standard` o fino a sei come :ref:`azione_di_round_completo`. 

	In alternativa, si può portare un normale attacco senz’armi, o con un’arma naturale, mentre si conserva la carica. In questo caso, non si è considerati armati e si possono provocare :ref:`Attacchi di Opportunità<attacco_di_opportunità>` come normale per l’attacco: se l’attacco non provoca normalmente :ref:`Attacchi di Opportunità<attacco_di_opportunità>`, non lo provoca neppure questo attacco.

	Se l’attacco ha successo, si infliggono danni normali per l’attacco senz’armi o l’arma naturale e l’incantesimo viene scaricato. Se l’attacco, non va a segno, la carica continua a conservarsi.

**********************************
Incantesimi di Contatto a Distanza
**********************************
Alcuni incantesimi permettono di effettuare un attacco di contatto a distanza come parte del lancio dell’incantesimo, senza richiedere un'azione separata. 

Gli attacchi di contatto a distanza provocano :ref:`Attacchi di Opportunità<attacco_di_opportunità>`, anche se l’incantesimo che prevede l’attacco è lanciato :ref:`sulla difensiva<incantare_sulla_difensiva>`. 

A meno che non sia indicato diversamente, gli attacchi a contatto a distanza non possono essere mantenuti fino al turno successivo.

***************************
Interrompere un Incantesimo
***************************
Interrompere un incantesimo attivo è un’:ref:`azione_standard` che non provoca alcun :ref:`attacco_di_opportunità`.

.. figure:: Immagini/Combattimento2.png

Usare una Capacità Speciale
--------------------------------
Usare una capacità speciale è di solito un’:ref:`azione_standard`, ma quale che sia il suo tipo è definito dalla capacità in sé.

***********************
Capacità Magiche (Mag)
***********************
Usare una capacità magica funziona come lanciare un incantesimo, ossia richiede :ref:`concentrazione` e provoca :ref:`Attacchi di Opportunità<attacco_di_opportunità>`. 

Le capacità magiche si possono bloccare: se la concentrazione viene interrotta, il tentativo di usare la capacità fallisce, ma conta come se essa fosse stata usata. 

Usare una Capacità Magica sulla Difensiva
	Si può tentare di usare una capacità magica sulla :ref:`difensiva<incantare_sulla_difensiva>`, proprio come con un incantesimo: se la prova di concentrazione con **CD = 15 + 2 x Liv Incantesimo** fallisce non si può usare la capacità, ma il tentativo conta come se fosse stata usata.

******************************
Capacità Soprannaturali (Sop)
******************************
	L'uso di una capacità soprannaturale non può essere interrotto, non richiede :ref:`concentrazione` e non provoca :ref:`Attacchi di Opportunità<attacco_di_opportunità>`.

****************************
Capacità Straordinarie (Str)
****************************
Usare una capacità straordinaria di solito non è un’azione perché la maggior parte delle capacità straordinarie si verifica in automatico, per reazione. 

Quelle capacità straordinarie che sono azioni di solito sono :ref:`Azioni Standard<azione_standard>` che non possono essere interrotte, non richiedono :ref:`concentrazione` e non provocano :ref:`Attacchi di Opportunità<attacco_di_opportunità>`. 

.. _azione_movimento:
.. _azione_di_movimento:

--------------------------------
Azione di Movimento
--------------------------------
.. contents:: In breve:
	:local:

A differenza di alcune abilità relative al movimento, la maggior parte delle **Azioni di Movimento** non richiede alcuna prova.

Movimento
--------------------------------
La più semplice delle azioni di movimento è muoversi alla propria velocità: se si effettua questo tipo di azione durante il proprio turno, non si può fare anche il passo di 1,5 metri.

Molti tipi speciali di movimento rientrano in questa categoria, come :ref:`Scalare<scalare>` e :ref:`Nuotare<nuotare>`, entrambe fino a un quarto della propria velocità,.

*******************
Scalare Accelerato
*******************
Si può scalare alla metà della propria velocità come :ref:`azione_di_movimento`, con penalità –5 alla prova di :ref:`Scalare<scalare>`.

**************
Andar Carponi
**************
Si può avanzare carponi per 1,5 metri come :ref:`azione_di_movimento`. 

Quando si va carponi, durante la propria avanzata a terra si incorre negli :ref:`Attacchi di Opportunità<attacco_di_opportunità>` di tutti gli avversari che minacciano i quadretti adiacenti. 

Un personaggio che va carponi è considerato :ref:`Prono<prono>` e deve compiere un’:ref:`azione_di_movimento` per :ref:`alzarsi_in_piedi`, provocando un :ref:`attacco_di_opportunità`.

.. _alzarsi_in_piedi:

Alzarsi in Piedi
--------------------------------
Alzarsi in piedi da una posizione :ref:`Prona<prono>` richiede un’:ref:`azione_di_movimento` e provoca :ref:`Attacchi di Opportunità<attacco_di_opportunità>`.

Dirigere o Cambiare Direzione a un Incantesimo
----------------------------------------------------------------
Alcuni incantesimi permettono di dirigere i loro effetti, dopo averli lanciati, contro nuovi bersagli o aree. 

Cambiare direzione a un incantesimo equivale a un’:ref:`azione_di_movimento`, non provoca :ref:`Attacchi di Opportunità<attacco_di_opportunità>` e non richiede :ref:`concentrazione`.

Estrarre o Rinfoderare l'Arma
--------------------------------
Con un’:ref:`azione_di_movimento`, si può estrarre un’arma da usare in battaglia o rinfoderarla per avere una mano libera. Questa azione vale anche per altri mezzi di attacco tenuti a portata di mano, come le bacchette. 

.. Note::
	Se l’arma o i mezzi di attacco sono contenuti in uno zaino o sono altrimenti difficili da prendere, questa azione va considerata alla stregua di un’azione per :ref:`Recuperare un Oggetto Custodito<recuperare_oggetto_custodito>`.

Con un BAB pari o superiore a +1 si può estrarre un’arma come :ref:`azione_gratuita`, abbinata a
un’:ref:`azione_di_movimento` e se si possiede :ref:`Combattere con Due Armi<tal_combattere_con_due_armi>` è possibile estrarre due armi leggere o due armi a una mano nello stesso tempo che normalmente occorrerebbe per estrarne una sola.

Estrarre le munizioni da usare con un’arma a distanza come frecce, proiettili per fionda, quadrelli o shuriken è un’:ref:`azione_gratuita`.

.. _maneggiare_oggetto:
.. _recuperare_oggetto_custodito:

Maneggiare un Oggetto
--------------------------------
Nella maggior parte dei casi, muoversi e maneggiare un oggetto è un’:ref:`azione_di_movimento`.

Questa categoria comprende recuperare o mettere via un oggetto custodito, raccogliere un oggetto, spingere un oggetto pesante e aprire una porta. Altri esempi, insieme agli eventuali :ref:`Attacchi di Opportunità<attacco_di_opportunità>` provocati, sono elencati nella :ref:`Tabella delle Azioni<tabella_azioni>`.


Montare/Smontare da una Cavalcatura
----------------------------------------------------------------
Montare o smontare da una cavalcatura richiede un’:ref:`azione_di_movimento`.

Montare o Smontare Veloce
	Si può montare o smontare da una cavalcatura come :ref:`azione_gratuita` con una prova di :ref:`Cavalcare<cavalcare>` con CD 20. Se si fallisce la prova, montare o smontare viene invece considerata come :ref:`azione_di_movimento`. 

	Non si può tentare di montare o smontare velocemente se non si è già in grado di montare o smontare da una cavalcatura in quel round.

Preparare o Lasciar Cadere uno Scudo
----------------------------------------------------------------
Allacciare uno scudo all’avambraccio per ottenere il bonus di scudo alla propria CA o lasciarlo cadere per avere una mano libera a disposizione, corrisponde a un’:ref:`azione_di_movimento`.

Se si ha un BAB pari o superiore a +1 si può imbracciare o slegare uno scudo come :ref:`azione_gratuita`, purchè sia abbinata a un’:ref:`azione_di_movimento`.

Lasciar cadere uno scudo trasportato, ma non allacciato, equivale a un’:ref:`azione_gratuita`.

.. _azione_di_round_completo:

--------------------------------
Azione di Round Completo
--------------------------------
Un’**Azione di Round Completo** richiede un round intero per essere ultimata. 

Non la si può quindi combinare con un’:ref:`azione_standard` o un’:ref:`azione_di_movimento`, tuttavia, se non si percorre alcuna distanza, è possibile compiere un passo di 1,5 metri.

.. _azione_di_attacco_completo:
.. _attacco_completo:

Attacco Completo
--------------------------------
Se si possiede più di un attacco per round perché il proprio bonus di attacco base è sufficientemente alto, si combatte con due armi, si usa un’arma doppia o per qualche altro motivo speciale, si deve usare l’azione di **Attacco Completo** per poter compiere questi attacchi aggiuntivi. 

Non è necessario dichiarare in anticipo quali bersagli si vuole attaccare. Si può attendere l’esito dei primi prima di decidere come portare gli ultimi e nel corso di un attacco completo, è possibile soltanto compiere un passo di 1,5 metri, prima, dopo o tra un attacco e l’altro.

Se si hanno attacchi multipli in ragione del proprio BAB, bisogna compiere gli attacchi in ordine, dal bonus più alto a quello più basso. Se si impugnano due armi, è possibile portare il primo attacco con una delle due armi, indifferentemente. Se si impugna un’arma doppia, si
può portare il primo attacco con una delle due estremità, indifferentemente.

*************************************************************
Scegliere tra un Attacco e uno Completo
*************************************************************
Dopo aver portato il primo attacco, si può scegliere di compiere un’:ref:`azione_di_movimento` invece di completare gli attacchi rimanenti, in base all’esito del primo attacco e al fatto che non si è ancora compiuto nessun movimento per quel round. 

Se si è già fatto un passo di 1,5 metri, non si può usare la propria :ref:`azione_di_movimento` per coprire alcuna distanza, ma si potrebbe lo stesso usare un altro tipo di :ref:`azione_di_movimento`.

***************************
Combattere sulla Difensiva 
***************************
Si può scegliere di combattere sulla difensiva quando si compie un’:ref:`attacco_completo`. In tal caso si subisce penalità –4 a tutti gli attacchi nel round e si ottiene bonus di schivare +2 alla CA in quello stesso round.

Avanzare di 1.5 metri su Terreno Difficile
----------------------------------------------------------------
A volte il movimento è cosi ostacolato da non permettere neppure un passo di 1,5 metri. In questi casi bisogna usare un’:ref:`azione_di_round_completo` per muoversi di 1,5 metri in qualsiasi direzione, anche in diagonale. 

Anche se questo sembra in tutto e per tutto un passo di 1,5 metri, non lo è e provoca :ref:`Attacchi di Opportunità<attacco_di_opportunità>`.

.. _azione_correre:
.. _correre:

Correre
--------------------------------
Si può correre come :ref:`azione_di_round_completo`. In questo caso, però, non si ottiene un passo di 1,5 metri. Quando si corre, si è quattro volte più veloci del normale in linea retta; tre in caso si indossi un’armatura pesante. 

Correndo si perde il bonus di Destrezza alla CA, a meno che non si abbia il talento :ref:`Correre<tal_correre>`, e lo si può fare per un numero di round pari al proprio
valore di Costituzione. Oltre questa soglia bisogna effettuare una prova di Costituzione con CD 10 per continuare la corsa. La prova va ripetuta ogni round in cui si continua a correre con un incremento alla CD di 1 per ogni round successivo.

Quando si fallisce la prova, non si può più correre: un personaggio che ha corso fino allo stremo deve riposarsi per 1 minuto (10 round) prima di tornare a correre e durante questo periodo non può muoversi più in fretta di una normale :ref:`azione_di_movimento`.

.. Note::
	Non si può correre attraverso un terreno difficile o se non si vede in che direzione si sta andando.

Una corsa rappresenta una velocità di circa 18 km all’ora per un umano senza ingombro.

Lanciare un Incantesimo
--------------------------------
Un incantesimo che si lancia in un round è un’:ref:`azione_di_round_completo` e ha effetto poco prima dell’inizio del proprio round successivo a quello in cui lo si lancia. Si agisce normalmente dopo aver completato l’incantesimo.

Un incantesimo che si lancia in 1 minuto ha effetto immediatamente prima del proprio turno 1 minuto più tardi e per ciascuno di quei 10 round, si continua a lanciare l’incantesimo come :ref:`Azione di Round Completo<azione_di_round_completo>`. Queste azioni devono essere consecutive e ininterrotte, pena il fallimento automatico dell’incantesimo.

Quando si inizia un incantesimo che richiede 1 o più round di lancio bisogna invocare, gesticolare e concentrarsi a partire dal proprio turno nel round di lancio e continuare fino a poco prima dell’inizio del proprio turno nel round successivo (come minimo). Se si perde la concentrazione dopo aver iniziato il lancio dell’incantesimo e prima del suo completamento, l’incantesimo va in fumo.

Si provocano :ref:`Attacchi di Opportunità<attacco_di_opportunità>` soltanto all’inizio del
lancio di un incantesimo e mentre viene lanciato non si minaccia alcun quadretto intorno a sé. Questa azione è altrimenti identica a quella di :ref:`Lanciare un Incantesimo con un'Azione Standard<lanciare_inc_standard>`.

***********************************
Lanciare un Incantesimo Metamagico
***********************************
:ref:`Bardi<bardo>` e :ref:`Stregoni<stregone>` impiegano un tempo più lungo del normale per lanciareuna versione metamagica di un incantesimo: se il normale tempo di lancio dell’incantesimo è un':ref:`azione_standard`, il tempo di lancio di una versione metamagica dello stesso incantesimo per queste classi diventa di un’:ref:`azione_di_round_completo`, a meno che siano modificati da :ref:`Incantesimi Rapidi<tal_incantesimi_rapidi>`. 

.. Note::
	Questo non vale in caso di un incantesimo con un tempo di lancio di 1 round.

Gli incantesimi di questo tipo non richiedono un’:ref:`azione_di_round_completo` per essere lanciati hanno effetto nello stesso round in cui si inizia a lanciarli, senza la necessità di continuare l’invocazione, i gesti e la concentrazione fino al turno successivo. 

Nel caso di incantesimi con tempo di lancio più lungo occorre un’:ref:`azione_di_round_completo` extra per lanciare la versione metamagica degli stessi.

:ref:`Chierici<chierico>` e :ref:`Druidi<druido>` impiegano un tempo più lungo del normale per lanciare spontaneamente una versione metamagica di qualsiasi incantesimo :ref:`Curare<inc_curare>`, :ref:`Infliggere<inc_infliggere>` o :ref:`Evocare<inc_evocare>`. Il lancio spontaneo della versione metamagica di un incantesimo con un tempo di lancio di un':ref:`azione_standard` è un’:ref:`azione_di_round_completo`, mentre quelli con tempo di lancio più lungo si possono lanciare con un’:ref:`azione_di_round_completo` aggiuntiva.

.. _azione_ritirata:

Ritirarsi
--------------------------------
Arretrare da uno scontro in mischia è un’:ref:`azione_di_round_completo`: quando ci si ritira, ci si può muovere fino al doppio della propria velocità. 

Il quadretto di partenza non si considera minacciato da alcun nemico che si possa vedere, perciò i nemici visibili non ottengono :ref:`Attacchi di Opportunità<attacco_di_opportunità>` quando si esce da quel quadretto, a contrario di quelli invisibili. 

.. Caution::
	Non ci si può ritirare se si è :ref:`Accecati<accecato>` e non è possibile fare un passo di 1,5 metri nello stesso round in cui ci si ritira.

Se, nel corso della ritirata, si esce da un quadretto minacciato diverso da quello di partenza, i nemici ottengono :ref:`Attacchi di Opportunità<attacco_di_opportunità>` come di norma. Non ci si può ritirare con una forma di movimento di cui non è riportata la velocità.

.. Note::
	Si noti che a dispetto del nome di questa azione, non è necessario affatto rinunciare completamente allo scontro.

*******************
Ritirata Limitata
*******************
Se si può compiere soltanto un’:ref:`azione_standard` per round, ci si può ritirare come :ref:`azione_standard`, ma muovendosi fino alla propria velocità massima.

Usare una Capacità Speciale
--------------------------------
Usare una capacità speciale è di solito un’:ref:`azione_standard`, ma potrebbe anche essere necessaria un’:ref:`azione_di_round_completo`, come definito dalla capacità in sé.

.. _azione_gratuita:

--------------------------------
Azione Gratuita
--------------------------------
Le **Azioni Gratuite** sono di per sé istantanee e non richiedono tempo, tuttavia ci possono essere dei limiti al numero massimo di azioni gratuite effettuabili a ogni round. Ad ogni modo un’:ref:`azione_gratuita` raramente provoca un :ref:`attacco_di_opportunità`. 

Alcune tra azioni più comuni sono descritte di seguito.

Cadere a Terra (Prono)
--------------------------------
Cadere a terra in posizione :ref:`Prona<prono>` nel proprio spazio è un’:ref:`azione_gratuita`.

Interrompere la Concentrazione
--------------------------------
Si può smettere di concentrarsi su un incantesimo con un’:ref:`azione_gratuita`. 

Lasciar Cadere un Oggetto
--------------------------------
Lasciar cadere un oggetto nel proprio spazio o in un quadretto adiacente è un’:ref:`azione_gratuita`.

Parlare
--------------------------------
In generale, parlare è un’:ref:`azione_gratuita` che si può compiere anche se non è il proprio turno, ma pronunciare più di qualche frase è di solito oltre il limite.

.. _azione_veloce:

--------------------------------
Azione Veloce
--------------------------------
Un’**Azione Veloce** consuma una breve frazione di tempo, ma rappresenta un maggior sforzo ed una maggior spesa di energia di una :ref:`Gratuita<azione_gratuita>`: se ne può compiere una sola per turno senza influire sulla propria capacità di compiere altre azioni. 

A riguardo, un’:ref:`azione_veloce` è come un’:ref:`azione_gratuita` di cui però se ne può compiere una sola a turno, a prescindere da quale altra azione si vuole intraprendere. Si può compiere un’:ref:`azione_veloce` ogni volta che si è in grado di poter compierne una :ref:`Gratuita<azione_gratuita>`. 

Queste azioni di solito riguardano lanciare incantesimi, attivare un talento o un oggetto magico.

Lanciare un Incantesimo Rapido
--------------------------------	
Si può lanciare un :ref:`Incantesimo Rapido<tal_incantesimi_rapidi>`, o qualsiasi incantesimo con tempo di lancio di un’:ref:`azione_gratuita` o veloce, con un’:ref:`azione_veloce`. 

Se ne può lanciare soltanto uno di questo tipo a round ed esso non conta sul normale limite di un incantesimo al round, nè provoca alcun :ref:`attacco_di_opportunità`.

.. _azione_immediata:

--------------------------------
Azione Immediata
--------------------------------
Come l’:ref:`azione_veloce`, un’**Azione Immediata** consuma una breve frazione di tempo ma rappresenta un maggior sforzo e consumo di energia di un’:ref:`azione_gratuita`. A differenza di
una :ref:`Veloce<azione_veloce>`, tuttavia, un’:ref:`Azione_Immediata` può essere compiuta in qualsiasi momento, anche non durante il proprio turno.

.. Note::
	Lanciare :ref:`Caduta Morbida<inc_caduta_morbida>` è un’:ref:`azione_immediata`, dato che l’incantesimo può essere lanciato in qualsiasi momento. 

Utilizzare un’:ref:`azione_immediata` nel proprio turno è uguale ad usare un’:ref:`azione_veloce` e conta come :ref:`azione_veloce` per quel turno: non si può utilizzare un’altra :ref:`azione_immediata` o :ref:`Veloce<azione_veloce>` fino a dopo il proprio turno successivo. Non si può inoltre usare questo tipo di azione se si è :ref:`Impreparati<impreparato>`.

--------------------------------
Azioni Varie
--------------------------------
Le azioni seguenti richiedono un tempo variabile per essere compiute o funzionano diversamente dalle altre azioni.

.. _passo:

Fare un Passo di 1.5 metri
--------------------------------
Si può fare un passo di 1,5 metri in qualsiasi round in cui non si compie alcun altro tipo di movimento, senza provocare un :ref:`attacco_di_opportunità`: non si può fare più di un passo di 1,5 metri a round, nè si può fare se si effettua un altro movimento nello stesso round.

Il passo può essere compiuto prima, dopo o durante la propria azione nel round, soltanto se non ci si trova in terreno difficile o al buio. 

Qualsiasi creatura con una velocità di 1,5 metri o inferiore non può fare un passo di 1,5 metri,
poiché una creatura così lenta, anche solo per muoversi di un passo, dovrebbe usare un’:ref:`azione_di_movimento`. Lo stesso vale per forme di movimento di cui non è riportata la velocità.

Usare un Talento
--------------------------------
Alcuni talenti consentono di compiere azioni speciali in battaglia, altri, che non sono azioni di per sé, conferiscono un bonus per fare qualcosa che si può già fare. Alcuni non sono concepiti per essere usati in combattimento. 

Le rispettive descrizioni riportano tutto ciò che occorre sapere su di essi.

Usare un'Abilità
--------------------------------	
L’uso delle abilità è per lo più un’:ref:`azione_standard`, ma alcune potrebbero essere :ref:`di Movimento<azione_movimento>`, :ref:`di Round Completo<azione_di_round_completo>`, :ref:`Gratuite<azione_gratuita>` o cose di tutt’altro genere.

Le rispettive descrizioni delle :ref:`Abilità<abilità>` riportano il tipo di azioni richiesto per farne uso.

Ferimento e Morte
===================
I punti ferita misurano la difficoltà a morire: per quanti punti ferita si possano perdere, non si subisce alcuna penalità finché essi non scendono a 0 o meno.

.. contents:: In breve:
	:local:

--------------------------------
Perdita di Punti Ferita
--------------------------------
Il modo più comune di farsi male per un personaggio è subire danni letali, ossia perdere punti ferita.


Cosa rappresentano
-------------------
I punti ferita hanno un duplice significato nel mondo di gioco: da un lato la capacità di sopportare sofferenze fisiche e tirare avanti, dall’altro la capacità di trasformare un brutto colpo in uno meno grave.

Effetti dei Danni
--------------------
I danni continuano a cumularsi finché i propri punti ferita non arrivano a 0 o meno. 

A 0 punti ferita si è :ref:`Inabili<inabile>`. Con un totale negativo ma non uguale al proprio valore di Costituzione, si perdono i sensi e si diventa :ref:`Morenti<morente>`. Quando il totale dei punti ferita negativi è uguale al proprio valore di Costituzione, si :ref:`Muore<morto>`.

Danno massiccio (opzionale)
----------------------------
Se si subisce un unico attacco che infligge una quantità di danni pari alla metà dei propri punti ferita totali (minimo 50 danni) o più e non si muore sul colpo, bisogna effettuare un TS su Tempra con CD 15. 

Se si fallisce questo TS si :ref:`Muore<morto>`, a prescindere dai propri punti ferita attuali. Questa regola si applica solo a colpi singoli e non vale per attacchi multipli che singolarmente non raggiungono i danni designati.

.. _inabile:

--------------------------------
Inabile
--------------------------------
Quando i punti ferita attuali scendono esattamente a 0, si diventa **Inabili**: si diventa :ref:`Barcollanti<barcollante>` e si può compiere ad ogni turno soltanto un’:ref:`azione_di_movimento` o una :ref:`Standard<azione_standard>`. 

Si possono compiere :ref:`azione_di_movimento` senza aggravare la propria condizione, ma se si compie qualsiasi :ref:`azione_standard` (o qualsiasi altra attività faticosa), si subisce 1 danno dopo aver portato a termine l’azione. 

.. Note::
	A meno che questa attività non faccia aumentare i propri punti ferita, ci si ritrova a –1 punti ferita e :ref:`Morenti<morente>`.

Una guarigione che porti i propri punti ferita sopra lo 0 permette di riacquistare la piena capacità, proprio come se i punti ferita non fossero mai scesi a 0 o meno.

Si può anche diventare inabili man mano che ci si riprende dall’essere :ref:`Morenti<morente>`. In tal caso, si è un passo sulla via della guarigione, :ref:`Stabili e in Recupero<stabilizzati_e_recupero>` , quindi, si possono anche avere meno di 0 punti ferita.

.. _morente:

--------------------------------
Morente
--------------------------------
Quando i propri punti ferita attuali sono negativi, ma non uguali o superiori al proprio valore di Costituzione, si è **Morenti**: si perdono i sensi e non si può compiere alcuna azione.

In questa condizione si perdono 1 punto ferita a round, fino a quando non si :ref:`Muore<morto>` o si diventa :ref:`Stabili<stabilizzati_e_recupero>`.

.. _morto:

--------------------------------
Morto
--------------------------------
Quando i punti ferita attuali scendono ad un ammontare negativo uguale o superiore al proprio valore di Costituzione, si **Muore**. 

Si può anche morire per aver subito danni alle caratteristiche o per aver sofferto un :ref:`Risucchio di Caratteristica<risucchio_caratteristica>` che riduce il proprio punteggio di Costituzione a 0.

.. Important::
	Certi tipi di magia potente, come :ref:`Rianimare Morti<inc_rianimare_morti>` e :ref:`Resurrezione<inc_resurrezione>`, sono in grado di riportare in vita un personaggio morto.

.. _stabilizzati_e_recupero:
.. _stabili:

----------------------------------
Personaggi Stabilizzati e Recupero
----------------------------------
A partire dal round in cui i punti ferita sono scesi in negativo, senza morire, e per tutti i turni successivi, bisogna effettuare una prova di Costituzione con CD 10 per vedere se si diventa **Stabili**. Questo tiro subisce una penalità pari al totale di punti ferita negativi. 

.. Note::
	Se si è già :ref:`Stabili<stabili>` non occorre effettuare questa prova. 

.. figure:: Immagini/Combattimento3.png

Il risultato di 20 alla prova è sempre un successo automatico, metre se la prova fallisce, si perde 1 punto ferita. Quando si è privi di sensi o morenti, inotre, non si possono usare azioni speciali che cambino il conteggio di iniziativa al quale l’azione accade.

Se si subiscono danni continuati, come da freccia acida o effetti di :ref:`Sanguinamento<sanguinamento>`, si fallisce automaticamente la prova di Costituzione per stabilizzarsi e si subisce 1 punto ferita aggiuntivo a round oltre al danno continuato.

Si può anche evitare che un personaggio :ref:`morente` continui a perdere punti ferita e renderlo stabile con una prova riuscita di :ref:`Guarire<guarire>` con CD 15 e se una qualunque guarigione cura il personaggio anche di 1 solo danno, questi smette di perdere punti ferita e
diventa :ref:`Stabile<stabili>`. 

Una guarigione che fa aumentare i punti ferita fino a 0, permette al personaggio di diventare cosciente e :ref:`Inabile<inabile>`. Una guarigione che fa aumentare i suoi punti ferita fino a 1 o più, permette al personaggio di riacquistare la piena capacità, come se i suoi punti ferita non fossero mai scesi a 0 o meno. Un incantatore, per esempio, conserva qualunque capacità di lanciare incantesimi che possedeva prima di scendere a 0 punti ferita.

Un personaggio :ref:`Stabile<stabili>` che è stato curato da un guaritore o che è stato guarito magicamente, alla fine riprenderà conoscenza e comincerà a recuperare normalmente i punti ferita, tuttavia, se il personaggio non ha nessuno che possa aiutarlo, la sua vita è ancora in pericolo e potrebbe comunque perderla.

Recupero Assistito
-------------------
Un’ora dopo che un personaggio :ref:`morente` ma sotto cura è diventato :ref:`Stabile<stabili>`, deve effettuare una prova di Costituzione con CD 10 per riprendere i sensi, con una penalità pari al totale di punti ferita negativi. 

Se il personaggio recupera i sensi, ma è ancora in negativo, è trattato come se fosse :ref:`inabile`. Se rimane privo di sensi, invece, riceve ogni ora la stessa prova per riprendersi. Il
risultato di 20 alla prova è sempre un successo automatico.

Anche se privo di sensi, recupera i punti ferita come di norma e torna in condizioni normali quando i suoi punti ferita salgono a 1 o più.

Recupero non Assistito
------------------------
Un personaggio gravemente ferito abbandonato a se stesso di solito :ref:`Muore<morto>`. In ogni caso, ha una piccola probabilità di riprendersi da solo: va trattato come se tentasse un recupero assistito, ma ogni volta che fallisce la prova di Costituzione per riprendere i sensi perde 1 punto ferita. 

Un personaggio non assistito non è in grado di recuperare punti ferita per via naturale, ma una volta cosciente può effettuare una prova di Costituzione con CD 10 ogni giorno, dopo aver riposato 8 ore, per iniziare a recuperare per via naturale. Egli subisce anche una penalità al tiro pari al totale dei punti ferita negativi. 

Se fallisce la prova perde 1 punto ferita, ma senza perdere i sensi. Una volta superata questa prova, il personaggio continua a recuperare per via naturale e non è più in pericolo di perdere punti ferita.

--------------------------------
Guarigione
--------------------------------
Dopo aver subito danni, è possibile recuperare punti ferita per guarigione naturale o per guarigione magica, ma, in ogni caso, non si possono recuperare punti ferita oltre il proprio
ammontare massimo.

Guarigione Naturale
-----------------------
Una notte completa di riposo (8 ore di sonno o più), permette di recuperare 1 punto ferita per ogni livello del personaggio, ma qualsiasi interruzione significativa che interrompa il riposo impedisce di guarire per quella notte.

Se ci si sottopone a un riposo totale a letto di un giorno e di una notte, si recupera due volte il proprio livello del personaggio in punti ferita.

Guarigione Magica
-------------------
Diverse capacità e incantesimi sono in grado di ripristinare i punti ferita.

Limiti di Guarigione
--------------------
Non è possibile recuperare più punti ferita di quelli persi e la guarigione magica non fa eccezione: non è possibile aumentare i punti ferita attuali oltre i propri normali punti ferita totali.

Guarigione di Danni alle Caratteristiche
------------------------------------------
I danni alle caratteristiche temporanei si ripristinano al ritmo di 1 punto per ogni notte di riposo (8 ore) per ciascuna caratteristica influenzata. Un completo riposo a letto permette invece di ripristinarne 2 al giorno (24 ore).

--------------------------------
Punti Ferita Temporanei
--------------------------------
Certi effetti forniscono punti ferita temporanei, che vanno ad aggiungersi a quelli attuali e quando si subiscono danni, si sottraggono prima dai temporanei. I danni in eccesso si applicano ai punti ferita attuali come di norma. 

Se l’effetto che conferisce i punti ferita temporanei finisce o viene dissolto, i restanti punti ferita temporanei svaniscono senza che il danno da essi sostenuto venga trasferito ai punti ferita correnti.

Quando i punti ferita temporanei sono perduti non possono essere reintegrati come i punti ferita reali, neppure dalla magia.

Incrementi di Costituzione e Punti Ferita
-----------------------------------------
Un aumento del valore di Costituzione di un personaggio, anche temporaneo, può fornire un effettivo aumento di punti ferita, non considerati temporanei: si possono reintegrare e non si perdono per primi, a differenza di questi ultimi.

.. _danno_non_letale:

--------------------------------
Danno Non Letale
--------------------------------
Il **Danno non Letale** rappresenta delle ferite che non sono tali da mettere in pericolo di vita e che, a differenza del danno normale, si guariscono velocemente con il semplice riposo.

Infliggere Danni non Letali
----------------------------
Alcuni attacchi infliggono danni non letali così come altri effetti, come il calore o l’essere esausti. Quando si subiscono questi danni, occorre registrare il totale corrente di quelli
accumulati. 

I danni non letali non vanno sottratti dai propri punti ferita attuali poiché non sono “reali”, ma quando sono pari ai propri punti ferita attuali si diventa :ref:`barcollanti<barcollante>` e
quando li superano si :ref:`Perdono i Sensi<privo_di_sensi>`.

***********************
Da Normali a non Letali
***********************
Si può impugnare un’arma da mischia che infligge danni letali per infliggere danni non letali, ma si subisce penalità –4 al tiro per colpire.

***********************
Da non Letali a Normali
***********************
Si può impugnare un’arma che infligge danni non letali, compreso un colpo senz’armi, per infliggere danni letali, ma si subisce penalità –4 al tiro per colpire.

.. _barcollante:
.. _privo_di_sensi:

Barcollante e Privo di Sensi
----------------------------
Quando i danni non letali eguagliano i propri punti ferita attualisi diventa :ref:`barcollanti<barcollante>` ed è possibile compiere solo un’:ref:`azione_standard` o un’:ref:`azione_di_movimento` per round, oltre a quelle più rapide. Questa condizione sparisce quando i punti ferita attuali superano di nuovo i danni non letali. 

Quando i danni non letali superano i propri punti ferita attuali si :ref:`Perdono i Sensi<privo_di_sensi>` e si è :ref:`Indifesi<indifeso>`. 

.. Note::
	Un incantatore privo di sensi conserva qualunque capacità di lanciare incantesimi che possedeva prima di svenire.

Se i danni non letali di una creatura sono uguali al suo totale di punti ferita massimi (non a quello corrente), tutto il danno non letale ulteriore è trattato come letale. Questo non si applica alle creature dotate di rigenerazione che semplicemente accumulano il danno non letale
ulteriore, aumentando il tempo in cui restano prive di sensi.

Guarire i Danni non Letali
---------------------------
I danni non letali si guariscono al ritmo di 1 * Liv Personaggio punti ferita all’ora.

Quando un incantesimo o una capacità magica cura punti ferita da danni, elimina anche un pari ammontare di danni non letali.


Movimento, Posizione e Distanza
================================
Le miniature sono in scala 30 mm (1:6), ovvero una miniatura di un uomo alto un metro e ottanta corrisponde a circa 30 mm. 

Un quadretto sulla griglia di battaglia di lato di 2,5 centimetri rappresenta un quadrato con lato di 1,5 metri (1:60).


.. contents:: In breve:
	:local:

--------------------------------
Movimento Tattico
--------------------------------
La velocità è determinata dalla razza e dall’armatura, come riportato in :ref:`Tabella<tabella_velocità_tattica>`. 

La velocità cui ci si muove senza indosso l’armatura si definisce **velocità base** sul terreno.

.. _tabella_velocità_tattica:

.. figure:: Immagini/VelocitaTattica.png
	:width: 700

Ingombro
---------
Se si è intralciati dal peso di una grossa quantità di attrezzatura, di un tesoro o di compagni caduti ci si muove :ref:`più lenti del normale<capacità_di_trasporto>`.

Movimento Impedito
-------------------
Terreni difficili, ostacoli o scarsa visibilità possono impedire il movimento.

Movimento in Combattimento
---------------------------
In generale, ci si può muovere alla propria velocità in un round e fare ancora qualcosa come compiere un’:ref:`azione_di_movimento` e una :ref:`Standard<azione_standard>`. Se ci si muove e basta, usando entrambe le azioni a disposizione in un round per muoversi alla propria velocità, è possibile muoversi al doppio della propria velocità.

Se si corre a perdifiato per un intero round, ci si può muovere al quadruplo della propria velocità, o al triplo se si indossa un’armatura pesante. 

Se si fa qualcosa che richiede un round completo è possibile fare soltanto un passo di 1,5 metri.

Bonus alla Velocità
---------------------
Un :ref:`barbaro` ha un bonus di +3 metri alla sua velocità, a meno che indossi un’armatura
pesante, così come i :ref:`Monaci<monaco>` esperti hanno dei bonus al movimento. Molti incantesimi e oggetti magici, inoltre, possono influire sulla velocità del personaggio. 

Prima di modificare la velocità per l’armatura o l’ingombro, vanno sempre applicati tutti i modificatori alla velocità del personaggio, ricordando che i bonus multipli dello stesso tipo, come i bonus di potenziamento, non sono cumulabili.

.. figure:: Immagini/MovimentoTattico.png
	:width: 700

--------------------------------
Misurare le Distanze
--------------------------------
Come regola generale, la distanza è misurata presumendo che 1 quadretto sia uguale a 1,5 metri.

Diagonali
--------------
Quando si misurano le distanze la prima diagonale conta come 1 quadretto, la seconda come 2, la terza come 1, la quarta come 2, e così via.

Non ci si può muovere in diagonale oltre un angolo, ma ci si può muovere in diagonale oltre una creatura, persino un avversario. 

.. Note::
	Ci si può muovere in diagonale per superare ostacoli altrimenti invalicabili, come le fosse.

Creature Vicine
---------------
Quando è importante stabilire quale sia il quadretto o la creatura più vicina a un certo luogo, nel caso in cui due quadretti o creature siano entrambi equidistanti si sceglie a caso il più vicino tirando un dado.

--------------------------------
Muoversi Attraverso un Quadretto
--------------------------------
Ci si può muovere attraverso un quadretto non occupato senza difficoltà in molte circostanze. Il terreno difficile e certi effetti magici possono ostacolare il movimento attraverso gli spazi aperti.

Amico
	A meno che non si sia in :ref:`azione_carica`, ci si può muovere attraverso un quadretto occupato da una creatura amica, ma quest’ultima non fornisce alcuna copertura.

Avversario
	In generale non ci si può muovere attraverso un quadretto occupato da un avversario, a meno che quest’ultimo non sia :ref:`indifeso<indifeso>`.

	.. Note::
		Alcune creature, specie quelle molto grosse, potrebbero essere d’ostacolo anche se sono :ref:`indifese<indifeso>`. In questi casi, ogni quadretto di movimento conta come 2 quadretti.

Terminare il proprio movimento
	Non si può portare a termine il proprio movimento nello stesso quadretto occupato da un’altra creatura, a meno che essa non sia :ref:`indifesa<indifeso>`.

Oltrepassare
	Nel corso del movimento o come parte di una :ref:`azione_carica`, si può :ref:`tentare<azione_oltrepassare>` di muoversi attraverso un quadretto occupato da un avversario.

Acrobazie
	Si può tentare di compiere delle :ref:`acrobazie<Acrobazia>` per muoversi attraverso un quadretto occupato da un avversario.

Creatura molto piccola
	Una creatura di taglia Piccolissima, Minuta o Minuscola può muoversi all’interno e attraverso un quadretto occupato. La creatura, tuttavia, muovendosi in questo modo provoca un :ref:`attacco_di_opportunità`.

Creatura di tre taglie più grande o più piccola
	Una qualunque creatura può muoversi attraverso un quadretto occupato da una creatura di tre categorie di taglia più grande della sua. Viceversa, una creatura grande può muoversi attraverso un quadretto occupato da una creatura di tre categorie di taglia più piccola della sua. 

	.. Warning::
		Le creature che si muovono attraverso quadretti occupati da altre creature provocano :ref:`attacchi di opportunità <attacco_di_opportunità>` da parte di queste creature.

Eccezioni specifiche
	Alcune creature infrangono queste regole. 

	Una creatura che occupa del tutto i suoi quadretti, non si può :ref:`azione_oltrepassare`, né con :ref:`Acrobazia` né con simili Capacità Speciali.

--------------------------------
Terreni e Ostacoli
--------------------------------
Dai rovi alle rocce, ci sono molteplici situazioni ambientali che possono influenzare il movimento.

Terreno difficile
	I terreni difficili, come pietrisco, dislivelli del suolo delle caverne, sottobosco fitto e simili, ostacolano il movimento. 

	Ogni quadretto di terreno difficile conta come 2 quadretti di movimento. Ogni movimento in diagonale conta come 3 quadretti. Non si può :ref:`azione_correre` o :ref:`caricare<azione_carica>` attraverso un terreno difficile.

	Se si occupano quadretti di terreno di vario tipo, ci si può muovere soltanto alla velocità massima consentita dal più difficile tra i terreni occupati.

	.. Note::
		Le creature volanti o incorporee non vengono ostacolate dai terreni difficili.

Ostacoli
	Gli ostacoli, allo stesso modo dei terreni difficili, intralciano il movimento. 

	Se un ostacolo intralcia il movimento, *senza però bloccarlo del tutto* (come un muro basso) ogni quadretto bloccato o ogni ostacolo tra due quadretti conta come 2 quadretti di movimento. Si deve pagare questo costo per superare la barriera, in aggiunta al costo per muoversi nel quadretto sull’altro lato. Se non si dispone di movimento a sufficienza per passare la barriera e muoversi dall’altra parte, non si può superare la barriera. Alcuni ostacoli si possono attraversare soltanto con una prova di Abilità.

	Se un ostacolo *blocca completamente* il movimento, non ci si può muovere attraverso di esso.

	.. Note::
		Le creature volanti o incorporee possono evitare la maggior parte degli ostacoli.

Stringersi
	In alcuni casi, bisogna infilarsi o sgusciare attraverso un’area che è più piccola dello spazio che si può occupare. Si può sgusciare attraverso o all’interno di uno spazio che deve essere largo almeno quanto la metà del proprio spazio normale. 

	Ogni movimento attraverso o all’interno di uno spazio ristretto conta come se fosse 2 quadretti, e mentre ci si ritrova in uno spazio ristretto si subisce penalità –4 al tiro per colpire e penalità –4 alla CA.

	Quando una creatura Grande (che di solito occupa fino a 4 quadretti) si stringe in uno spazio che è largo un quadretto, la miniatura della creatura occupa due quadretti, e si posiziona al centro della linea che separa i due quadretti. Il centro delle creature di taglia più grande viene individuato allo stesso modo nell’area ristretta.

	Una creatura in movimento può stringersi per superare un avversario, ma non può terminare il suo movimento in un quadretto occupato.

	Per stringersi in uno spazio inferiore alla metà della propria larghezza, bisogna usare :ref:`Artista_della_Fuga`. In tal caso, non si può attaccare, si subisce penalità –4 alla CA e si perde qualsiasi bonus di Destrezza alla Classe Armatura.

--------------------------------
Regole Speciali di Movimento
--------------------------------
Queste regole gestiscono situazioni speciali di movimento.

Movimento in uno spazio non consentito
	A volte, si completa il proprio movimento mentre ci si sposta all’interno di uno spazio in cui non è consentito fermarsi. 

	Quando accade, la miniatura va spostata nell’ultima posizione consentita occupata in precedenza, o in quella consentita più vicina, sempre che ne esista una.

Costo doppio
	In generale quando il movimento è intralciato in un qualche modo, costa il doppio. Ad esempio, tutti i quadretti attraverso un terreno difficile contano come 2 quadretti e ogni movimento in diagonale all’interno di questo tipo di terreno ne costa 3 (come due normali spostamenti in diagonale).

	Se il costo del movimento va raddoppiato due volte, allora ogni quadretto conta come 4 (o 6 nel caso di movimento in diagonale). Se va raddoppiato tre volte, ogni quadretto conta come 8 quadretti (o 12 nel caso di movimento in diagonale), e così via. 

	.. Note::
		Questa costituisce un’eccezione alla regola generale per cui raddoppiare due volte equivale a triplicare.

Movimento minimo
	A dispetto delle penalità al movimento, si può compiere un’:ref:`azione_di_round_completo` per muoversi di 1,5 metri (1 quadretto) in qualsiasi direzione, anche in diagonale. 

	Questa regola non permette però di muoversi attraverso terreni invalicabili o dove il movimento è altrimenti impossibile e provoca :ref:`attacchi di opportunità<attacco_di_opportunità>`, come di norma (questo infatti non vale come passo di 1,5 metri).

----------------------------------------------------------------
Creature Grandi e Piccole in Combattimento
----------------------------------------------------------------
La posizione delle creature di taglia inferiore alla Piccola o superiore alla Media è gestita da regole apposite.

.. figure:: Immagini/TagliaScalaCreature.png
	:width: 300
	:align: right

Creature Minuscole, Minute e Piccolissime
	Le creature molto piccole occupano meno di 1 quadretto di spazio. Quindi più creature di questo tipo possono sostare nello stesso quadretto. 

	Una creatura di taglia Minuscola in genere occupa un spazio di 75 centimetri di lato, per cui fino a quattro dello stesso tipo possono occupare un unico quadretto. Venticinque creature di taglia Minuta o cento creature di taglia Piccolissima possono occupare uno stesso quadretto. 

	Queste creature di solito hanno una portata naturale di 0 metri, ossia non possono raggiungere i quadretti adiacenti: devono entrare nel quadretto di un avversario per attaccare in mischia. Questo provoca un :ref:`attacco_di_opportunità` da parte dell’avversario. Il personaggio può attaccare nel suo quadretto, quando necessario, quindi egli può attaccare normalmente questo tipo di creature. 

	Poiché non hanno portata naturale, queste creature non minacciano i quadretti circostanti ed è quindi possibile oltrepassarle senza provocare :ref:`attacchi di opportunità<attacco_di_opportunità>`. Esse inoltre non possono attaccare ai fianchi i nemici.

Creature Grandi, Enormi, Mastodontiche e Colossali
	Le creature molto grandi occupano più di 1 quadretto e di solito hanno una portata naturale di 3 metri o superiore: possono raggiungere bersagli che si trovano oltre i quadretti adiacenti.

	A differenza di chi impugna un’arma con portata, una creatura con portata naturale superiore alla norma (1,5 metri o più) minaccia anche i quadretti intorno a sé e di solito ottiene un :ref:`attacco_di_opportunità` contro chi si avvicina, poiché attraversa e si muove entro la portata della creatura prima di poterla attaccare.

	.. Note::
		Se si fa un passo di 1,5 metri, non si provoca questo :ref:`Attacco di Opportunità<attacco_di_opportunità>`.

	Le creature di taglia Grande o superiore che usano armi con portata possono colpire fino al doppio della loro portata naturale, ma non entro la loro portata naturale o più da vicino.

Modificatori al Combattimento
==============================

.. contents:: In breve:
	:local:

.. figure:: Immagini/ModificatoriClasseArmatura.png
	:width: 300
	:align: right

	Modificatori al Tiro per Colpire

Certi fattori possono influenzare un :ref:`Tiro_Colpire`. Molte di queste circostanze garantiscono un bonus o una penalità ai tiri per colpire o alla :ref:`Classe_Armatura` del difensore.

.. _copertura:

--------------------------------
Copertura
--------------------------------
Ai fini di stabilire se un bersaglio è sotto copertura contro un attacco a distanza, si scelga un angolo del proprio quadretto: se una qualsiasi linea da questo angolo a uno qualsiasi del quadretto bersaglio attraversa un quadretto (o contorno) che blocca la linea di visuale o fornisce copertura, o un quadretto occupato da un’altra creatura, il bersaglio ottiene una **copertura** (bonus +4 alla CA).

Quando si porta un attacco in mischia contro un bersaglio adiacente, quest’ultimo è sotto copertura se una qualsiasi linea che collega il proprio quadretto con quello avversario attraversa un muro (compreso un muretto). 

.. Note::
	Quando si porta un attacco in mischia contro un bersaglio che non è adiacente (come con un’arma con portata), si utilizzino le regole per stabilire la copertura contro gli attacchi a distanza.


.. figure:: Immagini/Copertura.png
	:width: 700

Ostacoli bassi
	Un ostacolo basso (come un muro che arriva alla cintura) fornisce copertura, ma solo contro le creature che si trovano entro 9 metri (6 quadretti) da esso. L’attaccante può ignorare la copertura se è più vicino all’ostacolo del suo avversario.

Attacchi di Opportunità
	Non si può portare un :ref:`attacco_di_opportunità` contro un avversario che è sotto copertura nei propri confronti.

Tiri Salvezza su Riflessi
	La copertura conferisce bonus +2 ai TS su Riflessi contro attacchi che hanno origine o esplodono in un punto dall’altro lato della propria copertura. 

	.. Warning::
		Gli effetti a propagazione possono aggirare gli angoli e quindi negare questo bonus di copertura.

Prove di Furtività
	Si può usare la copertura per effettuare una prova di :ref:`Furtività`. 

	Altrimenti occorre :ref:`Occultamento`.

Copertura Leggera
	Le creature, persino i propri nemici, possono fornire copertura contro gli attacchi a distanza, conferendo bonus +4 alla CA. 

	In ogni caso, la copertura leggera non conferisce bonus ai TS su Riflessi né consente di effettuare prove di :ref:`Furtività`.

.. figure:: Immagini/ModificatoriTiroColpire.png
	:width: 300
	:align: right

	Modificatori alla Classe Armatura

Creature Grandi
	Qualunque creatura il cui spazio è maggiore di 1,5 metri (1 quadretto) ottiene copertura dagli **attacchi in mischia** in maniera leggermente diversa  rispetto a quelle di taglia più piccola. 

	Una creatura di questo tipo può scegliere qualsiasi quadretto occupato per stabilire se un avversario è sotto copertura rispetto agli attacchi della creatura. Allo stesso modo, quando si attacca in mischia una creatura di questo tipo si può scegliere uno qualunque dei quadretti occupati dalla creatura per stabilire se essa sia sotto copertura rispetto alla propria posizione.

Copertura parziale
	Se una creatura ha copertura, ma più della metà di essa è visibile, il bonus si riduce a +2 alla CA e +1 ai TS su Riflessi. 

	Questo tipo di copertura è soggetta alla discrezione del GM.

Copertura totale
	Se non si ha alcuna linea di visuale verso il proprio bersaglio (cioè non si può tracciare una linea dal proprio quadretto a quello in cui si trova il bersaglio senza attraversare una barriera solida), quest’ultimo beneficia di una copertura totale. 

	Non si può portare alcun attacco contro un bersaglio sotto copertura totale.

Gradi variabili di copertura
	La copertura, in alcuni casi, può conferire un bonus superiore alla CA e ai TS su Riflessi, come ad esempio attaccare un personaggio nascosto dietro un angolo o una feritoia. 

	In queste circostanze i normali bonus di copertura alla CA e ai TS sui Riflessi si possono raddoppiare (a +8 e a +4, rispettivamente) e la creatura ottiene a tutti gli effetti :ref:`eludere_migliorato` contro tutti gli attacchi cui si applica il bonus. La copertura migliorata conferisce inoltre bonus +10 alle prove di :ref:`Furtività`.

.. _occultamento:

--------------------------------
Occultamento
--------------------------------

Ai fini di stabilire se un bersaglio è occultato contro un **attacco a distanza**, si scelga un angolo del proprio quadretto. Se un fascio di rette, con l’origine in questo angolo, che congiunge un qualunque angolo del quadretto avversario, attraversa un quadretto o un contorno che conferisce occultamento, allora il bersaglio è occultato.

Quando si porta un **attacco in mischia** contro un bersaglio adiacente, quest’ultimo è occultato se tutto il suo spazio si trova entro un effetto che conferisce occultamento. Quando
si porta un attacco in mischia contro un bersaglio che non è adiacente si utilizzino le regole per stabilire l’occultamento contro gli attacchi a distanza.

.. Note:: 
	Alcuni effetti magici forniscono occultamento contro tutti gli attacchi, quali che siano le eventuali condizioni di occultamento.

Probabilità di essere Mancati
	L’occultamento fornisce alla creatura contro cui è stato messo a segno un attacco una probabilità del 20% che l’attaccante la manchi. 

	Il difensore può effettuare un tiro percentuale sulla probabilità di essere mancati ai fini di sfuggire al colpo. Condizioni multiple di occultamento non si cumulano.

Prove di Furtività
	Si può usare l’occultamento per effettuare una prova di :ref:`Furtività` (di solito occorre una copertura).

.. _occultamento_totale:

Occultamento Totale
	Se non si ha alcuna linea di visuale contro il bersaglio, quest’ultimo beneficia di occultamento totale: non si può subire :ref:`attacchi di opportunità <attacco_di_opportunità>`, ma se si può essere attaccati nel quadretto dove l'avversario pensa ci si trovi.

	Un attacco messo a segno in un quadretto occupato da un nemico sotto occultamento totale ha una probabilità di fallimento del 50% (anziché 20% di un occultato).

Ignorare Occultamento
	L’occultamento non ha sempre effetto. 

	Un’area in penombra o buia non conferiscono alcun occultamento contro un avversario con :ref:`Scurovisione<scurovisione>` così come i personaggi con :ref:`Visione Crepuscolare<visione_crepuscolare>` vedono nitido a una distanza maggiore degli altri personaggi. 

	Anche se l’invisibilità conferisce occultamento totale, gli avversari che hanno l’uso della vista possono lo stesso effettuare delle prove di :ref:`Percezione<percezione>` per stabilire la posizione dell’avversario. Quest'ultimo ottiene bonus +20 alle prove di :ref:`Furtività` se si muove o +40 se rimane immobile.

	.. Note:
		Anche se gli avversari non sono in grado di vedere il personaggio invisibile, potrebbero comunque stabilire la sua posizione da altri indizi esterni.

Gradi variabili di Occultamento
	Alcune situazioni forniscono una copertura maggiore o minore del normale occultamento, modificando di conseguenza la percentuale di essere mancati.

--------------------------------
Attaccare ai Fianchi
--------------------------------
Se si compie un **Attacco in Mischia** mentre l'avversario è minacciato su un lato o un angolo opposto si ottiene bonus di fiancheggiamento +2.

.. figure:: Immagini/AttaccareAiFianchi.png
	:width: 700

In caso di dubbio, si tracci una retta immaginaria per unire i centri (dei quadretti) degli attaccanti. Se la retta interseca i contorni opposti dello spazio occupato dall’avversario, allora questo è attaccato ai fianchi.

Un attaccante che occupa più di 1 quadretto, ottiene un bonus di fiancheggiamento se almeno uno dei suoi quadretti è valido ai fini di un attacco ai fianchi.

.. Note::
	Le creature con portata di 0 metri non possono attaccare ai fianchi.

--------------------------------
Difensori Indifesi
--------------------------------
**Indifeso** è chiunque sia legato, addormentato, paralizzato, privo di sensi o altrimenti alla mercé del personaggio.

Attacco Normale
	Un personaggio indifeso subisce penalità –4 alla CA contro gli attacchi in mischia e la sua Destrezza va considerata 0, subendo penalità –5 contro tutti gli attacchi (per un totale di –5 per quelli a distanza e –9 per quelli in mischia. 

	Un personaggio indifeso è anche :ref:`impreparato`.

Colpo di Grazia
	Con un':ref:`Azione di Round Completo<azione_di_round_completo>`, si può usare un’arma da mischia per dare un colpo di grazia a un nemico indifeso, così come un arco o una balestra, purché ci si trovi adiacenti al bersaglio.

	Si colpisce automaticamente e si mette a segno un colpo critico e, se il difensore sopravvive ai danni, deve superare un TS su Tempra con **CD 10 + danni inflitti** o muore. Un ladro che dà un colpo di grazia, aggiunge anche i suoi danni extra per l’:ref:`Attacco Furtivo<attacco_furtivo_ladro>`.

	.. Warning:: 
		Chi dà un colpo di grazia incorre negli :ref:`Attacchi di Opportunità<attacco_di_opportunità>` di tutti gli avversari che lo minacciano.

	

	Non è possibile dare un colpo di grazia a una creatura immune ai colpi critici, ma si può dare un colpo di grazia a una creatura sotto occultamento totale. Per farlo occorrono due :ref:`Azioni di Round Completo<azione_di_round_completo>` consecutive (la prima per “trovare” la creatura dopo aver individuato il quadretto in cui è situata, e la seconda per dare il colpo di grazia).

Attacchi Speciali
=======================
Questa sezione descrive tutte le varie manovre standard che è possibile effettuare durante il combattimento diverse da attaccare, lanciare incantesimi o usare capacità di classe.

Alcuni di questi attacchi speciali possono essere compiuti come parte di un’altra azione (come un attacco) o come un :ref:`Attacco di Opportunità<attacco_di_opportunità>`.

.. _azione_aiutare:

--------------------------------
Aiutare un Altro
--------------------------------
Si può aiutare un amico ad attaccare o a difendersi negli scontri in mischia, distraendo o interferendo con un avversario. L'azione è possibile solo se si può portare un **attacco in mischia** contro un avversario che ha già ingaggiato battaglia con il proprio alleato, e costa un':ref:`azione_standard`.

Si effettua un tiro per colpire contro CA 10. Se va a segno, l’amico ottiene bonus di circostanza +2 per il prossimo attacco, in attacco o in difesa a propria scelta, purché sia effettuato prima del prossimo turno del personaggio. 

Più personaggi possono contribuire ad aiutare: bonus di questo tipo sono cumulabili.

.. Important::
	È anche possibile usare questa azione per aiutare un amico in svariati modi, come quando si trova sotto l’influenza di un incantesimo, o per aiutare qualcun altro a effettuare una prova di abilità.

.. _azione_carica:

--------------------------------
Carica
--------------------------------
Caricare è un’:ref:`Azione di Round Completo<azione_di_round_completo>` speciale che permette di muoversi fino al **doppio della propria velocità e attaccare** con la stessa azione. Tuttavia, si è limitati nel modo in cui ci si può muovere.

Se all’inizio della carica non si ha nella propria linea di visuale l’avversario, questo non può essere caricato.

Movimento
	Occorre muoversi prima dell’attacco, di almeno 3 metri (2 quadretti) e fino al doppio della propria velocità contro l’avversario caricato. 	Se ci si muove di una distanza pari alla propria velocità o inferiore e il proprio BAB è almeno +1, si può estrarre un’arma durante la carica. Si deve avanzare verso l’avversario su un terreno sgombro, e nulla deve intralciare il movimento (come un terreno difficile od ostacoli).

	Innanzitutto, occorre muoversi fino al primo spazio utile da cui si può attaccare l’avversario e se questo è occupato o altrimenti bloccato, non si può caricare. 

	In secondo luogo, se il percorso interseca un quadretto che blocca, rallenta il movimento, contiene una creatura o un alleato, non si può caricare. 

	.. Note::
		Le creature indifese non ostacolano una carica. 

	Non si può fare un passo da 1,5 metri nello stesso round di una carica e se si è in grado di compiere soltanto un’:ref:`azione_standard` o un’:ref:`azione_di_movimento` nel proprio turno, si può caricare muovendosi fino alla propria velocità (anziché il doppio) e non si può estrarre un’arma, a meno che si abbia :ref:`Estrazione Rapida<tal_estrazione_rapida>`. 

	.. Warning::
		Non si può usare questa opzione, a meno che non si sia obbligati a compiere soltanto un’:ref:`azione_standard` o un’:ref:`azione_di_movimento` al proprio turno.

Attacco
	Al termine del proprio movimento, si può effettuare un **unico attacco di mischia**, anche se si possiedono attacchi extra, grazie al BAB o l’uso di più armi.

	Chi sfrutta la spinta della carica a suo favore, ottiene +2 al tiro per colpire e –2 alla CA fino all’inizio del suo turno successivo, così come +2 alle :ref:`Manovre in Combattimento<azione_manovra_in_combattimento>` per :ref:`Spingere<azione_spingere>`.

Lance da Cavaliere in Carica
	Una lancia da cavaliere infligge **danni raddoppiati** se impugnata da un personaggio in sella durante una carica.

Armi Preparate contro una Carica
	Lance, tridenti e altre armi perforanti infliggono danni raddoppiati quando preparate e usate contro un personaggio che sta effettuando una carica.

.. _combattere_con_due_armi:

--------------------------------
Combattere con Due Armi
--------------------------------
Se si impugna un’arma nella mano secondaria, si può ottenere con quell’arma un **attacco extra** per round, tuttavia si subisce –6 all’attacco normale o agli
attacchi con la mano primaria e –10 agli attacchi con la mano secondaria. 

.. _tabella_penalità_due_armi:

.. figure:: Immagini/PenalitaCombattereDueArmi.png
	:width: 300
	:align: right

Queste penalità possono essere ridotte (riassunto in :ref:`Tabella<tabella_penalità_due_armi>`:
	
	* Se l’arma secondaria è leggera, entrambe le penalità diminuiscono di 2. 

	.. Note:: 
		Un colpo senz’armi è sempre considerato alla stregua di un’arma leggera. 

	* :ref:`Combattere con Due Armi<tal_combattere_con_due_armi>` riduce di 2 la penalità alla mano primaria e di 6 la penalità alla mano secondaria.


Armi Doppie
	Si possono usare armi doppie per effettuare un attacco extra con la seconda estremità dell’arma, come se si combattesse con due armi. Le penalità si applicano come se la seconda estremità dell’arma fosse un’arma leggera.

Armi da Lancio
	Quando si usano armi da lancio in entrambe le mani, valgono le stesse regole. Si trattino i dardi o gli shuriken come armi leggere, mentre le bolas, il giavellotto, la rete o la corda equivalgono ad armi a una mano.

--------------------------------
Combattere in Sella
--------------------------------
Queste regole riguardano il combattere in sella a cavalli in combattimento, ma possono anche essere applicate a cavalcature insolite, come grifoni e draghi.

Cavalli in Combattimento
	Cavalli da guerra, pony e cani da galoppo sono eccellenti destrieri, mentre quelle cavalcature che non possiedono un addestramento militare (vedi :ref:`Addestrare Animali<addestrare_animali>`) temono la battaglia. 

	Finché non si smonta da cavallo, occorre effettuare una prova di :ref:`Cavalcare<cavalcare>` con CD 20 a ogni round come :ref:`azione_di_movimento` per controllare cavalcature di questo tipo. Se si supera la prova, si può compiere un’:ref:`azione_standard` dopo quella di :ref:`Movimento<azione_di_movimento>`. In caso contrario, l’azione diventa :ref:`di Round Completo<azione_di_round_completo>` e non si può fare nient’altro fino al turno successivo.

	La cavalcatura agisce al conteggio di iniziativa del cavaliere, come questi la controlla. Il cavaliere si muove alla velocità della cavalcatura, ma usa la sua azione per muoversi.

	Un cavallo (non un pony) è una creatura di taglia Grande e quindi occupa uno spazio di lato 3 metri (2 quadretti). Per semplicità, si ipotizzi che nel corso della battaglia il cavaliere e la cavalcatura occupino lo stesso spazio.

.. figure:: Immagini/Combattimento4.png
	:width: 350
	:align: left

Combattere
	Con una prova di :ref:`Cavalcare` con CD 5, si può condurre la cavalcatura con le ginocchia e usare entrambe le mani per attaccare o difendersi con un’:ref:`azione_gratuita`. Il combattimento in questi casi ha i seguenti modificatori:

	* Quando si attacca una creatura a piedi di taglia Media o più piccola, si ottiene +1 agli attacchi in mischia, poiché ci si trova in posizione sopraelevata.

	* Se la cavalcatura si muove di più di 1,5 metri, si può compiere un solo attacco in mischia. In pratica prima di attaccare bisogna aspettare che la cavalcatura raggiunga  l’avversario, quindi non si potrà compiere un :ref:`attacco_completo`. 

	* Mentre la cavalcatura è in movimento, si può anche compiere un :ref:`attacco_completo` con armi a distanza.

	* Chi è in sella non subisce alcuna penalità agli attacchi in mischia, persino a piena velocità della cavalcatura.

	* Anche il cavaliere subisce la penalità alla CA derivante da una :ref:`Carica<azione_carica>`. Un attacco al termine della :ref:`Carica<azione_carica>`, ottiene il bonus conferito dalla carica e infligge danni raddoppiati con una lancia da cavaliere.

	* Si possono usare armi a distanza, mentre la cavalcatura compie un **movimento doppio**, ma si subisce –4 al tiro per colpire, mentre in corsa (velocità quadrupla) la penalità sale a -8. In entrambi i casi, si effettua il tiro per colpire quando la cavalcatura ha completato metà del suo movimento. 

	* Si possono compiere normalmente :ref:`Azioni di Movimento<azione_movimento>`.

Lanciare Incantesimi
	Si può lanciare un incantesimo normalmente se la cavalcatura si muove fino alla propria velocità, prima o dopo il lancio. 

	Se la cavalcatura si muove prima e dopo il lancio, chi lancia l'incantesimo deve superare una **prova di Concentrazione** per il movimento vigoroso **CD 10 + Liv Incantesimo** o perde l’incantesimo. Se la cavalcatura va di corsa (velocità quadrupla), si può lanciare un incantesimo, quando la cavalcatura si è mossa fino al doppio della sua velocità, ma la prova diventa più difficile a causa del movimento violento con **CD 15 + il Liv Incantesimo**.

Cavalcatura Caduta in Battaglia
	Se la cavalcatura cade a terra, occorre superare una prova di :ref:`cavalcare` con **CD 15** per cadere in piedi senza subire danni. Altrimenti, si subiscono 1d6 danni.

Cadere dalla cavalcatura
	Se si viene colpiti e si perdono i sensi, c’è una probabilità del 50% di rimanere in sella (75% su una sella militare), altrimenti si cade a terra e si subiscono 1d6 danni. 

	.. Note:: 
		Una cavalcatura senza cavaliere non combatte.

.. _fintare:
.. _azione_fintare:

--------------------------------
Fintare
--------------------------------
Fintare è un’:ref:`azione_standard` e bisogna superare una prova di :ref:`Raggirare<raggirare>` con **CD = 10 + BAB avversario + mod Sag avversario**, a meno che il bonus :ref:`Intuizione<intuizione>` sia maggiore, in tal caso **CD = 10 + Bonus Intuizione**. 

Se la prova riesce, il prossimo attacco in mischia contro il bersaglio non gli permette di usare il suo bonus Destrezza alla CA. Questo attacco deve essere effettuato durante o prima il proprio turno successivo.

.. Note::
	Contro una creatura non umanoide, si subisce penalità –4 a fintare; contro una creatura di Intelligenza animale (1 o 2), si subisce penalità –8; contro una creatura priva di Intelligenza, non si può fintare.

Fintare non provoca alcun :ref:`Attacco di Opportunità<attacco_di_opportunità>`.

Fintare come Azione di Movimento
	Con :ref:`Fintare Migliorato<tal_fintare_migliorato>`, si può tentare una finta come :ref:`azione_movimento`.

.. _armi_a_spargimento:

--------------------------------
Lanciare Armi a Spargimento
--------------------------------
Un’arma a spargimento è un’arma a distanza che, rompendosi all’impatto, spruzza o disperde il suo contenuto sul bersaglio e sulle creature o gli oggetti vicini. Per farlo, occorre compiere un :ref:`Attacco di Contatto a Distanza<attacco_contatto>` contro il bersaglio. Le armi da lancio non richiedono alcuna competenza, quindi non si subisce penalità –4 per non essere competenti nel lancio di esse. 

Un colpo messo a segno infligge danni diretti al bersaglio, e spruzza danni a tutte le creature che si trovano entro un raggio di 1,5 metri dal bersaglio. Le armi a spargimento
non possono infliggere danni da precisione (per esempio da un :ref:`Attacco Furtivo<attacco_furtivo_ladro>`).

Si può anche mirare a una particolare intersezione della griglia, trattando la situazione come un attacco a distanza contro CA 5. Questo non è possibile nel caso l’intersezione sia occupata da una creatura di taglia Grande o maggiore: si deve mirare alla creatura. Ad ogni modo, se si mira all’intersezione della griglia, tutte le creature nei quadretti adiacenti subiscono danni da spruzzo, ma nessuna creatura subisce i danni diretti. 

Se si manca il bersaglio, occorre tirare 1d8 che determina la traiettoria errata del lancio: **1** significa che il colpo è tornato indietro, mentre da **2 a 8** rappresentano le creature in senso orario intorno all’intersezione della griglia o alla creatura bersaglio. Quindi, nella direzione appena trovata, si conti un numero di quadretti pari all’incremento di gittata del lancio. Una volta stabilito il punto di impatto dell’arma, essa infligge danni da spargimento contro tutte le creature che si trovano nei quadretti adiacenti.

.. _azione_manovra_in_combattimento:

--------------------------------
Manovre in Combattimento
--------------------------------
Durante il combattimento, si possono tentare alcune manovre che sono in grado di ostacolare o perfino bloccare l’avversario, come :ref:`azione_disarmare`, :ref:`azione_lottare`, :ref:`azione_oltrepassare`, :ref:`azione_sbilanciare`, :ref:`azione_spezzare` e :ref:`azione_spingere`. Anche se hanno risultati differenti, queste manovre usano le stesse meccaniche per determinarne il successo.

.. _bmc:

Bonus da Manovra in Combattimento (BMC)
	Ogni personaggio e creatura hanno un Bonus da Manovra in Combattimento, o BMC, che rappresenta l’abilità nel compiere manovre in combattimento. 

	.. math::
		BMC = BAB + Mod For + Mod Taglia Speciale

	I modificatori di taglia speciali per il BMC di una creatura sono i seguenti: 

		* Piccolissima –8
		* Minuta –4
		* Minuscola –2
		* Piccola –1
		* Media +0
		* Grande +1
		* Enorme +2
		* Mastodontica +4
		* Colossale +8.

	.. Note::
		Creature di taglia Minuscola o più piccola usano il modificatore di Destrezza anziché di Forza per determinare il BMC. 

	Certi talenti o capacità conferiscono un bonus al proprio BMC quando si compiono manovre specifiche.

Compiere una Manovra in Combattimento
	Quando si compie una manovra in combattimento, bisogna usare un’azione appropriata alla manovra che si intende compiere. 
	Sebbene molte possano essere compiute come parte di un attacco, un :ref:`attacco_completo`, o :ref:`di opportunità<attacco_di_opportunità>`, altre richiedono un’azione specifica. 

	A meno che non sia indicato diversamente, compiere una manovra in combattimento provoca un :ref:`Attacco di Opportunità<attacco_di_opportunità>` da parte del bersaglio e, se si viene colpiti, si subiscono i danni normalmente e si applica quel danno come penalità al tiro per compiere la manovra. 

	Se il bersaglio è immobilizzato, privo di sensi o in altro modo incapacitato, la manovra riesce automaticamente, come si fosse ottenuto un 20 naturale, mentre se il bersaglio è stordito, si riceve +4 al BMC contro di lui.

	Quando si tenta di compiere una manovra in combattimento, si effettua un attacco e si aggiunge il proprio BMC al posto del normale bonus di attacco. Si aggiunge qualsiasi bonus che si abbia attualmente per incantesimi, talenti o altri effetti, che devono essere applicabili all’arma o all’attacco usato per compiere la manovra. La CD di questa manovra è la Difesa da Manovra in Combattimento (o DMC) del bersaglio. 

	Siccome le manovre in combattimento sono attacchi, bisogna tirare per l’occultamento e si subiscono tutte le penalità che si applicherebbero normalmente ad un attacco.

.. _dmc:

.. figure:: Immagini/Combattimento5.png
	:width: 700

Difesa da Manovra in Combattimento
	Ogni personaggio o creatura ha una Difesa da Manovra in Combattimento (o DMC) che rappresenta la sua capacità di resistere alle manovre in combattimento. 

	.. math::
		DMC = 10 + BAB + Mod For + Mod Des + Mod Taglia Speciale

	I modificatori di taglia speciali per il DMC di una creatura sono i seguenti: 

		* Piccolissima –8
		* Minuta –4
		* Minuscola –2
		* Piccola –1
		* Media +0
		* Grande +1
		* Enorme +2
		* Mastodontica +4
		* Colossale +8.
	
	Certi talenti o capacità conferiscono un bonus alla propria DMC quando si resiste a manovre specifiche. Una creatura può aggiungere inoltre qualsiasi bonus di circostanza, di deviazione, di schivare, di morale, cognitivi, profano e sacro alla CA, alla propria DMC. Qualsiasi penalità alla CA della creatura si applica anche alla sua DMC. 

	Una creatura :ref:`Impreparata<impreparato>` non aggiunge il proprio bonus di Destrezza alla sua DMC.

Determinare il Successo
	Se il proprio tiro d’attacco è uguale o superiore alla DMC del bersaglio, la manovra riesce e produce gli effetti indicati. 

	Alcune manovre, come :ref:`azione_spingere`, hanno vari livelli di successo in base a quanto l’attacco eccede la DMC del bersaglio. Un 20 naturale è sempre un successo (eccetto quando si tenta di fuggire da legami), mentre un 1 naturale è sempre un fallimento.

.. _azione_disarmare:

Disarmare
--------------------------------
Si può tentare di disarmare un avversario, al posto di un attacco in mischia. Senza :ref:`Disarmare Migliorato<tal_disarmare_migliorato>` o una capacità simile, tentare di disarmare un avversario provoca un :ref:`Attacco di Opportunità<attacco_di_opportunità>` da parte dell’avversario che si tenta di disarmare. 

Se l’attacco riesce, il bersaglio lascia cadere un oggetto che sta trasportando a scelta dell’attaccante, anche se l’oggetto è impugnato con due mani. Se l’attacco eccede la DMC del bersaglio di 10 o più, quest’ultimo lascia cadere al massimo due oggetti che sta trasportando nelle mani, anche se ne ha più di due (di mani). Se l’attacco fallisce di 10 o più, l’attaccante lascia cadere l’arma che stava usando nel tentativo di disarmare. 

Se si riesce a disarmare un avversario senza usare un’arma, si può raccogliere automaticamente l’oggetto che è caduto.

.. Warning::
	Tentare di disarmare un avversario mentre si è disarmati, comporta penalità –4 all’attacco.

.. _azione_lottare:

Lottare
--------------------------------
Come :ref:`azione_standard`, si può tentare di afferrare un avversario, ostacolandone le opzioni in combattimento. Senza :ref:`Lottare Migliorato<tal_lottare_migliorato>`, la capacità afferrare o una capacità simile, tentare di lottare con un avversario provoca un :ref:`Attacco di Opportunità<attacco_di_opportunità>` da parte del bersaglio della manovra.

Se il tiro ha successo, entrambi i personaggi coinvolti ricevono la condizione :ref:`in lotta<in_lotta>`. Se si afferra una creatura non adiacente, si deve muovere quella creatura ad uno spazio libero adiacente e se non c’è spazio disponibile, il tentativo di afferrare fallisce.

Sebbene entrambe le creature abbiano la condizione in lotta, chi ha dato inizio alla lotta può liberarsi dalla stessa con un’:ref:`azione_gratuita`, rimuovendo la condizione da entrambi. Se chi ha iniziato la lotta non si libera dalla lotta, deve continuare ad effettuare prove ad ogni round, come :ref:`azione_standard`, per mantenere la presa e se il bersaglio non si libera dalla lotta nel primo round, ottiene bonus di circostanza +5 alle prove di lottare effettuate contro lo stesso bersaglio nei round successivi.

Una volta in lotta con un avversario, una prova riuscita di lottare permette di continuare a rimanere in lotta con esso, e di compiere una delle azioni seguenti (come parte
dell’azione spesa per mantenere la lotta).

.. Warning::
	Le creature umanoidi senza due mani libere che tentano di afferrare un avversario subiscono penalità –4 ai tiri di manovra.

Muoversi
	Ci si può muovere alla metà della propria velocità trascinando con sè anche il bersaglio. Alla fine del movimento si può piazzare il bersaglio in un quadretto qualsiasi adiacente a sè. 

	Se si cerca di trascinare l’avversario su un luogo pericoloso, come in un muro di fuoco o su una fossa, il bersaglio riceve un tentativo gratuito di liberarsi dalla lotta con bonus +4.

Danno
	Si può infliggere danno all’avversario pari al proprio colpo senz’armi, un attacco naturale, un attacco effettuato con le chiodature dell’armatura, un’arma leggera o ad una mano. 

	Questo danno può essere letale o non letale.

Immobilizzare
	Si può dare all’avversario la condizione :ref:`Immobilizzato<immobilizzato>`. 

	L’aggressore mantiene solo la condizione in lotta, ma perde il bonus di Destrezza alla CA.

Legare
	Se si è riusciti ad immobilizzare l’avversario, a bloccarlo in altro modo o a fargli perdere i sensi, è possibile usare una corda per legarlo. Ciò funziona come un effetto di immobilizzare, ma la **CD per liberarsi** dalle corde è pari a **20 + proprio BMC** (anziché DMC). Le corde non hanno bisogno di effettuare una prova ogni round per mantenere il bersaglio immobilizzato. 

	Se si sta lottando con il bersaglio, si può cercare di legarlo con una corda, ma per farlo occorre una prova di manovra in combattimento con penalità –10. Se la CD per liberarsi è più alta di 20 + BMC del bersaglio, quest’ultimo non può liberarsi dalle corde, anche se ottiene un 20 naturale alla prova.

Se si è Afferrati
	Se si è afferrati, si può tentare di liberarsi dalla presa come :ref:`azione_standard` effettuando o una prova di manovra in combattimento con **CD = DMC avversario**, senza provocare  un :ref:`Attacco di Opportunità<attacco_di_opportunità>` o una prova di :ref:`Artista della Fuga<artista_della_fuga>` con la stessa CD. 

	Se si ha successo, ci si libera dalla lotta e si può agire normalmente. Alternativamente, si può afferrare l’avversario a propria volta. 

	.. Hint::
		Invece che tentare di liberarsi o invertire la lotta, è possibile compiere qualsiasi azione che richieda una mano sola per essere compiuta, come lanciare un incantesimo o effettuare un attacco con un’arma leggera o ad una mano contro qualsiasi creatura a portata, compreso l’aggressore (vedi :ref:`in Lotta<in_lotta>`. 

	Se si è :ref:`immobilizzati<immobilizzato>`, le azioni sono molto limitate.

Più Creature in Lotta
	Più creature possono tentare di afferrare un bersaglio. 

	La creatura che ha per prima iniziato la lotta è l’unica che effettua la prova, con bonus +2 per ogni creatura che partecipa alla lotta usando :ref:`azione_aiutare`. Più creature possono inoltre assisterne un’altra in lotta che cerca di liberarsi da una presa, e quest’ultima riceve bonus +2 alla prova di manovra in combattimento per ogni creatura che l’aiuta a liberarsi usando :ref:`azione_aiutare`.

.. _azione_oltrepassare:

Oltrepassare
--------------------------------
Come :ref:`azione_standard`, intrapresa durante il proprio movimento o come parte di una :ref:`azione_carica`, si può tentare di oltrepassare un bersaglio muovendosi attraverso il suo quadretto, a patto che questo non sia più grande di una categoria di taglia rispetto alla propria. Senza :ref:`Oltrepassare Migliorato<tal_oltrepassare_migliorato>`, o una capacità simile, iniziare a oltrepassare provoca un :ref:`Attacco di Opportunità<attacco_di_opportunità>` da parte del bersaglio.

Se il tentativo fallisce, ci si ferma direttamente davanti all’avversario, o comunque nel più vicino spazio libero di fronte alla creatura.
Quando si tenta di oltrepassare un bersaglio, questi può scegliere di evitarlo, permettendo il passaggio attraverso il quadretto senza necessità di un attacco. Se il bersaglio non
evita il tentativo, bisogna effettuare una prova di manovra in combattimento come di norma. 

Se la manovra ha successo, ci si muove attraverso lo spazio del bersaglio. Se l’attacco eccede la DMC dell’avversario di 5 o più, ci si muove attraverso lo spazio del bersaglio e quest’ultimo cade a terra :ref:`prono<prono>`. 

.. Warning::
	Se il bersaglio ha più di due gambe, si aggiunge +2 alla CD del tiro di manovra per ogni gamba addizionale.

.. _azione_sbilanciare:
.. _sbilanciare:

Sbilanciare
--------------------------------
Si può tentare di sbilanciare un avversario, al posto di un attacco in mischia, a patto che non sia più grande di una categoria di taglia della propria. Senza :ref:`Sbilanciare Migliorato<tal_sbilanciare_migliorato>`, o una capacità simile, iniziare a sbilanciare provoca un :ref:`Attacco di Opportunità<attacco_di_opportunità>` da parte del bersaglio.

Se l’attacco supera la DMC del bersaglio, questo viene fatto cadere a terra :ref:`prono<prono>`, ma se fallisce di 10 o più, si cade a terra :ref:`proni<prono>` al posto suo. 

.. Warning::
	Se il bersaglio ha più di due gambe, si aggiunge +2 alla CD del tiro di manovra per ogni gamba addizionale. 

	Alcune creature—come le melme, quelle senza gambe e quelle volanti—non possono essere sbilanciate.

.. _azione_spezzare:

Spezzare
--------------------------------
Si può tentare di spezzare un oggetto posseduto o impugnato da un avversario come parte di un’azione di attacco anziché attaccarlo in mischia. Senza :ref:`Spezzare Migliorato<tal_spezzare_migliorato>` , o una capacità simile, cercare di spezzare un oggetto provoca un :ref:`Attacco di Opportunità<attacco_di_opportunità>` da parte del bersaglio.

Se l’attacco riesce, si infligge danno all’oggetto normalmente e il danno che eccede la durezza dell’oggetto viene sottratto dai suoi punti ferita. Se ad un oggetto resta la metà o meno dei suoi punti, riceve la condizione :ref:`Rotto<rotto>`. Se il danno inflitto riduce i punti ferita dell’oggetto a meno di 0, si può decidere di distruggerlo; in caso contrario, l’oggetto viene lasciato con 1 solo punto ferita e la condizione :ref:`Rotto<rotto>`.

.. _azione_spingere:

Spingere
--------------------------------
Si può spingere con un’:ref:`azione_standard` o come parte di una :ref:`azione_carica`, anziché attaccare, posto che l'avversario abbia una differenza di taglia di al più 1. Senza :ref:`Spingere Migliorato<tal_spingere_migliorato>`, o una capacità simile, si incorre in un :ref:`Attacco di Opportunità<attacco_di_opportunità>` dell’avversario.

Se l’attacco riesce, il bersaglio viene respinto indietro di 1,5 metri e ogni 5 punti extra con cui l’attacco supera la DMC dell’avversario, lo si può respingere indietro di altri 1,5 metri. Ci si può muovere insieme al bersaglio se si vuole, ma occorre avere la possibilità di muoversi per farlo. 

Se l’attacco fallisce, il movimento dell’attaccante finisce di fronte al bersaglio.

Un nemico che è stato spinto non provoca un :ref:`Attacco di Opportunità<attacco_di_opportunità>` per il movimento a meno che si possieda :ref:`Spingere Superiore<tal_spingere_superiore>` e non è possibile spingere una creatura in un quadretto che è occupato da un oggetto solido o un ostacolo. 

Se è presente un’altra creatura lungo la linea in cui si spinge, occorre effettuare una prova di manovra in combattimento per spingere anche questa, subendo penalità –4 per ogni creatura che è spinta oltre alla prima. Se si ha successo, si può continuare a spingere le creature per una distanza uguale al risultato minore. 

Per esempio: se un guerriero spinge un goblin per un totale di 4,5 metri, ma c’è un altro goblin 1,5 metri dietro il primo, si deve effettuare un’altra prova di manovra in combattimento contro il secondo goblin dopo aver spinto per i primi 1,5 metri. Se la prova rivela che si può spingere il secondo goblin per un totale di 6 metri, si può continuare a spingere entrambi i goblin per altri 3 metri, poiché il primo goblin sarà mosso per un totale di 4,5 metri.

.. _azioni_speciali_iniziativa:

Azioni Speciali di Iniziativa
==============================

.. contents:: In breve:
	:local:

I metodi qui presentati permettono di cambiare il proprio ordine d’azione di iniziativa in battaglia.


.. figure:: Immagini/Combattimento6.png
	:width: 400
	:align: left

.. _azione_preparare:

--------------------------------
Preparare
--------------------------------
L’azione di preparare consente di organizzare un’azione tardiva, al termine del proprio turno, ma prima dell’inizio di quello, successivo. 

**Preparare** è un’:ref:`azione_standard` che non provoca :ref:`Attacchi di Opportunità<attacco_di_opportunità>` (sebbene possa farlo l’azione preparata).

Preparare un’Azione
	Si possono preparare soltanto :ref:`Azioni Standard<azione_standard>`, :ref:`di Movimento<azione_movimento>` o :ref:`Gratuite<azione_gratuita>` e per prepararle, bisogna dichiarare l’azione che si andrà a compiere e le condizioni in cui la si compirà. 

	A partire da quel momento, in qualsiasi istante prima della propria azione successiva, si può compiere l’azione preparata in risposta a quelle condizioni. L’azione preparata avviene prima di quella che la innesca e se l’azione preparata fa parte dell’attività di un altro personaggio, allora si interrompe quel personaggio. Nell’ipotesi che questi possa proseguire, egli al termine dell’azione preparata tornerà a fare ciò che stava facendo prima di essere interrotto. 

	Il proprio risultato di iniziativa cambia. Il risultato di iniziativa per la durata dell’incontro è il conteggio a cui è stata compiuta l’azione preparata, e si agisce subito prima del personaggio che ha innescato l’azione preparata. Si può fare un passo di 1,5 metri come parte della propria azione preparata, ma soltanto se nello stesso round non si copre altra distanza.

Conseguenze sull’Iniziativa
	Il risultato di iniziativa diventa il conteggio a cui è stata compiuta l’azione preparata. Se si arriva all’azione successiva senza aver compiuto l’azione preparata, non si è obbligati a compierla, ma la si può ripreparare. 

	Se si compie l’azione preparata nel round successivo, prima che arrivi il turno regolare, l’iniziativa sale al nuovo valore nell’ordine di combattimento, ma non si ottiene alcuna azione normale in quel round.

Distrarre Incantatori
	Si può preparare un attacco contro un incantatore con l’attivazione “se inizia a lanciare un incantesimo”. 

	Se si infliggono danni all’incantatore, quest’ultimo può perdere l’incantesimo che tentava di lanciare, secondo il risultato della sua prova di concentrazione.

.. _controincantesimo:

Preparare un controincantesimo
	Si può preparare un controincantesimo contro un incantatore (a volte con l’attivazione “se inizia a lanciare un incantesimo”). 

	In questo caso, quando l’incantatore inizia un incantesimo, si può identificarlo con una prova di :ref:`Sapienza Magica<sapienza_magica>` con **CD 15 + Liv Inc**. Se lo si identifica, e si può lanciare lo stesso incantesimo (per esempio, lo si è già preparato, in caso si preparano gli incantesimi), lo si può lanciare come controincantesimo e spezzare in automatico l’incantesimo dell’altro incantatore. 

	Il controincantesimo funziona persino se un incantesimo è divino e l’altro arcano.

	Un incantatore può usare :ref:`Dissolvi Magie<inc_dissolvi_magie>` per lanciare un controincantesimo contro un altro incantatore, ma non sempre funziona.

Preparare un’arma contro una Carica
	Si possono preparare certe armi perforanti e posizionarle per ricevere :ref:`Cariche<azione_carica>`. Un’arma preparata di questo tipo infligge danni raddoppiati, se con essa si colpisce un avversario in :ref:`azione_carica`.

--------------------------------
Ritardare
--------------------------------
Se si sceglie di ritardare, non si compie alcuna azione e poi si agisce normalmente al conteggio di iniziativa in cui si decide di farlo. 

Se si ritarda, si riduce di proposito il proprio risultato di iniziativa per l’intera durata della battaglia. Quando il nuovo (e più basso) conteggio di iniziativa si presenta più avanti nello stesso round, si può agire normalmente. 
Si può dichiarare il nuovo risultato di iniziativa o temporeggiare e agire poi nel round, fissando a quel punto il nuovo conteggio di iniziativa.

Non si può recuperare in alcun modo il tempo perso per decidere il da farsi. Inoltre, non si può interrompere l’azione di qualcun altro, come accade con un’azione preparata.

Conseguenze sull’Iniziativa
	Il risultato d’iniziativa diventa il conteggio in cui si decide di agire in ritardo. Se arriva il proprio turno successivo e non si è ancora compiuta un’azione, non si è costretti a compiere l’azione ritardata, sebbene si possa ritardare di nuovo.

	Se si posticipa l’azione nel round successivo, prima del proprio turno normale, il conteggio di iniziativa sale a quel nuovo valore nell’ordine di battaglia, e si perde la propria azione normale per quel round.