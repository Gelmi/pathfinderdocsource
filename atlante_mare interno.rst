
.. _atlante_mare_interno:

=================================================
Il Mare Interno
=================================================

.. contents:: Contenuti

.. _varisia:

Varisia
=====================
.. rubric:: Regione Selvaggia di Frontiera

.. figure:: Immagini/Atlante/Varisia.png

.. figure:: Immagini/Atlante/VarisiaSimbolo.png
	:align: right
	:width: 250

	Simbolo di Varisia

:Allineamento: Neutrale
:Capitale: Nessuna
:Regnante: Nessuno
:Governo: Città-stato non affiliate
:Linguaggi: Comune, Shoanti, Varisiano
:Religioni: Abadar, Calistria, Cayden Cailean, Desna, Erastil, Gozreh, Lamashtu, Norgorber, Pharasma, Urgathoa, Zon-Kuthon
:Insediamenti Notevoli: 
	* 	Abken (298)
	*	Arsmeril (288)
	* 	Baslwief (405)
	*	Biston (286)
	*	Covo di Palin (1,896)
	*	Covo di Roderic (1,100)
	*	Crying Leaf (780)
	*	Galduria (1420)
	*	Harse (828)
	*	Ilsurian (790)
	*	Janderhof (10,230)
	*	Kaer Maga (8,000)
	*	Korvosa (18,486)
	*	Magnimar (16,428)
	*	Melfesh (955)
	*	Nybor (528)
	*	Ravenmoor (135)
	*	Riddleport (13,300)
	*	Sandpoint (1,240)
	*	Sipplerose (635)
	*	Sirathu (440)
	*	Turtleback Ferry (430)
	*	Urglin (5,800)
	*	Veldraine (2,360)
	*	Wartle (302)
	*	Whistledown (551)
	*	Wolf’s Ear (240)

Un'irregolare regione compresa tra le regioni meridionali dell'antico impero di *Taldor* e le terre settentrionali governate dai *Re di Linnorm*, **Varisia** è stata a lungo considerata un luogo isolato. 

A fronte del fatto che Varisia fosse priva di insediamenti costieri da razziare, i *Re di Linnorm* hanno tradizionalmente evitato la regione, preferendo attaccare i regni più a Sud, mentre nel Sud la distanza necessaria rendeva l'esplorazione dell'area economicamente svantaggiosa.

Solo nelle ultime poche centinaia di anni le nazioni del *Mare Interno* hanno volto la loro attenzione a Varisia, trovando una terra matura per l'esplorazione, una cosa che i Varisiani hanno saputo per migliaia di anni, ma di cui hanno preferito mantenere il silenzio a riguardo.

Benché una porzione relativamente larga di *Varisia* sia stata insediata dai coloni *Chelisciani* e dai nativi :ref:`Varisiani<varisiani>`, rimane il fatto che questa regione sia primariamente una landa selvaggia. Mostri leggendari, che vanno dalle sinistre storie locali (come l'elusivo :ref:`Diavolo di Sandpoint<diavolo_sandpoint>`) fino a racconti di terrore regionali (come il mortale *Magga Nero*, o il mitico *Olifante di Jandelay*) hanno dato alle lande una nomea singolare, tuttavia non sono promemoria così incisivi dei pericoli di queste terre come i numerosi monumenti *Thassiloniani* che ancora punteggiano la regione. 

Costruiti da armate di giganti di pietra artigiani sotto il controllo dei signori delle rune, questi monumenti sono stati preservati e protetti dalla magia. Solo oggi questa magia protettiva va svanendo, lasciando gli antichi monumenti a soffrire lentamente l'oltraggio dell'erosione e dei cercatori di trofei. In molti di questi monoliti si annidano pericoli nascosti, immortali mostri intrappolati per secoli, allettanti camere di tesori protette da trappole e (qualcuno mormora) i dormienti signori delle rune, che attendono pazientemente di svegliarsi dal loro sono secolare per reclamare una terra loro di diritto.

-----------
Storia
-----------
Benché *Varisia* sia tornata al centro degli interessi del *Mare Interno* solo di recente, non è sempre esistita solo in quanto frontiera remota. 

Prima dell'*EarthFall* questo era il sito della più potente tra le civiltà umane: l'impero di *Thassilon*. Fondato da esuli di *Azlant* e governato da maghi conosciuti come signori delle rune che veneravano i sette peccati capitali, la nazione di *Thassilon* schiavizzava giganti per costruire giganteschi monumenti al suo proprio ego. 

Quando *EarthFall* portò nell'*Era Oscura*, *Thassilon*, già vacillante a causa della proprio decadenza e corruzione, cadde. La regione rimase selvaggia per centinaia di anni, abitata solo fa tribù barbare di viaggiatori *Shoanti* e :ref:`Varisiani<varisiani>` finchè la frontiere non rientrò finalmente nelle mire espansionistiche di *Cheliax* le cui armate marciarono sulla regione nel 4405 CA.

Soldati *Chelish* scacciarono i bellicosi *Shoanti* nelle aspre regioni a Nord-Est, mentre i coloni adottarono una tenue pace con i nativi :ref:`Varisiani<varisiani>` sotto le pretese di portare "cultura e civiltà" nelle loro vite. 

Fu così che l'antica frontiera divenne nota come *Varisia**. Da allora, l'area è diventata la regione del *Mare Interno* con lo sviluppo più veloce, una terra di nuove opportunità, tuttavia ancora piena di antichi misteri e le promesse di ricchezze sconosciute.

-------------
Governo
-------------
La *Varisia* moderna è una regione di conflitti, un tratto di frontiera giacente contro l'*Altura di Storval*, una landa di barbari e giganti a Nord-Est. 

Benché nessun governo centrale governi *Varisia*, tre città-stato sono emerse, ciascuna delle quali potrebbe reclamare un giorno il controllo della regione. 

La più antica e grande di queste città è *Korvosa*, una città di di lealisti *Cheliani* governati da una monarchia, ma spaccata verso *Cheliax* nel tentativo di venire riassorbita in quell'impero. Tuttavia, a differenza di quanto le sue genti vorrebbero far trasparire, *Korvosa* è lontana dall'essere l'unico centro di civiltà in *Varisia*.
La seconda più grande città della regione, la cosmopolita *Magnimar*, è in un'era di crescita, mentre *Korvosa* ristagna nella sua rigida collezione di valori tradizionali.
Più a Nord, nella terza più grande città di *Riddleport*, il crimine è attivamente incoraggiato, risultando in un covo di pirati, ladri e furfanti in rapida crescita.


Una qualsiasi di queste città potrebbe un giorno reclamare il diritto di governare *Varisia*, ma mentre tutte e tre competono per questo onore a loro modo, solo il tempo potrà dire quali di queste città sono destinate a una simile gloria.

.. figure:: Immagini/Atlante/VarisiaMappa.png
	:width: 800

---------
Registro
---------

Abbazia Windsong
----------------
Nonostante vi sia stato un periodo in cui figure del clero di tutte le maggiori religioni di *Varisia*, buone e cattive, si incontravano all'abbazia *Windsong* per risolvere i conflitti, diverse chiese si sono ritirate dall'assemblea dal momento della morte di *Aroden*, lasciando sale vuote e un crescente senso di paranoia al loro interno.

Altipiano Storval
-----------------
Nell'entroterra della regione, la terra si alza in una drammatica, seghettata fessura. Oltre questo confine naturale la terra cambia da boscosa e verdeggiante della zona inferiore ad accidentata landa. 

Qui *Orchi* e *Troll* sono insediati su aspre cime e profondi burroni, tuttavia i veri signori dell'altipiano sono i *Giganti*. Scendendo dalle caste schive dell'antica *Thassilon* giganti di tutti i tipi chiamano le selvaggie vette di *Varisia* casa e le loro periodiche incursioni e razzie contro l'uomo ne fanno un continuo e brutale promemoria del fatto che questo reame è lontano dall'essere addomesticato. 

Celwynvian
-----------
Nelle profondo della foresta di *Mierani*, si trova l'antica città elfica infestata di *Celwynvian*, i suoi lussureggianti palazzi e delicate torri chiuse in un misterioso conflitto. 

Gli elfi hanno provato diverse volte a reclamare la città abbandonata, ma mantengono il silenzio riguardo a cosa si annidi nelle rovine e che gli impedisce di aver successo. Alcuni suggeriscono uno strano contagio, altri dragoni o demoni. Alcuni pettegolezzi parlano persino di *Drow*, leggendari elfi del profondo sottosuolo, e suggeriscono che la città sia ora sede di innominabili atrocità che gli elfi stessi sono riluttanti a conoscere, come si vergognassero troppo di quello che i loro simili potrebbero fare.

Costa Perduta
---------------
Ritenuta da molti la miglior tenuta di caccia del leggendario :ref:`Diavolo di Sandpoint<diavolo_sandpoint>`, questo scorcio di costa lungo la riva occidentale di *Varisia* ha visto una repentina crescita nelle ultime quattro decadi, qualcosa che ha forzato le centinaia di tribù *Goblin* locali in conflitti più grandi che mai, sia tra di loro che con i loro vicini. 

Le acque di *Costa Perduta* presentano un abbondante pescato di qualità, così come pericolose creature selvagge come *Bunyips*, *Reefclaws*, e *Grandi Squali Bianchi*.

Kaer Maga
----------
La città più famosa di *Varisia*, *Kaer Maga* è appollaiata sulla cima di una delle più alte parti del *Pendio di Storval*, dominando le verdeggianti pianure a Sud. 

La città è costruita nelle rovine di una immensa, misteriosa fortezza che predata *Thassilon*, e i suoi abitanti apprezzano l'anarchia nella sua forma più pura. Tutti i generi di strane fazioni hanno sede tra le sale di *Kaer Maga*, dai sanguinari *BloatMage* ai monaci militanti dei *Fratelli del Sigillo* fino ai repellenti *Augur*, troll indovini che usano le loro stesse viscere per profetizzare, con discutibile precisione.

Montagne Kodar
----------------
Alte e minacciose, i frastagliati picchi innevati  delle *Montagne Kodar* sono forse le più alte nella regione del *Mare Interno*. Solo le creature più resistenti come *Giganti*, *Roc*, *Yeti*, *Wendigo* e *Draghi* sono in grado di sopportare il clima estremo e le altezze qui raggiunte.

Numerosi misteri e leggende hanno origini nascoste nelle profondità di queste montagne, ma poche possono competere al livello della città perduta di *Xin-Shalast*.

.. figure:: Immagini/Atlante/VarisiaRegina.png
	:align: left
	:width: 300

.. _korvosa:

Korvosa
---------
Un rifugio per mercanti e artigiani, *Korvosa* funge da viadotto per gli scambi attraverso tutta *Varisia*. 

La città è governata (alcuni dicono più che governata) da una complessa società che divide le responsabilità tra diversi magistrati, arbitri e una monarchia di re e regine. Il regnante corrente è re *Eodred*, benché sia progressivamente infermo negli ultimi pochi anni, lasciando il regno nelle mani della sua bellissima, ma meschina, giovane moglie, regina *Ileosa Arabasti*. 

La maggior parte dei cittadini di *Korvosa* sono nativi della città, ma conservano gran parte del sangue *Chelish* sia nell'aspetto che nelle tradizioni. I :ref:`Varisiani<varisiani>` sono tollerati in città ma spesso discriminati, mentre i *Shoanti* sono apertamente evitati e ritenuti rozzi e violenti barbari che non appartengono ad una città civilizzata.

Magnimar
----------
A differenza di :ref:`Korvosa<korvosa>`, le gilde sono attivamente incoraggiate in *Magnimar*, e con sufficiente fortuna e abilità chiunque può salire a una posizione di potere.

I :ref:`Varisiani<varisiani>` locali sono molto più tollerati qui, anche se tendono comunque a raccogliersi in specifici ghetti all'interno delle mura della città.

La legislatura di *Magnimar* si divive tra il Signor Sindaco *Haldmeer Grobaras* e un *Consiglio di Guide*. Man mano che la città cresce, anche il Consiglio lo fa, e col tempo *Magnimar* potrebbe davvero mettere in ombra *Korvosa*, specialmente finché la voce che *Magnimar* applica meno restrizioni sugli scambi raggiunge luoghi più distanti del mondo. 

Certamente gli interessi di *Magnimar* sono più sani di quelli di *Korvosa*.

Mobhad Leigh
--------------
Con un nome *Shoanti* che si traduce in "Cammina all'Inferno", il *Mobhad Leigh* ha catturato l'immaginazione per ere. 

In una fossa perfettamente circolare in un altrimenti ordinario campo alla base delle *Montagne Kodar*, un percorso di scale porta ad un via giù per la tromba della gola. Queste scale si estendono all'ingiù per qualche centinaio di metri, mentre il fondo  della fossa giace in realtà migliaia di metri più giù, nel regno di *Sekamina*. 

Le tribù *Shoanti* evitano questa regione, specialmente dopo che diversi dei loro incantatori sono caduti morti nel tentativo di scrutare le profondità della fossa con magia cristallografica.

Montagne Cave
--------------
Il più grande dei picchi dell'*Isola di Rivenrake* è ancora testimone di una particolarmente stupefacente rovina *Thassiloniana*: l'incisione del volto austero di una donna. 

Un immenso squarcio nel centro della faccia si apre in un'equamente immensa rete di caverne cosparse di rovine che, dicono le voci, porti in una città distrutta e soffocata dalla polvere nelle pendici delle *Montagne Cave*. 

Gli studiosi ritengono che questo sia il luogo di *Xin-Bakrakhan*, la *Città del'Ira* e capitale della nazione dei *Signori delle Rune di Alaznist*. 

Mushfens
----------
Una dei più grandi acquitrini di *Avistan*, il groviglio di paludi conosciuto come *Mushfens* si distende lungo l'intero confine Sud di *Varisia*. Quest'impervia regione ha resistito ogni tentativo di colonizzazione finora e fino ad oggi rimane una terra selvaggia in ogni senso del termine. 

*Boggards*, *Giganti delle Paludi* e strane creature regnano qui.

Riddleport
-------------
Questa città è una soluzione per colore che trovano la legge oppressiva in qualunque modo e serve da rifugio sicuro per mercenari, ladri, banditi e pirati di ogni sorta. 

I racconti do banditi che regnano sulle strade, di rapine e omicidi in pieno giorno e di rivolte e anarchia sono popolari tra i nobili di *Korsova*, tuttavia c'è poca verità in queste storie, poichè i *Signori di Riddleport* sono innegabilmente severi nel punire coloro che potrebbero tentare di disturbare le funzioni civili.

Benché *Riddleport* non sia un posto per i timidi, attira un sorprendente numero di studiosi e intellettuali, poichè la città ospita uno dei più intriganti e ben conservati monumenti *Thassiloniani*: il criptico *Cyphergate*, un anello di pietra che si inarca elegantemente sul porto cittadino. 

Il *Cyphergate* da lungo tempo intriga maghi, saggi e così via, tanto che questi stereotipicamente mansueti individui sono diventati una forza potente e influente nella società di *Riddleport*. 

.. figure:: Immagini/Atlante/VarisiaSindaco.png
	:align: right
	:width: 300


Regina Affondata
-----------------
Questa torreggiante piramide è parzialmente sprofondata nelle fangose profondità delle *Mushfens* circostanti, lasciando l'immensa rovina *Thassiloniana* piegata ad un pericoloso angolo. 

Una faccia della piramide è decorata con un immenso bassorilievo di una bellissima donna svestita, mentre torri ricurve si estendono dalla sua cima come funghi o ciminiere

Diverse tribù *Boggard* tengono la *Regina Affondata* in grande considerazione.

Sandpoint
-------------
La città costiera di *Sandpoint* ha goduto di prosperità nelle sue 4 o poco più decadi di esistenza, ed è recentemente cresciuta grande abbastanza da diventare la sesta più grande città di *Varisia*.

Nonostante la recente crescita di problemi (come la crescita della criminalità, incluse le razzie del notorio assassino *Jervas Stoot*, conosciuto anche come "Mannaia"), il sindaco di *Sandpoint* *Kendra Deverin* spera di vedere la prosperità della sua città crescere ancora negli anni a venire. 

*Sandpoint* è costruita intorno ad un'antica torre *Thassiloniana* conosciuta oggi come l'*Antica Luce*. Questo edificio in rovina, combinato con i tradizionali problemi della città con i *Goblin* locali, ne fa un ricettacolo di opportunità d'avventura.

Terre Cenere
--------------
Casa delle tre tribù *Shoanti*, le ostili *Terre Cenere* sono una regione unica. Non propriamente deserto, queste aspre rocciose terre desolate sono piuttosto vulcaniche. 

Bufere nere di carbone, immense praterie incendiate e mortali eruzioni di gas velenoso lo rendono un luogo pericoloso in cui dimorare, tuttavia gli *Shoanti* perseverano nel farlo, adattatasi al rigoroso ambiente come necessario.


Viperwall
----------
Goffrate con grandi teste di serpente, i tetti conici delle numerose torri di *Viperwall* risplendono alla luce della luna. 

Evitata dai locali, la struttura è spesso circondata da una verde foschia di gas velenoso che fuoriesce regolarmente da sculture di zanne nelle mura del castello. 

Xin-Shalast
-------------
Raccontata come la capitale di uno dei signori delle rune di *Thassilion*, *Karzoug il Conquistatore*, si pensa che *Xin-Shalast* giaccia nascosta da qualche parte nelle profondità delle *Montagne Kodar*. 

La leggendaria città di *Xin-Shalast* infesta da lungo tempo i sogni di romantici e storici.