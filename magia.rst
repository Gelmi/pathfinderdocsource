
.. _scuola_abiurazione:
.. _scuola_ammaliamento:
.. _scuola_divinazione:
.. _scuola_evocazione:
.. _scuola_illusione:
.. _scuola_invocazione:
.. _scuola_necromanzia:
.. _scuola_trasmutazione:

.. _magia:

==========
Magia
==========

.. contents:: Contenuti
	:local:
	:depth: 3

Dalla creazione di un bagliore di luce ad un terremoto,gli incantesimi sono una fonte di immenso potere. 

Un incantesimo è un effetto magico singolo che può essere di due tipi: :ref:`Arcani<incantesimi_arcani>`, lanciati da :ref:`Bardi<bardo>`, :ref:`Stregoni<stregone>` e :ref:`Maghi<mago>`, e :ref:`Divini<incantesimi_divini>`, lanciati da :ref:`Chierici<chierico>`, :ref:`Druidi<druido>`, :ref:`Paladini<paladino>` e :ref:`Ranger<ranger>`.

Alcuni incantatori selezionano i loro incantesimi da un limitato elenco di incantesimi conosciuti, mentre altri hanno accesso a un’ampia varietà di opzioni, molti preparano i loro incantesimi in anticipo, attraverso libri degli incantesimi o preghiere, altri lanciano incantesimi spontaneamente senza alcuna preparazione. 

Nonostante i diversi modi che i personaggi utilizzano per impararli o prepararli, quando li lanciano, i loro incantesimi sono molto simili.

.. _magia_e_incantesimi:

Magia e Incantesimi
=================================================

-----------
Scelta
-----------

.. _concentrazione:

------------------
Concentrazione
------------------

.. .. figure:: Immagini/RazzeElfo.png
..    :align: right
..    :width: 200

Ferimento
-----------------

Incantesimo
-----------------

In lotta/Immobilizzato
----------------------

Movimento Vigoroso
------------------

Movimento Violento
------------------

Tempo Impervio
-----------------

Lanciare sulla Difensiva
-------------------------

Intralciato
-----------------

-----------------
Controincantesimi
-----------------

.. .. figure:: Immagini/RazzeGnomo.png
..     :align: left
..     :width: 150

Funzionamento
--------------

Metamagia
----------

Eccezioni
----------

Dissolvi Magie
----------------

-------------------
Livello Incantatore
-------------------

-----------------------------
Fallimento degli Incantesimi
-----------------------------

-----------------
Effetti Speciali
-----------------

Attacchi
--------

Tipi di Bonus
-------------

Riportare in Vita i Morti
---------------------------

-----------------
Combinare Effetti
-----------------

Cumulativi
-----------

Contrapposti
-------------

Istantanei
-----------

Descrizione degli Incantesimi
===============================

-----------------
Nome
-----------------

.. _scuola_magia:

--------------------
Scuola (Sottoscuola)
--------------------

.. _abiurazione:

Abiurazione
-------------

.. _ammaliamento:

Ammaliamento
-------------

.. _divinazione:

Divinazione
-------------

.. _evocazione:

Evocazione
-------------

.. _illusione:

Illusione
-------------

.. _invocazione:

Invocazione
-------------

.. _necromanzia:

Necromanzia
-------------

.. _trasmutazione:

Trasmutazione
-------------

-----------------
[Descrittore]
-----------------

-----------------
Livello
-----------------

-----------------
Componenti
-----------------

-----------------
Tempo di Lancio
-----------------

-----------------
Raggio d'Azione
-----------------

----------------------------
Dirigere un Incantesimo
----------------------------

----------
Durata
----------

-------------------
Tiro Salvezza
-------------------

----------------------------
Resistenza agli Incantesimi
----------------------------

-----------------------------
Testo Descrittivo
-----------------------------

.. _incantesimi_arcani:

Incantesimi Arcani
====================

-----------------------------
Preparare Incantesimi da Mago
-----------------------------

---------------------------
Scritti Magici Arcani
---------------------------

Incantesimi e Libri in Prestito
--------------------------------

Aggiungere Incantesimi al Libro
--------------------------------

Scrivere un Nuovo Incantesimo
------------------------------

Sostituire e Copiare Libri
---------------------------

Vendere Libri
----------------

----------------
Stregoni e Bardi
----------------

.. _incantesimi_divini:

Incantesimi Divini
===================

--------------------
Preparazione
--------------------

--------------------------
Scritti Divini
--------------------------

------------------------
Nuovi Incantesimi
------------------------

Capacità Speciali
========================