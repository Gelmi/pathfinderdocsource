
Welcome to PathFinder!
====================================================

.. figure:: Immagini/Pathfinder.png
    :align: center

La seguente guida è per lo più tratta da diversi manuali di PathFinder rifrasando, filtrando, espandendo e riassumendone i contenuti come mi sembrava il caso, in modo da rendere la lettura più fluida e meno confusionaria.

L'obbiettivo era rendere la guida il più possibile fruibile a prescindere dalla conoscenza delle regole di PathFinder o in generale dei giochi di ruolo. 
A volte questo ha portato ad una maggior libertà di interpretazione delle regole rispetto ai manuali che sono costruiti in modo da essere il meno equivoci possibile, a scapito spesso della semplicità dell'esposizione. 

Gran parte delle connotazioni puramente descrittive e fantasy sono state rimosse in modo da rendere questa guida più vicina a un bigino (non tanto "ino") che al manuale vero e proprio, che si lancerebbe in descrizioni fantastiche, come è giusto che sia!

Tenete presente, quindi, che in caso di dubbi è possibile che il manuale possa gettare maggiore chiarezza sull'interpretazione delle regole. Essendo questa guida costruita (per lo più) seguendo la struttura del manuale di gioco stesso, non dovrebbe essere troppo difficile orientarsi per trovare le sezioni corrispondenti. 

Buon Divertimento!

.. Link da aggiungere

.. _ts_magia:
.. _attacchi_secondari:
.. _tabella_denaro_per_livello:
.. _macchine_da_assedio:
.. _azione_contrastare_incantesimo:
.. _inc_aiuto:
.. _inc_allarme:
.. _inc_allineare_arma:
.. _inc_allucinazione_mortale:
.. _inc_alterare_sè_stesso:
.. _inc_ammorbidire_terra_e_pietra:
.. _inc_animare_corde:
.. _inc_animare_morti:
.. _inc_animare_oggetti:
.. _inc_animare_vegetali:
.. _inc_anti_individuazione:
.. _inc_arma_magica:
.. _inc_arma_spirituale:
.. _inc_armatura_magica:
.. _inc_aura_sacra:
.. _inc_aura_sacrilega:
.. _inc_bagliore_solare:
.. _inc_banchetto_degli_eroi:
.. _inc_barriera_di_lame:
.. _inc_benedire_acqua:
.. _inc_benedire_arma:
.. _inc_blasfemia:
.. _inc_blocca_animali:
.. _inc_blocca_mostri:
.. _inc_bocca_magica:
.. _inc_caduta_morbida:
.. _inc_calmare_animali:
.. _inc_camminare_nellaria:
.. _inc_camminare_nelle_ombre:
.. _inc_campo_anti_magia:
.. _inc_camuffare_sè_stesso:
.. _inc_cancellare:
.. _inc_catena_di_fulmini:
.. _inc_cecitàsordità:
.. _inc_cerchio_di_teletrasporto:
.. _inc_cerchio_magico_contro_il_bene:
.. _inc_cerchio_magico_contro_il_caos:
.. _inc_cerchio_magico_contro_il_male:
.. _inc_cerchio_magico_contro_la_legge:
.. _inc_charme_sui_mostri:
.. _inc_chiaroveggenza:
.. _inc_colpo_accurato:
.. _inc_colpo_infuocato:
.. _inc_comandare_vegetali:
.. _inc_comando_superiore:
.. _inc_comprensione_dei_linguaggi:
.. _inc_confusione_inferiore:
.. _inc_confusione:
.. _inc_congedo:
.. _inc_cono_di_freddo:
.. _inc_conoscenze_delle_leggende:
.. _inc_contagio:
.. _inc_contrastare_elementi:
.. _inc_controllare_acqua:
.. _inc_controllare_non_morti:
.. _inc_controllare_tempo_atmosferico:
.. _inc_controllare_vegetali:
.. _inc_controllare_venti:
.. _inc_corpo_elementale_I:
.. _inc_corpo_elementale_II:
.. _inc_corpo_elementale_III:
.. _inc_corpo_elementale_IV:
.. _inc_costrizionecerca:
.. _inc_creare_non_morti:
.. _inc_creare_non_morti_superiori:
.. _inc_creazione_maggiore:
.. _inc_creazione_minore:
.. _inc_crescita_di_spine:
.. _inc_crescita_vegetale:
.. _inc_cumulo_strisciante:
.. _inc_curare:
.. _inc_cura_ferite_critiche:
.. _inc_cura_ferite_critiche_di_massa:
.. _inc_cura_ferite_gravi:
.. _inc_danza_irresistibile:
.. _inc_dardo_incantato:
.. _inc_deformare_legno:
.. _inc_demenza:
.. _inc_desiderio:
.. _inc_desiderio_limitato:
.. _inc_dettame:
.. _inc_disgiunzione_arcana:
.. _inc_disintegrazione:
.. _inc_dissolvi_il_bene:
.. _inc_dissolvi_il_caos:
.. _inc_dissolvi_il_male:
.. _inc_dissolvi_la_legge:
.. _inc_dissolvi_magie_superiore:
.. _inc_distruggere_viventi:
.. _inc_distruzione:
.. _inc_dito_della_morte:
.. _inc_divinazione:
.. _inc_dominare_animali:
.. _inc_dominare_mostri:
.. _inc_dominare_persone:
.. _inc_eroismo:
.. _inc_esigere:
.. _inc_esilio:
.. _inc_esplosione_solare:
.. _inc_estasiare:
.. _inc_evoca:
.. _inc_evocare:
.. _inc_evoca_alleato_naturale_II:
.. _inc_evoca_alleato_naturale_III:
.. _inc_evoca_alleato_naturale_IV:
.. _inc_evoca_alleato_naturale_V:
.. _inc_evoca_alleato_naturale_VI:
.. _inc_evoca_alleato_naturale_VII:
.. _inc_evoca_alleato_naturale_VIII:
.. _inc_evoca_mostri_III:
.. _inc_evoca_mostri_IV:
.. _inc_evoca_mostri_V:
.. _inc_evoca_mostri_VI:
.. _inc_evoca_mostri_VII:
.. _inc_evoca_mostri_VIII:
.. _inc_evoca_mostri_IX:
.. _inc_evocazioni_istantanee:
.. _inc_fabbricare:
.. _inc_fatale:
.. _inc_favore_divino:
.. _inc_ferire:
.. _inc_fermare_il_tempo:
.. _inc_fiamma_perenne:
.. _inc_forme_animali:
.. _inc_forma_di_drago_I:
.. _inc_forma_di_drago_II:
.. _inc_forma_di_drago_III:
.. _inc_forma_di_vegetale_I:
.. _inc_forma_di_vegetale_II:
.. _inc_forma_di_vegetale_III:
.. _inc_forma_eterea:
.. _inc_forma_ferina_II:
.. _inc_forma_ferina_III:
.. _inc_forma_gassosa:
.. _inc_forza_del_toro:
.. _inc_frantumare:
.. _inc_fuorviare:
.. _inc_giusto_potere:
.. _inc_glifo_di_interdizione:
.. _inc_glifo_di_interdizione_superiore:
.. _inc_grido:
.. _inc_guarigione:
.. _inc_guarigione_di_massa:
.. _inc_guscio_anti_vita:
.. _inc_identificare:
.. _inc_immagine_silenziosa:
.. _inc_immagine_speculare:
.. _inc_immunità_agli_incantesimi:
.. _inc_implosione:
.. _inc_inaridire:
.. _inc_individuazione_dei_pensieri:
.. _inc_individuazione_del_magico:
.. _inc_individuazione_dello_scrutamento:
.. _inc_infliggere:
.. _inc_infliggi_ferite_critiche:
.. _inc_influenza_sacrilega:
.. _inc_infondere_capacità_magiche:
.. _inc_ingrandire_persone:
.. _inc_interdizione_alla_morte:
.. _inc_intermittenza:
.. _inc_invisibilità:
.. _inc_invisibilità_superiore:
.. _inc_invisibilità_di_massa:
.. _inc_invocare_il_fulmine:
.. _inc_ira_dellordine:
.. _inc_lamento_della_banshee:
.. _inc_legame_planare_inferiore:
.. _inc_legame_planare:
.. _inc_legame_telepatico:
.. _inc_legno_di_ferro:
.. _inc_lettura_del_magico:
.. _inc_libertà:
.. _inc_libertà_di_movimento:
.. _inc_linguaggi:
.. _inc_localizza_oggetto:
.. _inc_luce:
.. _inc_luce_diurna:
.. _inc_luce_incandescente:
.. _inc_incubo:
.. _inc_mani_brucianti:
.. _inc_mano_stringente:
.. _inc_mano_stritolatrice:
.. _inc_mantello_del_caos:
.. _inc_martello_del_caos:
.. _inc_metamorfosi:
.. _inc_miracolo:
.. _inc_momento_di_prescienza:
.. _inc_muro_di_ferro:
.. _inc_muro_di_fuoco:
.. _inc_muro_di_pietra:
.. _inc_muro_di_spine:
.. _inc_muro_di_vento:
.. _inc_neutralizza_veleno:
.. _inc_non_morto_a_morto:
.. _inc_nube_di_nebbia:
.. _inc_nube_incendiaria:
.. _inc_ombra_di_una_evocazione:
.. _inc_ombra_di_una_invocazione_superiore:
.. _inc_ombre:
.. _inc_onde_di_affaticamento:
.. _inc_onde_di_esaurimento:
.. _inc_orrido_avvizzimento:
.. _inc_oscurità_profonda:
.. _inc_pagina_segreta:
.. _inc_palla_di_fuoco:
.. _inc_parlare_con_i_morti:
.. _inc_parola_del_caos:
.. _inc_parola_del_potere_accecare:
.. _inc_parola_del_potere_stordire:
.. _inc_parola_del_potere_uccidere:
.. _inc_parola_sacra:
.. _inc_passo_veloce:
.. _inc_paura:
.. _inc_pelle_coriacea:
.. _inc_pelle_di_pietra:
.. _inc_pietra_magica:
.. _inc_pirotecnica:
.. _inc_porta_dimensionale:
.. _inc_porta_in_fase:
.. _inc_portale:
.. _inc_potere_divino:
.. _inc_preghiera:
.. _inc_prestidigitazione:
.. _inc_previsione:
.. _inc_produrre_fiamma:
.. _inc_proiezione_astrale:
.. _inc_protezione_dal_caos:
.. _inc_protezione_dagli_incantesimi:
.. _inc_protezione_dal_bene:
.. _inc_protezione_dalla_legge:
.. _inc_protezione_dallenergia:
.. _inc_protezione_dal_male:
.. _inc_pugno_serrato:
.. _inc_punizione_sacra:
.. _inc_raggio_rovente:
.. _inc_regressione_mentale:
.. _inc_repulsione:
.. _inc_resistenza_agli_incantesimi:
.. _inc_respingere_legno:
.. _inc_respirare_sottacqua:
.. _inc_respiro_di_vita:
.. _inc_resurrezione:
.. _inc_rianimare_morti:
.. _inc_rigenerazione:
.. _inc_riflettere_incantesimo:
.. _inc_rifugio:
.. _inc_rimuovi_cecità:
.. _inc_rimuovi_malattia:
.. _inc_rimuovi_paralisi:
.. _inc_rimuovi_paura:
.. _inc_rimuovi_sordità:
.. _inc_rintocco_di_morte:
.. _inc_riposo_inviolato:
.. _inc_risata_spaventosa:
.. _inc_riscaldare_il_metallo:
.. _inc_risucchio_di_energia:
.. _inc_rivela_bugie:
.. _inc_rivela_locazioni:
.. _inc_rocce_aguzze:
.. _inc_rune_esplosive:
.. _inc_santuario:
.. _inc_schermo:
.. _inc_sciame_di_meteore:
.. _inc_sciame_elementale:
.. _inc_scolpire_legno:
.. _inc_scolpire_pietra:
.. _inc_scopri_il_percorso:
.. _inc_scudo_della_fede:
.. _inc_scudo_della_legge:
.. _inc_scudo_di_fuoco:
.. _inc_scudo_su_altri:
.. _inc_scrutare:
.. _inc_semi_di_fuoco:
.. _inc_sfera_prismatica:
.. _inc_sfocatura:
.. _inc_silenzio:
.. _inc_simbolo:
.. _inc_simbolo_di_morte:
.. _inc_simulacro:
.. _inc_sonno_profondo:
.. _inc_spada_sacra:
.. _inc_spezzare_incantamento:
.. _inc_spostamento_planare:
.. _inc_status:
.. _inc_teletrasporto:
.. _inc_teletrasporto_superiore:
.. _inc_tempesta_di_ghiaccio:
.. _inc_tempesta_di_nevischio:
.. _inc_tempesta_di_vendetta:
.. _inc_tentacoli_neri:
.. _inc_terremoto:
.. _inc_tocco_del_vampiro:
.. _inc_tocco_di_idiozia:
.. _inc_tocco_gelido:
.. _inc_trama_scintillante:
.. _inc_trappola_di_fuoco:
.. _inc_trasformazione:
.. _inc_traslazione_arborea:
.. _inc_turbine:
.. _inc_vedere_invisibilità:
.. _inc_veleno:
.. _inc_velo:
.. _inc_velocità:
.. _inc_veste_magica:
.. _inc_visione_del_vero:
.. _inc_visione_della_morte:
.. _inc_visione_falsa:
.. _inc_vita_falsata:
.. _inc_volare:
.. _inc_volo_giornaliero:
.. _inc_vuoto_mentale:
.. _oggetti_magici:
.. _appendice_capacità_speciali:
.. _capacità_fiuto:
.. linka anche il fiuto bestiario.
.. _capacità_percezione_tellurica:
.. _visione_crepuscolare:
.. _percezione_cieca:
.. _scurovisione:
.. _vista_cieca:
.. _caduta:
.. _costo_capacità_armi:
.. _costo_bastoni:
.. _equipaggiamento_png:
.. _creazione_oggetti_magici:
.. _armi_anarchica:
.. _armi_assiomatica:
.. _armi_danzante:
.. _armi_infuocata:
.. _armi_sacrilega:
.. _sanguinamento:
.. _abbagliato:
.. _accecato:
.. _affaticato:
.. _assordato:
.. _danno_caratteristica:
.. _oggetti_completamento_incantesimo:
.. _confuso:
.. _immobilizzato:
.. _indifeso:
.. _infermo:
.. _in_lotta:
.. _in_preda_al_panico:
.. _intralciato:
.. _invisibile:
.. _paralizzato:
.. _prono:
.. _scosso:
.. _spaventato:
.. _stabilizzato:
.. _rotto:
.. _distrutto:
.. _risucchio_caratteristica:
.. _veleni:

:Stato della guida: 
   Incompleta: Diversi link riportano alla home se la voce corrispondente non è ancora stata creata. Altre portano al posto corretto, ma che non ha ancora le relative informazioni :)

Contenuti:

.. toctree::
   :maxdepth: 2
   :caption: Manuale di Gioco:
   :numbered: 2

   introduzione
   razze
   classi
   abilita
   talenti
   equipaggiamento
   regole_aggiuntive
   combattimento
   magia
   incantesimi
   classi_prestigio
   oggetti_magici

.. toctree::
   :maxdepth: 2
   :caption: Bestiario:
   :numbered: 2

   bestiario
   bestiario_per_tipo

.. toctree::
   :maxdepth: 2
   :caption: Atlante del Mare Interno:
   :numbered: 2

   atlante_introduzione
   atlante_razze
   atlante_mare interno
   atlante_mostri

.. toctree::
   :maxdepth: 2
   :caption: Extra:
   :numbered: 2
   
   glossario
   download

