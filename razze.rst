
.. _razze:

=================================================
Razze
=================================================

.. contents:: Contenuti
	:local:
	:depth: 3

Dagli intrepidi e robusti nani ai nobili ed aggraziati elfi, le razze di *PathFinder* differiscono per cultura, taglia, atteggiamento ed aspetto fisico. 

Il secondo passo nella creazione del personaggio è la scelta della razza le cui alternative sono proposte nel presente capitolo. 

Le prime sette sono quelle base: le razze civilizzate che si incontrano più comunemente in *Pathfinder*. Queste hanno capacità, personalità e società molto diverse tra loro, ma sono allo stesso tempo tutte abbastanza simili:
nessuna delle razze si discosta troppo da quella umana e tutte le loro capacità sono pressochè uguali ed equilibrate. Le altre sono più insolite e meno equilibrate.

Naturalmente, ogni razza si adatta meglio ad un tipo specifico di ruolo: i nani sono meglio come guerrieri che come stregoni, mentre gli halfling non eccellono quanto i mezzorchi come barbari. 

Può anche essere divertente giocare una razza che è in contrasto con il suo ruolo tipico, ma non è molto divertente guadagnare tre livelli e poi rendersi conto che il personaggio che si sta giocando sarebbe stato migliore se fosse stato di una razza completamente diversa.

Le razze sono presentate a partire da una descrizione generica del ruolo della razza nel mondo, seguita da brevi descrizioni sull’aspetto fisico, la società ed i rapporti con le altre razze. Anche se la vostra razza non costringe a scegliere una religione o un allineamento specifici, vengono accennate le scelte più comuni per ogni razza, si discute la ragione per cui un membro della razza potrebbe decidere di intraprendere la vita dell’avventuriero e, infine, viene presentanto un elenco di alcuni nomi tipici, maschili e femminili.

Ciascuna delle sette razze ha una serie di capacità speciali, bonus ed altre modifiche che si applicano a tutti i membri di quella razza, dette *tratti razziali*, così come dei modificatori ai punteggi di caratteristica che possono aumentare una caratteristica al di sopra di 18 o diminuirla a meno di 3: ma dovreste comunque avere l’approvazione del Game Master prima di giocare un personaggio con qualsiasi punteggio di caratteristica inferiore a 3.

.. Note::
	La familiarità nelle armi indica la possibilità di utilizzare un tipo di arma a prescindere dalla classe scelta.

.. _razze_base:

Razze Base
============

.. _elfi:

------------------
Elfi
------------------

.. figure:: Immagini/RazzeElfo.png
    :align: right
    :width: 200

I longevi elfi sono i figli del mondo naturale, simili per alcuni versi alle creature fatate, e tuttavia differenti. Essi danno valore alla riservatezza ed alle tradizioni e sebbene sia per loro piuttosto difficile instaurare nuove amicizie, a qualsiasi livello, una volta che uno straniero è accettato come compagno, tale alleanza può durare per intere generazioni. 

Gli elfi hanno un curioso legame con ciò che li circonda, forse conseguenza della loro longevità o altri motivi più profondi e mistici, e se abitano nello stesso luogo per molto tempo scoprono che il loro fisico si è adattato al luogo: il segno più evidente è il colore della pelle che riflette quello dell’ambiente. 

Quegli elfi che passano le loro vite fra le razze meno longeve, d’altra parte, spesso sviluppano una percezione distorta della mortalità e diventano più cupi, per aver visto invecchiare e morire davanti ai loro occhi i propri compagni numerose volte.

Descrizione fisica
	Più alti, aggraziati ed esili degli umani, con lunghe orecchie a punta e ampi occhi a mandorla e pupille grandi dai colori vivaci. 

	I vestiti elfici spesso tendono ad adattarsi alla bellezza dell’ambiente naturale, ma gli elfi che vivono in città tendono ad abbigliarsi all’ultima moda.
Società
	Gli elfi percepiscono un forte legame con la natura e si sforzano di vivere in armonia con essa, trovando la manipolazione della terra e della pietra spiacevole e preferendo arti più raffinate. Questo rende gli elfi molto portati alla magia.
Rapporti 
	Tengono lontane le altre razze, considerandole impetuose ed impulsive, ma sanno giudicare le persone in modo eccellente. 

	Un elfo potrebbe non volere mai un **nano** come vicino, ma sarebbe il primo a riconoscerne le abilità come fabbro. Considerano gli **gnomi** come bizzarre curiosità (ed a volte pericolosi) e gli **halfling** con un po’ di pietà, dato che questi popoli appaiono agli elfi alla deriva, senza una vera e propria patria. 

	Gli elfi sono affascinati dagli **umani**, come provato dal grande numero di **mezzelfi** nel mondo, sebbene solitamente rinneghino tale prole. Considerano i **mezzorchi** con diffidenza e sospetto.
Allineamento e religione
	Impressionabili e capricciosi, danno valore a bontà e bellezza. 

	Gli elfi sono per lo più di allineamento caotico buono. Preferiscono quelle divinità che condividono il loro amore per le qualità mistiche del mondo, come Desna e Nethys, la prima per l’ammirazione e l’amore per i luoghi selvaggi e il secondo per la sua padronanza della magia. 

	Calistria è forse la più famosa delle divinità elfiche, dato che rappresenta gli ideali elfici portati al loro estremo.
Avventurieri
	Molti elfi vanno in avventura spinti dal desiderio di esplorare il mondo, abbandonando i loro appartati regni nelle foreste per recuperare la dimenticata magia elfica o cercare regni perduti fondati millenni fa dai loro antenati. 

	Per quelli cresciuti fra gli umani, la vita effimera e libera di un avventuriero conserva un richiamo naturale. 

	Non amano la mischia finendo per preferire le classi del :ref:`Mago<mago>` e del :ref:`Ranger<ranger>`.
Nomi maschili
	Caladrel, Heldalel, Lanliss, Meirdrarel, Seldlon, Talathel, Variel, Zordlon.
Nomi femminili
	Amrunelara, Dardlara, Faunra, Jathal, Merisiel, Oparal, Soumral, Tessara, Yalandlara.

Tratti Razziali
-----------------
:Caratteristiche: Des +2, Int +2, Cos –2

	Gli elfi sono lesti e svegli, ma piuttosto deboli.

:Taglia: Media
:Velocità: Normale (9m)
:Visione Crepuscolare: Gli elfi vedono :ref:`due volte più lontano<visione_crepuscolare>` di un umano in condizioni di luce scarsa.
:Immunità degli elfi: Immunità a effetti di sonno magico e +2 ai TS contro incantesimi ed effetti di ammaliamento.
:Magia degli Elfi: +2 alle prove di Liv incantatore effettuate per superare la resistenza agli incantesimi. +2 alle prove di :ref:`Sapienza Magica<sapienza_magica>` effettuate per identificare le proprietà degli oggetti magici.
:Sensi Acuti: +2 alle prove di :ref:`Percezione<percezione>`.
:Familiarità nelle armi: Arco corto (compreso arco corto composito), arco lungo (compreso arco lungo composito), spada lunga e stocco, e qualsiasi arma con la parola *elfico* nel nome vale come arma da guerra.
:Linguaggi: Gli elfi iniziano il gioco parlando Comune ed Elfico e possono scegliere tra Celestiale, Draconico, Gnoll, Gnomesco, Goblin, Orchesco e Silvano a seconda del loro Mod Int.

.. _gnomi:

-------------
Gnomi
-------------

.. figure:: Immagini/RazzeGnomo.png
    :align: left
    :width: 150

Gli gnomi tracciano la loro discendenza nel misterioso regno fatato, un luogo in cui i colori sono più luminosi, le terre incolte più selvagge e le emozioni più primeve. 

Forze sconosciute hanno portato via gli gnomi antichi da quel regno, costringendoli a cercare rifugio in questo mondo, ciononostante gli gnomi non hanno mai completamente abbandonato le loro radici fatate né si sono adattati alla cultura mortale, finendo per essere considerati dalle altre razze per lo più come misteriosi e bizzarri.

Descrizione fisica
	Gli gnomi sono una delle razze comuni più piccole, in genere non più alti di 90cm, con i capelli di colori accesi come l’arancione vivace delle foglie in autunno, il verde brillante delle foreste in primavera, o il rosso e il viola profondo dei fiori di campo. 

	La loro carnagione varia dal marrone al rosa, solitamente con poco riguardo per l’ereditarietà, con lineamenti molto diversi. Molti hanno bocche ed occhi molto grandi, peculiarità che può essere inquietante o affascinante, a seconda dell’individuo.
Società
	Non si organizzano nelle classiche strutture sociali.
	Creature capricciose per natura, gli gnomi viaggiano spesso da soli o con compagni provvisori, sempre alla ricerca di esperienze nuove e più emozionanti e formano raramente relazioni durature fra loro o con le altre razze, perseguendo invece i mestieri, le professioni o l’arte con una passione che a volte sfocia nel fanatismo. 

	Gli gnomi maschi hanno una bizzarra ammirazione per cappelli e copricapi insoliti, mentre le femmine amano le acconciature elaborate ed eccentriche.
Rapporti
	Gli gnomi hanno difficoltà ad interagire con le altre razze, sia a livello fisico che emozionale. L’umore degli gnomi è difficile da capire e spesso viene travisato dalle altre razze come malizioso o senza senso, mentre gli gnomi a loro volta ritengono le razze più alte come giganti noiosi e pesanti. 

	Vanno d’accordo con **halfling** e **umani**, ma sono eccessivamente affettuosi nello scherzare con **nani** e **mezzorchi**, che gli gnomi tendono a voler far rilassare. Rispettano gli **elfi**, ma spesso si sentono frustrati dalla lentezza con cui essi prendono le decisioni. 

	Per gli gnomi l’azione è sempre meglio dell’inerzia, e per questo molti sono soliti portare con sè sempre parecchi progetti piuttosto complessi per mantenersi attivi durante i periodi di riposo.
Allineamento e religione
	Solitamente di allineamento neutrale buono e preferiscono adorare le divinità che valorizzano l’individualità e la natura, come Shelyn, Gozreh, Desna e sempre più Cayden Cailean.
Avventurieri
	L’amore degli gnomi per i viaggi li rende degli avventurieri naturali. Diventano spesso vagabondi per sperimentare nuovi aspetti della vita. 

	La loro debolezza è compensata da una propensione per la :ref:`stregoneria<stregone>` e l’arte dei :ref:`Bardi<bardo>`.
Nomi maschili
	Abroshtor, Bastargre, Halungalom, Krolmnite, Poshment, Zarzuket, Zatqualmie.
Nomi femminili
	Besh, Fijit, Lini, Neji, Majet, Pai, Queck, Trig.

Tratti Razziali
-----------------
:Caratteristiche: Cos +2, Car +2, For –2

	Gli gnomi sono fisicamente deboli ma sorprendentemente resistenti, ed il loro atteggiamento li rende per natura simpatici.
:Taglia: Piccola
	
	Bonus di taglia +1 alla CA e ai tiri per colpire, penalità –1 a BMC e DMC, e +4 alle prove di :ref:`Furtività<furtività>`.
:Velocità: Lenta (6m)
:Visione Crepuscolare: Gli gnomi vedono :ref:`due volte più lontano<visione_crepuscolare>` di un umano in condizioni di luce scarsa.
:Addestramento Difensivo: Bonus di schivare +4 alla CA contro mostri del sottotipo Gigante.
:Magia Gnomesca: 
	+1 alla CD di ogni TS contro gli incantesimi di illusione che lanciano. 

	Gli gnomi con Carisma 11 o superiore guadagnano le seguenti capacità magiche: 1/giorno—:ref:`Luci Danzanti<inc_luci_danzanti>`, :ref:`Parlare con gli Animali<inc_parlare_con_gli_animali>`, :ref:`Prestidigitazione<inc_prestidigitazione>` e :ref:`Suono Fantasma<inc_suono_fantasma>`. Il Liv Incantatore per questi effetti è uguale al Liv dello gnomo, con CD = 10 + Liv Inc + Mod Car.
:Odio: +1 ai tiri per colpire contro creature umanoidi del sottotipo rettile e goblinoide grazie allo speciale addestramento contro questi odiati nemici.
:Resistenza alle Illusioni: 
	+2 ai TS contro incantesimi ed effetti di Illusione.
:Sensi Acuti: +2 alle prove di :ref:`Percezione<percezione>`.
:Ossessione: +2 alle prove di :ref:`Artigianato<artigianato>` o :ref:`Professione<professione>` di loro scelta.
:Familiarità nelle Armi: Le armi con la parola “gnomesca” nel nome valgono come arma da guerra.
:Linguaggi: Gli gnomi iniziano il gioco parlando Comune, Gnomesco e Silvano e possono scegliere tra Draconico, Elfico, Gigante, Goblin, Nanico e Orchesco a seconda del loro Mod Int.

.. _halfling:

------------
Halfling
------------

.. figure:: Immagini/RazzeHalfling.png
    :align: right
    :width: 180

Ottimisti e allegri per natura, benedetti da una fortuna misteriosa e determinati da uno spirito formidabile, gli halfling compensano la loro bassa statura con un’abbondanza di spavalderia e curiosità. 

Opportunisti inveterati, emozionabili e accomodanti allo stesso tempo, gli halfling prediligono mantenere un temperamento costante e controllato in ogni occasione, evitando quegli scoppi troppo violenti o emotivi delle altre razze più volubili. Anche nei momenti più catastrofici, un halfling non perde quasi mai il suo senso dell’umorismo. 

Incapaci di difendersi fisicamente dai rigori del mondo, sanno quando piegarsi al vento e quando nascondersi, tuttavia la curiosità degli halfling spesso sovrasta il buon senso, portandoli a decisioni poco adeguate per evitare i pericoli. 

Benché la loro curiosità li guidi per viaggiare e cercare nuovi posti ed esperienze, gli halfling possiedono un forte senso della propria casa, spendendo spesso
oltre i loro mezzi per aumentare le comodità della vita domestica.

Descrizione fisica
	Gli halfling hanno l’umile altezza di 90 cm, preferiscono camminare a piedi nudi, e ciò fa sì che la pianta dei loro piedi sia piuttosto callosa: ciuffi di peli ricci e fitti scaldano la parte superiore dei loro piedi abbronzati e larghi. 

	La loro carnagione tende verso il color mandorla ed i loro capelli verso le tonalità chiare del marrone, hanno orecchie a punta, ma non molto più grandi di quelle di un umano.
Società
	Non reclamano una patria e non controllano insediamenti più grandi di assemblee rurali di città libere e molto più spesso, abitano vicino agli umani in città umane, sbarcando il lunario come possono dagli scarti di società più grandi.

	Molti halfling conducono vite assai ricche all’ombra dei loro vicini più alti, mentre alcuni preferiscono una vita nomade, viaggiando per il mondo e sperimentando tutto quello che esso può offrire.
Rapporti
	Si vantano spesso con le altre razze della loro capacità di passare inosservati, caratteristica che permette a molti di eccellere nell’arte del furto e del sotterfugio. 

	Molti halfling, sapendo bene i pregiudizi a riguardo, cercano di essere disponibili ed amichevoli verso le razze più grandi quando non stanno provando a passare inosservati. 

	Vanno d’accordo con gli eccentrici **gnomi**, ma li trattano sempre con estrema cautela. Convivono bene con gli **umani**, ma dato che alcune società umane più aggressive li considerano poco meno di schiavi, cercano di controllarsi quando hanno a che fare con loro. Rispettano gli **elfi** e i **nani**, ma vivono spesso in luoghi remoti lontani dalle comodità che gli halfling amano, limitando le occasioni di interazione. Soltanto i **mezzorchi** sono proprio del tutto evitati dagli halfling, dato che le loro dimensioni e la natura violenta ne intimoriscono non poco la maggior parte.
Allineamento e religione
	Leali ai loro amici e famiglie, ma sanno che a volte dovranno litigare e elemosinare per sopravvivere.

	Per lo più di allineamento neutrale, gli halfling prediligono le divinità che incoraggiano le piccole e ben congegnate comunità, sia che tendano al bene (come Erastil) o al male (come Norgorber).
Avventurieri
	La loro fortuna intrinseca unita al loro insaziabile desiderio di viaggiare rendono gli halfling ideali per l’avventura. 

	Altri vagabondi tendono a tollerare questa razza curiosa nella speranza che una parte della sua fortuna mistica possa riflettersi anche su di loro.
Nomi maschili
	Antal, Boram, Evan, Jamir, Kaleb, Lem, Miro, Sumak.
Nomi femminili
	Anafa, Bellis, Etune, Filiu, Lissa, Marra, Rillka, Sistra, Yamyra.


Tratti Razziali
-----------------
:Caratteristiche: Des +2, Car +2, For –2

	Agili e caparbi, ma la loro bassa statura li rende più deboli degli altri.
:Taglia: Piccola 

	+1 alla CA e ai tiri per colpire, –1 a BMC e DMC, e +4 alle prove di :ref:`Furtività<furtività>`.
:Velocità: Lenta (6m)
:Temerarietà: +2 ai TS contro paura, che si cumula con quello conferito da fortuna halfling.
:Fortuna Halfling: +1 a tutti i TS.
:Sensi Acuti: +2 alle prove di :ref:`Percezione<percezione>`.
:Piede Saldo: +2 alle prove di :ref:`Acrobazia<acrobazia>` e :ref:`Scalare<scalare>`.
:Familiarità nelle Armi: Fionda e qualsiasi arma con la parola *halfling* nel nome vale come arma da guerra.
:Linguaggi: Gli halfling iniziano il gioco parlando Comune e Halfling e possono scegliere tra Elfico, Gnomesco, Goblin e Nanico a seconda del loro Mod Int.

.. _mezzelfi:

------------
Mezzelfi
------------
.. figure:: Immagini/RazzeMezzelfo.png
    :align: left
    :width: 200

Gli elfi hanno per lungo tempo attratto gli sguardi bramosi delle altre razza. La loro longevità, affinità magica e innata grazia contribuiscono tutte all’ammirazione o talvolta all’amara invidia di chi sta loro vicino. Di tutte le loro caratteristiche, tuttavia, nessuna è più attraente per gli umani della loro bellezza. 

Da quando le due razze sono entrate in contatto tra loro, gli umani hanno sostenuto gli elfi come modelli di perfezione fisica, vedendoli come versioni idealizzate di sè
stessi. Da parte loro, molti elfi trovano gli umani attraenti malgrado i loro modi alquanto barbari, spinti dalla passione e dall’impeto con cui i membri di questa razza più giovane vivono la propria breve esistenza. 

A volte questa infatuazione reciproca porta a rapporti romantici, solitamente di breve durata; questi incontri segreti portano di solito alla nascita dei mezzelfi,
una razza che discende da due culture, ma non è erede di nessuna. 

I mezzelfi possono riprodursi tra loro, ma persino questi mezzelfi “di sangue puro” tendono ad essere visti egualmente come bastardi dagli umani e dagli elfi.

Descrizione fisica
	I mezzelfi sono più alti degli umani ma più bassi degli elfi.
	
	Esili e avvenenti come gli elfi, la loro carnagione è data dal loro sangue umano. Hanno le orecchie a punta come gli elfi, ma più arrotondate e meno evidenti. 
	Gli occhi sono simili a quelli umani e dai colori esotici, dall’ambrato o viola al verde smeraldo e al blu scuro.

Società
	La mancanza di una patria e di una cultura unificate costringe i mezzelfi a rimanere versatili, in grado di conformarsi con ogni ambiente. 

	Mentre sono spesso attraenti per entrambe le razze per gli stessi motivi dei loro genitori, i mezzelfi raramente si trovano a loro agio con gli umani o gli elfi, dato che entrambe le razze vedono troppa prova dell’altra in essi. 

	Questa mancanza di accettazione pesa parecchio su molti mezzelfi, mentre altri sono più motivati dal loro status unico, vedendo questa mancanza di cultura formalizzata come l’ultimo baluardo di libertà. Di conseguenza, i mezzelfi sono incredibilmente adattabili, capaci di cambiare la propria mentalità e le proprie capacità in quello che le società si aspettano da loro.

Rapporti
	Un mezzelfo comprende la solitudine e sa che il carattere è spesso un prodotto dell’esperienza di vita piuttosto che della razza. Pertanto, i mezzelfi sono spesso aperti alle amicizie ed alle alleanze con le altre razze e non si basano quasi mai solo sulla prima impressione quando devono farsi un’opinione su nuove conoscenze.
Allineamento e Religione
	L’isolamento dei mezzelfi influisce fortemente sul loro carattere e sulle loro idee. Non sono portati per natura alla crudeltà, né a mescolarsi e piegarsi alle convenzioni sociali, e perciò la maggior parte dei mezzelfi ha allineamento caotico buono. 

	La mancanza di una cultura unitaria non spinge quasi mai i mezzelfi verso la religione, ma quelli che lo fanno, in genere, seguono la fede comune della loro patria.
Avventurieri
	I mezzelfi tendono a viaggiare spesso alla ricerca di un posto che potrebbero finalmente chiamare casa. 

	Il desiderio di dimostrarsi degni di una comunità e di stabilire un’identità personale, o persino una discendenza, guida molti avventurieri mezzelfi a vite spinte dal valore e dal coraggio.

Nomi Maschili
	Calathes, Encinal, Kyras, Narciso, Quiray, Satinder, Seltyiel, Zirul.
Nomi Femminili
	Cathran, Elsbeth, Iandoli, Kieyanna, Lialda, Maddela, Reda, Tamarie.


Tratti Razziali
-----------------
:Caratteristiche: +2 ad un punteggio a scelta

	Prova della loro natura diversa.
:Taglia: Media
:Velocità: Normale (9m)
:Visione Crepuscolare: Gli elfi vedono :ref:`due volte più lontano<visione_crepuscolare>` di un umano in condizioni di luce scarsa.
:Adattabilità: Talento bonus :ref:`Abilità Focalizzata<tal_abilità_focalizzata>`.
:Sangue Elfico: I mezzelfi contano sia come elfi che come umani per qualsiasi effetto relativo alla razza.
:Immunità degli elfi: Immunità a effetti di sonno magico e +2 ai TS contro incantesimi ed effetti di ammaliamento.
:Sensi Acuti: +2 alle prove di :ref:`Percezione<percezione>`.
:Doti Innate: Si possono scegliere 2 :ref:`Classi Preferite<classe_preferita>`.
:Linguaggi: I mezzelfi iniziano il gioco parlando Comune ed Elfico e possono scegliere tutti i linguaggi che vogliono, tranne i linguaggi segreti come il Druidico, a seconda del Mod Int.

.. _mezzorchi:

--------------
Mezzorchi
--------------

.. figure:: Immagini/RazzeMezzorco.png
    :align: right
    :width: 250

I mezzorchi sono delle mostruosità, le loro nascite tragiche il risultato di perversione e violenza, o almeno è così che le altre razze li vedono. 

Essi sono raramente il risultato di unioni d’amore e per questo sono costretti a crescere in fretta tra mille difficoltà, lottando spesso per difendere se stessi o per farsi un nome. 

Temuti, mal visti e maltrattati, i mezzorchi riescono con crescente costanza a sorprendere i loro detrattori con gesta eroiche ed inattesa saggezza, benché a volte sia più facile farlo spaccando qualche cranio.

Descrizione Fisica
	Alti da 1,8m a 2,1m, con fisico potente e pelle verdastra o grigia, i loro canini crescono spesso piuttosto lunghi fino a sporgere dalle loro bocche e queste “zanne”, unite con una fronte ampia e le orecchie un po’ a punta, danno loro quell’aspetto notoriamente “bestiale”. 

	I mezzorchi possono essere impressionanti, pochi li descrivono come belli.

Società
	Fisicamente più deboli degli orchi, tendono ad essere temuti o attaccati da molti umani che non si preoccupano di fare distinzione fra orchi e mezzosangue, eppure, sebbene non siano perfettamente accettati, i mezzorchi nelle società civilizzate tendono ad essere stimati per il loro valore marziale ed i capi degli orchi sono noti per generarli intenzionalmente, poiché i mezzosangue compensano regolarmente la loro mancanza di suprema forza fisica con una spiccata astuzia ed aggressività, diventando ottimi capi e rispettati strateghi.

Rapporti
	Una vita di persecuzioni rende il mezzorco medio prudente e facile agli scoppi d’ira, ma chi riesce a far breccia in questo aspetto esteriore selvaggio potrebbe trovare una dose ben nascosta di empatia. :ref:`elfi` e :ref:`nani` tendono a non accettare i mezzorchi, vedendo in loro una somiglianza troppo forte con i loro nemici razziali, ma le altre razze sono molto più comprensive.

	Le società **umane** con pochi problemi con gli orchi tendono ad essere più accomodanti e i mezzorchi diventano spesso mercenari e guardie.

Allineamento e Religione
	Costretti a vivere fra i brutali orchi o come emarginati nelle terre civilizzate, i mezzorchi sono spesso amareggiati, violenti e solitari. 

	Sono portati facilmente al male, ma non sono malvagi di natura: piuttosto, molti sono di allineamento caotico neutrale, avendo imparato dalla lunga esperienza che non c’è vantaggio nel fare qualcosa se questo non dà benefici personali diretti. 

	Quando si preoccupano di adorare gli dèi, preferiscono quelli che promuovono la guerra o la forza individuale, come Gorum, Cayden Cailean, Lamashtu e Rovagug.

Avventurieri
	Fieri ed indipendenti, molti mezzorchi intraprendono la via dell’avventura per necessità, cercando di fuggire da un passato doloroso o di migliorare il loro destino attraverso le proprie forze. 

	Altri, più ottimisti o disperati di essere accettati, indossano il mantello dei crociati per dimostrare il loro valore al mondo.

Nomi Maschili
	Ausk, Davor, Hakak, Kizziar, Makoa, Nesteruk, Tsadok. 
Nomi Femminili
	Canan, Drogheda, Goruza, Mazon, Shirish, Tevaga, Zeljka.

Tratti Razziali
-----------------
:Caratteristiche: +2 ad un punteggio a scelta

	Prova della loro natura diversa.
:Taglia: Media
:Velocità: Normale (9m)
:Scurovisione: I mezzorchi possono vedere al buio fino a 18 metri.
:Intimidazione: +2 alle prove di :ref:`Intimidire<intimidire>` grazie alla loro natura spaventosa.
:Sangue Orchesco: I mezzorchi contano sia come orchi che come umani per qualsiasi effetto relativo alla razza.
:Ferocia degli Orchi: Una volta al giorno, quando un mezzorco è ridotto a meno di zero punti ferita ma non è morto, può continuare a combattere per un round ulteriore come se fosse inabile. 

	Alla fine del suo turno successivo, a meno che non venga riportato a punti ferita positivi, perde i sensi e inizia a morire.
:Familiarità nelle Armi: Ascia bipenne e falchion e qualsiasi arma con la parola *orchesco* nel nome vale come arma da guerra.
:Linguaggi: I mezzorchi iniziano il gioco parlando Comune e Orchesco e possono scegliere tra Abissale, Draconico, Gigante, Gnoll e Goblin a seconda del Mod Int.

.. _nani:

-------------
Nani
-------------

.. figure:: Immagini/RazzeNano.png
    :align: left
    :width: 250

I nani sono una razza stoica ma severa, nascosta in città scavate nel cuore delle montagne e determinata ferocemente a respingere le razzie delle creature selvagge come orchi e goblin. 

Più di qualunque altra razza, i nani hanno acquisito una reputazione come artigiani taciturni e seri. Si può dire che la storia dei nani abbia forgiato l’indole introversa di molti nani, data la loro abitudine a vivere su picchi quasi inaccessibili o in scuri e pericolosi regni sotterranei, costantemente in guerra
con giganti, goblin ed altri orrori simili.

Descrizione Fisica
	Razza bassa e tarchiata, raggiungono un’altezza massima di circa 30 cm inferiore a quella degli umani, con una corporatura robusta e compatta che dà loro un aspetto massiccio. 

	Sia i maschi che le femmine portano orgogliosamente i capelli lunghi e gli uomini decorano spesso le barbe con vari generi di fermagli e trecce intricate; un nano maschio ben rasato è un segno sicuro di pazzia, o peggio: nessuno che sia familiare con la loro razza si fiderebbe mai di un nano senza barba.

Società
	Le grandi distanze fra le loro cittadelle tra i monti giustificano le molteplici differenze culturali esistenti all’interno della società dei nani, ma tutti sono noti per il loro amore nel lavorare la pietra, la loro passione per l’arte e l’architettura basate sulla pietra ed il metallo, ed un odio feroce verso giganti, orchi e goblinoidi.

Rapporti
	I nani e gli orchi hanno abitato a lungo vicini, e la loro è una storia di odio reciproco vecchio quanto entrambe le razze, quindi generalmente diffidano dei **mezzorchi** e li evitano.
	Trovano **halfling**, **elfi** e **gnomi** troppo delicati, volubili o “graziosi” per essere degni di rispetto. È con gli **umani** che i nani condividono un più forte legame, dato che la natura industriosa e la brama sincera degli umani si avvicinano di più a quelle dell’ideale nanico.

Allineamento e Religione
	Guidati da onore e tradizione, sono spesso derisi come alteri, ma hanno un forte sentimento di amicizia e giustizia e chi si guadagna la loro fiducia capisce che, anche se lavorano strenuamente, scherzano ancora più accanitamente, specie davanti ad una buona birra. 

	I nani sono per lo più legali buoni e preferiscono adorare le divinità i cui principi si abbinano ai loro tratti e Torag è la preferita fra i nani, sebbene anche Abadar e Gorum siano scelte comuni.

Avventurieri
	Anche se i nani avventurieri sono rari, si possono trovare in molte regioni del mondo: spesso abbandonano i confini dei loro rifugi per cercare gloria per i loro clan, ricchezze con cui abbellire le case-fortezze della loro famiglia, o per rivendicare cittadelle di nani preda dei nemici razziali. 

	Le tecniche di guerra dei nani sono caratterizzate spesso da combattimenti in stretti cunicoli e in mischia, e per questo i nani tendono a diventare guerrieri e barbari.

Nomi Maschili
	Dolgrin, Grunyar, Harsk, Kazmuk, Morgrym, Rogar.

Nomi Femminili
	Agna, Bodill, Ingra, Kotri, Rusilka, Yangrit.

Tratti Razziali
-----------------
:Caratteristiche: Cos +2, Sag +2, Car –2

	I nani sono sia robusti che saggi, ma un po’ burberi.
:Taglia: Media
:Velocità: Lenta (6m)
:Lenti e saldi: La velocità non viene mai modificata dall’armatura o dall’ingombro.
:Scurovisione: I nani possono vedere al buio fino a 18 metri.
:Addestramento Difensivo: +4 alla CA contro i mostri del sottotipo gigante.
:Cupidigia: +2 alle prove di :ref:`valutare` effettuate per determinare il prezzo di beni non di natura magica che contengono metalli preziosi o gemme.
:Esperto Minatore: +2 alle prove di :ref:`percezione` per individuare strane opere in muratura, come trappole e porte segrete situate su muri o pavimenti di pietra.

	Ogni volta che i nani passano a meno di 3 metri da queste opere possono effettuare tale prova, anche se non le stanno effettivamente cercando.
:Odio: +1 ai tiri per colpire contro creature umanoidi del sottotipo orco e goblin grazie allo speciale addestramento verso questi odiati nemici.
:Resistenza: +2 ai TS contro veleni, incantesimi e capacità magiche.
:Stabilità: +4 alla DMC quando devono resistere ai tentativi di spingere e sbilanciare mentre sono saldi sul terreno.
:Familiarità nelle armi: Asce da guerra, martelli da guerra e picconi pesanti e qualsiasi arma con la parola *nanico* nel nome vale come arma da guerra.
:Linguaggi: I nani iniziano il gioco parlando Comune e Nanico e possono scegliere fra Gigante, Gnomesco, Goblin, Orchesco, Terran, Sottocomune a seconda del loro Mod Int.

.. _umani:

----------
Umani
----------

.. figure:: Immagini/RazzeUmano.png
    :align: right
    :width: 200

Gli umani possiedono un’eccezionale spinta ed una grande capacità di resistere ed espandersi, permettendogli di essere la razza dominante nel mondo. I loro imperi e nazioni sono ampi e in espansione ed i cittadini di queste società scolpiscono i loro
nomi con la forza delle armi ed il potere dei loro incantesimi.

L’umanità è meglio caratterizzata dalla sua turbolenza e diversità e le culture umane vanno dalle tribù selvagge, ma dai rigidi codici d’onore, alle famiglie nobili decadenti e adoratrici di demoni nelle città più cosmopolite. 

La curiosità e l’ambizione umane trionfano spesso sulla predilezione per uno stile di vita sedentario e molti lasciano le loro case per esplorare i numerosi angoli dimenticati del mondo o per condurre potenti eserciti per conquistare i vicini, semplicemente perché possono.

Descrizione Fisica
	Varie quanto i climi del mondo: dalle tribù dalla carnagione scura dei continenti meridionali ai razziatori pallidi e barbari delle regioni settentrionali, gli umani possiedono una grande varietà di colori di pelle, corporature e lineamenti. 

	In linea generale, il colore di pelle degli umani presuppone una tonalità più scura più vicino all’equatore. 

Società
	Gran numero di governi, atteggiamenti e stili di vita.

	Benché le più vecchie culture umane traccino le loro storie migliaia di anni nel passato, quando vengono paragonate alle società delle razze comuni come elfi e nani, sembra che le società umane siano in una condizione di cambiamento continuo mentre gli imperi si dividono e nuovi regni prendono il posto dei vecchi. Gli umani sono noti generalmente per la loro flessibilità, ingegnosità ed ambizione.

Rapporti
	Fecondi e il con un impeto e numero che li stimola spesso al contatto con altre razze durante i periodi di espansione e di colonizzazione territoriali. 

	In molti casi, questo porta alla violenza ed alla guerra, tuttavia gli umani sono egualmente rapidi a perdonare e forgiare alleanze con le razze che non tentano di pareggiarli o superarli in violenza. 

	Fieri, a volte al punto di essere arroganti, gli umani considerano i **nani** come ubriaconi avari, gli **elfi** come sciocchi e frivoli, gli **halfling** come ladri e vigliacchi, gli **gnomi** come maniaci e bizzarri, e **mezzelfi e mezzorchi** come imbarazzanti, ma le diversità intrinseche nella razza rendono gli umani abbastanza aperti ad accettare ugualmente gli altri per quello che sono.

Allineamento e Religione
	L’umanità è forse la più eterogenea di tutte le razze comuni, con una capacità di perseguire il male e il bene fino al massimo grado. Alcuni si radunano in grandi orde barbariche, mentre altri costruiscono enormi città che si espandono per diversi chilometri. 

	Presi complessivamente, gli umani sono di allineamento neutrale, tuttavia tendono generalmente ad unirsi in nazioni e civiltà con allineamenti specifici. Gli umani inoltre hanno la gamma più vasta di divinità e religioni, non essendo legati come le altre razze alla tradizione e desiderosi di rivolgersi verso chiunque offra loro gloria o protezione. 

	Hanno persino adottato divinità come Torag o Calistria, che per millenni erano state identificate con le razze più antiche e dato che l’umanità continua a svilupparsi e prosperare, nuove divinità hanno cominciato ad affiorare dalle loro leggende in perenne espansione.

Avventurieri
	L’ambizione da sola guida gli innumerevoli umani e per molti l’avventura è un mezzo per un proprio fine, possa essere ricchezza, potere, status sociale o conoscenza arcana. Alcuni perseguono semplicemente la carriera avventurosa per l’emozione del pericolo. 

	Gli umani provengono da miriadi di regioni e ambienti e come tali possono adattarsi ad ogni ruolo in un gruppo.

Nomi
	La diversità dell’umanità ha portato ad un insieme quasi infinito di nomi. Gli umani di una tribù barbara settentrionale hanno nomi molto differenti da quelli che provengono da una nazione subtropicale di marinai e commercianti.
	
	In quasi tutto il mondo gli umani parlano il Comune, ma i loro nomi sono vari quanto le loro credenze e il loro aspetto.

Tratti Razziali
-----------------
:Caratteristiche: +2 ad un punteggio a scelta
	
	Prova della loro natura diversa.
:Taglia: Media
:Velocità: Normale (9m)
:Talento Bonus: Gli umani scelgono un talento addizionale al 1° livello.
:Esperti: 1 grado di abilità aggiuntivo al 1° livello e 1 ogni volta che avanzano di livello.
:Linguaggi: Gli umani iniziano il gioco parlando Comune e possono scegliere tutti i linguaggi che vogliono, tranne quelli segreti come il Druidico, a seconda del loro Mod Int.