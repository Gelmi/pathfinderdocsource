***************
Oggetti Magici
***************
TODO

.. contents:: Contenuti
	:local:
	:depth: 2

Lista per Tipo
==================

.. contents:: Contenuti
	:local:
	:depth: 1

Anelli
^^^^^^^^^^^^^^^^^


Protezione
"""""""""""

:Aura: Abiurazione Debole
:Costo Costruzione:
    * 1000 mo (+1)
    * 4000 mo (+2)
    * 9000 mo (+3)
    * 16000 mo (+4)
    * 25000 mo (+5)
:Livello Incantatore: 5°
:Peso(M): N/A
:Prezzo:
    * 2000 mo (+1)
    * 8000 mo (+2)
    * 18000 mo (+3)
    * 32000 mo (+4)
    * 50000 mo (+5)
:Requisiti Costruzione: :ref:`Forgiare Anelli<tal_forgiare_anelli>`, :ref:`Scudo della Fede<inc_scudo_della_fede>`, LI >= 3 + Bonus CA da assegnare.
:Slot: Anello

Questo anello offre costantemente una protezione magica sotto forma di bonus di deviazione alla CA che varia da +1 a +5.


