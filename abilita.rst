
.. _abilità:

=================================================
Abilità
=================================================

.. figure:: Immagini/AbilitaImmagine.png

.. contents:: Contenuti
	:local:
	:depth: 3

Capacità fondamentali che il personaggio possiede, acquisisce e sviluppa man mano che avanza di livello.

Acquisizione
=====================

.. _tabella_gradi_abilita:

.. sidebar:: Abilità per livello

		A questi va sommato il modificatore intelligenza.

	.. csv-table::
	   :header: Classi,Gradi
	   :widths: 10, 5

	   	Barbaro, 4
		Bardo, 5
		Chierico, 2
		Druido, 4 
		Guerriero, 2
		Ladro, 8  
		Mago, 2  
		Monaco, 4 
		Paladino, 2
		Ranger, 6  
		Stregone, 2

Ad ogni livello, i personaggi ottengono gradi di abilità in base alle classi più il loro modificatore di Intelligenza, come in tabella. 

Investire un grado in un’abilità rappresenta una misura dell’addestramento in quell’abilità. 

**Non si possono avere più gradi in un’abilità rispetto ai propri Dadi Vita totali.**

Ogni classe ha un certo numero di abilità preferite, chiamate abilità di classe 
(bonus +3 a tutte le abilità di classe in cui sono stati messi dei gradi, non cumulabile). 

Gli **umani** ottengono 1 grado di abilità aggiuntivo per livello di
classe. 

I personaggi che avanzano di livello in una :ref:`classe preferita <classe_preferita>`
hanno l’opzione di guadagnare 1 punto ferita addizionale o
1 grado di abilità. 

------------------
Prove di abilità
------------------
Per determinare il successo quando si usa un’abilità, bisogna effettuare una **prova di abilità**.

Ogni grado di abilità garantisce bonus +1 alle prove
effettuate per usare quell’abilità. 

Si tira 1d20 e si aggiungono i gradi e il modificatore
della caratteristica appropriata al risultato della prova, più l'eventuale *bonus di classe +3*. 
Se il risultato della prova è uguale o maggiore rispetto alla Classe Difficoltà associata (o CD) all’impresa la prova riesce. Se inferiore fallisce. 
In alcuni casi ci sono vari livelli di successo e fallimento in base a quanto la prova è maggiore o minore della CD. 

Alcune prove sono contrapposte da una prova del bersaglio. Se il risultato della propria è maggiore di quella del bersaglio si ha successo.

Si può sempre tentare di usare l’abilità applicando solo il bonus (o la penalità) del modificatore di caratteristica.

**Ulteriori modifiche** possono derivare da razza, privilegi di classe, equipaggiamento, incantesimi o oggetti magici, e così via. 

-----------------------
Prendere 10 o 20
-----------------------

Se non si è minacciati o automaticamente distratti è possibile usare un’abilità in condizioni ancora più favorevoli

:Prendere 10: 
	
	Se non si è in immediato pericolo o distratti, è possibile prendere 10 alla prova.

	Invece di tirare 1d20, si calcola il risultato come se si fosse ottenuto un risultato di 10. Per molti semplici compiti, prendere 10 potrebbe già costituire un successo automatico alla prova. Distrazioni o minacce (come un combattimento) rendono impossibile prendere 10. 

	Prendere 10 è particolarmente utile quando un tiro elevato non sarebbe di grande aiuto.

:Prendere 20: 

	Quando si ha molto tempo, non si è sotto minaccia o distrazioni, e quando **l’abilità** che deve essere tentata **non comporta penalità di fallimento**, è possibile prendere 20.

	Si presume che ritirando il d20 varie volte, alla fine si otterrebbe 20.

	Prendere 20 significa che si prova fino a quando non si riesce e si assume che si fallisca molte volte prima di riuscire, quindi si impiega venti volte il tempo necessario per (di solito 2 minuti per un’abilità che normalmente può essere provata in 1 round o meno).

	Se si tentasse di prendere 20 su un’abilità che comporta una penalità, si incorrerebbe automaticamente in essa.

	Le abilità in cui normalmente si può *prendere 20* comprendono :ref:`Artista della Fuga<artista_della_fuga>`, :ref:`Disattivare Congegni<disattivare_congegni>` (se usata per scassinare serrature) e :ref:`Percezione<percezione>` (se usata per cercare trappole).

:Prove di caratteristica:

	La regola di prendere 10 e prendere 20 si applica normalmente anche per le prove di caratteristica.

:Prove di livello dell’incantatore:
	
	La regola di prendere 10 e prendere 20 non si applica normalmente anche per le prove di livello dell’incantatore o alle prove di concentrazione.

----------------------
Aiutare un altro
----------------------

Si può aiutare un altro personaggio a ottenere un successo nella sua prova di abilità facendone uno sforzo collettivo. 

**Se si ottiene 10 o più** alla prova, il personaggio che si sta aiutando riceve bonus +2 alla sua prova. (Non è possibile prendere 10 in una prova di aiutare un altro). 

Nei casi in cui un’abilità abbia dei limiti nei confronti di chi possa ottenere certi risultati, come cercare di scassinare una serratura usando Disattivare Congegni, un personaggio non può collaborare per conferire un bonus ad un’impresa che non è in grado di svolgere da solo. 

Il GM potrebbe imporre altre restrizioni all’azione di aiutare un altro in base alle situazioni specifiche.

Descrizione
========================

.. contents:: Elenco Abilità:
	:local:

Questa sezione descrive ogni abilità, compresi gli usi più comuni e i modificatori tipici. 

I personaggi, qualche volta, possono utilizzare le abilità per scopi diversi da quelli indicati, a discrezione del GM. 
Quello che segue è il formato delle descrizioni delle abilità.

:Nome dell’abilità: 

	Il nome dell’abilita include, oltre al nome dell’abilita, le seguenti informazioni.

	* Caratteristica chiave: 
		L’abbreviazione della caratteristica il cui modificatore si applica alla prova di abilità.
	* Solo con addestramento: 
		Se questa annotazione è riportata nella riga del nome dell’abilità, è necessario avere almeno 1 grado nell’abilità per utilizzarla; so no l’abilità può essere utilizzata senza addestramento (con grado 0).
		Se qualche notazione particolare si applica in riferimento all’uso con o senza addestramento, è inserita nella sezione Senza addestramento (vedi sotto).
	* Penalità di armatura alla prova: 
		Se questa annotazione è riportata nella riga del nome dell’abilità, una penalità di armatura (vedi :ref:`Armature<armature>`) si applica alle prove che riguardano questa abilità.
:Descrizione: 

	Una descrizione generale di ciò che accade quando si utilizza quella abilità.

:Prova: 
	
	Cosa si può fare con una abilità effettuata la sua Classe Difficoltà (CD).

:Azione: 

	Il tipo di azione che richiede l’uso dell’abilità o l’ammontare di tempo necessario per effettuare la prova.

:Ritentare: 

	Qualsiasi condizione che si applica ai tentativi successivi per utilizzare in modo efficace l’abilità. 
	Se questo paragrafo è assente, l’abilità può essere ritentata senza alcuna penalità intrinseca, a parte l’ulteriore tempo richiesto.

:Speciale:
	
	Qualsiasi notazione particolare.

:Restrizioni: 
	
	Il pieno utilizzo di certe abilità è ristretto ai personaggi di certe classi. 

:Senza addestramento: 

	Questa voce indica cosa si può fare senza avere almeno 1 grado nell’abilità. Se non compare, l’abilità funziona normalmente per i personaggi senza addestramento, se può essere usata senza addestramento.

.. figure:: Immagini/Abilita.png
    :align: center

    Un elenco delle abilità delle principali classi (``C``), se possono essere utilizzate senza un grado abilità (``S.A.``) e il relativo modificatore caratteristica (``Car.``). 

.. _acrobazia:

-----------------------------------
Acrobazia (Des - Penalità Armatura)
-----------------------------------
.. contents:: In breve:
	:local:

.. figure:: Immagini/Acrobazia.png
    :align: right
    :width: 300

Con questa abilità è possibile mantenere l’equilibrio mentre si attraversano superfici strette o pericolose, così come tuffarsi, rotolare, fare capriole o salti mortali per evitare attacchi e superare ostacoli.

Prova
-------
Si può usare :ref:`Acrobazia<acrobazia>` per muoversi su una superficie stretta o precaria senza cadere alla metà della propria velocità lungo la superficie. Solo una prova per round. 

.. _acrobazia_superficie:

.. figure:: Immagini/AcrobaziaSuperficie.png
    :align: left
    :width: 350

In :ref:`tabella<acrobazia_superficie>` sono riportate le CD base, che sono alterate dai :ref:`modificatori di Acrobazia<acrobazia_modificatori>`. Con l'asterisco sono indicate le situazioni in cui non è prevista una prova a meno che i modificatori non aumentino la CD a 10 o più.
Se si usa :ref:`Acrobazia<acrobazia>` in questo modo, si è considerati impreparati e si perde il bonus di Destrezza alla CA. 

.. Warning::
	Se si subisce danno mentre si usa :ref:`Acrobazia<acrobazia>`, bisogna effettuare un’altra prova.

Ci si può muovere attraverso un quadretto minacciato usando :ref:`Acrobazia<acrobazia>` senza provocare :ref:`Attacchi di Opportunità<azione_attacco_di_opportunità>` da parte di un avversario con una **CD = DMC + 2 * # nemici evitati in un round**. Bisogna però muoversi a metà della propria velocità base o a piena velocità aumentando la CD della prova di 10. 

Non si può usare :ref:`Acrobazia<acrobazia>` per muoversi vicino ad avversari se la propria velocità è ridotta a causa dell’ingombro dovuto ad un carico medio/pesante o per un’armatura media/pesante. Se una capacità permette di muoversi alla propria piena velocità in tali condizioni, si può usare :ref:`Acrobazia<acrobazia>` per muoversi vicino agli avversari. Si può usare Acrobazia in questo modo mentre si è proni, ma per farlo occorre un’:ref:`Azione di Round Completo<azione_di_round_completo>` per muoversi di 1,5 metri, e la CD viene aumentata di 5.

Lo stesso si può fare per attraversare uno spazio occupato da un nemico, aumentando di 5 la CD.

.. _acrobazia_salto:

.. figure:: Immagini/AcrobaziaSalto.png
    :align: right
    :width: 350

Si può usare questa abilità anche per effettuare salti o per attutire una caduta con una CD base per effettuare un salto di 5 per ogni 1.5m di distanza da attraversare e 4 per ogni 30cm di altezza da raggiungere. 
Queste CD raddoppiano se non si hanno almeno 3 metri di spazio per prendere la rincorsa. 

Gli unici modificatori ad :ref:`Acrobazia<acrobazia>` che si applicano sono quelli riguardanti la superficie da cui si salta. 

Se si fallisce la prova di 4 o meno, si può tentare un TS sui Riflessi con CD 20 per afferrare l’estremità opposta, ma se al contrario si fallisce di 5 o più, si sbaglia il salto e si cade (proni, in caso di salto in alto). 

Le creature con una velocità base sul terreno sopra i 9 metri ricevono bonus razziale +4 alle prove di :ref:`Acrobazia<acrobazia>` effettuate per saltare per ogni 3 metri della propria velocità sopra i 9 metri. 
Al contrario sotto i 9 metri di velocità si riceve penalità razziale –4 alle prove di :ref:`Acrobazia<acrobazia>` effettuate per saltare per ogni 3 metri della propria velocità sotto i 9 metri. 

Nessun salto permette di eccedere il proprio movimento massimo per round. In caso di salto con rincorsa, il risultato della prova di :ref:`Acrobazia<acrobazia>` indica la distanza percorsa nel salto. Dimezzare questo risultato per un salto da fermo in lungo per determinare la distanza alla quale si atterra.

.. _acrobazia_modificatori:

.. figure:: Immagini/AcrobaziaModificatori.png
    :align: left
    :width: 350

Se si salta in basso intenzionalmente, anche in conseguenza di un salto fallito, una prova di :ref:`Acrobazia<acrobazia>` con CD 15 permette di ignorare i primi 3 metri di caduta, sebbene si finisca sempre proni se si subiscono danni da caduta. Vedi le regole relative alla :ref:`caduta<caduta>` per maggiori dettagli.

I modificatori delle CD si applicano a tutte le prove di :ref:`Acrobazia<acrobazia>` e si sommano l’uno con l’altro, ma solo quello più grave per ogni diversa condizione che si applica alla prova.

Azione 
-------
Nessuna.

Una prova di :ref:`Acrobazia<acrobazia>` è effettuata come parte di un’altra azione o come reazione ad una situazione.

Speciale
---------
Se si hanno 3 o più gradi in :ref:`Acrobazia<acrobazia>` si ottiene bonus di schivare +3 alla CA quando si combatte sulla difensiva invece del solito +2, e bonus di schivare +6 alla CA, quando si esegue una :ref:`Difesa Totale<azione_difesa_totale>` invece del solito +4. 

Se si possiede il talento :ref:`Acrobatico<tal_acrobatico>`, si riceve un bonus alle prove di Acrobazia.

.. _addestrare_animali:

-------------------------------------------
Addestrare Animali (Car - Addestramento)
-------------------------------------------
.. contents:: In breve:
	:local:

.. figure:: Immagini/AddestrareAnimali.png
    :align: right
    :width: 300

    Elenco di azioni con relativi CD di :ref:`Addestrare Animali<addestrare_animali>`.

Con questa abilità si possono addestrare gli animali, insegnare loro dei semplici comandi da eseguire, o addomesticarli.

Prova
--------
La CD dipende da ciò che si sta cercando di fare.

.. _add_gestire:

*******************
Gestire
*******************
Questa azione riguarda dare ordine ad un animale di eseguire un’azione o comando che già conosce.
Se la prova riesce, l’animale svolge l’impresa o il comando con la sua
azione successiva.

Se l’animale è ferito o ha subito danni non letali o ai punteggi di caratteristica, la CD aumenta di 2.

.. _add_spingere:

*****************************
"Spingere"
*****************************
Spingere un animale significa fargli svolgere un’impresa o comando che non conosce ma che è fisicamente in grado di compiere, compreso il fare svolgere ad un animale una marcia forzata od obbligarlo ad andare veloce per più di 1 ora tra un turno di riposo e un altro. Se la prova riesce, l’animale svolge l’impresa o il comando con la sua azione successiva.

Se l’animale è ferito o ha subito danni non letali o ai punteggi di caratteristica, la CD
aumenta di 2. 

***************************
Insegnare Comandi
***************************
Si può insegnare ad un animale un comando specifico con una settimana di lavoro e una prova riuscita di :ref:`Addestrare Animali<addestrare_animali>` alla CD indicata. 

Un animale con un punteggio di Intelligenza di 1 può apprendere un massimo di tre comandi, mentre con un punteggio pari a 2 può impararne fino a 6 sei.

Possibili comandi (e le CD associate) comprendono, ma non sono necessariamente limitati ai seguenti:

.. _add_annusa:

Annusa (CD 20)
	L’animale segue le tracce dell’odore che gli viene fatto annusare, ma deve possedere :ref:`Fiuto<capacità_fiuto>`.

.. _add_attacca:

Attacca (CD 20)
	L’animale attacca i nemici: si può indicare una particolare creatura che si desidera far attaccare dall’animale e questo, se è in grado, eseguirà il compito. 

	Normalmente, un animale attaccherà solo umanoidi, umanoidi mostruosi, giganti o altri animali ed insegnare ad un animale ad attaccare tutte le creature (comprese creature innaturali come non morti e aberrazioni) conta come due comandi.

.. _add_cerca:

Cerca (CD 15)
	L’animale si muove in un’area alla ricerca di qualsiasi cosa palesemente vivente o animata.

.. _add_difendi:

Difendi (CD 20)
	L’animale difende il personaggio specificato (o è pronto a difenderlo se non c’è nessuna minaccia evidente), anche senza ricevere alcun ordine.

.. _add_fermo:

Fermo (CD 15)
	L’animale resta sul posto, aspettando il ritorno del personaggio senza aggredire le creature di passaggio e difendendosi se viene attaccato.

.. _add_indietro:

Indietro (CD 15)
	L’animale interrompe l’attacco o si ritira in qualche modo. 

	.. Caution::
		Un animale che non conosce questo comando continua a combattere finché non deve fuggire (a causa di ferite, effetti di paura o simili) o il suo avversario non viene sconfitto.

.. _add_intrattieni:

Intrattieni (CD 15)
	L’animale esegue diversi semplici comandi come mettersi seduto, rotolarsi, ruggire o abbaiare e così via.

.. _add_lavora:

Lavora (CD 15)
	L’animale tira o spinge un carico medio o pesante.

.. _add_proteggi:

Proteggi (CD 20)
	L’animale resta sul posto e impedisce ad altri di avvicinarsi.

.. _add_recupera:

Recupera (CD 15)
	L’animale si reca a recuperare una cosa. 

	Se non gli viene indicato un oggetto specifico, l’animale recupera un oggetto a caso.

.. _add_segui:

Segui (CD 15)
	L’animale segue il personaggio da vicino, anche in luoghi dove normalmente non si recherebbe.

.. _add_vieni:

Vieni (CD 15)
	L’animale torna dal padrone, anche se normalmente non lo farebbe.

**********************************
Addestrare per un Compito Generico
**********************************
Piuttosto che insegnare ad un animale singoli comandi, lo si può semplicemente addestrare ad un compito generico: essenzialmente, il compito dell’animale rappresenta una serie selezionata di comandi conosciuti che rientrano in uno schema comune, come :ref:`Fare la Guardia<add_proteggere>` o :ref:`Lavori di Fatica<add_lavori_pesanti>`.

L’animale deve soddisfare tutti i normali prerequisiti per tutti i comandi compresi nel pacchetto di addestramento e se il pacchetto comprende più di tre comandi, l’animale deve avere un punteggio di Intelligenza pari a 2 o più.

Un animale può essere addestrato per un solo compito generico, e se la creatura è in grado di apprendere ulteriori comandi (oltre quelli inclusi nel suo compito generico) può farlo. 

Addestrare un animale per un compito richiede meno prove dell’insegnare singoli comandi, ma non meno tempo.

Cacciare (CD 20)
	Un animale addestrato a cacciare conosce i comandi :ref:`Annusa<add_annusa>`, :ref:`Attacca<add_attacca>`, :ref:`Cerca<add_cerca>`, :ref:`Indietro<add_indietro>`, :ref:`Recupera<add_recupera>`, :ref:`Segui<add_segui>`. 

	Insegnare ad un animale a cacciare richiede 6 settimane.

.. _add_cavalcare:

Cavalcare (CD 15)
	Un animale addestrato a portare un cavaliere conosce i comandi :ref:`Fermo<add_fermo>`, :ref:`Segui<add_segui>`, :ref:`Vieni<add_vieni>`. 

	Addestrare un animale ad essere cavalcato richiede 3 settimane.

.. _addestrato_al_combattimento:

Cavalcare in combattimento (CD 20)
	Un animale addestrato a portare un cavaliere in combattimento, conosce i comandi :ref:`Attacca<add_attacca>`, :ref:`Difendi<add_difendi>`, :ref:`Indietro<add_indietro>`, :ref:`Proteggi<add_proteggi>`, :ref:`Segui<add_segui>`, :ref:`Vieni<add_vieni>`.

	Addestrare un animale a cavalcare in combattimento richiede sei settimane, ma si può anche “migliorare” un animale addestrato a :ref:`Cavalcare<add_cavalcare>` in uno addestrato a cavalcare in combattimento impiegando 3 settimane e superando una prova di :ref:`Addestrare Animali<addestrare_animali>` con CD 20. 

	Il nuovo compito generico e comando rimpiazzano completamente il precedente compito dell’animale e qualsiasi comando conoscesse già. 

	.. Note::
		Cavalli e cani da galoppo sono già addestrati a portare un cavaliere in combattimento.

Combattere (CD 20)
	Un animale addestrato ad entrare in combattimento conosce i comandi :ref:`Attacca<add_attacca>`, :ref:`Fermo<add_fermo>` e :ref:`Indietro<add_indietro>`. 

	Addestrare un animale a combattere richiede 3 settimane.
Intrattenere (CD 15)
	Un animale addestrato ad intrattenere conosce i comandi :ref:`Fermo<add_fermo>`, :ref:`Intrattieni<add_intrattieni>`, :ref:`Recupera<add_recupera>`, :ref:`Segui<add_segui>`, :ref:`Vieni<add_vieni>`. 

	Addestrare un animale ad intrattenere richiede 5 settimane.

.. _add_lavori_pesanti:

Lavori pesanti (CD 15)
	Un animale addestrato a svolgere lavori pesanti conosce i comandi :ref:`Lavora<add_lavora>` e :ref:`Vieni<add_vieni>`.

	Addestrare un animale a svolgere lavori pesanti richiede 2 settimane.

.. _add_proteggere:

Proteggere (CD 20)
	Un animale addestrato a proteggere conosce i comandi :ref:`Attacca<add_attacca>`, :ref:`Difendi<add_difendi>`, :ref:`Indietro<add_indietro>`, :ref:`Proteggi<add_proteggi>`.

	Addestrare un animale a proteggere richiede 4 settimane.

.. _add_allevare:

************************************
Allevare un Animale Selvatico
************************************
Allevare un animale significa crescere una creatura selvatica fin da piccola per farla diventare addomesticata. 

Un allevatore può crescere fino a tre creature della stessa specie alla volta e un animale allevato con successo può apprendere dei comandi mentre viene cresciuto, oppure più tardi come un animale addomesticato.

Azione
-------
Varie. :ref:`Gestire<add_gestire>` un animale è un’:ref:`Azione di Movimento<azione_movimento>`, mentre :ref:`Spingere<add_spingere>` un animale è un’:ref:`Azione di Round Completo<azione_di_round_completo>`. (Un :ref:`Druido` o :ref:`Ranger` può :ref:`Gestire<add_gestire>` il suo :ref:`Compagno_Animale_Druido` con un’:ref:`Azione Gratuita<azione_gratuita>` o :ref:`Spingerlo<add_spingere>` con un :ref:`Azione di Movimento<azione_movimento>`). 

Per compiti che richiedono dei prolungati periodi di tempo, bisogna spendere metà del tempo (al ritmo di 3 ore al giorno per animale addestrato) al lavoro sul completamento del compito prima di poter tentare una prova di :ref:`Addestrare Animali<addestrare_animali>`. Se la prova fallisce il tentativo di insegnare, allevare o addestrare l’animale fallisce e non c’è bisogno di completare il periodo di insegnamento, allevamento o addestramento. Se il periodo viene interrotto o il compito non è portato termine il tentativo di insegnare, allevare o addestrare l’animale fallisce automaticamente.

Ritentare
----------
Sì, tranne per l':ref:`Allevamento<add_allevare>`.

Speciale
---------
Si può utilizzare questa abilità su una creatura con un punteggio di Intelligenza di 1 o 2 che non sia un animale, ma la CD di qualsiasi prova aumenta di 5 e queste creature hanno lo stesso limite sui comandi apprendibili di un animale.

Un :ref:`Druido` o :ref:`Ranger` ricevono bonus di circostanza +4 alle prove di :ref:`Addestrare Animali<addestrare_animali>` con il loro :ref:`Compagno_Animale_Druido` che conosce inoltre uno o più comandi bonus, che non vengono conteggiati per il normale limite di comandi conosciuti e non richiedono alcun periodo di addestramento o prova per essere appresi.

Se si possiede :ref:`Affinità Animale<tal_affinità_animale>`, si riceve un bonus alle prove di :ref:`Addestrare Animali<addestrare_animali>`.

Senza Addestramento
--------------------
Si può utilizzare anche una prova di Carisma per :ref:`Gestire<add_gestire>` e :ref:`Spingere<add_spingere>` animali domestici, ma non si possono fare le altre azioni. 

Questo discorso rimane valido anche nei riguardi di un :ref:`Compagno_Animale_Druido`.

.. _artigianato:

---------------------------
Artigianato (Int)
---------------------------
.. contents:: In breve:
	:local:


Con questa abilità si possono di creare oggetti di un tipo specifico, come armi o armature, ma così come :ref:`Conoscenze<conoscenze>`, :ref:`Intrattenere<intrattenere>` e :ref:`Professione<professione>`, essa è in realtà un certo numero di abilità separate. 

Si possono avere diverse abilità di :ref:`Artigianato<artigianato>`, ognuna con i suoi gradi. Le più comuni sono: Alchimia, Calligrafia, Carpenteria, Ceramica, Costruire Archi, Costruire Navi, Costruire Trappole, Fabbricare Armature, Fabbricare Armi, Ferramenta, Intessere Ceste, Lavorare Pellami, Lavori in Muratura, Metallurgia, Pittura, Oreficeria, Rilegare Libri, Riparare Scarpe, Scultura, Tessitura.

In generale un’abilità di :ref:`Artigianato<artigianato>` è specializzata nella creazione di qualcosa e se non è così, allora è una :ref:`Professione<professione>`.

.. _tabella_artigianato:

.. figure:: Immagini/Artigianato.png
    :width: 800

    Elenco delle abilità di :ref:`Artigianato<artigianato>`.

Prova
--------
È possibile praticare il mestiere con un buon stipendio, guadagnando la metà del risultato della prova in monete d’oro per ogni settimana di lavoro. Il personaggio sa come utilizzare gli strumenti del mestiere, come eseguire le mansioni giornaliere del lavoro, come supervisionare
aiutanti inesperti e come risolvere i problemi più comuni. 

.. Note::
	I lavoratori inesperti e gli assistenti guadagnano una media di 1 moneta d’argento al giorno.

Ad ogni modo, la funzione base di questa abilità è di permettere di realizzare un oggetto del tipo appropriato con una CD dipendente dalla difficoltà dell’oggetto che deve essere creato. 

La CD, i risultati della prova e il prezzo dell’oggetto determinano quanto tempo occorre per realizzarlo e il prezzo dell’oggetto finito determina anche il costo delle materie prime.

In alcuni casi, l’incantesimo :ref:`Fabbricare<inc_fabbricare>` può essere utilizzato per ottenere i risultati di una prova di :ref:`Artigianato<artigianato>` senza dover effettuare alcun tiro, ma è necessario effettuarne una prova appropriata quando si utilizza l’incantesimo per realizzare oggetti che richiedono un elevato grado di abilità manifatturiera. Lo stesso vale per :ref:`Creazione Minore<inc_creazione_minore>`.

Una prova di :ref:`Artigianato<artigianato>` in relazione a oggetti lavorati in legno e in congiunzione con l’incantesimo :ref:`Legno di Ferro<inc_legno_di_ferro>`, permette
di realizzare oggetti in legno della consistenza dell’acciaio.

Sono richiesti gli :ref:`Arnesi da Artigiano<equip_arnesi_da_artigiano>` per poter lavorare. Se questi sono perfetti si hanno le migliori possibilità di successo con un bonus di circostanza +2 , viceversa se vengono utilizzati degli attrezzi improvvisati, si ha una penalità di circostanza –2. 

***************
Tempo e Denaro
***************
Per determinare quanto **tempo e denaro** sono necessari per realizzare un oggetto, occorre seguire questi passi:
1. Trovare il prezzo dell’oggetto in monete d’argento (1 mo = 10 ma).
2. Trovare la CD dell’oggetto dalla :ref:`Tabella<tabella_artigianato>`.
3. Pagare 1/3 del prezzo dell’oggetto in materie prime.
4. Effettuare una prova di :ref:`Artigianato<artigianato>` che rappresenti una settimana di lavoro e moltiplicarne il risultato per la CD in caso di successo.

	Se tale valore è maggiore o uguale al prezzo dell’oggetto in monete d’argento, allora l’oggetto è stato completato. Se raggiunge il doppio o il triplo, allora l’oggetto e stato completato in metà o in un terzo del tempo e così via.

	Se invece è minore, allora rappresenta i progressi fatti durante la settimana: occorre trascrivere il risultato ed effettuare un’altra prova per la settimana successiva. Ogni settimana si ottengono dei progressi fino a quando il totale raggiunge il prezzo dell’oggetto. 

	Se la prova è fallita di 4 o meno, per quella settimana non ci sono progressi. Se di 5 o più, metà delle materie prime vengono sprecate ed è necessario pagare di nuovo il costo.

********************
Progressi al giorno
********************
Si possono effettuare prove giorno per giorno invece che per settimana, e in questo caso il progresso (risultato della prova × CD) dovrebbe essere diviso per il numero dei giorni della settimana.

***********************
Creare Oggetti Perfetti
***********************
Si può creare un oggetto perfetto: un’arma, un’armatura, uno scudo o un attrezzo che conferisce un bonus per il suo uso grazie alla sua eccezionale fattura.

Per farlo bisogna creare la componente perfetta come se si trattasse di un oggetto distinto in aggiunta all’oggetto standard, con un prezzo fissato di 300 mo per un’arma e 150 mo per
un’armatura o uno scudo e CD 20. Il costo per la componente perfetta è un terzo dell’ammontare indicato, proprio come il costo delle materie prime.

Una volta completate entrambe le componenti si ottiene l’oggetto perfetto.

*****************
Riparare Oggetti
*****************
Generalmente è possibile riparare un oggetto a un quinto del suo prezzo e alla stessa CD necessaria per realizzarlo la prima volta.

Azione
-------
Non si applica. Le prove di :ref:`Artigianato<artigianato>` sono svolte in giorni o settimane.

Ritentare
---------
Sì, ma ogni volta che la prova è fallita di 5 o più, metà delle materie prime sono sprecate e si deve pagarne nuovamente il prezzo.

Speciale
----------
Si può aggiungere volontariamente +10 alla CD indicata per costruire un oggetto per creare
gli oggetti più rapidamente: moltiplicando il risultato per una CD più alta si progredisce più rapidamente. La scelta va fatta prima di compiere la prova.

Per creare un oggetto utilizzando :ref:`Artigianato<artigianato>` (alchimia), è necessario essere in possesso dell'equipaggiamento alchemico necessario per la ricetta, non reperibile ovunque. 

Acquistare e mantenere un :ref:`Laboratorio da Alchimista<equip_lab_alchimista>` fornisce bonus di circostanza +2 alle prove di :ref:`Artigianato<artigianato>` (alchimia), per avere gli attrezzi perfetti per il lavoro, ma non influenza il costo di alcun oggetto creato utilizzando l’abilità.

.. Note::
	Gli :ref:`gnomi` ricevono bonus +2 alle prove di :ref:`Artigianato<artigianato>` o Professione di loro scelta.

.. _artista_della_fuga:

----------------------------------------------
Artista della Fuga (Des - Penalità Armatura)
----------------------------------------------
.. contents:: In breve:
	:local:


Con questa abilità ci si può liberare da legacci o sottrarsi dalla lotta.

Prova
-------
.. figure:: Immagini/ArtistaDellaFuga.png
    :align: right
    :width: 300

La tabella seguente mostra le CD per sfuggire a varie forme di impedimenti.

******
Corde
******
La CD della prova di :ref:`Artista della Fuga<artista_della_fuga>` è pari al BMC in combattimento di chi lega +20.

**************************
Manette e manette perfette
**************************
Le manette hanno una CD stabilita al momento della costruzione. 

(Ndr: Non sono sicuro di cosa significhi...)

****************
Spazio Ristretto
****************
Questa è la CD per passare attraverso uno spazio in cui sta la testa ma non le spalle. Se lo spazio è lungo, potrebbero occorrere più prove. Non è possibile passare attraverso uno spazio in cui non passa neanche la testa.

**********
Lottatore
**********
È possibile effettuare una prova di :ref:`Artista della Fuga<artista_della_fuga>` al posto di una di :ref:`Manovra in Combattimento<azione_manovra_in_combattimento>` per liberarsi dalla :ref:`Lotta<azione_lottare>` o per passare da :ref:`Immobilizzati<immobilizzato>` a semplice :ref:`Lotta<azione_lottare>`.

Azione
------
Effettuare una prova di :ref:`Artista della Fuga<artista_della_fuga>` per liberarsi da corde, manette o altri impedimenti (tranne lottare) richiede 1 minuto di lavoro. 

Liberarsi da una rete o da un incantesimo :ref:`Animare Corde<inc_animare_corde>`, :ref:`Comandare Vegetali<inc_comandare_vegetali>`, :ref:`Controllare Vegetali<inc_controllare_vegetali>` o :ref:`Intralciare<inc_intralciare>` è un’:ref:`Azione di Round Completo<azione_di_round_completo>`. 

Fuggire ad una presa o da essere :ref:`Immobilizzati<immobilizzato>` è un’:ref:`Azione Standard<azione_standard>`. 

Sgusciare attraverso uno spazio ristretto richiede almeno 1 minuto, forse di più, a seconda di quanto sia lungo lo spazio.

Ritentare
----------
È possibile ritentare se si sta passando attraverso uno spazio ristretto, effettuando più prove di abilità. In generale se la situazione lo permette è anche possibile effettuare prove addizionali, o prendere 20 se non vi sono azioni contrapposte al tentativo. 

Se la CD per liberarsi da corde o legacci è maggiore di 20 + il bonus di :ref:`Artista della Fuga<artista_della_fuga>`, non è possibile liberarsi usando l’abilità.

Speciale
----------
Se si possiede il talento :ref:`Furtivo<tal_furtivo>`, si riceve un bonus alle prove di :ref:`Artista della Fuga<artista_della_fuga>`.

.. _camuffare:

---------------------------
Camuffare (Car)
---------------------------
.. contents:: In breve:
	:local:

Con questa abilità si può cambiare il proprio aspetto.

Prova
------
Il risultato della prova di :ref:`Camuffare<camuffare>` determina che successo ha ottenuto il travestimento ed è contrapposto ai risultati delle prove di :ref:`Percezione<percezione>` degli altri. Tuttavia, se non si attira l’attenzione, gli altri individui non devono effettuare prove di :ref:`Percezione<percezione>`. 

Quando si attira l’attenzione di persone sospettose (come una guardia che osserva la gente comune attraversare la porta della città), si presume che questi osservatori stiano prendendo 10 alle loro prove di :ref:`Percezione<percezione>`.

.. Caution::
	La prova di :ref:`Camuffare<camuffare>` si effettua una sola volta, anche se più persone vi si contrappongono con le proprie prove di :ref:`Percezione<percezione>`. Questa viene effettuata dal GM di nascosto in modo che non si sappia se il travestimento è riuscito o meno.

.. figure:: Immagini/Camuffare.png
    :align: right
    :width: 300

L’efficacia del travestimento dipende in parte da quanto si sta cercando di modificare il proprio aspetto, come descritto in tabella. 
	
.. Note::
	:ref:`Camuffare<camuffare>` può essere usato per apparire come una creatura di una categoria di taglia più grande o più piccola della propria, ma non la altera realmente, pertanto essa non cambia ai fini del combattimento.

.. figure:: Immagini/Camuffare2.png
    :align: right
    :width: 300

Se si sta impersonando un individuo particolare, coloro che conoscono l’aspetto di quella persona hanno un bonus alle loro prove di :ref:`Percezione<percezione>` e sono considerati automaticamente sospettosi del personaggio camuffato, così che le prove contrapposte sono sempre richieste.

Un individuo effettua una prova di :ref:`Percezione<percezione>` subito dopo un incontro e poi ad ogni ora successiva. Se per caso si incontrano molte creature diverse, ognuna per un tempo breve, bisogna effettuare la prova una volta al giorno o all’ora, utilizzando un modificatore medio di :ref:`Percezione<percezione>` per il gruppo.

Azione
--------
Creare un travestimento richiede 1d3 × 10 minuti di lavoro. Usare la magia come l’incantesimo :ref:`Camuffare Sè Stesso<inc_camuffare_sè_stesso>` riduce questa azione al tempo necessario per lanciare l’incantesimo o attivarne l’effetto.

Ritentare
----------
Sì. Un personaggio può tentare di rifare un travestimento fallito, ma una volta che gli altri sanno che è stato tentato un travestimento saranno più sospettosi.

Speciale
----------
La magia che altera la forma del ricevente come :ref:`Alterare Sè Stesso<inc_alterare_sè_stesso>`, :ref:`Camuffare Sè Stesso<inc_camuffare_sè_stesso>`, :ref:`Metamorfosi<inc_metamorfosi>` e
:ref:`Trasformazione<inc_trasformazione>` garantisce bonus +10 alle prove di :ref:`Camuffare<camuffare>`. 

La magia di divinazione che vede attraverso le illusioni come :ref:`Visione del Vero<inc_visione_del_vero>` non permette di vedere attraverso un travestimento comune, ma può negare la componente magica di un travestimento potenziato magicamente.

Occorre effettuare una prova di :ref:`Camuffare<camuffare>` quando si lancia l’incantesimo :ref:`Simulacro<inc_simulacro>` per determinare la qualità della somiglianza.

Se si ha il talento :ref:`Ingannevole<tal_ingannevole>`, si riceve un bonus alle prove di :ref:`Camuffare<camuffare>`.

.. _cavalcare:

-----------------------------------
Cavalcare (Des - Penalità Armatura)
-----------------------------------
.. contents:: In breve:
	:local:


Con questa abilità è possibile montare una cavalcatura, sia essa un cavallo o una più esotica come un grifone o un pegaso. Se si utilizza questa abilità con una cavalcatura inadatta ad essere cavalcata, si subisce penalità –5 alle prove di :ref:`Cavalcare<cavalcare>`.

Prova
-----
Tipiche azioni di equitazione non richiedono prove; si può sellare, montare, cavalcare e smontare da una cavalcatura senza problemi, tuttavia le azioni seguenti richiedono una prova:

.. image:: Immagini/Cavalcare.png
    :width: 300
    :align: center

************************
Guidare con le Ginocchia
************************
:Azione: Nessuna

Si può reagire istantaneamente per guidare la cavalcatura con le ginocchia in modo da poter
utilizzare entrambe le mani in combattimento. 

La prova di :ref:`Cavalcare<cavalcare>` deve essere effettuata all’inizio del proprio turno e se si fallisce si può usare una sola mano durante quel round poiché l’altra serve per controllare la cavalcatura.

*****************
Rimanere in Sella
*****************
:Azione: Nessuna

Si può reagire istantaneamente per cercare di evitare di cadere quando la cavalcatura si impenna, scarta all’improvviso o si subisce danno. 

******************************************
Combattere con una Cavalcatura Addestrata
******************************************
:Azione: :ref:`Gratuita<azione_gratuita>`

Se si dirige la propria cavalcatura da guerra ad attaccare in combattimento, si può utilizzare il proprio attacco (o attacchi) normalmente.

*********
Copertura
*********
:Azione: :ref:`Immediata<azione_immediata>`

Si può reagire istantaneamente abbassandosi e aggrappandosi al fianco della cavalcatura per usarla come copertura, ma non si può attaccare né lanciare incantesimi in tal caso. 

Se si fallisce la prova di :ref:`Cavalcare<cavalcare>` non si ottiene il vantaggio della
copertura. 

Per recuperare la posizione è richiesta un’:ref:`Azione di Movimento<azione_movimento>` senza alcuna prova.

***************
Caduta Morbida
***************
:Azione: Nessuna

Si reagisce istantaneamente per tentare di non subire danni quando si cade dalla cavalcatura. In caso di fallimento si subiscono 1d6 danni per la caduta e si cade a terra proni.

*****
Salto
*****
:Azione: Nessuna

Si può far saltare gli ostacoli alla cavalcatura come parte del suo movimento utilizzando il più basso modificatore di abilità tra :ref:`Cavalcare<cavalcare>` del personaggio o  :ref:`Saltare<acrobazia>` della cavalcatura per vedere quanto lontano la cavalcatura può saltare. 

Se si fallisce la prova, si cade dalla cavalcatura durante il salto e si subiscono danni da caduta (minimo 1d6 danni). 

L’uso non richiede un’azione, ma fa parte del movimento della cavalcatura.

*********************
Spronare Cavalcatura
*********************
:Azione: :ref:`Movimento<azione_movimento>`

Si può spronare una cavalcatura ad andare più veloce: il successo della prova aumenta la velocità della cavalcatura di 3 metri per 1 round ma infligge 1d3 danni alla creatura.

Si può usare questa capacità ogni round, ma la cavalcatura diventa affaticata dopo un numero di round pari al suo punteggio di Costituzione e non può essere impiegata su una cavalcatura :ref:`Affaticata<affaticato>`.

*************************************
Controllare Cavalcatura in Battaglia
*************************************
:Azione: :ref:`Movimento<azione_movimento>`

Si può tentare di controllare in combattimento un cavallo leggero, un pony, un cavallo pesante o un’altra cavalcatura **non** :ref:`Addestrata a Combattere<addestrato_al_combattimento>`. 

Se la prova fallisce non si può fare altro in quel round.

**************************
Montare o Smontare Veloce
**************************
:Azione: :ref:`Gratuita<azione_gratuita>`, ma bisogna averne una di :ref:`Movimento<azione_movimento>` a disposizione nel round

Si può montare o smontare da una cavalcatura **fino ad una taglia più grande** della propria. Se si fallisce la prova montare o smontare è un’:ref:`Azione di Movimento<azione_movimento>`. 

Azione
-------
Varie: montare o smontare normalmente è un’:ref:`Azione di Movimento<azione_movimento>`, le altre sono :ref:`Azioni di Movimento<azione_movimento>`, :ref:`Gratuite<azione_gratuita>` o non sono azioni come indicato sopra.

Speciale
---------
È possibile cavalcare senza sella, ma si applica penalità –5 a tutte le prove di :ref:`Cavalcare<cavalcare>`. Se la cavalcatura è dotata di una sella militare, conferisce bonus di circostanza +2 alle prove per rimanere in sella.

L’abilita :ref:`Cavalcare<cavalcare>` è un prerequisito per :ref:`Attacco in Sella<tal_attacco_in_sella>`, :ref:`Carica Devastante<tal_carica_devastante>`, :ref:`Combattere in Sella<tal_combattere_in_sella>`, :ref:`Tirare in Sella<tal_tirare_in_sella>` e :ref:`Travolgere<tal_travolgere>`.

Se si possiede :ref:`Affinità Animale<tal_affinità_animale>`, si riceve un bonus alle prove di :ref:`Cavalcare<cavalcare>`.

.. _conoscenze:

---------------------------------
Conoscenze (Int - Addestramento)
---------------------------------
.. contents:: In breve:
	:local:

Con questa abilità, si è istruiti in un campo di studi e si è in grado di rispondere a domande semplici e complesse. 

Come :ref:`Artigianato<artigianato>` e :ref:`Professione<professione>`, :ref:`Conoscenze<conoscenze>` in realtà comprende una serie di specializzazioni diverse:

Arcane
	Misteri antichi, tradizioni magiche, simboli arcani, frasi criptiche, costrutti, draghi e bestie magiche.
Dungeon
	Aberrazioni, caverne, melme e esplorazioni sotterranee.
Geografia
	Territori, terreni, clima e popolazione.
Ingegneria
	Costruzioni, acquedotti, ponti e fortificazioni.
Locali
	Leggende, personalità, abitanti, leggi, costumi, tradizioni e umanoidi.
Natura
	Animali, folletti, giganti, umanoidi mostruosi, vegetali, stagioni e cicli, tempo atmosferico e parassiti.
Nobiltà
	Lignaggi, araldica e regalità.
Piani
	Piani Interni, Piani Esterni, Piano Astrale, Piano Etereo, esterni e magia legata ai piani.
Religioni
	Divinità, mitologia, tradizione ecclesiastica, simboli sacri e non morti.
Storia
	Guerre, colonie, migrazioni e fondazione di città.

.. _tabella_conoscenze:

.. figure:: Immagini/Conoscenze.png
    :width: 800

    Elenco di CD per le prove di :ref:`Conoscenze<conoscenze>`.

Prova
------
Rispondere a una domanda nel proprio campo di studio ha CD 10 per domande decisamente facili, 15 per domande base e 20 o 30 per domande molto complesse.

Molte delle abilità di Conoscenze hanno usi specifici, riportati in :ref:`Tabella<tabella_conoscenze>`.

*******************
Identificare Mostri
*******************
Si può utilizzare questa abilità anche per identificare mostri e i loro poteri speciali o vulnerabilità. 

In generale, la CD di queste prove è pari a 10 + il GS del mostro, ma per mostri comuni come i :ref:`Goblin<bestiario_goblin>` la CD della prova è pari a 5 + il GS del mostro.
Per i mostri particolarmente rari come un :ref:`Tarrasque<bestiario_tarrasque>` la CD è invece pari a 15 + il GS del mostro, o superiore.

Una prova riuscita permette di ricordare un’informazione utile riguardante il mostro e per ogni 5 punti per cui la prova eccede la CD, si può ricordare un’ulteriore informazione.

Azione
-------
Generalmente nessuna: nella maggior parte dei casi compiere una prova di :ref:`Conoscenze<conoscenze>` non richiede un’azione a meno di non essere privi di addestramento (vedi sotto).

Ritentare
----------
No. 

Il tiro rappresenta le conoscenze possedute e meditare una seconda volta su un argomento non permette di sapere qualcosa che già non si sapeva prima.

Senza addestramento
--------------------
Non si può effettuare una prova di :ref:`Conoscenze<conoscenze>` senza addestramento che abbia una CD superiore a 10 a meno di avere accesso ad una biblioteca ben fornita che tratta un argomento specifico. Il tempo necessario per effettuare prove usando una biblioteca, però, aumenta a 1d4 ore. 

.. Note::
	Biblioteche particolarmente complete potrebbero concedere un bonus alle prove di :ref:`Conoscenze<conoscenze>` riguardanti gli argomenti di cui trattano.

.. _diplomazia:

---------------------------
Diplomazia (Car)
---------------------------
.. figure:: Immagini/Abilita2.png

.. contents:: In breve:
	:local:


Con questa abilità si possono persuadere gli altri ad essere d’accordo con i propri argomenti, risolvere diverbi e raccogliere preziose informazioni e dicerie dalle persone. L’abilità è anche usata per negoziare in modo efficace con la giusta etichetta e condotta adatta alla situazione controversa. 

Prova
------
Durante le contrattazioni i negoziatori effettuano prove contrapposte di :ref:`Diplomazia<diplomazia>` per vedere chi ottiene vantaggi.

.. figure:: Immagini/Diplomazia.png
    :align: right
    :width: 300

Si può cambiare l’atteggiamento iniziale dei personaggi non giocanti con una prova effettuata con successo. La CD della prova dipende dall’atteggiamento iniziale della creatura ed è modificata dal suo punteggio di Carisma: se la prova riesce, l’atteggiamento del personaggio viene migliorato di un grado e per ogni 5 punti di cui la prova supera la CD, l’atteggiamento del personaggio aumenta di un grado addizionale.

.. Note::
	L’atteggiamento di una creatura non può essere spostato di più di due gradi in questo modo, sebbene il GM possa ignorare questa regola in alcune situazioni. 

Se si fallisce la prova di 4 o meno, l’atteggiamento del personaggio resta inalterato, ma se si fallisce di 5 o più scende di un grado.

Non si può usare :ref:`Diplomazia<diplomazia>` contro una creatura che non è in grado di capire o ha un’Intelligenza di 3 o meno ed è in genere inefficace in combattimento e contro creature che intendono ferire il personaggio o i suoi alleati nell’immediato futuro. 

Qualsiasi variazione di atteggiamento causata da :ref:`Diplomazia<diplomazia>` in genere dura 1d4 ore ma può durare di più o di meno in base alla situazione (a discrezione del GM).

.. figure:: Immagini/Diplomazia2.png
    :align: right
    :width: 300

Se l’atteggiamento della creatura nei propri confronti è almeno indifferente, si può farle delle richieste con una prova aggiuntiva, usandone l’atteggiamento corrente per determinare la CD base, con dei modificatori dipendenti dalla richiesta. 

Una volta che l’atteggiamento della creatura si è spostato a premuroso, la creatura soddisferà la maggior parte delle richieste senza una prova, a meno che la richiesta non sia contro la sua natura o la metta in serio pericolo. 

.. Caution::
	Alcune richieste possono fallire automaticamente se sono contro la natura della creatura o i suoi valori, a discrezione del GM.

*************************
Raccogliere Informazioni
*************************
Si può anche utilizzare :ref:`Diplomazia<diplomazia>` per raccogliere informazioni su un argomento o individuo specifico passando almeno 1d4 ore a saggiare l’opinione della gente in locande, mercati e altri luoghi simili. 

La CD di questa prova dipende dalla notorietà delle informazioni raccolte: per fatti o dicerie comunemente noti è 10, per conoscenze oscure o segrete può aumentare a 20 o più. Il GM potrebbe decidere che alcuni argomenti siano semplicemente sconosciuti alle gente comune.

Azione
------
Cambiare l’atteggiamento degli altri generalmente richiede 1 minuto intero di interazione continua mentre effettuare una richiesta richiede 1 o più round di interazione, in base alla sua complessità. 

Usare :ref:`Diplomazia<diplomazia>` per raccogliere informazioni richiede 1d4 ore di ricerca di notizie ed informatori.

Ritentare
-----------
Non si può utilizzare :ref:`Diplomazia<diplomazia>` per influenzare l’atteggiamento di una data creatura più di una volta in un periodo di 24 ore. Se una richiesta viene rifiutata, il risultato non cambia con prove addizionali, ma si possono fare altre richieste. La raccolta di informazioni si può ritentare.

Speciale
---------
Se si possiede :ref:`Persuasivo<tal_persuasivo>`, si riceve un bonus alle prove di :ref:`Diplomazia<diplomazia>`.

.. _disattivare_congegni:

------------------------------------------------------------------------------------
Disattivare Congegni (Des - Penalità Armatura - Addestramento)
------------------------------------------------------------------------------------
.. contents:: In breve:
	:local:

Con questa abilità si possono disarmare trappole, aprire serrature e sabotare congegni meccanici semplici come le catapulte, le ruote di un carro o le porte.

Prova
------
Quando si tenta di disarmare una trappola o un altro congegno **la prova è fatta in segreto** in modo che non si sappia se il tentativo è riuscito o meno, con una CD che dipende da quanto è complicato il meccanismo. 

.. _tabella_disattivare1:

.. figure:: Immagini/DisattivareCongegni.png
    :align: right
    :width: 300

Con un successo si disattiva il congegno, un fallimento di 4 o meno permette di tentare nuovamente, ma se si fallisce per più di 5 qualcosa non ha funzionato: se si tratta di un trappola viene attivata, se è sabotaggio si è convinti di aver disattivato il congegno, ma in realtà esso funziona normalmente.

È possibile anche sistemare meccanismi semplici come selle o ruote di carro perché funzionino normalmente per un po’ per poi rompersi più tardi, di solito dopo 1d4 round o minuti di utilizzo.

*********************
Scassinare Serrature
*********************
.. figure:: Immagini/DisattivareCongegni2.png
    :align: right
    :width: 300

La CD per scassinare una serratura dipende della qualità della stessa.

.. Caution::
	Se non si hanno degli :ref:`Arnesi da Scasso<equip_arnesi_da_scasso>`, le CD aumentano di 10.

Azione
------
Il tempo necessario per effettuare una prova di :ref:`Disattivare Congegni<disattivare_congegni>` dipende dall’azione intrapresa, come indicato in :ref:`Tabella<tabella_disattivare1>`: un congegno semplice o una serratura richiedono 1 round ed è un’:ref:`Azione di Round Completo<azione_di_round_completo>`, uno complicato o difficile 1d4 o 2d4 round.

Ritentare
---------
Varie. 

Si possono ritentare prove effettuate per disattivare le trappole se si fallisce di 4 o meno, mentre le prove fallite di scassinare serrature si possono ritentare. 

Speciale
--------
Se si possiede :ref:`Manolesta<tal_manolesta>`, si riceve un bonus alle prove di :ref:`Disattivare Congegni<disattivare_congegni>`.

Un ladro che supera la CD di una trappola di 10 o più può studiarla, capirne il funzionamento ed oltrepassarla senza disarmarla e manipolarla in modo che anche i suoi alleati possano oltrepassarla.

Restrizioni
-------------
I personaggi con la capacità di scoprire trappole, per esempio i :ref:`Ladri<ladro>`, possono disarmare trappole magiche. Esse hanno in genere **CD = 25 + Livello Incantesimo**.

Anche gli incantesimi :ref:`Cerchio di Teletrasporto<inc_cerchio_di_teletrasporto>`, :ref:`Glifo di Interdizione<inc_glifo_di_interdizione>`, :ref:`Simboli<inc_simbolo>` e :ref:`Trappola di Fuoco<inc_trappola_di_fuoco>` creano trappole che un :ref:`Ladro<ladro>` può disarmare con una prova riuscita di :ref:`Disattivare Congegni<disattivare_congegni>`, al contrario :ref:`Crescita di Spine<inc_crescita_di_spine>` e :ref:`Rocce Aguzze<inc_rocce_aguzze>` creano pericoli magici contro i quali questa abilità è inefficace. Vedi le descrizioni degli incantesimi per i dettagli a riguardo.

.. _furtività:

------------------------------------
Furtività (Des - Penalità Armatura)
------------------------------------
.. contents:: In breve:
	:local:

Con questa abilità si è in grado di passare inosservati, nascondersi, muoversi silenziosamente e colpire gli avversari ignari. 

Prova
------
La prova di :ref:`Furtività<furtività>` per **nascondersi** è contrapposta a quella di :ref:`Percezione<percezione>` di chiunque sia in grado di vedervi. 

Il personaggio può muoversi fino alla metà della velocità normale per round senza penalità e fino alla propria piena velocità subendo penalità –5.

.. Caution::
	È praticamente impossibile usare :ref:`Furtività<furtività>` mentre si attacca, si corre o si :ref:`Carica<azione_carica>`.

Creature di taglia superiore o inferiore alla Media hanno dei modificatori alla prova: Piccolissima +16, Minuta +12, Minuscola +8, Piccola +4, Grande –4, Enorme –8, Mastodontica –12,
Colossale –16.

Se si è osservati da qualcuno (si presume tramite il senso della vista, ma è possibile utilizzare anche altri sensi), non si può usare :ref:`Furtività<furtività>`, ma una :ref:`Copertura<copertura>` o :ref:`Occultamento<occultamento>` generalmente ne permettono l’uso nei confronti della maggior parte delle creature che usano la vista. 

Se gli osservatori sono momentaneamente distratti, per esempio da una prova di :ref:`Raggirare<raggirare>`, si può tentare di usare :ref:`Furtività<furtività>` così come quando gli altri distolgono la loro attenzione, in caso si riesca a raggiungere un posto nascosto di qualche tipo. Quest'ultima prova, tuttavia, è effettuata con penalità –10 poiché bisogna muoversi velocemente.

.. _cecchino:

********
Cecchino
********
Se si supera la prova di :ref:`Furtività<furtività>` ad almeno 3 metri dal bersaglio, si può compiere un :ref:`Attacco a Distanza<azione_attacco_a_distanza>` e poi riutilizzare immediatamente :ref:`Furtività<furtività>` cun una penalità –20 alla prova perché bisogna occultarsi dopo il tiro.

************************************
Creare un Diversivo per Nascondersi
************************************
Si può utilizzare l’abilità :ref:`Raggirare<raggirare>` per avere un aiuto a :ref:`Furtività<furtività>`, concedendo il diversivo momentaneo di cui si ha bisogno per tentare di usare
:ref:`Furtività<furtività>` mentre la gente distoglie l’attenzione.

Azione
-------
Generalmente nessuna. 

Normalmente, la prova di :ref:`Furtività<furtività>` fa parte del movimento, quindi non richiede
un’azione separata, ma usarla immediatamente dopo un :ref:`Attacco a Distanza<azione_attacco_a_distanza>` (vedi :ref:`cecchino`) è un’:ref:`Azione di Movimento<azione_movimento>`.

Speciale
--------
Se si è :ref:`Invisibili<invisibile>` si riceve bonus +40 alle prove di :ref:`Furtività<furtività>` mentre si è :ref:`Immobili<immobilizzato>`, o bonus +20 se si è in movimento.

Se si possiede :ref:`Furtivo<tal_furtivo>`, si riceve un bonus alle prove di :ref:`Furtività<furtività>`.

.. _guarire:

---------------------------
Guarire (Sag)
---------------------------
.. contents:: In breve:
	:local:

Con questa abilità si possono curare le ferite e le malattie.

Prova
------
.. figure:: Immagini/Guarire.png
    :align: right
    :width: 300

La CD e l’effetto dipendono da cosa si sta cercando di fare.

****************
Pronto Soccorso
****************
Di solito, lo si usa per salvare un personaggio :ref:`Morente<morente>`. 

Se un personaggio ha punti ferita negativi e ne sta perdendo sempre di più (al ritmo di 1 a
round, 1 all’ora o 1 al giorno), è possibile renderlo stabile. in questo modo non riguadagna punti ferita, ma smette di perderne. 

Pronto soccorso permette anche di fermare gli effetti di :ref:`Sanguinamento<sanguinamento>`.

*********************
Cura a Lungo Termine
*********************
Fornire cure a lungo termine significa curare per un giorno o più una persona ferita. Se la
prova di :ref:`Guarire<guarire>` ha successo, permette al paziente di recuperare i suoi punti ferita o punti di caratteristica persi al doppio del ritmo normale: 2 punti ferita/livello e 2 punti caratteristica per 8 ore di riposo in un giorno, o 4 punti ferita/livello e 4 punti caratteristica per ogni giorno di completo riposo.

Ci si può occupare al massimo di sei pazienti per volta e si necessita di alcuni oggetti e scorte (bende, pomate ecc.) che sono facilmente reperibili nei territori abitati. 

Fornire cure a lungo termine conta come attività leggera per il guaritore, ma non si possono fornire cure a lungo termine a se stessi.

***********************************************************
Trattare Ferita di Tribolo, Crescita di Spine, Rocce Aguzze
***********************************************************
Una creatura ferita calpestando un :ref:`Tribolo<equip_triboli>` ha la velocità dimezzata, ma una prova di Guarire può eliminare questa penalità.

Una creatura ferita da :ref:`Crescita di Spine<inc_crescita_di_spine>` o :ref:`Rocce Aguzze<inc_rocce_aguzze>` deve superare un TS sui Riflessi o subisce delle ferite che riducono la sua velocità ad un terzo. Un altro personaggio può togliere questa penalità spendendo 10 minuti a curare le ferite della vittima e superando una prova di :ref:`Guarire<guarire>` con la CD del TS dell’incantesimo.

************************
Trattare Ferite Mortali
************************
Quando si trattano ferite mortali, si possono ristorare i punti ferita persi da una creatura. 

La cura consiste in generale in 1 punto ferita per livello della creatura, ma se si supera la CD di 5 o più, si aggiunge il proprio Mod Sag (se positivo) alla quantità di ferite curate. 

Una creatura può beneficiare di una cura di questo tipo solo se non sono passate più di 24 ore da quando è stata ferita e non più di una volta al giorno e si devono spendere due utilizzi da una :ref:`Borsa del Guaritore<equip_borsa_del_guaritore>` per trattare ferite mortali. 

Si subisce penalità –2 alla propria prova per ogni utilizzo di cui non si dispone della :ref:`Borsa del Guaritore<equip_borsa_del_guaritore>`.

****************
Trattare Veleno
****************
Trattare un veleno significa occuparsi di un singolo personaggio che è stato avvelenato e che sta per subire ulteriori danni dal veleno, o altro effetto. 

Ogni volta che il personaggio avvelenato effettua un TS contro il veleno, si effettua una prova di :ref:`Guarire<guarire>`. Se la prova supera la CD del veleno, il personaggio avvelenato riceve bonus di competenza +4 al suo TS contro il veleno.

******************
Trattare Malattia
******************
Trattare una malattia significa occuparsi di un singolo personaggio malato. 

Ogni volta che il personaggio malato effettua un TS contro gli effetti della malattia, si effettua una prova di :ref:`Guarire<guarire>`. Se la prova supera la CD della malattia, il personaggio malato riceve bonus di competenza +4 al suo TS contro la malattia.

Azione
-------
Fornire il pronto soccorso e trattare ferite di tribolo o veleno è un’:ref:`Azione Standard<azione_standard>`, trattare una malattia o creatura ferita da :ref:`Crescita di Spine<inc_crescita_di_spine>`/:ref:`Rocce Aguzze<inc_rocce_aguzze>` richiede 10 minuti di lavoro; trattare ferite mortali richiede 1 ora di lavoro; fornire cure a lungo termine richiede 8 ore di attività leggera.

Ritentare
----------
Varie. 

In generale, non si può ritentare una prova di :ref:`Guarire<guarire>` senza essere certi del fallimento della prova originale. 

Si può sempre ritentare una prova per fornire pronto soccorso, assumendo che il bersaglio del precedente tentativo sia ancora vivo.

Speciale
---------
Un personaggio con :ref:`Autosufficiente<tal_autosufficiente>` riceve un bonus alle prove di :ref:`Guarire<guarire>`, così come una ref:`Borsa del Guaritore<equip_borsa_del_guaritore>` fornisce bonus di circostanza +2.

.. _intimidire:

---------------------------
Intimidire (Car)
---------------------------
.. contents:: In breve:
	:local:

Con questa abilità si può spaventare un avversario o costringerlo ad agire in modo più conveniente ai propri scopi.

L’intimidazione comprende sia le minacce verbali che il linguaggio del corpo.

Prova
-----
Si può usare :ref:`Intimidire<intimidire>` per costringere un avversario ad agire in modo amichevole nei propri confronti per 1d6 × 10 minuti con **CD = 10 + Dadi Vita bersaglio + Mod Sag bersaglio**. 

Se si supera la prova, il bersaglio darà le informazioni richieste, compirà azioni che non lo mettano in pericolo, o offrirà un aiuto limitato, similmente a quanto succede per :ref:`Diplomazia<diplomazia>`. Al termine dell’effetto, l’atteggiamento del bersaglio nei propri confronti diventerà maldisposto e potrebbe fare rapporto alle autorità locali. 

Se si fallisce la prova di 5 o più, il bersaglio fornirà informazioni errate o inutili, oppure frustrerà gli sforzi del personaggio.

.. _demoralizzare:

*************
Demoralizzare
*************
Si può utilizzare :ref:`Intimidire<intimidire>` anche per rendere :ref:`Scosso<scosso>` un avversario per qualche round con **CD = 10 + Dadi Vita bersaglio + Mod Sag bersaglio**. 

Se si supera la prova, il bersaglio rimane :ref:`Scosso<scosso>` per 1 round e per un round extra ogni 5 punti di cui si supera la CD. 

.. Note::
	Si può demoralizzare un avversario solo se si trova entro 9 metri e se si è chiaramente a portata di vista e udito.

Azione
--------
Cambiare l’atteggiamento di qualcuno richiede 1 minuto di interazione, mentre :ref:`Demoralizzare` è un’:ref:`Azione Standard<azione_standard>`.

Ritentare
---------
Si può ritentare una prova di :ref:`Intimidire<intimidire>` contro lo stesso avversario, ma ad ogni tentativo aggiuntivo la CD aumenta +5. 

Questo aumento si annulla dopo che è passata 1 ora.

Speciale
---------
Si riceve +4 alla prova di :ref:`Intimidire<intimidire>` per ogni categoria di taglia per cui si è più grande del bersaglio e viceversa.

Se si possiede :ref:`Persuasivo<tal_persuasivo>`, si riceve un bonus alle prove di :ref:`Intimidire<intimidire>` e i :ref:`Mezzorchi<mezzorchi>` ricevono bonus +2.

.. _intrattenere:

---------------------------
Intrattenere (Car)
---------------------------
.. contents:: In breve:
	:local:

Con questa abilità si è esperti in diversi tipi di espressione artistica, dal canto al suonare uno strumento. 

Come :ref:`Artigianato<artigianato>`, :ref:`Conoscenze<conoscenze>` e :ref:`Professione<Professione>`, :ref:`Intrattenere<Intrattenere>` è in realtà un certo numero di abilità separate: si possono avere diverse abilità di :ref:`Intrattenere<Intrattenere>`, ognuna con i suoi gradi.

Ognuna delle nove categorie dell’abilità :ref:`Intrattenere<Intrattenere>` comprende diversi metodi, strumenti o tecniche, di cui un piccolo elenco di esempio viene riportato di seguito per ogni categoria:

Canto
	Ballate, cantici e melodie.
Commedia
	Barzellette, buffoneria e lazzi.
Danza
	Balletti, madrigali e valzer.
Oratoria
	Cantastorie, epica e ode.
Recitazione
	Commedia, dramma e mimo.
Strumenti a Corda
	Arpa, liuto, mandolino e violino.
Strumenti a Fiato
	Flauto, flauto di pan, shawm, tromba e zufolo.
Strumenti a Percussione
	Campana, campanella, tamburo e gong.
Strumenti a Tastiera
	Arpicordo, pianoforte e organo.

Prova
------
.. figure:: Immagini/Intrattenere.png
    :align: right
    :width: 300

Si può impressionare il pubblico con le proprie doti e capacità.

Uno strumento musicale perfetto conferisce bonus di circostanza +2 alle prove di :ref:`Intrattenere<Intrattenere>` che ne coinvolgono l'uso.

Azione
------
Varie. 

Cercare di guadagnare soldi con un’esibizione in pubblico richiede da una serata di lavoro ad un’intera giornata di esibizioni. 

Le capacità speciali basate su :ref:`Intrattenere<Intrattenere>` del :ref:`Bardo` sono presentate nella descrizione della classe.

Ritentare
----------
Sì. 

Ulteriori tentativi sono consentiti, ma non eliminano i fallimenti precedenti e un pubblico che non è stato impressionato in passato avrà sicuramente dei pregiudizi sulle rappresentazioni future. La CD deve quindi essere aumentata di 2 punti per ogni fallimento precedente.

Speciale
----------
Un :ref:`bardo` deve avere gradi nelle specifiche categorie di :ref:`Intrattenere<Intrattenere>` per utilizzare alcune delle sue capacità di :ref:`Esibizione_Bardica`.

.. _intuizione:

---------------------------
Intuizione (Sag)
---------------------------
.. contents:: In breve:
	:local:

Con questa abilità si può capire se qualcuno sta mentendo o si possono intuire le sue vere intenzioni.

Prova
-------
.. figure:: Immagini/Intuizione.png
    :align: right
    :width: 300

Una prova effettuata con successo permette di evitare di essere :ref:`Raggirati<raggirare>`, ma è anche possibile utilizzare quest'abilità per capire quando si sta tramando qualcosa, se sta accadendo qualcosa di strano di cui si è all’oscuro o per valutare l’onestà di qualcuno.

********
Sospetto
********
Quest’uso dell’abilità essenzialmente significa fare una valutazione istintiva della situazione. 

In base al comportamento di un altro, è possibile avere l’impressione che ci sia qualcosa di sbagliato, come quando si parla con un impostore. In alternativa, si può avere la sensazione che
qualcuno sia completamente degno di fiducia.

**********************
Percepire Ammaliamento
**********************
È possibile capire quando il comportamento di qualcuno è influenzato da un effetto di ammaliamento anche se la persona non ne è consapevole. 

La CD tipica è 25, ma se il bersaglio è :ref:`Dominato<inc_dominare_persone>`, la CD è solo 15 a causa della portata limitata delle capacità del bersaglio.

*********************************
Distinguere un Messaggio Segreto
*********************************
Si può tentare di carpire un messaggio segreto in una conversazione tra altri due personaggi che stanno utilizzando l’abilità :ref:`Raggirare<raggirare>`. 

In questo caso, la prova di :ref:`Intuizione<intuizione>` è contrapposta a quella di :ref:`Raggirare<raggirare>` del personaggio che trasmette il messaggio e per ogni informazione che sfugge all’ascolto si subisce penalità –2 alla prova. 

Se si supera la prova con un margine di 4 punti o meno, si apprende che sta venendo comunicato qualcosa di segreto, ma non se ne riesce a comprendere il significato generale. Di 5 o più, si intercetta e comprende il messaggio. 

Se si fallisce di 4 o meno, non si riesce ad individuare la conversazione segreta. Di 5 o più punti significa che è stata dedotta qualche falsa informazione.

Azione
--------
Per cercare di ottenere informazioni con :ref:`Intuizione<intuizione>` occorre 1 minuto e si può spendere un’intera serata nel tentativo di verificare le intenzioni di chi si ha intorno.

Ritentare
---------
No, anche se si può effettuare una prova di :ref:`Intuizione<intuizione>` per ogni prova contrapposta di :ref:`Raggirare<raggirare>` effettuata.

Speciale
--------
I :ref:`ranger` hanno bonus alle prove di :ref:`Intuizione<intuizione>` quando utilizza questa abilità contro un nemico prescelto.

Se si possiede :ref:`Allerta<tal_allerta>` si riceve un bonus alle prove di :ref:`Intuizione<intuizione>`.

.. _linguistica:

----------------------------------
Linguistica (Int - Addestramento)
----------------------------------
.. contents:: In breve:
	:local:

Con questa abilità si è in grado di lavorare con i linguaggi, sia nella loro forma parlata che scritta. Si possono parlare più linguaggi, e decifrare qualsiasi linguaggio se si ha tempo a sufficienza. 

L’abilità nella scrittura permette di creare ed individuare anche i falsi.

Prova
-----

Si può decifrare la scrittura in un linguaggio non familiare o redatto in una forma incompleta o arcaica. La CD base è 20 per i messaggi più semplici, 25 per i testi normali e 30 o più per scritti complicati, esotici o molto antichi e se la prova riesce, si capisce il contenuto in generale di un brano dello scritto della lunghezza di una pagina (o l’equivalente).

Se la prova fallisce, occorre effettuare una prova di Saggezza con CD 5 per vedere se si riesce ad evitare di trarre una conclusione errata dal testo. Entrambe le prove di :ref:`Linguistica<linguistica>` e se necessaria quella di Saggezza sono effettuate **in segreto dal GM**, in modo
che non si sappia se le conclusioni tratte siano giuste o sbagliate.

*****************************************
Falsificare o Individuare Falsificazioni
*****************************************
Falsificare richiede materiali per scrivere che siano appropriati per il documento da falsificare. 

Per falsificare un documento la cui grafia non è specifica per una persona, si ha bisogno solo di aver visto prima un documento simile e si ottiene bonus +8 alla prova. Per falsificare una firma, è necessaria una firma autografa di quella persona per copiarla e si ottiene bonus
+4 alla prova. Per falsificare un lungo documento scritto di pugno da una persona particolare, è necessario un ampio esempio della grafia di quella persona.

La prova di :ref:`Linguistica<linguistica>` è effettuata di nascosto perché non si deve essere sicuri di quanto sia riuscita la falsificazione e come per :ref:`Camuffare<camuffare>`, non è necessaria alcuna prova fino a quando qualcuno non esamina il lavoro. 

.. figure:: Immagini/Linguistica.png
    :align: right
    :width: 300

La prova del personaggio è contrapposta a quella della persona che esamina il documento per controllare la sua autenticità, ma chi legge guadagna modificatori alla sua prova come indicato nella tabella.

********************************************
Imparare un Linguaggio
********************************************
Ogni volta che si mette un grado in questa abilità, si apprende a leggere e parlare un nuovo
linguaggio. I linguaggi comuni (e chi li parla normalmente) comprendono i seguenti:

.. figure:: Immagini/AbilitaLinguistica.png
    :align: left
    :width: 300

Abissale
	Demoni e esterni caotici malvagi.
Aklo
	Derro, mostri inumani o alieni e folletti malvagi.
Aquan
	Creature d’acqua e acquatiche.
Auran
	Creature d’aria e volanti.
Celestiale
	Angeli ed esterni buoni.
Comune
	:ref:`Razze_Base`.
Draconico
	Draghi e umanoidi del sottotipo rettile.
Druidico
	Solo i :ref:`druidi<druido>`.
Elfico
	:ref:`Elfi` e :ref:`mezzelfi`.
Gigante
	Ciclopi, ettin, ogre, giganti e troll.
Gnomesco
	:ref:`gnomi`.
Goblin
	Bugbear, goblin e hobgoblin.
Gnoll
	Gnoll.
Halfling
	:ref:`halfling`.
Ignan
	Creature di fuoco.
Infernale
	Diavoli e esterni legali malvagi.
Nanico
	Nani.
Orchesco
	Orchi e :ref:`mezzorchi`.
Silvano
	Centauri, folletti, vegetali e unicorni.
Terran
	Creature di terra.
Sottocomune
	Drow, duergar, morlock e svirfneblin.

Azione
--------
Varie. 

Decifrare una pagina di testo normale richiede 1 minuto o 10 round consecutivi, per falsificare un documento occorre da 1 minuto a 1d4 minuti per pagina, individuare una falsificazione usando :ref:`Linguistica<linguistica>` richiede 1 round per pagina esaminata.

Ritentare
----------
Sì.

Speciale
------------
Bisogna essere addestrati in questa abilità per utilizzarla, ma si può sempre tentare di leggere un testo arcaico o strano scritto nei propri linguaggi razziali bonus. Inoltre, si può sempre tentare di individuare una falsificazione.

.. _nuotare:

----------------------------------
Nuotare (For - Penalità Armatura)
----------------------------------
.. contents:: In breve:
	:local:

Con questa abilità, si è in grado di nuotare, anche in acque tempestose.

Prova
-------
Le prove di :ref:`Nuotare<nuotare>` vanno effettuate una volta per round mentre ci si trova in acqua. 

Un successo permette di nuotare alla metà della propria velocità come :ref:`Azione di Round Completo<azione_di_round_completo>` o a un quarto della propria velocità come :ref:`Azione di Movimento<azione_movimento>`. 

Se il tiro fallisce di 4 o meno semplicemente non si avanza nell’acqua, ma se si fallisce
di 5 o più si finisce sott’acqua.

Quando si è sott’acqua, a prescindere che sia perché si è fallita la prova di :ref:`Nuotare<nuotare>` o perché si sta nuotando in profondità intenzionalmente, bisogna trattenere il fiato: si può trattenere il fiato per un numero di round pari al doppio del proprio punteggio di Costituzione se si compiono azioni di :ref:`Movimento<azione_movimento>` o :ref:`Gratuite<azione_gratuita>` e si perde un round extra per ogni azione :ref:`Standard<azione_standard>` o di :ref:`Round Completo<azione_di_round_completo>`. 

.. Note::
	Effettivamente, un personaggio impegnato in combattimento può trattenere il fiato solo per la metà del normale. 

Dopo questo periodo di tempo, bisogna effettuare una prova di Costituzione con CD 10 ad ogni round per continuare a trattenere il fiato, aumentandone la CD di 1 ogni volta. Quando si fallisce la prova di Costituzione, si comincia ad annegare. 

.. figure:: Immagini/Nuotare.png
    :align: right
    :width: 300

La CD per la prova di :ref:`Nuotare<nuotare>` dipende dall’acqua, come mostrato nella
tabella.

Per ogni ora nuotata, occorre superare una prova di :ref:`Nuotare<nuotare>` con CD 20 oppure si subiscono 1d6 danni non letali per l’affaticamento.

Azione
--------
Una prova riuscita di :ref:`Nuotare<nuotare>` permette di nuotare alla metà della propria velocità come :ref:`Azione di Round Completo<azione_di_round_completo>` o a un quarto della propria velocità come :ref:`Azione di Movimento<azione_movimento>`.

Speciale
--------
Una creatura con una velocità di nuoto può muoversi in acqua alla velocità indicata senza dover compiere prove di :ref:`Nuotare<nuotare>` e riceve bonus razziale +8 alle prove di :ref:`Nuotare<nuotare>` per compiere manovre speciali o evitare pericoli. La creatura può sempre prendere 10 alla prova di :ref:`Nuotare<nuotare>`, anche se distratta o minacciata mentre nuota. Una creatura di questo tipo può utilizzare l’azione :ref:`Correre<azione_correre>` mentre nuota, purché si sposti in linea retta.

Se si possiede :ref:`Atletico<tal_atletico>` si riceve un bonus alle prove di :ref:`Nuotare<nuotare>`.

.. _percezione:

---------------------------
Percezione (Sag)
---------------------------
.. contents:: In breve:
	:local:

Con questa abilità si possono notare piccoli dettagli e avvertire un pericolo. La percezione comprende tutti i cinque sensi: vista, udito, tatto, gusto e olfatto.

Prova
-------
L’abilità :ref:`Percezione<percezione>` ha numerosi utilizzi, il più importante dei quali è come prova contrapposta alla prova di :ref:`Furtività<furtività>` di una creatura per notarla e per evitare di essere colti di sorpresa. 

Una prova di :ref:`Percezione<percezione>` effettuata con successo permette di notare l’avversario e si può reagire di conseguenza, ma se si fallisce l’avversario può effettuare varie azioni, tra cui avvicinarsi di soppiatto ed attaccare.

Questa abilità viene utilizzata anche per individuare piccoli dettagli nell’ambiente con CD dipendente dalla distanza, il tipo di ambiente, e quanto individuabile sia il dettaglio: le tabelle seguenti forniscono alcuni spunti da seguire, ma la scelta in generale rimane al GM.

.. figure:: Immagini/Percezione.png
    :align: left
    :width: 300

.. figure:: Immagini/Percezione2.png
    :align: right
    :width: 300

Azione
--------
Molte prove di :ref:`Percezione<percezione>` sono reazioni, effettuate in risposta a stimoli d’osservazione, ma cercare intenzionalmente stimoli è un’:ref:`Azione di Movimento<azione_movimento>`.

Ritentare
---------
Sì. 

Si può cercare di percepire qualcosa che prima è sfuggito, finché esso rimane presente.

Speciale
---------
:ref:`Elfi`, :ref:`mezzelfi`, :ref:`gnomi` e :ref:`halfling` ricevono bonus razziale +2 alle prove di :ref:`Percezione<percezione>`. Creature con :ref:`Fiuto<capacità_fiuto>` hanno bonus +8 alle prove per individuare un odore, mentre quelle con :ref:`Percezione Tellurica<capacità_percezione_tellurica>` hanno bonus +8 contro creature a contatto con il terreno ed effettuano automaticamente queste prove entro la loro portata. Un incantatore con un :ref:`Famiglio<famiglio_mago>` falco o gufo riceve bonus +3. 

Se si possiede :ref:`Allerta<tal_allerta>`, si riceve un bonus alle prove di :ref:`Percezione<percezione>`.


.. _professione:

----------------------------------
Professione (Sag - Addestramento)
----------------------------------
.. contents:: In breve:
	:local:

Con questa abilità si è esperti in un mestiere specifico. 

Come per l’abilità :ref:`Artigianato<artigianato>`, :ref:`Conoscenze<conoscenze>` e :ref:`Intrattenere<intrattenere>`, :ref:`Professione<professione>` in realtà è una serie di abilità separate: è possibile avere diverse abilità :ref:`Professione<professione>`, ognuna con i suoi
gradi. 

Mentre un’abilità di :ref:`Artigianato<artigianato>` rappresenta la capacità di creare o realizzare un oggetto, una di :ref:`Professione<professione>` rappresenta un’attitudine a una vocazione che richiede una più ampia gamma di conoscenze meno specifiche. 

I mestieri più comuni che rientrano nella categoria di Professione sono:

*	Allevatore
*	Architetto
*	Avvocato
*	Barcaiolo
*	Bibliotecario
*	Birraio
*	Boscaiolo
*	Cacciatore
*	Carrettiere
*	Conciatore
*	Contabile
*	Contadino
*	Cortigiano
*	Cuoco
*	Erborista
*	Farmacista
*	Fornaio
*	Giardiniere
*	Giocatore d’azzardo
*	Ingegnere
*	Levatrice
*	Locandiere
*	Macellaio
*	Marinaio
*	Mercante
*	Minatore
*	Mugnaio
*	Pastore
*	Pescatore
*	Scrivano
*	Soldato
*	Stalliere
*	Taglialegna

Prova
------
Si può guadagnare metà del risultato della prova di :ref:`Professione<professione>` in monete d’oro per ogni settimana di lavoro. Si sa come utilizzare gli arnesi del mestiere, come eseguire le mansioni giornaliere della propria professione, come supervisionare aiutanti inesperti e come affrontare i problemi più comuni. 

Si può anche rispondere a domande sulla propria :ref:`Professione<professione>`: quelle semplici hanno CD 10, mentre quelle più complesse hanno CD 15 o superiore.

Azione
------
Non applicabile. 

Una singola prova di solito rappresenta una settimana di lavoro.

Ritentare
------------
Varie. 

Un tentativo di utilizzare un’abilità di :ref:`Professione<professione>` per guadagnare uno stipendio non può essere ripetuto e qualsiasi stipendio settimanale abbia portato il risultato della prova rimane fisso, anche se dopo una settimana può essere effettuata un’altra prova per determinare un altro stipendio per il successivo periodo di tempo.

Un tentativo di compiere qualche mansione specifica di solito può essere ritentato.

Senza Addestramento
------------------------
Lavoratori e assistenti non addestrati, cioè senza gradi in :ref:`Professione<professione>`, guadagnano 1 moneta d’argento al giorno.

Speciale
----------
Gli :ref:`gnomi` ricevono bonus +2 alle abilità di :ref:`Professione<professione>` di loro scelta.

.. _raggirare:

---------------------------
Raggirare (Car)
---------------------------
.. contents:: In breve:
	:local:

Con questa abilità si sa come ingannare e mentire.

Prova
------
Una prova di :ref:`Raggirare<raggirare>` è contrapposta ad una di :ref:`Intuizione<intuizione>` da parte della vittima. 

.. figure:: Immagini/Raggirare.png
    :align: right
    :width: 300

Il successo indica che la vittima crede a ciò che le si dice, ma le prove sono modificate in base alla credibilità o meno delle proprie affermazioni fino al punto che alcune bugie sono così improbabili che è impossibile convincere qualcuno che siano vere (a discrezione del GM).

********
Fintare
********
È possibile utilizzare :ref:`Raggirare<raggirare>` per :ref:`Fintare<azione_fintare>` in combattimento in modo che il bersaglio perda il suo bonus di Destrezza alla CA contro il prossimo :ref:`Attacco in Mischia<azione_attacco_in_mischia>` che si compie contro di lui. La CD in tal caso è 10 + BAB dell’avversario + Mod Sag dell’avversario, ma se l’avversario è addestrato in :ref:`Intuizione<intuizione>`, la CD è 10 + bonus :ref:`Intuizione<intuizione>`
dell’avversario, se maggiore. 

********************************
Trasmettere un Messaggio Segreto
********************************
È possibile utilizzare :ref:`Raggirare<raggirare>` per trasmettere un messaggio ad un altro personaggio senza farlo capire ad altri usando allusioni per celare il vero messaggio. La CD è 15 per i messaggi semplici e 20 per messaggi complessi. 

Se si supera la prova si riesce nel tentativo di trasmettere il messaggio al bersaglio, sempre che si sia comunicato in un linguaggio che egli è in grado di capire. Un fallimento di 5 implica la trasmissione di informazioni sbagliate. 

Chiunque ascolti lo scambio può compiere una prova di :ref:`Intuizione<intuizione>` contrapposta alla prova di :ref:`Raggirare<raggirare>` per decifrare il messaggio.

Azione
-------
Una prova di ingannare qualcuno richiede almeno 1 round, ma può anche richiedere più tempo se si tenta qualcosa di complicato (a discrezione del GM), mentre Fintare in combattimento è un’:ref:`Azione Standard<azione_standard>`.

Una prova di :ref:`Raggirare<raggirare>` per trasmettere un messaggio segreto richiede il doppio del tempo neccessario a trasmetterlo normalmente.

Ritentare
----------
Generalmente, se non si riesce ad ingannare qualcuno, ulteriori tentativi di :ref:`Raggirare<raggirare>` sulla stessa persona subiscono penalità –10 o possono risultare impossibili.

Fintare in combattimento invece si può ritentare liberamente, anche se si è fallita la prova. 

Si può anche ritentare se si è fallito il primo tentativo di trasmettere un messaggio segreto.

Speciale
---------
Un incantatore con un :ref:`Famiglio<famiglio_mago>` vipera riceve bonus +3 alle prove di :ref:`Raggirare<raggirare>`.

Se si possiede :ref:`Ingannevole<tal_ingannevole>` si riceve un bonus alle prove di :ref:`Raggirare<raggirare>`.

.. _rapidità_di_mano:

---------------------------------------------------------------------------------
Rapidità di Mano (Des - Penalità Armatura - Addestramento)
---------------------------------------------------------------------------------
.. figure:: Immagini/AbilitaRapiditaDiMano.png

.. contents:: In breve:
	:local:

Con questa abilità si può borseggiare, estrarre un’arma nascosta, oppure compiere altre azioni senza essere notati.

Prova
------
Una prova di :ref:`Rapidità di Mano<rapidità_di_mano>` con CD 10 permette di sgraffignare un oggetto incustodito grande come una moneta. Anche piccoli giochi di prestigio come far sparire una moneta sono effettuati con CD 10, a meno che un osservatore non sia intenzionato a notare dove finisca l’oggetto.

Quando si utilizza questa abilità sotto stretta osservazione, la prova di abilità è contrapposta dalla prova di :ref:`Percezione<percezione>` dell’osservatore. Il successo della prova dell’osservatore non impedisce di compiere l’azione, ma solo di compierla senza essere notati.

Si può tentare di nascondersi addosso un oggetto piccolo (compresa un’arma leggera come un’ascia o un’arma a distanza facilmente occultabile come dardo, fionda o balestra a mano). In tal caso prova di :ref:`Rapidità di Mano<rapidità_di_mano>` viene contrapposta dalla prova di :ref:`Percezione<percezione>` di chiunque stia osservando o perquisendo. Nel secondo caso,
l’esaminatore riceve bonus +4 alla prova dato che generalmente è più facile trovare un oggetto
che nasconderlo. 

Un pugnale è più facile da nascondere della maggior parte delle armi leggere e conferisce bonus
+2 alla prova per nasconderlo, mentre un oggetto straordinariamente piccolo (moneta, shuriken o
anello) conferisce bonus +4 alla prova per nasconderlo e un abito pesante o ingombrante (come
un mantello) conferisce bonus +2 alla prova.

Estrarre un oggetto nascosto è un’:ref:`Azione Standard<azione_standard>` che non provoca :ref:`Attacchi di Opportunità<azione_attacco_di_opportunità>`.

.. figure:: Immagini/RapiditaDiMano.png
    :align: right
    :width: 300

Se si cerca di sottrarre qualcosa a un’altra creatura occorre effettuare una prova di :ref:`Rapidità di Mano<rapidità_di_mano>` con CD 20 e l’avversario effettua una prova di :ref:`Percezione<percezione>` per individuare il tentativo. L’avversario individua il tentativo se il risultato della sua prova supera il risultato della prova di :ref:`Rapidità di Mano<rapidità_di_mano>`, indipendentemente dal fatto che si sia riusciti o meno a prendere l’oggetto. 

.. Note::
	Non si può utilizzare questa abilità per prendere un oggetto ad un’altra creatura durante un combattimento se la creatura si è accorta della vostra presenza.

Si può anche utilizzare :ref:`Rapidità di Mano<rapidità_di_mano>` per intrattenere un pubblico come se si stesse utilizzando :ref:`Intrattenere<intrattenere>` recitando il ruolo di prestigiatore, giocoliere e così via.

Azione
--------
Qualsiasi prova di :ref:`Rapidità di Mano<rapidità_di_mano>` è normalmente un’:ref:`Azione Standard<azione_standard>`, ma si può compiere una prova con un’:ref:`Azione Gratuita<azione_gratuita>` prendendo penalità –20 alla prova.

Ritentare
---------
Sì, ma dopo un primo fallimento un tentativo contro lo stesso bersaglio (o quando si è controllati dallo stesso osservatore che ha notato il tentativo precedente) aumenta la CD dell’azione di 10.

Senza Addestramento
--------------------
Una prova senza addestramento di :ref:`Rapidità di Mano<rapidità_di_mano>` è una semplice prova di Destrezza, ma non si può superare una prova di CD maggiore di 10, tranne che per nascondersi un oggetto addosso.

Speciale
--------
Se si possiede :ref:`Manolesta<tal_manolesta>`, si riceve un bonus alle prove di :ref:`Rapidità di Mano<rapidità_di_mano>`.

.. _sapienza_magica:

-----------------------------------------
Sapienza Magica (Int - Addestramento)
-----------------------------------------
.. contents:: In breve:
	:local:

Con questa abilità si è esperti di magia, di incantesimi e di creazione di oggetti magici e si è in grado di identificare gli incantesimi che vengono lanciati.

Prova
--------
:ref:`Sapienza Magica<sapienza_magica>` è usata ogni volta che le proprie conoscenze ed abilità relative agli incantesimi o agli oggetti magici entrano in gioco. 

Questa abilità inoltre è usata per identificare le proprietà degli oggetti magici in proprio possesso con l’uso di incantesimi come :ref:`Identificare<inc_identificare>` e :ref:`Individuazione del Magico<inc_individuazione_del_magico>`. 

La CD di questa prova varia a seconda del compito:

.. figure:: Immagini/SapienzaMagica.png

Azione
-------
L’identificazione di un incantesimo che sta per essere lanciato non richiede azione, ma occorre
essere in grado di vedere chiaramente chi sta lanciando l’incantesimo mentre lo fa, e ciò incorre nelle stesse penalità di una prova di :ref:`Percezione<percezione>` dovute alla distanza, delle scarse condizioni di luce e di altri fattori. 

Imparare un incantesimo da un libro degli incantesimi richiede 1 ora per livello dell’incantesimo (gli incantesimi di livello 0 richiedono 30 minuti) e preparare un incantesimo da un libro degli incantesimi preso in prestito non aggiunge altro tempo a quello richiesto dalla preparazione dell’incantesimo. 

Effettuare una prova di :ref:`Sapienza Magica<sapienza_magica>` per creare un oggetto magico fa parte del processo di creazione, mentre tentare di appurare le proprietà di un oggetto magico richiede 3 round per oggetto da identificare e occorre poter esaminare attentamente l’oggetto.

Ritentare
---------
Non si possono ritentare le prove effettuate per identificare un incantesimo, se non si riesce ad imparare un incantesimo da un libro degli incantesimi o da una pergamena occorre attendere almeno 1 settimana prima che si possa ritentare, se non si riesce a preparare un incantesimo da un libro degli incantesimi preso in prestito non si può ritentare fino al giorno seguente. 

Quando si usa :ref:`Identificare<inc_identificare>` e :ref:`Individuazione del Magico<inc_individuazione_del_magico>` per apprendere le proprietà degli oggetti magici, si può tentare di appurare le proprietà di un oggetto specifico una volta al giorno. 

I tentativi aggiuntivi rivelano gli stessi risultati.

Speciale
---------
Se si è un :ref:`Mago` specialista si riceve bonus +2 alle prove di :ref:`Sapienza Magica<sapienza_magica>` effettuate per identificare, imparare e preparare gli incantesimi della propria scuola, ma si subisce penalità –5 a per le scuole opposte alla propria.

Un elfo riceve bonus razziale +2 alle prove di :ref:`Sapienza Magica<sapienza_magica>` per identificare le proprietà degli oggetti magici.

Se si possiede :ref:`Vocazione Magica<tal_vocazione_magica>` si riceve un bonus alle prove di :ref:`Sapienza Magica<sapienza_magica>`.

.. _scalare:

----------------------------------
Scalare (For - Penalità Armatura)
----------------------------------
.. contents:: In breve:
	:local:

Con questa abilità si possono scalare superfici verticali, dalle mura cittadine alle pareti rocciose.

Prova
-----
Con una prova di :ref:`Scalare<scalare>` effettuata con successo si può salire verso l’alto, il basso o in orizzontale su una parete o un’altra superficie in pendenza fino anche a un soffitto con appigli, ad un quarto della propria velocità. 

.. Note::
	Per pendenza si intende qualsiasi superficie con un’inclinazione inferiore a 60 gradi e per parete si intende qualsiasi superficie inclinata per 60 gradi o più.

.. figure:: Immagini/Scalare.png
    :align: left
    :width: 325

Una prova di :ref:`Scalare<scalare>` fallita di 4 o meno significa che non si fanno progressi, ma se fallita di 5 o più significa che si cade dall’altezza già raggiunta.

.. figure:: Immagini/Scalare2.png
    :align: right
    :width: 325

La CD  dipende dalle condizioni della scalata e da cosa si scala, come riportato nelle tabelle. 

Bisogna avere entrambe le mani libere per scalare, ma si può rimanere appesi ad una parete con una mano mentre si lancia un incantesimo o s’intraprende qualche altra azione che richiede l’uso di una sola mano. 

Durante l’arrampicata, non è possibile spostarsi per evitare un colpo, quindi **si perde il bonus di Destrezza alla CA**. Inoltre non è possibile utilizzare lo scudo e ogni volta che
si subisce danno occorre effettuare una prova :ref:`Scalare<scalare>` con la CD della parete o della pendenza. Il fallimento significa che si cade dall’attuale altezza e si subiscono i danni appropriati per la caduta.

******************
Scalare Accelerato
******************
Si tenta di scalare più velocemente del normale: accettando penalità –5, ci si può muovere a metà della propria velocità.

****************************************
Farsi i propri Appigli
****************************************
Ci si può creare degli appigli per mani e piedi piantando :ref:`Chiodi da Rocciatore<equip_chiodi_da_rocciatore>` in una parete. Per farlo occorre 1 minuto per chiodo, ed è necessario un chiodo ogni 150 cm di distanza.

Come ogni superficie con appigli, una parete con i chiodi da rocciatore ha CD 15. 

Allo stesso modo, uno scalatore con un’ascia o un attrezzo simile può crearsi degli appigli su una superficie di ghiaccio.

**************************
Aggrapparsi mentre si Cade
**************************
È praticamente impossibile aggrapparsi ad una parete mentre si cade: per riuscirci, bisogna effettuare una prova di :ref:`Scalare<scalare>` con **CD = CD della parete + 20**. 

È molto più facile aggrapparsi ad una pendenza con **CD = CD della pendenza + 10**.

**************************************************
Afferrare un Personaggio che Cade 
**************************************************
Se qualcuno che si sta arrampicando sopra o accanto a sè cade è possibile tentare di afferrarlo se è a portata di braccio.

Farlo richiede superare un :ref:`Attacco di Contatto in Mischia<azione_attacco_di_contatto_in_mischia>` contro il personaggio che cade, che può rinunciare a qualsiasi bonus di Destrezza alla CA se lo desidera. 

Se l’attacco ha successo, si deve tentare una prova di :ref:`Scalare<scalare>` con **CD = CD della parete + 10**. Il successo indica che si è riusciti ad afferrare il personaggio che cade, ma il suo peso totale, compreso l’equipaggiamento, non può eccedere il proprio limite di carico massimo altrimenti si rischia di cadere a propria volta. Se si fallisce la prova di 4 o meno non si riesce a fermare la caduta, ma se si fallisce di 5 o più si cade con l’altro personaggio.

Azione
---------
:ref:`Scalare<scalare>` fa parte del movimento quindi è generalmente parte di un’:ref:`Azione di Movimento<azione_movimento>` e può essere combinato con altri tipi di movimento in un’:ref:`Azione di Movimento<azione_movimento>`. 

Ogni azione che comprende lo scalare richiede una prova di :ref:`Scalare<scalare>` separata, ma aggrapparsi o afferrare un personaggio che cade non richiede un’azione.

Speciale
---------
Si può usare una corda per tirare su un personaggio (oppure calarlo) solo con la forza. Si può sollevare fino al doppio del carico massimo in questo modo.

Una creatura con una velocità di scalata ha bonus razziale +8 alle prove di :ref:`Scalare<scalare>`; la creatura deve compiere una prova di :ref:`Scalare<scalare>` per arrampicarsi su una parete o pendenza con una CD superiore a 0, ma può sempre scegliere di prendere 10 anche se di fretta o minacciata mentre si arrampica. 

Se una creatura con una velocità di scalata sceglie di compiere una scalata accelerata, si muove al doppio della sua velocità di scalata (o alla sua velocità sul terreno, quale sia più lenta) e compie una singola prova di Scalare con penalità –5. Questa creatura mantiene il bonus di Destrezza alla Classe Armatura mentre si arrampica e gli avversari non ricevono alcun bonus particolare agli attacchi portati contro di essa. 

In ogni caso non si può :ref:`Correre<azione_correre>` mentre ci si arrampica.

Se si possiede :ref:`Atletico<tal_atletico>` si riceve un bonus alle prove di :ref:`Scalare<scalare>`.



.. _sopravvivenza:

---------------------------
Sopravvivenza (Sag)
---------------------------
.. contents:: In breve:
	:local:

Con questa abilità si è in grado di sopravvivere e orientarsi nelle terre selvagge, nonché seguire piste e tracce lasciate dagli altri.

Prova
------
Si può proteggere e nutrire se stessi e altre persone all’interno di territori selvaggi. La tabella seguente fornisce la CD per varie imprese che richiedono l’uso dell’abilità :ref:`Sopravvivenza<sopravvivenza>`:

.. figure:: Immagini/Sopravvivenza.png
    :align: center
    :width: 300

**************
Seguire tracce
**************
Trovare tracce o seguirle per 1,5 km richiede una prova di :ref:`Sopravvivenza<sopravvivenza>` e occorre effettuarne un’altra ogni volta che le tracce diventano difficili da seguire. 

Se non si è addestrati in questa abilità si possono effettuare prove per trovare le tracce, ma soltanto se la CD 10 o meno. In alternativa, si può utilizzare l’abilità :ref:`Percezione<percezione>` per trovare un’impronta o una traccia del passaggio di una creatura usando le stesse CD, ma non si può comunque utilizzare :ref:`Percezione<percezione>` per seguire le tracce, persino se qualcun altro le ha già individuate.

Quando si seguono le tracce, ci si muove alla metà della propria velocità normale, alla propria velocità normale con penalità –5 o fino al doppio della propria velocità normale con penalità –20. 

La CD dipende dalla superficie e dalle condizioni prevalenti, come riportato nella seguente tabella:

.. figure:: Immagini/Sopravvivenza2.png
    :align: center
    :width: 300

Terreno Molto Soffice
*********************
Qualsiasi superficie su cui si possono lasciare impronte profonde e chiare. 

Esempi: neve fresca, polvere spessa e fango umido.

Terreno Soffice
****************
Qualsiasi superficie abbastanza soffice da cedere alla pressione, ma più compatta del fango umido o della neve fresca, in cui la creatura lascia impronte frequenti, ma poco profonde.

Terreno Compatto
*****************
La maggior parte delle normali superfici all’aperto, o superfici al chiuso molto sporche o soffici. 

La creatura potrebbe lasciare alcune tracce (rami spezzati o capelli), ma solo impronte occasionali e parziali.

Esempi: prati, campi, boschi, spessi tappeti, pavimenti alquanto sporchi o impolverati.

Terreno Duro
************
Qualsiasi superficie che non conserva affatto le impronte, come la nuda roccia o i pavimenti delle abitazioni. I letti di fiumi per lo più rientrano in questa categoria, poiché le impronte lasciate sono confuse o vengono lavate via. 

La creatura lascia solo tracce di segni come piedi trascinati o ciottoli spostati.

In Generale
*************
.. figure:: Immagini/Sopravvivenza3.png
    :align: right
    :width: 300

Esistono numerosi modificatori che si possono applicare alla prova di :ref:`Sopravvivenza<sopravvivenza>`, come riportato nella tabella.

Azione
-------
Varie. 

Una singola prova può rappresentare un’attività che si svolge nell’arco di ore di un intero giorno. Una prova di :ref:`Sopravvivenza<sopravvivenza>` effettuata per seguire tracce è minimo un’:ref:`Azione di Round Completo<azione_di_round_completo>`, ma può richiedere più tempo.

Ritentare
----------
Varie. 

Per cavarsela in territori selvaggi e per ottenere i bonus ai TS su Tempra, occorre effettuare la prova ogni 24 ore e il risultato si applica fino alla prova successiva.

Per evitare di perdersi e per evitare i pericoli naturali occorre effettuare la prova quando necessario. Nuovi tentativi non sono permessi. 

Per seguire tracce si può riprovare una prova fallita dopo 1 ora di ricerca all’esterno e 10 minuti all’interno.

Speciale
-----------
Un personaggio addestrato in :ref:`Sopravvivenza<sopravvivenza>` è automaticamente in grado di determinare dove si trovi il nord rispetto alla sua posizione.

Un :ref:`ranger` ottiene un bonus alle proprie prove per trovare o seguire le tracce di un nemico prescelto.

Se si possiede :ref:`Autosufficiente<tal_autosufficiente>` si riceve un bonus alle prove di :ref:`Sopravvivenza<sopravvivenza>`.

.. _utilizzare_congegni_magici:

------------------------------------------------------
Utilizzare Congegni Magici (Car - Addestramento)
------------------------------------------------------
.. contents:: In breve:
	:local:

.. figure:: Immagini/AbilitaUtilizzareCongegniMagici.png
    :align: left
    :width: 300

Con questa abilità si possono attivare oggetti magici, che altrimenti non sarebbe possibile utilizzare.

Prova
------
Si può utilizzare questa abilità per leggere un incantesimo o per attivare un oggetto magico: si può utilizzare un oggetto magico come se si avesse la capacità magica o i privilegi di classe di un’altra classe, come se si fosse di una razza diversa, e come se si avesse un
allineamento differente.

Occorre compiere una prova di :ref:`Utilizzare Congegni Magici<utilizzare_congegni_magici>` ogni volta che si cerca di attivare un oggetto magico, come una bacchetta. Se si sta utilizzando la prova per emulare un allineamento o qualche altra qualità comportamentale, bisogna effettuare le prove di emulazione relative ad :ref:`Utilizzare Congegni Magici<utilizzare_congegni_magici>`  una volta all’ora.

Occorre scegliere coscientemente cosa emulare: bisogna sapere cosa si sta cercando di emulare quando si effettua una prova a quel proposito. Le CD per le varie azioni che coinvolgono l’uso
di :ref:`Utilizzare Congegni Magici<utilizzare_congegni_magici>`  sono riassunte nella tabella seguente:

.. figure:: Immagini/UtilizzareCongegniMagici.png
    :align: center
    :width: 300

*******************
Attivare alla Cieca
*******************
Alcuni oggetti magici sono attivati da parole, pensieri e azioni speciali. 

È possibile attivare tali oggetti come se si stesse usando la parola, il pensiero o l’azione di attivazione anche se non lo si sta facendo e anche se non la si conosce, ma bisogna utilizzare una qualche attività equivalente per compiere la prova: parlare, roteare l’oggetto o tentare in altro modo di attivarlo. 

Si riceve bonus speciale +2 se si è attivato l’oggetto almeno un’altra volta in passato. 

Se si fallisce di 9 o meno non si riesce ad attivare l’oggetto, ma per 10 o più, si subisce
un malfunzionamento: l’energia magica viene comunque sprigionata, ma non nel modo desiderato. 

I malfunzionamenti in genere implicano che l’oggetto ha effetto sul bersaglio sbagliato o che viene sprigionata energia magica incontrollata che infligge 2d6 danni a chi utilizza l’oggetto. 

Questo malfunzionamento è in aggiunta alla possibilità del normale malfunzionamento che capita quando si lancia un incantesimo da una pergamena che non si potrebbe altrimenti utilizzare.

*********************************
Decifrare un Incantesimo Scritto
*********************************
Funziona esattamente come decifrare con l’abilità :ref:`Sapienza Magica<sapienza_magica>` un incantesimo scritto, eccetto per la CD che è superiore di 5 punti. 

Decifrare un incantesimo scritto richiede 1 minuto di concentrazione.

***********************
Emulare un Allineamento
***********************
Alcuni oggetti magici hanno effetti positivi o negativi in base all’allineamento e :ref:`Utilizzare Congegni Magici<utilizzare_congegni_magici>` permette di utilizzare questi oggetti come se si fosse di un allineamento diverso. 

Si può emulare solo un allineamento alla volta.

********************************
Emulare un Privilegio di Classe
********************************
Talvolta occorre un privilegio di classe per attivare un oggetto magico. In questo caso, il proprio livello effettivo nella classe emulata è pari al risultato ottenuto con la prova di :ref:`Utilizzare Congegni Magici<utilizzare_congegni_magici>` meno 20. 

.. Caution::
	Questa abilità non permette di sfruttare il privilegio di classe di un’altra classe: permette solo di attivare oggetti magici come se si possedesse il privilegio di classe.

Se la classe di cui si sta emulando il privilegio ha un requisito d’allineamento occorre soddisfarlo, sia onestamente sia emulando un allineamento appropriato con una prova separata.

**************************************
Emulare un Punteggio Caratteristica
**************************************
Per lanciare un incantesimo da una pergamena si ha bisogno di un alto punteggio di caratteristica nella caratteristica appropriata: Intelligenza per il :ref:`mago`, Saggezza per :ref:`Incantesimi Divini<incantesimi_divini>` e Carisma per il :ref:`paladino`, :ref:`stregone` o :ref:`bardo`. 

Il proprio effettivo punteggio di caratteristica è il risultato ottenuto nella prova di :ref:`Utilizzare Congegni Magici<utilizzare_congegni_magici>` meno 15. 

Se già si possiede un punteggio sufficientemente alto per utilizzare la capacità appropriata, allora la prova non è necessaria.

******************
Emulare una Razza
******************
Alcuni oggetti magici funzionano esclusivamente per certe razze o funzionano meglio per i membri di certe razze; è possibile utilizzare tali oggetti come se si fosse della razza appropriata. 

Si può emulare una sola razza per volta. 

************************************************************************************
Utilizzare una Bacchetta, Bastone o altri Ooggetti ad Attivazione di Incantesimo
************************************************************************************
Normalmente, per utilizzare una bacchetta, bisogna possedere l’incantesimo della bacchetta nella lista degli incantesimi della propria classe, ma questo uso dell’abilità permette di utilizzare una bacchetta come se si possedesse quel particolare incantesimo nella propria lista.

************************
Utilizzare una Pergamena
************************
Normalmente per lanciare un incantesimo da una pergamena si deve possedere quell’incantesimo particolare nella lista degli incantesimi della propria classe, ma usando :ref:`Utilizzare Congegni Magici<utilizzare_congegni_magici>` si può utilizzare un tale oggetto comunque, con **CD = 20 + LI dell’incantesimo** che si sta cercando di lanciare. 

In aggiunta, lanciare un incantesimo da una pergamena richiede un punteggio minimo (10 + il livello dell’incantesimo) nella caratteristica appropriata. Se non la si possiede bisogna emularla con una prova separata.

Questo uso dell’abilità si applica anche ad altri oggetti magici a completamento di incantesimo.

Azione
--------
Nessuna. 

La prova di :ref:`Utilizzare Congegni Magici<utilizzare_congegni_magici>` viene considerata come parte dell’azione (se richiesta) per attivare l’oggetto.

Ritentare
---------
Sì, ma nel caso si ottenga un 1 naturale mentre si tenta di attivare un oggetto e si fallisce, allora non è possibile cercare di attivarlo nuovamente prima che siano passate 24 ore.

Speciale
---------
Non è possibile prendere 10 con questa abilità né collaborare sulle prove di :ref:`Utilizzare Congegni Magici<utilizzare_congegni_magici>`: solo l’utilizzatore dell’oggetto può tentare questa prova.

Se si possiede :ref:`Vocazione Magica<tal_vocazione_magica>` si riceve un bonus alle prove di :ref:`Utilizzare Congegni Magici<utilizzare_congegni_magici>`.

.. _valutare:

---------------------------
Valutare (Int)
---------------------------
.. contents:: In breve:
	:local:

Permette di stimare il valore monetario di un oggetto.

Prova
----------
Una prova di :ref:`Valutare<valutare>` con CD 20 permette di determinare il valore di un oggetto comune e se si supera la prova di 5 o più si determina anche se un oggetto ha proprietà magiche, sebbene il successo non dia la conoscenza di quali esse siano. 

Se si fallisce la prova di meno di 5 si determina il prezzo dell’oggetto con un’approssimazione
del 20% del suo reale valore, ma se si fallisce di 5 o più il prezzo non è per niente accurato ed è soggetto alla discrezione del GM. 

.. Note::
	Gli oggetti particolarmente rari o esotici potrebbero aumentare la CD della prova di 5 o più.

È possibile utilizzare la prova per determinare l’oggetto di maggior valore in un tesoro con una CD in genere di 20 ma che può aumentare fino a 30 per tesori particolarmente grandi.

Azione
-------
Valutare un oggetto richiede un':ref:`Azione Standard<azione_standard>`.

Determinare l’oggetto di maggior valore in un tesoro richiede un':ref:`Azione di Round Completo<azione_di_round_completo>`.

Ritentare
-----------
Tentativi aggiuntivi di :ref:`Valutare<valutare>` un oggetto rivelano sempre lo stesso risultato.

Speciale
---------
Un incantatore con un :ref:`Famiglio<famiglio_mago>` corvo ottiene bonus +3 alle prove di :ref:`Valutare<valutare>`.

.. _volare:

--------------------------------
Volare (Des - Penalità Armatura)
--------------------------------
.. contents:: In breve:
	:local:

Con questa abilità si è in grado di volare, attraverso l’uso di ali o della magia, e di compiere semplici o complesse manovre mentre si è in aria. 

.. Caution::
	Questa abilità non concede in ogni caso la capacità di volare.

Prova
------
Si ha in genere bisogno di effettuare una prova di :ref:`Volare<volare>` quando si tenta una manovra complessa. 

Senza la prova, una creatura che vola può restare in volo alla fine del suo turno finché si muove per una distanza maggiore di metà della sua velocità e può inoltre virare di 45 gradi sacrificando 1,5 metri di movimento, risalire a metà velocità ad una angolazione di 45 gradi e discendere a qualsiasi angolazione a velocità normale. 

Queste restrizioni si applicano solo al movimento compiuto durante il proprio turno.

All’inizio del proprio turno successivo, ci si può muovere in una direzione diversa da quella presa il turno precedente senza effettuare una prova. 

Intraprendere un’azione che viola queste regole, richiede una prova di :ref:`Volare<volare>`. La difficoltà di queste manovre varia in base alla manovra che si sta tentando, come indicato nella tabella seguente:

.. figure:: Immagini/Volare.png
    :align: center
    :width: 300

*****************
Attaccare in Volo
*****************
Non si è considerati impreparati mentre si vola. Se si vola per mezzo di ali e si subisce danno in volo, occorre effettuare una prova di :ref:`Volare<volare>` con CD 10 per evitare di perdere 3 metri di altitudine.

Questa discesa non provoca :ref:`Attacchi di Opportunità<azione_attacco_di_opportunità>` e non conta nel movimento della creatura.

*******************
Collisione in Volo
*******************
Se si vola per mezzo di ali e ci si scontra con un oggetto uguale alla propria taglia o più grande occorre immediatamente effettuare una prova di :ref:`Volare<volare>` con CD 25 per evitare di cadere a terra, subendo il relativo danno da caduta.

***********************
Evitare Danno da Caduta
***********************
Se si sta cadendo e si ha la capacità di volare si può effettuare una prova di :ref:`Volare<volare>` con CD 10 per negare il danno. 

Non si può effettuare questa prova se si sta cadendo a causa di una prova fallita di :ref:`Volare<volare>` o di una collisione.

***********************
Volare con Venti Forti
***********************
.. figure:: Immagini/Volare2.png

Volare in condizioni di vento forte aggiunge penalità alle prove di :ref:`Volare<volare>` come indicato in Tabella. 

Il campo **Taglia della Creatura** significa che le creature di quella taglia o più piccola devono superare una prova di :ref:`Volare<volare>` con CD 20 per muoversi finché la condizione di vento persiste. 
**Portata Via** significa che le creature di quella taglia o più piccola devono superare una prova di :ref:`Volare<volare>` con CD 25 o vengono spinte indietro 1d6 × 3 metri e subiscono 2d6 danni non letali. Questa prova deve essere effettuata ad ogni round in cui la creatura resta in aria. La prova per muoversi va comunque effettuate.

Azione
-------
Nessuna. 

Una prova di :ref:`Volare<volare>` non richiede un’azione e viene effettuata come parte di un’altra azione o come reazione ad una situazione.

Ritentare
---------
Varie.

Si può tentare una prova di :ref:`Volare<volare>` per compiere la stessa manovra nei round seguenti. Se si usano ali e si fallisce la prova di 5 o più, si cade a terra subendo il danno da caduta appropriato.

Speciale
----------
Un incantatore con un :ref:`Famiglio<famiglio_mago>` pipistrello riceve bonus +3 alle prove di :ref:`Volare<volare>`.

Le creature con velocità di volare trattano l’abilità come abilità di classe. Se la velocità
di volare è naturale riceve bonus (o penalità) alle prove in base alla propria manovrabilità: Maldestra –8, Scarsa –4, Media +0, Buona +4 e Perfetta +8. Le creature che non hanno una specifica manovrabilità, si presume abbiano manovrabilità media.

Una creatura più grande o più piccola della taglia Media ha bonus o penalità di taglia alle prove di :ref:`Volare<volare>` in base alla sua categoria di taglia: Piccolissima +8, Minuta +6, Minuscola +4, Piccola +2, Grande –2, Enorme –4, Mastodontica –6 e Colossale –8.

Non si possono avere gradi in questa abilità senza mezzi naturali di volare o planare. Le creature possono mettere gradi in :ref:`Volare<volare>` anche se possiedono dei mezzi affidabili
di volare giornalieri, come un incantesimo o qualche altra capacità speciale.

Se si possiede :ref:`Acrobatico<tal_acrobatico>` si riceve un bonus alle prove di :ref:`Volare<volare>`.