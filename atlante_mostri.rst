
.. _atlante_mostri:

=================================================
Mostri
=================================================

.. contents:: Contenuti

.. _diavolo_sandpoint:

Diavolo di Sandpoint
========================
Questa logora creatura dalle sembianze di un cavallo cammina per lo più eretto. Ali, una coda da drago e una bocca piena di zanne completano il suo aspetto abietto.

-------
Scheda
-------
.. figure:: Immagini/Atlante/DiavoloSandpoint.png
	:align: right
	:width: 400

.. rubric:: Base

:GS: 8
:Tipo: Esterno
:Terreno: Rovine/Dungeon
:Clima: Temperato
:Punti Esperienza: 4800
:Allineamento: NM
:Taglia: Enorme
:Iniziativa: +3
:Sensi: 

	*	:ref:`Scurovisione<scurovisione>` 18m 
	*	:ref:`Visione Crepuscolare<visione_crepuscolare>`
	*	:ref:`Fiuto<capacità_fiuto>`
	* 	:ref:`Percezione<percezione>` +18

.. rubric:: Difesa

:CA: +4 Des, +10 naturale, -2 Taglia

	*	Base 22
	*	Contatto 13
	*	Impreparato 18
:pf: 114 (12d10 + 48)
:Tempra: +12
:Riflessi: +7 
:Volontà: +11
:RD: 5/ferro freddo
:Immunità: Fuoco, Effetti di Paura
:RI: 19

.. rubric:: Attacco

:Velocità: 12m, Volare 18m (Scarsa)
:Mischia: Morso +17 (2d6+6/19–20), 2 Zoccoli (1d8+6)
:Spazio: 3m
:Portata: 1.5m
:Attacchi Speciali: Ruggito, Respiro Infernale, Calcio, Calpestare (2d6+9, CD 22)
:Capacità Magiche: LI 10°, Concentrazione +13
	
	**A volontà**: :ref:`Nube di Nebbia<inc_nube_di_nebbia>`, :ref:`Folata di Vento<inc_folata_di_vento>`, :ref:`Pirotecnica<inc_pirotecnica>` (CD 15)

	**3 al giorno**: :ref:`Porta Dimensionale<inc_porta_dimensionale>`, :ref:`Allucinazione Mortale<inc_allucinazione_mortale>` (CD 17)

.. rubric:: Statistiche

:For: 22
:Des: 17
:Cos: 19
:Int: 8
:Sag: 17
:Car: 16
:Att Base: +12
:BMC: +19
:DMC: 33 (37 contro sblianciare)
:Talenti: :ref:`Attacco Rapido<tal_attacco_rapido>`, :ref:`Colpo Vitale<tal_colpo_vitale>`, :ref:`Colpo Vitale Migliorato<tal_colpo_vitale_migliorato>`, :ref:`Fluttuare<mostri_fluttuare>`, :ref:`Mobilità<tal_mobilità>`, :ref:`Schivare<tal_schivare>`
:Abilità: :ref:`Conoscenze<conoscenze>` (geografia) +5, :ref:`Intimidire<intimidire>` +12, :ref:`Furtività<furtività>` +14, :ref:`Percezione<percezione>` +18, :ref:`Sopravvivenza<sopravvivenza>` +18, :ref:`Volare<volare>` +12
:Lingue: Abissale, Varisiano

.. rubric:: Ecologia

:Ambiente: Qualsiasi (Varisia)
:Organizzazione: Solitario
:Tesori: Accidentale

----------------
Abilità Speciali
----------------

Ruggito (Sop)
--------------
:Tipo: Azione Standard

Quando il *Diavolo di Sandpoint* strilla tutte le creature all'interno di un raggio di 90m devono effettuare un TS su Volontà con CD 19 per evitare di entrare nel panico per 2d4 round. 

Questo è un effetto sonico di paura che influenza la mente. Le creature sono poi immuni al colpo per le successive 24 ore. La CD è basata su Carisma.

Respiro Infernale (Sop)
-------------------------
:Tipo: Azione Standard

Una volta ogni 1d4 round il *Diavolo di Sandpoint* può rilasciare un getto di fiamme infernali dalla sua bocca, riempiendo un cono di 9m e causando 10d6 danni da fuoco (con un TS su riflessi con CD 20 per dimezzare il danno).

Chiunque subisca danno da questo respiro deve anche fare un TS su Volontà 20 per evitare di rimanere maledetto dalle fiamme. In caso contrario subisce -4 su tutti i tiri per attaccare, i TS e le prove abilità per un numero di giorni pari al danno subito, la sua pelle orribilmente bruciata sul posto a prescindere dalle cure applicate. 

Il LI di questa maledizione è 12. Il CD per entrambi i tiri è basato su Cos.

Calcio (Str)
------------
Gli attacchi con gli zoccoli del *Diavolo di Sandpoint* sono attacchi principali che causano danni ad area contundenti. 

-----------------
Descrizione
-----------------
Il leggendario *Diavolo di Sandpoint* ha infestato le terre prossime alla da cui prende il nome, sulla costa occidentale Varisiana, per oltre un decennio. 

Appare più frequentemente nelle notti nebbiose e senza luna, a volte lasciando strane prove del suo passaggio, come tracce di zoccoli dal puzzo di zolfo in posti eccentrici (come le grondaie di ripidi tetti). 

Ladro di provviste, rapitore di bambini e portatore di dolore: innumerevoli storie orbitano attorno alla bestia. Alcune narrano dell'origine antica della creatura, evocata magicamente da un'era dimenticata. Altre ne descrivono la nascita da una strega maledetta, dopo aver spezzato un patto col diavolo. Tuttavia forse il racconto più inquietante cita la possibilità che la creatura non sia che una delle molte che si nascondono nelle terre incivilizzate.