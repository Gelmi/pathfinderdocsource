
.. _atlante_razze:

=================================================
Razze
=================================================

.. contents:: Contenuti

.. _varisiani:

Varisiani
========================

Linguaggi
	Comune, Varisiano
Regioni Preferite
	Lastwall, Nidal, Nirmathas, Numeria, Terre dei Re Linnorm, Ustalav, :ref:`Varisia<varisia>`
Religioni Preferite
	Abadar, Calistria, Cayden Cailean, Desna, Erastil, Gozreh, Norgorber, Sarenrae, Shelyn, Urgathoa
Nomi Femminili
	Alika, Alinza, Anca, Bordana, Carmelizzia, Ilinica, Iolana, Luminita, Mirelinda, Narcizia, Nicinniana, Piousa, Zeldana, Zriorica
Nomi Maschili
	Alezandaru, Andrezi, Dortlin, Eugeni, Henric, Ionacu, Iozif, Kazallin, Marduzi, Silvui, Skender, Tiberiu, Viorec, Zandu, Zstelian

.. figure:: Immagini/Atlante/Varisiani.png
	:align: left
	:width: 250

Aspetto
	I *Varisiani* hanno la pelle ambrata e occhi larghi ed espressivi, spesso di colori inusuali come viola e oro. 

	Il colore dei loro capelli varia nettamente: dal platino al biondo fino al rosso scuro, marrone e nero. Sono molto rari i colori di capelli considerati inusuali tra i *Varisiani*.

	Tendono ad essere persone flessuose e dalle lunghe gambe, gli uomini spesso con difficoltà a crescere una barba, facendo delle barbe e baffi irregolari dei delinquenti Sczarni un tratto ben noto.

Molti *Varisiani* sono girovaghi e nomadi, muovendosi tra le terre in carovane e fermandosi solo per recitare in performance esotiche o per raggirare e sedurre i locali. Altrettanti *Varisiani* si sistemano per formare piccole città o, come nel caso di *Ustalav*, intere città e nazioni. 

Nessuno può però negare la stereotipata visione di artisti e truffatori che danno ai *Varisiani* una pessima nomea. I più rispettano i *Varisiani* per le loro antiche tradizioni e vaste conoscenze, ma per lo stesso motivo dubitano delle loro intenzioni: per la gente comune questi colorati viandanti che non si sistemano mai ma guizzano come farfalle tra le terre risultano affascinanti, ma anche un po' spaventosi.

Un viaggiatore può sedersi in qualsiasi taverna di *Avistan* e sentire una storia riguardante i *Varisiani*, di come non costruiscano mai città o seminino il raccolto, come vivano in carri che li trasportano tra le terre, come cantino e coprano i loro corpi con gioielli e tatuaggi intricati e di come un *Varisiano* abbia una volta rapinato lo zio, la moglie, il fratello o il migliore amico in un'elaborata truffa.

La saggezza popolare suggerisce che gli osservatori dovrebbero vedere i bellissimi, esotici *Varisiani* dalla distanza, come se guardassero una tigre aggirarsi furtivamente nella giungla. 

I racconti dell'infedeltà e degli inganni *Varisiani* solitamente derivano dagli incontri con gli **Sczarni**: famiglie di criminalità organizzata *Varisiane* dedite a ruberie e truffe.

Gli *Sczarni* viaggiano meno frequentemente dei loro consanguinei, stabilendo negozi in città per mesi, persino anni a volte. Fintanto che le loro attività criminali proseguono anonime, gli *Sczarni* continuano a dissanguare le loro vittime fino a che non hanno le tasche piene o i loro vicini si insospettiscono. 

I *Varisiani* chiamano casa il mondo, anche quando si stabiliscono in un posto. Apprezzano foulard di tutte le grandezze e i colori, ma che abbiano qualche significato speciale. La più rilevante è il foulard di famiglia, o "kapenia". I bambini ricevono le loro kapenie quando raggiungono la maturità: per possederne una bisogna essere adulti. 

Queste lunghe e pesanti sciarpe mostrano eleganti e complessi ricami, incomprensibili alla maggior parte dei forestieri. Per i *Varisiani*, tuttavia, queste sciarpe mostrano la genealogia della famiglia. Seguendo le onde e le spirali di una kapenia, un *Varisiano* è in grado di risalire alla storia di una persona, passando da madre, padre, fratelli, nonni, bis-nonni, fin dove è noto alla famiglia.

I *Varisiani* credono che certi colori abbiano specifici poteri e scelgono il loro abbigliamento per attrarre il giusto tipo di energia. 
Il rosa è il colore dell'amore, gentilezza e coraggio. Il rosso rappresenta il desiderio, la lunga vita e la forza interiore. L'arancione felicità e ingegnosità, tanto che gli avventurieri *Varisiani* sono soliti indossare qualcosa di arancione. Il verde migliora la saggezza e l'autocontrollo. Il turchese rappresenta la forza fisica e la comunicazione non verbale ed è il colore utilizzato per la maggior parte dei costumi di scena danzante. Blu è il colore della salute, giovinezza e bellezza. Il viola migliora l'intuizione e l'ispirazione divina ed è colore portato dalla maggior parte degli indovini e i veggenti portano sciarpe di questo colore.

I *Varisiani* amano i gioielli e preferiscono le gemme al denaro. La maggior parte ritiene accortamente che la ricchezza sia più difficile da rubare se viene indossata piuttosto che nascosta alla vista.

Al contrario degli *Shoanti* che usano i tatuaggi come segno di onore e traguardi raggiunti, per i *Varisiani* un tatuaggio è una forma d'arte, spesso usata per valorizzare una bellezza già presente. Questi tatuaggi tipicamente comprendono diversi colori significativi per la persona che li porta e un'intera metodologia magica si muove attorno ad alcuni di questi tatuaggi mistici. 

Questo gusto ed ossessione per i tatuaggi deriva della comparsa periodica di "tatuaggi nativi" nei neonati *Varisiani*: voglie che possono essere estremamente elaborate e colorate. Alcuni rappresentano oggetti fisici, come una spada o un unicorno, mentre altri sillabano frasi un'antica lingua morta o delineano mappe. Questi tatuaggi potrebbero apparire come piccoli, sfocati marchi durante l'infanzia, ma crescere con il bambino fino a diventare chiari e dettagliati nell'età adulta.
