.. _talenti:

=================================================
Talenti
=================================================

.. Warning::
	I talenti che derivano direttamente da un altro talento sono stati sistemati in una sottocategoria dello stesso per avere un colpo d'occhio migliore su quali talenti si possono acquisire. Se si preferisce l'ordinamento alfabetico si possono trovare i link :ref:`qui<tal_ordine_alfabetico>`.

.. figure:: Immagini/Talenti1.png
	:align: right
	:width: 400

.. contents:: Contenuti
	:local:
	:depth: 3

Acquisizione
=====================
Alcuni attributi dei personaggi non sono legati a razza, classe o abilità, come la rapidità dei riflessi, la capacità di realizzare oggetti magici, l’addestramento per vibrare colpi con estrema potenza con armi da mischia, o l’abilità di deviare le frecce. Queste capacità sono rappresentate dai talenti. 

Mentre alcuni talenti sono più utili ad alcuni tipi di personaggi piuttosto che ad altri, e molti hanno dei prerequisiti speciali, come regola generale, i talenti rappresentano tutte quelle capacità che esulano dagli scopi normali della razza e della classe del personaggio. 

Molti alterano, migliorano o attenuano le restrizioni dei privilegi di classe, mentre altri possono conferire bonus alle statistiche o la possibilità di effettuare azioni altrimenti proibite. Scegliendo i talenti, i giocatori possono adattare il proprio personaggio in modo da renderlo assolutamente unico.

-----------------
Prerequisiti
-----------------
Alcuni talenti hanno dei prerequisiti: il proprio personaggio deve avere la caratteristica, il privilegio di classe, il talento, l’abilità, il bonus di attacco o altre peculiarità per poter selezionare o utilizzare quel talento.

Un personaggio può ottenere un talento in cui soddisfa il prerequisito, ma non può più utilizzarlo se perde il prerequisito. Se, successivamente, lo riottiene riguadagna il pieno uso del talento.

-----------------
Tipi
-----------------
Alcuni talenti sono generali, nel senso che non ci sono regole speciali che li gestiscono come gruppo. 

Altri permettono, per esempio, di creare oggetti magici di ogni sorta o preparare e lanciare un incantesimo con maggiore efficacia trattandolo come se fosse di un livello più alto di quanto non sia realmente.

Colpo Critico
-----------------
I talenti di colpo critico ne modificano gli effetti infliggendo una condizione addizionale alla vittima. 

I personaggi privi del talento :ref:`Critico Prodigioso<tal_critico_prodigioso>` possono applicare solo gli effetti di un talento di colpo critico ad uno specifico colpo decidendo quale applicare dopo che il colpo critico è stato confermato.

.. _talenti_di_combattimento:

Combattimento
-----------------
Ogni talento indicato come talento di combattimento può essere selezionato come  bonus dei guerrieri. 

.. Note:: 

	I talenti così designati sono comunque accessibili a tutte le classi, a patto che vengano soddisfatti i prerequisiti.


Creazione Oggetto
---------------------
Un talento di creazione oggetto permette all’incantatore di creare un oggetto magico di un certo tipo. 

A prescindere dall'oggetto, tutti i talenti di creazione oggetto hanno alcune peculiarità in comune.

:Costo in Materie Prime: 
	Il costo è pari alla metà del costo dell’oggetto ed è richiesto l'accesso a un laboratorio o a un’officina magica, attrezzi speciali e così via. 

	Un personaggio in genere avrà a disposizione tutto l’occorrente, a meno che non sussistano circostanze particolari.
:Tempo: 
	Dipendente dal talento e dal costo dell'oggetto.
:Costo dell’Oggetto: 
	:ref:`Mescere Pozioni<tal_mescere_pozioni>`, :ref:`Creare Bacchette<tal_creare_bacchette>` e :ref:`Scrivere Pergamene<tal_scrivere_pergamene>` creano oggetti che riproducono direttamente un determinato effetto magico, e il potere di questi oggetti dipende dal LI, ossia un incantesimo da uno di questi oggetti ha il potere che avrebbe se lanciato da un incantatore di quel livello, che deve essere almeno tale da poter lanciare l'incantesimo. 

	Il prezzo di questi oggetti (e quindi il costo delle materie prime) dipende quindi dal LI. Per determinare il prezzo finale di ciascun oggetto, si moltiplichi il LI per il livello dell’incantesimo e si moltiplichi per una costante:

	*	Bacchette = Liv Incantesimo × LI × 750 mo
	*	Bastoni = Il costo dei bastoni si calcola usando una :ref:`formula più complessa<costo_bastoni>`
	*	Pergamene = Liv Incantesimo × LI × 25 mo
	*	Pozioni = Liv Incantesimo × LI × 50 mo

.. Note::
	Gli incantesimi di livello zero contano come se avessero un Liv Incantesimo pari a 1/2.

:Costi Aggiuntivi: 
	Qualsiasi pozione, pergamena o bacchetta che racchiude un incantesimo con componente materiale costosa comporta anche un costo aggiuntivo in proporzione. 

	Per pozioni e pergamene il creatore deve consumare la componente materiale quando crea l’oggetto. Per una bacchetta il creatore deve consumare 50 unità della componente materiale. Alcuni oggetti magici hanno dei costi aggiuntivi in modo simile, come indicato nelle loro descrizioni.

:Prova d’Abilità: 
	Per creare con successo un oggetto magico occorre una prova di :ref:`Sapienza Magica<sapienza_magica>` con CD = 10 + LI.
	Alternativamente, è possibile usare un’abilità associata di :ref:`Artigianato<artigianato>` o :ref:`Professione<professione>` per tentare questa prova, in base all’oggetto che deve essere creato. 

	Una prova fallita rovina i materiali usati, mentre un fallimento di 5 o più potrebbe comportare la creazione di un oggetto maledetto.
	Vedi la :ref:`Sezione Dedicata<creazione_oggetti_magici>` per approfondire quali prove di abilità sono necessarie e che maledizioni possono capitare.

Metamagia
--------------
Un incantatore che approfondisce lo studio della magia, impara via via a lanciare incantesimi in modi nuovi e diversi da come erano stati ideati o da come li aveva appresi in origine.
Preparare e lanciare un incantesimo in questo modo è più difficile del normale, ma grazie ai talenti di metamagia ci si può comunque riuscire. 

Gli incantesimi modificati da un talento di metamagia usano uno slot incantesimo più alto del normale, senza cambiarne il livello né la CD per i TS.

:Maghi e Incantatori Divini: 
	:ref:`Maghi<mago>` e :ref:`Incantatori Divini<incantesimi_divini>` scelgono gli incantesimi su cui applicare i propri talenti di metamagia durante la preparazione.
:Stregoni e Bardi: 
	:ref:`Stregoni<stregone>` e :ref:`Bardi<bardo>` scelgono gli incantesimi quando li lanciano, e in quell’istante decidono se utilizzare i talenti di metamagia per potenziarli, tuttavia impiegano un tempo maggiore per il lancio. Se il tempo di lancio normale di un incantesimo è un’:ref:`Azione Standard<azione_standard>`, essi impiegano un':ref:`Azione di Round Completo<azione_di_round_completo>`. Se il tempo necessario fosse un round, ce ne vorrebbe uno aggiuntivo.

.. Note::
	Il tempo di lancio degli incantesimi modificati dal talento metamagico :ref:`Incantesimi Rapidi<tal_incantesimi_rapidi>` resta invariato.

:Lancio spontaneo: 
	Un :ref:`Chierico<chierico>` che lancia spontaneamente incantesimi di curare o infliggere, o un :ref:`Druido<druido>` che lancia spontaneamente :ref:`Evoca Alleato Naturale<inc_evoca_alleato_naturale>`, può lanciarne la versione metamagica con un tempo di lancio maggiore. 

	Lanciare spontaneamente un incantesimo metamagico con tempo di lancio pari a un’:ref:`Azione Standard<azione_standard>` richiede un’:ref:`Azione di Round Completo<azione_di_round_completo>`, mentre per lanciare incantesimi con tempo di lancio più lungo occorre un’:ref:`Azione di Round Completo<azione_di_round_completo>` aggiuntiva. 

.. Note::
	Gli incantesimi spontanei modificati da :ref:`Incantesimi Rapidi<tal_incantesimi_rapidi>` possono essere lanciati come :ref:`Azione Veloce<azione_veloce>`.

:Effetti:
	Un incantesimo metamagico continua a funzionare al suo livello originale, anche se preparato e lanciato come incantesimo di livello superiore. Non vi è alcuna modifica ai TS, a meno che indicato diversamente. Le modifiche di questi talenti si applicano soltanto agli incantesimi lanciati direttamente da chi utilizza il talento, non da bacchette, pergamene, etc.

	I talenti che permettono di eliminare le componenti di un incantesimo, non eliminano l’attacco di opportunità derivante dal lanciare quell'incantesimo sotto minaccia, tranne nel caso di :ref:`Incantesimi Rapidi<tal_incantesimi_rapidi>`. 

	Non tutti gli incantesimi sono candidati ad essere modificati, a seconda della descrizione dei talenti.

:Talenti di Metamagia Multipli:
	Si possono applicare più talenti di metamagia sullo stesso incantesimo, sommando le eventuali variazioni di livello e senza applicare più volte lo stesso talento su una singola magia.
:Oggetti Magici: 
	Il personaggio può immagazzinare incantesimi metamagici in pergamene, pozioni o bacchette e non è necessario possedere talenti di metamagia per attivare oggetti contenenti incantesimi metamagici.
:Controincantesimi: 
	Non ci sono variazioni agli effetti di un :ref:`Controincantesimo<controincantesimo>` o nella sua capacità di controincantare altri incantesimi.

-----------------
Descrizione
-----------------
Ciascun talento contiene i dettagli che lo riguardano nel seguente formato.

:Nome: 
	Il nome del talento indica anche a quale sottocategoria il talento appartiene (fanno eccezione i metamagici e quelli di creazione oggetto, che sono separati in sezioni dedicate) ed è seguito da una breve descrizione dei suoi effetti. 
:Prerequisito: 
	Un punteggio di caratteristica, un altro talento (o talenti), un bonus di attacco base , un numero di gradi in una o più abilità o qualsiasi altra cosa necessaria per poter acquisire questo talento. Assente se il talento non ha prerequisiti.
:Beneficio: 
	Cosa permette di fare il talento. Possedere più volte il talento non ne cumula i benefici a meno che sia indicato diversamente nella descrizione.
:Normale: 
	In cosa è limitato o impossibilitato un personaggio che non possiede questo talento. Assente se il mancato possesso di questo talento non implica particolari svantaggi.
:Speciale: Elementi aggiuntivi riguardanti il talento.

Elenco
========================

.. _tal_abilità_focalizzata:

--------------------
Abilità Focalizzata
--------------------
Si diventa particolarmente esperti in un'abilità a scelta.

:Beneficio: 
	+3 a tutte le prove che riguardano quella abilità che aumenta a +6 se si hanno 10 o più gradi.
:Speciale: 
	Si può acquisire più volte, senza cumularlo: bisogna scegliere ogni volta un nuovo tipo di abilità.

.. _tal_acrobatico:

-------------
Acrobatico
-------------

Gran capacità nel balzare, saltare e volare.

:Beneficio: 
	+2 alle prove di :ref:`Acrobazia<acrobazia>` e :ref:`Volare<volare>` che aumenta a +4 se nell'abilità utilizzata per la prova si hanno 10 gradi o più. 

.. figure:: Immagini/Talenti2.png

.. _tal_addestramento_arcano_nelle_armature:

----------------------------------------------------
Addestramento Arcano nelle Armature (Combattimento)
----------------------------------------------------
Capacità nel lanciare incantesimi indossando l’armatura.

:Tipo: Azione Veloce
:Prerequisiti: :ref:`Competenza nelle Armature Leggere<tal_competenza_nelle_armature_leggere>`, LI 3.
:Beneficio: Riduce la probabilità di fallimento arcano degli incantesimi in base all’armatura indossata del 10% per qualsiasi incantesimo lanciato nel round.

.. _tal_maestria_arcana_nelle_armature:

Maestria Arcana nelle Armature (Combattimento)
----------------------------------------------
Competenza nel lanciare incantesimi indossando l’armatura.

:Tipo: Azione Veloce
:Prerequisiti: 
	:ref:`Addestramento Arcano nelle Armature<tal_addestramento_arcano_nelle_armature>`, :ref:`Competenza nelle Armature Medie<tal_competenza_nelle_armature_leggere>`, LI 7.
:Beneficio: 
	Riduce la probabilità di fallimento arcano degli incantesimi in base all’armatura indossata del 20% per qualsiasi incantesimo lanciato nel round. 
	Non si cumula con quello di :ref:`Addestramento Arcano nelle Armature<tal_addestramento_arcano_nelle_armature>`.

.. _tal_addestramento_nel_combattimento_difensivo:

-------------------------------------------------------------
Addestramento nel Combattimento Difensivo (Combattimento)
-------------------------------------------------------------
Eccelse capacità difensive per contrastare tutti i tipi di manovre in combattimento.

:Beneficio: 
	Quando si calcola la propria DMC si considerano i propri Dadi Vita totali come Bonus di Attacco Base.

.. _tal_affinità_animale:

-----------------------
Affinità Animale
-----------------------
Capacità nel trattare con animali e cavalcature.

:Beneficio: 
	+2 alle prove di :ref:`Addestrare Animali<addestrare_animali>` e :ref:`Cavalcare<cavalcare>` che aumenta a +4 se si hanno 10 gradi o più.

.. _tal_agile:

-------------
Agile
-------------
Si è più veloci del normale.

:Beneficio: 
	Quando non si indossano armature, o se ne indossa una leggera, la velocità base aumenta di 1,5 metri, a meno che si stia trasportando un carico medio o pesante.
:Speciale: Si può prendere questo talento più volte cumulandone gli effetti.

.. _tal_allerta:

---------
Allerta
---------
Attenta capacità di notare cose che agli altri sfuggono.

:Beneficio:
	+2 alle prove di :ref:`Intuizione<intuizione>` e :ref:`Percezione<percezione>` che aumenta a +4 se nell'abilità utilizzata per la prova si hanno 10 gradi o più. 


.. _tal_arma_accurata:

----------------------------------------
Arma Accurata (Combattimento)
----------------------------------------
Si usa la propria agilità invece che la Forza in mischia.

:Beneficio: 
	Con armi leggere, catene chiodate, fruste o stocci adatti alla propria taglia, si può utilizzare il Mod Des anziché quello di Forza al proprio tiro per colpire. 

	Se si imbraccia uno scudo, si subisce la penalità di armatura alla prova di quest’ultimo al proprio tiro per colpire.
:Speciale:
	Le armi naturali vanno sempre considerate alla stregua di armi leggere.

.. _tal_arma_focalizzata:

----------------------------------------
Arma Focalizzata (Combattimento)
----------------------------------------
Si sceglie un tipo di arma, compreso colpo senz'arma e lotta (o raggio se si è incantatori) per padroneggiarne l'uso.

:Prerequisiti:
	:ref:`Competenza nelle armi<tal_competenza_nelle_armi>` (arma scelta), BAB +1
:Beneficio: 
	+1 a tutti i tiri per colpire con l'arma scelta
:Speciale:
	Si può acquisire più volte, senza cumularlo: bisogna scegliere ogni volta un nuovo tipo di arma.

.. _tal_arma_focalizzata_superiore:

Arma Focalizzata Superiore (Combattimento)
------------------------------------------
Si sceglie un tipo di arma già scelta con :ref:`Arma Focalizzata<tal_arma_focalizzata>`, per padroneggiarne l’uso.
 
:Prerequisiti: 
	:ref:`Arma Focalizzata<tal_arma_focalizzata>` (arma scelta), :ref:`Competenza nelle armi<tal_competenza_nelle_armi>` (arma scelta), BAB +1, :ref:`Guerriero<guerriero>` liv 8.
:Beneficio:
	+1 ai tiri per colpire effettuati con l’arma, che si cumula con gli altri bonus ai tiri per colpire, compreso quello di :ref:`Arma Focalizzata<tal_arma_focalizzata>`.
:Speciale: 
	Si può acquisire :ref:`Arma Focalizzata Superiore<tal_arma_focalizzata_superiore>` più volte, senza cumularlo: bisogna scegliere ogni volta un nuovo tipo di arma.

.. _tal_arma_specializzata:

Arma Specializzata (Combattimento)
------------------------------------------
Capacità di infliggere danni con un tipo di arma già scelta in :ref:`Arma Focalizzata<tal_arma_focalizzata>`

:Prerequisiti: 
	:ref:`Arma Focalizzata<tal_arma_focalizzata>` (arma scelta), :ref:`Competenza nelle armi<tal_competenza_nelle_armi>` (arma scelta), :ref:`Guerriero<guerriero>` liv 4.
:Beneficio: 
	+2 a tutti i danni inflitti con l’arma.
:Speciale: 
	Si può acquisire :ref:`Arma Focalizzata Superiore<tal_arma_focalizzata_superiore>` più volte, senza cumularlo: bisogna scegliere ogni volta un nuovo tipo di arma.

.. _tal_arma_specializzata_superiore:

*********************************************
Arma Specializzata Superiore (Combattimento)
*********************************************
Capacità di infliggere più danni con un tipo di arma già scelta in :ref:`Arma Specializzata<tal_arma_specializzata>`

:Prerequisiti: 
	:ref:`Arma Focalizzata<tal_arma_focalizzata>` (arma scelta), :ref:`Arma Focalizzata Superiore<tal_arma_focalizzata_superiore>` (arma scelta), :ref:`Arma Specializzata<tal_arma_specializzata>` (arma scelta), :ref:`Competenza nelle armi<tal_competenza_nelle_armi>` (arma scelta), :ref:`Guerriero<guerriero>` liv 12.
:Beneficio: 
	+2 a tutti i danni inflitti con l’arma, che si cumula con gli altri bonus ai tiri per colpire, compreso quello di :ref:`Arma Specializzata<tal_arma_specializzata>`.
:Speciale: 
	Si può acquisire :ref:`Arma Specializzata Superiore<tal_arma_specializzata_superiore>` più volte, senza cumularlo: bisogna scegliere ogni volta un nuovo tipo di arma.

.. _tal_colpo_penetrante:

Colpo Penetrante (Combattimento)
----------------------------------
I propri attacchi con un':ref:`Arma Focalizzata<tal_arma_focalizzata>` sono in grado di penetrare le difese di alcune creature.

:Prerequisiti: 
	:ref:`Arma Focalizzata<tal_arma_focalizzata>` (arma scelta), :ref:`Competenza nelle armi<tal_competenza_nelle_armi>` (arma scelta), BAB +1, :ref:`Guerriero<guerriero>` liv 12.
:Beneficio: 
	Gli attacchi con l’arma ignorano fino a 5 punti di riduzione del danno, ma non si applica a riduzione del danno senza un tipo (come RD 10/—).

.. _tal_colpo_penetrante_superiore:

*******************************************
Colpo Penetrante Superiore (Combattimento)
*******************************************
Gli attacchi con l’arma scelta penetrano le difese dei nemici.

:Prerequisiti: 
	:ref:`Arma Focalizzata<tal_arma_focalizzata>` (arma scelta), :ref:`Colpo Penetrante<tal_colpo_penetrante>` (arma scelta), :ref:`Guerriero<guerriero>` liv 16.
:Beneficio: 
	Gli attacchi con l’arma scelta ignorano fino 10 punti di riduzione del danno che si riducono a 5 per la riduzione del danno senza un tipo (come RD 10/—).

.. _tal_maestria_intimorente:

Maestria Intimorente (Combattimento)
-------------------------------------
L’abilità con la propria arma intimorisce i nemici.

:Tipo: :ref:`Azione di Round Completo<azione_di_round_completo>`
:Prerequisiti: 
	:ref:`Arma Focalizzata<tal_arma_focalizzata>` (arma scelta), :ref:`Competenza nelle armi<tal_competenza_nelle_armi>` (arma scelta).
:Beneficio: 
	Mentre si impugna l’arma è possibile esibirsi in una serie di mosse spettacolari, effettuando una prova di :ref:`Intimidire<intimidire>` per demoralizzare tutti i nemici entro 9 metri che assistono a tale esibizione.

.. _tal_spezzare_le_difese:

*******************************************
Spezzare le Difese (Combattimento)
*******************************************
Capacità nell’arma scelta che lascia gli avversari indifesi colpendoli quando le loro difese sono già compromesse.

:Prerequisiti:
	:ref:`Arma Focalizzata<tal_arma_focalizzata>` (arma scelta), :ref:`Competenza nelle armi<tal_competenza_nelle_armi>` (arma scelta), :ref:`Maestria Intimorente<tal_maestria_intimorente>`, BAB +6.
:Beneficio: 
	Qualsiasi avversario scosso, spaventato o in preda la panico che si colpisce durante il round è considerato impreparato fino alla fine del turno successivo compreso qualsiasi attacco addizionale.

.. _tal_colpo_mortale:

Colpo Mortale (Combattimento)
******************************
Con un colpo ben piazzato, si uccide l’avversario con una morte rapida ma dolorosa.

:Tipo: :ref:`Azione Standard<azione_standard>`
:Prerequisiti: 
	:ref:`Arma Focalizzata<tal_arma_focalizzata>` (arma scelta), :ref:`Arma Focalizzata Superiore<tal_arma_focalizzata_superiore>` (arma scelta), :ref:`Competenza nelle armi<tal_competenza_nelle_armi>` (arma scelta), :ref:`Maestria Intimorente<tal_maestria_intimorente>`, :ref:`Spezzare le Difese<tal_spezzare_le_difese>`, BAB +11.
:Beneficio: 
	Si effettua un attacco singolo contro un avversario stordito o impreparato e se si colpisce, si infliggono danni raddoppiati ed il bersaglio subisce 1 danno da sanguinamento alla Costituzione (in :ref:`Appendice<danno_caratteristica>` gli effetti). 

	Il danno addizionale e il sanguinamento non sono moltiplicati in caso di colpo critico.

.. _tal_atletico:

--------------------
Atletico
--------------------
Si possiedono innate doti fisiche.

:Beneficio: 
	+2 alle prove di :ref:`Nuotare<nuotare>` e :ref:`Scalare<scalare>` che aumenta a +4 se nell'abilità utilizzata per la prova si hanno 10 gradi o più. 

.. _tal_attacco_poderoso:

--------------------------------
Attacco Poderoso (Combattimento)
--------------------------------
Capacità di compiere attacchi in mischia straordinariamente potenti, a scapito della precisione.

:Prerequisiti:
	For 13, BAB +1.
:Beneficio: 
	Si può decidere, **prima di effettuare il tiro per colpire**, di subire penalità –1 ai tiri per colpire in mischia per ottenere bonus +2 al danno in mischia. 

	Se si impugna un’arma a due mani, o un’arma a una mano ma impugnata a due mani, o un’arma naturale primaria che aggiunge una volta e mezza il Mod For al danno, il bonus al danno viene aumentato della metà. 

	Il bonus viene invece dimezzato se si attacca con un’arma nella mano secondaria o con un’arma naturale secondaria.

	Quando il proprio bonus di attacco base raggiunge +4 e ogni 4 punti guadagnati, la penalità aumenta di –1 ed il bonus al danno aumenta di +2.
	
	Il bonus al danno non si applica agli attacchi di contatto o effetti che non infliggono danni.

.. _tal_incalzare:

Incalzare (Combattimento)
--------------------------
Capacità di colpire due nemici con un solo colpo.

:Tipo: :ref:`Azione Standard<azione_standard>`
:Prerequisiti:
	For 13, :ref:`Attacco Poderoso<tal_attacco_poderoso>`, BAB +1.
:Beneficio:
	Si può effettuare un attacco singolo al proprio bonus di attacco base pieno contro un avversario che sia a portata. 

	Se il colpo va a segno, infligge i danni normali ed è possibile compiere un attacco addizionale (usando il proprio bonus di attacco base pieno) contro un avversario che sia adiacente al primo e sia a portata del colpo. Si può effettuare un solo attacco addizionale per round con questo talento. 

	Si subisce penalità –2 alla CA fino al proprio turno successivo.

.. _tal_incalzare_potenziato:

****************************************
Incalzare Potenziato (Combattimento)
****************************************
Capacità di colpire più nemici adiacenti con un solo colpo.

:Tipo: :ref:`Azione Standard<azione_standard>`
:Prerequisiti: 
	For 13, :ref:`Attacco Poderoso<tal_attacco_poderoso>`, :ref:`Incalzare<tal_incalzare>`, BAB +4.
:Beneficio: 
	Si può fare un singolo attacco al proprio bonus di attacco completo contro un nemico che sia a portata. 

	Se si colpisce, si infligge normalmente il danno e si può compiere un attacco addizionale (usando il proprio bonus di attacco completo) contro un avversario che sia adiacente al nemico precedente ed a portata. 

	Se lo si colpisce, è possibile continuare ad effettuare attacchi contro gli avversari adiacenti all’avversario precedente, a condizione che siano sempre a portata. 

	Non si può attaccare più di una volta un determinato avversario durante questa azione di attacco e si subisce penalità –2 alla CA fino al proprio turno successivo.

.. _tal_oltrepassare_migliorato:

Oltrepassare Migliorato (Combattimento)
----------------------------------------
Grande esperienza nel buttare a terra gli avversari.

:Prerequisiti: 
	For 13, :ref:`Attacco Poderoso<tal_attacco_poderoso>`, BAB +1.
:Beneficio: 
	Non si provoca :ref:`Attacco di Opportunità<azione_attacco_di_opportunità>` quando si tenta una manovra di :ref:`Oltrepassare<azione_oltrepassare>` in combattimento e si ottiene +2 alle prove di :ref:`Oltrepassare<azione_oltrepassare>` un avversario e +2 alla DMC ogni volta che un avversario cerca di oltrepassarvi a propria volta. 

	I bersagli del tentativo di :ref:`oltrepassare<azione_oltrepassare>` non possono scegliere di evitare di subire la manovra in alcun modo.
:Normale: 
	Si provoca un :ref:`Attacco di Opportunità<azione_attacco_di_opportunità>` quando si compie una manovra di :ref:`Oltrepassare<azione_oltrepassare>` in combattimento.

.. _tal_oltrepassare_superiore:

***************************************
Oltrepassare Superiore (Combattimento)
***************************************
Gli avversari devono tuffarsi per evitarvi.

:Prerequisiti: 
	For 13, :ref:`Attacco Poderoso<tal_attacco_poderoso>`, :ref:`Oltrepassare Migliorato<tal_oltrepassare_migliorato>`, BAB +6.
:Beneficio: 
	+2 alle prove per :ref:`Oltrepassare<azione_oltrepassare>` un avversario che si cumula con quello di :ref:`Oltrepassare Migliorato<tal_oltrepassare_migliorato>`. 

	Ogni volta che si oltrepassano gli avversari, essi provocano :ref:`Attacchi di Opportunità<azione_attacco_di_opportunità>` se cadono a terra proni a causa dell’azione di :ref:`Oltrepassare<azione_oltrepassare>`.
:Normale: 
	Le creature che cadono a terra prone a causa di un’azione di :ref:`Oltrepassare<azione_oltrepassare>` non provocano un :ref:`Attacco di Opportunità<azione_attacco_di_opportunità>`.

.. _tal_spezzare_migliorato:

Spezzare Migliorato (Combattimento)
------------------------------------
Capacità di danneggiare le armi e l’armatura degli avversari.

:Prerequisiti: 
	For 13, :ref:`Attacco Poderoso<tal_attacco_poderoso>`, BAB +1.
:Beneficio: 
	Non si provoca alcun :ref:`Attacco di Opportunità<azione_attacco_di_opportunità>` quando si tenta una manovra di :ref:`Spezzare<azione_spezzare>` in combattimento e si ottiene +2 alle prove di :ref:`Spezzare<azione_spezzare>` un oggetto +2 alla DMC ogni volta che un avversario cerca di spezzare un proprio oggetto d’equipaggiamento.
:Normale: 
	Si provoca un :ref:`Attacco di Opportunità<azione_attacco_di_opportunità>` quando si compie una manovra di :ref:`Spezzare<azione_spezzare>` in combattimento.

.. _tal_spezzare_superiore:

**********************************
Spezzare Superiore (Combattimento)
**********************************
Si infliggono colpi devastanti alle armi e alle armature, danneggiando sia gli oggetti che colui che li porta con un solo terrificante attacco.

:Prerequisiti: 
	For 13, :ref:`Attacco Poderoso<tal_attacco_poderoso>`, :ref:`Spezzare Migliorato<tal_spezzare_migliorato>`, BAB +6.
:Beneficio:
	+2 alle prove per :ref:`Spezzare<azione_spezzare>` un oggetto, che si cumula con quello di :ref:`Spezzare Migliorato<tal_spezzare_migliorato>`. 

	Ogni volta che si usa per distruggere un’arma, uno scudo o un’armatura, si applica il danno in eccesso al portatore dell’oggetto. Nessun danno viene trasferito se si decide di lasciare l’oggetto a 1 punto ferita.

.. _tal_spingere_migliorato:

Spingere Migliorato (Combattimento)
------------------------------------
Capacità di respingere gli avversari.

:Prerequisiti: 
	For 13, :ref:`Attacco Poderoso<tal_attacco_poderoso>`, BAB +1.
:Beneficio: 
	Quando si dà una :ref:`Spingere<azione_spingere>` non si subisce alcun :ref:`Attacco di Opportunità<azione_attacco_di_opportunità>` da parte del difensore e si ottiene +2 alle prove per :ref:`Spingere<azione_spingere>` indietro il difensore e +2 alla DMC ogni volta che un avversario cerca di :ref:`Spingere<azione_spingere>` voi.
:Normale: 
	Si provoca un :ref:`Attacco di Opportunità<azione_attacco_di_opportunità>` quando si tenta di effettuare in combattimento una manovra di :ref:`Spingere<azione_spingere>`.

.. _tal_spingere_superiore:

**********************************
Spingere Superiore (Combattimento)
**********************************
Gli attacchi di :ref:`Spingere<azione_spingere>` fanno perdere l’equilibrio agli avversari.

:Prerequisiti: 
	For 13, :ref:`Attacco Poderoso<tal_attacco_poderoso>`, :ref:`Spingere Migliorato<tal_spingere_migliorato>`, BAB +6.
:Beneficio: 
	+2 alle prove di :ref:`Spingere<azione_spingere>` che si cumula con quello di :ref:`Spingere Migliorato<tal_spingere_migliorato>`. 

	Ogni volta che si spinge un avversario, il suo :ref:`Movimento<azione_movimento>` provoca :ref:`Attacci di Opportunità<azione_attacco_di_opportunità>` da tutti i propri alleati (ma non da chi spinge).
:Normale: 
	Le creature spostate da una spinta non provocano :ref:`Attacci di Opportunità<azione_attacco_di_opportunità>`.

.. _tal_autorità:

------------------
Autorità
------------------
Capacità di radunare gregari e seguaci per la propria causa e un compagno per le proprie avventure.

:Prerequisiti: 
	Liv Personaggio 7.

.. _tabella_autorità:

.. figure:: Immagini/TalentiAutorita3.png
	:align: right
	:width: 250

:Beneficio: 
	Questo talento permette di convocare in proprio aiuto un gregario leale e seguaci devoti e subordinati.

	Un gregario è in genere un PNG con livelli di classe, mentre i seguaci sono solitamente PNG di basso livello. Vedi la :ref:`Tabella<tabella_autorità>` per sapere il livello del gregario e quanti seguaci si possono convocare.

Valore Autorità 
	Il valore base di :ref:`Autorità<tal_autorità>` è uguale al proprio livello + Mod Car, ma può essere influenzato da fattori esterni. In caso di un modificatore negativo, la :ref:`Tabella<tabella_autorità>` prevede anche dei valori molto bassi di :ref:`Autorità<tal_autorità>`.
Livello Gregario
	Si può attrarre un gregario di un livello non superiore a questo e a prescindere dal proprio valore di :ref:`Autorità<tal_autorità>`, è possibile reclutare solo gregari di due o più livelli inferiori al proprio. 

	Il gregario dovrebbe essere **equipaggiato** con oggetti :ref:`appropriati al suo livello<equipaggiamento_png>` e può essere di qualsiasi razza o classe. 

	L’**allineamento** del gregario non può essere opposto al proprio né per il binomio legge/caos né per quello bene/male, e si subisce penalità –1 al proprio valore di :ref:`Autorità<tal_autorità>` se si recluta un gregario con un allineamento diverso dal proprio.

	Il gregario non conta come membro del gruppo quando si determinano i PE, ma per assegnargli PE bisogna divider il livello del gregario per il livello del capo e si moltiplicarlo per il totale dei PE guadagnati dal capo. Tuttavia se il gregario guadagna PE sufficienti a portarlo ad un livello meno del capo, il gregario si ferma a 1 PE in meno rispetto a quello necessario per avanzare di livello.
Numero di Seguaci
	Si può radunare un gruppo di personaggi fino al numero indicato per ciascun livello.

	I seguaci sono simili ai gregari, tranne che in genere sono PNG di basso livello, anche di 5 o più livelli rispetto al capo, che raramente sono efficaci in combattimento.
	
	Non guadagnano esperienza e non avanzano di livello e quando si ottiene un nuovo livello, si può consultare la :ref:`Tabella<tabella_autorità>` per determinare se se ne acquisiscono altri.
Modificatori di Autorità
	Diversi fattori possono influire sul punteggio di :ref:`Autorità<tal_autorità>`, modificandolo rispetto al valore base di Liv + Mod Car. 

	La reputazione (dal punto di vista del gregario o seguace che si vuole convocare) aumenta o diminuisce il proprio punteggio di Autorità:

	.. image:: Immagini/TalentiAutorita1.png

	Quando si prova ad attirare un gregario si possono applicare altri modificatori, come indicato sotto:

	.. image:: Immagini/TalentiAutorita2.png

	Mentre i seguaci hanno priorità diverse dai gregari e quando si cerca di attrarre un seguace, bisogna utilizzare i modificatori seguenti:

	.. image:: Immagini/TalentiAutorita4.png

.. _tal_autosufficiente:

------------------
Autosufficiente
------------------
Capacità di badare a se stessi in situazioni estreme e curarsi le ferite.

:Beneficio: 
	+2 alle prove di :ref:`Guarire<guarire>` e :ref:`Sopravvivenza<sopravvivenza>` che aumenta a +4 se nell'abilità si hanno 10 gradi o più.

.. _tal_cogliere_di_sorpresa:

------------------------------------
Cogliere di Sorpresa (Combattimento)
------------------------------------
I nemici sono colti di sorpresa dal tuo abile uso di armi improvvisate e fuori dalla norma.

:Beneficio: 
	Non si subiscono penalità nell’usare un’arma da mischia improvvisata e gli avversari disarmati sono colti impreparati contro qualsiasi attacco fatto con un’arma improvvisata.
:Normale: 
	Attaccare con un’arma improvvisata comporta penalità –4 al tiro per colpire.

.. _tal_colpo_arcano:

----------------------------------
Colpo Arcano (Combattimento)
----------------------------------
Capacità di attingere al proprio potere arcano per infondere le armi di energia magica.

:Tipo: :ref:`azione_veloce`
:Prerequisito: 
	Poter lanciare incantesimi arcani.
:Beneficio: 
	Si può infondere nella propria arma una parte del proprio potere arcano: per 1 round, le proprie armi hanno +1 al danno e sono considerate come magiche al fine di superare la riduzione del danno. 

	Ogni cinque livelli da incantatore posseduti, il bonus aumenta di +1, fino ad un massimo di +5 al 20° livello.

.. _tal_colpo_di_ritorno:

--------------------------------------
Colpo di Ritorno (Combattimento)
--------------------------------------
Capacità di colpire i nemici che vi attaccano usando la loro portata, mirando alle armi o agli arti quando si avvicinano.

:Tipo: :ref:`Preparare<azione_preparare>`
:Prerequisiti: 
	BAB +11.
:Beneficio: 
	Si può preparare un’azione per effettuare un attacco in mischia contro un avversario che vi attacca in mischia, anche se quest’ultimo non è a portata.

.. _tal_colpo_senzarmi_migliorato:

------------------------------------------
Colpo Senz'Armi Migliorato (Combattimento)
------------------------------------------
Capacità di combattere senz’armi.

:Beneficio: 
	Si è considerati armati anche quando si è senz’armi: se si attacca senz’armi un avversario armato, non si subisce alcun :ref:`Attacco di Opportunità<azione_attacco_di_opportunità>` da parte di quest’ultimo. 

	Si può scegliere di infliggere danni letali o non.
:Normale: 
	Si è considerati senz’armi e si possono infliggere solo danni non letali con questo tipo di attacco.

.. _tal_deviare_frecce:

Deviare Frecce (Combattimento)
-------------------------------
Capacità di deviare le frecce in arrivo, così come gli altri proiettili, in modo che siano inoffensive.

:Prerequisiti: 
	Des 13, :ref:`Colpo Senz’Armi Migliorato<tal_colpo_senzarmi_migliorato>`.
:Beneficio: 
	Quando si ha almeno una mano libera e si verrebbe colpiti da un'arma a distanza, una volta per round, è possibile deviarla in modo da non subire danni. 

	Bisogna essere consapevoli dell’attacco e non essere colti impreparati, ma la deviazione non va conteggiata come azione. 

	Lanci eccezionali (come macigni o dardi di baliste) e gli attacchi a distanza generati da attacchi naturali o da effetti di incantesimi non possono essere deviati.

.. figure:: Immagini/TalentiAfferrareFrecce.png
	:align: left
	:width: 300


.. _tal_afferrare_frecce:

**********************************
Afferrare Frecce (Combattimento)
**********************************
Invece di evitare una freccia o un attacco a distanza, si può cercare di bloccarlo.

:Prerequisiti: 
	Des 15, :ref:`Colpo Senz’Armi Migliorato<tal_colpo_senzarmi_migliorato>`, :ref:`Deviare Frecce<tal_deviare_frecce>`.
:Beneficio: 
	Quando si usa :ref:`Deviare Frecce<tal_deviare_frecce>`,il personaggio può afferrare l’arma anziché limitarsi a deviarla. 

	Le armi da lancio si possono immediatamente scagliare indietro contro l’aggressore originale (persino se non è il proprio turno) o si possono raccogliere per utilizzarle in un secondo momento. 

.. _tal_lottare_migliorato:

Lottare Migliorato (Combattimento)
-----------------------------------
Esperienza nel lottare con gli avversari.

:Prerequisiti: 
	Des 13, :ref:`tal_colpo_senzarmi_migliorato`.
:Beneficio: 
	Non si provoca alcun :ref:`Attacco di Opportunità<azione_attacco_di_opportunità>` quando si tenta una manovra di :ref:`Lottare<azione_lottare>` in combattimento e si ottiene bonus +2 alle prove di :ref:`Lottare<azione_lottare>` con un avversario e +2 alla DMC ogni volta che un avversario cerca di lottare a propria volta.
:Normale: 
	Si provoca un :ref:`Attacco di Opportunità<azione_attacco_di_opportunità>` quando si compie una manovra di :ref:`Lottare<azione_lottare>` in combattimento.

.. _tal_lottare_superiore:

***********************************
Lottare Superiore (Combattimento)
***********************************
Mantenere una presa è qualcosa di naturale.

:Prerequisiti: 
	Des 13, :ref:`Colpo Senz’Armi Migliorato<tal_colpo_senzarmi_migliorato>`, :ref:`Lottare Migliorato<tal_lottare_migliorato>`, BAB +6.
:Beneficio: 
	+2 alle prove per afferrare un avversario che si cumula con quello di :ref:`Lottare Migliorato<tal_lottare_migliorato>`. 

	Una volta in lotta con una creatura, mantenere la presa è un’:ref:`Azione di Movimento<azione_movimento>` e si possono effettuare due prove di :ref:`Lottare<azione_lottare>` ad ogni round (per muoversi, ferire o immobilizzare l’avversario). Basta avere successo in una di queste per mantenere una presa in lotta.
:Normale: 
	Mantenere una presa in lotta è un’:ref:`Azione Standard<azione_standard>`.

.. _tal_pugno_stordente:

Pugno Stordente (Combattimento)
--------------------------------
Capacità di colpire gli avversari in zone vulnerabili e stordirli.

:Prerequisiti: 
	Des 13, Sag 13, :ref:`Colpo Senz’Armi Migliorato<tal_colpo_senzarmi_migliorato>`, BAB +8.
:Beneficio: 
	Prima di effettuare il tiro per colpire, bisogna dichiarare che si utilizzerà questo talento (quindi, un tiro per colpire fallito spreca il tentativo). 

	:ref:`Pugno Stordente<tal_pugno_stordente>`, in aggiunta ai danni inflitti normalmente, costringe l’avversario colpito a effettuare un TS su Tempra con **CD = 10 + 0.5 * Liv PG + Mod Sag**.

	Se fallisce il TS, rimane stordito per 1 round (fino all’inizio del turno successivo del personaggio) e non può agire, perde qualsiasi bonus di Destrezza alla CA e subisce –2 alla CA. 

	Si può tentare un attacco stordente soltanto una volta al giorno per ogni quattro livelli conseguiti, e non più di una volta per round. 

	Costrutti, melme, non morti, vegetali, creature incorporee e creature immuni ai colpi critici non possono rimanere storditi.
:Speciale: 
	Un monaco può selezionare :ref:`Pugno Stordente<tal_pugno_stordente>` come talento bonus al 1° livello anche se non soddisfa i prerequisiti e può usarlo tante volte al giorno quanti sono i suoi livello da monaco, più una volta ogni quattro livelli conseguiti in altre classi.

.. _tal_stile_dello_scorpione:

Stile dello Scorpione (Combattimento)
----------------------------------------
Capacità di compiere un colpo senz’armi in grado di ostacolare il movimento del bersaglio.

:Tipo: :ref:`Azione Standard<azione_standard>`
:Prerequisiti: 
	:ref:`Colpo Senz’Armi Migliorato<tal_colpo_senzarmi_migliorato>`.
:Beneficio: 
	Un singolo attacco senz’armi che se colpisce infligge danno normalmente e riduce  la velocità base del bersaglio 1,5 metri per un numero di round pari Mod Sag a meno che non superi un TS su Tempra con **CD = 10 + 0.5 * Liv PG + Mod Sag**.

.. _tal_pugno_della_gorgone:

*************************************
Pugno della Gorgone (Combattimento)
*************************************
Con un colpo ben piazzato, si lascia il bersaglio barcollante.

:Tipo: :ref:`Azione Standard<azione_standard>`
:Prerequisiti: 
	:ref:`Colpo Senz’Armi Migliorato<tal_colpo_senzarmi_migliorato>`, :ref:`Stile dello Scorpione<tal_stile_dello_scorpione>`, BAB +6.
:Beneficio: 
	Un singolo attacco senz’armi in mischia contro un avversario la cui velocità è ridotta (come da :ref:`Stile dello Scorpione<tal_stile_dello_scorpione>`); se va a segno, si infligge danno normalmente e il bersaglio è barcollante fino alla fine del proprio turno a meno che non superi un TS su Tempra con **CD = 10 + 0.5 * Liv PG + Mod Sag**. 

	Non ha effetto su bersagli già barcollanti.

.. _tal_collera_della_medusa:

Collera della Medusa (Combattimento)
*************************************
Ci si può avvantaggiare della confusione dell’avversario per compiere colpi multipli.

:Prerequisiti: 
	:ref:`Colpo Senz’Armi Migliorato<tal_colpo_senzarmi_migliorato>`, :ref:`Pugno della Gorgone<tal_pugno_della_gorgone>`, :ref:`Stile dello Scorpione<tal_stile_dello_scorpione>`, BAB +11.
:Beneficio: 
	Ogni volta che si usa un':ref:`Azione di Attacco Completo<azione_di_attacco_completo>` ed almeno un colpo senz’armi, si possono effettuare due colpi senz’armi addizionali al proprio bonus di attacco base più alto. 

	Questi attacchi bonus devono essere fatti contro un avversario frastornato, impreparato, paralizzato, barcollante, stordito o privo di sensi.

.. _tal_colpo_vitale:

----------------------------
Colpo Vitale (Combattimento)
----------------------------
Capacità di infliggere più danni con un attacco.

:Prerequisiti: 	
	BAB +6.
:Beneficio: 
	Quando si attacca, si può effettuare un attacco che infligge danno addizionale, al proprio bonus di attacco base più alto. 

	Si tira il dado per il danno dell’attacco due volte sommando i risultati, ma non si moltiplicano i bonus al danno per la Forza, la capacità dell’arma (come :ref:`Infuocata<armi_infuocata>`) o il danno da precisione (come :ref:`Attacco Furtivo<attacco_furtivo_ladro>`).

	Questo danno non viene moltiplicato in caso di colpo critico (sebbene altri bonus al danno si moltiplichino normalmente)

.. _tal_colpo_vitale_migliorato:

Colpo Vitale Migliorato (Combattimento)
---------------------------------------
Capacità di effettuare un attacco devastante.

:Prerequisiti: 
	:ref:`Colpo Vitale<tal_colpo_vitale>`, BAB +11.

:Beneficio: 
	Quando si attacca, si può effettuare un attacco che infligge danno addizionale, al proprio bonus di attacco base più alto.  

	Si tira il dado per il danno dell’attacco tre volte sommando i risultati, ma non si moltiplicano i bonus al danno per la Forza, la capacità dell’arma (come :ref:`Infuocata<armi_infuocata>`) o il danno da precisione (come :ref:`Attacco Furtivo<attacco_furtivo_ladro>`).

	Questo danno non viene moltiplicato in caso di colpo critico (sebbene altri bonus al danno si moltiplichino normalmente)

.. _tal_colpo_vitale_superiore:

***************************************
Colpo Vitale Superiore (Combattimento)
***************************************
Capacità di sferrare un colpo singolo dagli effetti devastanti.

:Prerequisiti: 
	:ref:`Colpo Vitale<tal_colpo_vitale>`, :ref:`Colpo Vitale Migliorato<tal_colpo_vitale_migliorato>`, BAB +16.

:Beneficio: 
	Quando si attacca, si può effettuare un attacco che infligge danno addizionale, al proprio bonus di attacco base più alto.  

	Si tira il dado per il danno dell’attacco quattro volte sommando i risultati, ma non si moltiplicano i bonus al danno per la Forza, la capacità dell’arma (come :ref:`Infuocata<armi_infuocata>`) o il danno da precisione (come :ref:`Attacco Furtivo<attacco_furtivo_ladro>`).

	Questo danno non viene moltiplicato in caso di colpo critico (sebbene altri bonus al danno si moltiplichino normalmente)

.. _tal_comandare_non_morti:

---------------------------
Comandare Non Morti
---------------------------
Usando gli empi poteri della necromanzia, si possono comandare le creature non morte, asservendole al proprio volere.

:Tipo: :ref:`Azione Standard<azione_standard>`
:Prerequisiti: 
	:ref:`Incanalare Energia<incanalare_energia>` negativa.
:Beneficio: 
	Si può utilizzare uno degli usi di :ref:`Incanalare Energia<incanalare_energia>` negativa per asservire al proprio volere i non morti entro 9 metri. 

	Le creature non morte ricevono un TS su Volontà per negare l’effetto con **CD = 10 + 0.5 * LI + Mod Car**. Le creature non morte che falliscono il tiro cadono sotto il controllo del personaggio e gli obbediranno al massimo delle proprie possibilità, come se fossero sotto gli effetti di :ref:`Controllare Non Morti<inc_controllare_non_morti>`. 

	I non morti intelligenti ricevono un nuovo TS ogni giorno per resistere al comando. Si può controllare qualsiasi numero di non morti, finché i loro Dadi Vita totali non superino il proprio livello da :ref:`Chierico<chierico>`. 

	Se si usa :ref:`Incanalare Energia<incanalare_energia>` in questo modo, questo non produce altri effetti (non cura o ferisce le creature vicine) e se una creatura non morta è sotto il controllo di un’altra creatura, bisogna effettuare una prova contrapposta di Carisma ogni volta che gli ordini sono in conflitto tra loro.

.. _tal_combattere_alla_cieca:

--------------------------------------
Combattere alla Cieca (Combattimento)
--------------------------------------
Capacità di attaccare gli avversari che non sono chiaramente percepibili.

:Beneficio: 
	In mischia, ogni volta che si manca l’avversario a causa dell’:ref:`Occultamento<occultamento>`, si può ripetere una volta il tiro percentuale fallito per vedere se si è riusciti a colpire comunque.

	Un attaccante invisibile non ottiene alcun vantaggio derivante dal colpire il personaggio in mischia, quindi non si perde il bonus positivo di Destrezza alla CA e l’avversario non guadagna +2 per essere invisibile. 

	I bonus dell’attaccante invisibile si applicano lo stesso agli attacchi a distanza.

	Non c’è bisogno di effettuare prove di :ref:`Acrobazia<acrobazia>` per muoversi a piena velocità mentre si è accecati.
:Normale:
	Si applicano i modificatori al tiro per colpire standard per gli attaccanti invisibili; il personaggio perde anche il bonus di Destrezza alla CA. Si applica inoltre una riduzione della velocità per l’oscurità e la scarsa visibilità.
:Speciale: 
	Questo talento non ha alcuna utilità nei confronti di un personaggio se questo è sotto l’effetto di un incantesimo :ref:`Intermittenza<inc_intermittenza>`

.. _tal_combattere_con_due_armi:

---------------------------------------
Combattere con Due Armi (Combattimento)
---------------------------------------
Capacità di combattere con due armi: Si può compiere un attacco addizionale ogni round con l’arma secondaria.

:Prerequisiti: 
	Des 15.
:Beneficio: 
	Le :ref:`Penalità all’Attacco<combattere_con_due_armi>` per combattere con due armi si riducono: per la mano primaria scende di 2 e per la secondaria di 6.
:Normale: 
	Se si impugna una seconda arma nella mano secondaria, si ottiene un attacco addizionale per round con quell’arma, ma si subisce penalità –6 all’attacco o agli attacchi regolari con la mano primaria e penalità –10 all’attacco con l’arma secondaria.

	Se l’arma secondaria è leggera, le penalità sono ridotte di 2 ciascuna. Un colpo senz’armi è sempre considerato leggero.

.. _tal_combattere_con_due_armi_migliorato:


Combattere con Due Armi Migliorato (Combattimento)
--------------------------------------------------
Capacità nel combattere con due armi.

:Prerequisiti: 
	Des 17, :ref:`Combattere con Due Armi<tal_combattere_con_due_armi>`, BAB +6.
:Beneficio: 
	In aggiunta al normale attacco addizionale che si possiede con la mano secondaria, si ottiene un secondo attacco con la mano secondaria, seppure con penalità –5.
:Normale: 
	Si ha solo un attacco addizionale con la mano secondaria.

.. _tal_combattere_con_due_armi_superiore:

***************************************************
Combattere con Due Armi Superiore (Combattimento)
***************************************************
Capacità incredibile di combattere con due armi.

:Prerequisiti: 
	Des 19, :ref:`Combattere con Due Armi<tal_combattere_con_due_armi>`, :ref:`Combattere con Due Armi Migliorato<tal_combattere_con_due_armi_migliorato>`, BAB +11.
:Beneficio: 
	Si ottiene un terzo attacco con l’arma secondaria, ma con penalità –10.

.. _tal_difendere_con_due_armi:


Difendere con Due Armi (Combattimento)
-------------------------------------------
Capacità di difendersi impugnando due armi.

:Prerequisiti: 
	Des 15, :ref:`Combattere con Due Armi<tal_combattere_con_due_armi>`.
:Beneficio: 
	Quando si impugna un’arma doppia o due armi (escluse le armi naturali o i colpi senz’armi), si ottiene bonus +1 alla propria CA, che sale a +2 se si combatte sulla difensiva o si compie un’:ref:`Azione di Difesa Totale<azione_difesa_totale>`.

.. _tal_doppio_taglio:


Doppio Taglio (Combattimento)
--------------------------------
L’arma nella mano secondaria colpisce con maggior forza mentre si combatte con due armi.

:Prerequisiti: 
	Des 15, :ref:`Combattere con Due Armi<tal_combattere_con_due_armi>`.
:Beneficio: 
	Si aggiunge il Mod For ai danni dell’arma impugnata nella mano secondaria.
:Normale: 
	Se ne aggiunge solo la metà.

.. _tal_attacco_lacerante_a_due_armi:

*********************************************************
Attacco Lacerante a Due Armi (Combattimento)
*********************************************************
Capacità di colpire contemporaneamente con entrambe le armi infliggendo ferite devastanti.

:Prerequisiti: 
	Des 17, :ref:`Combattere con Due Armi<tal_combattere_con_due_armi>`, :ref:`Combattere con Due Armi Migliorato<tal_combattere_con_due_armi_migliorato>`, :ref:`Doppio Taglio<tal_doppio_taglio>`, BAB +11.
:Beneficio: 
	Se si colpisce l’avversario con l’arma nella mano primaria e con quella nella mano secondaria, si infliggono 1d10 + 1.5 * Mod For danni addizionali. 

	Massimo una volta per round.

.. _tal_combattere_in_sella:

---------------------------------------
Combattere in Sella (Combattimento)
---------------------------------------
Capacità di guidare la cavalcatura in combattimento.

:Tipo: :ref:`Azione Immediata<azione_immediata>`
:Prerequisiti: 
	:ref:`Cavalcare<cavalcare>` 1 grado.
:Beneficio: 
	Una volta per round, quando la propria cavalcatura viene colpita in battaglia, si ottiene una prova di :ref:`Cavalcare<cavalcare>` per negare il colpo. 

	Il colpo viene negato se la prova è maggiore del tiro per colpire dell’avversario.

.. _tal_attacco_in_sella:

Attacco in Sella (Combattimento)
---------------------------------
Si è in grado di effettuare attacchi veloci in sella, colpendo l’avversario e continuare a muoversi.

:Prerequisiti: 
	:ref:`Cavalcare<cavalcare>` 1 grado, :ref:`Combattere in Sella<tal_combattere_in_sella>`.
:Beneficio: 
	Quando si è in sella e si :ref:`Carica<azione_carica>`, ci si può muovere e attaccare con una carica standard e poi muoversi ancora (in linea retta). 

	Il movimento totale per quel round non può superare il doppio della propria velocità in sella e il personaggio e la sua cavalcatura non subiscono alcun :ref:`Attacco di Opportunità<azione_attacco_di_opportunità>` da parte dell’avversario attaccato.

.. _tal_carica_devastante:

***************************************
Carica Devastante (Combattimento)
***************************************
Capacità di compiere attacchi devastanti in sella.

:Prerequisiti: 
	:ref:`Cavalcare<cavalcare>` 1 grado, :ref:`Attacco in Sella<tal_attacco_in_sella>`, :ref:`Combattere in Sella<tal_combattere_in_sella>`.
:Beneficio: 
	Quando si è in sella e si :ref:`Carica<azione_carica>` si infliggono danni raddoppiati con un’arma da mischia o triplicati con una lancia da cavaliere.

.. _tal_disarcionare:

Disarcionare (Combattimento)
--------------------------------
Capacità di disarcionare avversari in sella.

:Tipo: :ref:`Azione Gratuita<azione_gratuita>`
:Prerequisiti: 
	For 13, :ref:`Cavalcare<cavalcare>` 1 grado, :ref:`Attacco Poderoso<tal_attacco_poderoso>`, :ref:`Combattere in Sella<tal_combattere_in_sella>`, :ref:`Spingere Migliorato<tal_spingere_migliorato>`, BAB +1.
:Beneficio: 
	Quando si :ref:`Carica<azione_carica>` mentre si è in sella e si impugna una lancia da cavaliere, si risolve l’attacco come al solito, ma se si colpisce si può immediatamente effettuare un tentativo gratuito di :ref:`Spingere<azione_spingere>` oltre al danno normale. 

	Se si ha successo, il bersaglio viene disarcionato dalla sua cavalcatura e cade a terra prono in uno spazio libero adiacente alla sua cavalcatura che si trova di fronte a voi ad una distanza determinata dall’attacco.

.. _tal_tirare_in_sella:

Tirare in Sella (Combattimento)
-------------------------------
Capacità di utilizzare armi a distanza in sella.

:Prerequisiti: 
	:ref:`Cavalcare<cavalcare>` 1 grado, :ref:`Combattere in Sella<tal_combattere_in_sella>`.
:Beneficio: 
	La penalità che si subisce quando si utilizza un’arma a distanza in sella si dimezza: –2 invece di –4 se la cavalcatura compie due azioni di movimento, e –4 invece di –8 se la cavalcatura sta correndo.

.. _tal_travolgere:

Travolgere (Combattimento)
---------------------------
Capacità di usare la cavalcatura per abbattere gli avversari.

:Prerequisiti: 
	:ref:`Cavalcare<cavalcare>` 1 grado, :ref:`Combattere in Sella<tal_combattere_in_sella>`.
:Beneficio: 
	Quando si tenta di :ref:`Oltrepassare<azione_oltrepassare>` un avversario mentre si cavalca, quest’ultimo non può spostarsi. La cavalcatura può effettuare un attacco con gli zoccoli contro ogni avversario travolto, ottenendo bonus +4 al tiro per colpire contro avversari proni.

.. _tal_competenza_negli_scudi:

-----------------------
Competenza negli Scudi
-----------------------
Capacità di usare propriamente gli scudi.

:Beneficio: 
	Quando si usa uno scudo (tranne gli scudi torre), la penalità di armatura alla prova dello scudo si applica solo alle prove di abilità basate su Destrezza e Forza.
:Normale: 
	Se si utilizza uno scudo in cui non si è competenti, si subisce la penalità di armatura alla prova dello scudo al tiro per colpire e a tutte le prove di abilità che riguardano il movimento.
:Speciale: 
	:ref:`Barbari<barbaro>`, :ref:`Bardi<bardo>`, :ref:`Chierici<chierico>`, :ref:`Druidi<druido>`, :ref:`Guerrieri<guerriero>`, :ref:`Paladini<paladino>` e :ref:`Ranger<ranger>` possiedono automaticamente :ref:`Competenza negli Scudi<tal_competenza_negli_scudi>`.

.. _tal_attacco_con_lo_scudo_migliorato:

Attacco con lo Scudo Migliorato (Combattimento)
------------------------------------------------
Ci si può proteggere con lo scudo, anche se lo si usa per attaccare.

:Prerequisiti: 
	:ref:`Competenza negli Scudi<tal_competenza_negli_scudi>`.
:Beneficio: 
	Quando si effettua un :ref:`Attacco con Scudo<attacco_con_scudo>`, si applica sempre il bonus dello scudo alla propria CA.
:Normale: 
	Un personaggio che compie un colpo con lo scudo perde il bonus dello scudo alla CA fino al turno successivo.

.. _tal_botta_di_scudo:

********************************
Botta di Scudo (Combattimento)
********************************
Nella giusta posizione, il proprio scudo può essere usato per mandare gli avversari gambe all’aria.

:Prerequisiti: 
	:ref:`Attacco con lo Scudo Migliorato<tal_attacco_con_lo_scudo_migliorato>`, :ref:`Combattere con Due Armi<tal_combattere_con_due_armi>`, :ref:`Competenza negli Scudi<tal_competenza_negli_scudi>`, BAB +6.
:Beneficio: 
	Qualsiasi avversario colpito dalla :ref:`Botta di Scudo<tal_botta_di_scudo>` è colpito anche da un attacco di :ref:`Spingere<azione_spingere>` gratuito, sostituendo il proprio tiro per colpire con la prova di :ref:`Manovra in Combattimento<azione_manovra_in_combattimento>`. 

	Questa spinta non provoca un :ref:`Attacco di Opportunità<azione_attacco_di_opportunità>`. Gli avversari che non possono muoversi indietro a causa di un muro o di altra superficie cadono a terra proni dopo essersi mossi per la massima distanza possibile. 

	Si può scegliere di muoversi con il bersaglio se si è in grado di compiere un passo di 1,5 metri o di spendere un’azione per :ref:`Muoversi<azione_movimento>` nel turno.

.. _tal_maestria_negli_scudi:

Maestria negli Scudi (Combattimento)
*************************************
Maestria nell’uso degli scudi che permette di combattere con essi senza impedimenti.

:Prerequisiti: 
	:ref:`Attacco con lo Scudo Migliorato<tal_attacco_con_lo_scudo_migliorato>`, :ref:`Botta di Scudo<tal_botta_di_scudo>`, :ref:`Combattere con Due Armi<tal_combattere_con_due_armi>`, :ref:`Competenza negli Scudi<tal_competenza_negli_scudi>`, BAB +11.
:Beneficio: 
	Non si subisce alcuna penalità ai tiri per colpire effettuati con uno scudo mentre si impugna un’altra arma. 

	Si aggiunge il bonus protettivo dello scudo al tiro per colpire e per i danni effettuati con lo scudo come se fosse un bonus di potenziamento.

.. _tal_competenza_negli_scudi_torre:

Competenza negli Scudi Torre (Combattimento)
----------------------------------------------
Capacità nell’uso degli scudi torre.

:Prerequisiti: 
	:ref:`Competenza negli Scudi<tal_competenza_negli_scudi>`.
:Beneficio: 
	Quando si usa uno scudo torre, la penalità di armatura alla prova dello scudo si applica solo alle abilità basate su Forza e Destrezza.
:Normale: 
	Se si usa uno scudo in cui non si è competenti, si subisce la penalità di armatura alla prova dello scudo al tiro per colpire e a tutte le prove di abilità che riguardano il movimento, comprese le prove di :ref:`Cavalcare<cavalcare>`.
:Speciale: 
	I :ref:`Guerrieri<guerriero>` possiedono automaticamente :ref:`Competenza negli Scudi Torre<tal_competenza_negli_scudi_torre>`.

.. _tal_scudo_focalizzato:

Scudo Focalizzato (Combattimento)
----------------------------------
Capacità di deviare i colpi con lo scudo.

:Prerequisiti: 
	:ref:`Competenza negli Scudi<tal_competenza_negli_scudi>`, BAB +1.
:Beneficio: 
	+1 al bonus CA fornito da qualsiasi scudo.

.. _tal_scudo_focalizzato_superiore:

*********************************************
Scudo Focalizzato Superiore (Combattimento)
*********************************************
Capacità di deflettere i colpi con lo scudo.

:Prerequisiti: 
	:ref:`Competenza negli Scudi<tal_competenza_negli_scudi>`, :ref:`Scudo Focalizzato<tal_scudo_focalizzato>`, BAB +1, :ref:`Guerriero<guerriero>` Liv 8.
:Beneficio:
	+1 al bonus CA dato da qualsiasi scudo che si cumula con quello di :ref:`Scudo Focalizzato<tal_scudo_focalizzato>`.

.. _tal_competenza_nelle_armature:
.. _tal_competenza_nelle_armature_leggere:

------------------------------------
Competenza nelle Armature Leggere
------------------------------------
Capacità di indossare ed utilizzare le armature leggere.

:Beneficio: 
	Quando si utilizza un tipo di armatura in cui si ha competenza, la penalità di armatura alla prova si applica solo alle prove di abilità basate su Forza e Destrezza.
:Normale: 
	Se si indossa un’armatura in cui non si ha la competenza si subisce la penalità di armatura alla prova al tiro per colpire e a tutte le prove di abilità che riguardano il movimento.
:Speciale: 
	Tutte le classi tranne :ref:`Maghi<mago>`, :ref:`Monaci<monaco>` e :ref:`Stregoni<stregone>`, possiedono :ref:`Competenza nelle Armature Leggere<tal_competenza_nelle_armature_leggere>`.

.. _tal_competenza_nelle_armature_medie:

Competenza nelle Armature Medie
------------------------------------
Capacità di indossare ed utilizzare le armature medie.

:Prerequisito: 
	:ref:`Competenza nelle Armature Leggere<tal_competenza_nelle_armature_leggere>`.
:Beneficio: 
	Vedi :ref:`Competenza nelle Armature Leggere<tal_competenza_nelle_armature_leggere>`.
:Normale:
	 Vedi :ref:`Competenza nelle Armature Leggere<tal_competenza_nelle_armature_leggere>`.
:Speciale: 
	:ref:`Barbari<barbaro>`, :ref:`Chierici<chierico>`, :ref:`Druidi<druido>`, :ref:`Guerrieri<guerriero>`, :ref:`Paladini<paladino>` e :ref:`Ranger<ranger>` ricevono automaticamente :ref:`Competenza nelle Armature Medie<tal_competenza_nelle_armature_medie>`.

.. _tal_competenza_nelle_armature_pesanti:

***********************************
Competenza nelle Armature Pesanti
***********************************
Capacità di indossare ed utilizzare le armature pesanti.

:Prerequisito: 
	:ref:`Competenza nelle Armature Leggere<tal_competenza_nelle_armature_leggere>`.
:Beneficio: 
	Vedi :ref:`Competenza nelle Armature Leggere<tal_competenza_nelle_armature_leggere>`.
:Normale:
	 Vedi :ref:`Competenza nelle Armature Leggere<tal_competenza_nelle_armature_leggere>`, :ref:`Competenza nelle Armature Medie<tal_competenza_nelle_armature_medie>`.
:Speciale: 
	`Guerrieri<guerriero>` e :ref:`Paladini<paladino>` ricevono automaticamente :ref:`Competenza nelle Armature Pesanti<tal_competenza_nelle_armature_pesanti>`.

.. _tal_competenza_nelle_armi:
.. _tal_competenza_nelle_armi_da_guerra:

------------------------------------------------
Competenza nelle Armi da Guerra
------------------------------------------------
Si sceglie un tipo di arma da guerra per imparare come brandire quel tipo di arma in battaglia.

:Beneficio: 
	Si effettua normalmente il tiro per colpire con l’arma.
:Normale: 
	Se si utilizza un’arma in cui non si è competenti si subisce penalità –4 al tiro per colpire. 
:Speciale: 
	:ref:`Barbari<barbaro>`, :ref:`Guerrieri<guerriero>`, :ref:`Paladini<paladino>` e :ref:`Ranger<ranger>` sono competenti in tutte le armi da guerra. 

	Si può acquisire :ref:`Competenza nelle Armi da Guerra<tal_competenza_nelle_armi_da_guerra>` più volte applicandolo a un nuovo tipo di arma.

.. _tal_competenza_nelle_armi_esotiche:

------------------------------------------------
Competenza nelle Armi Esotiche (Combattimento)
------------------------------------------------
Si sceglie un tipo di arma esotica, come la catena chiodata o la frusta, per imparare come brandire quel tipo di arma ed utilizzare qualsiasi qualità o trucco che tale arma permette.

:Prerequisiti: 
	BAB +1.
:Beneficio: 
	Si effettua normalmente il tiro per colpire con un’arma esotica.
:Normale: 
	Chi utilizza un’arma in cui non è competente subisce penalità –4 al tiro per colpire.
:Speciale: 
	Si può acquisire :ref:`Competenza nelle Armi Esotiche<tal_competenza_nelle_armi_esotiche>` più volte applicandolo a un nuovo tipo di arma.

.. _tal_competenza_nelle_armi_semplici:

----------------------------------------
Competenza nelle Armi Semplici
----------------------------------------
Capacità di usare tutti i tipi di armi semplici in battaglia.

:Beneficio: 
	Si effettua normalmente il tiro per colpire con le armi semplici.
:Normale: 
	Se si utilizza un’arma in cui non si è competenti si subisce penalità –4 al tiro per colpire.
:Speciale: 
	Tutti i personaggi, tranne :ref:`Druidi<druido>`, :ref:`Maghi<mago>` e :ref:`Monaci<monaco>`, sono automaticamente competenti in tutte le armi semplici.

.. _tal_controincantesimo_migliorato:

------------------------------
Controincantesimo Migliorato
------------------------------
Capacità di contrastare gli incantesimi usando magie simili.

:Beneficio: 
	Quando si :ref:`Contrastano gli Incantesimi<azione_contrastare_incantesimo>`, si può usare un incantesimo della stessa scuola che è di uno o più livelli superiore all’incantesimo bersaglio.
:Normale: 
	Si può contrastare un incantesimo solo con lo stesso incantesimo o con un incantesimo specificamente designato per contrastare l’incantesimo bersaglio.

.. _tal_correre:

----------------
Correre
----------------
Capacità di correre velocemente.

:Beneficio: 
	Se si va di corsa, ci si muove cinque volte più velocemente della propria normale velocità, senza armatura o con una media o leggera, trasportando al massimo un carico medio, o quattro volte più velocemente se si indossa un’armatura pesante o si trasporta un carico pesante. 

	Se si fa un salto con rincorsa si ottiene bonus +4 alla prova di :ref:`Acrobazia<acrobazia>`. Quando si corre, si mantiene il proprio bonus di Destrezza alla CA.
:Normale: 
	Se si va di corsa, ci si muove quattro volte più velocemente della propria normale velocità o tre a seconda dell'armatura e del carico e si perde il proprio bonus di Destrezza alla CA.

.. _tal_critico_focalizzato:

---------------------------------------------
Critico Focalizzato (Combattimento, Critico)
---------------------------------------------
Capacità di infliggere e causare dolore.

:Prerequisiti: 
	BAB +9.
:Beneficio: 
	+4 ai tiri per colpire effettuati per confermare i colpi critici.

.. _tal_critico_accecante:

Critico Accecante (Combattimento, Critico)
---------------------------------------------
I colpi critici rendono ciechi gli avversari.

:Prerequisiti: 
	:ref:`Critico Focalizzato<tal_critico_focalizzato>`, BAB +15.
:Beneficio: 
	Ogni volta che si mette a segno un colpo critico l’avversario è accecato in modo permanente. Un TS riuscito su Tempra con **CD = 10 + BAB**, rende l’avversario abbagliato per 1d4 round. 

	Questo talento non ha effetto sulle creature che non basano la vista sugli occhi o quelle con più di due occhi (sebbene colpi critici multipli potrebbero causare cecità, a discrezione del GM). 

	La cecità può essere curata con :ref:`Guarigione<inc_guarigione>`, :ref:`Rigenerazione<inc_rigenerazione>`, :ref:`Rimuovi Cecità<inc_rimuovi_cecità>` o capacità simili.
:Speciale: 
	Si possono applicare gli effetti di un solo talento critico ad uno specifico colpo critico, a meno che non si possegga :ref:`Critico Prodigioso<tal_critico_prodigioso>`.

.. _tal_critico_affaticante:

Critico Affaticante (Combattimento, Critico)
---------------------------------------------
I colpi critici affaticano l'avversario.

:Prerequisiti: 
	:ref:`Critico Focalizzato<tal_critico_focalizzato>`, BAB +13.
:Beneficio: 
	Ogni volta che si mette a segno un colpo critico l’avversario diventa affaticato.

	Questo talento non ha effetti addizionali su creature già affaticate o esauste.
:Speciale: 
	Si possono applicare gli effetti di un solo talento critico ad uno specifico colpo critico, a meno che non si possegga :ref:`Critico Prodigioso<tal_critico_prodigioso>`.

.. _tal_critico_inesorabile:

*********************************************
Critico Inesorabile (Combattimento, Critico)
*********************************************
I colpi critici rendono esausti gli avversari.

:Prerequisiti: 
	:ref:`Critico Affaticante<tal_critico_affaticante>`, :ref:`Critico Focalizzato<tal_critico_focalizzato>`, BAB +15.
:Beneficio: 
	Ogni volta che si mette a segno un colpo critico l’avversario diventa esausto.

	Questo talento non ha effetti addizionali su creature già esauste.
:Speciale: 
	Si possono applicare gli effetti di un solo talento critico ad uno specifico colpo critico, a meno che non si possegga :ref:`Critico Prodigioso<tal_critico_prodigioso>`.

.. _tal_critico_assordante:

Critico Assordante (Combattimento, Critico)
---------------------------------------------
I colpi critici fanno perdere l'udito agli avversari.

:Prerequisiti: 
	:ref:`Critico Focalizzato<tal_critico_focalizzato>`, BAB +13.
:Beneficio: 
	Ogni volta che si mette a segno un colpo critico l’avversario diventa sordo in modo permanente. Un TS riuscito su Tempra con **CD = 10 + BAB** riduce la sordità a 1 round.

	Questo talento non ha effetto sulle creature già sorde.

	La sordità può essere curata con :ref:`Guarigione<inc_guarigione>`, :ref:`Rigenerazione<inc_rigenerazione>`, :ref:`Rimuovi Sordità<inc_rimuovi_sordità>` o capacità simili.
:Speciale: 
	Si possono applicare gli effetti di un solo talento critico ad uno specifico colpo critico, a meno che non si possegga :ref:`Critico Prodigioso<tal_critico_prodigioso>`.

.. _tal_critico_debilitante:

Critico Debilitante (Combattimento, Critico)
---------------------------------------------
I colpi critici rendono infermo l'avversario.

:Prerequisiti: 
	:ref:`Critico Focalizzato<tal_critico_focalizzato>`, BAB +11.
:Beneficio: 
	Ogni volta che si mette a segno un colpo critico l’avversario diventa infermo per 1 minuto.

	Gli effetti di questo talento non si cumulano. Colpi addizionali invece si aggiungono alla durata dell’effetto.
:Speciale: 
	Si possono applicare gli effetti di un solo talento critico ad uno specifico colpo critico, a meno che non si possegga :ref:`Critico Prodigioso<tal_critico_prodigioso>`.

.. _tal_critico_incapacitante:

Critico Incapacitante (Combattimento, Critico)
----------------------------------------------
I colpi critici rendono incapace l'avversario.

:Prerequisiti: 
	:ref:`Critico Focalizzato<tal_critico_focalizzato>`, BAB +13.
:Beneficio: 
	Ogni volta che si mette a segno un colpo critico l’avversario diventa barcollante per 1d4+1 round. Un TS riuscito su Tempra con **CD = 10 + BAB** riduce la durata a 1 round.

	Gli effetti di questo talento non si cumulano. Colpi addizionali invece si aggiungono alla durata dell’effetto.
:Speciale: 
	Si possono applicare gli effetti di un solo talento critico ad uno specifico colpo critico, a meno che non si possegga :ref:`Critico Prodigioso<tal_critico_prodigioso>`.

.. _tal_critico_stordente:

***********************************************
Critico Stordente (Combattimento, Critico)
***********************************************
I colpi critici stordiscono l'avversario.

:Prerequisiti: 
	:ref:`Critico Focalizzato<tal_critico_focalizzato>`, :ref:`Critico Incapacitante<tal_critico_incapacitante>`, BAB +17.
:Beneficio: 
	Ogni volta che si mette a segno un colpo critico l’avversario diventa stordito per 1d4+1 round. Un TS riuscito su Tempra con **CD = 10 + BAB** riduce la durata a 1d4 round.

	Gli effetti di questo talento non si cumulano. Colpi addizionali invece si aggiungono alla durata dell’effetto.
:Speciale: 
	Si possono applicare gli effetti di un solo talento critico ad uno specifico colpo critico, a meno che non si possegga :ref:`Critico Prodigioso<tal_critico_prodigioso>`.

.. _tal_critico_prodigioso:

Critico Prodigioso (Combattimento)
----------------------------------
I colpi critici causano due effetti addizionali.

:Prerequisiti: 
	:ref:`Critico Focalizzato<tal_critico_focalizzato>`, altri due talenti critici, :ref:`Guerriero<guerriero>` Liv 14.
:Beneficio: 
	Quando si mette a segno un colpo critico, si applicano gli effetti di due talenti critici oltre al danno inflitto.
:Normale: 
	Si possono applicare solo gli effetti di un talento critico oltre al danno inflitto.

.. _tal_critico_sanguinante:

Critico Sanguinante (Combattimento, Critico)
----------------------------------------------
I colpi critici rendono incapace l'avversario.

:Prerequisiti: 
	:ref:`Critico Focalizzato<tal_critico_focalizzato>`, BAB +11.
:Beneficio: 
	Ogni volta che si mette a segno un colpo critico, con un'arma tagliente o perforante, l’avversario subisce 2d6 danni da :ref:`Sanguinamento<sanguinamento>` ad ogni round nel suo turno, oltre ai normali danni. 

	Il danno da sanguinamento può essere fermato con una prova di :ref:`Guarire<guarire>` con CD 15 o con una cura magica. 

	Gli effetti di questo talento non si cumulano. 
:Speciale: 
	Si possono applicare gli effetti di un solo talento critico ad uno specifico colpo critico, a meno che non si possegga :ref:`Critico Prodigioso<tal_critico_prodigioso>`.

.. _tal_critico_migliorato:

-----------------------------------
Critico Migliorato (Combattimento)
-----------------------------------
Gli attacchi con un’arma scelta sono particolarmente letali.

:Prerequisiti: 
	:ref:`Competenza nell’arma<tal_competenza_nelle_armi>`, BAB +8.
:Beneficio: 
	Quando si impugna l’arma prescelta, l’intervallo di minaccia di quell’arma viene raddoppiato.
:Speciale: 
	Si può acquisire :ref:`Critico Migliorato<tal_critico_migliorato>` più volte, applicandolo a un nuovo tipo di arma. 

	Questo bonus non è cumulabile con alcun altro effetto che estende l’intervallo di minaccia.

.. _tal_dirompente:

--------------------------
Dirompente (Combattimento)
--------------------------
L’addestramento rende difficile agli incantatori nemici lanciare incantesimi nelle vicinanze in modo sicuro.

:Prerequisiti: 
	:ref:`Guerriero<guerriero>` Liv 6.
:Beneficio: 
	La CD per :ref:`Lanciare Incantesimi sulla Difensiva<azione_lanciare_incantesimi_sulla_difensiva>` aumenta di +4 per tutti i nemici che sono all’interno dell’area che minacciate. 

	Questo aumento si applica solo se si è consapevoli della posizione del nemico e si è in grado di effettuare un :ref:`Attacco di Opportunità<azione_attacco_di_opportunità>`. 

	Se è possibile compiere un solo :ref:`Attacco di Opportunità<azione_attacco_di_opportunità>` per round e lo si è già usato, questo aumento non si applica.

.. _tal_spezzaincantesimi:

Spezzaincantesimi (Combattimento)
---------------------------------
Si possono colpire gli incantatori avversari che non riescono a :ref:`Lanciare Incantesimi sulla Difensiva<azione_lanciare_incantesimi_sulla_difensiva>` quando li si minaccia.

:Prerequisiti: 
	:ref:`Dirompente<tal_dirompente>`, :ref:`Guerriero<guerriero>` Liv 10.
:Beneficio: 
	Gli avversari nell’area che si minaccia che falliscono la prova di :ref:`Lanciare Incantesimi sulla Difensiva<azione_lanciare_incantesimi_sulla_difensiva>` provocano un :ref:`Attacco di Opportunità<azione_attacco_di_opportunità>`.
:Normale: 
	Gli avversari che falliscono la loro prova di lanciare sulla difensiva non provocano un :ref:`Attacco di Opportunità<azione_attacco_di_opportunità>`.

.. _tal_escludere_materiali:

---------------------
Escludere Materiali
---------------------
Si possono lanciare incantesimi facendo a meno delle componenti materiali.

:Beneficio: 
	È possibile lanciare qualsiasi incantesimo che necessiti di una componente materiale del **costo di 1 mo** o meno, senza aver bisogno di quella componente.
	
	Lanciare l’incantesimo provoca comunque un :ref:`Attacco di Opportunità<azione_attacco_di_opportunità>`, come di norma. 

.. _tal_esibizione_bardica_extra:

-------------------------------
Esibizione Bardica Extra
-------------------------------
Capacità di usare :ref:`Esibizione Bardica<esibizione_bardica>` più a lungo.

:Prerequisiti: 
	:ref:`Esibizione Bardica<esibizione_bardica>`.
:Beneficio: 
	Si può usare :ref:`Esibizione Bardica<esibizione_bardica>` per 6 round addizionali al giorno.
:Speciale: 
	Si può ottenere questo talento più volte con effetti cumulativi.

.. _tal_estrazione_rapida:

---------------------------------
Estrazione Rapida (Combattimento)
---------------------------------
Capacità di estrarre le armi a velocità impressionante.

:Prerequisiti: 
	BAB +1.
:Beneficio: 
	Si può estrarre un’arma come azione gratuita invece che come azione di movimento. 
	Si può estrarre un’arma nascosta (vedi :ref:`Rapidità di Mano<rapidità_di_mano>`) come :ref:`Azione di Movimento<azione_movimento>`.

	Un personaggio che ha scelto questo talento può lanciare armi al suo normale numero completo di attacchi (proprio come un personaggio con l’arco), ma non si può applicare a bacchette, oggetti alchemici, pergamene e pozioni.
:Normale: 
	Si può estrarre un’arma come :ref:`Azione di Movimento<azione_movimento>`, o (con BAB > +1) come :ref:`Azione Gratuita<azione_gratuita>` compresa nel movimento e si può estrarre un’arma nascosta soltanto come :ref:`Azione Standard<azione_standard>`.

.. _tal_famiglio_migliorato:

---------------------
Famiglio Migliorato
---------------------
Questo talento permette di acquisire un famiglio più potente, ma solo se è possibile acquisire un nuovo famiglio.

:Prerequisiti: 
	Capacità di acquisire un nuovo famiglio, allineamento compatibile, livello sufficientemente alto (vedi sotto).
:Beneficio: 
	Quando si sceglie un famiglio, si possono scegliere anche le creature elencate, le statistiche nel :ref:`Bestiario<bestiario>`. 

	.. image:: Immagini/TalentiFamiglioMigliorato.png

	Si può scegliere un famiglio con un allineamento diverso fino ad un grado dell’asse dell’allineamento (da legale a caotico, da buono a malvagio).

I famigli migliorati per il resto usano le regole dei famigli normali, con due eccezioni: se il tipo della creatura è diverso da animale, esso non cambia, e i non guadagnano la capacità di parlare con altre creature della loro specie (sebbene molti di essi abbiano comunque la capacità di comunicare).

.. _tal_furtivo:

----------------------
Furtivo
----------------------
Si passa inosservati e si è abili a slegarsi.

:Beneficio: 
	+2 alle prove di :ref:`Artista della Fuga<artista_della_fuga>` e :ref:`Furtività<furtività>` che aumenta a +4 se si hanno 10 gradi o più.

.. _tal_imposizione_delle_mani_extra:

-----------------------------------
Imposizione delle Mani Extra
-----------------------------------
Capacità di usare imposizione delle mani più spesso.

:Prerequisiti: 
	:ref:`Imposizione delle Mani<imposizione_delle_mani>`.
:Beneficio: 
	Si può usare la propria capacità :ref:`Imposizione delle Mani<imposizione_delle_mani>` due volte in più al giorno.
:Speciale: 
	Si può acquisire più volte cumulandone gli effetti.

.. _tal_incanalare_allineamento:

-----------------------------------
Incanalare Allineamento
-----------------------------------
Si può scegliere bene, caos, legge o male per incanalare energia divina e colpire esterni di quel sottotipo.

:Prerequisiti:
	:ref:`Incanalare Energia<incanalare_energia>`.
:Beneficio: 
	Al posto del suo normale effetto, si può scegliere che la propria :ref:`Capacità di Incanalare Energia<incanalare_energia>` curi o ferisca gli esterni con quel particolare sottotipo di allineamento. 

	Bisogna effettuare la scelta ogni volta che si incanala energia e se si sceglie di curare o ferire le creature di quel particolare sottotipo di allineamento e l’energia incanalata non ha effetto su altre creature. 

	Il danno curato o inflitto e la CD per dimezzarlo non subiscono variazioni.
:Speciale: 
	Si può ottenere questo talento più volte applicandolo ad un nuovo sottotipo di allineamento. Ogni volta che si incanala, bisogna scegliere il sottotipo su cui si vuole abbia effetto.

.. _tal_incanalare_elementale:

-----------------------------------
Incanalare Elementale
-----------------------------------
Si può scegliere un sottotipo elementale, come acqua, aria, fuoco o terra per incanalare energia divina e colpire esterni di quel sottotipo.

:Prerequisiti:
	:ref:`Incanalare Energia<incanalare_energia>`.
:Beneficio: 
	Al posto del suo normale effetto, si può scegliere che la propria :ref:`Capacità di Incanalare Energia<incanalare_energia>` curi o ferisca gli esterni di quell'elemento. 

	Bisogna effettuare la scelta ogni volta che si incanala energia e se si sceglie di curare o ferire le creature di quel particolare sottotipo e l’energia incanalata non ha effetto su altre creature. 

	Il danno curato o inflitto e la CD per dimezzarlo non subiscono variazioni.
:Speciale: 
	Si può ottenere questo talento più volte applicandolo ad un nuovo sottotipo e ogni volta che si incanala bisogna scegliere su quale si vuole avere effetto.

.. _tal_incanalare_extra:

-----------------------------------
Incanalare Extra
-----------------------------------
Capacità di incanalare energia più spesso.

:Prerequisiti:
	:ref:`Incanalare Energia<incanalare_energia>`.
:Beneficio: 
	È possibile :ref:`Capacità di Incanalare Energia<incanalare_energia>` due volte in più al giorno.
:Speciale: 
	Se un :ref:`Paladino<paladino>` con la capacità di `Capacità di Incanalare Energia Positiva<incanalare_energia_positiva>` prende questo talento, può usare :ref:`Imposizione delle Mani<imposizione_delle_mani>` quattro volte in più al giorno, ma solo per `Capacità di Incanalare Energia Positiva<incanalare_energia_positiva>`.

.. _tal_incanalare_migliorato:

-----------------------------------
Incanalare Migliorato
-----------------------------------
L’energia incanalata è più difficile da resistere.

:Prerequisiti:
	:ref:`Capacità di Incanalare Energia<incanalare_energia>`.
:Beneficio:
	Si aggiunge 2 alla CD dei TS effettuati per resistere agli effetti della propria capacità di incanalare.

.. _tal_incanalare_punizione:

------------------------------------
Incanalare Punizione (Combattimento)
------------------------------------
L’energia divina viene incanalata nell’arma impugnata.

:Tipo: :ref:`Azione Veloce<azione_veloce>`
:Prerequisiti:
	:ref:`Incanalare Energia<incanalare_energia>`.
:Beneficio:
	Prima di effettuare un attacco, si può decidere di spendere un uso della propria capacità di :ref:`Capacità di Incanalare Energia<incanalare_energia>`. 

	Se si :ref:`Incanala Energia<incanalare_energia_positiva>` positiva e si colpisce una creatura non morta, essa subisce una quantità di danni addizionali pari al danno inflitto dalla propria capacità di :ref:`Incanalare Energia<incanalare_energia>`  positiva. Lo stesso vale per l'energia negativa con le creature viventi.

	Il bersaglio può effettuare un TS su Volontà, come di norma, per dimezzare questo danno addizionale. Se l’attacco non va a segno, :ref:`Incanalare Energia<incanalare_energia>` viene comunque speso senza produrre effetti.


.. _tal_incanalare_selettivo:

------------------------------------
Incanalare Selettivo
------------------------------------
Capacità di scegliere a chi estendere gli effetti di incanalare.

:Prerequisiti:
	:ref:`Incanalare Energia<incanalare_energia>`, CAR 13.
:Beneficio:
	Quando si :ref:`Incanala Energia<incanalare_energia>`, si può scegliere un numero di bersagli nell’area fino al proprio Mod Car che non vengano colpiti dai suoi effetti.
:Normale: 
	Tutti i bersagli in un’esplosione di 9 metri sono colpiti dagli effetti di :ref:`Incanalare Energia<incanalare_energia>`: si può solo scegliere se includere o meno se stessi.

.. _tal_incantare_in_combattimento:

----------------------------
Incantare in Combattimento
----------------------------
Capacità di lanciare incantesimi sotto minaccia o distrazione.

:Beneficio: 
	+4 alle prove di concentrazione effettuate per lanciare un incantesimo o utilizzare una capacità magica mentre si lancia sulla difensiva o si è in lotta.

.. _tal_incantesimi_focalizzati:

------------------------
Incantesimi Focalizzati
------------------------
Gli incantesimi della scuola di magia scelta diventano più potenti del normale.

:Beneficio: 
	+1 alla CD di tutti i TS contro gli incantesimi della scuola di magia
	selezionata.
:Speciale: 
	Si può acquisire più volte applicandolo a diverse scuole di magia.

.. _tal_incantesimi_focalizzati_superiore:

Incantesimi Focalizzati Superiore
----------------------------------
Gli incantesimi della scuola scelta sono più difficili da resistere.

:Prerequisiti: 
	:ref:`Incantesimi Focalizzati<tal_incantesimi_focalizzati>`.
:Beneficio: 
	+1 alla CD di tutti i TS contro gli incantesimi della scuola di magia che si è scelta che si cumula con quello di :ref:`Incantesimi Focalizzati<tal_incantesimi_focalizzati>`.
:Speciale: 
	Si può acquisire più volte applicandolo a diverse scuole di magia, già scelte con :ref:`Incantesimi Focalizzati<tal_incantesimi_focalizzati>`.

.. _tal_aumentare_evocazione:

Aumentare Evocazione
----------------------
Le creature evocate sono più forti e robuste.

:Prerequisito: 
	:ref:`Incantesimi Focalizzati<tal_incantesimi_focalizzati>` (evocazione).
:Beneficio: 
	Ogni creatura evocata con un qualsiasi incantesimo :ref:`Evoca<inc_evoca>` ottiene +4 a Forza e Costituzione per la durata dell’incantesimo di evocazione.

.. _tal_incantesimi_inarrestabili:

-------------------------
Incantesimi Inarrestabili
-------------------------
Gli incantesimi sono molto potenti e penetrano attraverso la resistenza agli incantesimi con più facilità.

:Beneficio: 
	+2 alle prove di livello incantatore (1d20 + LI) effettuate per superare la resistenza agli incantesimi di una creatura.

.. _tal_incantesimi_inarrestabili_superiore:

Incantesimi Inarrestabili Superiore
-----------------------------------
Gli incantesimi penetrano più facilmente la resistenza agli incantesimi.

:Prerequisiti: 
	:ref:`Incantesimi Inarrestabili<tal_incantesimi_inarrestabili>`.
:Beneficio: 
	+2 alle prove di livello incantatore (1d20 + LI) effettuate per superare la resistenza agli incantesimi di una creatura, che si cumula con quello di :ref:`Incantesimi Inarrestabili<tal_incantesimi_inarrestabili>`.

.. _tal_incantesimi_naturali:

-------------------------
Incantesimi Naturali
-------------------------
Capacità di lanciare incantesimi anche se si è in una forma che non lo permetterebbe.

:Prerequisiti: 
	Sag 13, :ref:`Forma Selvatica<forma_selvatica>`.
:Beneficio: 
	Si possono sostituire le componenti verbali e somatiche degli incantesimi mentre si è in forma selvatica, con altri gesti e suoni. 

	Si possono utilizzare le componenti materiali e i focus posseduti, anche se questi oggetti sono fusi nella propria forma attuale. 

	Questo talento non consente l’uso di oggetti magici mentre si è in una forma che non potrebbe normalmente utilizzarli, e non si ottiene la capacità di parlare mentre si è in forma selvatica.

.. _tal_indulgenza_extra:

-------------------------
Indulgenza Extra
-------------------------
La propria capacità di :ref:`Imposizione delle Mani<imposizione_delle_mani>` aggiunge una :ref:`Indulgenza<indulgenza>` addizionale.

:Prerequisiti: 
	:ref:`Imposizione delle Mani<imposizione_delle_mani>`, :ref:`Indulgenza<indulgenza>`.
:Beneficio: 
	Si scelga una indulgenza addizionale per la quale si è qualificati e quando si usa :ref:`Imposizione delle Mani<imposizione_delle_mani>` per curare i danni ad un bersaglio, quest’ultimo riceve anche l’effetto addizionale dell’:ref:`Indulgenza<indulgenza>` scelta.
:Speciale: 
	Si può ottenere questo talento più volte scegliendo diverse Indulgenze<indulgenza>`.

.. _tal_ingannevole:

-----------------
Ingannevole
-----------------
Capacità di ingannare gli altri, o con le parole o con il mascheramento fisico.

:Beneficio: 
	+2 alle prove di :ref:`Camuffare<camuffare>` e :ref:`Raggirare<raggirare>` che aumenta a +4 se si hanno 10 gradi o più.

.. _tal_iniziativa_migliorata:

-------------------------------------
Iniziativa Migliorata (Combattimento)
-------------------------------------
Capacità di reagire molto più velocemente al pericolo.

:Beneficio: 
	+4 alle prove di iniziativa.

.. _tal_inseguire:

---------------------------
Inseguire (Combattimento)
---------------------------
Capacità di inseguire rapidamente l’avversario che fugge.

:Tipo: :ref:`Azione Immediata<azione_immediata>`
:Prerequisiti: 
	BAB +1.
:Beneficio: 
	Quando un avversario adiacente tenta di effettuare un passo di 1,5 metri per allontanarsi, è possibile effettuare immediatamente un passo di 1,5 metri sempre che si finisca adiacenti all’avversario. 

	Se si effettua questo passo, non si può effettuare un passo di 1,5 metri durante il proprio turno successivo e se si usa un’:ref:`Azione per Muoversi<azione_movimento>` durante il turno successivo, si devono sottrarre 1,5 metri dal movimento totale che si può effettuare.

.. _tal_ira_extra:

----------------
Ira Extra
----------------
Capacità di usare ira più a lungo del normale.

:Prerequisiti: 
	:ref:`Ira<ira>`.
:eneficio: 
	Si può usare ira per 6 round in più al giorno.
:Speciale: 
	Ottenibile più volte cumulandone gli effetti.

.. _tal_ki_extra:

------------------
Ki Extra
------------------
Si può usare la propria riserva ki più volte al giorno.

:Prerequisiti: 
	:ref:`Riserva Ki<riserva_ki>`.
:Beneficio: 
	+2 alla :ref:`Riserva Ki<riserva_ki>`.
:Speciale: 
	Ottenibile più volte cumulandone gli effetti.

.. _tal_lanciare_oggetti:

--------------------------------
Lanciare Oggetti (Combattimento)
--------------------------------
Capacità di lanciare oggetti a portata di mano con grande efficacia.

:Beneficio: 
	Non si subiscono penalità per usare un’arma improvvisata a distanza e bonus di circostanza +1 agli attacchi effettuati con armi da lancio a spargimento.
:Normale: 
	–4 agli attacchi effettuati con un’arma improvvisata.

.. _tal_maestria_in_combattimento:

----------------------------------------------
Maestria in Combattimento (Combattimento)
----------------------------------------------
Capacità di migliorare la propria difesa a spese dell’accuratezza del colpo.

:Prerequisiti: 
	Int 13.
:Beneficio: 
	Si può scegliere di subire penalità –1 ai tiri per colpire in mischia e alle prove di manovre in combattimento per ottenere bonus di schivare +1 alla CA, fino al proprio turno successivo.

	Quando il bonus di attacco base raggiunge +4, e ogni +4 successivamente, la penalità aumenta di –1 e il bonus di schivare aumenta di +1. 

	Si può scegliere di usare questo talento solo quando si dichiara di effettuare un attacco o un’:ref:`Azione di Attacco Completo<azione_di_attacco_completo>` con un’arma da mischia. 

.. _tal_attacco_turbinante:

Attacco Turbinante (Combattimento)
--------------------------------------------
Si possono colpire tutti gli avversari a portata.

:Prerequisiti: 
	Des 13, Int 13, :ref:`Attacco Rapido<tal_attacco_rapido>`, :ref:`Maestria in Combattimento<tal_maestria_in_combattimento>`, :ref:`Mobilità<tal_mobilità>`, :ref:`Schivare<tal_schivare>`, BAB +4.
:Beneficio: 
	Quando si compie un’:ref:`Azione di Attacco Completo<azione_di_attacco_completo>`, si può rinunciare agli attacchi normali ed effettuare un unico attacco in mischia con il bonus di attacco base più alto contro ogni avversario che si trovi entro la propria portata. 

	Occorre effettuare un tiro per colpire separato per ogni avversario e non si possono applicare i bonus o gli attacchi extra conferiti da altri talenti, capacità o incantesimi.

.. figure:: Immagini/TalentiAttaccoTurbinante.png
	:align: center

.. _tal_disarmare_migliorato:

Disarmare Migliorato (Combattimento)
---------------------------------------
Capacità di disarmare gli avversari negli scontri in mischia.

:Prerequisiti: 
	Int 13, :ref:`Maestria in Combattimento<tal_maestria_in_combattimento>`.
:Beneficio: 
	Non si provoca alcun :ref:`Attacco di Opportunità<azione_attacco_di_opportunità>` quando si tenta una manovra di :ref:`Disarmare<azione_disarmare>` in combattimento.

	+2 alle prove di :ref:`Disarmare<azione_disarmare>` un avversario e alla DMC ogni volta che un avversario cerca di disarmarvi a propria volta.
:Normale:
	Si provoca un :ref:`Attacco di Opportunità<azione_attacco_di_opportunità>` quando si compie una manovra di :ref:`Disarmare<azione_disarmare>` in combattimento.

.. _tal_disarmare_superiore:

***********************************
Disarmare Superiore (Combattimento)
***********************************
Capacità di far slittare le armi lontano dalla presa dell’avversario.

:Prerequisiti: 
	Int 13, :ref:`Disarmare Migliorato<tal_disarmare_migliorato>`, :ref:`Maestria in Combattimento<tal_maestria_in_combattimento>`, BAB +6.
:Beneficio: 
	+2 alle prove effettuate per :ref:`Disarmare<azione_disarmare>` un avversario che si somma a quello di :ref:`Disarmare Migliorato<tal_disarmare_migliorato>`. 

	Se si riesce a disarmare, l’arma atterra a 4,5 metri da chi la stava impugnando, in una direzione casuale.
:Normale: 
	Le armi e l’equipaggiamento cadono ai piedi della creatura disarmata.

.. _tal_fintare_migliorato:

Fintare Migliorato (Combattimento)
---------------------------------------
Capacità di confondere le idee agli avversari contro cui si combatte.

:Tipo: :ref:`Azione di Movimento<azione_movimento>`
:Prerequisiti: 
	Int 13, :ref:`Maestria in Combattimento<tal_maestria_in_combattimento>`.
:Beneficio: 
	 Si può effettuare una prova di :ref:`Raggirare<raggirare>` per :ref:`Fintare<azione_fintare>` una mossa d’attacco.
:Normale: 
	:ref:`Fintare<azione_fintare>` in combattimento è un’:ref:`Azione Standard<azione_standard>`.

.. _tal_fintare_superiore:

***********************************
Fintare Superiore (Combattimento)
***********************************
Capacità di far slittare le armi lontano dalla presa dell’avversario.

:Prerequisiti: 
	Int 13, :ref:`Fintare Migliorato<tal_fintare_migliorato>`, :ref:`Maestria in Combattimento<tal_maestria_in_combattimento>`, BAB +6.
:Beneficio: 
	Ogni volta che si usa :ref:`Fintare<azione_fintare>` per far perdere ad un avversario il suo bonus di Destrezza, egli perde il bonus fino all’inizio del turno successivo di chi compie la finta.
:Normale: 
	La creatura che subisce una finta perde il suo bonus di Destrezza contro l’attacco successivo di chi la compie.

.. _tal_sbilanciare_migliorato:

Sbilanciare Migliorato (Combattimento)
---------------------------------------
Capacità di sbilanciare e buttare a terra gli avversari.

:Prerequisiti: 
	Int 13, :ref:`Maestria in Combattimento<tal_maestria_in_combattimento>`.
:Beneficio: 
	Non si provoca alcun :ref:`Attacco di Opportunità<azione_attacco_di_opportunità>` quando si tenta una manovra di :ref:`Sbilanciare<azione_sbilanciare>` in combattimento.

	+2 alle prove di :ref:`Sbilanciare<azione_sbilanciare>` un avversario e alla DMC ogni volta che un avversario cerca di sbilanciarvi a propria volta.
:Normale:
	Si provoca un :ref:`Attacco di Opportunità<azione_attacco_di_opportunità>` quando si compie una manovra di :ref:`Sbilanciare<azione_sbilanciare>` in combattimento.

.. _tal_sbilanciare_superiore:

*************************************
Sbilanciare Superiore (Combattimento)
*************************************
Capacità di effettuare attacchi gratuiti sui nemici buttati a terra

:Prerequisiti: 
	Int 13, :ref:`Maestria in Combattimento<tal_maestria_in_combattimento>`, :ref:`Sbilanciare Migliorato<tal_sbilanciare_migliorato>`, BAB +6.
:Beneficio: 
	+2 alle prove effettuate per :ref:`Sbilanciare<azione_sbilanciare>` un avversario che si somma a quello di :ref:`Sbilanciare Migliorato<tal_sbilanciare_migliorato>`. 

	Ogni volta che si sbilancia con successo un avversario, quell’avversario provoca un :ref:`Attacco di Opportunità<azione_attacco_di_opportunità>`.
:Normale: 
	 Le creature non provocano :ref:`Attacchi di Opportunità<azione_attacco_di_opportunità>` dall’essere sbilanciate.

.. _tal_maestria_nelle_armi_improvvisate:

--------------------------------------------------
Maestria nelle Armi Improvvisate (Combattimento)
--------------------------------------------------
Capacità di trasformare qualsiasi oggetto in un’arma mortale, dalla gamba di una sedia ad un sacco di farina.

:Prerequisiti: 
	:ref:`Cogliere di Sorpresa<tal_cogliere_di_sorpresa>` o :ref:`Lanciare Oggetti<tal_lanciare_oggetti>`, BAB +8.
:Beneficio: 
	Non si subiscono le penalità per usare un’arma improvvisata. 

	Si aumenta la quantità di danno inflitto dall’arma improvvisata di un grado (per esempio, 1d4 diventa 1d6) fino al massimo ad 1d8 (2d6 se l’arma improvvisata è impugnata a due mani). 

	L’arma improvvisata ha una minaccia di critico di 19–20, con moltiplicatore per il critico di ×2.

.. _tal_dita_sottili:
.. _tal_manolesta:

-------------
Manolesta
-------------
Eccezionali doti di destrezza manuale.

:Beneficio: 
	+2 alle prove di :ref:`Disattivare Congegni<disattivare_congegni>` e :ref:`Rapidità di Mano<rapidità_di_mano>` che aumenta a +4 se si hanno 10 gradi o più

.. admonition:: Note alle Edizioni

	In alcuni casi questo talento viene chiamato **Dita Sottili**.

.. _tal_manovre_agili:

------------------------------
Manovre Agili (Combattimento)
------------------------------
Capacità di usare la rapidità invece che la forza bruta nelle manovre in combattimento.

:Beneficio: 
	Si aggiunge il bonus di Destrezza al proprio BAB e al bonus taglia al fine di determinare il BMC invece che il bonus di Forza.

.. _tal_mastro_artigiano:

----------------------------
Mastro Artigiano
----------------------------
Capacità di creare semplici oggetti magici.

:Prerequisiti: 
	5 gradi in una qualsiasi abilità di :ref:`Artigianato<artigianato>` o :ref:`Professione<professione>`.
:Beneficio: 
	+2 ad un’abilità di :ref:`Artigianato<artigianato>` o :ref:`Professione<professione>` di propria scelta in cui si hanno almeno 5 gradi.

	I gradi nell’abilità scelta contano come il proprio livello d’incantatore ai fini di qualificarsi per i talenti :ref:`Creare Armi ed Armature Magiche<tal_creare_armi_e_armature_magiche>` e :ref:`Creare Oggetti Meravigliosi<tal_creare_oggetti_meravigliosi>`. 

	Occorre usare l’abilità scelta per la prova per creare l’oggetto. La CD per creare l’oggetto aumenta comunque per qualsiasi requisito di incantesimi necessario (vedi le regole sulla :ref:`Creazione degli Oggetti Magici<creazione_oggetti_magici>`). 

	Non si può usare questo talento per creare un oggetto ad attivazione di incantesimo o a completamento di incantesimo
:Normale: 
	Solo gli incantatori possono qualificarsi per i talenti :ref:`Creare Armi ed Armature Magiche<tal_creare_armi_e_armature_magiche>` e :ref:`Creare Oggetti Meravigliosi<tal_creare_oggetti_meravigliosi>`.

.. _tal_mira_letale:

------------------------------
Mira Letale (Combattimento)
------------------------------
Capacità di effettuare mortali attacchi a distanza, mirando ai punti deboli del nemico, a discapito della certezza che l’attacco vada a segno più facilmente.

:Prerequisiti: 
	Des 13, BAB +1.
:Beneficio: 
	Si può scegliere di subire penalità –1 a tutti i tiri colpire a distanza per guadagnare +2 a tutti i tiri per i danni con armi a distanza. 
	Quando il bonus di attacco base raggiunge +4, e successivamente ogni +4, la penalità aumenta di –1 ed il bonus di +2. 

	Bisogna scegliere di usare questo talento prima di effettuare il tiro per colpire ed i suoi effetti perdurano fino al vostro turno successivo. Il bonus al danno non si applica agli attacchi a contatto o agli effetti che non infliggono punti ferita di danno.

.. _tal_padronanza_degli_incantesimi:

----------------------------
Padronanza degli Incantesimi
----------------------------
Si conosce così bene un certo numero di incantesimi da non aver più bisogno del libro degli incantesimi per prepararli.

:Prerequisiti: 
	:ref:`Mago<mago>` Liv 1.
:Beneficio: 
	Ogni volta che si sceglie questo talento, si può scegliere un numero di incantesimi conosciuti pari al proprio Mod Int e da quel momento si possono preparare senza consultare il libro degli incantesimi.
:Normale: 
	Occorre consultare un libro degli incantesimi per preparare tutti gli incantesimi, con la sola eccezione di :ref:`Lettura del Magico<inc_lettura_del_magico>`.

.. _tal_passo_leggero:

-----------------
Passo Leggero
-----------------
Capacità di muoversi facilmente attraverso un ostacolo.

:Prerequisiti: 
	Des 13.
:Beneficio: 
	Ogni volta che ci si muove, si possono attraversare 1,5 metri di terreno difficile ogni round come se fosse terreno normale, è quindi possibile compiere un passo di 1,5 metri in un terreno difficile.

.. _tal_passo_acrobatico:

Passo Acrobatico
-----------------
Capacità di muoversi facilmente attraverso un ostacolo.

:Prerequisiti: 
	Des 15, :ref:`Passo Leggero<tal_passo_leggero>`.
:Beneficio: 
	Ogni volta che ci si muove, si possono attraversare 4,5 metri di terreno difficile ogni round come se fosse terreno normale, che si cumulano con quelli di :ref:`Passo Leggero<tal_passo_leggero>`.

.. _tal_persuasivo:

----------------
Persuasivo
----------------
Capacità di modificare l’atteggiamento e persuadere gli altri attraverso la mediazione o l’intimidazione.

:Beneficio: 
	+2 alle prove di :ref:`Diplomazia<diplomazia>` e :ref:`Intimidire<intimidire>` che aumenta a +4 se si hanno 10 gradi o più.

.. _tal_prodezza_intimidatrice:

--------------------------------------
Prodezza Intimidatrice (Combattimento)
--------------------------------------
La propria prestanza fisica intimorisce gli avversari.

:Beneficio: 
	Si aggiunge il Mod For alle prove di :ref:`Intimidire<intimidire>` oltre al proprio modificatore di Carisma.

.. _tal_ricarica_rapida:

------------------------------------
Ricarica Rapida (Combattimento)
------------------------------------
Si sceglie un tipo di balestra (a mano, leggera o pesante) da poter caricare più velocemente del normale.

:Prerequisiti: 
	:ref:`Competenza nelle Armi<tal_competenza_nelle_armi>` (tipo di balestra scelto).
:Beneficio: 
	Il tempo per caricare una balestra del tipo prescelto si riduce a un’:ref:`Azione Gratuita<azione_gratuita>` (per le balestre a mano o leggere) o a un’:ref:`Azione di Movimento<azione_movimento>` (per le balestre pesanti). 

	Si noti che caricare una balestra provoca comunque :ref:`Attacchi di Opportunità<azione_attacco_di_opportunità>`. Se si applica questo talento alle balestre a mano o leggere, in un’:ref:`Azione di Attacco Completo<azione_di_attacco_completo>`, si possono scagliare tanti quadrelli a round come se si attaccasse con un arco.
:Normale: 
	Occorre un’:ref:`Azione di Movimento<azione_movimento>` per caricare una balestra a mano o leggera, e un’:ref:`Azione di Round Completo<azione_di_attacco_completo>` per caricare una balestra pesante.
:Speciale: 
	Si può acquisire più volte applicandolo a diversi tipi di balestra.

.. _tal_resistenza_fisica:

------------------------
Resistenza Fisica
------------------------
Capacità di sopportare condizioni estreme e sforzi prolungati.

:Beneficio: 
	+4 a prove e TS vari: prove di :ref:`Nuotare<nuotare>` per evitare danni non letali, prove di Costituzione per non smettere di correre, prove di Costituzione per evitare danni non letali causati da marce forzate, prove di Costituzione per trattenere il fiato, prove di Costituzione per resistere alla fame o alla sete, TS su Tempra per evitare danni non letali causati da ambienti caldi o freddi e TS su Tempra per evitare danni causati da soffocamento. 

	É possibile inoltre dormire con indosso armature medie senza svegliarsi affaticati.
:Normale: 
	Dormire con indosso armature medie o più pesanti è affatica automaticamente il giorno seguente.

.. _tal_duro_a_morire:

Duro a Morire
--------------
Si è difficili da uccidere: non solo le ferite si stabilizzano automaticamente quando si è gravemente colpiti, ma si rimane coscienti e si può continuare ad agire anche se si sta per morire.

:Prerequisiti: 
	:ref:`Resistenza Fisica<tal_resistenza_fisica>`.
:Beneficio: 
	Se i punti ferita totali sono sotto lo 0, ma non si è morti, ci si stabilizza automaticamente, senza bisogno di effettuare la prova di Costituzione ad ogni round per evitare di perdere ulteriori punti ferita. 

	Si può continuare ad agire come se si fosse inabili, piuttosto che morenti, ma si deve prendere questa decisione nel momento in cui i punti ferita sono ridotti in negativo (anche se non è il proprio turno). 
	Se non si decide di agire come se si fosse inabili, si perdono subito i sensi.

	Quando si utilizza questo talento, si è barcollanti. Si può compiere un’:ref:`Azione di Movimento<azione_movimento>` senza subire alcun danno, ma se si esegue una qualsiasi :ref:`Azione Standard<azione_standard>` (o qualsiasi altra azione che sia stancante, comprese alcune azioni gratuite come lanciare incantesimi rapidi), si subisce 1 danno dopo averla completata. 

	Se i punti ferita negativi diventano minori o uguali al proprio punteggio di Costituzione, si muore sul colpo.
:Normale: 
	A punti ferita negativi si è considerati privi di sensi e morenti.

.. _tal_riflessi_fulminei:

-------------------------
Riflessi Fulminei
-------------------------
Si possiedono riflessi più veloci del normale.

:Beneficio: 
	+2 a tutti i TS su Riflessi.

.. _tal_riflessi_fulminei_migliorati:

Riflessi Fulminei Migliorati
----------------------------
Maggior intuizione per evitare i pericoli attorno a sè.

:Prerequisiti: 
	:ref:`Riflessi Fulminei<tal_riflessi_fulminei>`.

:Beneficio: 
	Una volta al giorno si può ritirare un TS su Riflessi, prima che i risultati siano rivelati, e si deve tenere il secondo tiro anche se peggiore.

.. _tal_riflessi_in_combattimento:

-----------------------------------------
Riflessi in Combattimento (Combattimento)
-----------------------------------------
Possibilità di effettuare :ref:`Attacchi di Opportunità<azione_attacco_di_opportunità>` aggiuntivi.

:Beneficio: 
	Si può compiere un numero di :ref:`Attacchi di Opportunità<azione_attacco_di_opportunità>` aggiuntivi per round pari al proprio bonus Destrezza e anche se si è colti impreparati.
:Normale: 
	Si ha a disposizione un solo :ref:`Attacco di Opportunità<azione_attacco_di_opportunità>` per round e non se ne possono effettuare se si è colti impreparati.
:Speciale: 
	Non consente di utilizzare :ref:`Opportunismo<opportunismo>` a un :ref:`Ladro<ladro>` più di una volta per round.

.. _tal_impedire_il_movimento:

Impedire il Movimento (Combattimento)
--------------------------------------
Si possono fermare i nemici che cercano di passarvi accanto.

:Prerequisiti: 
	:ref:`Riflessi in Combattimento<tal_riflessi_in_combattimento>`.
:Beneficio: 
	Quando un avversario provoca un :ref:`Attacco di Opportunità<azione_attacco_di_opportunità>` muovendosi attraverso quadretti minacciati, si può effettuare una prova di manovra di combattimento come proprio :ref:`Attacco di Opportunità<azione_attacco_di_opportunità>`. 

	Se riesce, l’avversario mantiene il resto della sua azione, ma non si può muovere. 

.. _tal_robustezza:

-----------------
Robustezza
-----------------
Si possiede una forma fisica invidiabile.

:Beneficio: 
	+3 punti ferita, +1 per ogni Dado Vita posseduto oltre il terzo. Questo valore va aggiornato ogni volta che si guadagna un Dado Vita.

.. _tal_scacciare_non_morti:

-------------------------
Scacciare Non Morti
-------------------------
Invocando poteri divini, si costringono i non morti alla fuga.

:Tipo: :ref:`Azione Standard<azione_standard>`
:Prerequisiti: 
	:ref:`Incanalare energia positiva<incanalare_energia_positiva>`.
:Beneficio: 
	Si può usare :ref:`Incanalare energia positiva<incanalare_energia_positiva>` per costringere alla fuga tutti in non morti entro 9 metri per 1 minuto, come se fossero in preda al panico.

	I non morti ricevono un TS su Volontà per negare questo effetto con **CD = 10 + 0.5 * Liv Chierico + Mod Car**. I non morti intelligenti ricevono un nuovo TS ogni round fino alla fine dell’effetto. 

	Se si utilizza :ref:`Incanalare Energia<incanalare_energia>` in questo modo, essa non ha altri effetti (non cura o ferisce le creature vicine).

.. _tal_schivare:

------------------------
Schivare (Combattimento)
------------------------
L’allenamento e i riflessi permettono di reagire velocemente per evitare gli attacchi degli avversari.

:Prerequisiti: 
	Des 13.
:Beneficio: 
	Bonus di schivare +1 alla CA. 

	Qualsiasi condizione che fa perdere il bonus di Destrezza alla CA, fa perdere anche il questo bonus.

.. _tal_mobilità:

Mobilità (Combattimento)
---------------------------
Capacità di muoversi attraverso gli avversari in mischia.

:Prerequisiti: 
	Des 13, :ref:`Schivare<tal_schivare>`.
:Beneficio: 
	Bonus di schivare +4 alla CA contro :ref:`Attacchi di Opportunità<azione_attacco_di_opportunità>` provocati dal muoversi dentro e fuori un’area minacciata.

	Qualsiasi condizione che fa perdere il bonus di Destrezza alla CA, fa inoltre perdere i bonus di schivare. Si noti che i bonus di schivare si cumulano, a differenza di altri tipi di bonus.

.. _tal_attacco_rapido:

*****************************************
Attacco Rapido (Combattimento)
*****************************************
Il personaggio è in grado di attaccare l’avversario e ritirarsi prima che quest’ultimo possa reagire.

:Prerequisiti: 
	Des 13, :ref:`Mobilità<tal_mobilità>`, :ref:`Schivare<tal_schivare>`, BAB +4.
:Beneficio: 
	Quando si compie un singolo attacco con un’arma da mischia, ci si può muovere sia prima sia dopo l’attacco, purché la distanza percorsa sia di almeno 3 metri prima dell’attacco e quella totale non superi quella consentita dalla propria velocità. 

	Non si può usare questo talento per attaccare un avversario adiacente a se stessi all’inizio del proprio turno.
:Normale: 
	Non ci si può muovere prima e dopo l’attacco.

.. _tal_posizione_velata:

Posizione Velata (Combattimento)
--------------------------------
Il proprio movimento rende difficile essere presi di mira dagli avversari.

:Prerequisiti: 
	Des 15, :ref:`Schivare<tal_schivare>`, BAB +6.
:Beneficio: 
	Se ci si muove più di 1,5 metri nel proprio turno, si ottiene il 20% di :ref:`Occultamento<occultamento>` per 1 round contro gli attacchi a distanza.

.. _tal_andatura_fulminea:

*****************************************
Andatura Fulminea (Combattimento)
*****************************************
La propria velocità vanifica gli attacchi degli avversari.

:Prerequisiti: 
	Des 17, :ref:`Posizione Velata<tal_posizione_velata>`, :ref:`Schivare<tal_schivare>`, BAB +11.
:Beneficio: 
	Se si compiono due :ref:`Azioni di Movimento<azione_movimento>` o un’azione di :ref:`Ritirata<azione_ritirata>` in un turno, si ottiene 50% di :ref:`Occultamento<occultamento>` per 1 round.

.. _tal_stoccata:

--------------------------
Stoccata (Combattimento)
--------------------------
Capacità di colpire i nemici fuori portata.

:Prerequisiti: 
	BAB +6.
:Beneficio: 
	Si può aumentare la portata dei propri attacchi in mischia di 1,5 metri fino alla fine del proprio turno subendo penalità –2 alla CA fino al turno successivo. 

	Si deve dichiarare di usare questa capacità prima di effettuare qualsiasi attacco.

.. _tal_tempra_possente:

---------------
Tempra Possente
---------------
Resistenza a veleni, malattie ed altre afflizioni.

:Beneficio: 
	+2 a tutti i TS su Tempra.

.. _tal_tempra_possente_migliorata:


Tempra Possente Migliorata
---------------------------
Capacità di attingere alle proprie riserve innate per resistere a malattie, veleni ed altre afflizioni.

:Prerequisiti: 
	:ref:`Tempra Possente<tal_tempra_possente>`.
:Beneficio: 
	Una volta al giorno si può ritirare un TS su Tempra, prima che i risultati siano rivelati, e si deve tenere il secondo tiro anche se peggiore.
	
.. _tal_tiro_ravvicinato:

--------------------------------
Tiro Ravvicinato (Combattimento)
--------------------------------
Capacità di effettuare tiri ben piazzati con armi a distanza anche contro bersagli ravvicinati.

:Beneficio: 
	+1 al tiro per colpire e ai danni con armi a distanza con gittata fino a 9 metri.

.. _tal_tirare_in_movimento:

Tirare in Movimento (Combattimento)
------------------------------------
Capacità di muoversi, attaccare a distanza, e poi muoversi ancora prima che l’avversario possa reagire.

:Tipo: :ref:`Azione di Round Completo<azione_di_round_completo>`
:Prerequisiti: 
	Des 13, :ref:`Mobilità<tal_mobilità>`, :ref:`Schivare<tal_schivare>`, :ref:`Tiro Ravvicinato<tal_tiro_ravvicinato>`, BAB +4.
:Beneficio: 
	Si può attaccare con un’arma a distanza e muoversi sia prima che dopo l’attacco, purché la distanza totale percorsa non superi quella consentita dalla propria velocità.
:Normale: 
	Non ci si può muovere prima e dopo un attacco a distanza.

.. _tal_tiro_lontano:

Tiro Lontano (Combattimento)
----------------------------
Maggior accuratezza nei tiri a lunga distanza.

:Prerequisiti: 
	:ref:`Tiro Ravvicinato<tal_tiro_ravvicinato>`.
:Beneficio: 
	–1 per ogni intero incremento di gittata tra sè ed il proprio bersaglio quando si usa un’arma a distanza.
:Normale: 
	–2 per ogni intero incremento di gittata tra sè ed il proprio bersaglio.

.. _tal_tiro_preciso:

Tiro Preciso (Combattimento)
----------------------------
Capacità di effettuare attacchi a distanza in mischia.

:Prerequisiti:
	:ref:`Tiro Ravvicinato<tal_tiro_ravvicinato>`.
:Beneficio: 
	Si può tirare o lanciare armi a distanza contro un avversario impegnato in mischia senza subire la normale penalità –4 al tiro per colpire.

.. _tal_tiro_preciso_migliorato:

***************************************
Tiro Preciso Migliorato (Combattimento)
***************************************
Gli attacchi a distanza possono ignorare gli effetti di :ref:`Copertura<copertura>` o di :ref:`Occultamento<occultamento>`.

:Prerequisiti: 
	Des 19, :ref:`Tiro Preciso<tal_tiro_preciso>`, :ref:`Tiro Ravvicinato<tal_tiro_ravvicinato>`, BAB +11.
:Beneficio: 
	Gli attacchi a distanza ignorano sia il bonus alla CA offerto da una qualsiasi :ref:`Copertura<copertura>` inferiore a quella totale sia la probabilità di essere mancati conferita da qualsiasi :ref:`Occultamento<occultamento>` inferiore a quello totale.

.. _tal_mira_inesorabile:

Mira Inesorabile (Combattimento)
********************************
Capacità di colpire i punti deboli dell’avversario.

:Tipo: :ref:`Azione Standard<azione_standard>`
:Prerequisiti: 
	Des 19, :ref:`Tiro Preciso<tal_tiro_preciso>`, :ref:`Tiro Preciso Migliorato<tal_tiro_preciso_migliorato>`, :ref:`Tiro Ravvicinato<tal_tiro_ravvicinato>`, BAB +16.
:Beneficio: 
	Un singolo attacco a distanza per cui il bersaglio non guadagna i bonus di armatura, armatura naturale o scudo alla CA. 

	Non si ottengono i benefici del talento se ci si muove durante il round.

.. _tal_tiro_rapido:

Tiro Rapido (Combattimento)
---------------------------
Si può effettuare un attacco a distanza addizionale.

:Prerequisiti: 
	Des 13, :ref:`Tiro Ravvicinato<tal_tiro_ravvicinato>`.
:Beneficio: 
	Quando si effettua un’:ref:`Azione di Attacco Completo<azione_di_attacco_completo>` con un’arma a distanza, si può compiere un attacco a distanza in più nel round, ma subendo –2 su tutti i tiri per colpire.

.. _tal_tiro_multiplo:

*****************************
Tiro Multiplo (Combattimento)
*****************************
Si possono scagliare più frecce insieme contro lo stesso avversario.

:Prerequisiti: 
	Des 17, :ref:`Tiro Rapido<tal_tiro_rapido>`, :ref:`Tiro Ravvicinato<tal_tiro_ravvicinato>`, BAB +6.
:Beneficio: 
	Quando si compie un’:ref:`Azione di Attacco Completo<azione_di_attacco_completo>`, con il primo attacco si possono scagliare due frecce contemporaneamente: se l’attacco va a segno, entrambe le frecce colpiscono il bersaglio. 

	Il danno di precisione e critico si applica solo una volta per questo attacco, mentre i bonus al danno per usare un arco composito con un alto bonus di Forza si applicano ad ogni freccia, così come gli altri bonus al danno, come quelli del :ref:`Nemico Prescelto<nemico_prescelto>`. 

	La riduzione del danno e le resistenze si applicano separatamente per ciascuna freccia.

.. _tal_vocazione_magica:

-----------------
Vocazione Magica
-----------------
Maggiore capacità nel lanciare incantesimi ed utilizzare oggetti magici.

:Beneficio: 
	+2 alle prove di :ref:`Sapienza Magica<sapienza_magica>` e :ref:`Utilizzare Congegni Magici<utilizzare_congegni_magici>` che aumenta a +4 se si hanno 10 gradi o più.

.. _tal_volontà_di_ferro:

----------------
Volontà di Ferro
----------------
Maggiore capacità di resistere agli effetti mentali.

:Beneficio: 
	+2 ai TS su Volontà.

.. _tal_volontà_di_ferro_migliorata:


Volontà di Ferro Migliorata
--------------------------------
Una mente lucida permette di resistere agli attacchi mentali.

:Prerequisiti: 
	:ref:`Volontà di Ferro<tal_volontà_di_ferro>`.
:Beneficio: 
	Una volta al giorno si può ritirare un TS su Volontà, prima che i risultati siano rivelati, e si deve tenere il secondo tiro anche se peggiore.
	
----------------------------
Creazione Oggetto
----------------------------
Per ciascuno di questi si vedano le regole relative alla :ref:`creazione degli oggetti magici<creazione_oggetti_magici>` per maggiori informazioni.

.. _tal_creare_armi_e_armature_magiche:

Creare Armi e Armature Magiche
-------------------------------
Capacità di creare armi, armature e scudi magici.

:Prerequisiti: 
	LI 5.
:Beneficio: 
	Si può creare qualsiasi arma, armatura o scudo magico. 

	Il potenziamento di un’arma, di un’armatura completa o di uno scudo richiede un giorno per ogni 1.000 mo del :ref:`costo delle rispettive caratteristiche magiche<creazione_oggetti_magici>`. Per potenziare un’arma, un’armatura o uno scudo si devono consumare materie prime per un costo pari alla metà del prezzo totale. 
	L’arma, l’armatura o lo scudo da potenziare devono essere oggetti perfetti, forniti dal personaggio. Il costo dell’oggetto perfetto non è incluso nel costo descritto sopra.

	È possibile riparare un’arma, un’armatura completa o uno scudo magico rotto, purché si tratti di uno di quelli che si è in grado di creare. In questo modo la riparazione costa metà delle materie prime e del tempo che altrimenti servirebbero a creare l’oggetto.

.. _tal_creare_bacchette:

Creare Bacchette
-----------------------
Capacità di creare bacchette magiche.

:Prerequisiti: LI 5.
:Beneficio: 
	Si può creare una bacchetta di qualsiasi incantesimo di 4° livello o inferiore che si conosce. 

	La creazione di una bacchetta richiede un giorno per ogni 1.000 mo del suo prezzo base e materie prime per un costo pari alla metà del prezzo base. Una bacchetta di nuova creazione possiede 50 cariche. 

.. _tal_creare_bastoni:

Creare Bastoni
-----------------------
Capacità di creare bastoni magici.

:Prerequisiti: 
	LI 11.
:Beneficio: 
	Si può creare qualsiasi bastone magico. 

	La creazione di un bastone richiede un giorno per ogni 1.000 mo del suo prezzo base e materie prime per un costo pari alla metà del prezzo base. 

	Un bastone di nuova creazione contiene 10 cariche. 

.. _tal_creare_oggetti_meravigliosi:

Creare Oggetti Meravigliosi
---------------------------
Capacità di creare oggetti meravigliosi, un tipo di oggetti magici.

:Prerequisiti: 
	LI 3.
:Beneficio: 
	Si può creare qualsiasi tipo di oggetto meraviglioso. 

	Creare un oggetto meraviglioso richiede 1 giorno per ogni 1.000 mo del suo prezzo e materie prime per un costo pari alla metà del prezzo base. 

.. _tal_creare_verghe:

Creare Verghe
-----------------------
Capacità di creare verghe magiche.

:Prerequisiti: 
	LI 9.
:Beneficio: 
	Si può creare qualsiasi verga magica. 

	La creazione di una verga richiede un giorno per ogni 1.000 mo del suo prezzo base e materie prime per un costo pari alla metà del prezzo base.

.. _tal_forgiare_anelli:

Forgiare Anelli
-----------------------
Capacità di creare anelli magici.

:Prerequisiti: 
	LI 7.
:Beneficio: 
	Si è in grado di creare qualsiasi anello magico. 

	La forgiatura di un anello richiede 1 giorno per ogni 1.000 mo del suo prezzo base e materie prime per un costo pari alla metà del suo prezzo base. 

	È inoltre possibile riparare un anello rotto, purché  si tratti di uno di quelli che si è in grado di forgiare. In questo modo la riparazione costa metà delle materie prime e del tempo che altrimenti servirebbero per forgiare quell’anello la prima volta.

.. _tal_mescere_pozioni:

Mescere Pozioni
-----------------------
Capacità di creare pozioni magiche.

:Prerequisiti: 
	LI 3.
:Beneficio: 
	Si può creare una pozione di un qualsiasi incantesimo conosciuto di 3° livello o inferiore, che abbia come bersaglio una o più creature. 

	La mescita richiede 2 ore se il suo prezzo base è 250 mo o meno, altrimenti 1 giorno per ogni 1.000 mo nel suo prezzo base. 

	Quando si crea una pozione, si stabilisce il LI, che deve essere sufficiente per lanciare l’incantesimo in questione e non superiore al proprio livello. Per mescere una pozione, bisogna consumare materiali grezzi per un costo pari alla metà del prezzo base. 

	Quando si crea una pozione, si fanno tutte le scelte che si farebbero normalmente quando si lancia l’incantesimo e chi beve la pozione è il bersaglio dell’incantesimo.


.. _tal_scrivere_pergamene:

Scrivere Pergamene
-----------------------
Capacità di creare pergamene magiche.

:Prerequisiti: 
	LI 1.
:Beneficio: 
	È possibile creare una pergamena di qualsiasi incantesimo che si conosce. 

	La scrittura richiede 2 ore se il suo prezzo base è 250 mo o meno, altrimenti 1 giorno ogni 1.000 mo nel suo prezzo base. 

	Per scrivere una pergamena il personaggio deve consumare materie prime per un costo pari alla metà del prezzo base.

.. _tal_metamagia:

----------------------------
Metamagia
----------------------------

.. _tal_incantesimi_ampliati:

Incantesimi Ampliati
----------------------
I propri incantesimi occupano un’area maggiore.

:Beneficio: 
	Si modifica la linea, l’emanazione, l’esplosione o la propagazione di un incantesimo per aumentarne l’area influenzata. Qualsiasi estensione numerica dell’area dell’incantesimo, aumenta del 100%.

	Un incantesimo ampliato occupa uno slot di **tre livelli superiore** a quello effettivo.

	Gli incantesimi la cui area non rientra in alcuna delle quattro categorie sopra riportate non sono influenzati da questo talento.

.. _tal_incantesimi_estesi:

Incantesimi Estesi
-----------------------
Capacità di far durare gli incantesimi più a lungo.

:Beneficio: 
	Un incantesimo **dura il doppio** del normale occupando uno slot di un livello superiore a quello effettivo. 

	Gli incantesimi la cui durata è la concentrazione, istantanea o permanente non sono influenzati da questo talento. 


.. _tal_incantesimi_immobili:

Incantesimi Immobili
-----------------------
Capacità di lanciare incantesimi senza compiere gesti.

:Beneficio: 
	Si può lanciare un incantesimo senza le componenti somatiche occupando uno slot di un livello superiore a quello effettivo. 

	Gli incantesimi privi di componenti somatiche non sono influenzati. 

.. _tal_incantesimi_ingranditi:

Incantesimi Ingranditi
---------------------------
Capacità di aumentare il raggio d’azione dei propri incantesimi.

:Beneficio: 
	È possibile modificare un incantesimo con raggio di azione vicino, medio o lungo per ingrandirlo del 100%. 

	Un incantesimo ingrandito con raggio di azione vicino viene quindi ad avere un raggio di azione di 15 m + 1,5 m per livello, quelli medi ottengono un raggio di azione di 60 m + 6 m per livello e quelli lunghi arrivano a un raggio di azione di 240 m + 24 m per livello.

	Un incantesimo ingrandito occupa uno slot di un livello superiore a quello effettivo.

	Gli incantesimi i cui raggi di azione non sono definiti dalla distanza, e allo stesso modo gli incantesimi i cui raggi di azione non sono vicino, medio o lontano, non possono essere ingranditi con questo talento.

.. _tal_incantesimi_intensificati:

Incantesimi Intensificati
-------------------------
Capacità di lanciare un incantesimo come se fosse di un livello superiore a quello effettivo.

:Beneficio: 
	Un incantesimo ha un livello di incantesimo più alto del normale (fino a un massimo del 9° livello) sia per quanto riguarda gli effetti che gli slot occupati. 

	A differenza di altri talenti di metamagia, :ref:`Incantesimi Intensificati<tal_incantesimi_intensificati>` aumenta a tutti gli effetti il vero e proprio livello dell’incantesimo su cui viene applicato.


.. _tal_incantesimi_massimizzati:

Incantesimi Massimizzati
------------------------
Gli incantesimi hanno il massimo effetto possibile.

:Beneficio: 
	Tutti gli effetti numerici variabili sono elevati alla massima potenza, occupando uno slot di tre livelli superiore a quello effettivo. 

	Non ha alcun effetto sui TS e tiri contrapposti, né sugli incantesimi privi di variabili casuali. 

	Un incantesimo sia potenziato che massimizzato, ottiene i benefici separati di ciascun talento: il risultato massimo più la metà del risultato che normalmente si otterrebbe.

.. _tal_incantesimi_potenziati:

Incantesimi Potenziati
-----------------------
Capacità di aumentare il potere degli incantesimi, in modo che infliggano maggior danno.

:Beneficio: 
	Tutti gli effetti numerici variabili di un incantesimo potenziato aumentano della metà, occupando uno slot di due livelli superiore a quello effettivo. 

	Non ha alcun effetto sui TS e tiri contrapposti, né sugli incantesimi privi di variabili casuali. 

.. _tal_incantesimi_rapidi:

Incantesimi Rapidi
----------------------
Capacità di lanciare incantesimi in frazioni del tempo normale.

:Beneficio: 
	Il lancio è un’:ref:`Azione Veloce<azione_veloce>`, a meno che il tempo di lancio sia più lungo di un':ref:`Azione di Round Completo<azione_di_round_completo>`, che non sarebbe velocizzabile.

	Si può compiere un’altra azione, persino lanciare un altro incantesimo, nello stesso round in cui si lancia un incantesimo rapido.

	Un incantesimo rapido occupa uno slot di quattro livelli superiore a quello effettivo e il lancio non provoca alcun :ref:`Attacco di Opportunità<azione_attacco_di_opportunità>`.
:Speciale: 
	Questo talento può essere applicato agli incantesimi lanciati spontaneamente, purché abbiano un tempo di lancio non superiore a un':ref:`Azione di Round Completo<azione_di_round_completo>`, senza aumentare il tempo di lancio dell’incantesimo.

.. _tal_incantesimi_silenziosi:

Incantesimi Silenziosi
----------------------
Capacità di lanciare incantesimi silenziosamente

:Beneficio: 
	Un incantesimo può essere lanciato senza le componenti verbali, occupando uno slot di un livello superiore a quello effettivo. 

	Gli incantesimi privi di componenti verbali non sono influenzati.
:Speciale: 
	Gli incantesimi da :ref:`Bardo<bardo>` non possono essere potenziati da questo talento.

.. _tal_ordine_alfabetico:

Ordine Alfabetico
====================

#.	:ref:`Abilità Focalizzata<tal_abilità_focalizzata>`
#.	:ref:`Acrobatico<tal_acrobatico>`
#.	:ref:`Addestramento Arcano nelle Armature (Combattimento)<tal_addestramento_arcano_nelle_armature>`
#.	:ref:`Addestramento nel Combattimento Difensivo (Combattimento)<tal_addestramento_nel_combattimento_difensivo>`
#.	:ref:`Afferrare Frecce (Combattimento<tal_afferrare_frecce>`
#.	:ref:`Affinità Animale<tal_affinità_animale>`
#.	:ref:`Agile<tal_agile>`
#.	:ref:`Allerta<tal_allerta>`
#.	:ref:`Andatura Fulminea (Combattimento)<tal_andatura_fulminea>`
#.	:ref:`Arma Accurata (Combattimento)<tal_arma_accurata>`
#.	:ref:`Arma Focalizzata (Combattimento)<tal_arma_focalizzata>`
#.	:ref:`Arma Focalizzata Superiore (Combattimento)<tal_arma_focalizzata_superiore>`
#.	:ref:`Arma Specializzata (Combattimento)<tal_arma_specializzata>`
#.	:ref:`Arma Specializzata Superiore (Combattimento)<tal_arma_specializzata_superiore>`
#.	:ref:`Atletico<tal_atletico>`
#.	:ref:`Attacco con lo Scudo Migliorato (Combattimento)<tal_attacco_con_lo_scudo_migliorato>`
#.	:ref:`Attacco in Sella (Combattimento)<tal_attacco_in_sella>`
#.	:ref:`Attacco Lacerante a Due Armi (Combattimento)<tal_attacco_lacerante_a_due_armi>`
#.	:ref:`Attacco Poderoso (Combattimento)<tal_attacco_poderoso>`
#.	:ref:`Attacco Rapido (Combattimento)<tal_attacco_rapido>`
#.	:ref:`Attacco Turbinante (Combattimento)<tal_attacco_turbinante>`
#.	:ref:`Aumentare Evocazione<tal_aumentare_evocazione>`
#.	:ref:`tal_autorità`
#.	:ref:`Autosufficiente<tal_autosufficiente>`
#.	:ref:`Botta di Scudo (Combattimento)<tal_botta_di_scudo>`
#.	:ref:`Carica Devastante (Combattimento)<tal_carica_devastante>`
#.	:ref:`Cogliere di Sorpresa (Combattimento)<tal_cogliere_di_sorpresa>`
#.	:ref:`Collera della Medusa (Combattimento)<tal_collera_della_medusa>`
#.	:ref:`Colpo Arcano (Combattimento)<tal_colpo_arcano>`
#.	:ref:`Colpo di Ritorno (Combattimento)<tal_colpo_di_ritorno>`
#.	:ref:`Colpo Mortale (Combattimento)<tal_colpo_mortale>`
#.	:ref:`Colpo Penetrante (Combattimento)<tal_colpo_penetrante>`
#.	:ref:`Colpo Penetrante Superiore (Combattimento)<tal_colpo_penetrante_superiore>`
#.	:ref:`Colpo Senz'Armi Migliorato (Combattimento)<tal_colpo_senzarmi_migliorato>`
#.	:ref:`Colpo Vitale (Combattimento)<tal_colpo_vitale>`
#.	:ref:`Colpo Vitale Migliorato (Combattimento)<tal_colpo_vitale_migliorato>`
#.	:ref:`Colpo Vitale Superiore (Combattimento)<tal_colpo_vitale_superiore>`
#.	:ref:`Comandare Non Morti<tal_comandare_non_morti>`
#.	:ref:`Combattere alla Cieca (Combattimento)<tal_combattere_alla_cieca>`
#.	:ref:`Combattere con Due Armi (Combattimento)<tal_combattere_con_due_armi>`
#.	:ref:`Combattere con Due Armi Migliorato (Combattimento)<tal_combattere_con_due_armi_migliorato>`
#.	:ref:`Combattere con Due Armi Superiore (Combattimento)<tal_combattere_con_due_armi_superiore>`
#.	:ref:`Combattere in Sella (Combattimento)<tal_combattere_in_sella>`
#.	:ref:`Competenza negli Scudi (Combattimento)<tal_competenza_negli_scudi>`
#.	:ref:`Competenza negli Scudi Torre (Combattimento)<tal_competenza_negli_scudi_torre>`
#.	:ref:`Competenza nelle Armature Leggere (Combattimento)<tal_competenza_nelle_armature_leggere>`
#.	:ref:`Competenza nelle Armature Medie (Combattimento)<tal_competenza_nelle_armature_medie>`
#.	:ref:`Competenza nelle Armature Pesanti (Combattimento)<tal_competenza_nelle_armature_pesanti>`
#.	:ref:`Competenza nelle Armi da Guerra (Combattimento)<tal_competenza_nelle_armi_da_guerra>`
#.	:ref:`Competenza nelle Armi Esotiche (Combattimento)<tal_competenza_nelle_armi_esotiche>`
#.	:ref:`Competenza nelle Armi Semplici (Combattimento)<tal_competenza_nelle_armi_semplici>`
#.	:ref:`Controincantesimo Migliorato<tal_controincantesimo_migliorato>`
#.	:ref:`Correre<tal_correre>`
#.	:ref:`Creare Armi e Armature Magiche (Creazione Oggetto)<tal_creare_armi_e_armature_magiche>`
#.	:ref:`Creare Bacchette (Creazione Oggetto)<tal_creare_bacchette>`
#.	:ref:`Creare Bastoni (Creazione Oggetto)<tal_creare_bastoni>`
#.	:ref:`Creare Oggetti Meravigliosi (Creazione Oggetto)<tal_creare_oggetti_meravigliosi>`
#.	:ref:`Creare Verghe (Creazione Oggetto)<tal_creare_verghe>`
#.	:ref:`Critico Accecante (Combattimento, Critico)<tal_critico_accecante>`
#.	:ref:`Critico Affaticante (Combattimento, Critico)<tal_critico_affaticante>`
#.	:ref:`Critico Assordante (Combattimento, Critico)<tal_critico_assordante>`
#.	:ref:`Critico Debilitante (Combattimento, Critico)<tal_critico_debilitante>`
#.	:ref:`Critico Focalizzato (Combattimento)<tal_critico_focalizzato>`
#.	:ref:`Critico Incapacitante (Combattimento, Critico)<tal_critico_incapacitante>`
#.	:ref:`Critico Inesorabile (Combattimento, Critico)<tal_critico_inesorabile>`
#.	:ref:`Critico Migliorato (Combattimento)<tal_critico_migliorato>`
#.	:ref:`Critico Prodigioso (Combattimento)<tal_critico_prodigioso>`
#.	:ref:`Critico Sanguinante (Combattimento, Critico)<tal_critico_sanguinante>`
#.	:ref:`Critico Stordente (Combattimento, Critico)<tal_critico_stordente>`
#.	:ref:`Deviare Frecce (Combattimento)<tal_deviare_frecce>`
#.	:ref:`Difendere con Due Armi (Combattimento)<tal_difendere_con_due_armi>`
#.	:ref:`Dirompente (Combattimento)<tal_dirompente>`
#.	:ref:`Disarcionare (Combattimento)<tal_disarcionare>`
#.	:ref:`Disarmare Migliorato (Combattimento)<tal_disarmare_migliorato>`
#.	:ref:`Disarmare Superiore (Combattimento)<tal_disarmare_superiore>`
#.	:ref:`Doppio Taglio (Combattimento)<tal_doppio_taglio>`
#.	:ref:`Duro a Morire<tal_duro_a_morire>`
#.	:ref:`Escludere Materiali<tal_escludere_materiali>`
#.	:ref:`Esibizione Bardica Extra<tal_esibizione_bardica_extra>`
#.	:ref:`Estrazione Rapida (Combattimento)<tal_estrazione_rapida>`
#.	:ref:`Famiglio Migliorato<tal_famiglio_migliorato>`
#.	:ref:`Fintare Migliorato (Combattimento)<tal_fintare_migliorato>`
#.	:ref:`Fintare Superiore (Combattimento)<tal_fintare_superiore>`
#.	:ref:`Forgiare Anelli (Creazione Oggetto)<tal_forgiare_anelli>`
#.	:ref:`Furtivo<tal_furtivo>`
#.	:ref:`Impedire il Movimento (Combattimento)<tal_impedire_il_movimento>`
#.	:ref:`Imposizione delle Mani Extra<tal_imposizione_delle_mani_extra>`
#.	:ref:`Incalzare (Combattimento)<tal_incalzare>`
#.	:ref:`Incalzare Potenziato (Combattimento)<tal_incalzare_potenziato>`
#.	:ref:`Incanalare Allineamento<tal_incanalare_allineamento>`
#.	:ref:`Incanalare Elementale<tal_incanalare_elementale>`
#.	:ref:`Incanalare Extra<tal_incanalare_extra>`
#.	:ref:`Incanalare Migliorato<tal_incanalare_migliorato>`
#.	:ref:`Incanalare Punizione (Combattimento)<tal_incanalare_punizione>`
#.	:ref:`Incanalare Selettivo<tal_incanalare_selettivo>`
#.	:ref:`Incantare in Combattimento<tal_incantare_in_combattimento>`
#.	:ref:`Incantesimi Ampliati (Metamagia)<tal_incantesimi_ampliati>`
#.	:ref:`Incantesimi Estesi (Metamagia)<tal_incantesimi_estesi>`
#.	:ref:`Incantesimi Focalizzati<tal_incantesimi_focalizzati>`
#.	:ref:`Incantesimi Focalizzati Superiore<tal_incantesimi_focalizzati_superiore>`
#.	:ref:`Incantesimi Immobili(Metamagia)<tal_incantesimi_immobili>`
#.	:ref:`Incantesimi Inarrestabili<tal_incantesimi_inarrestabili>`
#.	:ref:`Incantesimi Inarrestabili Superiore<tal_incantesimi_inarrestabili_superiore>`
#.	:ref:`Incantesimi Ingranditi (Metamagia)<tal_incantesimi_ingranditi>`
#.	:ref:`Incantesimi Intensificati (Metamagia)<tal_incantesimi_intensificati>`
#.	:ref:`Incantesimi Massimizzati (Metamagia)<tal_incantesimi_massimizzati>`
#.	:ref:`Incantesimi Naturali<tal_incantesimi_naturali>`
#.	:ref:`Incantesimi Potenziati (Metamagia)<tal_incantesimi_potenziati>`
#.	:ref:`Incantesimi Rapidi (Metamagia)<tal_incantesimi_rapidi>`
#.	:ref:`Incantesimi Silenziosi (Metamagia)<tal_incantesimi_silenziosi>`
#.	:ref:`Indulgenza Extra<tal_indulgenza_extra>`
#.	:ref:`Ingannevole<tal_ingannevole>`
#.	:ref:`Iniziativa Migliorata (Combattimento)<tal_iniziativa_migliorata>`
#.	:ref:`Inseguire (Combattimento)<tal_inseguire>`
#.	:ref:`Ira Extra<tal_ira_extra>`
#.	:ref:`Ki Extra<tal_ki_extra>`
#.	:ref:`Lanciare Oggetti (Combattimento)<tal_lanciare_oggetti>`
#.	:ref:`Lottare Migliorato (Combattimento)<tal_lottare_migliorato>`
#.	:ref:`Lottare Superiore (Combattimento)<tal_lottare_superiore>`
#.	:ref:`Maestria Arcana nelle Armature (Combattimento)<tal_maestria_arcana_nelle_armature>`
#.	:ref:`Maestria Intimorente (Combattimento)<tal_maestria_intimorente>`
#.	:ref:`Maestria in Combattimento (Combattimento)<tal_maestria_in_combattimento>`
#.	:ref:`Maestria negli Scudi (Combattimento)<tal_maestria_negli_scudi>`
#.	:ref:`Maestria nelle Armi Improvvisate (Combattimento)<tal_maestria_nelle_armi_improvvisate>`
#.	:ref:`Manolesta<tal_manolesta>`
#.	:ref:`Manovre Agili (Combattimento)<tal_manovre_agili>`
#.	:ref:`Mastro Artigiano<tal_mastro_artigiano>`
#.	:ref:`Mescere Pozioni (Creazione Oggetto)<tal_mescere_pozioni>`
#.	:ref:`Mira Inesorabile (Combattimento)<tal_mira_inesorabile>`
#.	:ref:`Mira Letale (Combattimento)<tal_mira_letale>`
#.	:ref:`Mobilità (Combattimento)<tal_mobilità>`
#.	:ref:`Oltrepassare Migliorato (Combattimento)<tal_oltrepassare_migliorato>`
#.	:ref:`Oltrepassare Superiore (Combattimento)<tal_oltrepassare_superiore>`
#.	:ref:`Padronanza degli Incantesimi<tal_padronanza_degli_incantesimi>`
#.	:ref:`Passo Acrobatico<tal_passo_acrobatico>`
#.	:ref:`Passo Leggero<tal_passo_leggero>`
#.	:ref:`Persuasivo<tal_persuasivo>`
#.	:ref:`Posizione Velata (Combattimento)<tal_posizione_velata>`
#.	:ref:`Prodezza Intimidatrice (Combattimento)<tal_prodezza_intimidatrice>`
#.	:ref:`Pugno della Gorgone (Combattimento)<tal_pugno_della_gorgone>`
#.	:ref:`Pugno Stordente (Combattimento)<tal_pugno_stordente>`
#.	:ref:`Resistenza Fisica<tal_resistenza_fisica>`
#.	:ref:`Ricarica Rapida (Combattimento)<tal_ricarica_rapida>`
#.	:ref:`Riflessi Fulminei<tal_riflessi_fulminei>`
#.	:ref:`Riflessi Fulminei Migliorati<tal_riflessi_fulminei_migliorati>`
#.	:ref:`Riflessi in Combattimento (Combattimento)<tal_riflessi_in_combattimento>`
#.	:ref:`Robustezza<tal_robustezza>`
#.	:ref:`Sbilanciare Migliorato (Combattimento)<tal_sbilanciare_migliorato>`
#.	:ref:`Sbilanciare Superiore (Combattimento)<tal_sbilanciare_superiore>`
#.	:ref:`Scacciare Non Morti<tal_scacciare_non_morti>`
#.	:ref:`Schivare (Combattimento)<tal_schivare>`
#.	:ref:`Scrivere Pergamene (Creazione Oggetto)<tal_scrivere_pergamene>`
#.	:ref:`Scudo Focalizzato (Combattimento)<tal_scudo_focalizzato>`
#.	:ref:`Scudo Focalizzato Superiore (Combattimento)<tal_scudo_focalizzato_superiore>`
#.	:ref:`Spezzaincantesimi (Combattimento)<tal_spezzaincantesimi>`
#.	:ref:`Spezzare le Difese (Combattimento)<tal_spezzare_le_difese>`
#.	:ref:`Spezzare Migliorato (Combattimento)<tal_spezzare_migliorato>`
#.	:ref:`Spezzare Superiore (Combattimento)<tal_spezzare_superiore>`
#.	:ref:`Spingere Migliorato (Combattimento)<tal_spingere_migliorato>`
#.	:ref:`Spingere Superiore (Combattimento)<tal_spingere_superiore>`
#.	:ref:`Stile dello Scorpione (Combattimento)<tal_stile_dello_scorpione>`
#.	:ref:`Stoccata (Combattimento)<tal_stoccata>`
#.	:ref:`Tempra Possente<tal_tempra_possente>`
#.	:ref:`Tempra Possente Migliorata<tal_tempra_possente_migliorata>`
#.	:ref:`Tirare in Movimento (Combattimento)<tal_tirare_in_movimento>`
#.	:ref:`Tirare in Sella (Combattimento)<tal_tirare_in_sella>`
#.	:ref:`Tiro Lontano (Combattimento)<tal_tiro_lontano>`
#.	:ref:`Tiro Multiplo (Combattimento)<tal_tiro_multiplo>`
#.	:ref:`Tiro Preciso (Combattimento)<tal_tiro_preciso>`
#.	:ref:`Tiro Preciso Migliorato (Combattimento)<tal_tiro_preciso_migliorato>`
#.	:ref:`Tiro Rapido (Combattimento)<tal_tiro_rapido>`
#.	:ref:`Tiro Ravvicinato (Combattimento)<tal_tiro_ravvicinato>`
#.	:ref:`Travolgere (Combattimento)<tal_travolgere>`
#.	:ref:`Vocazione Magica<tal_vocazione_magica>`
#.	:ref:`Volontà di Ferro<tal_volontà_di_ferro>`
#.	:ref:`Volontà di Ferro Migliorata<tal_volontà_di_ferro_migliorata>`
