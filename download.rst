
.. _download:

=================================================
Downloads
=================================================

Un elenco di file che possono tornare utili. Copyright non pervenuti...

Schede
=================

.. _scheda_personaggio:

:download:`Scheda Personaggio PathFinder<_static/Scheda Personaggio.pdf>`

.. _player_booklet:

:download:`Player's Booklet<_static/Player's Booklet.pdf>`
