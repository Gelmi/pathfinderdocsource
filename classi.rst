
.. _classi:

=================================================
Classi
=================================================

.. contents:: Contenuti
    :local:
    :depth: 3

Fondamenti
=====================

La classe costituisce la fonte della maggiorparte delle capacità dei personaggi, e gli fornisce un ruolo specifico all’interno di un gruppo di avventurieri. Le classi seguenti rappresentano le quelle base.

:Barbaro: 

    Un combattente brutale alimentato da un’ira primeva.

:Bardo: 

    Una guida arcana che ispira e potenzia gli alleati e confonde i nemici.

:Chierico: 

    Un fedele seguace di una divinità, che può curare, rianimare i morti e invocare l’ira divina.

:Druido:

    Un adoratore della natura e della magia primordiale, capace di assumere forme diverse.

:Guerriero: 

    Maestro coraggioso dell’arte bellica e delle tecniche di combattimento.

:Ladro: 

    Astuto esperto dell’arte del sotterfugio per colpire i nemici ed aiutare gli alleati in molte occasioni.

:Mago: 

    Esperto studioso di magia con incredibili poteri arcani.

:Monaco: 

    Studente delle arti marziali, allena il suo corpo ad essere arma e difesa contro i nemici.

:Paladino: 

    Seguace devoto della legge e della giustizia.

:Ranger: 

    Cacciatore e guida, il ranger è un abile combattente nei boschi e nell’inseguire i suoi nemici.

:Stregone:

    Incantatore dalle doti magiche e dai poteri arcani innati.

.. _avanzamento:

----------------------------
Avanzamento dei personaggi
----------------------------

L’esperienza fa guadagnare ai personaggi dei punti esperienza. Una volta accumulati i personaggi avanzano di livello e potere. 

La velocità di avanzamento dipende dal tipo di gioco che voi ed il GM preferite: alcuni preferiscono un gioco più dinamico, dove si avanza di livello dopo qualche sessione di gioco, mentre altri preferiscono un gioco dove l’avanzamento è più lento.
L’avanzamento degli eroi è mostrato in tabella.


.. figure:: Immagini/Avanzamento.png
    :align: right
    :width: 300

.. _salire_di_livello:

Salire di livello
--------------------------------

I personaggi avanzano di livello quando hanno accumulato punti esperienza sufficienti, dati dal GM. Il processo di avanzamento del personaggio *funziona più o meno allo stesso modo della sua creazione*, salvo che i punteggi di caratteristica, la razza e le scelte precedenti riguardo a classe, abilità e talenti non possono essere cambiati. 

Aggiungere un livello generalmente conferisce nuove capacità, gradi d’abilità addizionali da spendere, più punti ferita e possibilmente un aumento del punteggio di una caratteristica o un talento aggiuntivo.

Quando si aggiungono nuovi livelli ad una classe esistente o si aggiungono livelli di una nuova classe (vedi :ref:`Multiclasse<multiclasse>`, sotto), bisogna assicurarsi di seguire le azioni indicate nell’ordine:

#.	Scegliete il vostro nuovo livello di classe. 

    Occorre qualificarsi per questo livello prima di procedere oltre con le modifiche successive.

    Fai attenzione nel caso tu scelga la tua :ref:`classe preferita<classe_preferita>`.

#.	Aumentare un punteggio di caratteristica qualsiasi dato dall’avanzamento di livello (se previsto). 

#.	Integrare tutti i nuovi privilegi di classe dati dal livello. Per esempio un incremento al Bonus di Attacco Base, i Tiri Salvezza.

#.	Tirare per i punti ferita addizionali. Ricorda di aggiungere il Modificatore Costituzione o altri eventuali bonus, come il punto ferita extra se si sale di livello nella :ref:`Classe Preferita<classe_preferita>` (a meno che si sia scelto il punto abilità extra).

#.	Aggiungere abilità e talenti (se previsto).



.. _multiclasse:

Multiclasse
-----------------

Invece di guadagnare le capacità concesse dal livello successivo nella classe attuale del personaggio, si possono guadagnare le capacità di 1° livello di una nuova classe, aggiungendo
tutte queste nuove capacità a quelle esistenti, cioè *multiclassare*.

Per esempio, un guerriero di 5° livello decide di cimentarsi nelle arti arcane ed aggiunge un livello da mago quando avanza al 6° livello. Questo personaggio avrà i poteri e le capacità sia di un guerriero di 5° livello che di un mago di 1° livello, ma è considerato un personaggio di 6° livello. Mantiene tutti i suoi talenti bonus guadagnati dai 5 livelli da guerriero, ma ora può anche lanciare incantesimi di 1° livello e scegliere una scuola di magia. Aggiunge tutti i punti ferita, ibonus di attacco base e i bonus ai tiri salvezza come mago di 1° livello a quelli ottenuti dall’essere un guerriero di 5° livello.

Si noti che ci sono un certo numero di effetti e di prerequisiti che si basano sul livello del personaggio e sui suoi Dadi Vita.

Tali effetti sono basati sempre sul numero totale dei livelli o Dadi Vita che un personaggio possiede, non su quelli di una classe, a eccezzione dei privilegi di classe.

.. _classe_preferita:

Classe preferita
-----------------

Ogni personaggio inizia il gioco con una classe (a eccezione del mezzelfo, che ne ha due) preferita a scelta, solitamente quella scelta al 1° livello. La scelta non può essere cambiata.

Ogni volta che avanza di livello nella sua classe preferita, un personaggio riceve o 1 punto ferita o 1 grado di abilità aggiuntivo, a sua scelta. La scelta non può essere cambiata.

Le classi di prestigio (vedi :ref:`Classi di Prestigio<classi_di_prestigio>`) non possono mai essere classi preferite.

.. _barbaro:

Barbaro
==================================

Per qualcuno, c’è soltanto l’ira. Il conflitto è tutto quello che queste anime selvagge conoscono. Sono creature del massacro, spiriti guerrieri posseduti dalla
battaglia. Sono dotati di un sesto senso che li rende più reattivi al pericolo e di una innata resistenza alle sue conseguenze.

Nei barbari infuria lo spirito primevo della battaglia: guai a chi affronta la loro ira.

:Ruolo: 

    I barbari eccellono nel combattimento. L’ira che concede loro quell’audacia necessaria per osare di più degli altri combattenti. I barbari caricano furiosi in battaglia e distruggono tutto ciò che incontrano sulla loro strada.

.. figure:: Immagini/Barbaro.png
    :align: right
    :width: 400

:Allineamento: Qualsiasi non legale
:Dado Vita: d12

----------------------------
Abilità di classe
----------------------------

:Crescita per livello: 4 + modificatore Intelligenza

*	:ref:`Acrobazia (Des)<acrobazia>`
*	:ref:`Addestrare Animali (Car)<addestrare_animali>`
*	:ref:`Artigianato (Int)<artigianato>`
*	:ref:`Cavalcare (Des)<cavalcare>`
*	:ref:`Conoscenze (natura) (Int)<conoscenze>`
*	:ref:`Intimidire (Car)<intimidire>`
*	:ref:`Nuotare (For)<nuotare>`
*	:ref:`Percezione (Sag)<percezione>`
*	:ref:`Scalare (For)<scalare>`
*	:ref:`Sopravvivenza (Sag)<sopravvivenza>`

--------------------
Privilegi di Classe
--------------------

.. contents:: Riassumendo:
    :local:

Competenze nelle armi e armature
-----------------------------------

Tutte le armi semplici e da guerra, nelle armature leggere, nelle armature medie e negli scudi (tranne gli scudi torre).

.. _ira:

Ira (Str)
-------------------

Il barbaro può entrare, con un':ref:`Azione Gratuita<azione_gratuita>`, in uno stato di furia omicida che gli garantisce una forza ed una resistenza straordinarie durante il combattimento.
Il barbaro guadagna temporaneamente bonus +4 a Forza e a Costituzione, bonus morale +2 ai tiri salvezza su Volontà, ma subisce penalità –2 alla CA. L’aumento del punteggio di Costituzione garantisce al barbaro 2 punti ferita aggiuntivi per Dado Vita che non vengono persi per primi come accade per i punti ferita temporanei.

A partire dal 1° livello, il barbaro può entrare in ira per un numero di **round al giorno pari a 4 + il suo modificatore di Costituzione**. Rinnovandosi con 8 ore di riposo, anche non consecutive.

Ad ogni livello successivo, può entrare in ira per 2 round addizionali. Un aumento temporaneo della Costituzione non aumenta il numero dei round. 

In ira, un barbaro non può utilizzare abilità basate su Carisma, Destrezza ed Intelligenza (ad eccezione di :ref:`Acrobazia<acrobazia>`, :ref:`Intimidire<intimidire>`, :ref:`Cavalcare<cavalcare>` e :ref:`Volare<volare>`), o che richiedano
pazienza e concentrazione.

Il barbaro può placare l’ira con un’azione gratuita. Alla fine il barbaro è :ref:`Affaticato<affaticato>` per un numero di round pari a 2 volte il numero di round spesi in ira.

Se il barbaro è :ref:`Affaticato<affaticato>` non può entrare nuovamente in ira, ma può comunque entrare in ira più volte durante un singolo incontro o combattimento. Se il barbaro cade privo di sensi, l’ira si placa immediatamente, ed il barbaro è in pericolo di morte.

.. _avanzamento_barbaro:

.. figure:: Immagini/AvanzamentoBarbaro.png
    :align: center
    :width: 700

.. _poteri_ira:

Poteri d'Ira (Str)
------------------------

A partire dal 2° livello, un barbaro ottiene un potere d’ira, e uno addizionale ogni due livelli. 

Un barbaro può utilizzare i suoi poteri d’ira **mentre è in preda all’ira**, ed alcuni di questi poteri richiedono che il barbaro compia prima un’azione. I poteri possono essere selezionati una sola volta, a meno che sia indicato diversamente.

.. contents:: Lista dei poteri:
    :local:

Accuratezza sorprendente (Str):
**********************************

:Tipo: Azione Veloce
:Limiti: Una volta per ira.

Il barbaro ottiene bonus morale +1 ad un tiro per colpire +1 ogni 4 livelli da barbaro ottenuti. 
Da usare prima di effettuare il tiro.

Balzo difensivo (Str):
**********************************

:Tipo: Azione di Movimento

Bonus di schivare +1 alla Classe Armatura contro attacchi a distanza per un numero di round pari al Modificatore attuale di Costituzione (minimo 1) +1 ogni 6 livelli da barbaro ottenuti. 

Collera inarrestabile (Str):
**********************************

:Tipo: Passiva

Il barbaro può cadere in preda all’ira anche se è :ref:`Affaticato<affaticato>`. Mentre è in ira usando questo potere, il barbaro è immune alla condizione :ref:`Affaticato<affaticato>`.
Quando l’ira termina, il barbaro è esausto per 10 minuti per ogni round spesi in ira.

Colpo possente (Str):
**********************************

:Tipo: Azione Veloce
:Limiti: Una volta per ira

+1 ad un tiro per i danni +1 ogni 4 livelli da barbaro ottenuti. Va usata prima di effettuare il tiro.

Fiuto (Str):
**********************************

:Tipo: Passiva

Il barbaro ottiene la capacità fiuto mentre è in preda all’ira e può usarla per localizzare nemici invisibili (vedi :ref:`Capacità di Fiuto<capacità_fiuto>`).

Furia animale (Str):
**********************************

:Tipo: Attacco

Attacco con il morso. Se parte di un’azione di attacco completo, l’attacco con il morso viene effattuato usando il suo bonus di attacco base pieno –5. 

Se il morso va a segno, infligge 1d4 danni (1d3 se di taglia Piccola) più metà del suo modificatore di Forza. 

Si può usare questo potere come parte di un’azione per mantenere una presa in lotta o per liberarsi da essa, prima di effettuare la prova di :ref:`Lottare<azione_lottare>`. Se l’attacco con il morso va a segno, ogni prova di lotta effettuata contro il bersaglio nello stesso round riceve bonus +2.

Impulso dirompente (Str):
**********************************

:Tipo: Azione Immediata
:Limiti: Una volta per ira

Si aggiunge il livello da barbaro a:

-	Una prova di Forza
-	Prova di manovra in combattimento
-	Difesa da manovra in combattimento

Momento di chiarezza (Str):
**********************************

:Tipo: Azione Gratuita
:Limiti: Una volta per ira

Il barbaro non subisce nessun beneficio e nessuna penalità dall’ira per 1 round.

Il round si conteggia comunque nel numero totale di round di ira al giorno. 

Nessuna fuga (Str):
**********************************

:Tipo: Azione Immediata
:Limiti: Una volta per ira

Il barbaro può muoversi fino al doppio della sua velocità quando un avversario a lui adiacente usa l’azione di ritirata per allontanarsi da lui. 

Il movimento deve finire adiacente al nemico che ha usato l’:ref:`Azione di Ritirata<azione_ritirata>`.

Si provocano :ref:`Attacchi di Opportunità<attacco_di_opportunità>` come di norma durante questo movimento.

Nuotatore indomabile (Str):
**********************************

:Tipo: Passiva

Il barbaro aggiunge il suo livello come bonus di potenziamento a tutte le prove di :ref:`Nuotare<nuotare>`.

Piede veloce (Str):
**********************************

:Tipo: Passiva

Bonus di potenziamento alla sua velocità di 1,5 metri. 

Potere selezionabile fino a 3 volte.

Posizione difensiva (Str):
**********************************

:Tipo: Azione di Movimento

Bonus di schivare +1 alla Classe Armatura contro attacchi in mischia per round = Modificatore Costituzione (minimo 1) +1 ogni 6 livelli da barbaro. 

Non provoca attacchi di opportunità.

Respingere gli avversari (Str):
**********************************

:Tipo: Attacco in mischia

Una volta per round, si può effettuare un tentativo di spingere contro un bersaglio al posto di un attacco in mischia, senza provocare :ref:`Attacchi di Opportunità<attacco_di_opportunità>`.

Se si ha successo, il bersaglio subisce danni pari al modificatore di Forza e viene spinto indietro, senza bisogno di seguirlo. 

Riflessi rapidi (Str):
**********************************

:Tipo: Passiva

Mentre è in ira, il barbaro può effettuare un :ref:`Attacco di Opportunità<attacco_di_opportunità>` addizionale per round.

Saltatore indomabile (Str):
**********************************

:Tipo: Passiva

Il livello da barbaro si aggiunge come bonus di potenziamento a tutte le prove di :ref:`Acrobazia<acrobazia>` effettuate per saltare. Quando effettua un salto in questo modo, si suppone abbia una rincorsa.

Scalatore indomabile (Str):
**********************************

:Tipo: Passiva

Il livello da barbaro si aggiunge come bonus di potenziamento a tutte le prove di :ref:`Scalare<scalare>`.

.. _sguardo_intimidatorio:

Sguardo intimidatorio (Str):
**********************************

:Tipo: Azione di Movimento

Si può effettuare una prova di Intimidire contro un nemico adiacente. Se ha successo, il barbaro demoralizza il suo avversario che resta scosso per 1d4 round + 1 round per ogni 5 punti con cui la prova supera la CD.

Superstizione (Str):
**********************************

:Tipo: Passiva

Bonus morale +2 +1 ogni 4 livelli da barbaro ai tiri salvezza effettuati per resistere a incantesimi, capacità soprannaturali e magiche. 

Non si può essere un bersaglio consenziente di alcun incantesimo e si deve effettuare tiri salvezza per resistere a tutti gli incantesimi, **anche quelli alleati**.

Vigore Rinnovato (Str):
**********************************

:Tipo: Azione Standard
:Liv Min: 4
:Limiti: Una volta al giorno

Si può guarire 1d8 danni + modificatore Costituzione + 1d8 ogni 4 livelli sopra il quarto. Massimo 5d8.

Visione Crepuscolare (Str):
**********************************

:Tipo: Passiva

Si guadagna :ref:`visione crepuscolare<visione_crepuscolare>`.

Visione notturna (Str):
**********************************

:Tipo: Passiva
:Prerequisito: :ref:`Visione Crepuscolare<visione_crepuscolare>`

Si guadagna :ref:`scurovisione<scurovisione>` fino a 18m.

:ref:`Visione Crepuscolare<visione_crepuscolare>` si può avere anche come tratto razziale.

Attacco improvviso (Str):
**********************************

:Tipo: Attacco di Opportunità
:Liv Min: 8
:Limiti: Una volta per ira

Il barbaro può effettuare un attacco di opportunità contro un avversario che si muove in qualsiasi quadretto minacciato che minaccia, a prescindere dal fatto che quel movimento normalmente provochi un attacco di opportunità. 

Grido terrificante (Str):
**********************************

:Tipo: Azione Standard
:Liv Min: 8
:Prerequisito: Potere d'ira :ref:`Sguardo Intimidatorio<sguardo_intimidatorio>`

Tutti i nemici scossi entro 9 metri devono superare un tiro salvezza su Volontà (CD = 10 + 0.5 liv barbaro + mod For) o cadono in preda al panico per 1d4+1 round.

L'avversario è poi immune a questo potere per 24 ore.

Mente lucida (Str):
**********************************

:Tipo: Azione Immediata
:Liv Min: 8
:Limiti: Una volta per ira

Si può ritirare un tiro salvezza fallito su Volontà, ma prima che il risultato del fallimento sia rivelato dal GM. 

Il secondo risultato, anche se peggiore, rimane valido. 

Riduzione del danno aumentata (Str):
*************************************

:Tipo: Passiva
:Liv Min: 8

La riduzione del danno aumenta di 1/—. Si può scegliere questo potere fino ad un massimo di tre volte per cumularne gli effetti.

Vigore innato (Str):
**********************************

:Tipo: Passiva
:Liv Min: 8

Immunità a infermità e nauseato.

Attacco devastante (Str):
**********************************

:Tipo: Azione Immediata
:Liv Min: 12
:Limiti: Una volta per ira

Il barbaro può confermare automaticamente un colpo critico. 

Ira indomita (Str):
**********************************

:Tipo: Passiva
:Liv Min: 12

In Ira si è immuni da scosso e spaventato.

-------------------------------------------------

.. figure:: Immagini/Barbaro2.png
    :align: left
    :width: 400

Movimento Veloce (Str)
------------------------------

Il barbaro ha una velocità superiore alla norma per la sua razza di +3 metri quando non indossa armature, o ne indossa una leggera o media, e non sta trasportando un carico pesante. 

Questo bonus si applica prima di modificare la velocità del barbaro in base a qualsiasi carico portato o armatura indossata. 

Il bonus si cumula con altri bonus alla velocità sul terreno del barbaro.

.. _schivare_prodigioso:

Schivare prodigioso (Str)
------------------------------

:Liv Min: 2

Non si può essere colti impreparati, anche se l’attaccante è invisibile. 
Si perde comunque il bonus di Destrezza alla CA se si viene immobilizzati o se si subisce un'azione di :ref:`Fintare<fintare>`. 

Se il barbaro possiede già schivare prodigioso come capacità di un’altra classe, guadagna automaticamente :ref:`schivare prodigioso migliorato<schivare_prodigioso_migliorato>`.

Percepire trappole (Str)
-------------------------------

:Liv Min: 3

+1 ai tiri salvezza su Riflessi effettuati per evitare le trappole e bonus di schivare +1 alla CA contro gli attacchi effettuati dalle trappole. 

+1 ogni tre ulteriori livelli da barbaro (6°, 9°, 12°, 15° e 18°). 

I bonus di da più classi sono cumulativi.

.. _schivare_prodigioso_migliorato:

Schivare prodigioso migliorato (Str)
---------------------------------------

:Liv Min: 5

Non si può essere attaccati ai fianchi. 

Questa difesa nega ai ladri la possibilità di utilizzare attacchi furtivi attaccando ai fianchi, a meno che non abbiano 4 livelli da ladro in più del barbaro preso di
mira.

Se un personaggio ha già :ref:`schivare prodigioso<schivare_prodigioso>` da un’altra classe, i livelli delle classi che forniscono schivare prodigioso si sommano per determinare il livello minimo che un ladro deve avere per potere attaccare il personaggio ai fianchi.

Riduzione del danno (Str)
----------------------------

:Liv Min: 7

Riduzione del danno. 

Ogni volta che il barbaro subisce dei danni da un’arma o da un attacco naturale bisogna sottrarre 1 danno dal totale dei danni di ogni singolo attacco andato a
segno. 

Al 10° livello, e ad ogni tre livelli da barbaro ulteriori (13°, 16° e 19°), questa riduzione aumenta di 1 punto. 

Ira superiore (Str)
----------------------------

:Liv Min: 11

Il bonus morale in Ira a Forza e Costituzione aumenta a +6 ed il bonus morale ai tiri salvezza su Volontà aumenta a +3.

Volontà indomita (Str)
----------------------------

:Liv Min: 14

Durante l’ira bonus +4 ai tiri salvezza su Volontà per resistere ad incantesimi di ammaliamento. 

Questo bonus si somma a tutti gli altri modificatori, compreso il bonus morale ai tiri salvezza su Volontà che il barbaro riceve durante l’ira.

Ira infaticabile (Str)
----------------------------

:Liv Min: 17

Non si è più affaticati al termine dell’ira.

Ira possente (Str)
----------------------------

:Liv Min: 20

Il bonus morale in Ira a Forza e Costituzione aumenta a +8 ed il bonus morale ai tiri salvezza su Volontà aumenta a +4.

----------------------------
Ex-Barbari
----------------------------

Un barbaro che diventa legale perde la capacità di ira e non può più avanzare di livello come barbaro. 

Mantiene gli altri benefici di classe.

.. _bardo:

Bardo
==================================

Con ingegno, esperienza e magia, i bardi mettono in chiaro le astuzie del mondo,
diventando adepti delle arti della persuasione, della manipolazione e dell’ispirazione. 

I bardi possiedono una capacità prodigiosa di conoscere più di quanto dovrebbero e di usare ciò che apprendono per mantenere se stessi ed i loro alleati un passo avanti al pericolo. Ogni giorno porta nuove occasioni, avventure e sfide e soltanto sapendo di più ed essendo i migliori si potrà esigere la loro ricompensa.

:Ruolo: 

    I bardi confondono e disorientano i loro nemici mentre ispirano i loro alleati a più alti rischi.

    Sebbene accompagnata da armi e magia, la vera forza dei bardi si trova fuori dalla mischia, dove possono aiutare i loro compagni ed indebolire i nemici senza paura delle interruzioni alle loro esibizioni.

.. figure:: Immagini/Bardo.png
    :align: right
    :width: 400

:Allineamento: Qualsiasi
:Dado Vita: d8

----------------------------
Abilità di classe
----------------------------

:Crescita per livello: 6 + modificatore Intelligenza (Mod Int)

*	:ref:`Acrobazia (Des)<acrobazia>`
*	:ref:`Artigianato (Int)<artigianato>`
*	:ref:`Artista della Fuga (Des)<artista_della_fuga>`
*	:ref:`Camuffare (Car)<camuffare>`
*	:ref:`Conoscenze (tutte) (Int)<conoscenze>`
*	:ref:`Diplomazia (Car)<diplomazia>`
*	:ref:`Furtività (Des)<furtività>`
*	:ref:`Intimidire (Car)<intimidire>`
*	:ref:`Intrattenere (Car)<intrattenere>`
*	:ref:`Intuizione (Sag)<intuizione>`
*	:ref:`Linguistica (Int)<linguistica>`
*	:ref:`Percezione (Sag)<percezione>`
*	:ref:`Professione (Sag)<professione>`
*	:ref:`Raggirare (Car)<raggirare>`
*	:ref:`Rapidità di Mano (Des)<rapidità_di_mano>`
*	:ref:`Sapienza Magica (Int)<sapienza_magica>`
*	:ref:`Scalare (For)<scalare>`
*	:ref:`Utilizzare Congegni Magici (Car)<utilizzare_congegni_magici>`
*	:ref:`Valutare (Int)<valutare>`

--------------------
Privilegi di Classe
--------------------

.. contents:: Riassumendo:
    :local:

Competenze nelle armi e armature
----------------------------------

Tutte le armi semplici, più arco corto, frusta, manganello, spada corta, spada lunga e stocco. 

Armature leggere e scudi (tranne gli scudi torre). 

Si possono lanciare incantesimi mentre si indossa un’armatura leggera e usa uno scudo senza incorrere nelle normali probabilità di fallimento. 

Tuttavia, come qualsiasi altro incantatore arcano, indossando un’armatura media o pesante si incorre nella probabilità di fallimento degli incantesimi arcani se l’incantesimo lanciato ha componenti somatiche. 

Un bardo multiclasse incorre sempre nelle normali probabilità di fallimento degli incantesimi arcani se lancia incantesimi arcani ricevuti da altre classi.

Incantesimi
-------------------

:Lista: :ref:`Incantesimi da Bardo<incantesimi_bardo>`
:Preparazione: No

Ogni incantesimo da bardo ha una componente verbale (canto, recitazione o musica) e per impararli o lanciarli, deve avere un Carisma di almeno 10 + Liv Incantesimo. 

La Classe Difficoltà per un tiro salvezza contro un incantesimo da bardo è 10 + Liv incantesimo + modificatore Carisma.

.. _incantesimi_bardo_giorno:

Come gli altri incantatori, un bardo può lanciare solo un certo numero di incantesimi al giorno per ogni livello di incantesimi (+ quelli :ref:`bonus<incantesimi_bonus>`, basati sul carisma): 

.. figure:: Immagini/IncantesimiBardo.png
    :align: center

La selezione di incantesimi è estremamente limitata, cominciando la carriera conoscendo quattro incantesimi di livello 0 e due incantesimi di 1° livello
di sua scelta. Man mano che sale di livello, il bardo guadagna
uno o più nuovi incantesimi, come indicato nella Tabella a lato
(il numero di incantesimi conosciuti dal bardo non è influenzato dal punteggio
di Carisma).

.. _incantesimi_bardo_livello:

.. figure:: Immagini/IncantesimiBardoLivello.png
    :align: left
    :width: 300

Al 5° livello, e ogni tre livelli da bardo ulteriori (8°, 11°...), un bardo può scegliere di imparare un (e uno solo) nuovo incantesimo al posto di un incantesimo che già
conosce. 

In pratica, “perde” il vecchio incantesimo in cambio di uno nuovo. Il livello del nuovo incantesimo deve essere identico a quello dell’incantesimo scambiato, e almeno di due livelli inferiore del più alto livello di incantesimo da bardo che il bardo è in grado di lanciare. La decisione deve avvenire nel momento in cui guadagna nuovi incantesimi conosciuti per livello.

.. _esibizione_bardica:

Esibizione Bardica
----------------------------

:Tipo: Azione Standard, di Movimento o Veloce (dipende dal livello)

Un bardo può usare un’abilità di :ref:`Intrattenere<intrattenere>` per creare effetti magici su chi gli sta vicino, compreso se stesso. 

Si può usare questa capacità per 4 + Mod Carisma round al giorno + 2 per ogni livello dopo il 1°. Ad ogni round, un bardo può produrre uno dei tipi di esibizione bardica che ha appreso, come indicato dal suo livello. 

Attivare un effetto di esibizione bardica è un’:ref:`azione_standard`, mantenuta come azione gratuita ogni round. Cambiare gli effetti di esibizione bardica richiede che il bardo interrompa la sua esecuzione e ne inizi una nuova come azione standard.

Esibizione bardica non può essere interrotta, ma termina immediatamente se il bardo
viene ucciso, paralizzato o stordito, reso privo di sensi o gli viene impedito in altro modo di usare l’azione gratuita per mantenerla ogni round. 

Un bardo non può avere più di un effetto di esibizione bardica attivo nello stesso momento.
Al 7° livello, **azione di movimento** anziché standard. 
Al 13° livello, **azione veloce**.

Ogni esibizione bardica ha componenti sonore, visive, o entrambe.
Se componenti sonore, i bersagli devono essere in grado di sentire il bardo affinché questa abbia effetto e dipende dal linguaggio. Un bardo sordo ha una probabilità del 20% di fallire se tenta di usare una esibizione bardica con una componente sonora. Se fallisce, il tentativo conta lo stesso nel suo limite giornaliero.

Se la capacità di esibizione bardica ha componenti visive, i bersagli devono essere in grado di vedere il bardo affinché questa capacità abbia effetto. 
Un bardo cieco ha una probabilità del 50% di fallire se tenta di usare una esibizione bardica con una componente visiva. Se fallisce, il tentativo conta lo stesso nel suo limite giornaliero. 

.. contents:: Lista di Esibizioni:
    :local:

.. _bardo_affascinare:

Affascinare (Sop)
******************************

:Liv Min: 1
:Componenti: Sonore, Visive

Un bardo può affascinare uno o più creature che siano entro 27 metri, in grado di vederlo, sentirlo e prestargli attenzione. 
Anche il bardo deve essere in grado di vedere la creatura. 
La distrazione di un combattimento vicino o di altri pericoli impedisce che la capacità funzioni. 

Ogni 3 livelli ottenuti dal bardo dopo il 1°, con questa capacità egli può colpire una creatura addizionale.

Ogni creatura entro il raggio riceve un tiro salvezza su Volontà, con **CD = 10 + 0.5 liv bardo + Mod Car**, per negare l’effetto. Se riesce, il bardo non può tentare di affascinare di nuovo quella creatura per le successive 24 ore. Se fallisce, la creatura si siede tranquillamente ad osservare il bardo, senza effettuare nessun’altra azione, finchè guarda l'esibizione.

Mentre è affascinata, una creatura subisce –4 alle prove di abilità effettuate come reazioni, come :ref:`Percezione<percezione>`. Qualsiasi minaccia potenziale al bersaglio gli permette di effettuare un nuovo tiro salvezza contro l’effetto.

Qualsiasi minaccia evidente, come estrarre un’arma, lanciare un incantesimo, puntare un’arma a distanza al bersaglio, spezza automaticamente l’effetto.

*Affascinare* è una capacità di ammaliamento (compulsione) che influenza la mente. 

.. _bardo_controcanto:

Controcanto (Sop)
******************************
:Liv Min: 1
:Componenti: Sonore

Permette di contrastare gli effetti magici basati sul suono (non con componente verbale). 

Ad ogni round il bardo effettua una prova di :ref:`Intrattenere<intrattenere>` (canto, strumenti a corda, a fiato, a percussione o a tastiera) e ogni creatura nel raggio di 9 metri dal bardo (incluso il bardo), condizionata da un attacco magico dipendente dal linguaggio o sonoro può usare il risultato della prova di :ref:`Intrattenere<intrattenere>` del bardo al posto del tiro salvezza dopo averlo effettuato. 

Se una creatura è già sotto l’effetto di un attacco magico dipendente dal linguaggio o sonoro non istantaneo, guadagna un altro tiro salvezza contro l’effetto ad ogni round in cui ascolta *controcanto*, ma deve utilizzare il risultato della prova di :ref:`Intrattenere<intrattenere>` del bardo come tiro salvezza. 

Il *controcanto* non ha efficacia contro effetti che non permettono tiri salvezza.

.. _bardo_distrazione:

Distrazione (Sop)
******************************
:Liv Min: 1
:Componenti: Visive

Permette di contrastare effetti magici basati sulla vista. 

Ad ogni round il bardo effettua una prova di :ref:`Intrattenere<intrattenere>` (commedia, danza, oratoria o recitazione) e ogni creatura nel raggio di 9 metri dal bardo (incluso il bardo), condizionata da un attacco magico dipendente da un’illusione (trama o finzion) può usare il risultato della prova di :ref:`Intrattenere<intrattenere>` del bardo al posto del tiro salvezza dopo averlo effettuato. 

Se una creatura è già sotto l’effetto di un attacco magico di un’illusione non istantanea (trama o finzione), guadagna un altro tiro salvezza contro l’effetto ad ogni round in cui vede la *distrazione*, ma deve utilizzare il risultato della prova di :ref:`Intrattenere<intrattenere>` del bardo come tiro salvezza. 

La *distrazione* non ha efficacia contro effetti che non permettono tiri salvezza.

.. _bardo_ispirare_coraggio:

Ispirare coraggio (Sop)
******************************
:Liv Min: 1
:Componenti: Sonore o Visive

Permette al bardo di ispirare coraggio negli alleati (incluso se stesso), sostenendoli contro la paura e migliorando le loro capacità di combattimento, a patto che possano sentirlo o vederlo. 

Bonus morale +1 ai tiri salvezza contro charme e paura, e bonus competenza +1 al tiro per colpire e ai danni. 

Al 5° livello, e ogni 6 livelli da bardo successivi, questo bonus aumenta di +1, fino ad un massimo di +4. 

*Ispirare coraggio* è una capacità di influenza mentale e il bardo decide con che componenti lanciarla.

Ispirare competenza (Sop)
******************************
:Liv Min: 3
:Componenti: Sonore

Permette di aiutare un alleato ad eseguire un compito, a patto che sia entro 9 metri ed in grado di vedere e sentire il bardo. 

Bonus di competenza +2 alle sue prove di abilità con un’abilità particolare per tutto il tempo in cui ascolta la musica. 

Questo bonus aumenta di +1 ogni quattro livelli che il bardo ottiene dopo il 3° (+3 al 7°, +4 all’11°, +5 al 15° e +6 al 19°). 

Alcuni usi di non sono fattibili, come i tentativi di :ref:`Furtività<furtività>`, e potrebbero essere vietati a discrezione del GM. 

Un bardo non può ispirare competenza su se stesso. 

.. _bardo_suggestione:

Suggestione (Mag)
******************************
:Liv Min: 6
:Componenti: Sonore

Permette di imporre una *suggestione* (come :ref:`l’incantesimo<inc_suggestione>`) ad una sola creatura che sia già :ref:`affascinata<bardo_affascinare>`.

Utilizzare questa capacità non spezza la concentrazione del bardo per l’effetto di :ref:`affascinare<bardo_affascinare>`, ma richiede un’**azione standard** per essere attivata. 

Si può usare più di una volta contro una creatura specifica durante un utilizzo specifico della stessa.

Imporre la suggestione **non conta per il limite** giornaliero del bardo di esibizioni bardiche. Un tiro salvezza su Volontà, con **CD = 10 + 0.5 liv bardo + Mod Car**, nega l’effetto. 

*Suggestione* è una capacità di ammaliamento (compulsione), che influenza la mente, dipendente dal linguaggio.

Ispirare Terrore (Sop)
*************************
:Liv Min: 8
:Componenti: Sonore e Visive

Permette di scatenare un senso di orrore crescente nei nemici entro 9 metri ed in grado di vedere e sentire il bardo suonare, facendoli diventare scossi.  

L’effetto dura fin tanto che il nemico resta entro 9 metri dal bardo, e questi
continua la sua esecuzione. 

Questa capacità non fa diventare i nemici spaventati o in preda al panico, anche se sono già scossi per altri effetti. 

Ispirare terrore è un effetto di paura che influenza la mente.

Ispirare grandezza (Sop)
****************************
:Liv Min: 9
:Componenti: Sonore e Visive

Permette di ispirare grandezza su se stesso o su un singolo alleato consenziente entro 9 metri, fornendo capacità aggiuntive in combattimento. 

Per ogni 3 livelli da bardo dopo il 9°, il bardo può ispirare grandezza su un alleato addizionale (fino ad un massimo di 4). 
Tutti i bersagli devono essere in grado di vedere e sentire il bardo. 

Fornisce 2 *Dadi Vita* bonus (d10), il corrispondente numero di punti ferita temporanei (compreso il Mod Cos), bonus di competenza +2 al tiro per colpire e bonus +1 ai tiri salvezza su *Tempra*. 
I *Dadi Vita* bonus contano come *Dadi Vita* regolari per determinare gli effetti di incantesimi che sono dipendenti dai *Dadi Vita*.

Ispirare grandezza è una capacità di influenza mentale.

Musica lenitiva (Mag)
************************
:Liv Min: 12
:Componenti: Sonore e Visive

Permette di creare un effetto equivalente ad un incantesimo :ref:`cura ferite gravi di massa<inc_cura_ferite_gravi_di_massa>` con Liv Bardo come Liv Inc. 

Questa musica rimuove le condizioni di affaticato, infermo e scosso. 

Questa capacità richiede 4 round di esecuzione ininterrotta, e i bersagli devono essere in grado di sentire e vedere il bardo e restare entro 9 metri da lui per tutta l’esecuzione. 

Accordo Spaventoso (Mag)
***************************
:Liv Min: 14
:Componenti: Sonore

Permette di causare paura nei nemici in grado di sentire l’esecuzione del bardo e trovarsi entro 9 metri da lui. 

Ogni nemico riceve un tiro salvezza su Volontà, con **CD = 10 + 0.5 Liv Bardo + Mod Car**, per negare l’effetto. Se il tiro riesce, il bardo non può usare accordo spaventoso su quella creatura nuovamente per le successive 24 ore. Se fallisce, la creatura è spaventata e fugge via finché sente l’esecuzione del bardo

Ispirare Eroismo (Sop)
**************************
:Liv Min: 15
:Componenti: Sonore e Visive

Permette di ispirare eroismo su se stesso o su un singolo alleato entro 9 metri. 

Ogni 3 livelli da bardo ottenuti dopo il 15°, il bardo può ispirare eroismo su un alleato addizionale. 
Tutti i bersagli devono essere in grado di sentire e vedere il bardo. 

+4 ai tiri salvezza e bonus di schivare +4 alla CA. L’effetto dura per tutto il tempo in cui i bersagli sono in grado di assistere all’esecuzione del bardo.

*Ispirare eroismo* è una capacità di influenza mentale.

Suggestione di Massa (Mag)
****************************
:Liv Min: 18
:Componenti: Sonore

Il bardo può usare :ref:`suggestione<bardo_suggestione>` su qualsiasi creatura abbia :ref:`affascinato<bardo_affascinare>`.

*Suggestione di massa* è una capacità di ammaliamento (compulsione), che influenza la mente e dipendente dal linguaggio.

Musica Mortale (Sop)
***********************
:Liv Min: 20
:Componenti: Sonore e Visive

Permette di causare la morte di un nemico per gioia o disperazione. 

Il bersaglio deve essere in grado di sentire l’esecuzione del bardo per 1 round
completo e trovarsi entro 9 metri da lui. Il bersaglio riceve un tiro salvezza su Volontà, con **CD = 10 + 0.5 Liv Bardo + Mod Car**, per negare l’effetto. 

Seil tiro salvezza riesce, il bersaglio è barcollante per 1d4 round ed il bardo non può usare musica mortale su quella creatura nuovamente per le successive 24 ore. 
Se fallisce, la creatura muore. 

Musica mortale è un effetto di morte che influenza la mente.

-------------------------------------------------

.. figure:: Immagini/Bardo2.png
    :align: left
    :width: 400

.. _conoscenze_bardiche:

Conoscenze Bardiche (Str)
----------------------------

Il bardo aggiunge metà del suo livello di classe (minimo 1) a tutte le prove di Conoscenze e può effettuarle anche senza addestramento.

Trucchetti (Mag)
--------------------------

Un bardo conosce alcuni trucchetti, o incantesimi di livello 0 (:ref:`Vedi Tabella<incantesimi_bardo_livello>`). 

I trucchetti vengono lanciati come tutti gli altri incantesimi, ma non consumano slot e possono essere utilizzati di nuovo.

Esecuzione versatile (Str)
----------------------------
:Liv Min: 2

Un bardo può scegliere un tipo di :ref:`Intrattenere<intrattenere>`. 

Egli può usare il suo bonus in quell’abilità al posto del bonus che ha in abilità associate utilizzando il bonus totale di :ref:`Intrattenere<intrattenere>`, compreso quello come abilità di classe, al posto del bonus dell’abilità associata. 

Al 6° livello, e successivamente ogni 4 livelli, il bardo può scegliere un tipo
addizionale di Intrattenere da sostituire.

I tipi di :ref:`Intrattenere<intrattenere>` e le loro abilità associate sono: 

*	Canto - :ref:`Intuizione<intuizione>`, :ref:`Raggirare<raggirare>`
*	Commedia - :ref:`Intimidire<intimidire>`, :ref:`Raggirare<raggirare>`
*	Danza - :ref:`Acrobazia<acrobazia>`, :ref:`Volare<volare>`
*	Oratoria - :ref:`Diplomazia<diplomazia>`, :ref:`Intuizione<intuizione>`
*	Recitazione - :ref:`Camuffare<camuffare>`, :ref:`Raggirare<raggirare>`
*	Strumenti a corda - :ref:`Diplomazia<diplomazia>`, :ref:`Raggirare<raggirare>`
*	Strumenti a fiato - :ref:`Addestrare Animali<addestrare_animali>`, :ref:`Diplomazia<diplomazia>`
*	Strumenti a percussione - :ref:`Addestrare Animali<addestrare_animali>`, :ref:`Intimidire<intimidire>`
*	Strumenti a tastiera - :ref:`Diplomazia<diplomazia>`, :ref:`Intimidire<intimidire>`

Avvezzo (Str)
-------------
:Liv Min: 2

Il bardo diventa resistente alla esibizione bardica degli altri bardi e agli
effetti sonori in genere. 

+4 ai tiri salvezza effettuati contro capacità di esibizione bardica ed effetti sonori e dipendenti dal linguaggio.

Maestro del sapere (Str)
------------------------
:Liv Min: 5

Il bardo diventa un maestro del sapere e può prendere 10 a tutte le prove di Conoscenze in cui ha gradi, ma può scegliere di tirare normalmente. 

Una volta al giorno, il bardo può prendere 20 alle prove di Conoscenze come azione standard. Può usare questa capacità una volta in più al giorno per ogni 6 livelli che possiede oltre al 5°, fino ad un massimo di 3 volte al giorno al 17° livello.

Eclettico (Str)
-------------------
:Liv Min: 10

Il bardo può usare qualsiasi abilità.

Al 16° livello, il bardo considera tutte le abilità come abilità di classe. 

Al 19° livello, il bardo può prendere 10 a tutte le prove di abilità, anche se ciò non è normalmente permesso.

.. _chierico:

Chierico
=========================

Chiamati per servire i poteri oltre la comprensione dei mortali, tutti i sacerdoti predicano le meraviglie del creato e provvedono ai bisogni spirituali dei loro fedeli. 

I chierici sono più che semplici sacerdoti. Sono emissari del divino che esaudiscono la volontà delle loro divinità con la forza delle armi o con i poteri magici loro concessi. 

I chierici dimostrano di essere diversi l’uno dall’altro come gli déi che servono: alcuni offrono redenzione e guarigione, altri portano legge e giustizia, e altri ancora seminano conflitto e corruzione. 

I modi dei chierici sono svariati, ma tutti coloro che seguono questa carriera lo fanno con gli alleati più forti ed imbracciano le armi delle divinità.

:Ruolo: 

    I chierici si dimostrano spesso combattenti vigorosi e capaci. La loro vera forza sta nella loro capacità di attingere al potere della loro divinità, per aumentare il proprio valore o quello dei propri alleati in battaglia, per vessare i nemici con magie divine, o per soccorrere i compagni che hanno bisogno delle loro cure.

    I poteri sono influenzati dalla fede, e tutti i chierici focalizzano la loro fede su una fonte divina.

    Un piccolo numero onora un concetto di fede astratto che reputano degno di devozione: come la guerra, la morte, la giustizia o la conoscenza (chiedete al vostro GM se preferite questo percorso invece che scegliere una divinità specifica).

.. figure:: Immagini/Chierico.png
    :align: right
    :width: 400

:Allineamento: 

    Entro un passo da quello della propria divinità lungo l'asse legge/caos o bene/male.
:Dado Vita: d8

----------------------------
Abilità di classe
----------------------------

:Crescita per livello: 2 + modificatore Intelligenza (Mod Int)

*	:ref:`Artigianato (Int)<artigianato>`
*	:ref:`Conoscenze (arcane, nobiltà, piani, religioni, storia) (Int)<conoscenze>`
*	:ref:`Diplomazia (Car)<diplomazia>`
*	:ref:`Guarire (Sag)<guarire>`
*	:ref:`Intuizione (Sag)<intuizione>`
*	:ref:`Linguistica (Int)<linguistica>`
*	:ref:`Professione (Sag)<professione>`
*	:ref:`Sapienza Magica (Int)<sapienza_magica>`
*	:ref:`Valutare (Int)<valutare>`

--------------------
Privilegi di Classe
--------------------

.. contents:: Riassumendo:
    :local:

Competenze nelle armi e armature
----------------------------------

Tutte le armi semplici, le armature leggere o medie e negli scudi (tranne gli scudi torre). 

I chierici sono inoltre competenti nell’uso dell’arma preferita della loro divinità.

Aura (Str)
--------------------

Un chierico di una divinità buona, caotica, legale o malvagia è dotato di una particolare potente aura corrispondente all’allineamento della divinità (vedi gli incantesimi di individuazione di :ref:`bene<inc_individuazione_del_bene>`, :ref:`caos<inc_individuazione_del_caos>`, :ref:`legge<inc_individuazione_della_legge>` e :ref:`male<inc_individuazione_del_male>`).

Incantesimi
--------------------

.. _incantesimi_giorno_chierico:

.. figure:: Immagini/IncantesimiChierico.png

:Lista: :ref:`Incantesimi da Chierico<incantesimi_chierico>`
:Preparazione: Sì

L'allineamento può precludere il lancio di alcuni incantesimi avversi al proprio credo etico e morale, come :ref:`descritto più avanti<incantesimi_buoni_caotici_etc>`.

Per preparare o lanciare un incantesimo, un chierico deve avere Saggezza di almeno 10 + Liv Incantesimo. La Classe Difficoltà per un tiro salvezza contro un incantesimo da chierico è 10 + il Liv Incantesimo + Mod Sag.

Come gli altri incantatori, un chierico ha un :ref:`numero limitato di incantesimi al giorno<incantesimi_giorno_chierico>` che può essere modificata a seconda del Mod Sag, :ref:`com'è usuale<incantesimi_bonus>`.

Per ricaricare la riserva giornaliera di incantesimi un chierico deve scegliere un momento del giorno in cui spendere 1 ora in tranquilla contemplazione o supplica e può preparare e lanciare qualsiasi incantesimo che si trova nella lista degli incantesimi da chierico, ammesso che li possa utilizzare.

.. _incanalare_energia:

Incanalare energia (Sop)
---------------------------
:Tipo: Azione Standard

I chierici possono liberare un’ondata di energia incanalando il potere della propria fede attraverso il simbolo sacro (o sacrilego). che può essere usata per infliggere
o curare danni, in base al tipo di energia incanalata e alle creature bersaglio.

**Divinità Buona**: (o buono) con energia positiva si può scegliere di infliggere danni alle creature non morte o di curare le creature viventi.
**Divinità Malvagia**: (o malvagio) con energia negativa si può scegliere di infliggere danni alle creature viventi o di curare le creature non morte. 
**Nessuna Divinità**: (o neutrale) sia energia positiva che negativa. Una volta effettuata la scelta,non si può più modificare. Questa decisione determina anche se il chierico :ref:`lancia spontaneamente<lancio_spontaneo>` incantesimi di cura o infliggere.

*Incanalare energia* causa un’esplosione che colpisce tutte le creature di un tipo (non morte o viventi) in un raggio di 9 metri attorno al chierico. 
La quantità di danno inflitto o curato è pari a 1d6 + 1d6 ogni due livelli da chierico oltre il 1° (2d6 al 3°, 3d6 al 5°, e così via). 

Le creature che subiscono danno dall’energia incanalata ricevono un tiro salvezza su Volontà per dimezzarlo, con **CD = 10 + 0.5 Liv Chierico + Mod Car**. 

Un chierico può incanalare per un numero di volte al giorno pari a 3 + Mod Car.

Si può scegliere di includere o meno se stessi nell’effetto e bisogna essere in grado di presentare il simbolo sacro per usare questa capacità.

Domini
-----------------

:Tipo: Azione Standard (normalmente)

Un chierico deve scegliere due domini tra quelli appartenenti alla sua divinità. Può selezionare un dominio di allineamento (Bene, Caos, Legge, Male) solo se il suo allineamento concorda con quel dominio. 

Se il chierico non è devoto a una divinità particolare, deve comunque scegliere due domini che rappresentino le sue inclinazioni spirituali e le sue capacità. Le restrizioni sull’allineamento rimangono.

Ogni dominio fornisce al chierico dei poteri, in base al suo livello da chierico, così come degli incantesimi bonus, uno slot incantesimo di dominio per ogni livello di incantesimo che è in grado di lanciare. 

Ogni giorno, un chierico può preparare un incantesimo in questo slot incantesimi **da uno dei suoi due domini**. Se un incantesimo di dominio non è elencato nella lista degli incantesimi da chierico, è possibile prepararlo solo nello slot di dominio. 

Gli incantesimi di dominio non possono essere usati per il :ref:`lancio spontaneo<lancio_spontaneo>`.

Orazioni
---------------

I chierici possono preparare un numero di orazioni o incantesimi di livello 0, ogni giorno. Questi incantesimi sono lanciati come tutti gli altri incantesimi ma non si
consumano una volta lanciati e possono essere usati di nuovo.

.. _lancio_spontaneo:

Lancio Spontaneo
-------------------------

Un **chierico buono** (o di una divinità buona) può catalizzare l’energia magica accumulata in incantesimi di guarigione che non ha preparato in precedenza. 
Si può *perdere* un incantesimo preparato che non sia un’orazione o un incantesimo di dominio per lanciare un qualsiasi incantesimo curare (con *cura* nel nome) dello stesso livello o di livello inferiore.

Un **chierico malvagio** (o una divinità malvagia) può convertire gli incantesimi
preparati in incantesimi curare, ma può convertirli in incantesimi infliggere (con *infliggi* nel nome).

Un **chierico neutrale**, può convertire gli incantesimi in incantesimi curare o di infliggere, a scelta. Una volta scelto, il giocatore non può più cambiare idea.
Questa scelta determina anche se il chierico neutrale :ref:`incanala energia<incanalare_energia>` positiva o negativa.

.. _incantesimi_buoni_caotici_etc:

Incantesimi Buoni/Caotici/Legali/Malvagi
------------------------------------------

Un chierico non può lanciare incantesimi di un allineamento opposto al suo o a quello della sua divinità. 

Incantesimi associati con particolari allineamenti sono indicati dai descrittori del bene, del caos, della legge e del male nella descrizione dell’incantesimo.

Linguaggi Bonus
-----------------

Le opzioni per i linguaggi bonus del chierico comprendono Abissale, Celestiale ed Infernale (rispettivamente i linguaggi degli esterni di allineamento caotico malvagio,
buono, e legale malvagio). Questi linguaggi sono in aggiunta a quelli bonus dati dalla razza di appartenenza.

-------------------
Ex-Chierici
-------------------

Un chierico che viola grossolanamente il codice di condotta dettato dalla sua divinità perde tutti gli incantesimi e i privilegi di classe, tranne le competenze nelle armature e negli scudi, e la competenza nelle armi semplici. 

Non può guadagnare ulteriori livelli come chierico di quella divinità fino a quando non :ref:`espia le sue colpe<inc_espiazione>`.

.. _domini_chierico:

-------------------
Domini
-------------------

.. figure:: Immagini/Chierico2.png
    :align: right
    :width: 400

.. contents:: Riassumendo:
    :local:

I chierici scelgono due domini tra quelli garantiti dalla propria divinità. I chierici senza una divinità possono scegliere due domini qualsiasi, ma la scelta deve essere approvata dal GM.

.. _dominio_acqua:

Acqua
-------------------
Manipolare l’acqua, la nebbia e il ghiaccio, evocare creature dell’acqua e resistere al freddo.

:Divinità: Gozreh, Pharasma

Dardo di ghiaccio (Mag)
*************************
:Tipo: Azione Standard - Attacco di contatto a distanza

Un dardo di ghiaccio scaturisce dal proprio dito, bersagliando qualunque nemico entro 9 metri. 

Il dardo infligge 1d6 danni da freddo + 0.5 * Liv chierico. 

Si può utilizzare questa capacità un numero di volte al giorno pari a 3 + Mod Sag.

Resistenza al freddo (Str)
*****************************
:Liv Min: 6

Si ottiene resistenza al freddo 10, che aumenta a 20 al 12° livello e ad immunità al
20°.

Incantesimi di Dominio
************************

#. :ref:`Foschia Occultante<inc_foschia_occultante>`
#. :ref:`Nube di Nebbia<inc_nube_di_nebbia>`
#. :ref:`Respirare Sott'Acqua<inc_respirare_sottacqua>`
#. :ref:`Controllare Acqua<inc_controllare_acqua>`
#. :ref:`Tempesta di Ghiaccio<inc_tempesta_di_ghiaccio>`
#. :ref:`Cono di Freddo<inc_cono_di_freddo>`
#. :ref:`Corpo Elementale IV<inc_corpo_elementale_IV>` (solo acqua)
#. :ref:`Orrido Avvizzimento<inc_orrido_avvizzimento>`
#. :ref:`Sciame Elementale<inc_sciame_elementale>` (solo con descrittore acqua)

.. _dominio_animale:

Animale
----------------------
Si può parlare con gli animali e renderli mansueti più facilmente. In più, :ref:`Conoscenze (natura)<conoscenze>` diventa un’abilità di classe.

:Divinità: Erastil, Gozreh

Parlare con gli animali (Mag)
*****************************
Si può parlare con gli animali, come per l’:ref:`incantesimo<inc_parlare_con_gli_animali>`, per 3 + Liv Chierico round al giorno.

Compagno animale (Str)
************************
:Liv Min: 4

Si ottiene il servizio di un compagno animale con un Liv Druido per il compagno di Liv Chierico –3. 

Incantesimi di Dominio
***********************
#. :ref:`Calmare Animali<inc_calmare_animali>`
#. :ref:`Blocca Animali<inc_blocca_animali>`
#. :ref:`Dominare Animali<inc_dominare_animali>`
#. :ref:`Evoca Alleato Naturale IV<inc_evoca_alleato_naturale_IV>` (solo animali)
#. :ref:`Forma Ferina III<inc_forma_ferina_III>`
#. :ref:`Guscio Anti-Vita<inc_guscio_anti_vita>`
#. :ref:`Forme Animali<inc_forme_animali>`
#. :ref:`Evoca Alleato Naturale VIII<inc_evoca_alleato_naturale_VIII>`
#. :ref:`Trasformazione<inc_trasformazione>`

.. _dominio_aria:

Aria
------------------
Si possono maneggiare fulmini, foschia e vento, avere a che fare con le creature dell’aria e resistere ai danni da elettricità.

:Divinità: Gozreh, Shelyn

Arco di Fulmini (Mag)
***********************
:Tipo: :ref:`Azione Standard<azione_standard>`

Si può liberare un arco di elettricità contro qualunque nemico entro 9 metri con un attacco di contatto a distanza. 

Infligge 1d6 danni da elettricità + 1 danno ogni due livelli da chierico posseduti e si può utilizzare 3 + Mod Sag volte al giorno.

Resistenza di Elettricità (Str)
********************************
:Liv Min: 6

Eesistenza all’elettricità 10, che aumenta a 20 al 12° livello.

Al 20° livello si ottiene immunità all’elettricità.

Incantesimi di Dominio
*************************
#. :ref:`Foschia Occultante<inc_foschia_occultante>`
#. :ref:`Muro di Vento<inc_muro_di_vento>`
#. :ref:`Forma Gassosa<inc_forma_gassosa>`
#. :ref:`Camminare nell'Aria<inc_camminare_nellaria>`
#. :ref:`Controllare Venti<inc_controllare_venti>`
#. :ref:`Catena di Fulmini<inc_catena_di_fulmini>`
#. :ref:`Corpo Elementale IV<inc_corpo_elementale_IV>` (solo aria)
#. :ref:`Turbine<inc_turbine>`
#. :ref:`Sciame Elementale<inc_sciame_elementale>` (solo con descrittore aria)

.. _dominio_artificio:

Artificio
-----------------------
Si possono riparare i danni agli oggetti, anche animati, e creare oggetti dal nulla.

:Divinità: Torag

Tocco dell’Artificiere (Mag)
*****************************
Si può lanciare :ref:`Riparare<inc_riparare>`, con il Liv Chierico come Liv Incantatore. 

Si possono danneggiare oggetti e costrutti con un attacco di contatto in mischia, causando 1d6 danni +1ogni due Liv Chierico. Questo attacco oltrepassa una quantità di riduzione del danno e di durezza pari al Liv Chierico e si può utilizzare 3 + Mod Sag volte al giorno.

Armi Danzanti (Sop)
*********************
:Liv Min: 8

Si può impartire ad un’arma toccata la capacità speciale :ref:`Danzante<armi_danzante>` per 4 round. 

Si può utilizzare questa capacità una volta al giorno all’8° livello e una volta in più ogni quattro livelli.

Incantesimi di Dominio
***********************
#. :ref:`Animare Corde<inc_animare_corde>`
#. :ref:`Scolpire Legno<inc_scolpire_legno>`
#. :ref:`Scolpire Pietra<inc_scolpire_pietra>`
#. :ref:`Creazione Minore<inc_creazione_minore>`
#. :ref:`Fabbricare<inc_fabbricare>`
#. :ref:`Creazione Maggiore<inc_creazione_maggiore>`
#. :ref:`Muro di Ferro<inc_muro_di_ferro>`
#. :ref:`Evocazioni Istantanee<inc_evocazioni_istantanee>`
#. :ref:`Sfera Prismatica<inc_sfera_prismatica>`

.. _dominio_bene:

Bene
--------------------
Si è impegnata la propria vita e l’anima alla bontà ed alla purezza.

:Divinità: Cayden Cailean, Desna, Erastil, Iomedae, Sarenrae, Shelyn, Torag

Tocco del Bene (Mag)
**********************
:Tipo: :ref:`Azione Standard<azione_standard>`

Si può toccare una creatura concedendole un bonus sacro ai tiri per colpire, alle prove di abilità, alle prove di caratteristica e ai TS pari a 0.5 * Liv Chierico per 1 round. 

Si può utilizzare 3 + Mod Sag volte al giorno.

Lancia Sacra (Sop)
********************
:Liv Min: 8

Si può infondere ad un’arma toccata la capacità speciale sacra per 0.5 * Liv Chierico round. 

Si può utilizzare una volta al giorno all’8° livello e una volta in più ogni quattro
livelli.

Incantesimi di Dominio
***********************
#. :ref:`Protezione dal Male<inc_protezione_dal_male>`
#. :ref:`Allineare Arma<inc_allineare_arma>` (solo bene)
#. :ref:`Cerchio Magico Contro il Male<inc_cerchio_magico_contro_il_male>`
#. :ref:`Punizione Sacra<inc_punizione_sacra>`
#. :ref:`Dissolvi il Male<inc_dissolvi_il_male>`
#. :ref:`Barriera di Lame<inc_barriera_di_lame>`
#. :ref:`Parola Sacra<inc_parola_sacra>`
#. :ref:`Aura Sacra<inc_aura_sacra>`
#. :ref:`Evoca mostri IX<inc_evoca_mostri_IX>` (solo bene)

.. figure:: Immagini/DivinitàChierico.png
    :align: center

.. _dominio_caos:

Caos
-----------------
Il proprio tocco infonde di caos la vita e le armi e rivela tutto ciò che è anarchico.


:Divinità: Calistria, Cayden Cailean, Desna, Gorum, Lamashtu, Rovagug

Tocco del Caos (Mag)
*********************
:Tipo: :ref:`Azione Standard<azione_standard>`

Si può infondere un bersaglio di energia caotica con un attacco di contatto in mischia. Per il prossimo round, ogni volta che il bersaglio tira un d20, deve tirare due volte e tenere il risultato meno favorevole. 

Si può utilizzare 3 + Mod Sag volte al giorno.

Lama del Caos (Sop)
*********************
:Liv Min: 8

Si può dare ad un’arma toccata la capacità speciale :ref:`Anarchica<armi_anarchica>` per 0.5 * Liv Chierico round.

Si può utilizzare una volta al giorno all’8° livello e più una ogni quattro livelli.

Incantesimi di Dominio
***********************
#. :ref:`Protezione dalla Legge<inc_protezione_dalla_legge>`
#. :ref:`Allineare Arma<inc_allineare_arma>` (solo caos)
#. :ref:`Cerchio Magico Contro la legge<inc_cerchio_magico_contro_la_legge>`
#. :ref:`Martello del Caos<inc_martello_del_caos>`
#. :ref:`Dissolvi la Legge<inc_dissolvi_la_legge>`
#. :ref:`Animare Oggetti<inc_animare_oggetti>`
#. :ref:`Parola del Caos<inc_parola_del_caos>`
#. :ref:`Mantello del Caos<inc_mantello_del_caos>`
#. :ref:`Evoca mostri IX<inc_evoca_mostri_IX>` (solo caos)

.. _dominio_charme:

Charme
--------------------
Si può confondere ed ingannare i nemici con un tocco o un sorriso e la propria bellezza e grazia sono divine.

:Divinità: Calistria, Cayden Cailean, Norgorber, Shelyn

Tocco Frastornante (Mag)
***************************
:Tipo: :ref:`Azione Standard<azione_standard>`

Si può indurre una creatura vivente a diventare frastornata per 1 round con un attacco di contatto in mischia per 3 + Mod Sag volte al giorno.

Le creature con più Dadi Vita del proprio livello da chierico non subiscono alcun effetto.

Sorriso Affascinante (Mag)
****************************
:Tipo: Azione Veloce
:Liv Min: 8

Si può lanciare charme su persone come azione veloce, con CD = 10 + 0.5 Liv Chierico + Mod Sag, ma si può avere soltanto una creatura affascinata alla volta in questo modo. 

Il numero totale di round di questo effetto al giorno è pari al proprio livello da chierico, che non devono essere consecutivi e si può interrompere lo charme in qualunque momento come azione gratuita. 

Ogni tentativo di utilizzare questa capacità consuma 1 round della sua durata, indipendentemente dal fatto che la creatura riesca a superare o meno il TS per resistere all’effetto.

Incantesimi di Dominio
***********************
#. :ref:`Charme su Persone<inc_charme_su_persone>`
#. :ref:`Calmare Emozioni<inc_calmare_emozioni>`
#. :ref:`Suggestione<inc_suggestione>`
#. :ref:`Eroismo<inc_eroismo>`
#. :ref:`Charme sui Mostri<inc_charme_sui_mostri>`
#. :ref:`Costrizione/Cerca<inc_costrizionecerca>`
#. :ref:`Demenza<inc_demenza>`
#. :ref:`Esigere<inc_esigere>`
#. :ref:`Dominare Mostri<inc_dominare_mostri>`

.. _dominio_comunità:

Comunità
-----------------------
Il proprio tocco può curare ferite e la presenza del chierico infonde unità e rafforza i legami emozionali.

:Divinità: Erastil

Tocco Calmante (Mag)
***************************
:Tipo: :ref:`Azione Standard<azione_standard>`

Si può toccare una creatura per curarle 1d6 + 1 * Liv Chierico danni non letali e rimuovendo le condizioni affaticato, infermo e scosso. 

Si può utilizzare 3 + Mod Sag volte al giorno.

Unità (Mag)
****************************
:Tipo: :ref:`Azione Immediata<azione_immediata>`
:Liv Min: 8

Si può permettere ai propri alleate di usare il proprio TS anziché il loro ogni volta che un incantesimo o un effetto vi designa come bersaglio entro 9 metri, ma ogni alleato deve decidere individualmente prima di effettuare i tiri.

Si può utilizzare una volta al giorno all’8° livello più una ogni altri quattro livelli da chierico.

Incantesimi di Dominio
***********************
#. :ref:`Benedizione<inc_benedizione>`
#. :ref:`Scudo su Altri<inc_scudo_su_altri>`
#. :ref:`Preghiera<inc_preghiera>`
#. :ref:`Status<inc_status>`
#. :ref:`Legame Telepatico<inc_legame_telepatico>`
#. :ref:`Banchetto degli Eroi<inc_banchetto_degli_eroi>`
#. :ref:`Rifugio<inc_rifugio>`
#. :ref:`Cura Ferite Critiche di Massa<inc_cura_ferite_critiche_di_massa>`
#. :ref:`Miracolo<inc_miracolo>`

.. _dominio_conoscenza:

Conoscenza
-------------------------
Si è saggi eruditi di miti e leggende e tutte le abilità di :ref:`Conoscenze<conoscenze>` sono considerate abilità di classe.

:Divinità: Calistria, Irori, Nethys, Norgorber, Pharasma

Custode di Sapere (Mag)
***************************
:Tipo: :ref:`Attacco di Contatto<azione_attacco_di_contatto_in_mischia>`

Si può toccare una creatura per apprenderne capacità e punti deboli ottenendo le informazioni volute come se si fosse superata una prova di :ref:`Conoscenze<conoscenze>` con 15 + Liv Chierico + Mod Sag.

Visione Remota (Mag)
****************************
:Tipo: :ref:`Azione Immediata<azione_immediata>`
:Liv Min: 6

Si può utilizzare :ref:`Chiaroudienza/Chiaroveggenza<inc_chiaroveggenza>` come
capacità magica usando il Liv Chierico come LI e per Liv Chierico volte al giorno.

Incantesimi di Dominio
***********************
#. :ref:`Comprensione Dei Linguaggi<inc_comprensione_dei_linguaggi>`
#. :ref:`Individuazione dei Pensieri<inc_individuazione_dei_pensieri>`
#. :ref:`Parlare con i Morti<inc_parlare_con_i_morti>`
#. :ref:`Divinazione<inc_divinazione>`
#. :ref:`Visione del Vero<inc_visione_del_vero>`
#. :ref:`Scopri il Percorso<inc_scopri_il_percorso>`
#. :ref:`Conoscenze delle Leggende<inc_conoscenze_delle_leggende>`
#. :ref:`Rivela Locazioni<inc_rivela_locazioni>`
#. :ref:`Previsione<inc_previsione>`

Distruzione
-------------------------
Ci si crogiola nella rovina e nella devastazione e si possono sferrare attacchi particolarmente distruttivi.

:Divinità: Gorum, Nethys, Rovagug, Zon-Kuthon

Punizione Distruttiva (Sop)
****************************
Capacità di effettuare un singolo attacco in mischia con bonus morale ai tiri per il
danno pari a 0.5 * Liv Chierico (minimo 1), dichiarandola prima dell'attacco.

L'utilizzo giornaliero è 3 + Mod Sag.

Aura Distruttiva (Sop)
****************************
:Liv Min: 8

Si può emettere un’aura di distruzione di 9 metri per *Liv Chierico* round al giorno. Tutti gli attacchi contro i bersagli nell’area (compreso se stessi) ottengono un bonus morale al danno do 0.5 * Liv Chierico e tutte le minacce di critico sono confermate automaticamente.

Incantesimi di Dominio
***********************
#. :ref:`Colpo Accurato<inc_colpo_accurato>`
#. :ref:`Frantumare<inc_frantumare>`
#. :ref:`Ira<inc_ira>`
#. :ref:`Infliggi Ferite Critiche<inc_infliggi_ferite_critiche>`
#. :ref:`Grido<inc_grido>`
#. :ref:`Ferire<inc_ferire>`
#. :ref:`Disintegrazione<inc_disintegrazione>`
#. :ref:`Terremoto<inc_terremoto>`
#. :ref:`Implosione<inc_implosione>`

Follia
-------------------------
Si abbraccia la follia che si annida nel profondo del cuore e si può liberarla per condurre i nemici alla pazzia o per sacrificare determinate capacità per procurare loro dolore.

:Divinità: Lamashtu

Visione di Follia (Mag)
****************************
:Tipo: :ref:`Attacco di Contatto in Mischia<azione_attacco_di_contatto_in_mischia>`

Si può impartire ad una creatura una visione di follia scegliendo tra: tiro per colpire, TS o
prove di abilità. Il bersaglio riceve un bonus alla scelta pari a 0.5 Liv Chierico (minimo +1) e un'uguale penalità agli altri due tipi di tiri. 

Questo effetto svanisce dopo 3 round e si può utilizzare 3 + Mod Sag volte al giorno.

Aura di Follia (Sop)
****************************
:Liv Min: 8

Si può emettere un’aura di follia di 9 metri per Liv Chierico round al giorno, non necessariamente consecutivi. 

I nemici all’interno di questa aura sono influenzati da confusione a meno che non superino un TS su Volontà con **CD = 10 + 0.5 Liv Chierico + Mod Sag**. L’effetto di confusione termina immediatamente quando la creatura lascia l’area o l’aura finisce. 

.. Warning::
    Le creature che superano il TS sono immuni a questa aura per 24 ore.

Incantesimi di Dominio
***********************
#. :ref:`Confusione Inferiore<inc_confusione_inferiore>`
#. :ref:`Tocco di Idiozia<inc_tocco_di_idiozia>`
#. :ref:`Ira<inc_ira>`
#. :ref:`Confusione<inc_confusione>`
#. :ref:`Incubo<inc_incubo>`
#. :ref:`Allucinazione Mortale<inc_allucinazione_mortale>`
#. :ref:`Demenza<inc_demenza>`
#. :ref:`Trama Scintillante<inc_trama_scintillante>`
#. :ref:`Fatale<inc_fatale>`

Fortuna
-------------------------
Si è infusi con la fortuna e la propria presenza può diffondere la buona sorte.

:Divinità: Calistria, Desna, Shelyn

Briciolo di Fortuna (Mag)
****************************
:Tipo: :ref:`Azione Standard<azione_standard>`

Si può toccare una creatura consenziente, donandole un po’ di fortuna. Per tutto il round successivo, ogni volta che il bersaglio deve tirare un d20, può effettuare due tiri e tenere il risultato più favorevole. 

Si può utilizzare questa capacità per 3 + Mod Sag volte al giorno.

Buona Fortuna (Str)
****************************
:Liv Min: 6

Una volta al giorno si può ritirare qualsiasi tiro appena effettuato, prima che l'effetto ne sia rivelato, per tenerne il risultato anche se peggiore del tiro iniziale. 

Se ne acquisisce un utilizzo giornaliero ogni 6 livelli.

Incantesimi di Dominio
***********************
#. :ref:`Colpo Accurato<inc_colpo_accurato>`
#. :ref:`Aiuto<inc_aiuto>`
#. :ref:`Protezione dall'Energia<inc_protezione_dallenergia>`
#. :ref:`Libertà di Movimento<inc_libertà_di_movimento>`
#. :ref:`Spezzare Incantamento<inc_spezzare_incantamento>`
#. :ref:`Fuorviare<inc_fuorviare>`
#. :ref:`Riflettere Incantesimo<inc_riflettere_incantesimo>`
#. :ref:`Momento di Prescienza<inc_momento_di_prescienza>`
#. :ref:`Miracolo<inc_miracolo>`

Forza
-------------------------
Nella forza e nel vigore risiede la verità; la fede che si segue dona incredibile potere.

:Divinità: Cayden, Cailean, Gorum, Irori, Lamashtu, Urgathoa

Impulso di Forza (Mag)
****************************
:Tipo: :ref:`Azione Standard<azione_standard>`

Si può toccare una creatura per infonderle una grande forza. 

Per 1 round, il bersaglio guadagna un potenziamento di 0.5 Liv Chierico (minimo +1) a attacchi in mischia, prove di manovre in combattimento che si basano su Forza, prove di abilità basate su Forza e prove di Forza. 

Si può utilizzare questa capacità 3 + Mod Sag volte al giorno.

Forza degli Déi (Sop)
****************************
:Liv Min: 8

Si può aggiungere il Liv Chierico come potenziamento al proprio punteggio di Forza per Liv Chierico round al giorno, non necessariamente consecutivi. 

Questo bonus si applica soltanto alle prove di Forza e alle prove di abilità basate sulla Forza.

Incantesimi di Dominio
***********************
#. :ref:`Ingrandire Persone<inc_ingrandire_persone>`
#. :ref:`Forza del Toro<inc_forza_del_toro>`
#. :ref:`Veste Magica<inc_veste_magica>`
#. :ref:`Immunità agli Incantesimi<inc_immunità_agli_incantesimi>`
#. :ref:`Giusto Potere<inc_giusto_potere>`
#. :ref:`Pelle di Pietra<inc_pelle_di_pietra>`
#. :ref:`Mano Stringente<inc_mano_stringente>`
#. :ref:`Pugno Serrato<inc_pugno_serrato>`
#. :ref:`Mano Stritolatrice<inc_mano_stritolatrice>`

.. _dominio_fuoco:

Fuoco
-------------------------
Si può richiamare il fuoco, comandare le creature infernali e la propria carne non brucia.

:Divinità: Asmodeus, Sarenrae

Dardo di Fuoco (Mag)
****************************
:Tipo: :ref:`Azione Standard<azione_standard>`

Si può liberare un dardo di fuoco divino dalla mano aperta e designare come bersaglio qualunque nemico entro 9 metri con un :ref:`Attacco di Contatto a Distanza<azione_attacco_di_contatto_a_distanza>`. Se si colpisce il nemico, il dardo infligge 1d6 danni da fuoco + 0.5 Liv Chierico.

Si può utilizzare questa capacità 3 + Mod Sag volte al giorno.

Resistenza al Fuoco (Str)
****************************
:Liv Min: 6

Si ottiene resistenza al fuoco 10, che aumenta a 20 al 12° livello e diventa immunità al 20°.

Incantesimi di Dominio
***********************
#. :ref:`Mani Brucianti<inc_mani_brucianti>`
#. :ref:`Produrre Fiamma<inc_produrre_fiamma>`
#. :ref:`Palla di Fuoco<inc_palla_di_fuoco>`
#. :ref:`Muro di Fuoco<inc_muro_di_fuoco>`
#. :ref:`Scudo di Fuoco<inc_scudo_di_fuoco>`
#. :ref:`Semi di Fuoco<inc_semi_di_fuoco>`
#. :ref:`Corpo Elementale IV<inc_corpo_elementale_IV>` (solo fuoco)
#. :ref:`Nube Incendiaria<inc_nube_incendiaria>`
#. :ref:`Sciame Elementale<inc_sciame_elementale>` (solo con descrittore fuoco)


Incantesimi di dominio: 1°—scudo della fede, 2°— benedire
arma, 3°—luce incandescente, 4°—punizione sacra, 5°— giusto
potere, 6°—non morto a morto, 7°— spada sacra, 8°— aura sacra, 9°—portale.

Gloria
-------------------------
Si è infusi con la gloria del divino e si è nemici giurati dei non morti. 

Quando si :ref:`Incanala Energia Positiva<incanalare_energia>` per ferire i non morti, la CD del TS per dimezzare il danno aumenta di 2.

:Divinità: Gorum, Iomedae, Sarenrae

Tocco di Gloria (Mag)
****************************
:Tipo: :ref:`Azione Standard<azione_standard>`

Si può infondere la mano del bagliore della radiosità divina che toccando una creatura le concede un bonus paro a Liv Chierico ad una singola prova di abilità basata su Carisma o ad una prova di Carisma. Il potenziamento dura 1 ora o finché la creatura sceglie di applicare il bonus
ad un tiro. 

Si può utilizzare questa capacità 3 + Mod Sag volte al giorno.

Presenza Divina (Sop)
****************************
:Liv Min: 8

Si può emettere un’aura di presenza divina entro 9 metri per Liv Chierico round al giorno, non necessariamente consecutivi.

Tutti gli alleati all’interno di quest’aura sono considerati sotto gli effetti di un incantesimo :ref:`Santuario<inc_santuario>` con **CD = 10 + 0.5 Liv Chierico + Mod Sag**. 

.. Note:: 
    L’attivazione della questa capacità è un’:ref:`Azione Standard<azione_standard>` e se un alleato lascia l’area o effettua un attacco, il suo effetto termina per quell’alleato.

    Se si effettua un attacco, l’effetto termina per sè ed i propri alleati.

Incantesimi di Dominio
***********************
#. :ref:`Scudo della Fede<inc_scudo_della_fede>`
#. :ref:`Benedire Arma<inc_benedire_arma>`
#. :ref:`Luce Incandescente<inc_luce_incandescente>`
#. :ref:`Punizione Sacra<inc_punizione_sacra>`
#. :ref:`Giusto Potere<inc_giusto_potere>`
#. :ref:`Non Morto a Morto<inc_non_morto_a_morto>`
#. :ref:`Spada Sacra<inc_spada_sacra>`
#. :ref:`Aura Sacra<inc_aura_sacra>`
#. :ref:`Portale<inc_portale>`

Guarigione
-------------------------
Il proprio tocco allontana il dolore e la morte e la vostra magia curativa è particolarmente vitale e potente.

:Divinità: Irori, Pharasma, Sarenrae

Allontanare la Morte (Mag) 
****************************
:Tipo: :ref:`Azione Standard<azione_standard>`

Si può toccare una creatura vivente con meno di 0 punti ferita curandole 1d4 + 0.5 * Liv Chierico danni. 

Si può utilizzare questa capacità 3 + Mod Sag volte al giorno.

Benedizione del Guaritore (Sop)
*******************************
:Liv Min: 6

Tutti i propri incantesimi curativi sono considerati potenziati, aumentando la quantità di danno curato della metà (+50%).

Ciò non si applica ai danni inflitti ai non morti con un incantesimo di cura. 

.. Warning::
    Non è cumulabile con :ref:`Incantesimi Potenziati<tal_incantesimi_potenziati>`.
 
Incantesimi di Dominio
***********************
#. :ref:`Cure Ferite Leggere<inc_cura_ferite_leggere>`
#. :ref:`Cura Ferite Moderato<inc_cura_ferite_moderate>`
#. :ref:`Cura Ferite Gravi<inc_cura_ferite_gravi>`
#. :ref:`Cura Ferite Critiche<inc_cura_ferite_critiche>`
#. :ref:`Respiro di Vita<inc_respiro_di_vita>`
#. :ref:`Guarigione<inc_guarigione>`
#. :ref:`Rigenerazione<inc_rigenerazione>`
#. :ref:`Cura Ferite Critiche di Massa<inc_cura_ferite_critiche_di_massa>`
#. :ref:`Guarigione di Massa<inc_guarigione_di_massa>` 

Guerra
-------------------------
Si è crociati del proprio dio, sempre pronti e desiderosi di combattere per difendere la propria fede.

:Divinità: Gorum, Iomedae, Rovagug, Urgathoa

Ira della Battaglia (Mag)
****************************
:Tipo: :ref:`Azione Standard<azione_standard>`

Si può toccare una creatura per concederle un bonus ai tiri per i danni in mischia di 0.5 * Liv Chierico (minimo +1) per 1 round.

Si può utilizzare questa capacità 3 + Mod Sag volte al giorno.

Maestro di Armi (Sop)
*******************************
:Liv Min: 8

Si può usare un :ref:`Talento di Combattimento<talenti_di_combattimento>` per un numero di round al giorno pari al Liv Chierico, non necessariamente consecutivi.

Si può cambiare il talento scelto ogni volta che si utilizza questa capacità, ma bisogna sempre soddisfarne i prerequisiti.

Incantesimi di Dominio
***********************
#. :ref:`Arma Magica<inc_arma_magica>`
#. :ref:`Arma Spirituale<inc_arma_spirituale>`
#. :ref:`Veste Magica<inc_veste_magica>`
#. :ref:`Potere Divino<inc_potere_divino>`
#. :ref:`Colpo Infuocato<inc_colpo_infuocato>`
#. :ref:`Barriera di Lame<inc_barriera_di_lame>`
#. :ref:`Parola del Potere: Accecare<inc_parola_del_potere_accecare>`
#. :ref:`Parola del Potere: Stordire<inc_parola_del_potere_stordire>`
#. :ref:`Parola del Potere: Uccidere<inc_parola_del_potere_uccidere>`

Inganno
-------------------------
Si è maestri delle illusioni e degli inganni.

:ref:`Camuffare<camuffare>`, :ref:`Furtività<furtività>` e :ref:`Raggirare<raggirare>` sono abilità di classe.

:Divinità: Asmodeus, Calistria, Lamashtu, Norgorber

Imitatore Pedissequo (Mag)
****************************
:Tipo: :ref:`Azione di Movimento<azione_movimento>`

Si può creare un doppio illusorio di se stessi che funziona come una singola :ref:`Immagine Speculare<inc_immagine_speculare>` e dura Liv Chierico round, o finché il duplicato non viene dissolto o distrutto. 

.. Note:: 
    Si può avere solo un *Imitatore Pedissequo* alla volta e non può essere cumulato con :ref:`Immagine Speculare<inc_immagine_speculare>`.

Si può utilizzare questa capacità 3 + Mod Sag volte al giorno.

Illusione Prodigiosa (Mag)
*******************************
:Liv Min: 8

Si può creare un’illusione che nasconde la propria presenza e quella di ogni alleato entro 9 metri per 1 round per Liv Chierico, non necessariamente consecutivi. Per il resto funziona come :ref:`Velo<inc_velo>`.

Incantesimi di Dominio
***********************
#. :ref:`Camuffare Sè Stesso<inc_camuffare_sè_stesso>`
#. :ref:`Invisibilità<inc_invisibilità>`
#. :ref:`Anti-Individuazione<inc_anti_individuazione>`
#. :ref:`Confusione<inc_confusione>`
#. :ref:`Visione Falsa<inc_visione_falsa>`
#. :ref:`Fuorviare<inc_fuorviare>`
#. :ref:`Schermo<inc_schermo>`
#. :ref:`Invisibilità di Massa<inc_invisibilità_di_massa>`
#. :ref:`Fermare il Tempo<inc_fermare_il_tempo>`

Legge
-------------------------
Si segue un rigoroso codice di legge per raggiungere l’illuminazione.

:Divinità: Abadar, Asmodeus, Erastil, Iomedae, Irori, Torag, Zon-Kuthon

Tocco della Legge (Mag)
****************************
:Tipo: :ref:`Azione Standard<azione_standard>`

Si può toccare una creatura consenziente infondendogli il potere dell’ordine divino e permettendole di considerare tutti i tiri per colpire, le prove di abilità, le prove di caratteristica e i TS per 1 round come un 11 naturale su un d20.

Si può utilizzare questa capacità 3 + Mod Sag volte al giorno.

Bastone dell’Ordine (Sop)
*******************************
:Liv Min: 8

Una volta al giorno si può infondere in un’arma toccata la capacità speciale :ref:`Assiomatica<armi_assiomatica>` per 0.5 * Liv Chierico round. Ogni 4 livelli se ne guadagna un utilizzo extra al giorno.

Incantesimi di Dominio
***********************
#. :ref:`Protezione dal Caos<inc_protezione_dal_caos>`
#. :ref:`Allineare Arma<inc_allineare_arma>` (solo legge)
#. :ref:`Cerchio Magico Contro il Caos<inc_cerchio_magico_contro_il_caos>`
#. :ref:`Ira dell'Ordine<inc_ira_dellordine>`
#. :ref:`Dissolvi il Caos<inc_dissolvi_il_caos>`
#. :ref:`Blocca Mostri<inc_blocca_mostri>`
#. :ref:`Dettame<inc_dettame>`
#. :ref:`Scudo della Legge<inc_scudo_della_legge>`
#. :ref:`Evoca Mostri IX<inc_evoca_mostri_IX>` (solo descrittore legge)

Incantesimi di dominio: 1°—rimuovi paura, 2°—rimuovi
paralisi, 3°—rimuovi maledizione, 4°—libertà di movimento,
5°—spezzare incantamento, 6°—dissolvi magie superiore, 7°—
rifugio, 8°—vuoto mentale, 9°—libertà.

Libertà
-------------------------
Si è spiriti liberi e nemici letali contro tutti coloro che schiavizzano ed opprimono.

:Divinità: Desna

Liberazione (Sop)
****************************
Si ha la capacità di ignorare gli impedimenti alla propria mobilità. 

Per Liv Chierico round al giorno, non necessariamente consecutivi, ci si può muovere normalmente a prescindere da effetti magici che impediscono il movimento, come se si fosse sotto l’influenza di :ref:`Libertà di Movimento<inc_libertà_di_movimento>`. Questo effetto si applica automaticamente non appena si verifica l’impedimento. 

Chiamata della Libertà (Sop)
*******************************
:Liv Min: 8

Si può emettere un’aura di libertà di 9 metri per Liv Chierico round al giorno, non necessariamente consecutivi. 

Gli alleati all’interno di quest’aura non sono influenzati dalle condizioni confuso, in lotta, spaventato, in preda al panico, paralizzato, immobilizzato o scosso. Questa aura sopprime
soltanto questi effetti, i quali ritornano una volta che una creatura lascia l’aura o quando l’aura termina, se applicabile.

Incantesimi di Dominio
***********************
#. :ref:`Rimuovi Paura<inc_rimuovi_paura>`
#. :ref:`Rimuovi Paralisi<inc_rimuovi_paralisi>`
#. :ref:`Rimuovi Maledizione<inc_rimuovi_maledizione>`
#. :ref:`Libertà di Movimento<inc_libertà_di_movimento>`
#. :ref:`Spezzare Incantamento<inc_spezzare_incantamento>`
#. :ref:`Dissolvi Magie Superiore<inc_dissolvi_magie_superiore>`
#. :ref:`Rifugio<inc_rifugio>`
#. :ref:`Vuoto Mentale<inc_vuoto_mentale>`
#. :ref:`Libertà<inc_libertà>`

Magia
-------------------------
Si è dediti a tutte le cose mistiche e si riesce a vedere il divino nella purezza della magia.

:Divinità: Asmodeus, Nethys, Urgathoa

Mano dell’Accolito (Sop)
****************************
Si può indurre la propria arma da mischia a volare dalla vostra presa e colpire un nemico prima del suo immediato ritorno in mano. 

Come :ref:`Azione Standard<azione_standard>` si può effettuare un singolo attacco con un’arma da mischia ad una distanza di 9 metri che viene trattato come :ref:`Attacco a Distanza con un’Arma da Lancio<azione_attacco_a_distanza_con_arma_da_lancio>`, salvo aggiungere il Mod Sag al
tiro di attacco anziché il Mod Des.

.. Note::
    Questa capacità non può essere utilizzata per effettuare una manovra in combattimento.

Si può utilizzare questa capacità 3 + Mod Sag volte al giorno.

Tocco Dissolutore (Mag)
*******************************
:Liv Min: 8

Una volta al giorno, si può utilizzare un effetto designato di :ref:`Dissolvi Magie<inc_dissolvi_magie>` con un :ref:`Attacco di Contatto in Mischia<azione_attacco_di_contatto_in_mischia>`. 

Se ne acquisisce un utilizzo giornaliero aggiuntivo ogni 4 livelli.

Incantesimi di Dominio
***********************
#. :ref:`Identificare<inc_identificare>`
#. :ref:`Bocca Magica<inc_bocca_magica>`
#. :ref:`Dissolvi Magie<inc_dissolvi_magie>`
#. :ref:`Infondere Capacità Magiche<inc_infondere_capacità_magiche>`
#. :ref:`Resistenza agli Incantesimi<inc_resistenza_agli_incantesimi>`
#. :ref:`Campo Anti-Magia<inc_campo_anti_magia>`
#. :ref:`Riflettere Incantesimo<inc_riflettere_incantesimo>`
#. :ref:`Protezione dagli Incantesimi<inc_protezione_dagli_incantesimi>`
#. :ref:`Disgiunzione Arcana<inc_disgiunzione_arcana>`

Male
-------------------------
Si è sinistri e crudeli, e completamente dediti al male.

:Divinità: Asmodeus, Lamashtu, Norgorber, Rovagug, Urgathoa, Zon-Kuthon

Tocco del Male (Mag)
****************************
:Tipo: :ref:`Azione Standard<azione_standard>`

Si può rendere inferma una creatura con un :ref:`Attacco di Contatto in Mischia<azione_attacco_di_contatto_in_mischia>`.

.. Note::
    Le creature inferme per questo tocco contano come buone allo scopo degli incantesimi con il descrittore male.

Questa capacità dura 0.5 * Liv Chierico round (minimo 1) e si può utilizzare 3 + Mod Sag volte al giorno.

Falce del Male (Sop)
*******************************
:Liv Min: 8

Una volta al giorno, si può infondere ad un’arma toccata la capacità speciale :ref:`Sacrilega<armi_sacrilega>` per un numero di round pari a 0.5 * Liv Chierico.

Se ne acquisisce un utilizzo giornaliero aggiuntivo ogni 4 livelli.

Incantesimi di Dominio
***********************
#. :ref:`Protezione dal Bene<inc_protezione_dal_bene>`
#. :ref:`Allineare Arma<inc_allineare_arma>` (solo malvagio)
#. :ref:`Cerchio Magico contro il Bene<inc_cerchio_magico_contro_il_bene>`
#. :ref:`Influenza Sacrilega<inc_influenza_sacrilega>`
#. :ref:`Dissolvi il Bene<inc_dissolvi_il_bene>`
#. :ref:`Creare Non Morti<inc_creare_non_morti>`
#. :ref:`Blasfemia<inc_blasfemia>`
#. :ref:`Aura Sacrilega<inc_aura_sacrilega>`
#. :ref:`Evoca Mostri IX<inc_evoca_mostri_IX>` (solo descrittore male)

Abbraccio della morte (Str): All’8° livello, si può curare invece
che subire danno dall’energia negativa incanalata. Se l’energia negativa incanalata ha come bersaglio i non morti, è possibile curarsi i punti ferita come i non morti nell’area.
Incantesimi di dominio: 1°—incuti paura, 2°—rintocco di
morte, 3°—animare morti, 4°—interdizione alla morte, 5°— distruggere viventi, 6°—creare non morti, 7°—distruzione, 8°—creare non morti superiori, 9°—lamento della banshee.

Morte
-------------------------
Si può causare il sanguinamento delle creature viventi, e trovare conforto in presenza dei morti.

:Divinità: Norgorber, Pharasma, Urgathoa, Zon-Kuthon

Tocco Sanguinante (Mag)
****************************
:Tipo: :ref:`Azione Standard<azione_standard>`

Con un :ref:`Attacco di Contatto in Mischia<azione_attacco_di_contatto_in_mischia>` si può infliggere ad una creatura vivente 1d6 danni per round. 

L'effetto persiste per 0.5 * Liv Chierico round (minimo 1) o finché il sanguinamento non viene fermato con una prova di :ref:`Guarire<guarire>` con CD 15 o con qualsiasi incantesimo o effetto che curi i danni.

Si può utilizzare 3 + Mod Sag volte al giorno.

Falce del Male (Sop)
*******************************
:Liv Min: 8

Si può curare invece che subire danno dall’energia negativa incanalata. 

Se l’energia negativa incanalata ha come bersaglio i non morti, è possibile curarsi i punti ferita come i non morti nell’area.

Incantesimi di Dominio
***********************
#. :ref:`Incuti Paura<inc_incuti_paura>`
#. :ref:`Rintocco di Morte<inc_rintocco_di_morte>`
#. :ref:`Animare Morti<inc_animare_morti>`
#. :ref:`Interdizione alla Morte<inc_interdizione_alla_morte>`
#. :ref:`Distruggere Viventi<inc_distruggere_viventi>`
#. :ref:`Creare Non Morti<inc_creare_non_morti>`
#. :ref:`Distruzione<inc_distruzione>`
#. :ref:`Creare Non Morti Superiori<inc_creare_non_morti_superiori>`
#. :ref:`Lamento della Banshee<inc_lamento_della_banshee>`

Autorità (Str): All’8° livello, si riceve Autorità come talento
bonus. Inoltre, si ottiene bonus +2 al proprio valore di
Autorità finché si sostengono i principi della propria divinità
(o del concetto divino se non si venera una divinità).
Incantesimi di dominio: 1°—favore divino, 2°—estasiare, 3°—veste magica, 4°—rivela bugie, 5°—comando superiore,
6°—costrizione/cerca, 7°—repulsione, 8°—esigere, 9°—tempesta
di vendetta.

Nobiltà
-------------------------
Si è capi eccezionali, un’ispirazione per tutti quelli che seguono gli insegnamenti della fede.

:Divinità: Abadar

Parola d’Ispirazione (Mag)
****************************
:Tipo: :ref:`Azione Standard<azione_standard>`

Si può dire una parola d’ispirazione ad una creatura entro 9 metri. 

Quella creatura riceve bonus morale +2 ai tiri per colpire, alle prove di abilità, alle prove di caratteristica e ai TS per 0.5 * Liv Chierico round (minimo 1).

Si può utilizzare 3 + Mod Sag volte al giorno.

Autorità (Str)
*******************************
:Liv Min: 8

Si riceve :ref:`Autorità<tal_autorità>` come talento bonus e +2 al proprio valore di :ref:`Autorità<tal_autorità>` finché si sostengono i principi della propria divinità (o del concetto divino se non si venera una divinità).

Incantesimi di Dominio
***********************
#. :ref:`Favore Divino<inc_favore_divino>`
#. :ref:`Estasiare<inc_estasiare>`
#. :ref:`Veste Magica<inc_veste_magica>`
#. :ref:`Rivela Bugie<inc_rivela_bugie>`
#. :ref:`Comando Superiore<inc_comando_superiore>`
#. :ref:`Costrizione/Cerca<inc_costrizionecerca>`
#. :ref:`Repulsione<inc_repulsione>`
#. :ref:`Esigere<inc_esigere>`
#. :ref:`Tempesta di Vendetta<inc_tempesta_di_vendetta>`

: All’8° livello, 
Incantesimi di dominio: 1°—foschia occultante, 2°—cecità/sordità, 1°—oscurità profonda, 4°—ombra di una evocazione,
5°—evoca mostri V (evoca 1d3 ombre), 6°—camminare nelle
ombre, 7°—parola del potere: accecare, 8°—ombra di una invocazione superiore, 9°—ombre.

Oscurità
-------------------------
Si possono manipolare le ombre e l’oscurità e si riceve :ref:`Combattere alla Cieca<tal_combattere_alla_cieca>` come talento bonus.

:Divinità: Zon-Kuthon

Tocco dell’Oscurità (Mag)
****************************
:Tipo: :ref:`Azione Standard<azione_standard>`

Con un :ref:`Attacco di Contatto in Mischia<azione_attacco_di_contatto_in_mischia>` si può provocare nella visione di una creatura un ammasso di ombre e di oscurità. 

La creatura toccata considera tutte le altre creature come se avessero :ref:`Occultamento<occultamento>` ed i suoi attacchi subiscono una probabilità del 20% di mancare il bersaglio. 

L'effetto dura 0.5 * Liv Chierico round (minimo 1) e si può utilizzare 3 + Mod Sag volte al giorno.

Occhi dell’Oscurità (Sop)
*******************************
:Liv Min: 8

Per 0.5 * Liv Chierico round al giorno, la propria visione non è alterata dalle condizioni di luce, neppure nell’oscurità assoluta e in quella magica. Questi round non devono necessariamente essere consecutivi.

Incantesimi di Dominio
***********************
#. :ref:`Foschia Occultante<inc_foschia_occultante>`
#. :ref:`Cecità/Sordità<inc_cecitàsordità>`
#. :ref:`Oscurità Profonda<inc_oscurità_profonda>`
#. :ref:`Ombra di una Evocazione<inc_ombra_di_una_evocazione>`
#. :ref:`Evoca Mostri V<inc_evoca_mostri_V>` (evoca 1d3 ombre)
#. :ref:`Camminare nelle Ombre<inc_camminare_nelle_ombre>`
#. :ref:`Parola del Potere: Accecare<inc_parola_del_potere_accecare>`
#. :ref:`Ombra di una Invocazione Superiore<inc_ombra_di_una_invocazione_superiore>`
#. :ref:`Ombre<inc_ombre>`

Protezione
-------------------------
La fede è la più grande sorgente di protezione e la si può utilizzare per difendere gli altri.

Si riceve bonus di resistenza +1 ai TS che aumenta di +1 ogni 5 livelli posseduti.

:Divinità: Abadar, Nethys, Shelyn, Torag

Tocco Resistente (Mag)
****************************
:Tipo: :ref:`Azione Standard<azione_standard>`

Si può trasferire per 1 minuto il proprio bonus di resistenza concesso dal *Dominio di Protezione* a un alleato toccandolo. In questo tempo si perde tale bonus.

Si può utilizzare 3 + Mod Sag volte al giorno.

Aura di Protezione (Sop)
*******************************
:Liv Min: 8

Si può emettere un’aura di protezione di 9 metri per Liv Chierico round al giorno, non necessariamente consecutivi. 

Gli alleati (e se stessi) all’interno dell’aura ottengono bonus di deviazione +1 alla CA e resistenza 5 a tutti gli elementi (acido, freddo, elettricità, fuoco e sonoro). Il bonus di deviazione aumenta di +1 ogni altri 4 livelli Chierico. 

Al 14° livello, la resistenza agli elementi aumenta a 10.

Incantesimi di Dominio
***********************
#. :ref:`Santuario<inc_santuario>`
#. :ref:`Protezione dall'energia<inc_protezione_dallenergia>`
#. :ref:`Scudo su Altri<inc_scudo_su_altri>`
#. :ref:`Immunità agli Incantesimi<inc_immunità_agli_incantesimi>`
#. :ref:`Resistenza agli Incantesimi<inc_resistenza_agli_incantesimi>`
#. :ref:`Campo Anti-Magia<inc_campo_anti_magia>`
#. :ref:`Repulsione<inc_repulsione>`
#. :ref:`Vuoto Mentale<inc_vuoto_mentale>`
#. :ref:`Sfera Prismatica<inc_sfera_prismatica>`

Riposo
-------------------------
Si considera la morte non come qualcosa da temere, ma come riposo e premio definitivo per una vita ben spesa. La corruzione della non morte è una perversione di quello che è giudicato di supremo valore.

:Divinità: Pharasma

Riposo Delicato (Mag)
****************************
:Tipo: :ref:`Azione Standard<azione_standard>`

Il proprio tocco può infondere in una creatura vivente un effetto di letargia, inducendola a
diventare barcollante per 1 round con un :ref:`Attacco di Contatto in Mischia<azione_attacco_di_contatto_in_mischia>`. Se si tocca una creatura vivente barcollante, quella creatura cade invece addormentata per 1 round. 

Sulle creature non morte la durata aumenta a Mod Sag round.

Si può utilizzare 3 + Mod Sag volte al giorno.

Barriera contro la Morte (Sop)
*******************************
:Liv Min: 8

Si può emettere un’aura di 9 metri che protegge dalla morte per Liv Chierico round al giorno, non necessariamente consecutivi. 

Le creature viventi in questa zona sono immuni a tutti gli effetti di morte, risucchi di energia ed effetti che infliggono livelli negativi, ma i livelli negativi già
ottenuti da una creatura non sono rimossi, benchè non abbiano effetto dentro l’area della barriera.

Incantesimi di Dominio
***********************
#. :ref:`Visione della Morte<inc_visione_della_morte>`
#. :ref:`Riposo Inviolato<inc_riposo_inviolato>`
#. :ref:`Parlare con i Morti<inc_parlare_con_i_morti>`
#. :ref:`Interdizione alla Morte<inc_interdizione_alla_morte>`
#. :ref:`Distruggere Viventi<inc_distruggere_viventi>`
#. :ref:`Non Morto a Morto<inc_non_morto_a_morto>`
#. :ref:`Distruzione<inc_distruzione>`
#. :ref:`Onde di Esaurimento<inc_onde_di_esaurimento>`
#. :ref:`Lamento della Banshee<inc_lamento_della_banshee>`

Rune
-------------------------
In rune sconosciute ed arcane è stata riscontrata una magia potente. 

Si ottiene :ref:`Scrivere Pergamene<tal_scrivere_pergamene>` come talento bonus.

:Divinità: Irori, Nethys

.. _runa_esplosiva:

Runa Esplosiva (Mag)
****************************
:Tipo: :ref:`Azione Standard<azione_standard>`

Si può creare una runa esplosiva in un qualsiasi quadretto adiacente non occupato da una creatura e tutte le creature che ci entrano subiscono 1d6 danni + 0.5 * Liv Chierico.

Questa runa invisibile infligge danni da acido, elettricità, freddo o fuoco, decisi nel momento in cui si crea la runa e dura Liv Chierico round o fino a quando non viene scaricata. 

.. Note::
    Questa runa conta come un incantesimo di 1° livello allo scopo delle prove di dissolvere, può essere scoperta con una prova di :ref:`Percezione<percezione>` con CD 26 ed essere disarmata con una prova di :ref:`Disattivare Congegni<disattivare_congegni>` con CD 26.

Si può utilizzare 3 + Mod Sag volte al giorno.

Runa Incantata (Mag)
*******************************
:Liv Min: 8

Si può infondere un altro incantesimo che si è in grado di lanciare ad una :ref:`Runa Esplosiva<runa_esplosiva>`, causando gli effetti di quell’incantesimo contro la creatura che la innesca, oltre il danno da esplosione.

Questo incantesimo deve essere almeno di un livello più basso dell’incantesimo da Chierico di livello più alto che si è in grado di lanciare e deve avere come bersaglio una o più creature. 

.. Warning::
    A prescindere dal numero di bersagli su cui l’incantesimo può avere normalmente effetto, esso colpisce soltanto la creatura che innesca la runa.

Incantesimi di Dominio
***********************
#. :ref:`Cancellare<inc_cancellare>`
#. :ref:`Pagina Segreta<inc_pagina_segreta>`
#. :ref:`Glifo di Interdizione<inc_glifo_di_interdizione>`
#. :ref:`Rune Esplosive<inc_rune_esplosive>`
#. :ref:`Legame Planare Inferiore<inc_legame_planare_inferiore>`
#. :ref:`Glifo di Interdizione Superiore<inc_glifo_di_interdizione_superiore>`
#. :ref:`Evocazioni Istantanee<inc_evocazioni_istantanee>`
#. :ref:`Simbolo di Morte<inc_simbolo_di_morte>`
#. :ref:`Cerchio di Teletrasporto<inc_cerchio_di_teletrasporto>`

Sole
-------------------------
Si vede la verità nella luce pura e brillante del sole e si può richiedere la sua benedizione o la giusta forza per portare a termine grandi imprese.

:Divinità: Iomedae, Sarenrae

Benedizione del Sole (Sop)
***************************
Ogni volta che si :ref:`Incanala Energia Positiva<incanalare_energia>` per ferire i non morti, si aggiunge Liv Chierico al danno inflitto e i non morti non aggiungono la loro resistenza ad incanalare ai loro TS.

Nube di Luce (Sop)
*******************************
:Liv Min: 8

Si può emettere una nube di luce di 9 metri per Liv Chierico round al giorno, non necessariamente consecutivi. 

Ciò funziona come :ref:`Luce Diurna<inc_luce_diurna>` e i non morti all’interno di questa nube subiscono Liv Chierico danni per ogni round in cui vi rimangono all'interno. 

Gli incantesimi e le capacità magiche con il descrittore oscurità sono dissolti automaticamente se portati all’interno di questa nube.

Incantesimi di Dominio
***********************
#. :ref:`Contrastare Elementi<inc_contrastare_elementi>`
#. :ref:`Riscaldare il Metallo<inc_riscaldare_il_metallo>`
#. :ref:`Luce Incandescente<inc_luce_incandescente>`
#. :ref:`Scudo di Fuoco<inc_scudo_di_fuoco>`
#. :ref:`Colpo Infuocato<inc_colpo_infuocato>`
#. :ref:`Semi di Fuoco<inc_semi_di_fuoco>`
#. :ref:`Bagliore Solare<inc_bagliore_solare>`
#. :ref:`Esplosione Solare<inc_esplosione_solare>`
#. :ref:`Sfera Prismatica<inc_sfera_prismatica>`

.. _dominio_tempo_atmosferico:

Tempo Atmosferico
-------------------------
Con il potere sulle tempeste ed il cielo, si può richiamare l’ira degli déi sul mondo.

:Divinità: Gozreh, Rovagug

Tempesta (Mag)
***************************
:Tipo: :ref:`Azione Standard<azione_standard>`

Si può creare una tempesta designando come bersaglio nemico qualunque entro 9 metri con un :ref:`Attacco di Contatto a Distanza<azione_attacco_di_contatto_a_distanza>`. 

La tempesta infligge 1d6 danni non letali + 0.5 * Liv Chierico e il bersaglio è colpito
da venti e pioggia, che comportano penalità –2 ai tiri per colpire per 1 round.

Si può utilizzare 3 + Mod Sag volte al giorno.

Signore del Fulmine (Mag)
*******************************
:Tipo: :ref:`Azione Standard<azione_standard>`
:Liv Min: 8

Si può richiamare Liv Chierico fulmini al giorno. 

Si possono richiamare quanti fulmini si vogliono con un’azione, ma nessuna creatura può essere
colpita da più di un fulmine e le creature colpite non possono trovarsi a più di 9 metri l’una dall’altra. Questa capacità funziona per il resto come :ref:`Invocare il Fulmine<inc_invocare_il_fulmine>`.

Incantesimi di Dominio
***********************
#. :ref:`Foschia Occultante<inc_foschia_occultante>`
#. :ref:`Nube di Nebbia<inc_nube_di_nebbia>`
#. :ref:`Invocare il Fulmine<inc_invocare_il_fulmine>`
#. :ref:`Tempesta di Nevischio<inc_tempesta_di_nevischio>`
#. :ref:`Tempesta di Ghiaccio<inc_tempesta_di_ghiaccio>`
#. :ref:`Controllare Venti<inc_controllare_venti>`
#. :ref:`Controllare Tempo Atmosferico<inc_controllare_tempo_atmosferico>`
#. :ref:`Turbine<inc_turbine>`
#. :ref:`Tempesta di Vendetta<inc_tempesta_di_vendetta>`

.. _dominio_terra:

Terra
-------------------------
Si acquisisce la padronanza su terra, metallo e pietra, si possono lanciare dardi di acido e comandare le creature della terra.

:Divinità: Abadar, Torag

Dardo Acido (Mag)
***************************
:Tipo: :ref:`Azione Standard<azione_standard>`

Si può rilasciare un dardo acido contro un bersaglio entro 9 metri con un :ref:`Attacco di Contatto a Distanza<azione_attacco_di_contatto_a_distanza>`. 

Questo dardo infligge 1d6 danni da acido + 0.5 * Liv Chierico.

Si può utilizzare 3 + Mod Sag volte al giorno.

Resistenza all’Acido (Str)
*******************************
:Liv Min: 6

Si ottiene +10 resistenza all’acido che aumenta a 20 al 12° livello e diventa immunità al 20°.

Incantesimi di Dominio
***********************
#. :ref:`Pietra Magica<inc_pietra_magica>`
#. :ref:`Ammorbidire Terra e Pietra<inc_ammorbidire_terra_e_pietra>`
#. :ref:`Scolpire Pietra<inc_scolpire_pietra>`
#. :ref:`Rocce Aguzze<inc_rocce_aguzze>`
#. :ref:`Muro di Pietra<inc_muro_di_pietra>`
#. :ref:`Pelle di Pietra<inc_pelle_di_pietra>`
#. :ref:`Corpo Elementale IV<inc_corpo_elementale_IV>` (solo terra)
#. :ref:`Terremoto<inc_terremoto>`
#. :ref:`Sciame Elementale<inc_sciame_elementale>` (solo descrittore terra)

.. _dominio_vegetale:

Vegetale
------------------------
Si trova conforto nel verde, si possono coltivare spine difensive e si è in grado di comunicare con i vegetali.

:Divinità: Erastil, Gozreh

Pugno di Legno (Sop)
***************************
:Tipo: :ref:`Azione Gratuita<azione_gratuita>`

Le mani possono diventare dure come il legno, ricoperte da piccole spine. 

Mentre pugno di legno è attivo, i colpi senz’armi non provocano :ref:`Attacchi di Opportunità<azione_attacco_di_opportunità>`, infliggono danni letali e ottengono un bonus ai tiri per i danni di 0.5 * Liv Chierico (minimo +1).

Si può utilizzare 3 + Mod Sag round al giorno.

Armatura di Rovi (Sop)
*******************************
:Tipo: :ref:`Azione Gratuita<azione_gratuita>`
:Liv Min: 6

Si fa scaturire una miriade di spine di legno dalla pelle.

Mentre questa capacità è in effetto, tutti gli avversari che attaccano con colpi senz’armi o armi da mischia senza portata subiscono 1d6 danni penetranti + 0.5 * Liv Chierico. 

L'armatura si può utilizzare Liv Chierico round al giorno, non necessariamente consecutivi.

Incantesimi di Dominio
***********************
#. :ref:`Intralciare<inc_intralciare>`
#. :ref:`Pelle Coriacea<inc_pelle_coriacea>`
#. :ref:`Crescita Vegetale<inc_crescita_vegetale>`
#. :ref:`Comandare Vegetali<inc_comandare_vegetali>`
#. :ref:`Muro di Spine<inc_muro_di_spine>`
#. :ref:`Respingere Legno<inc_respingere_legno>`
#. :ref:`Animare Vegetali<inc_animare_vegetali>`
#. :ref:`Controllare Vegetali<inc_controllare_vegetali>`
#. :ref:`Cumulo Strisciante<inc_cumulo_strisciante>`

Viaggio
------------------------
Si è esploratori che trovano la propria illuminazione spirituale nella gioia semplice del viaggiare, con mezzi propri di trasporto normali o magici. 

La propria velocità base aumenta di 3 metri.

:Divinità: Abadar, Cayden Cailean, Desna

Piedi Agili (Sop)
***************************
:Tipo: :ref:`Azione Gratuita<azione_gratuita>`

Si può guadagnare un aumentò della mobilità per 1 round. 

Per il round successivo, si ignora tutto il terreno difficile e non si subiscono penalità per muoversi attraverso di esso.

Si può utilizzare 3 + Mod Sag volte al giorno.

Balzo Dimensionale (Mag)
*******************************
:Tipo: :ref:`Azione di Movimento<azione_movimento>`
:Liv Min: 8

Ci si può teletrasportare fino a 3 * Liv Chierico metri al giorno. 

Questo teletrasporto deve essere utilizzato in incrementi di 1,5 metri e non provoca :ref:`Attacchi di Opportunità<azione_attacco_di_opportunità>`, ma bisogna avere la visuale della
destinazione per utilizzarlo.

.. Note::
    Si possono portare con sè altre creature consenzienti, ma si consuma distanza percorribile per ogni creatura portata.

Incantesimi di Dominio
***********************
#. :ref:`Passo Veloce<inc_passo_veloce>`
#. :ref:`Localizza Oggetto<inc_localizza_oggetto>`
#. :ref:`Volare<inc_volare>`
#. :ref:`Porta Dimensionale<inc_porta_dimensionale>`
#. :ref:`Teletrasporto<inc_teletrasporto>`
#. :ref:`Scopri il Percorso<inc_scopri_il_percorso>`
#. :ref:`Teletrasporto Superiore<inc_teletrasporto_superiore>`
#. :ref:`Porta in Fase<inc_porta_in_fase>`
#. :ref:`Proiezione Astrale<inc_proiezione_astrale>`

.. _druido:

Druido
===========

Nella purezza degli elementi e dell’ordine selvaggio giace un potere oltre le meraviglie della civilizzazione. Non appariscente ma innegabile, questa magia primordiale è custodita dai servitori dell’equilibrio filosofico noti come druidi.

Alleati alle creature animali e manipolatori della natura, questi protettori spesso incompresi del mondo selvaggio si sforzano di proteggere le loro terre da tutti quelli che potrebbero minacciarle e di comprovare la forza del mondo selvaggio a chi si chiude dietro le mura della città. 

I druidi guadagnano capacità senza pari di metamorfosi, compagnia di animali e invocazione della collera della natura fino a tempeste, terremoti e vulcani con una saggezza primeva ormai abbandonata e dimenticata dalla civiltà.

:Ruolo: 

    Alcuni druidi si tengono ai margini della battaglia, permettendo ai compagni e alle creature evocate di combattere mentre essi confondono i nemici con i poteri della natura. Altri si trasformano in creature ferine mortali e selvagge che si tuffano nel combattimento.

    I druidi adorano le personificazioni delle forze elementali, dei poteri naturali, o della natura in sé. Questo significa tipicamente la devozione ad una divinità della natura, benché i druidi venerino solitamente spiriti primevi, semi-divinità animali o specifiche meraviglie naturali.

.. figure:: Immagini/Druido.png
    :align: right
    :width: 400

:Allineamento: Qualsiasi Neutrale
:Dado Vita: d8

----------------------------
Abilità di classe
----------------------------

:Crescita per livello: 4 + modificatore Intelligenza (Mod Int)

*	:ref:`Addestrare Animali (Car)<addestrare_animali>`
*	:ref:`Artigianato (Int)<artigianato>`
*	:ref:`Cavalcare (Des)<cavalcare>`
*	:ref:`Conoscenze (geografia, natura) (Int)<conoscenze>`
*	:ref:`Guarire (Sag)<guarire>`
*	:ref:`Nuotare (For)<nuotare>`
*	:ref:`Percezione (Sag)<percezione>`
*	:ref:`Professione (Sag)<professione>`
*	:ref:`Sapienza Magica (Int)<sapienza_magica>`
*	:ref:`Scalare (For)<scalare>`
*	:ref:`Sopravvivenza (Sag)<sopravvivenza>`
*	:ref:`Volare (Int)<volare>`

--------------------
Privilegi di Classe
--------------------

.. contents:: Riassumendo:
    :local:

Competenze nelle armi e armature
----------------------------------

Le seguenti armi: bastone ferrato, dardi, falce, falcetto, fionda, lancia, lancia corta, pugnale, randello e scimitarra. Tutti gli attacchi naturali (artiglio, morso e così via) di qualsiasi forma assunta con :ref:`Forma Selvatica<forma_selvatica>`.

Armature leggere e medie, ma con il divieto di indossare armature di metallo: solo corazze di cuoio, di pelle o imbottite. 
Un druido può anche indossare un’armatura in legno modificata dall’incantesimo :ref:`Legno di Ferro<inc_legno_di_ferro>` in modo che funzioni come se fosse acciaio. 

I druidi sono competenti negli scudi (tranne gli scudi torre), ma devono usare solo
quelli di legno.

.. Caution::
    Un druido che indossa un’armatura proibita o che porta uno scudo proibito non è in grado di lanciare gli incantesimi da druido, né di utilizzare le proprie capacità magiche o soprannaturali finché non smette di usare questi oggetti, e per le 24 ore successive.

Incantesimi
-------------------

.. _avanzamento_druido:

.. figure:: Immagini/AvanzamentoDruido.png

:Lista: :ref:`Incantesimi da Druido<incantesimi_druido>`
:Preparazione: Sì

L'allineamento può precludere il lancio di alcuni incantesimi :ref:`avversi al proprio credo etico e morale<incantesimi_buoni_caotici_etc_druido>`.

Per preparare o lanciare un incantesimo, un druido deve Saggezza ≥ 10 + Liv Incantesimo, corrispondente a una CD per un tiro salvezza di 10 + Liv Incantesimo + Mod Sag.


Come gli altri incantatori, il druido può lanciare solo un :ref:`certo numero<avanzamento_druido>` di incantesimi al giorno per ogni livello di incantesimi (+ quelli :ref:`bonus<incantesimi_bonus>`, basati sulla Sag).

Un druido deve passare 1 ora ogni giorno in una trance meditativa sui misteri della natura per riguadagnare il suo assortimento giornaliero di incantesimi e può preparare e lanciare qualsiasi incantesimo che si trova nella lista degli incantesimi da druido.

Lancio spontaneo: Un druido può catalizzare l’energia
magica accumulata in incantesimi di evocazione che non
ha preparato in precedenza. Il druido può perdere un
incantesimo preparato per lanciare un qualsiasi incantesimo
evoca alleato naturale dello stesso livello o di livello inferiore.

.. _lancio_spontaneo_druido:

Lancio spontaneo
-------------------
L’energia magica accumulata può essere catalizzata in incantesimi di evocazione che non sono stati preparati in precedenza. 

Il druido può perdere un incantesimo preparato per lanciare un qualsiasi incantesimo
:ref:`Evoca Alleato Naturale<inc_evoca_alleato_naturale>` dello stesso livello o di livello inferiore.

.. _incantesimi_buoni_caotici_etc_druido:

Incantesimi buoni, caotici, legali, malvagi
-------------------------------------------
Un druido non può lanciare incantesimi di un allineamento opposto al suo o a quello della sua divinità, se dotata di allineamento.

Incantesimi associati con particolari allineamenti sono indicati dai descrittori del bene, del caos, della legge e del male nella descrizione dell’incantesimo.

Orazioni
-----------
I druidi possono preparare un :ref:`certo numero<avanzamento_druido>` di orazioni, o incantesimi di livello 0, ogni giorno che sono lanciati come qualsiasi altro incantesimo, ma non consumano nessuno slot e possono essere utilizzati di nuovo.

Linguaggi bonus
--------------------
Le opzioni di linguaggi bonus di un druido includono il Silvano, che è il linguaggio delle creature boschive, che si aggiunge ai linguaggi bonus a disposizione di un personaggio per la sua razza.

Un druido conosce il Druidico, linguaggio segreto noto solamente ai druidi, che imparano nel momento in cui divengono druidi. 

Il Druidico è un linguaggio gratuito per un druido e non occupa alcuno slot per i linguaggi, ma è vietato insegnarlo ai non druidi e ha un proprio alfabeto.

.. figure:: Immagini/Druido2.png
    :align: left
    :width: 400

.. _legame_con_la_natura:

Legame con la Natura (Str)
--------------------------
Un druido forma un legame con la natura che può essere di due forme. 

La **prima** è un vincolo intimo con il mondo naturale, che garantisce al druido uno dei seguenti :ref:`Domini dei Chierici<domini_chierico>`: :ref:`Acqua<dominio_acqua>`, :ref:`Animale<dominio_animale>`, :ref:`Aria<dominio_aria>`, :ref:`Fuoco<dominio_fuoco>`, :ref:`Tempo Atmosferico<dominio_tempo_atmosferico>`, :ref:`Terra<dominio_terra>` o :ref:`Vegetale<dominio_vegetale>`. 

Quando si determinano i poteri e gli incantesimi bonus concessi da questo dominio, il Liv Druido vale come Liv Chierico.

In tal caso il druido riceve uno slot incantesimo di dominio addizionale, come un :ref:`Chierico<chierico>`, e deve preparare l’incantesimo dal suo dominio in questo slot senza poterlo usare per il :ref:`Lancio Spontaneo<lancio_spontaneo_druido>`.

La **seconda** opzione è un vincolo intimo con un :ref:`Compagno Animale<compagno_animale_druido>`, scelto e acquisito all'inizio del gioco: un leale compagno che
accompagna il druido nelle sue avventure.

A differenza degli animali normali della sua specie, i Dadi Vita, le caratteristiche, le abilità e i talenti del :ref:`Compagno Animale<compagno_animale_druido>` avanzano quando il druido avanza di livello e, se un personaggio riceve un :ref:`Compagno Animale<compagno_animale_druido>` da più di una fonte, i Liv Druido si cumulano al fine di
determinare le statistiche e le capacità del compagno. Molti :ref:`Compagno Animale<compagno_animale_druido>` aumentano di taglia quando il druido
raggiunge il 4° o il 7° livello, in base al compagno. 

Se un Druido libera il suo compagno animale dal suo servizio o alla sua morte, può ottenerne uno nuovo celebrando una **cerimonia** che richiede 24 ore ininterrotte di preghiera nell’ambiente in cui il nuovo compagno solitamente vive.

Senso della Natura (Str)
-------------------------

+2 a :ref:`Conoscenze<conoscenze>` (natura) e :ref:`Sopravvivenza<sopravvivenza>`.

.. _empatia_selvatica:

Empatia Selvatica (Str)
-----------------------

Un druido può migliorare l’atteggiamento di un animale come una prova di :ref:`Diplomazia<diplomazia>` per migliorare l’atteggiamento di una persona, con **1d20 + Liv Druido + Mod Car** come risultato.

.. Note::
    Il tipico animale domestico ha un atteggiamento iniziale indifferente, mentre un animale selvatico è solitamente maldisposto.

L'animale deve essere entro 9m e tipicamente occorre un minuto per influenzarlo.

Si può influenzare anche una bestia magica con un punteggio di Intelligenza di 1 o 2, ma con penalità –4 alla prova.

Andatura nel Bosco (Str)
------------------------
:Liv Min: 2

Un Druido può muoversi attraverso qualsiasi tipo di sottobosco (come rovi, sterpi naturali, zone infestate e simili terreni) a velocità normale e senza subire danni o altri impedimenti, a meno che siano incantati o manipolati damagicamente

Passo Senza Tracce (Str)
------------------------
:Liv Min: 3

Non si lasciano tracce in ambienti naturali e non si può essere inseguiti seguendo le proprie tracce.

Resistenza al Richiamo della Natura (Str): 
-------------------------------------------
:Liv Min: 4

\+4 ai TS contro le capacità magiche e soprannaturali dei folletti, che si applica anche ad incantesimi ed effetti che utilizzano o hanno come bersaglio i vegetali, come :ref:`Crescita di Spine<inc_crescita_di_spine>`, :ref:`Deformare Legno<inc_deformare_legno>`, :ref:`Inaridire<inc_inaridire>` e :ref:`Intralciare<inc_intralciare>`.

.. _forma_selvatica:

Forma selvatica (Sop)
----------------------
:Liv Min: 4

Capacità magica di trasformarsi in una qualsiasi creatura del tipo animale di taglia Piccola o Media e di nuovo in Druido una volta al giorno, allo stesso modo di :ref:`Forma Ferina I<inc_forma_ferina_I>` con alcune eccezioni:

-	Dura 1 ora per Liv Druido, o finché non si riprende la forma naturale, e il cambiamento di forma (da animale e viceversa) è un':ref:`azione_standard` che non provoca :ref:`Attacchi di Opportunità<azione_attacco_di_opportunità>`.
-	La forma scelta deve essere quella di un animale con cui il druido è familiare.
-	Il druido perde la capacità di parlare perché limitato ai suoni che può emettere un normale animale non addestrato, ma è in grado di comunicare normalmente con altri animali dello stesso raggruppamento generale della sua nuova forma. (Il verso normale di un pappagallo selvatico è uno stridio rauco, quindi assumere questa forma non permette di parlare.)

Si ottiene un utilizzo extra di questa capacità ogni due livelli dopo il quarto e al 20° si può usare forma selvatica a volontà.

Al **6° livello**, il druido diventa in grado di utilizzare la forma selvatica per trasformarsi in una creatura animale Grande o Minuscola o in una elementale Piccola. Quando assume la forma di un animale, la :ref:`Forma Selvatica<forma_selvatica>` del druido funziona come :ref:`Forma Ferina II<inc_forma_ferina_II>` mentre quando assume la forma di un elementale funziona come :ref:`Corpo Elementale I<inc_corpo_elementale_I>`.

All’**8°** diventa in grado di utilizzare la :ref:`Forma Selvatica<forma_selvatica>` per trasformarsi in un animale Enorme o Minuto, un elementale Medio, o in una creatura vegetale
Piccola o Media. La trasformazione funziona come :ref:`Forma Ferina III<inc_forma_ferina_III>` per gli animali, :ref:`Corpo Elementale II<inc_corpo_elementale_II>` per gli elementali e :ref:`Forma di Vegetale I<inc_forma_di_vegetale_I>` per i vegetali.

Al **10°** può trasformarsi in un elementale Grande (:ref:`Corpo Elementale III<inc_corpo_elementale_III>`), o in una creatura vegetale Grande (:ref:`Forma di Vegetale II<inc_forma_di_vegetale_II>`).

Al **12°** in un elementale Enorme (:ref:`Corpo Elementale IV<inc_corpo_elementale_IV>`), o in una creatura vegetale Enorme (:ref:`Forma di Vegetale III<inc_forma_di_vegetale_III>`). 

Immunità ai Veleni (Str)
-------------------------
:Liv Min: 9

Immunità a tutti i veleni.

Mille Volti (Sop)
------------------
:Liv Min: 13

Capacità di modificare il proprio aspetto a piacere, come per :ref:`Alterare Sé Stesso<inc_alterare_sè_stesso>`, ma solo quando ci si trova nella forma naturale.

Corpo senza tempo (Str)
-----------------------
:Liv Min: 15

Non si subiscono più le penalità per l’invecchiamento e non si può essere invecchiati magicamente. 

Tutte le penalità già subite rimangono tali, i bonus valgono ancora e il druido muore comunque di vecchiaia quando arriva il suo momento.

----------------
Ex-Druidi
----------------

Un druido che smette di venerare la natura, che diventa di allineamento proibito o che insegna il linguaggio Druidico ad un non druido perde tutti gli incantesimi e le capacità druidiche (escluse le competenze nelle armi, armature e scudi), e non può guadagnare livelli come druido fino a quando non :ref:`espia le sue colpe<inc_espiazione>`.

.. _compagno_animale_druido:

------------------------
Compagno Animale
------------------------

.. figure:: Immagini/Druido3.png
    :align: right
    :width: 400

.. contents:: Riassumendo:
    :local:

Le caratteristiche di un compagno animale sono determinate in base al livello del druido e dai suoi tratti razziali da animale. 

La maggior parte delle statistiche di base di un *Compagno Animale* si possono trovare in :ref:`Tabella<compagno_animale_tabella>` ed esso rimane una creatura di tipo animale al fine di determinare quali incantesimi possono avere o meno effetto su di lui.

Statistiche Base
-----------------

.. _compagno_animale_tabella:

.. figure:: Immagini/CompagnoAnimale.png
    :width: 700

:Livello di Classe: 
    Il Liv Druido del personaggio, cumulativo con i livelli di qualsiasi altra classe che permetta di ottenere un compagno animale al fine di determinare le statistiche del compagno
:DV:
    Numero totale dei Dadi Vita d8 che il *Compagno Animale possiede*, su ognuno dei quali si applica il Mod Cos.
:BAB:
    Il bonus di attacco base del compagno animale, pari a quello di un Druido di livello pari ai DV dell’animale.

    I compagni animali non ottengono attacchi addizionali usando le loro armi naturali pur avendo un BAB alto.
:Temp/Rifl/Vol:
    I bonus ai TS, ottimi su Tempra e Riflessi.
:Abilità:
    I gradi d’abilità totali dell’animale, che possono essere assegnati a qualsiasi :ref:`Abilità degli Animali<abilità_compagno_animale>`, fino a un massimo dato dal numero dei DV.

    Se un *Compagno Animale* aumenta la sua Intelligenza a 10 o più, ottiene gradi di abilità bonus come di norma, e con più di 3 Intelligenza si possono  acquisire gradi in qualsiasi abilità.
:Talenti: 
    Il numero totale di talenti posseduti, che dovrebbero essere scelti tra i :ref:`Talenti degli Animali<talenti_compagno_animale>`.

    I *Compagni Animali* possono scegliere anche altri talenti, ma non sono in grado di utilizzarne alcuni, come :ref:`Competenza nelle Armi da Guerra<tal_competenza_nelle_armi_da_guerra>`.

    .. Note::
        Non si possono scegliere talenti con il prerequisito di BAB +1 finché non si raggiunge il secondo talento a 3 Dadi Vita.
:Bonus di Armatura Naturale: 
    Un aumento del Bonus di Armatura Naturale esistente del compagno animale.
:Bonus For/Des: 
    Da aggiungere ai valori di Forza e Destrezza del *Compagno Animale*.
:Comandi Bonus: 
    Il numero totale di comandi “bonus” che l’animale conosce in aggiunta a qualsiasi altro comando il :ref:`Druido<druido>` possa decidere di :ref:`insegnargli<addestrare_animali>`.

    Questi comandi bonus non richiedono alcun periodo di addestramento o prova di :ref:`Addestrare Animali<addestrare_animali>`, né contano per il normale limite di comandi conosciuti dall’animale.

    Il :ref:`Druido<druido>` seleziona questi comandi bonus, che una volta scelti non possono essere più cambiati.
:Speciale: 
    Capacità speciali ottenute man mano che si sale di livello, descritte nella :ref:`prossima sezione<speciali_compagno_animale>`.

.. _speciali_compagno_animale:

Caratteristiche Speciali
-------------------------

Legame (Str)
************
Un :ref:`Druido<druido>` può gestire le azioni del suo :ref:`Compagno Animale<compagno_animale_druido>` con un’:ref:`Azione Gratuita<azione_gratuita>` o “spingerlo” con un’:ref:`Azione di Movimento<azione_movimento>` anche se non possiede alcun grado in :ref:`Addestrare Animali<addestrare_animali>`. 

Il :ref:`Druido<druido>` ottiene inoltre un bonus di circostanza +4 a tutte le prove di :ref:`Empatia Selvatica<empatia_selvatica>` e di :ref:`Addestrare Animali<addestrare_animali>` nei confronti del suo :ref:`Compagno Animale<compagno_animale_druido>`.

Condividere Incantesimi (Str)
************************************
Il :ref:`Druido<druido>` può lanciare qualsiasi incantesimo che abbia come bersaglio “incantatore” sul suo :ref:`Compagno Animale<compagno_animale_druido>`, come un incantesimo di contatto a distanza, anche in caso gli effetti non abbiano normalmente effetto su creature del tipo del :ref:`Compagno<compagno_animale_druido>`.

Gli incantesimi così lanciati devono provenire da una classe che garantisce un :ref:`Compagno<compagno_animale_druido>` e questa capacità non permette di condividere capacità che non siano incantesimi, anche se funzionano come tali.

Eludere (Str)
************************
Se un :ref:`Compagno Animale<compagno_animale_druido>` è soggetto a un attacco che normalmente permette un TS su Riflessi per dimezzare i danni, non subisce danni se effettua con successo il TS.

Aumento di Caratteristica (Str)
************************************
+1 ad un punteggio di caratteristica.

Devozione (Str)
***************
Bonus morale +4 ai TS su Volontà contro incantesimi ed effetti di ammaliamento.

Multiattacco
************
Talento bonus :ref:`Multiattacco<tal_multiattacco>` se il :ref:`Compagno<compagno_animale_druido>` ha tre o più attacchi naturali e se già non lo possiede. 

In caso non possieda i tre o più attacchi naturali richiesti, il :ref:`Compagno Animale<compagno_animale_druido>` riceve un secondo attacco con la sua arma naturale primaria ma con penalità –5.

.. _eludere_migliorato:

Eludere Migliorato (Str)
************************
Se il :ref:`Compagno Animale<compagno_animale_druido>` è soggetto a un attacco che normalmente permette un TS su Riflessi per dimezzare i danni, non subisce danni se effettua con successo il TS e solo la metà dei danni se lo fallisce.

.. _abilità_compagno_animale:

Abilità degli Animali
----------------------
I :ref:`Compagni Animale<compagno_animale_druido>` possono avere gradi in:

    *	:ref:`Acrobazia* (Des)<acrobazia>`
    *	:ref:`Artista della Fuga (Des)<artista_della_fuga>`
    *	:ref:`Furtività* (Des)<furtività>`
    *	:ref:`Intimidire (Car)<intimidire>`
    *	:ref:`Nuotare* (For)<nuotare>`
    *	:ref:`Percezione* (Sag)<percezione>`
    *	:ref:`Scalare* (For)<scalare>`
    *	:ref:`Sopravvivenza (Sag)<sopravvivenza>`
    *	:ref:`Volare* (Des)<volare>`

Dove tutte le abilità con un asterisco (*) sono considerate abilità di classe. 

.. Note::
    I :ref:`Compagni Animale<compagno_animale_druido>` con Intelligenza 3 o più possono mettere gradi in qualsiasi abilità.

.. _talenti_compagno_animale:

Talenti degli Animali
------------------------
I :ref:`Compagni Animale<compagno_animale_druido>` possono scegliere tra i seguenti talenti:

    *	:ref:`Abilità Focalizzata<tal_abilità_focalizzata>`
    *	:ref:`Acrobatico<tal_acrobatico>`
    *	:ref:`Arma Accurata<tal_arma_accurata>`
    *	:ref:`Arma Focalizzata<tal_arma_focalizzata>`
    *	:ref:`Armatura Naturale Migliorata<tal_armatura_naturale_migliorata>`
    *	:ref:`Atletico<tal_atletico>`
    *	:ref:`Attacco Naturale Migliorato<tal_attacco_naturale_migliorato>`
    *	:ref:`Attacco Poderoso<tal_attacco_poderoso>`
    *	:ref:`Attacco Rapido<tal_attacco_rapido>`
    *	:ref:`Combattere alla Cieca<tal_combattere_alla_cieca>`
    *	:ref:`Competenza nelle Armature<tal_competenza_nelle_armature>` (leggere, medie e pesanti)
    *	:ref:`Correre<tal_correre>`
    *	:ref:`Duro a Morire<tal_duro_a_morire>`
    *	:ref:`Furtivo<tal_furtivo>`
    *	:ref:`Iniziativa Migliorata<tal_iniziativa_migliorata>`
    *	:ref:`Manovre Agili<tal_manovre_agili>`
    *	:ref:`Mobilità<tal_mobilità>`
    *	:ref:`Oltrepassare Migliorato<tal_oltrepassare_migliorato>`
    *	:ref:`Prodezza Intimidatrice<tal_prodezza_intimidatrice>`
    *	:ref:`Resistenza Fisica<tal_resistenza_fisica>`
    *	:ref:`Riflessi Fulminei<tal_riflessi_fulminei>`
    *	:ref:`Riflessi in Combattimento<tal_riflessi_in_combattimento>`
    *	:ref:`Robustezza<tal_robustezza>`
    *	:ref:`Schivare<tal_schivare>`
    *	:ref:`Spingere Migliorato<tal_spingere_migliorato>`
    *	:ref:`Tempra Possente<tal_tempra_possente>`
    *	:ref:`Volontà di Ferro<tal_volontà_di_ferro>`

.. Note::
    I :ref:`Compagni Animale<compagno_animale_druido>` con Intelligenza 3 o più possono scegliere qualsiasi talento che sono fisicamente in grado di usare e il GM può espandere la lista includendo talenti da altre fonti.

.. _scegliere_compagno_animale:

Scegliere l'Animale
--------------------
Ogni compagno animale ha *taglia*, *velocità*, *attacchi*, *caratteristiche* e *qualità speciali* iniziali differenti. 

Gli attacchi dell’animale sono effettuati usando il BAB pieno a meno che non sia indicato diversamente e aggiungono il Mod For dell’animale ai tiri per i danni, a meno che non abbia
un solo attacco, nel qual caso aggiunge una 1.5 Mod For. 
Alcuni hanno *capacità speciali*, come :ref:`Fiuto<capacità_fiuto>`, che si possono approfondire nell':ref:`Appendice<appendice_capacità_speciali>`. 

Quando si avanza di livello anche il proprio :ref:`Compagno Animale<compagno_animale_druido>` migliora e, in aggiunta ai bonus standard indicati nella :ref:`Tabella di Avanzamento<compagno_animale_tabella>`, a seconda del livello (di solito il 4° o il 7° livello) ottengono ulteriori avanzamenti. 

.. Note:: 
    Invece che prendere i benefici indicati al 4° o al 7° livello, si può scegliere di aumentare la Destrezza e la Costituzione del :ref:`Compagno<compagno_animale_druido>` di 2.

I compagni animali qui descritti non sono in nessun caso gli unici disponibili: tipi di :ref:`Compagno Animale<compagno_animale_druido>` aggiuntivi si possono trovare nel :ref:`Bestiario<bestiario_compagni_animale>`, in cui vengono anche spiegati dettagliatamente gli attacchi e le capacità
speciali posseduti dagli animali.

.. contents:: Scelte base:
    :local:

Cammello
**********
:Statistiche iniziali: 
    :Taglia: Grande
    :Velocità: 15m
    :CA: +1 Armatura Naturale
    :Attacchi: *Morso* (1d4) e *Sputo* (:ref:`Attacco di Contatto a Distanza<azione_attacco_di_contatto_a_distanza>`: il bersaglio è infermo per 1d4 round, raggio 3m)
    :Caratteristiche: For 18, Des 16, Cos 14, Int 2, Sag 11, Car 4
    :Speciali: :ref:`Fiuto<bestiario_fiuto>` e :ref:`Visione Crepuscolare<visione_crepuscolare>`

:4° Livello: 
    :Caratteristiche: For +2, Cos +2

Cane
**********
:Statistiche iniziali: 
    :Taglia: Piccola
    :Velocità: 12m
    :CA: +2 Armatura Naturale
    :Attacchi: *Morso* (1d4)
    :Caratteristiche: For 13, Des 17, Cos 15, Int 2, Sag 12, Car 6
    :Speciali: :ref:`Fiuto<bestiario_fiuto>` e :ref:`Visione Crepuscolare<visione_crepuscolare>`

:4° livello: 
    :Taglia: Media
    :Attacchi: *Morso* (1d6)
    :Caratteristiche: For +4, Des –2, Cos +2

Cavallo
**********
:Statistiche iniziali: 
    :Taglia: Grande
    :Velocità: 15m
    :CA: +4 Armatura Naturale
    :Attacchi: *Morso* (1d4) e 2 *Zoccoli* (1d6)
    :Caratteristiche: For 16, Des 13, Cos 15, Int 2, Sag 12, Car 6
    :Speciali: :ref:`Fiuto<bestiario_fiuto>` e :ref:`Visione Crepuscolare<visione_crepuscolare>`

    *Zoccoli* è un :ref:`Attacco Secondario<attacchi_secondari>`.

:4° livello: 
    :Caratteristiche: For +2, Cos +2
    :Speciali: :ref:`Addestrato al Combattimento<addestrato_al_combattimento>`

Cinghiale
**********
:Statistiche iniziali: 
    :Taglia: Piccola
    :Velocità: 12m
    :CA: +6 Armatura Naturale
    :Attacchi: *Corno* (1d6)
    :Caratteristiche: For 13, Des 12, Cos 15, Int 2, Sag 13, Car 4
    :Speciali: :ref:`Fiuto<bestiario_fiuto>` e :ref:`Visione Crepuscolare<visione_crepuscolare>`

:4° livello: 
    :Taglia: Media
    :Attacchi: *Corno* (1d8)
    :Caratteristiche: For +4, Des –2, Cos +2
    :Speciali: :ref:`Ferocia<bestiario_ferocia>`

Coccodrillo (Alligatore)
******************************
:Statistiche iniziali: 
    :Taglia: Piccola
    :Velocità: 6m, Nuotare 9m
    :CA: +4 Armatura Naturale
    :Attacchi: *Morso* (1d6)
    :Caratteristiche: For 15, Des 14, Cos 15, Int 1, Sag 12, Car 2
    :Speciali: :ref:`Trattenere il Fiato<bestiario_trattenere_il_fiato>` e :ref:`Visione Crepuscolare<visione_crepuscolare>`

:4° livello: 
    :Taglia: Media
    :Attacchi: *Morso* (1d8) e *Colpo di Coda* (1d12)
    :Caratteristiche: For +4, Des –2, Cos +2
    :Speciali: :ref:`Afferrare<bestiario_afferrare>`, :ref:`Rotazione Mortale<bestiario_rotazione_mortale>` e :ref:`Scatto<bestiario_scatto>`

Dinosauro (Deinonico, Velociraptor)
****************************************
:Statistiche iniziali: 
    :Taglia: Piccola
    :Velocità: 18m
    :CA: +1 Armatura Naturale
    :Attacchi: 2 *Speroni* (1d6) e *Morso* (1d4)
    :Caratteristiche: For 11, Des 17, Cos 17, Int 2, Sag 12, Car 14
    :Speciali: :ref:`Fiuto<bestiario_fiuto>` e :ref:`Visione Crepuscolare<visione_crepuscolare>`

:7° livello: 
    :Taglia: Media
    :Ca: +2 Armatura Naturale
    :Attacchi: 2 *Speroni* (1d8), *Morso* (1d6) e 2 *Artigli* (1d4)
    :Caratteristiche: For +4, Des –2, Cos +2
    :Speciali: :ref:`Assaltare<bestiario_assaltare>`

Felino Grande (Leone, Tigre)
******************************
:Statistiche iniziali: 
    :Taglia: Media
    :Velocità: 12m
    :CA: +1 Armatura Naturale
    :Attacchi: *Morso* (1d6) e 2 *Artigli* (1d4)
    :Caratteristiche: For 13, Des 17, Cos 13, Int 2, Sag 15, Car 10
    :Speciali: :ref:`Artigliare<bestiario_artigliare>` (1d4), :ref:`Fiuto<bestiario_fiuto>` e :ref:`Visione Crepuscolare<visione_crepuscolare>`

:7° livello: 
    :Taglia: Grande
    :Ca: +2 Armatura Naturale
    :Attacchi: *Morso* (1d8) e 2 *Artigli* (1d6)
    :Caratteristiche: For +8, Des –2, Cos +4
    :Speciali: :ref:`Afferrare<bestiario_afferrare>`, :ref:`Artigliare<bestiario_artigliare>` (1d6) e :ref:`Assaltare<bestiario_assaltare>`

Felino Piccolo (Ghepardo, Leopardo)
****************************************
:Statistiche iniziali: 
    :Taglia: Piccola
    :Velocità: 15m
    :CA: +1 Armatura Naturale
    :Attacchi: *Morso* (1d4 più sbilanciare) e 2 *Artigli* (1d2)
    :Caratteristiche: For 12, Des 21, Cos 13, Int 2, Sag 12, Car 6
    :Speciali: :ref:`Fiuto<bestiario_fiuto>` e :ref:`Visione Crepuscolare<visione_crepuscolare>`

:4° livello: 
    :Taglia: Media
    :Attacchi: *Morso* (1d6) e 2 *Artigli* (1d3)
    :Caratteristiche: For +4, Des –2, Cos +2
    :Speciali: :ref:`Scatto<bestiario_scatto>`

Gorilla
**********
:Statistiche iniziali: 
    :Taglia: Media
    :Velocità: 9m, Scalare 9m
    :CA: +1 Armatura Naturale
    :Attacchi: *Morso* (1d4) e 2 *Artigli* (1d4)
    :Caratteristiche: For 13, Des 17, Cos 10, Int 2, Sag 12, Car 7
    :Speciali: :ref:`Fiuto<bestiario_fiuto>` e :ref:`Visione Crepuscolare<visione_crepuscolare>`

:4° livello: 
    :Taglia: Grande
    :Ca: +2 Armatura Naturale
    :Attacchi: *Morso* (1d6) e 2 *Artigli* (1d6)
    :Caratteristiche: For +8, Des –2, Cos +4

Lupo
**********
:Statistiche iniziali: 
    :Taglia: Media
    :Velocità: 15m
    :CA: +2 Armatura Naturale
    :Attacchi: *Morso* (1d6 più sbilanciare)
    :Caratteristiche: For 13, Des 15, Cos 15, Int 2, Sag 12, Car 6
    :Speciali: :ref:`Fiuto<bestiario_fiuto>`

:7° livello: 
    :Taglia: Grande
    :Ca: +2 Armatura Naturale
    :Attacchi: *Morso* (1d8 più sbilanciare)
    :Caratteristiche: For +8, Des –2, Cos +4

Orso
**********
:Statistiche iniziali: 
    :Taglia: Piccola
    :Velocità: 12m
    :CA: +2 Armatura Naturale
    :Attacchi: *Morso* (1d4) e 2 *Artigli* (1d3)
    :Caratteristiche: For 15, Des 15, Cos 13, Int 2, Sag 12, Car 6
    :Speciali: :ref:`Fiuto<bestiario_fiuto>` e :ref:`Visione Crepuscolare<visione_crepuscolare>`

:4° livello: 
    :Taglia: Media
    :Attacchi: *Morso* (1d6) e 2 *Artigli* (1d4)
    :Caratteristiche: For +4, Des –2, Cos +2

Pony
**********
:Statistiche iniziali: 
    :Taglia: Media
    :Velocità: 12m
    :CA: +2 Armatura Naturale
    :Attacchi: 2 *Zoccoli* (1d3)
    :Caratteristiche: For 13, Des 13, Cos 12, Int 2, Sag 11, Car 4
    :Speciali: :ref:`Fiuto<bestiario_fiuto>` e :ref:`Visione Crepuscolare<visione_crepuscolare>`

:4° livello: 
    :Caratteristiche: For +2, Cos +2
    :Speciali: :ref:`Addestrato al Combattimento<addestrato_al_combattimento>`

Serpente, Strangolatore
******************************
:Statistiche iniziali: 
    :Taglia: Media
    :Velocità: 6m, Scalare 6m, Nuotare 6m
    :CA: +2 Armatura Naturale
    :Attacchi: *Morso* (1d3)
    :Caratteristiche: For 15, Des 17, Cos 13, Int 1, Sag 12, Car 2
    :Speciali: :ref:`Afferrare<bestiario_afferrare>` e :ref:`Fiuto<bestiario_fiuto>`

:4° livello: 
    :Taglia: Grande
    :Ca: +1 Armatura Naturale
    :Attacchi: *Morso* (1d4)
    :Caratteristiche: For +8, Des –2, Cos +4
    :Speciali: :ref:`Stritolare<bestiario_stritolare>` (1d4)

Serpente, Vipera
********************
:Statistiche iniziali: 
    :Taglia: Piccola
    :Velocità: 6m, Scalare 6m, Nuotare 6m
    :CA: +2 Armatura Naturale
    :Attacchi: *Morso* (1d3 più veleno)
    :Caratteristiche: For 8, Des 17, Cos 11, Int 1, Sag 12, Car 2
    :Speciali: :ref:`Veleno<bestiario_veleno>` (*Frequenza* 1 round, *Durata* 6 round, *Effetto* 1 danno Cos, *Cura* 1 TS con CD su Cos) e :ref:`Fiuto<bestiario_fiuto>`

:4° livello: 
    :Taglia: Media
    :Attacchi: *Morso* (1d4 più veleno) e 2 *Artigli* (1d6)
    :Caratteristiche: For +4, Des –2, Cos +2

Squalo
**********
:Statistiche iniziali: 
    :Taglia: Piccola
    :Velocità: Nuotare 18m
    :CA: +4 Armatura Naturale
    :Attacchi: *Morso* (1d4)
    :Caratteristiche: For 13, Des 15, Cos 15, Int 1, Sag 12, Car 2
    :Speciali: :ref:`Fiuto<bestiario_fiuto>`

:4° livello: 
    :Taglia: Media
    :Attacchi: *Morso* (1d6)
    :Caratteristiche: For +4, Des –2, Cos +2
    :Speciali: :ref:`Percezione Cieca<bestiario_percezione_cieca>`

Tasso (Ghiottone)
*****************
:Statistiche iniziali: 
    :Taglia: Piccola
    :Velocità: 9m, Scavare 3m, Scalare 3m
    :CA: +2 Armatura Naturale
    :Attacchi: *Morso* (1d4) e 2 *Artigli* (1d3)
    :Caratteristiche: For 10, Des 17, Cos 15, Int 2, Sag 12, Car 10
    :Speciali: :ref:`Ira<ira>` (come un :ref:`Barbaro<barbaro>` 6 round al giorno), :ref:`Fiuto<bestiario_fiuto>` e :ref:`Visione Crepuscolare<visione_crepuscolare>`

:4° livello: 
    :Taglia: Media
    :Attacchi: *Morso* (1d6) e 2 *Artigli* (1d4)
    :Caratteristiche: For +4, Des –2, Cos +2

Uccello (Aquila/Falco/Gufo)
******************************
:Statistiche iniziali: 
    :Taglia: Piccola
    :Velocità: 3m, Volare 24m (media)
    :CA: +1 Armatura Naturale
    :Attacchi: *Morso* (1d4) e 2 *Speroni* (1d4)
    :Caratteristiche: For 10, Des 15, Cos 12, Int 2, Sag 14, Car 6
    :Speciali: :ref:`Visione Crepuscolare<visione_crepuscolare>`

:4° livello: 
    :Caratteristiche: For +2, Cos +2

.. _guerriero:

Guerriero
==================================

Signori del campo di battaglia, i guerrieri sono individui di ogni genere, addestrati all’uso di molte armi o di una sola, allenati ad indossare sempre l’armatura, esperti delle tecniche di combattimento più esotiche e delle strategie militari, tutto per diventare armi viventi. 

I guerrieri sono specialisti della battaglia che rivelano la letalità delle loro armi, trasformando semplici pezzi di metallo in armi capaci di soggiogare regni, trucidare mostri e sollevare i cuori degli eserciti. Soldati, cavalieri, cacciatori e artisti della guerra, i guerrieri sono campioni senza pari e guai a chi osa sfidarli.

:Ruolo: 

    I guerrieri eccellono nel combattimento, sconfiggendo i nemici, dominando la sorte di una battaglia e sopravvivendo a tali attacchi essi stessi.

    Le armi e le tecniche specifiche che padroneggiano permettono loro di usare una grande varietà di tattiche, e pochi possono eguagliare i guerrieri per il valore in battaglia.

.. figure:: Immagini/Guerriero.png
    :align: right
    :width: 400

:Allineamento: Qualsiasi
:Dado Vita: d10

----------------------------
Abilità di classe
----------------------------

:Crescita per livello: 2 + modificatore Intelligenza (Mod Int)

*	:ref:`Addestrare Animali (Car)<addestrare_animali>`
*	:ref:`Artigianato (Int)<artigianato>`
*	:ref:`Cavalcare (Des)<cavalcare>`
*	:ref:`Conoscenze (Dungeon, Ingegneria) (Int)<conoscenze>`
*	:ref:`Intimidire (Car)<intimidire>`
*	:ref:`Nuotare (For)<nuotare>`
*	:ref:`Professione (Sag)<professione>`
*	:ref:`Scalare (For)<scalare>`
*	:ref:`Sopravvivenza (Int)<sopravvivenza>`

-----------------------------
Privilegi di Classe
-----------------------------

.. contents:: Riassumendo:
    :local:

Competenze nelle armi e armature
---------------------------------

Tutte le armi semplici e da guerra, tutte le armature (pesanti, medie e leggere) e negli scudi (incluso lo scudo torre).

Talenti Bonus
-------------------------

Al 1° livello, e successivamente ad ogni livello pari, il guerriero riceve un talento bonus in aggiunta a quelli ottenuti per l’avanzamento del personaggio (il che vuol
dire che un guerriero ottiene un talento bonus ad ogni livello).

Questi talenti bonus devono essere tratti da quelli indicati come talenti di combattimento, chiamati anche *talenti bonus del guerriero*.

Una volta raggiunto il 4° livello, e successivamente ogni quattro livelli (8°, 12°, e così via), un guerriero può scegliere di imparare un nuovo talento bonus al posto di un talento bonus che ha già, perdendo quel talento in cambio di uno nuovo. 
Il vecchio talento non può essere uno che è utilizzato come prerequisito per altro e la scelta deve avvenire nel momento in cui guadagna un nuovo talento bonus per il livello.

.. figure:: Immagini/AvanzamentoGuerriero.png
    :align: center

Audacia (Str)
----------------

.. figure:: Immagini/Guerriero2.png
    :align: left
    :width: 400

:Liv Min: 2

Bonus +1 ai tiri salvezza su Volontà contro paura. Questo bonus aumenta di +1 ogni quattro livelli.

Addestramento nelle armature (Str)
-------------------------------------

:Liv Min: 3


Si riduce la penalità di armatura alla prova di 1 (fino a 0) e aumenta il bonus di Destrezza massimo permesso dalla sua armatura di 1. 

Ogni 4 livelli (7°, 11° e 15°), questi bonus aumentano di +1, fino ad un massimo di riduzione della penalità di armatura alla prova di –4 e un massimo di aumento del bonus di Destrezza massimo permesso di +4.

Inoltre, un guerriero si può muovere alla sua normale velocità mentre indossa un’armatura media. 

Al 7° livello, un guerriero può muoversi alla sua normale velocità quando
indossa un’armatura pesante.

Addestramento nelle armi (Str)
---------------------------------

:Liv Min: 5

Un guerriero può selezionare un gruppo di armi. 
Ogni volta che attacca con un’arma di questo gruppo, riceve bonus +1 al tiro per colpire e per i danni.

Ogni quattro livelli (9°, 13° e 17°), un guerriero si può addestrare in un altro gruppo di armi. In aggiunta, i bonus concessi dai gruppi di armi precedenti aumentano ciascuno di +1. 

Per esempio, quando un guerriero raggiunge il 9° livello, riceve bonus +1 ai tiri per colpire e per i danni con un gruppo di armi e bonus +2 ai tiri per colpire e per i danni con il gruppo di armi selezionato al 5° livello. 

**I bonus concessi dai gruppi sovrapposti non si cumulano**, si prende il bonus più alto concesso per un’arma se questa sta in due o più gruppi.

Si aggiunge questo bonus a tutte le prove di manovra in combattimento effettuate con le armi di questo gruppo. Questo bonus si applica anche alla Difesa da manovra in combattimento del guerriero quando si difende da tentativi di disarmare e spezzare effettuati contro le armi di questo gruppo.

I gruppi di armi sono definiti come segue (il GM può aggiungere altre armi a questi gruppi, o aggiungere gruppi completamente nuovi):

:Archi: 
    Arco Corto, Arco Corto Composito, Arco Lungo, Arco Lungo Composito.
:Armi a Lama Leggera: 
    Astrum, Falcetto, Kama, Kukri, Pugnale, Spada corta e Stocco.
:Armi a Lama Pesante: 
    Falce, Falchion, Scimitarra, Spada Bastarda, Spada Elfica, Spada Lunga, Spadone e Spada a due lame.
:Armi ad Asta: 
    Alabarda, Corsesca, Falcione e Giusarma.
:Armi da Combattimento Ravvicinato: 
    Armatura chiodata, Colpo senz’armi, Guanto d’arme, Guanto d’arme Chiodato, Manganello, Pugnale da mischia, Scudo Chiodato, Scudo Leggero e Scudo Pesante.
:Armi da Lancio: 
    Ascia da Lancio, Astrum, Bastone Fionda Halfling, Bolas, Cerbottana, Dardi, Fionda, Giavellotto, Lancia, Lancia Corta, Martello Leggero, Pugnale, Randello, Rete, Shuriken e Tridente.
:Armi da monaco: 
    Bastone Ferrato, Colpo senz’armi, Kama, Nunchaku, Sai, Shuriken e Siangham.
:Armi doppie: 
    Bastone Ferrato, Doppia Ascia Orchesca, Martello-Picca Gnomesco, Mazzafrusto Doppio, Spada a Due Lame e Urgrosh Nanico.
:Asce: 
    Ascia, Ascia da Battaglia, Ascia da Guerra Nanica, Ascia da Lancio, Ascia Grande, Doppia Ascia Orchesca, Piccone Leggero e Piccone Pesante.
:Armi naturali: 
    Colpo senz’armi e tutte le armi naturali, quali Morso, Artiglio, Corno, Coda e Ala.
:Balestre: 
    Balestra a Mano, Balestra a Ripetizione Leggera, Balestra Leggera, Balestra a Ripetizione Pesante e Balestra Pesante.
:Lance: 
    Giavellotto, Lancia da Cavaliere, Lancia, Lancia Corta, Lancia Lunga e Tridente.
:Martelli: 
    Martello da Guerra, Martello Leggero, Mazza Leggera, Mazza Pesante, Randello e Randello Grande.
:Mazzafrusti: 
    Catena Chiodata, Frusta, Mazzafrusto, Mazzafrusto Doppio, Mazzafrusto Pesante, Morning Star e Nunchaku.

Padronanza dell'armatura (Str)
-------------------------------

:Liv Min: 19

RD 5/— se si indossa l’armatura o porta lo scudo.

Padronanza dell'arma (Str)
----------------------------

:Liv Min: 20

Il guerriero sceglie un’arma, come la spada lunga, l’ascia grande o l’arco lungo.

Ogni attacco effettuato con quell’arma conferma automaticamente tutte le minacce di critico e il moltiplicatore al danno aumenta di 1 (×2 diventa ×3, per esempio). 

Non si può essere disarmati mentre si impugna un’arma di questo tipo.

.. _ladro:

Ladro
==================================

Sempre un passo avanti al pericolo, i ladri sbarcano il lunario grazie ad astuzia, abilità e fascino per piegare il destino a loro favore. Non sapendo mai cosa aspettarsi, sono sempre pronti a tutto e diventano maestri di una vasta gamma di abilità. 

Furfanti e giocatori d’azzardo, diplomatici e loquaci, banditi e cacciatori di taglie,
esploratori ed investigatori, tutti potrebbero essere considerati dei ladri, così come innumerevoli altri professionisti che contano sull’ingegno, il valore o la fortuna. 

Molti ladri preferiscono le città e le innumerevoli opportunità della civiltà, ma alcuni abbracciano la vita da strada, viaggiando lontano, incontrando molte genti ed affrontando incredibili pericoli nell’inseguire altrettanto incredibili ricchezze. 

Chiunque desideri plasmare il proprio destino e la propria vita potrebbe essere chiamato *ladro*.

:Ruolo: 

    I ladri eccellono nel muoversi senza esser visti e cogliere i nemici di sorpresa, e tendono ad evitare il combattimento corpo a corpo; con molteplici abilità e capacità che gli permette di essere versatili, e di differenziarsi notevolmente gli uni con gli altri.

    La maggior parte eccelle nel superare ostacoli di tutti i tipi: aprire porte, disattivare trappole, oltrepassare con astuzia pericoli magici e imbrogliare avversari poco attenti.

.. figure:: Immagini/Ladro.png
    :align: right
    :width: 400

:Allineamento: Qualsiasi
:Dado Vita: d8

----------------------------
Abilità di classe
----------------------------

:Crescita per livello: 8 + modificatore Intelligenza (Mod Int)

*	:ref:`Acrobazia (Des)<acrobazia>`
*	:ref:`Artigianato (Int)<artigianato>`
*	:ref:`Artista della Fuga (Des)<artista_della_fuga>`
*	:ref:`Camuffare (Car)<camuffare>`
*	:ref:`Conoscenze (dungeon, locali) (Int)<conoscenze>`
*	:ref:`Diplomazia (Car)<diplomazia>`
*	:ref:`Disattivare Congegni (Des)<disattivare_congegni>`
*	:ref:`Furtività (Des)<furtività>`
*	:ref:`Intimidire (Car)<intimidire>`
*	:ref:`Intrattenere (Car)<intrattenere>`
*	:ref:`Intuizione (Sag)<intuizione>`
*	:ref:`Linguistica (Int)<linguistica>`
*	:ref:`Nuotare (For)<nuotare>`
*	:ref:`Percezione (Sag)<percezione>`
*	:ref:`Professione (Sag)<professione>`
*	:ref:`Raggirare (Car)<raggirare>`
*	:ref:`Rapidità di Mano (Des)<rapidità_di_mano>`
*	:ref:`Scalare (For)<scalare>`
*	:ref:`Utilizzare Congegni Magici (Car)<utilizzare_congegni_magici>`
*	:ref:`Valutare (Int)<valutare>`

--------------------
Privilegi di Classe
--------------------

.. contents:: Riassumendo:
    :local:

Competenze nelle armi e armature
--------------------------------

Tutte le armi semplici più arco corto, balestra a mano, manganello, spada corta e stocco. Armature leggere, ma non scudi.

.. _attacco_furtivo_ladro:

Attacco Furtivo
------------------

Se un ladro è in grado di sorprendere un avversario quando non è in grado di difendersi efficacemente dal suo attacco, egli può colpire un punto vitale per infliggere danni aggiuntivi, in caso possa vederlo bene e raggiungerlo.

L’attacco infligge danni aggiuntivi anche ogni volta che al bersaglio del ladro viene negato il bonus di Destrezza alla CA, o quando il ladro attacca il bersaglio ai fianchi. 

I danni sono 1d6 al 1° livello + 1d6 ogni due livelli da ladro successivi. Se il ladro dovesse infliggere un **colpo critico** con un attacco furtivo, questi danni aggiuntivi **non vengono moltiplicati**. 

Gli attacchi a distanza possono contare come attacchi furtivi solo se il bersaglio si trova entro 9 metri.

Con un’arma che infligge danni non letali il ladro può compiere un attacco furtivo che infligge danni non letali invece che danni letali. Non può usare un’arma che infligge danni letali per provocare danni non letali in un **attacco furtivo**, neanche con la solita penalità –4.

Il ladro non può compiere un *attacco furtivo* mentre colpisce una creatura con occultamento.

.. _avanzamento_ladro:

.. figure:: Immagini/AvanzamentoLadro.png
    :align: center

Doti da ladro
----------------

:Liv Min: 2

Il ladro apprende un numero di capacità che lo aiutano e
confondono i nemici. A partire dal 2° livello, un ladro ottiene una dote da ladro.
Successivamente, ne ottiene una addizionale ogni 2 livelli da ladro.

Un ladro può scegliere una dote specifica soltanto una volta. 

Le doti contrassegnate con un asterisco aggiungono degli effetti all’:ref:`Attacco Furtivo<attacco_furtivo_ladro>`. Solo una di queste doti può essere applicata ad uno specifico attacco e la decisione deve essere effettuata prima che l’attacco venga compiuto.

.. contents:: Lista delle doti:
    :local:

Accuratezza
***************
:ref:`Arma Accurata<tal_arma_accurata>` come talento bonus.

Acutezza percettiva (Str)
***************************
Ogni volta che un ladro con questa dote si avvicina entro 3 metri da una trappola, riceve immediatamente una prova di :ref:`Percezione<percezione>` per individuarla che dovrebbe essere tirata dal *GM* di nascosto.

Addestramento in un’arma
************************
:ref:`Arma Focalizzata<tal_arma_focalizzata>` come talento bonus.

Attacco improvviso (Str)
*************************
Durante il round di sorpresa, gli avversari sono sempre considerati impreparati per un ladro con questa dote, anche se hanno già agito.

Attacco sanguinante* (Str)
****************************
Gli avversari viventi sanguinano colpendoli con un :ref:`Attacco Furtivo<attacco_furtivo_ladro>`. 

Questo attacco provoca al bersaglio 1 danno addizionale all'inizio del loro turno ogni round per ogni dado dell’:ref:`Attacco Furtivo<attacco_furtivo_ladro>` (per esempio, 4d6 provocano 4 danni di sanguinamento). 

Il sanguinamento può essere fermato con una prova di :ref:`Guarire<guarire>` con **CD = 15** o applicando qualsiasi effetto in grado di curare i danni. 

Il danno da sanguinamento provocato da questa dote non si cumula, e supera qualsiasi riduzione del danno che la creatura potrebbe possedere.

Disattivare rapido (Str)
*************************
Il tempo normalmente necessario per disattivare una trappola usando :ref:`Disattivare Congegni<disattivare_congegni>` si dimezza (minimo 1 round).

Equilibrista (Str)
*******************
Questa dote permette di muoversi lungo le superfici strette alla sua piena velocità usando :ref:`Acrobazia<acrobazia>` senza subire alcuna penalità, compreso non poter essere colto impreparato.

Espediente
************
:ref:`Talento di Combattimento<talenti_di_combattimento>` bonus a scelta.


Furtività rapida (Str)
**********************
Ci si può muovere a piena velocità usando :ref:`Furtività<furtività>` senza subire penalità.

.. _magia_maggiore_ladro:

Magia maggiore (Mag)
**********************
:Prerequisiti:
    :ref:`Magia Minore<magia_minore_ladro>` |break|
    Int ≥ 11

Fornisce la capacità di lanciare un incantesimo di 1° livello dalla lista degli :ref:`Incantesimi di Mago/Stregone<incantesimi_mago>` due volte al giorno come capacità magica. 

Il livello dell’incantatore per questa capacità è pari al livello da ladro, con una CD del tiro salvezza di 11 + Mod Int. 

.. _magia_minore_ladro:

Magia minore (Mag)
*******************
:Prerequisito: Int ≥ 10

Fornisce la capacità di lanciare un incantesimo di livello 0 dalla lista degli :ref:`Incantesimi di Mago/Stregone<incantesimi_mago>` tre volte al giorno come capacità magica. 

Il livello dell’incantatore per questa capacità è pari a livello da ladro, con una CD del tiro salvezza di 10 + Mod Int. 

Reazioni lente* (Str)
**********************
Gli avversari colpiti dall’:ref:`Attacco Furtivo<attacco_furtivo_ladro>` non possono effettuare attacchi di opportunità per 1 round.

Recupero (Str)
****************
:Tipo: Azione Immediata

Una volta al giorno, un ladro con questa dote può guadagnare per 1 min un certo numero di punti ferita temporanei uguali al proprio livello da ladro. 

Questa dote può essere usata soltanto quando si è ridotti a meno di 0 punti ferita e può essere usata per evitare di morire.

Se i punti ferita sono ridotti a meno di 0 per la perdita di questi punti ferita temporanei, il ladro cade privo di sensi ed è morente come di norma.

Rialzarsi (Str)
*****************
Permette di rialzarsi in piedi con un’azione gratuita. 

Ciò provoca sempre un attacco di opportunità per rialzarsi se si è minacciati da un nemico.

Strisciare furtivo (Str)
*************************
Permetto di muoversi a metà della propria velocità da prono. Questo movimento provoca
attacchi di opportunità come di norma. 

Un ladro con questa dote può compiere un passo di 1,5 metri mentre striscia.

-------------------------------------------------

.. figure:: Immagini/Ladro2.png
    :align: left
    :width: 400

Scoprire Trappole
-------------------

\+ 0.5 * Liv (minimo +1) alle prove di :ref:`Percezione<percezione>` per localizzare le trappole e alle prove di :ref:`Disattivare Congegni<disattivare_congegni>`. 

Un ladro può usare l’abilità :ref:`Disattivare Congegni<disattivare_congegni>` per disarmare trappole magiche.

.. _eludere_ladro:

Eludere (Str)
--------------
:Liv Min: 2

Evitare anche gli attacchi magici e insoliti. 

Se supera il tiro salvezza su Riflessi contro un attacco che normalmente infliggerebbe la metà dei danni in caso di riuscita del tiro salvezza, non subisce alcun danno.

La capacità di eludere può essere usata solo se il ladro non indossa armature o ne indossa una leggera. Un ladro indifeso non riceve i benefici di eludere.

Percepire trappole (Str)
------------------------
:Liv Min: 3

+1 ai tiri salvezza su Riflessi effettuati per evitare trappole e bonus di schivare +1 alla CA contro gli attacchi effettuati da trappole. 

Questi bonus diventano +2 quando il ladro raggiunge il 6° livello, +3 quando raggiunge
il 9°, +4 al 12°, +5 al 15° e +6 al 18°. 

I bonus di percepire trappole ottenuti da più classi sono cumulativi.

.. _schivare_prodigioso_ladro:

Schivare prodigioso (Str)
------------------------------
:Liv Min: 4

Non si può essere colti impreparati, anche se l’attaccante è invisibile. 
Si perde comunque il bonus di Destrezza alla CA se si viene immobilizzati o se si subisce un'azione di :ref:`Fintare<fintare>`. 

Se il ladro possiede già schivare prodigioso come capacità di un’altra classe, guadagna automaticamente :ref:`schivare prodigioso migliorato<schivare_prodigioso_migliorato_ladro>`.

.. _schivare_prodigioso_migliorato_ladro:

Schivare prodigioso migliorato (Str)
---------------------------------------
:Liv Min: 8

Non si può essere attaccati ai fianchi. 

Questa difesa nega ad altri ladri la possibilità di utilizzare :ref:`attacco furtivo<attacco_furtivo_ladro>` attaccando ai fianchi, a meno che non abbiano 4 livelli da ladro in più.

Se un personaggio ha già :ref:`schivare prodigioso<schivare_prodigioso_ladro>` da un’altra classe, i livelli delle classi che forniscono schivare prodigioso si sommano per determinare il livello minimo che un ladro deve avere per potere attaccare il personaggio ai fianchi.

Doti Avanzate
-----------------------------
:Liv Min: 10

Raggiunto il 10° livello e ad ogni due livelli successivi, un ladro può scegliere una dote avanzata, al posto di una normale.

.. contents:: Lista delle Doti Avanzate:
    :local:

Attacco Dissolvente* (Sop)
***************************
:Prerequisito: :ref:`Magia Maggiore<magia_minore_ladro>`

Un avversario che ha subito danni dall’:ref:`Attacco Furtivo<attacco_furtivo_ladro>` con questa dote è bersaglio di :ref:`Dissolvi Magie<inc_dissolvi_magie>`, che ha effetto sull’incantesimo di livello più basso attivo sul bersaglio. 

Il livello dell’incantatore per questa dote è pari al livello del ladro. 

Attutire il Colpo (Str)
************************
Si può attutire un colpo potenzialmente letale per subire meno danni del normale.

Una volta al giorno, quando un ladro rischia di essere ridotto a zero o meno punti ferita per danni subiti in combattimento (non da un incantesimo o da una capacità speciale), può tentare di accompagnare il colpo. 

Serve un tiro salvezza su Riflessi (CD = danno subito).

Il ladro deve essere consapevole dell’attacco ed in grado di reagire per poter attutire il colpo; se gli è negato il bonus di Destrezza alla CA, non può
utilizzare questa dote. 

Siccome questo effetto non avrebbe normalmente consentito al personaggio un tiro salvezza su Riflessi per dimezzare i danni, la capacità di eludere del ladro non si applica ad attutire il colpo.

Colpo menomante* (Str)
************************

L':ref:`Attacco Furtivo<attacco_furtivo_ladro>` ha una precisione tale che i suoi
colpi indeboliscono e ostacolano l’avversario. 

L’avversario danneggiato subisce anche 2 danni a Forza.

Eludere migliorato (Str)
*************************

Questa dote funziona come :ref:`Eludere<eludere_ladro>` tranne che subisce solo la metà dei danni se fallisce il tiro salvezza. 

Un ladro indifeso non riceve i benefici di eludere migliorato.

Mente sfuggente (Str)
**********************
Questa capacita rappresenta la capacita del ladro di liberarsi dagli effetti magici che altrimenti lo porrebbero sotto controllo o compulsione. 

Quando si fallisce un tiro salvezza contro ammaliamento si può tentare di nuovo (una sola volta) il tiro dopo 1 round, con la stessa CD.

.. _opportunismo:

Opportunismo (Str)
********************
Permette di compiere un attacco di opportunità contro un avversario a cui sono stati appena inflitti danni in mischia da un altro personaggio, una volta per round. 

Questo attacco viene considerato come un attacco di opportunità del ladro per quel round, anche nel caso in cui si abbia :ref:`Riflessi in Combattimento<tal_riflessi_in_combattimento>` non si può utilizzare questa dote più di una volta per round.

Padronanza di un’abilità
*************************
Il ladro diventa così sicuro nell’uso di certe abilità che può usarle in modo affidabile anche in condizioni avverse. 

Il ladro seleziona un numero di abilità pari a 3 + Mod Int e quando effettua una prova
in una di queste abilità può prendere 10 anche se tensione e distrazioni normalmente glielo impedirebbero. 

Questa dote si può **acquisire più volte** selezionando abilità addizionali a cui
applicarla ogni volta.

Talento
**********
Un ladro può acquisire un talento di cui soddisfi i prerequisiti al posto di una dote da ladro.

------------------------------------

Colpo da maestro (Str)
-----------------------
:Liv Min: 20

Ogni volta che un ladro infligge danno con un :ref:`Attacco Furtivo<attacco_furtivo_ladro>`, può scegliere uno dei tre seguenti effetti sul bersaglio:

*	addormentare per 1d4 ore
*	paralizzare per 2d6 round
*	uccidere

Il bersaglio riceve un tiro salvezza su Tempra per negare l’effetto addizionale, con **CD = 10 + 0.5*Liv Ladro + Mod Int**. 

Una volta che una creatura è stata bersaglio di un *Colpo da Maestro* ne è immune per
24 ore. Le creature immuni ai danni da :ref:`Attacco Furtivo<attacco_furtivo_ladro>` sono immuni anche a questa capacità.

.. _mago:

Mago
==================================

Oltre il velo del mondo si nascondono i segreti del potere
assoluto. Tali misteri sono un richiamo per chi ha l’ambizione e l’intelletto per elevarsi sulla gente comune per afferrare la vera forza. Questa
è la via del mago.

Questi sagaci incantatori cercano, raccolgono e scoprono conoscenze esoteriche, attingendo alle arti occulte per creare meraviglie oltre le capacità dei semplici mortali. 

Alcuni potrebbero scegliere uno specifico campo di studi magici e padroneggiarne i poteri, altri abbracciano la versatilità, godendo delle meraviglie illimitate di tutta la magia. 

I maghi si dimostrano abili e potenti, capaci di annientare i loro nemici, potenziare gli alleati e plasmare il mondo ai loro desideri.

:Ruolo: 

    I maghi generici studiano per prepararsi a qualsiasi tipo di pericolo, mentre i maghi specialisti ricercano le scuola di magia che li rende particolarmente abili all’interno di un campo peculiare.

    A prescindere dalla specializzazione, tutti i maghi sono maestri dell’impossibile e possono aiutare i loro alleati nel superare qualsiasi genere di sfida.

.. figure:: Immagini/Mago.png
    :align: right
    :width: 400

:Allineamento: Qualsiasi
:Dado Vita: d6

----------------------------
Abilità di classe
----------------------------

:Crescita per livello: 2 + modificatore Intelligenza (Mod Int)

*	:ref:`Artigianato (Int)<artigianato>`
*	:ref:`Conoscenze (tutte) (Int)<conoscenze>`
*	:ref:`Linguistica (Int)<linguistica>`
*	:ref:`Professione (Sag)<professione>`
*	:ref:`Sapienza Magica (Int)<sapienza_magica>`
*	:ref:`Valutare (Int)<valutare>`
*	:ref:`Volare (Des)<volare>`

--------------------
Privilegi di Classe
--------------------

.. contents:: Riassumendo:
    :local:

Competenze nelle armi e armature
----------------------------------

Balestra leggera, balestra pesante, bastone ferrato, pugnale e randello. 

Nessun tipo di armatura o scudo perchè interferiscono con i movimenti del mago e può provocare il fallimento degli incantesimi con componenti somatiche.

Incantesimi
--------------------

.. _avanzamento_mago:

.. figure:: Immagini/AvanzamentoMago.png

:Lista: :ref:`Incantesimi da Mago<incantesimi_mago>`
:Preparazione: Sì

Per imparare, preparare o lanciare un incantesimo, è necessaria un'Intelligenza di almeno 10 + Liv Incantesimo. 
La CD per un tiro salvezza contro un incantesimo da mago è 10 + il Liv Incantesimo + Mod Int.

Come gli altri incantatori, il mago ha un :ref:`numero limitato di incantesimi al giorno<avanzamento_mago>` che può essere modificata a seconda del Mod Int, :ref:`com'è usuale<incantesimi_bonus>`.

Un mago può conoscere qualsiasi numero di incantesimi e per prepararli deve dormire almeno 8 ore e impiegarne 1 nello studio del suo libro di incantesimi. Mentre studia il mago decide quali incantesimi preparare.

Linguaggi Bonus
--------------------

Un mago può sostituire il Draconico ad uno dei linguaggi bonus a disposizione del personaggio per la sua razza.

.. _legame_arcano:

Legame Arcano (Str o Mag)
--------------------------
Al 1° livello, i maghi formano un legame potente con un oggetto o una creatura. 

Questo legame può assumere una di due forme: un :ref:`Famiglio<famiglio_mago>` o un
:ref:`Oggetto Legato<oggetto_legato_mago>`. 

Una volta che un mago effettua la scelta, non può più cambiarla. 

.. contents:: Riassumendo:
    :local:

.. _famiglio_mago:

.. figure:: Immagini/Famiglio.png
    :align: right
    :width: 400

Famiglio
***************
Un *Famiglio* è un animale domestico magico che potenzia le abilità ed i sensi del mago e può aiutarlo nella magia. Animali scelti dagli incantatori perché gli siano
d’aiuto nello studio della magia. 

Legato magicamente al suo padrone, un **famiglio è un normale animale** che mantiene
aspetto, Dadi Vita, bonus di attacco base, bonus ai tiri salvezza base, abilità e talenti del normale animale che era, ma viene trattato come bestia magica al fine di determinare qualsiasi effetto che dipenda dal suo tipo. 
Un compagno animale non può fungere anche da famiglio.

Un famiglio conferisce delle :ref:`Capacità Speciali<famiglio_mago_tabella>` al suo padrone che si applicano solo quando il padrone e il famiglio sono entro 1,5 km l’uno dall’altro.

I livelli delle diverse classi che permettono di ottenere un famiglio si cumulano ai fini di determinare le capacità del famiglio che dipendono dal livello del padrone.

Se il **famiglio muore o è perduto**, può essere sostituito 1 settimana dopo con uno speciale rituale che costa 200 mo per livello da mago, il cui completamento richiede 8 ore.

*****************
Basi del famiglio
*****************

Le statistiche base di una creatura della specie del famiglio vanno recuperate dal :ref:`Bestiario<bestiario>` apportando i seguenti cambiamenti.

:Dadi Vita: 
    Per gli effetti legati al numero dei Dadi Vita, va utilizzato il più alto tra il Liv del Padrone o il numero di DV del famiglio.
:Punti ferita: 
    Il famiglio ha metà dei punti ferita totali (non temporanei) del padrone, arrotondati per difetto, quali che siano i suoi attuali Dadi Vita.
:Attacchi: 
    Va usato il *Bonus di Attacco Base* del padrone e il più alto tra il Mod Destrezza e Forza del famiglio per calcolare il bonus di attacco in mischia del famiglio con le armi naturali.

    Il danno è uguale a quello di una normale creatura della specie del famiglio.
:Tiri Salvezza: 
    Per ogni tiro salvezza, utilizzare i più alti tra i bonus del famiglio (Tempra +2, Riflessi +2, Volontà +0) o quelli del padrone.

    Il famiglio applica i suoi modificatori di caratteristica ai tiri salvezza, e non condivide nessuno dei bonus che il suo padrone potrebbe ricevere ai propri tiri salvezza.
:Abilità: 
    Per ogni abilità in cui il padrone o il famiglio possiede dei gradi vanno utilizzati i più alti tra i gradi del famiglio o del padrone

    In entrambi i casi, il famiglio applica il proprio Mod di caratteristica.
    Alcune abilità possono risultare comunque inutilizzabili dalla creatura.

    I famigli considerano :ref:`Acrobazia<acrobazia>`, :ref:`Furtività<furtività>`, :ref:`Nuotare<nuotare>`, :ref:`Percezione<percezione>`, :ref:`Scalare<scalare>` e :ref:`Volare<volare>` come abilità di classe.

.. _famiglio_mago_tabella:

.. figure:: Immagini/MagoFamiglio.png

    A sinistra l'elenco delle Capacità Speciali che un famiglio trasmette al padrone. A destra le Capacità del famiglio stesso.

***********************
Capacità del famiglio
***********************
Tutti i famigli possiedono :ref:`Capacità Speciali<famiglio_mago_tabella>` a seconda dei livelli combinati del padrone nelle classi che concedono i famigli. 

Le capacità elencate nella :ref:`tabella<famiglio_mago_tabella>` sono cumulative.

:Mod armatura naturale: 
    Il numero indicato qui è in aggiunta al bonus di armatura naturale esistente del famiglio.
:Int:
    Il punteggio di Intelligenza del famiglio.
:Allerta (Str): 
    Quando il famiglio è a portata di braccio dal padrone, questi guadagna :ref:`Allerta<tal_allerta>`.
:Condividere incantesimi (Str):
    A propria discrezione, il padrone può lanciare qualsiasi incantesimo sul suo famiglio (come un incantesimo a contatto), anzichè se stesso.

    Il padrone può lanciare sul suo famiglio incantesimi anche se questi normalmente non hanno effetto su creature del tipo del famiglio (bestie magiche).
:Eludere migliorato (Str): 
    Se il famiglio è soggetto a un attacco che permette un tiro salvezza su Riflessi per dimezzare i danni, il famiglio non subisce danni se supera il TS e solo la metà dei danni se fallisce il tiro salvezza.
:Legame empatico (Sop): 
    Il padrone ha un *Legame Empatico* con il suo famiglio fino a una distanza di 1,5 km e può comunicare telepaticamente con esso, senza però vedere attraverso i suoi occhi.

    Si possono comunicare **solo emozioni generiche**.

    Grazie al *Legame Empatico* il padrone ha la stessa connessione del famiglio con un oggetto o un luogo.
:Trasmettere incantesimi a contatto (Sop): 
    :Liv Min: 3

    Il famiglio può trasmettere incantesimi a contatto al posto del padrone.

    Se il padrone e il famiglio sono in contatto quando il padrone lancia un incantesimo a contatto, egli può designare il suo famiglio come *colui che crea il contatto*, che può quindi trasmettere l’incantesimo come il padrone.

    Se il padrone lancia un altro incantesimo prima che il contatto venga effettuato, l’incantesimo a contatto si dissolve.
:Parlare con il padrone (Str): 
    :Liv Min: 5

    Il famiglio e il padrone possono comunicare verbalmente, come se utilizzassero un linguaggio comune, ma sono gli unici a comprendere la loro conversazione, se non utilizzando ausili magici.
:Parlare con animali della sua specie (Str): 
    :Liv Min: 7

    Il famiglio è in grado di comunicare con animali della sua specie generica (incluse le varianti crudeli): pipistrelli con pipistrelli, topi con roditori, gatti con felini, falchi e gufi e corvi con uccelli, serpenti e lucertole con rettili, rospi con anfibi, scimmie con altri primati, donnole con ermellini e mustelidi.

    La comunicazione è limitata dall’Intelligenza delle creature con cui il famiglio conversa.
:Resistenza agli incantesimi (Str): 
    :Liv Min: 11

    Il famiglio diventa resistente agli incantesimi. pari al livello del padrone +5.

    Se un altro incantatore tenta di colpire il famiglio con un incantesimo, deve effettuare una prova di livello dell’incantatore (1d20 + Liv incantatore) con **CD = Liv padrone +5**.
:Scrutare sul famiglio (Mag):
    :Liv Min: 13

    Si può scrutare attraverso il famiglio (come lanciando l’incantesimo :ref:`Scrutare<inc_scrutare>`) una volta al giorno.

.. _oggetto_legato_mago:

Oggetto Legato
***************
Un *Oggetto Legato* è un elemento che il mago può usare per lanciare gli incantesimi addizionali o come oggetto magico.

I maghi che scelgono un oggetto legato iniziano il gioco con un oggetto senza alcun costo aggiuntivo, che deve essere scelto da una delle seguenti categorie: amuleto, anello, bastone, bacchetta o arma. Questi oggetti sono sempre qualitativamente
perfetti. 

Le armi acquistate al 1° livello non sono fatte di alcun materiale speciale. Se l’oggetto è un amuleto o un anello, deve essere indossato per avere effetto, mentre bastoni, bacchette e armi devono essere impugnati. Se l’oggetto è un anello o un amuleto, occupa di conseguenza lo
slot del collo o dell’anello.

Se un mago tenta di lanciare un incantesimo senza il suo oggetto legato indosso o impugnato, deve effettuare una prova di concentrazione, con **CD = 20 + Liv Incantesimo**. 

Una volta al giorno si può usare l'*oggetto legato* per **lanciare qualsiasi incantesimo** che il mago abbia nel suo libro degli incantesimi e che può lanciare (non della :ref:`Scuola Arcana<scuola_arcana_mago>` opposta), non preparato. Questo incantesimo è trattato come qualunque altro incantesimo lanciato dal mago e non può essere modificato da talenti metamagici o da altre capacità. 

Si possono **aggiungere capacità** magiche addizionali all'oggetto legato come se si avessero i talenti di creazione oggetto richiesti e se ne soddisfano i prerequisiti del talento. Per esempio, un mago con un pugnale legato deve essere almeno di 5° livello per aggiungere capacità magiche al pugnale con il talento :ref:`Creare Armi e rmature Magiche<tal_creare_armi_e_armature_magiche>`.

Se l’oggetto legato è una bacchetta, perde le sue capacità da bacchetta quando viene consumata la sua ultima carica, ma non si distrugge e mantiene tutte le sue proprietà di oggetto legato e può essere usata per creare una nuova bacchetta. 

Le proprietà magiche di un oggetto legato, comprese tutte le capacità magiche aggiunte all’oggetto, funzionano soltanto per il mago che lo possiede. Se il proprietario muore, o l’oggetto è sostituito, esso ritorna ad essere un comune oggetto perfetto del suo tipo.

Se un oggetto legato è **danneggiato**, viene riparato ai suoi punti ferita massimi la volta successiva in cui il mago prepara i suoi incantesimi. Se l’oggetto legato viene **perso o distrutto**, può essere sostituito dopo 1 settimana con uno speciale rituale che costa 200 mo per livello del mago più il costo dell’oggetto nella sua versione perfetta. Questo rituale richiede 8 ore per essere completato. 
Gli oggetti sostituiti non possiedono nessuna delle capacità magiche addizionali che il mago ha aggiunto all’oggetto precedente. 

Un mago può designare un oggetto magico esistente come oggetto legato così come lo sostituisce salvo che il nuovo oggetto magico mantiene le sue qualità magiche mentre guadagna i benefici e gli svantaggi nel divenire un oggetto legato.

.. _scuola_arcana_mago:

Scuola Arcana
---------------

Al livello 1 un mago deve decidere se specializzarsi in una :ref:`Scuola di Magia<scuole_arcane>`, guadagnando incantesimi e poteri addizionali basati su quella scuola. Se rinuncia a questa scelta si considera appartenere alla scuola universale.

Una volta scelta la scuola di specializzazione si devono selezionare altre **due scuole opposte**, a rappresentare le conoscenze sacrificate in un’area del sapere
arcano per ottenere la padronanza in un altro. 

Un mago che prepara gli incantesimi di una sua scuola opposta deve usare due slot incantesimo di quel livello per preparare l’incantesimo. Per esempio, un mago con invocazione come scuola opposta deve consumare due dei suoi slot incantesimi disponibili di 3° livello per preparare una :ref:`Palla di Fuoco<inc_palla_di_fuoco>`. 

Uno specialista subisce penalità –4 a tutte le prove di abilità effettuate per fabbricare un oggetto magico che abbia un incantesimo di una scuola opposta come prerequisito. 

Un mago universale può preparare gli incantesimi di tutte le scuole senza restrizioni.

Ogni scuola arcana concede al mago un certo numero di poteri specialistici e uno slot incantesimo addizionale di ogni livello di incantesimi che sono in grado di lanciare. Ogni giorno un mago può preparare un incantesimo della sua scuola di specializzazione in quello slot. Questo incantesimo deve essere scritto nel :ref:`Libro degli Incantesimi<libro_mago>` del mago. 

Un mago può selezionare un incantesimo modificato da un talento metamagico da preparare nello slot della scuola, ma utilizza uno slot incantesimo di più alto livello. 

Trucchetti
--------------
I maghi possono preparare un numero di trucchetti, o incantesimi di livello 0, come indicato nella :ref:`Tabella con gli Incantesimi al giorno<avanzamento_mago>`. 

Questi incantesimi sono lanciati come ogni altro, ma non vengono consumati e possono essere utilizzati di nuovo. 

Un mago può preparare un trucchetto da una scuola opposta, ma deve usare due degli slot disponibili.

Scrivere pergamene
-------------------
Talento Bonus :ref:`Scrivere Pergamene<tal_scrivere_pergamene>`.

Talenti bonus
--------------
Al 5°, 10°, 15° e 20° livello un mago acquisisce un talento bonus di metamagia, di creazione oggetto oppure :ref:`Padronanza degli Incantesimi<tal_padronanza_degli_incantesimi>`, a patto che soddisfi tutti i prerequisiti. 

.. _libro_mago:

Libro degli Incantesimi
-----------------------
Un mago non può preparare incantesimi che non sono presenti sul suo libro, tranne :ref:`Lettura del Magico<inc_lettura_del_magico>` che tutti i maghi possono preparare a memoria.

Un mago comincia con un libro degli incantesimi che contiene tutti gli incantesimi da mago di livello 0 (eccetto delle scuole opposte) più 3 incantesimi di 1° livello di sua scelta e  un numero di incantesimi addizionali pari al suo Mod Int. 

Ogni volta che raggiunge un nuovo livello da mago, guadagna due nuovi incantesimi di qualsiasi livello che è in grado di lanciare, da aggiungere nel libro. 

In ogni momento, un mago può aggiungere incantesimi trovati in altri libri degli incantesimi nel suo.

.. figure:: Immagini/Mago2.png
    :align: right
    :width: 400

.. _scuole_arcane:

--------------------
Scuole Arcane
--------------------
.. contents:: Riassumendo:
    :local:

.. _mago_scuola_abiurazione:

Abiurazione
--------------
L’:ref:`Abiuratore<scuola_abiurazione>` utilizza la magia su di sè e padroneggia le arti magiche di difesa e protezione.

Resistenza (Str)
******************
Si ottiene resistenza 5 ad un tipo di energia di propria scelta, selezionato quando si preparano gli incantesimi e che può essere cambiata ogni giorno.

All’11° livello, questa resistenza aumenta a 10 e al 20° cambia in immunità.

Interdizione Protettiva (Sop)
******************************
:Tipo: :ref:`Azione Standard<azione_standard>`

Si può creare in un raggio di 3 metri un campo di magia protettiva centrato su di sè che dura un numero di round pari al Mod Int. 

Tutti gli alleati in questa zona (compreso se stessi) ricevono bonus di deviazione +1 alla CA per 1 round che aumenta di +1 ogni cinque livelli da mago posseduti.

Si può usare questa capacità 3 + Mod Int volte al giorno.

Assorbimento di Energia (Sop)
*****************************
:Liv Min: 6

Si ottiene una quantità di energia assorbita pari a 3 volte il proprio livello da
mago al giorno: ogni volta che si subisce danno da energia, occorre applicare prima immunità, vulnerabilità e resistenze, per poi assorbire il danno restante con questa capacità, riducendo così il proprio totale giornaliero di quell’ammontare. 

Tutto il danno in eccesso viene applicato normalmente.

.. _mago_scuola_ammaliamento:

Ammaliamento
--------------
L’:ref:`Ammaliatore<scuola_ammaliamento>` usa la magia per dominare e manipolare le menti delle sue vittime.

Sorriso Incantevole (Sop)
*************************
+2 alle prove di :ref:`Diplomazia<diplomazia>`, :ref:`Intimidire<intimidire>` e :ref:`Raggirare<raggirare>` che aumenta di +1 ogni cinque livelli da mago posseduti, fino ad un massimo di +6 al 20° livello. 

Al 20° livello, ogni volta che si supera un TS contro un incantesimo di ammaliamento, quell’incantesimo viene riflesso sull’incantatore che lo ha lanciato, come per :ref:`Riflettere Incantesimo<inc_riflettere_incantesimo>`.

Tocco Frastornante (Mag)
*************************
:Tipo: :ref:`Attacco di Contatto in Mischia<azione_attacco_di_contatto_in_mischia>`

Si può rendere una creatura vivente frastornata per 1 round a meno che abbia più Dadi Vita del proprio livello da mago. 

Si può utilizzare questa capacità 3 + Mod Int volte al giorno.

Aura di Disperazione (Sop)
***************************
:Liv Min: 8

Si può emanare un’aura di 9 metri di disperazione per un numero di round al giorno (non necessariamente consecutivi) pari al proprio livello da mago.

Tale aura deprime i nemici che subiscono –2 alle prove di abilità, ai tiri per colpire e per il danno, ai TS e alle prove di caratteristica.

.. _mago_scuola_divinazione:

Divinazione
---------------------
I :ref:`Divinatori<scuola_divinazione>` sono maestri dello scrutamento a distanza, delle profezie e dell’uso della magia per esplorare il mondo.

Prevenuto (Sop)
***************
Si può agire sempre nel :ref:`Round di Sorpresa<round_sorpresa>` anche se si fallisce la prova di :ref:`Percezione<percezione>` per notare un nemico, ma si è considerati comunque :ref:`Impreparati<impreparati>` finché non si compie un’azione. 

Bonus alle prove di iniziativa pari a metà del proprio livello da mago (minimo +1) fino al 20° livello che implica un 20 naturale sulle prove di iniziativa.

Fortuna del Divinatore (Mag)
*****************************

:Tipo: :ref:`Azione Standard<azione_standard>`

Quando si attiva questo potere, si può toccare qualsiasi creatura per concederle un bonus cognitivo a tutti i tiri per colpire, prove di abilità, prove di caratteristica, e TS pari a metà del proprio livello da mago (minimo +1) per 1
round. 

Si può utilizzare questa capacità 3 + Mod Int volte al giorno.

Adepto Scrutatore (Sop)
***********************

:Liv Min: 8

Si sa sempre se si è osservati magicamente, come se si avesse :ref:`Individuazione dello Scrutamento<inc_individuazione_dello_scrutamento>` attivo su di sè in modo permanente.

Ogni volta che si :ref:`Scruta<inc_scrutare>` un soggetto, lo si considera di un passo più familiare e i soggetti molto familiari subiscono -10 ai TS per evitare questi tentativi.

.. _mago_scuola_evocazione:

Evocazione
------------
L’evocatore si concentra sullo studio dell’:ref:`Evocazione<scuola_evocazione>` dei mostri e sul piegare la magia al suo volere.

Fascino dell’Evocatore (Sop)
****************************
Ogni volta che si lancia un incantesimo di evocazione (convocare), si aumenta la durata di un numero di round pari a metà del proprio livello da mago (minimo 1). 

Al 20° livello, si può cambiare la durata di tutti gli incantesimi :ref:`Evoca Mostri<inc_evoca_mostri>` a permanente, ma se si designa un altro incantesimo :ref:`Evoca Mostri<inc_evoca_mostri>` come permanente, l’incantesimo precedente ha termine immediatamente.

Dardo Acido (Mag)
*****************
:Tipo: :ref:`Attacco di Contatto a distanza<azione_attacco_di_contatto_a_distanza>`

Si può rilasciare un dardo acido contro un bersaglio entro 9 metri che infligge *1d6 + 1 ogni due livelli da mago* danni da acido e ignora la resistenza agli incantesimi.

Si può utilizzare 3 + Mod Int volte al giorno.

Passo Dimensionale (Mag)
************************
:Tipo: :ref:`Azione Standard<azione_standard>`
:Liv Min: 8

Ci si può teletrasportare fino a 9 metri per livello da mago al giorno che può essere utilizzato in incrementi di 1,5 metri e non provoca :ref:`Attacchi di Opportunità<azione_attacco_di_opportunità>`. 

Si possono portare con sè altre creature consenzienti, ma occorre spendere una quantità uguale di distanza per ogni creatura aggiuntiva portata.

.. _mago_scuola_illusione:

Illusione
-----------
Gli :ref:`Illusionisti<scuola_illusione>` usano la magia per creare trame, finzioni ed immagini per confondere ed irritare i loro nemici.

Illusioni Estese (Sop)
***********************
Tutti gli incantesimi di illusione lanciati con una durata di “concentrazione” durano un numero di round addizionali pari a metà del proprio livello da mago dopo che si smette di mantenerne la concentrazione (minimo +1 round). 

Al 20° livello, si può rendere un incantesimo di illusione con una durata di “concentrazione” permanente, ma se se ne designa un’altra come permanente quella precedente finisce.

Raggio Accecante (Mag)
**********************
:Tipo: :ref:`Attacco di Contatto a distanza<azione_attacco_di_contatto_a_distanza>`

Si può emanare un raggio luccicante contro qualunque nemico entro 9 metri che rende le
creature accecate per 1 round, a meno che abbiano più Dadi Vita del proprio livello da mago nel qual caso sono invece abbagliate.

Si può utilizzare questa capacità 3 + Mod Int volte al giorno.

Campo di Invisibilità (Mag)
***************************
:Tipo: :ref:`Azione Veloce<azione_veloce>`
:Liv Min: 8

Ci si può rendere invisibili per un numero di round al giorno pari al proprio livello da mago, non necessariamente consecutivi

Il funzionamento ricalca quello di :ref:`Invisibilità Superiore<inc_invisibilità_superiore>`.

.. _mago_scuola_invocazione:

Invocazione
------------------------
Gli :ref:`Invocatori<scuola_invocazione>` si basano sulla potenza pura della magia e possono usarla per creare e distruggere con facilità scioccante.

.. _mago_incantesimi_intensi:

Incantesimi Intensi (Sop)
*************************
Ogni volta che si lancia un incantesimo di invocazione che infligge danni, si deve
aggiungere metà del proprio livello da mago al danno (minimo +1), che si applica soltanto una volta ad un incantesimo, non una volta per dardo o raggio e non può
essere suddiviso fra dardi o raggi multipli. 

Questo danno è dello stesso tipo dell’incantesimo. 

Al 20° livello, ogni volta che si lancia un incantesimo di invocazione si può tirare
due volte per penetrare la resistenza agli incantesimi della creatura e tenere il risultato migliore.

Dardo di Forza (Mag)
**********************
:Tipo: :ref:`Azione Standard<azione_standard>`

Si può liberare un dardo di forza che colpisce automaticamente un nemico, come :ref:`Dardo Incantato<inc_dardo_incantato>`, e che infligge 1d4 danni più il danno dal proprio potere di invocazione di :ref:`Incantesimi Intensi<mago_incantesimi_intensi>`. 

Questo è un effetto di forza che si può utilizzare 3 + Mod Int volte al giorno.

Muro Elementale (Mag)
**********************
:Liv Min: 8

Si può creare un muro di energia che dura un numero di round al giorno pari al
proprio livello da mago, non necessariamente consecutivi. 

Il muro infligge danno da acido, freddo, fuoco o elettricità, determinato nel momento in cui viene creato e per il resto funziona come :ref:`Muro di Fuoco<inc_muro_di_fuoco>`.

.. _mago_scuola_necromanzia:

Necromanzia
-------------
Il temuto e macabro :ref:`Necromante<scuola_necromanzia>` domina i non morti ed usa il potere ripugnante della non vita contro i suoi nemici.

Comandare Non Morti (Sop)
*************************
Talento bonus :ref:`Comandare Non Morti<tal_comandare_non_morti>` o :ref:`Scacciare Non Morti<tal_scacciare_non_morti>`. 

Si può incanalare energia 3 + Mod Int volte al giorno e si possono scegliere altri talenti da aggiungere a questa capacità, come :ref:`Incanalare Extra<tal_incanalare_extra>` e :ref:`Incanalare Migliorato<tal_incanalare_migliorato>`, ma che la alterino, come :ref:`Incanalare Allineamento<tal_incanalare_allineamento>` e :ref:`Incanalare Elementale<tal_incanalare_elementale>`. 

La CD del TS contro questi talenti è 10 + 0.5 Liv Mago + Mod Car. 

Al 20° livello, i non morti non possono aggiungere la loro resistenza a incanalare ai TS contro questa capacità.

Tocco della Tomba (Mag)
************************
:Tipo: :ref:`Attacco di Contatto in Mischia<azione_attacco_di_contatto_in_mischia>`

L'attacco rende una creatura vivente scossa per un numero di round pari a metà del proprio livello da mago (minimo 1). 

Se si tocca una creatura scossa, questa diventa spaventata per 1 round se ha meno Dadi Vita del proprio livello da mago. 

Si può utilizzare 3 + Mod Int volte al giorno.

Visione della Vita (Sop)
*************************
:Liv Min: 8

Si ottiene :ref:`Vista Cieca<vista_cieca>` fino a 3 metri di raggio per un numero di round al giorno pari al proprio livello da mago, permettendo soltanto di rilevare le creature viventi e le creature non morte e di identificare se una creatura è vivente o non morta. 

Il raggio di questa capacità aumenta di 3 metri al 12° livello + 3 metri ogni quattro livelli addizionali.

.. _mago_scuola_trasmutazione:

Trasmutazione
--------------------------
I :ref:`Trasmutatori<scuola_trasmutazione>` usano la magia per plasmare il mondo intorno a loro.

Potenziamento fisico (Sop)
***************************
+1 ad un punteggio di caratteristica fisica (Forza, Destrezza o Costituzione) +1 ogni cinque livelli da mago posseduti, fino ad un massimo di +5 al 20° livello. 

Si può spostare questo bonus ad un nuovo punteggio di caratteristica quando si preparano gli incantesimi.

Al 20° livello, questo bonus si applica a due punteggi di caratteristica fisica di propria scelta.

Pugno Telecinetico (Mag)
*************************
:Tipo: :ref:`Attacco di Contatto a distanza<azione_attacco_di_contatto_a_distanza>`

Si può colpire con un pugno telecinetico, che abbia come bersaglio qualsiasi nemico entro 9 metri che infligge 1d4 danni contundenti +1 danno ogni due livelli da mago posseduti. 

Si può utilizzare 3 + Mod Int volte al giorno.

Cambiare Forma (Mag)
*********************
:Liv Min: 8

Si può mutare la propria forma corporea per un numero di round al giorno pari al proprio livello da mago, non necessariamente consecutivi. 

Funziona come :ref:`Forma Ferina II<inc_forma_ferina_II>` o :ref:`Corpo Elementale I<inc_corpo_elementale_I>` e al 12° livello come :ref:`Forma Ferina III<inc_forma_ferina_III>` o :ref:`Corpo Elementale II<inc_corpo_elementale_II>`.

.. _mago_scuola_universale:

Scuola Universale
------------------
I maghi che non si specializzano (conosciuti come generici) sono i più versatili di tutti gli incantatori arcani.

Mano dell’Apprendista (Sop)
***************************
:Tipo: :ref:`Azione Standard<azione_standard>`

Si può indurre la propria arma da mischia a volare dalla vostra presa e colpire un nemico fino a 9 metri prima del suo immediato ritorno in mano. 

Questo attacco è trattato come un attacco a distanza con un’arma da lancio, salvo che
si aggiunge il proprio Mod Int al tiro di attacco anziché il Mod Des (il danno
si basa sempre sulla Forza). 

Questa capacità non può essere utilizzata per effettuare una manovra in combattimento e si può utilizzare 3 + Mod Int volte al giorno.

Padronanza Metamagica (Sop)
***************************
:Liv Min: 8
 
Si può applicare qualsiasi talento :ref:`Metamagico<tal_metamagia>` che si conosce ad un incantesimo che si sta per lanciare senza alterarne il livello o tempo di lancio. 

Si può utilizzare questa capacità una volta al giorno all’8° livello e successivamente una volta in più al giorno ogni due livelli da mago posseduti. 

Ogni volta che si usa questa capacità per applicare un talento che incrementa il livello dell’incantesimo di più di 1, si deve impiegare un uso giornaliero addizionale per ogni livello extra. 

Anche se questa capacità non modifica il livello attuale dell’incantesimo, non si può utilizzare per lanciare un incantesimo il cui livello modificato sarebbe superiore al livello dell’incantesimo più alto che si è in grado di lanciare.

------------------------------
Incantesimi arcani e armature
------------------------------

L’armatura ostacola i gesti complicati necessari quando si lanciano gli incantesimi con componente somatica.

Le :ref:`descrizioni delle armature<equip_armature>` e degli scudi elencano la probabilità di fallimento degli incantesimi arcani in base alla loro tipologia.

Se un incantesimo non ha componente somatica, gli incantatori arcani possono lanciarlo mentre indossano l’armatura senza subire alcuna penalità di fallimento degli incantesimi arcani. Tali incantesimi possono essere lanciati anche se le mani dell’incantatore sono legate o se è coinvolto in una lotta, applicando come al solito le prove di concentrazione. 

Il talento di metamagia :ref:`Incantesimi Immobili<tal_incantesimi_immobili>` permette ad un incantatore di preparare o lanciare un incantesimo senza componente somatica ad un livello superiore al normale, che è un modo per lanciare un incantesimo indossando un’armatura senza rischiare il fallimento dell’incantesimo.

.. _monaco:

Monaco
==================================

Per il modello esemplare, le arti marziali oltrepassano il campo di battaglia: esse sono uno stile di vita, una dottrina, uno stato mentale. 
Questi combattenti artisti scoprono le vie del combattimento oltre spade e scudi, e le trovano in se stessi, diventano armi in grado di paralizzare ed uccidere come lame affilate. 

Questi *Monaci* elevano i loro corpi per trasformarli in armi da guerra, siano essi asceti studiosi di strategie militari o litigiosi autodidatti.

I monaci percorrono la via della disciplina e quelli con la volontà di resistere su quella via scoprono in se stessi non che cosa sono, ma che cosa sono destinati ad essere.

:Ruolo: 

    I monaci eccellono nel superare persino i pericoli più scoraggianti, nel colpire dove non ci si aspetta e ad approfittare delle vulnerabilità dei nemici.

    Veloci nella corsa ed esperti nel combattimento, i monaci possono attraversare facilmente qualsiasi campo di battaglia, aiutando gli alleati dovunque si abbia più bisogno di loro.

.. figure:: Immagini/Monaco.png
    :align: right
    :width: 400

:Allineamento: Qualsiasi legale
:Dado Vita: d8

----------------------------
Abilità di classe
----------------------------

:Crescita per livello: 4 + modificatore Intelligenza (Mod Int)

*	:ref:`Acrobazia (Des)<acrobazia>`
*	:ref:`Artigianato (Int)<artigianato>`
*	:ref:`Artista della Fuga (Des)<artista_della_fuga>`
*	:ref:`Cavalcare (Des)<cavalcare>`
*	:ref:`Conoscenze (religioni, storia) (Int)<conoscenze>`
*	:ref:`Furtività (Des)<furtività>`
*	:ref:`Intimidire (Car)<intimidire>`
*	:ref:`Intrattenere (Car)<intrattenere>`
*	:ref:`Intuizione (Sag)<intuizione>`
*	:ref:`Nuotare (For)<nuotare>`
*	:ref:`Percezione (Sag)<percezione>`
*	:ref:`Professione (Sag)<professione>`
*	:ref:`Scalare (For)<scalare>`

--------------------
Privilegi di Classe
--------------------

.. contents:: Riassumendo:
    :local:

Competenze nelle armi e armature
--------------------------------

Ascia, balestra (leggera o pesante), bastone ferrato, fionda, giavellotto, kama, lancia, lancia corta, nunchaku, pugnale, randello, sai, shuriken, siangham, spada corta. 

Nessuna armatura o scudo. Quando indossa un’armatura, utilizza uno scudo, o trasporta un carico medio o pesante, un monaco perde il :ref:`Bonus alla CA<bonus_ca_monaco>` insieme alle capacità :ref:`Movimento Veloce<movimento_veloce_monaco>` e :ref:`Raffica di Colpi<raffica_di_colpi_monaco>`.

.. _bonus_ca_monaco:

Bonus alla CA (Str)
-------------------

Quando ha ingombro nullo o leggero e non indossa armatura, un monaco somma il suo Mod Sag a CA e a DMC. 

In aggiunta un monaco guadagna bonus +1 alla CA e alla DMC al 4° livello, che aumenta di +1 ogni quattro livelli da monaco successivi, fino ad un massimo di +5 al 20° livello.

Questi bonus si applicano anche contro gli attacchi di contatto o quando il monaco è impreparato, ma sono persi quando è immobilizzato o indifeso, quando indossa una qualsiasi armatura, porta uno scudo o trasporta un carico medio o pesante.

.. _avanzamento_monaco:

.. figure:: Immagini/AvanzamentoMonaco.png
    :align: center

.. _raffica_di_colpi_monaco:

Raffica di Colpi (Str)
-----------------------

Si può compiere una *Raffica di Colpi* come azione di attacco completo, compiendo un attacco addizionale usando qualsiasi combinazione di colpi senz’armi o di attacchi con le armi speciali da monaco (bastone ferrato, kama, nunchaku, sai, shuriken e siangham) come se disponesse del talento :ref:`Combattere con Due Armi<tal_combattere_con_due_armi>`. 

Per questi attacchi, il bonus di attacco base del monaco è uguale al suo livello da monaco. 

All’8° livello si può compiere due attacchi addizionali, come se disponesse del talento :ref:`Combattere con Due Armi Migliorato<tal_combattere_con_due_armi_migliorato>`.

Al 15° livello il monaco può compiere tre attacchi addizionali, come se disponesse del talento :ref:`Combattere con Due Armi Superiore<tal_combattere_con_due_armi_superiore>`.

Un monaco applica il Mod For ai tiri per il danno per tutti gli attacchi riusciti con *Raffica di Colpi*, sia che gli attacchi siano effettuati con un’arma impugnata nella
mano secondaria o in entrambe. 

È possibile sostituire le manovre in combattimento disarmare, spezzare e sbilanciare
per attacchi senz’armi come parte di una raffica di colpi. 

Un monaco con armi naturali non può usare queste armi come parte di una raffica di colpi, e neppure effettuare attacchi naturali in aggiunta agli attacchi di raffica di colpi.

Colpo Senz'Armi
-----------------

Talento bonus :ref:`Colpo Senz’Armi Migliorato<tal_colpo_senzarmi_migliorato>`.

Gli attacchi di un monaco possono essere portati con entrambi i pugni o anche con gomiti, ginocchia e piedi. Ciò significa che un monaco può effettuare colpi senz’armi anche quando non ha le mani libere. 

Non esiste una cosa equivalente all’attacco con mano secondaria per un monaco che
colpisce senz’armi e si può quindi applicare il suo Mod For ai tiri per il danno a tutti i suoi colpi senz’armi.

In genere, i colpi senz’armi del monaco infliggono **danni letali**, ma egli può decidere di infliggere danni non letali senza alcuna penalità al tiro per colpire. Egli ha la stessa possibilità di scegliere se infliggere danni letali o non letali anche quando è in lotta.

Il colpo senz’armi di un monaco viene considerato sia un’arma manufatta che un’arma naturale ai fini degli incantesimi o effetti che potenziano o migliorano le armi manufatte o le armi naturali.

.. figure:: Immagini/MonacoDanni.png
    :align: left
    :width: 300

Un monaco inoltre infligge più danni con i suoi colpi senz’armi, di una persona normale, come mostrato nella sua :ref:`tabella<avanzamento_monaco>` nel caso sia di taglia Media. 
Per i monaci di taglia Piccola e Grande bisogna invece consultare la tabella qua a lato.

Talento Bonus
--------------
Al 1°, 2° e ogni quattro livelli successivi, un monaco può selezionare un talento
bonus, scelto nell'elenco seguente:

*	:ref:`Cogliere di Sorpresa<tal_cogliere_di_sorpresa>`
*	:ref:`Deviare Frecce<tal_deviare_frecce>`
*	:ref:`Lanciare Oggetti<tal_lanciare_oggetti>`
*	:ref:`Lottare Migliorato<tal_lottare_migliorato>`
*	:ref:`Riflessi in Combattimento<tal_riflessi_in_combattimento>`
*	:ref:`Schivare<tal_schivare>`
*	:ref:`Stile dello Scorpione<tal_stile_dello_scorpione>`

Al 6° talento i seguenti talenti sono aggiunti all’elenco: 

*	:ref:`Disarmare Migliorato<tal_disarmare_migliorato>`
*	:ref:`Fintare Migliorato<tal_fintare_migliorato>`
*	:ref:`Mobilità<tal_mobilità>`
*	:ref:`Pugno della Gorgone<tal_pugno_della_gorgone>`
*	:ref:`Sbilanciare Migliorato<tal_sbilanciare_migliorato>`
*	:ref:`Spingere Migliorato<tal_spingere_migliorato>`

Al 10° talento i seguenti talenti sono aggiunti all’elenco: 

*	:ref:`Afferrare Frecce<tal_afferrare_frecce>`
*	:ref:`Attacco Rapido<tal_attacco_rapido>`
*	:ref:`Critico Migliorato<tal_critico_migliorato>`
*	:ref:`Collera della Medusa<tal_collera_della_medusa>`

Un monaco non ha bisogno di soddisfare **nessuno dei prerequisiti** normalmente associati a questi talenti per selezionarli.

Pugno Stordente (Str)
----------------------

Talento bonus :ref:`Pugno Stordente<tal_pugno_stordente>`.

Al 4° livello e ad ogni 4 livelli successivi, il monaco guadagna la capacità di applicare una nuova condizione sul bersaglio del suo :ref:`Pugno Stordente<tal_pugno_stordente>`.

Questa condizione sostituisce lo stordimento del bersaglio per 1 round, ed un tiro salvezza riuscito ne nega l’effetto. 

**Al 4° livello** può scegliere di rendere il bersaglio affaticato. 
**All’8° livello** può rendere il bersaglio infermo per 1 minuto. 
**Al 12° livello** può rendere il bersaglio barcollante per 1d6+1 round.
**Al 16° livello** può permanentemente accecare o assordare il bersaglio. 
**Al 20° livello** può paralizzare il bersaglio per 1d6+1 round. 

Il monaco deve scegliere quale condizione applicare prima di effettuare il tiro d’attacco e gli effetti non si cumulano l’uno con l’altro (una creatura resa inferma
da :ref:`Pugno Stordente<tal_pugno_stordente>` non può diventare nauseata se colpita nuovamente da :ref:`Pugno Stordente<tal_pugno_stordente>`), ma i colpi aggiuntivi ne aumentano la durata.

Eludere (Str)
--------------
:Liv Min: 2

Se si supera un tiro salvezza su Riflessi contro un attacco che infliggerebbe normalmente la metà dei danni in caso di tiro salvezza riuscito, non si subisce alcun danno. 

La capacità di eludere può essere usata solo se il monaco non indossa alcuna armatura o ne indossa una leggera. 

Un monaco indifeso non riceve i benefici di eludere.

.. _movimento_veloce_monaco:

Movimento Veloce (Str)
------------------------
:Liv Min: 3

Potenziamento alla velocità sul terreno (vedi :ref:`tabella<avanzamento_monaco>`). 

Un monaco con l’armatura o che trasporta un carico medio o pesante perde questa velocità extra.

.. figure:: Immagini/Monaco2.png
    :align: left
    :width: 400

Addestramento alle Manovre (Str)
---------------------------------
:Liv Min: 3

Si usa il Liv monaco al posto del suo bonus di attacco base quando calcola il Bonus da Manovra in Combattimento. I bonus di attacco base concessi da altre classi
restano invariati e si aggiungono normalmente.

Mente Lucida (Str)
--------------------
:Liv Min: 3

\+2 ai tiri salvezza contro incantesimi ed effetti di ammaliamento.

.. _riserva_ki:

Riserva Ki (Sop)
----------------
:Liv Min: 4

Riserva di punti ki, energia soprannaturale che si può impiegare per portare a termine sfide eccezionali.

Il numero di punti nella riserva ki del monaco è 0.5*Liv Monaco + Mod Sag. Finché ha
almeno 1 punto della sua riserva ki, il monaco può utilizzare un Colpo Ki. 

* Il Colpo Ki permette di considerare gli attacchi senz’armi del monaco come armi magiche al fine di superare la riduzione del danno.
* Al **7° livello** gli attacchi senz’armi sono considerati come armi in ferro freddo e argento al fine di superare la riduzione del danno. 
* Al **10° livello**, gli attacchi senz’armi sono considerati anche armi legali al fine di superare la riduzione del danno. 
* Al **16° livello** gli attacchi senz’armi sono considerati come armi adamantine al fine di superare la riduzione del danno e superare la durezza.

Spendendo 1 punto dalla sua riserva ki, un monaco può:

* Effettuare un attacco addizionale con il suo bonus di attacco migliore quando compie raffica di colpi.
* Aumentare la sua velocità di 6 metri per 1 round.
* Darsi bonus di schivare +4 alla CA per 1 round. 

Ognuno di questi vale come azione veloce. 

Un monaco guadagna poteri addizionali che consumano punti dalla sua riserva ki quando avanza di livello.
La riserva ki viene reintegrata al mattino dopo 8 ore di riposo o meditazione, non necessariamente consecutive.

Caduta lenta (Str)
---------------------
:Liv Min: 4

Si può sfruttare un muro che sia ad un braccio di distanza per rallentare la caduta. 

Il monaco subisce danni come se la caduta fosse 6 metri più breve di quanto non sia realmente. La sua capacità di rallentare la caduta migliora con l’aumento di livello fino a quando, al 20° livello, il monaco può usare un muro vicino per rallentare la discesa e cadere da qualsiasi altezza senza farsi male.

Salto sublime (Str)
--------------------
:Liv Min: 5

Il livello da monaco si somma a tutte le prove di :ref:`Acrobazia<acrobazia>` effettuate per saltare, sia in alto che in lungo e  si considera che abbia sempre rincorsa quando effettua queste prove.

Spendendo 1 punto Ki come azione veloce, un monaco ottiene bonus +20 alle prove di :ref:`Acrobazia<acrobazia>` effettuate per saltare per 1 round.

Purezza del corpo (Str)
------------------------
:Liv Min: 5

Immunità a tutte le malattie, comprese quelle soprannaturali e magiche.

Integrità del corpo (Sop)
--------------------------
:Tipo: Azione Standard
:Liv Min: 7
:Costo: 2 punti Ki

Ci si può curare le ferite per un ammontare di danni pari al livello da monaco.

Eludere migliorato (Str)
--------------------------
:Liv Min: 9

Si subisce solo la metà dei danni anche quando si fallisce il tiro salvezza sui Riflessi. 

Un monaco indifeso non riceve i benefici di eludere migliorato.

Corpo adamantino (Str)
-------------------------
:Liv Min: 11

Immunità a tutti i tipi di veleno.

Passo abbondante (Sop)
------------------------
:Tipo: Azione di Movimento
:Liv Min: 12
:Costo: 2 punti Ki

Il monaco può spostarsi magicamente tra gli spazi, come se usasse l’incantesimo :ref:`Porta Dimensionale<inc_porta_dimensionale>`. 

Il suo livello da incantatore per questo effetto è pari al suo livello da monaco e non può portare con sè altre creature quando utilizza questa capacità.

Anima adamantina (Str)
----------------------
:Liv Min: 13

Resistenza agli incantesimi pari al livello da monaco +10. 

Perché un incantesimo abbia effetto sul monaco, un incantatore deve ottenere o superare la resistenza agli incantesimi del monaco con una prova di livello
dell’incantatore (1d20 + livello dell’incantatore).

Palmo tremante (Sop)
--------------------
:Liv Min: 15

Una volta al giorno il monaco può sviluppare delle vibrazioni nel corpo di un’altra creatura che, se così desidera, possono essere fatali, dichiarando le proprie intenzioni prima di effettuare il tiro per colpire. 

Creature immuni ai colpi critici non ne subiscono gli effetti. 

Se il monaco colpisce con successo e il bersaglio subisce danni dal colpo, l’attacco del palmo tremante ha avuto effetto. Quindi, il monaco può scegliere di uccidere
la vittima in qualsiasi momento successivo, purché ciò accada entro un numero di giorni pari al livello del monaco. 

Per farlo egli deve semplicemente desiderare che la vittima muoia (un’azione gratuita) e, a meno che la vittima non superi un tiro salvezza su Tempra, con **CD 10 + 0.5*Liv monaco + Mod Sag**, muore. Se il tiro salvezza riesce, il bersaglio non sarà più soggetto agli effetti di quel particolare attacco con palmo tremante, ma potrà essere colpito da un nuovo attacco in futuro. 

Un monaco non può avere più di 1 palmo tremante in effetto in un dato momento. Se un monaco usa palmo tremante mentre uno precedente è ancora attivo, l’effetto di quest’ultimo viene annullato.

Corpo senza tempo (Str)
-----------------------
:Liv Min: 17

Non si subisce più le penalità di caratteristica per l’invecchiamento e non può essere invecchiato magicamente. Qualsiasi penalità che abbia già subito rimane. 

I bonus di invecchiamento continuano ad accumularsi e il monaco comunque muore di vecchiaia quando arriva il momento.

Lingua del sole e della luna (Str)
------------------------------------
:Liv Min: 17

Capacità di parlare con qualsiasi creatura vivente.

Corpo vuoto (Sop)
------------------
:Tipo: Azione di Movimento
:Liv Min: 19
:Costo: 3 punti Ki

Il monaco può assumere uno stato etereo per 1 minuto, come per l’incantesimo :ref:`Forma Eterea<inc_forma_eterea>`. 

Questa capacità ha effetto solo sul monaco e non può essere usata per rendere eteree altre creature.

Perfezione interiore
---------------------
:Liv Min: 20 

Il monaco diventa una creatura magica e sarà sempre trattato come un esterno anziché un umanoide (o qualunque tipo di creatura sia) al fine degli incantesimi ed effetti magici. 

Inoltre, si ottiene riduzione del danno 10/caotico, che permette di ignorare i primi 10 danni da qualsiasi attacco effettuato da un’arma non caotica o di un attacco naturale compiuto da una creatura che non ha una riduzione del danno simile.

A differenza degli esterni, il monaco può sempre essere riportato in vita dalla morte come se fosse ancora un membro del tipo di creatura che era in precedenza.

-----------------------------
Ex-Monaci
-----------------------------

Un monaco che diventa non legale non può guadagnare nuovi livelli come monaco ma conserva comunque tutte le capacità da monaco.

.. _paladino:

Paladino
==================================

Grazie ad una attenta selezione, sono pochi quelli degni di risplendere del potere divino. Chiamati paladini, queste anime nobili dedicano la loro vita a combattere il male. 

Cavalieri, crociati e legislatori, i paladini non cercano di diffondere una giustizia divina ma di abbracciare gli insegnamenti virtuosi della divinità che servono. 
Come ricompensa per la loro onestà, questi sacri campioni sono benedetti con poteri in grado di aiutarli nelle loro cerche: il potere di punire il male, guarire gli innocenti ed ispirare i fedeli. 

:Ruolo: 

    I paladini sono il punto di riferimento dei loro alleati all’interno del caos della battaglia. Sono letali persecutori del male, possono anche potenziare le anime buone per aiutarle nelle loro crociate.

    Le loro capacità magiche e marziali li rendono anche ben adatti a difendere gli altri e a benedire chi vacilla ridandogli la forza di continuare a combattere.

.. figure:: Immagini/Paladino.png
    :align: right
    :width: 400

:Allineamento: Legale Buono
:Dado Vita: d10

----------------------------
Abilità di classe
----------------------------

:Crescita per livello: 2 + modificatore Intelligenza (Mod Int)

*	:ref:`Addestrare Animali (Car)<addestrare_animali>`
*	:ref:`Artigianato (Int)<artigianato>`
*	:ref:`Cavalcare (Des)<cavalcare>`
*	:ref:`Conoscenze (nobiltà, religioni) (Int)<conoscenze>`
*	:ref:`Diplomazia (Car)<diplomazia>`
*	:ref:`Guarire (Sag)<guarire>`
*	:ref:`Intuizione (Sag)<intuizione>`
*	:ref:`Professione (Sag)<professione>`
*	:ref:`Sapienza Magica (Int)<sapienza_magica>`

--------------------
Privilegi di Classe
--------------------

.. contents:: Riassumendo:
    :local:

Competenze nelle armi e armature
--------------------------------

Tutte le armi semplici e da guerra e di tutti i tipi di armatura (pesanti medie e leggere) e scudi (tranne gli scudi torre).

Aura di Bene (Str):
--------------------

Il potere dell’aura di bene di un paladino (vedi l’incantesimo :ref:`individuazione del bene<inc_individuazione_del_bene>`) è pari al suo livello da paladino.

Individuazione del male (Mag)
------------------------------

Il paladino può usare :ref:`individuazione del male<inc_individuazione_del_male>`. 

È possibile, inoltre, concentrarsi su un singolo oggetto o individuo entro 18 metri e determinare se è malvagio, apprendendo la forza della sua aura come se lo avesse studiato attentamente per 3 round, come azione di movimento. 

Mentre focalizza l’attenzione su un oggetto o un individuo, il paladino non può individuare il male in nessun altro oggetto o individuo che si trovi nel raggio d’azione dell’incantesimo.

.. _punire_il_male:

Punire il male (Sop)
-------------------------
:Tipo: Azione Veloce


Una volta al giorno, un paladino può invocare i poteri del bene per aiutarlo nella lotta contro il male.

Si sceglie un bersaglio malvagio visibile da punire ottenendo +**Mod Car** ai **tiri per colpire**, +**Liv paladino** ai tiri per il **danno** e +**Mod Car** alla **CA** contro il bersaglio, superando anche automaticamente qualsiasi RD che la creatura possiede. 

Se il bersaglio è un esterno con il sottotipo malvagio, un drago di allineamento
malvagio o un non morto, il bonus al danno aumenta di 2 danni per ogni livello da paladino. 

Se la creatura non è malvagia, punire il male è sprecato.

L’effetto di punire il male rimane attivo finché il bersaglio non muore o il paladino non riposa e riguadagna l’uso della capacità. 

Al 4° livello, e ogni tre livelli successivi, il paladino può punire il male una volta in più al giorno, fino ad un massimo di sette volte al 19° livello, come riassunto in :ref:`tabella<avanzamento_paladino>`.

.. _avanzamento_paladino:

.. figure:: Immagini/AvanzamentoPaladino.png

Grazia divina (Sop)
---------------------
:Liv Min: 2

Bonus a tutti i tiri salvezza pari al Mod Car.

.. _imposizione_delle_mani:

Imposizione delle mani (Sop)
-----------------------------
:Liv Min: 2

Permette di curare le ferite (le proprie e quelle degli altri) con il tocco. 

Si può usare questa capacità per un numero di volte al giorno pari a 0.5*Liv Paladino + Mod Car, curando 1d6 pf ogni due livelli da paladino posseduti. 

Usare questa capacità è un’azione standard, a meno che il paladino non scelga se stesso come bersaglio, nel qual caso è un’azione veloce. 

.. Caution:: 
    Nonostante il nome, un paladino necessita solo di una mano libera per usare questa capacità.

Come tutte le capacità di curare, questa può essere usata per infliggere danni a
creature non morte, causando 1d6 danni ogni due livelli da paladino posseduti.
Usare imposizione delle mani in questo modo necessita di un attacco di contatto in
mischia riuscito che non provoca attacchi di opportunità. I non morti non ricevono tiro salvezza contro questo danno.

Aura di coraggio (Sop)
-----------------------
:Liv Min: 3

Immunità alla paura (magica o meno).

Gli alleati presenti entro un raggio di 3 metri dal paladino guadagnano bonus morale +4 ai tiri salvezza contro effetti di paura, a meno che il paladino sia privo di sensi o morto.

Salute divina (Str)
---------------------
:Liv Min: 3

Immunità a tutte le malattie, comprese quelle soprannaturali e magiche.

.. _indulgenza:

Indulgenza (Sop)
-------------------
:Liv Min: 3

Dal 3° livello e ogni tre successivi, un paladino può selezionare una indulgenza, che 
aggiunge un effetto alla capacità del paladino :ref:`Imposizione delle Mani<imposizione_delle_mani>`. 

Ogni volta che il paladino usa :ref:`Imposizione delle Mani<imposizione_delle_mani>` per curare un bersaglio, quest’ultimo riceve gli effetti addizionali di ogni indulgenza posseduta. 

Una indulgenza può eliminare una condizione causata da una maledizione, una malattia o un veleno senza guarire l’afflizione, condizioni che tornano dopo 1 ora a meno
che l’indulgenza non elimini anche l’afflizione che le causa.

* Dal 3° livello può scegliere:
    • Affaticato:
        Il bersaglio non è più affaticato.
    • Scosso:
        Il bersaglio non è più scosso.
    • Infermo:
        Il bersaglio non è più infermo.
* Dal 6° livello:
    • Frastornato:
        Il bersaglio non è più frastornato.
    • Ammalato:
        Imposizione delle mani funge da :ref:`Rimuovi Malattia<inc_rimuovi_malattia>`, usando il livello del paladino come livello dell’incantatore.
    • Barcollante:
        Il bersaglio più non è barcollante, a meno che l’obiettivo non sia esattamente a 0 pf.
* Dal 9° livello:
    • Maledetto:
        Imposizione delle mani funge da :ref:`Rimuovi Maledizione<inc_rimuovi_maledizione>`, usando il livello del paladino come livello dell’incantatore.
    • Esausto:
        Il bersaglio più non è esausto. **Affaticato** Prerequisito.
    • Spaventato:
        Il bersaglio non è più spaventato. **Scosso** Prerequisito.
    • Nauseato:
        Il bersaglio non è più nauseato. **Infermo** Prerequisito.
    • Avvelenato:
        Imposizione delle mani funge da :ref:`Neutralizza Veleno<inc_neutralizza_veleno>`, usando il livello del paladino come livello dell’incantatore.
* Dal 12° livello:
    • Accecato:
        Il bersaglio più non è accecato.
    • Assordato:
        Il bersaglio non è più assordato.
    • Paralizzato:
        Il bersaglio non è più paralizzato.
    • Stordito:
        Il bersaglio non è più stordito.

Queste capacità sono cumulative. Quindi un paladino di 12° livello può con imposizione della mani curare 6d6 punti ferita e le condizioni affaticato ed esausto così come rimuovere malattie e neutralizzare veleni. 

.. Warning::
    Una volta che si sceglie una condizione o effetto ma non si può cambiare.

.. _incanalare_energia_positiva:

Incanalare energia positiva (Sop)
-----------------------------------
:Liv Min: 4

Capacità soprannaturale di :ref:`Incanalare Energia Positiva<incanalare_energia>` come un chierico, al costo di due usi della sua capacità :ref:`Imposizione delle Mani<imposizione_delle_mani>`. Un paladino utilizza il suo livello come livello da chierico effettivo quando incanala l’energia positiva. 

Questa è una capacità basata sul Carisma

Incantesimi
------------
:Lista: :ref:`Incantesimi da Paladino<incantesimi_paladino>`
:Preparazione: Sì
:Liv Min: 4

Si possono lanciare un piccolo numero di incantesimi divini.

Per preparare o lanciare un incantesimo, un paladino deve avere Car ≥ 10 + Liv Incantesimo e la Classe Difficoltà per un tiro salvezza contro un suo incantesimo è 10 + Liv Incantesimo + Mod Car.

Oltre alla sua :ref:`disponibilità giornaliera di incantesimi<avanzamento_paladino>` il paladino ottiene :ref:`incantesimi bonus<incantesimi_bonus>` al giorno a seconda del suo punteggio Car. Se la :ref:`tabella<avanzamento_paladino>` indica 0 incantesimi al giorno per un determinato livello di incantesimi, si ottengono solamente gli incantesimi bonus dati dal Carisma. 

Un paladino deve spendere 1 ora al giorno in preghiera e meditazione per riguadagnare la sua disponibilità giornaliera di incantesimi, con la possibilità di preparare e lanciare qualsiasi incantesimo sulla lista degli incantesimi da paladino.

Il livello da incantatore del paladino è pari al suo livello da paladino –3 (minimo 0). 

.. figure:: Immagini/Paladino2.png
    :align: left
    :width: 400

Legame divino (Mag)
---------------------
:Liv Min: 5

Il paladino stipula un legame divino con la sua divinità che può essere di una di due forme che non può essere cambiata una volta scelta.

Il **primo tipo** permette di potenziare la propria arma come azione standard invocando l’aiuto di uno spirito celestiale per 1 minuto per livello da paladino.

    Lo spirito rende l’arma lucente come una torcia e concede all’arma bonus di potenziamento +1. Ogni tre livelli, l’arma guadagna un altro bonus di potenziamento +1, fino ad un massimo di +6 al 20° livello.

    Questi bonus possono aggiungersi all’arma, e cumularsi con quelli esistenti dell’arma fino ad un **massimo di +5**, o possono essere usati per aggiungere all’arma una delle seguenti capacità: assiomatica, energia luminosa, difensiva, distruttiva, esplosione di fiamme, infuocata, sacra, affilata, pietosa e velocità.

    Queste capacità consumano una quantità di bonus uguale al :ref:`costo della capacità<costo_capacità_armi>` e si aggiungono a qualsiasi altra capacità l’arma possegga già, ma senza cumulare eventuali doppioni.

    Se l’arma non è magica, occorre aggiungere almeno bonus di potenziamento +1 prima di poter aggiungere una qualità speciale. Il bonus e le capacità speciali concessi dallo spirito sono determinati quando lo spirito è invocato e non possono essere cambiati finché lo spirito non viene invocato nuovamente.

    Lo spirito celestiale non concede alcun bonus se l’arma è impugnata da altri che non siano il paladino ma i bonus ritornano se l’arma torna nelle mani del paladino.

    Questi bonus si applicano soltanto ad un’estremità di un’arma doppia.

    Un paladino può utilizzare questa capacità una volta al giorno al 5° livello e successivamente una volta in più al giorno ogni quattro livelli, fino ad un massimo di quattro volte al giorno al 17° livello.

    Se l’arma viene distrutta, il paladino perde l’uso di questa capacità per 30 giorni, o fino a che non guadagna un livello e durante questo periodo subisce penalità –1 ai tiri per colpire e per il danno con l’arma.

Il **secondo tipo** di legame permette di ottenere i servigi di un destriero insolitamente intelligente, forte e leale, per servirlo nella sua crociata contro il male. 

    Questa cavalcatura in genere è un cavallo da guerra pesante (per i paladini di taglia Media) o un pony (per i paladini di taglia Piccola), anche se cavalcature più esotiche, come cammelli, cinghiali o cani sono comunque disponibili.

    Questa cavalcatura funziona come il :ref:`Compagno Animale<compagno_animale_druido>` di un druido, usando il livello del paladino come livello effettivo del druido.

    Le cavalcature legate hanno un punteggio di Intelligenza almeno di 6.

    Una volta al giorno, con un’azione di round completo, si può richiamare magicamente la cavalcatura con un incantesimo di livello pari ad un terzo del livello del paladino, facendola apparire immediatamente adiacente al paladino.

    Il paladino può usare questa capacità una volta al giorno al 5° livello e successivamente una volta in più ogni quattro livelli, per un totale di quattro volte al 17° livello.

    All’11° livello, la cavalcatura guadagna l’archetipo :ref:`Celestiale<archetipo_celestiale>` e diventa una bestia magica al fine di determinare quali incantesimi abbiano effetto su di essa.

    Al 15° livello, guadagna resistenza agli incantesimi pari a Liv paladino +11.

    Se la cavalcatura muore, il paladino non può richiamare un’altra cavalcatura per 30 giorni o finché non guadagna un livello da paladino e durante questo periodo subisce penalità –1 ai tiri per colpire e per i danni.

Aura di fermezza (Sop)
-----------------------
:Liv Min: 8

Immunità agli incantesimi e alle capacità magiche di charme.

Ogni alleato entro 3 metri ottiene bonus morale +4 ai tiri salvezza contro effetti di charme, a meno che il paladino sia privo di sensi o morto.

Aura di giustizia (Sop)
-------------------------
:Tipo: Azione Gratuita
:Liv Min: 11
:Costo: 2 usi di :ref:`Punire il Male<punire_il_male>`

Si può concedere :ref:`Punire il Male<punire_il_male>` a tutti gli alleati entro 3 metri, usando i propri bonus. 

Gli alleati devono utilizzare la capacità punire il male entro l’inizio del turno successivo del paladino ed i bonus durano 1 minuto.

Le creature malvagie non ottengono beneficio da questa capacità.

Aura di fede (Sop)
---------------------
:Liv Min: 14

Le armi del paladino sono considerate allineate al bene allo scopo di superare la riduzione del danno. 

Tutti gli attacchi effettuati contro gli avversari entro 3 metri sono considerati allineati al bene allo scopo di superare la riduzione del danno. 

.. Warning::
    Questa capacità funziona soltanto mentre il paladino è cosciente, non se è privo di sensi o morto.

Aura di rettitudine (Sop)
-------------------------
:Liv Min: 17

\+ RD 5/male ed immunità a incantesimi e capacità magiche di compulsione. 

Ogni alleato entro 3 metri ottiene bonus morale +4 ai tiri salvezza contro effetti di compulsione a meno che non si sia privi di sensi o morti.

Campione sacro (Sop)
---------------------
:Liv Min: 20

Il paladino diventa un veicolo del potere del suo dio. 

La sua RD aumenta a 10/male e ogni volta che usa :ref:`Punire il Male<punire_il_male>` e colpisce con successo un esterno malvagio, quest’ultimo è soggetto all’incantesimo :ref:`Esilio<inc_esilio>`, usando il livello del paladino come livello incantatore, considerando la sua arma e il suo simbolo sacro come oggetti che il soggetto odia. 

Dopo l’effetto di :ref:`Esilio<inc_esilio>` e il danno causato dall’attacco :ref:`Punire il Male<punire_il_male>` termina. 

Ogni volta che il paladino :ref:`Incanala Energia Positiva<incanalare_energia_positiva>` o usa :ref:`Imposizione delle Mani<imposizione_delle_mani>` per curare una creatura, quest’ultima guarisce l’ammontare massimo possibile.

Codice di condotta
---------------------
Un paladino deve essere di allineamento legale buono e perde tutte i suoi privilegi di classe se compie volontariamente un’azione malvagia. 

Il codice di un paladino richiede che rispetti l’autorità legittima, agisca con
onore (non menta, non imbrogli, non usi veleni ecc.), aiuti quanti sono in difficoltà (purché questi non sfruttino l’aiuto per fini malvagi o caotici) e punisca quanti opprimono e minacciano gli innocenti.

Associati
------------
Sebbene possa andare all’avventura con personaggi di qualsiasi allineamento buono o neutrale, un paladino non si unirà mai consapevolmente con personaggi malvagi, né continuerà un’associazione con qualcuno che offende costantemente il suo codice morale. 

In circostanze eccezionali, un paladino può allearsi con personaggi malvagi, ma solo per sconfiggere quello che reputa un male maggiore. Un paladino dovrebbe sottoporsi periodicamente ad un incantesimo :ref:`Espiazione<inc_espiazione>` durante una tale alleanza, e dovrebbe porne fine immediatamente se sente che questa sta causando più male che bene. 

Un paladino può accettare solo mercenari, gregari o seguaci che siano legali buoni.

------------
Ex-paladini
------------
Un paladino che smette di essere legale buono, che volontariamente compie un’azione malvagia o che viola decisamente il codice di condotta, perde tutte le capacità e gli incantesimi da paladino, compreso il servizio della cavalcatura del paladino, ma non le competenze in armi, armature e scudi. 

Inoltre, non può aumentare di livello come paladino. 

Riguadagna i suoi poteri se :ref:`Espia<inc_espiazione>` le sue colpe nel modo opportuno.

.. _ranger:

Ranger
==================================

Per coloro che amano l’emozione della caccia, ci sono soltanto predatori e prede. Siano essi esploratori, inseguitori o cacciatori di taglie, i ranger hanno molto in comune tra loro: padronanza unica delle armi specializzate, abilità ad inseguire persino la preda più elusiva e la perizia per sconfiggerne una vasta gamma. 

Cacciatori esperti, pazienti ed abili, questi ranger perseguitano gli uomini, le belve e i mostri allo stesso modo, affinando il proprio intuito da predatore, le proprie abilità di sopravvivenza nei vari ambienti ed il proprio letale valore marziale. 

Mentre alcuni inseguono creature cannibali per proteggere la frontiera, altri perseguono prede più astute: persino i fuggiaschi tra la loro gente.

:Ruolo: 

    I ranger sono agili schermagliatori, sia in mischia che a distanza, capaci di entrare ed uscire da un combattimento con grande scioltezza.

    Le loro capacità gli permettono di danneggiare in modo significativo tipi specifici di nemici, ma le loro qualità sono utili contro tutti i tipi di avversari.

.. figure:: Immagini/Ranger.png
    :align: right
    :width: 400

:Allineamento: Qualsiasi
:Dado Vita: d10

----------------------------
Abilità di classe
----------------------------

:Crescita per livello: 6 + modificatore Intelligenza (Mod Int)

*	:ref:`Addestrare Animali (Car)<addestrare_animali>`
*	:ref:`Artigianato (Int)<artigianato>`
*	:ref:`Cavalcare (Des)<cavalcare>`
*	:ref:`Conoscenze (dungeon, geografia, natura) (Int)<conoscenze>`
*	:ref:`Furtività (Des)<furtività>`
*	:ref:`Guarire (Sag)<guarire>`
*	:ref:`Intimidire (Car)<intimidire>`
*	:ref:`Nuotare (For)<nuotare>`
*	:ref:`Percezione (Sag)<percezione>`
*	:ref:`Professione (Sag)<professione>`
*	:ref:`Sapienza Magica (Int)<sapienza_magica>`
*	:ref:`Scalare (For)<scalare>`
*	:ref:`Sopravvivenza (Sag)<sopravvivenza>`

--------------------
Privilegi di Classe
--------------------

.. _tabella_avanzamento_ranger:

.. figure:: Immagini/AvanzamentoRanger.png
    :align: center
    :width: 800

    Tabella Avanzamento :ref:`Ranger<ranger>`.

.. contents:: Riassumendo:
    :local:

Competenze nelle armi e armature
--------------------------------
Tutte le armi semplici e da guerra, le armature leggere medie e gli scudi (tranne gli scudi torre).

.. _nemico_prescelto:

Nemico Prescelto (Str)
----------------------

.. figure:: Immagini/NemicoPrescelto.png
    :align: right
    :width: 300

    Tabella dei nemici prescelti del :ref:`Ranger<ranger>`.

Al 1° livello, un ranger sceglie un tipo di creatura dalla :ref:`Tabella dei Nemici Prescelti<nemico_prescelto>` del Ranger.

Egli ottiene bonus +2 alle prove di :ref:`Conoscenze<conoscenze>`, :ref:`Intuizione<intuizione>`, :ref:`Percezione<percezione>`, :ref:`Raggirare<raggirare>` e :ref:`Sopravvivenza<sopravvivenza>` effettuate contro le creature del tipo scelto, così come +2 ai tiri per colpire e per il danno contro di loro. 

.. Note::
    Un Ranger può effettuare prove di :ref:`Conoscenze<conoscenze>` anche senza addestramento quando cerca di identificare queste creature.

Al 5° livello e ogni 5 livelli successivi (10°, 15° e 20°) il Ranger può selezionare un nuovo nemico prescelto e il bonus associato ad uno di questi nemici prescelti (compreso quello appena
selezionato, se lo si desidera) aumenta di +2.

Se il ranger sceglie umanoidi o esterni come nemici prescelti, deve scegliere anche uno specifico sottotipo, come indicato nella :ref:`Tabella<nemico_prescelto>`. 

.. Note::
    Ci sono altri tipi di umanoidi tra cui scegliere in nel :ref:`Bestiario<bestiario>`, e quelli elencati in :ref:`Tabella<nemico_prescelto>` sono solo i più comuni.

Se una creatura specifica ricade in più di una categoria di nemico prescelto, i bonus del ranger non sono cumulativi e utilizzerà solo il bonus più elevato.

.. _seguire_tracce:

Seguire Tracce (Str)
---------------------
Si aggiunge metà del suo livello (minimo 1) alle prove di :ref:`Sopravvivenza<sopravvivenza>` effettuate per seguire o identificare le tracce.

.. _empatia_selvatica_ranger:

Empatia Selvatica (Str)
-------------------------
Un ranger può migliorare l’atteggiamento iniziale di un animale entro 9 metri (in condizioni normali di visibilità). 

Questa capacità funziona esattamente come una prova di :ref:`Diplomazia<diplomazia>` effettuata per migliorare l’atteggiamento di una persona, tirando però 1d20 e aggiungendo il proprio Liv Ranger e Mod Car per determinare il risultato della prova. 

.. Note::
    Il tipico animale domestico ha un atteggiamento iniziale indifferente, mentre un animale selvatico è solitamente maldisposto.

Generalmente, occorre 1 minuto per influenzare un animale in questo modo ma, come per le persone
influenzate, potrebbe servire un periodo più breve o più lungo.

Un ranger può anche utilizzare questa capacità per influenzare una bestia magica con un valore di Intelligenza di 1 0 2, ma subisce penalità –4 alla prova.

Stile di Combattimento (Str)
----------------------------
:Liv Min: 2

Un ranger deve selezionare uno tra i due stili di combattimento da praticare: **Tirare con l’Arco** o **Combattere con Due Armi**. Una volta fatta, la scelta non si può cambiare.

L’esperienza del ranger si manifesta in forma di *talenti bonus* al 2°, 6°, 10°, 14° e 18° livello che può scegliere anche se non ne soddisfa i prerequisiti normali nello stile prescelto.

Se il ranger sceglie *Tiro con l’Arco*, può scegliere:

|	:ref:`Tiro Lontano<tal_tiro_lontano>`
|	:ref:`Tiro Preciso<tal_tiro_preciso>`
|	:ref:`Tiro Rapido<tal_tiro_rapido>`
|	:ref:`Tiro Ravvicinato<tal_tiro_ravvicinato>`
|
|	Dal 6° livello:
|	:ref:`Tiro Preciso Migliorato<tal_tiro_preciso_migliorato>`
|	:ref:`Tiro Multiplo<tal_tiro_multiplo>`
|
|	Dal 10° livello:
|	:ref:`Mira Inesorabile<tal_mira_inesorabile>`
|	:ref:`Tirare in Movimento<tal_tirare_in_movimento>`
|

Se il ranger sceglie *Combattere con Due Armi*: 

|	:ref:`Attacco con lo Scudo Migliorato<tal_attacco_con_lo_scudo_migliorato>`
|	:ref:`Combattere con Due Armi<tal_combattere_con_due_armi>`
|	:ref:`Doppio Taglio<tal_doppio_taglio>`
|	:ref:`Estrazione Rapida<tal_estrazione_rapida>`
|
|	Dal 6° livello:
|	:ref:`Combattere con Due Armi Migliorato<tal_combattere_con_due_armi_migliorato>`
|	:ref:`Difendere con Due Armi<tal_difendere_con_due_armi>`
|
|	Dal 10° livello:
|	:ref:`Attacco Lacerante a Due Armi<tal_attacco_lacerante_a_due_armi>`
|	:ref:`Combattere con Due Armi Superiore<tal_combattere_con_due_armi_superiore>`
|

.. Warning::
    I benefici dello stile praticato si applicano solo quando non si indossa alcuna armatura o se ne indossa una leggera o media: sono persi se si indossa un’armatura pesante.

Resistenza Fisica
------------------
:Liv Min: 3

Talento Bonus :ref:`Resistenza Fisica<tal_resistenza_fisica>`.

.. _ambiente_prescelto:

Ambiente Prescelto (Str)
------------------------
:Liv Min: 3

.. figure:: Immagini/AmbientePrescelto.png
    :align: right
    :width: 300

    Tabella degli ambienti prescelti del :ref:`Ranger<ranger>`.

Si può scegliere un tipo di ambiente naturale dalla :ref:`Tabella degli Ambienti Prescelti<ambiente_prescelto>`.

Il Ranger ottiene +2 alle prove di Iniziativa, :ref:`Conoscenze<conoscenze>` (geografia), :ref:`Furtività<furtività>`, :ref:`Percezione<percezione>` e :ref:`Sopravvivenza<sopravvivenza>` quando si trova in quell’ambiente e non lascia segni del suo passaggio nè può essere inseguito seguendo le sue tracce (sebbene possa lasciare un segno del passaggio se lo desidera). 

All’8° livello e ogni cinque livelli successivi, si può scegliere un ambiente prescelto aggiuntivo e il bonus di uno di questi ambienti prescelti (compreso quello appena selezionato, se lo si desidera) aumenta di +2.

Se un ambiente specifico ricade in più di una categoria di ambiente prescelto, i bonus non sono cumulativi e andrà utilizzato solo il bonus più elevato.

Legame del Cacciatore (Str)
----------------------------
:Liv Min: 4

Si forma un legame con i compagni di caccia, che può essere di due forme. Una volta scelta, la forma non può essere cambiata. 

La **prima forma** permette al Ranger di spendere un’:ref:`Azione di Movimento<azione_movimento>` per concedere a tutti gli alleati entro 9 metri da lui in grado di vederlo e sentirlo metà del
suo bonus contro un nemico prescelto, nei confronti di un avversario specifico. 

Questo bonus dura per Mod Sag round (minimo 1) e non si cumula con eventuali bonus contro un nemico prescelto posseduti dagli alleati: essi usano il bonus più alto.

La **seconda opzione** è quella di un legame speciale con un :ref:`Compagno Animale<compagno_animale_druido>`, che un ranger può scegliere tra: :ref:`Cammello<bestiario_cammello>`, :ref:`Cane<bestiario_cane>`, :ref:`Cane da Galoppo<bestiario_cane_da_galoppo>`, :ref:`Cavallo<bestiario_cavallo>` (leggero o pesante), :ref:`Lupo<bestiario_lupo>`, :ref:`Pony<bestiario_pony>`, serpente (:ref:`Vipera<bestiario_vipera>` Piccola o Media), :ref:`Tasso<bestiario_tasso>`, :ref:`Topo Crudele<bestiario_topo_crudele>` o uccello. 

.. Note:: 
    Se la campagna è ambientata interamente o parzialmente in un ambiente acquatico, il Ranger può scegliere invece uno :ref:`Squalo<bestiario_squalo>`.

Questo animale è un compagno leale che segue il Ranger nelle sue avventure come appropriato
per la sua specie e funziona come :ref:`Legame con la Natura<legame_con_la_natura>` del :ref:`Druido<druido>`, ma con un livello effettivo da Druido pari a Liv Ranger – 3.

Incantesimi
------------
:Liv Min: 4

Si ottiene la capacità di lanciare un piccolo numero di :ref:`Incantesimi Divini da Ranger<incantesimi_ranger>`.

Un ranger deve scegliere e preparare i suoi incantesimi in anticipo e per preparare o lanciare un incantesimo, deve avere un punteggio di almeno 10 + Liv Incantesimo di Saggezza. Per la preparazione è necessaria 1 ora al giorno in tranquilla meditazione e si può preparare qualsiasi incantesimo della lista, ammesso di poterlo lanciare.

La Classe Difficoltà per un TS contro un incantesimo da Ranger è 10 + Liv Incantesimo + Mod Sag e, come gli altri incantatori, un Ranger può lanciare solo un :ref:`certo numero<tabella_avanzamento_ranger>` di incantesimi al giorno per ogni livello con un bonus dipendente dal punteggio Saggezza.

.. Note::
    Laddove la :ref:`Tabella<tabella_avanzamento_ranger>` indica che 0 incantesimi al giorno, si intende che si ottengono solo gli incantesimi bonus a cui avrebbe diritto in base alla sua Saggezza.

Fino al 3° livello, il Ranger non possiede un Livello da Incantatore e dal 4° livello in poi il suo LI è Liv Ranger – 3.

.. figure:: Immagini/Ranger2.png
    :align: left
    :width: 400

Andatura nel Bosco (Str)
-------------------------
:Liv Min: 7

Ci si può muovere attraverso qualsiasi tipo di sottobosco (come rovi e sterpi naturali, zone infestate e simili terreni) a velocità normale e senza subire danni o altri impedimenti.

.. Warning::
    Questo non vale per rovi, sterpi e aree infestate che sono incantati o manipolati magicamente per impedire il movimento.

Rapido Segugio (Str)
---------------------
:Liv Min: 8

Ci si può muovere velocità normale mentre si usa :ref:`Sopravvivenza<sopravvivenza>` per seguire le tracce, senza subire la penalità –5, e si subisce solo –10 (anzichè –20) quando ci si muove fino al doppio della normale velocità mentre si seguono tracce.

.. _eludere_ranger:

Eludere (Str)
--------------
:Liv Min: 9

Si può evitare con grande agilità anche gli attacchi magici e insoliti. 

Se si supera un TS su Riflessi contro un attacco che normalmente infliggerebbe la metà dei danni in caso di TS riuscito, non si subisce alcun danno. 

.. Warning::
    *Eludere* può essere usata solo se il Ranger non indossa alcuna armatura o ne indossa una leggera o media e un Ranger indifeso non riceve i benefici di *Eludere*.

.. _preda_ranger:

Preda (Str)
-----------
:Liv Min: 11 
:Tipo: :ref:`Azione Standard<azione_standard>`

Si può indicare un bersaglio all’interno della linea di visuale come preda. 

Ogni volta che si sta seguendo le tracce della preda, si può prendere 10 alle prove di :ref:`Sopravvivenza<sopravvivenza>` mentre ci si muove a velocità normale, senza subire penalità; si riceve bonus cognitivo +2 ai tiri per colpire effettuati contro la preda e tutte le minacce di critico sono confermate automaticamente.

Un Ranger non può avere più di una preda alla volta ed il tipo della creatura deve corrispondere a quello di un suo :ref:`Nemico Prescelto<nemico_prescelto>`, ma può in qualunque momento abbandonare questo effetto come :ref:`Azione Gratuita<azione_gratuita>`, senza selezionare una nuova preda per 24 ore. 

Se si ha la prova che la preda è morta, se ne può selezionare una nuova dopo aver atteso 1 ora.

Mimetismo (Str)
----------------
:Liv Min: 12

Si può usare :ref:`Furtività<furtività>` per nascondersi in qualsiasi tipo di :ref:`Ambiente Naturale Prescelto<ambiente_prescelto>`, anche se il terreno non conferisce copertura o occultamento.

Eludere Migliorato (Str)
-------------------------
:Liv Min: 16

:ref:`Eludere<eludere_ranger>` migliora: se il TS su Riflessi contro gli attacchi fallisce si subiscono solo la metà dei danni.

Nascondersi in Piena Vista (Str)
----------------------------------
:Liv Min: 17 

In uno qualsiasi degli :ref:`Ambienti Naturali Prescelti<ambiente_prescelto>` si può usare :ref:`Furtività<furtività>` per nascondersi anche mentre si viene osservati.

Preda Migliorata (Str)
-------------------------
:Liv Min: 19

:ref:`Preda<preda_ranger>` migliora: si può selezionare una preda come :ref:`Azione Gratuita<azione_gratuita>` e prendere 20 alle prove di :ref:`Sopravvivenza<sopravvivenza>` mentre ci si muove a velocità normale, senza subire penalità. 

Il bonus cognitivo ai tiri per colpire effettuati contro la preda aumenta a +4 e se la preda muore o viene abbandonata, se ne può selezionare una nuova dopo 10 minuti.

Maestro Cacciatore (Str)
-------------------------
:Liv Min: 20

Il *Maestro Cacciatore* può muoversi sempre alla sua piena velocità mentre usa :ref:`Sopravvivenza<sopravvivenza>` per seguire le tracce senza subire penalità e può, come :ref:`Azione Standard<azione_standard>`, effettuare un singolo attacco contro un :ref:`Nemico Prescelto<nemico_prescelto>` al suo bonus di attacco completo. 

Se l’attacco colpisce, il bersaglio subisce il danno normalmente e deve superare un TS su Tempra
o muore. La CD di questo TS è 10 + 0.5 * Liv Ranger+ Mod Sag e si può anche scegliere di infliggere una quantità di danno non letale uguale ai punti ferita attuali della creatura.

Si può utilizzare questa capacità una volta al giorno contro ogni tipo di :ref:`Nemico Prescelto<nemico_prescelto>` che si possiede, ma non più di una volta contro la stessa creatura nell’arco di 24 ore.

.. _stregone:

Stregone
==================================

Discendenti di stirpi per loro natura magiche, prescelti dagli déi, progenie di mostri, pedine del destino e del fato, o semplicemente casi fortuiti della imprevedibilità della magia, gli stregoni traggono da se stessi la loro forza arcana che rilasciano in modi che pochi mortali possono immaginare. 

Queste anime toccate dalla magia assecondano continuamente ed affinano le loro
capacità misteriose, imparando gradualmente a sfruttare il loro retaggio e a ricercare con voluttà talenti arcani sempre maggiori. 

Mentre alcuni cercano di dominare le loro capacità con la meditazione e la disciplina, diventando maestri della loro stirpe, altri cedono alla loro stessa magia,
lasciandola governare le loro vite con risultati spesso sorprendenti. 

Gli stregoni vivono e respirano quello che altri incantatori perseguono dedicando vite intere, e per loro la magia è più di un vantaggio o di un campo di studio: è la vita stessa.

:Ruolo: 

    Gli stregoni eccellono nel lanciare una selezione di incantesimi preferiti frequentemente, e ciò li rende potenti incantatori in combattimento.

    Acquisiscono dimestichezza con uno specifico insieme di incantesimi, ma scoprono spesso nuovi e versatili usi della magia che altri incantatori potrebbero trascurare.

    La loro stirpe concede capacità addizionali, che assicurano che non ci saranno mai due stregoni simili

.. figure:: Immagini/Stregone.png
    :align: right
    :width: 400

:Allineamento: Qualsiasi
:Dado Vita: d6

----------------------------
Abilità di classe
----------------------------

:Crescita per livello: 2 + modificatore Intelligenza (Mod Int)

*	:ref:`Artigianato (Int)<artigianato>`
*	:ref:`Conoscenze (arcane) (Int)<conoscenze>`
*	:ref:`Intimidire (Car)<intimidire>`
*	:ref:`Professione (Sag)<professione>`
*	:ref:`Raggirare (Car)<raggirare>`
*	:ref:`Sapienza Magica (Int)<sapienza_magica>`
*	:ref:`Utilizzare Congegni Magici (Car)<utilizzare_congegni_magici>`
*	:ref:`Valutare (Int)<valutare>`
*	:ref:`Volare (Des)<volare>`

--------------------
Privilegi di Classe
--------------------

.. contents:: Riassumendo:
    :local:

Competenze nelle armi e armature
----------------------------------

Tutte le armi semplici. 

Nessun tipo di armatura né di scudo. Le armature di qualsiasi tipo interferiscono con i gesti arcani di uno stregone e possono provocare il fallimento dei suoi incantesimi con componenti somatiche.

Incantesimi
-------------
.. _avanzamento_stregone:

.. figure:: Immagini/AvanzamentoStregone.png

:Lista: :ref:`Incantesimi da Stregone<incantesimi_mago>`
:Preparazione: No

Per imparare, preparare o lanciare un incantesimo, è necessario un Carisma ≥ 10 + Liv Incantesimo.
La CD per un tiro salvezza contro un incantesimo da mago è 10 + il Liv Incantesimo + Mod Car.

Come gli altri incantatori, lo strgone ha un :ref:`numero limitato di incantesimi al giorno<avanzamento_stregone>` che può essere modificata a seconda del Mod Car, :ref:`com'è usuale<incantesimi_bonus>`.

.. _incantesimi_stregone:

.. figure:: Immagini/IncantesimiStregone.png
    :align: right
    :width: 250

La selezione di incantesimi di uno stregone è estremamente limitata cominciando la propria carriera conoscendo 4 incantesimi di livello 0 e due di 1° livello di sua scelta, crescendo con il livello come nella tabella a fianco. 

Questi, nuovi incantesimi possono essere incantesimi comuni scelti dalla lista degli incantesimi da mago/stregone o insoliti che lo stregone ha studiato o si è procurato in qualche modo.

Al 4° livello, e ad ogni livello successivo pari (6°, 8°, e così via), uno stregone può scegliere di imparare un nuovo incantesimo al posto di un incantesimo che già conosce, perdendo quello vecchio. 

Il livello del nuovo incantesimo deve essere identico a quello dell’incantesimo che
è stato scambiato. 

Stirpe
----------
Ogni stregone ha una fonte di magia da qualche parte della sua discendenza che gli concede incantesimi, talenti bonus, capacità di classe addizionali ed altre capacità
speciali. 

Questa fonte può essere un’eredità del sangue o un evento estremo che coinvolge una creatura da qualche parte nel passato della sua famiglia. Per esempio, uno stregone
potrebbe avere un drago come ascendente distante o suo nonno potrebbe aver firmato un contratto terribile con un diavolo.

Questa influenza si manifesta in vari modi mentre lo stregone sale di livello.

Al 1° livello lo stregone sceglie una stirpe, che non può essere cambiata ed al 3° livello e ogni due successivi, impara un incantesimo addizionale, tratto da quelli della sua stirpe, in aggiunta a quelli normali. 

Questi incantesimi non possono essere scambiati per altri incantesimi ai livelli più alti.

Al 7° livello e ogni sei livelli successivi, uno stregone riceve un talento bonus, scelto tra quelli specifici della sua stirpe, per cui deve soddisfare i prerequisiti.

Trucchetti
------------
Si possono preparare un :ref:`certo numero<incantesimi_stregone>` di trucchetti, o incantesimi di livello 0, ogni giorno che sono lanciati come ogni altro incantesimo,
ma non vengono consumati e possono essere utilizzati di nuovo.

Escludere materiali
--------------------
Talento Bonus :ref:`Escludere Materiali<tal_escludere_materiali>`.

------------------------------------------------------

.. figure:: Immagini/Stregone2.png
    :align: right
    :width: 400

--------------------
Stirpi
--------------------
Le stirpi seguenti rappresentano solo alcune delle possibili fonti di potere a cui uno stregone può attingere. A meno che non sia indicato diversamente, si presume che la maggior parte degli stregoni abbia la stirpe arcana.

.. contents:: Stirpi dello Stregone:
    :local:

.. _stregone_aberrante:

Aberrante
--------------------
Sangue corrotto da un ancestrale retaggio alieno e bizzarro scorre nella propria famiglia: il proprio modo di pensare è strano e contorto, guardando ai problemi da angolature insolite per la maggior parte delle persone. 

Col passare del tempo, questa corruzione si manifesta anche nell’aspetto fisico.

Abilità di classe
*****************
:ref:`Conoscenze<conoscenze>` (dungeon)

Incantesimi Bonus
******************

*	:ref:`Ingrandire Persone<inc_ingrandire_persone>` (3°)
*	:ref:`Vedere Invisibilità<inc_vedere_invisibilità>` (5°)
*	:ref:`Linguaggi<inc_linguaggi>` (7°)
*	:ref:`Tentacoli Neri<inc_tentacoli_neri>` (9°)
*	:ref:`Regressione Mentale<inc_regressione_mentale>` (11°)
*	:ref:`Velo<inc_velo>` (13°)
*	:ref:`Spostamento Planare<inc_spostamento_planare>` (15°)
*	:ref:`Vuoto Mentale<inc_vuoto_mentale>` (17°)
*	:ref:`Trasformazione<inc_trasformazione>` (19°)

Talenti Bonus
**************

* :ref:`Abilità Focalizzata<tal_abilità_focalizzata>` (Conoscenze [dungeon])
* :ref:`Colpo Senz'Armi Migliorato<tal_colpo_senzarmi_migliorato>`
* :ref:`Disarmare Migliorato<tal_disarmare_migliorato>`
* :ref:`Incantare in Combattimento<tal_incantare_in_combattimento>`
* :ref:`Incantesimi Silenziosi<tal_incantesimi_silenziosi>`
* :ref:`Iniziativa Migliorata<tal_iniziativa_migliorata>`
* :ref:`Lottare Migliorato<tal_lottare_migliorato>`
* :ref:`Volontà di Ferro<tal_volontà_di_ferro>`

Magia della Stirpe
********************
Ogni volta che viene lanciato un incantesimo della sottoscuola metamorfosi, si aumenta la durata
dell’incantesimo del 50% (minimo 1 round). 

Questo bonus non si cumula con l’aumento dato da :ref:`Incantesimi Estesi<tal_incantesimi_estesi>`.

Poteri della Stirpe
*********************
Gli stregoni aberranti mostrano segni crescenti del retaggio aberrante mentre avanzano di livello, benchè essi siano visibili solo quando utilizzati.

***********************
Raggio Acido (Mag)
***********************
:Tipo: :ref:`Azione Standard<azione_standard>`

Si può emettere un raggio acido che abbia un bersaglio entro 9 metri come :ref:`Attacco di Contatto a Distanza<azione_attacco_di_contatto_a_distanza>`. 

Il raggio infligge 1d6 + 0.5 * Liv Stregone danni da acido e si può utilizzare 3 + Mod Car volte al giorno.

****************************
Arti Lunghi (Str)
****************************
:Liv Min: 3

La propria portata aumenta di 1,5 metri ogni volta che viene compiuto un :ref:`Attacco di Contatto in Mischia<azione_attacco_di_contatto_in_mischia>`. 

.. Caution::
    Questa capacità non aumenta l’area che si minaccia.

All’11° livello il bonus aumenta a 3 metri e al 17° a 4,5 metri.

***************************** 
Anatomia Insolita (Str)
*****************************
:Liv Min: 9

La propria anatomia cambia, conferendo una probabilità del 25% di ignorare i danni da qualsiasi colpo critico o attacco furtivo messo a segno, che aumenta a 50% al 13° livello.

**************************
Resistenza Aliena (Sop)
**************************
:Liv Min: 15

Si ottiene resistenza agli incantesimi pari a Liv Stregone +10.

***********************
Forma Aberrante (Str)
***********************
:Liv Min: 20

L’organismo diventa completamente innaturale: si ottiene immunità ai colpi critici ed agli attacchi furtivi; si ottiene :ref:`Vista Cieca<vista_cieca>` fino a 18 metri e riduzione del danno 5/—.

.. _stregone_abissale:

Abissale
--------------------
Generazioni fa, un demone ha corrotto la propria discendenza.

Mentre non si manifesta in tutta la famiglia, questa corruzione è particolarmente forte nello Stregone, che a volte può avere urgenze a commettere atti caotici o malvagi, anche se il destino (ed allineamento) resta nelle sue mani.

Abilità di classe
*****************
:ref:`Conoscenze<conoscenze>` (piani)

Incantesimi Bonus
******************

*	:ref:`Incuti Paura<inc_incuti_paura>` (3°)
*	:ref:`Forza del Toro<inc_forza_del_toro>` (5°)
*	:ref:`Ira<inc_ira>` (7°)
*	:ref:`Pelle di Pietra<inc_pelle_di_pietra>` (9°)
*	:ref:`Congedo<inc_congedo>` (11°)
*	:ref:`Trasformazione<inc_trasformazione>` (13°)
*	:ref:`Teletrasporto Superiore<inc_teletrasporto_superiore>` (15°)
*	:ref:`Aura Sacrilega<inc_aura_sacrilega>` (17°)
*	:ref:`Evoca Mostri IX<inc_evoca_mostri_IX>` (19°)

Talenti Bonus
**************

* :ref:`Abilità Focalizzata<tal_abilità_focalizzata>` (Conoscenze [piani])
* :ref:`Attacco Poderoso<tal_attacco_poderoso>`
* :ref:`Aumentare Evocazione<tal_aumentare_evocazione>`
* :ref:`Incalzare<tal_incalzare>`
* :ref:`Incantesimi Potenziati<tal_incantesimi_potenziati>`
* :ref:`Spezzare Migliorato<tal_spezzare_migliorato>`
* :ref:`Spingere Migliorato<tal_spingere_migliorato>`
* :ref:`Tempra Possente<tal_tempra_possente>`

Magia della Stirpe
********************
Ogni volta che si lancia un incantesimo della sottoscuola di evocazione, le creature convocate guadagnano RD/bene pari a 0.5 * Liv Stregone (minimo 1) che non si cumula con altre RD possedute dalla creatura.

Poteri della Stirpe
*********************
Mentre alcuni considerano lo stregone solo un posseduto, egli sa che c’è di più. 

L’influenza demoniaca nel suo sangue cresce mentre egli guadagna potere.

***********************
Artigli (Sop)
***********************
:Tipo: :ref:`Azione Gratuita<azione_gratuita>`

Si possono far crescere degli artigli che sono considerati armi naturali e permettono di effettuare due attacchi con gli artigli come :ref:`Azione di Attacco Completo<azione_di_attacco_completo>` usando il proprio bonus di attacco completo. 

Questi attacchi infliggono 1d4 danni ciascuno (1d3 se si è di taglia Piccola) più il Mod For.

Al 5° livello questi artigli sono considerati armi magiche al fine di superare la RD; al 7° il danno aumenta a 1d6 danni (1d4 se si è di taglia Piccola); all'11° gli artigli si trasformano in :ref:`Armi Infuocate<armi_infuocata>`, e ciascuno infligge 1d6 danni da fuoco addizionali ad ogni colpo andato a segno. 

Si possono utilizzare i propri artigli 3 + Mod Car round al giorno.

****************************
Resistenze Demoniache (Str)
****************************
:Liv Min: 3

Si ottiene resistenza all’elettricità 5 e +2 ai TS contro veleno, che aumentano a 10 e +4 al 9°
livello.

***************************** 
Forza dell’Abisso (Str)
*****************************
:Liv Min: 9

Si ottiene bonus intrinseco +2 a Forza che aumenta a +4 al 13° livello e a +6 al 17°.

***************************
Evocazioni Aggiuntive (Sop)
***************************
:Liv Min: 15

Ogni volta che si evoca una creatura con il sottotipo demone o l’archetipo immondo usando un incantesimo :ref:`Evoca Mostri<inc_evoca_mostri>`, si evoca una creatura addizionale dello stesso tipo. 

***********************
Potenza Demoniaca (Sop)
***********************
:Liv Min: 20

Il potere dell’Abisso scorre attraverso il proprio corpo. 

Si ottiene immunità a elettricità e veleno, resistenza ad acido 10, freddo 10 e fuoco 10; si guadagna inoltre telepatia entro un raggio di 18 metri (che permette di comunicare con qualsiasi
creatura in grado di parlare un linguaggio).

.. _stregone_arcana:

Arcana
--------------------
La famiglia dello stregone è stata sempre esperta nell’arte arcana della magia. Anche se molti dei suoi parenti erano :ref:`Maghi<mago>`, i suoi poteri si sono sviluppati senza bisogno di studio e pratica.

Abilità di classe
*****************
:ref:`Conoscenze<conoscenze>` (qualsiasi)

Incantesimi Bonus
******************

*	:ref:`Identificare<inc_identificare>` (3°)
*	:ref:`Invisibilità<inc_invisibilità>` (5°)
*	:ref:`Dissolvi Magie<inc_dissolvi_magie>` (7°)
*	:ref:`Porta Dimensionale<inc_porta_dimensionale>` (9°)
*	:ref:`Volo Giornaliero<inc_volo_giornaliero>` (11°)
*	:ref:`Visione del Vero<inc_visione_del_vero>` (13°)
*	:ref:`Teletrasporto Superiore<inc_teletrasporto_superiore>` (15°)
*	:ref:`Parola del Potere: Stordire<inc_parola_del_potere_stordire>` (17°)
*	:ref:`Desiderio<inc_desiderio>` (19°)

Talenti Bonus
**************

* :ref:`Abilità Focalizzata<tal_abilità_focalizzata>` (Conoscenze [arcane]])
* :ref:`Controincantesimo Migliorato<tal_controincantesimo_migliorato>`
* :ref:`Incantare in Combattimento<tal_incantare_in_combattimento>`
* :ref:`Incantesimi Focalizzati<tal_incantesimi_focalizzati>`
* :ref:`Incantesimi Immobili<tal_incantesimi_immobili>`
* :ref:`Iniziativa Migliorata<tal_iniziativa_migliorata>`
* :ref:`Scrivere Pergamene<tal_scrivere_pergamene>`
* :ref:`Volontà di Ferro<tal_volontà_di_ferro>`

Magia della Stirpe
********************
Ogni volta che si applica un talento metamagico ad un incantesimo che aumenta lo slot utilizzato
di almeno un livello, si aumenta la CD dell’incantesimo di +1.

Questo bonus non si cumula con se stesso e non si applica agli incantesimi modificati da :ref:`Incantesimi Intensificati<tal_incantesimi_intensificati>`.

Poteri della Stirpe
*********************
La magia viene naturale, ma mentre avanza di livello, occorre che lo stregone impedisca che il
potere lo possa sopraffare.

***********************
Legame Arcano (Sop)
***********************
Si ottiene un :ref:`Legame Arcano<legame_arcano>`, come un :ref:`Mago<mago>` del proprio Liv Stregone. 

I propri Liv Stregone si cumulano con qualsiasi Liv Mago posseduto quando si determinano i poteri dell’:ref:`Oggetto Legato<oggetto_legato_mago>` o :ref:`Famiglio<famiglio_mago>`.

.. Caution::
    Questa capacità non permette di avere sia un :ref:`Oggetto Legato<oggetto_legato_mago>` che un :ref:`Famiglio<famiglio_mago>`.

****************************
Adepto di Metamagia (Str)
****************************
:Liv Min: 3

Si può applicare un talento metamagico conosciuto ad un incantesimo che si sta per lanciare senza aumentarne il tempo di lancio, ma si deve sempre consumare uno slot incantesimo di più alto livello per lanciare questo incantesimo. 

Si può utilizzare questa capacità una volta al giorno al 3° livello e successivamente una volta in più ogni quattro livelli da Stregone, fino ad un massimo di cinque volte al giorno. Al 20° livello, questa capacità è sostituita da :ref:`Apoteosi Arcana<apoteosi_arcana>`.

***************************** 
Nuovi Arcani (Str)
*****************************
:Liv Min: 9

Si può aggiungere un incantesimo qualsiasi dalla lista degli :ref:`Incantesimi da Stregone<incantesimi_mago>` alla propria lista di incantesimi conosciuti, ammesso di poterlo lanciare.

Si può aggiungere un altro incantesimo al 13° e al 17° livello.

***************************
Potere della Scuola (Str)
***************************
:Liv Min: 15

Si sceglie una :ref:`Scuola di Magia<scuola_magia>`: la CD degli incantesimi di questa scuola che si lanciano aumenta di +2. 

Questo bonus si cumula con quello concesso da :ref:`Incantesimi Focalizzati<tal_incantesimi_focalizzati>`.

.. _apoteosi_arcana:

***********************
Apoteosi Arcana (Str)
***********************
:Liv Min: 20

Il corpo si alimenta di potenza arcana: si può aggiungere qualsiasi talento metamagico conosciuto ai propri incantesimi senza aumentarne il tempo di lancio, anche se bisogna sempre
consumare slot incantesimi di livello più alto. 

Ogni volta che si utilizza un oggetto magico che richiede cariche, si possono invece consumare slot incantesimi per attivare l’oggetto e ogni tre livelli di slot incantesimi consumati, si consuma una carica in meno.

.. _stregone_celestiale:

Celestiale
--------------------
La propria stirpe è benedetta dal potere celestiale, dato o da un antenato celestiale o  dall’intervento divino. 

Benchè questi poteri conducano verso il bene, il destino (ed allineamento) è determinato dalle proprie azioni.

Abilità di classe
*****************
:ref:`Guarire`

Incantesimi Bonus
******************

*	:ref:`Benedizione<inc_benedizione>` (3°)
*	:ref:`Resistere all'Energia<inc_resistere_allenergia>` (5°)
*	:ref:`Cerchio Magico contro il Male<inc_cerchio_magico_contro_il_male>` (7°)
*	:ref:`Rimuovi Maledizione<inc_rimuovi_maledizione>` (9°)
*	:ref:`Colpo Infuocato<inc_colpo_infuocato>` (11°)
*	:ref:`Dissolvi Magie Superiore<inc_dissolvi_magie_superiore>` (13°)
*	:ref:`Esilio<inc_esilio>` (15°)
*	:ref:`Esplosione Solare<inc_esplosione_solare>` (17°)
*	:ref:`Portale<inc_portale>` (19°)

Talenti Bonus
**************

* :ref:`Abilità Focalizzata<tal_abilità_focalizzata>` (Conoscenze [religioni]])
* :ref:`Arma Accurata<tal_arma_accurata>`
* :ref:`Attacco in Sella<tal_attacco_in_sella>`
* :ref:`Combattere in Sella<tal_combattere_in_sella>`
* :ref:`Incantesimi Estesi<tal_incantesimi_estesi>`
* :ref:`Mobilità<tal_mobilità>`
* :ref:`Schivare<tal_schivare>`
* :ref:`Volontà di Ferro<tal_volontà_di_ferro>`

Magia della Stirpe
********************
Ogni volta che si lancia un incantesimo della sottoscuola di :ref:`Evocazione`, le creature convocate guadagnano 0.5 * Liv Stregone (minimo 1) RD/male che non si cumula con altre RD possedute dalla creatura.

Poteri della Stirpe
*********************
La propria eredità celestiale concede enormi poteri, ma ad un prezzo: i signori dei piani celestiali osservano molto attentamente lo Stregone e le sue azioni.

***********************
Fuoco Celestiale (Mag)
***********************
:Tipo: :ref:`Azione Standard<azione_standard>`

Si può emanare un raggio di fuoco celestiale, che abbia come bersaglio qualunque nemico entro 9 metri come:ref:`Attacco di Contatto a Distanza<azione_attacco_di_contatto_a_distanza>`. 

Contro le **creature malvagie**, questo raggio infligge 1d4 + 0.5 * Liv Chierico danni divini e
non soggetti a resistenza o immunità all’energia. 

Le **creature buone** vengono curate per lo stesso valore, ma non possono trarre giovamento più di una volta al giorno da *Fuoco Celestiale*. 

Le **creature neutrali** non sono né danneggiate né curate da questo effetto. 

Si può utilizzare 3 + Mod Car volte al giorno.

****************************
Resistenze Celestiali (Str)
****************************
:Liv Min: 3

Si ottiene resistenza all’acido 5 e resistenza al freddo 5 che aumentano a 10 al 9° livello.

.. _ali_del_paradiso:

***************************** 
Ali del Paradiso (Sop)
*****************************
:Liv Min: 9

Si possono far spuntare delle ali e volare per Liv Stregon minuti al giorno, con velocità 18 metri e manovrabilità buona. 

Questa durata non deve essere consecutiva, ma deve essere utilizzata in incrementi di 1 minuto.

***************************
Convinzione (Sop)
***************************
:Liv Min: 15

Si può ritirare qualsiasi tiro per colpire, TS, prova di caratteristica, o prova di abilità appena effettuato ma prima che il risultato venga rivelato dal GM, tenendo il risultato del secondo tiro, anche se peggiore. 

Si può utilizzare questa capacità una volta al giorno.

***********************
Ascensione (Sop)
***********************
:Liv Min: 20

Si è infusi con il potere dei cieli: si ottiene immunità a acido, freddo e pietrificazione, resistenza a elettricità e fucoo 10 e bonus razziale +4 ai TS contro veleno.

In più, si ottiene un uso illimitato della capacità :ref:`Ali del Paradiso<ali_del_paradiso>` e si guadagna la capacità di parlare con tutte la creature che hanno un linguaggio (come per
l’incantesimo :ref:`Linguaggi<inc_linguaggi>`).

.. _stregone_draconica:

Draconica
--------------------
Ad un certo punto nella storia di famiglia, un drago si è incrociato con la stirpe dello Stregone ed ora il suo potere antico scorre nel suo sangue.

Abilità di classe
*****************
:ref:`Percezione<percezione>`

Incantesimi Bonus
******************

*	:ref:`Armatura Magica<inc_armatura_magica>` (3°)
*	:ref:`Resistere all'Energia<inc_resistere_allenergia>` (5°)
*	:ref:`Volare<inc_volare>` (7°)
*	:ref:`Paura<inc_paura>` (9°)
*	:ref:`Resistenza agli Incantesimi<inc_resistenza_agli_incantesimi>` (11°)
*	:ref:`Forma di Drago I<inc_forma_di_drago_I>` (13°)
*	:ref:`Forma di Drago II<inc_forma_di_drago_II>` (15°)
*	:ref:`Forma di Drago III<inc_forma_di_drago_III>` (17°)
*	:ref:`Desiderio<inc_desiderio>` (19°)

Talenti Bonus
**************
* :ref:`Abilità Focalizzata<tal_abilità_focalizzata>` (:ref:`Conoscenze<conoscenze>` [arcane]])
* :ref:`Abilità Focalizzata<tal_abilità_focalizzata>` (:ref:`Volare<volare>`)
* :ref:`Attacco Poderoso<tal_attacco_poderoso>`
* :ref:`Combattere alla Cieca<tal_combattere_alla_cieca>`
* :ref:`Incantesimi Rapidi<tal_incantesimi_rapidi>`
* :ref:`Iniziativa Migliorata<tal_iniziativa_migliorata>`
* :ref:`Robustezza<tal_robustezza>`
* :ref:`Tempra Possente<tal_tempra_possente>`

Magia della Stirpe
********************
Ogni volta che si lancia un incantesimo con descrittore di energia uguale a quello della propria
stirpe draconica, quell’incantesimo infligge +1 danno per ogni dado tirato.

Poteri della Stirpe
*********************
Il potere dei draghi vi attraversa e si manifesta in vari modi: al 1° livello, bisogna selezionare uno dei draghi :ref:`Cromatici<draghi_cromatici>` o :ref:`Metallici<draghi_metallici>`, senza poter cambiare la scelta una volta fatta. 

Diverse resistenze e danni dipenderanno dal tipo di drago selezionato, come indicato nella tabella seguente: 

.. image:: Immagini/StregoneDraconico.png

***********************
Artigli (Sop)
***********************
:Tipo: :ref:`Azione Gratuita<azione_gratuita>`

Si possono far crescere degli artigli: considerati come armi naturali, permettono di effettuare due attacchi con gli artigli come :ref:`Azione di Attacco Completo<azione_di_attacco_completo>` usando il proprio bonus di attacco completo. 

Questi attacchi infliggono 1d4 + Mod For danni ciascuno (1d3 se si è di taglia Piccola). 

Al 5° livello questi artigli sono considerati armi magiche al fine di superare la RD, al
7° il danno aumenta a 1d6 danni (1d4 se si è di taglia Piccola); all’11° infliggono 1d6 danni
da energia scelta addizionali ad ogni colpo andato a segno.

I round giornalieri permessi sono 3 + Mod Car.

****************************
Resistenze del Drago (Str)
****************************
:Liv Min: 3

Si ottiene resistenza 5 al tipo di energia scelta e +1 all’armatura naturale, che aumentanno a 10 e +2 al 9° livello. 

Al 15° livello, il bonus all’armatura naturale aumenta a +4.

***************************** 
Soffio (Sop)
*****************************
:Liv Min: 9

Una volta al giorno si ottiene un’arma a soffio che infligge 1d6 * Liv Stregone danni del tipo di energia scelta.

Chi si trova nell’area del soffio riceve un TS su Riflessi per dimezzare il danno, con **CD = 10 + 0.5 * Liv Stregone + Mod Car**. La forma del soffio dipende dal tipo di drago. 

Al 17° e al 20° livello si ottiene un utilizzo giornaliero aggiuntivo.

***************************
Ali (Sop)
***************************
:Liv Min: 15
:Tipo: :ref:`Azione Standard<azione_standard>`

Ali da drago spuntano sulla schiena, conferendo una velocità di volare di 18 metri con manovrabilità media. 

Si possono far sparire le ali con un’:ref:`Azione Gratuita<azione_gratuita>`.

***********************
Potere dei Wyrm (Sop)
***********************
:Liv Min: 20

La propria stirpe draconica si rende ancora più evidente: si ottiene immunità a paralisi, sonno e danni da energia del tipo scelto. Si ottiene inoltre :ref:`Percezione Cieca<percezione_cieca>` fino a 18 metri.

Elementale
--------------------
Il potere degli elementi alberga nel proprio sangue e a volte è difficile dominare la loro furia. 

Questa influenza proviene da un esterno elementale nella storia di famiglia o da un
momento in cui lo stregone o qualcuno di famiglia è stato esposto ad una potente forza elementale.

Abilità di classe
*****************
:ref:`Conoscenze<conoscenze>` (piani)

Incantesimi Bonus
******************
Gli incantesimi con asterisco infliggono un tipo di danno determinato dall'elemento scelto della stirpe e il sottotipo cambia per conformarsi a tale elemento.

*	:ref:`Mani Brucianti*<inc_mani_brucianti>` (3°)
*	:ref:`Raggio Rovente*<inc_raggio_rovente>` (5°)
*	:ref:`Protezione dall’Energia<inc_protezione_dallenergia>` (7°)
*	:ref:`Corpo Elementale I<inc_corpo_elementale_I>` (9°)
*	:ref:`Corpo Elementale II<inc_corpo_elementale_II>` (11°)
*	:ref:`Corpo Elementale III<inc_corpo_elementale_III>` (13°)
*	:ref:`Corpo Elementale IV<inc_corpo_elementale_IV>` (15°)
*	:ref:`Evoca Mostri VIII<inc_evoca_mostri_VIII>` (solo elementali) (17°)
*	:ref:`Sciame Elementale<inc_sciame_elementale>` (19°)

Talenti bonus
**************

* :ref:`Abilità Focalizzata<tal_abilità_focalizzata>` (:ref:`Conoscenze<conoscenze>` [piani])
* :ref:`Arma Accurata<tal_arma_accurata>`
* :ref:`Attacco Poderoso<tal_attacco_poderoso>`
* :ref:`Incantesimi Potenziati<tal_incantesimi_potenziati>`
* :ref:`Iniziativa Migliorata<tal_iniziativa_migliorata>`
* :ref:`Riflessi Fulminei<tal_riflessi_fulminei>`
* :ref:`Schivare<tal_schivare>`
* :ref:`Tempra Possente<tal_tempra_possente>`

Magia della Stirpe
********************
Ogni volta che si lancia un incantesimo che infligge danni da energia, si può cambiare il tipo di danno per conformarlo al tipo di elemento della propria stirpe, cambiandone anche il tipo.

.. _stregone_elementale:

.. image:: Immagini/StregoneElementale.png

Poteri della Stirpe
*********************
Uno dei quattro elementi infonde la propria persona e si può attingere ad esso in caso di bisogno, scelto al 1° livello: acqua, aria, fuoco o terra. 

Alcune capacità proprie concedono resistenze e infliggono danni in base all’elemento scelto.

***********************
Raggio Elementale (Mag)
***********************
:Tipo: :ref:`Azione Standard<azione_standard>`

Si può emanare un raggio elementale che abbia come bersaglio qualunque nemico entro 9 metri con un :ref:`Attacco di Contatto a Distanza<azione_attacco_di_contatto_a_distanza>`. 

Questo raggio infligge 1d6 danni del tipo di energia relativo all’elemento scelto +1 danno da energia ogni due livelli da stregone posseduti. 

Si può utilizzare questa capacità un numero di volte al giorno pari a 3 + Mod Car.

****************************
Resistenza Elementale (Str)
****************************
:Liv Min: 3

 \+10 resistenza al tipo di energia scelto. 

 Al 9° livello la resistenza aumenta a 20.

***************************** 
Esplosione Elementale (Mag)
*****************************
:Liv Min: 9

Un’esplosione di potere elementale con raggio di 6 metri e che infligge 1d6 + Liv Stregone danni del tipo scelto, utilizzabile una volta al giorno.

Coloro che si trovano nell’area dell’esplosione ricevono un tiro salvezza su Riflessi, con **CD = 10 + 0.5 Liv Stregone + Mod Car**, per dimezzare il danno subito. Le creature che falliscono il tiro ottengono vulnerabilità al tipo scelto fino alla fine del turno seguente dello stregone. 

Al 17° livello si può utilizzare questa capacità due volte al giorno e al 20° tre. 

Questo potere ha un raggio di azione di 18 metri.

**************************
Movimento Elementale (Sop)
**************************
:Liv Min: 15

A seconda dell'elemento scelto si ottiene un speciale tipo di movimento o bonus, consultabile in :ref:`tabella<stregone_elementale>`.

***********************
Corpo Elementale (Sop)
***********************
:Liv Min: 20

Il potere elementale attraversa il proprio corpo e si ottiene immunità agli attacchi
furtivi, ai colpi critici e ai danni da energia dell’elemento scelto.

.. _stregone_eletta:

Eletta
--------------------
La famiglia dello stregone è destinata in qualche modo a gesta eroiche. 

La sua nascita potrebbe essere stata predetta in una profezia, o forse è accaduta durante un evento particolarmente d’auspicio, come un’eclissi solare. A prescindere dall’origine della stirpe, lo stregone ha un grande futuro davanti a sè.

Abilità di classe
*****************
:ref:`Conoscenze<conoscenze>` (storia)

Incantesimi Bonus
******************
*	:ref:`Allarme<inc_allarme>` (3°)
*	:ref:`Sfocatura<inc_sfocatura>` (5°)
*	:ref:`Protezione dall'Energia<inc_protezione_dallenergia>` (7°)
*	:ref:`Libertà di Movimento<inc_libertà_di_movimento>` (9°)
*	:ref:`Spezzare Incantamento<inc_spezzare_incantamento>` (11°)
*	:ref:`Fuorviare<inc_fuorviare>` (13°)
*	:ref:`Riflettere Incantesimo<inc_riflettere_incantesimo>` (15°)
*	:ref:`Momento di Prescienza<inc_momento_di_prescienza>` (17°)
*	:ref:`Previsione<inc_previsione>` (19°)

Talenti Bonus
**************
* :ref:`Abilità Focalizzata<tal_abilità_focalizzata>` (:ref:`Conoscenze<conoscenze>` [storia]])
* :ref:`Arma Focalizzata<tal_arma_focalizzata>`
* :ref:`Autorità<tal_autorità>`
* :ref:`Colpo Arcano<tal_colpo_arcano>`
* :ref:`Duro a Morire<tal_duro_a_morire>`
* :ref:`Incantesimi Massimizzati<tal_incantesimi_massimizzati>`
* :ref:`Resistenza Fisica<tal_resistenza_fisica>`
* :ref:`Riflessi Fulminei<tal_riflessi_fulminei>`

Magia della Stirpe
***********************
Ogni volta che si lancia un incantesimo con raggio d’azione “se stessi” si ottiene un bonus di fortuna pari al livello dell’incantesimo su tutti i TS per 1 round.

Poteri della Stirpe
*********************
Si è destinati a grandi cose ed i poteri guadagnati sono utilizzati come difesa.

Tocco del Destino (Mag)
***********************
:Tipo: :ref:`Azione Standard<azione_standard>`

Si può toccare una creatura concedendole un bonus cognitivo di 0.5 * Liv Stregone (minimo 1) ai tiri per colpire, alle prove di abilità, alle prove di caratteristica e ai TS per 1 round. 

Si può utilizzare 3 + Mod Car volte al giorno.

.. _destinato:

****************************
Destinato (Sop)
****************************
:Liv Min: 3

Si ottiene bonus di fortuna +1 a tutti i TS e alla CA durante il :ref:`Round di Sorpresa<round_di_sorpresa>` e ogni volta che si è ignari di un attacco.

Questo bonus aumenta di +1 ogni quattro livelli dopo il terzo fino a un massimo di +5 al 19°.

***************************** 
Predestinato (Sop)
*****************************
:Liv Min: 9

Una volta al giorno si può ritirare qualsiasi tiro per colpire, tiro per confermare il critico, o prove di livello per superare la resistenza agli incantesimi, ma prima che il risultato venga rivelato dal GM e tenendo il secondo risultato, anche se peggiore. 

Al 17° livello si ottiene un utilizzo giornaliero extra.

***************************
Profetizzato (Sop)
***************************
:Liv Min: 15

Il destino sta per compiersi: una volta al giorno, quando un attacco o un incantesimo che
infliggono danno provocherebbero la propria morte, si può tentare un TS sulla Volontà con CD 20, a cui si può applicare anche :ref:`Destinato<destinato>`. 

Se riesce, si è ridotti a –1 punti ferita e ci si stabilizza automaticamente.

***********************
Rivelato (Sop)
***********************
:Liv Min: 20

Il destino si compie: tutte le minacce di critico effettuate contro lo Stregone sono confermate soltanto se si ottiene un 20 naturale con il secondo tiro di dado. 

Tutte le minacce di critico che vengono messe a segno dallo stregone con un incantesimo sono confermate automaticamente. 

Una volta al giorno, si può superare automaticamente una prova di LI effettuata per superare la resistenza agli incantesimi, ma occorre utilizzare questa capacità prima di effettuare il tiro.

.. _stregone_fatata:

Fatata
--------------------
La natura capricciosa del popolo fatato dei folletti scorre nella famiglia dello Stregone a seguito di un qualche miscuglio del sangue fatato o della magia. 

Si ha un carattere più impressionabile degli altri, e si è inclini a scoppi di gioia e di collera.

Abilità di classe
*****************
:ref:`Conoscenze<conoscenze>` (natura)

Incantesimi Bonus
******************
*	:ref:`Intralciare<inc_intralciare>` (3°)
*	:ref:`Risata Spaventosa<inc_risata_spaventosa>` (5°)
*	:ref:`Sonno Profondo<inc_sonno_profondo>` (7°)
*	:ref:`Veleno<inc_veleno>` (9°)
*	:ref:`Traslazione Arborea<inc_traslazione_arborea>` (11°)
*	:ref:`Fuorviare<inc_fuorviare>` (13°)
*	:ref:`Porta in Fase<inc_porta_in_fase>` (15°)
*	:ref:`Danza Irresistibile<inc_danza_irresistibile>` (17°)
*	:ref:`Trasformazione<inc_trasformazione>` (19°)

Talenti Bonus
**************
* :ref:`Abilità Focalizzata<tal_abilità_focalizzata>` (:ref:`Conoscenze<conoscenze>` [natura]])
* :ref:`Incantesimi Rapidi<tal_incantesimi_rapidi>`
* :ref:`Iniziativa Migliorata<tal_iniziativa_migliorata>`
* :ref:`Mobilità<tal_mobilità>`
* :ref:`Riflessi Fulminei<tal_riflessi_fulminei>`
* :ref:`Schivare<tal_schivare>`
* :ref:`Tiro Preciso<tal_tiro_preciso>`
* :ref:`Tiro Ravvicinato<tal_tiro_ravvicinato>`

Magia della Stirpe
***********************
Ogni volta che si lancia un incantesimo della sottoscuola di compulsione, si aumenta la CD dell’incantesimo di +2.

Poteri della Stirpe
*********************
Esiste un profondo legame con la natura, e mentre il proprio potere aumenta, lo fa anche l’influenza del popolo fatato sulla propria magia.

Tocco d’Ilarità (Mag)
***********************
:Tipo: :ref:`Azione Standard<azione_standard>`

Si può far scoppiare una creatura in una risata per 1 round con un :ref:`Attacco di Contatto
in Mischia<azione_attacco_di_contatto_in_mischia>`, permettendole di effettuare soltanto un’:ref:`Azione di Movimento<azione_movimento>`, ma difendersi normalmente.

Una volta che una creatura è stata influenzata da *Tocco d’Ilarità*, è immune ai suoi effetti per 24 ore. Gli utilizzi giornalieri sono 3 + Mod Car.


****************************
Andatura nel Bosco (Str)
****************************
:Liv Min: 3

Ci si può muovere attraverso qualsiasi tipo di sottobosco (come rovi e sterpi naturali, zone
infestate e simili terreni) a velocità normale e senza subire danni o altri impedimenti, a meno che questi non siano stati incantati o manipolati magicamente per impedire il movimento.

***************************** 
Fuggevolezza (Mag)
*****************************
:Liv Min: 9

Si può diventare invisibili per Liv Stregone round al giorno, non necessariamente consecutivi.

Questa capacità funziona come :ref:`Invisibilità Superiore<inc_invisibilità_superiore>`.

***************************
Magia Fatata (Sop)
***************************
:Liv Min: 15

Si può ritirare a volontà qualsiasi prova di LI effettuata per superare la resistenza agli incantesimi, decidendolo prima che i risultati siano rivelati dal GM e tenendo il secondo risultato, anche se peggiore.

***********************
Anima Fatata (Sop)
***********************
:Liv Min: 20

La propria anima diventa un tutt’uno con il mondo fatato: si ottiene immunità ai veleni e RD 10/ferro freddo. 

Le creature del tipo animale non attaccano lo stregone a meno che non siano costrette ad agire
in tal modo con la magia e, una volta al giorno, si può lanciare :ref:`Camminare nelle Ombre<inc_camminare_nelle_ombre>` come capacità magica usando il proprio Liv Stregone come LI.

.. _stregone_infernale:

Infernale
--------------------
In qualche momento nella storia della famiglia dello stregone, un parente ha concluso un patto con un diavolo e quel patto ha influenzato tutta la linea di discendenza. 

Si manifesta nello Stregone in modi diretti ed evidenti, accordando poteri e capacità. Anche se il destino è ancora nelle sue mani, non può far nulla se non sperare che la sua ultima ricompensa non sia quella di finire all’Inferno.

Abilità di classe
*****************
:ref:`Diplomazia<diplomazia>`

Incantesimi Bonus
******************
*	:ref:`Protezione dal Bene<inc_protezione_dal_bene>` (3°)
*	:ref:`Raggio Rovente<inc_raggio_rovente>` (5°)
*	:ref:`Suggestione<inc_suggestione>` (7°)
*	:ref:`Charme sui Mostri<inc_charme_sui_mostri>` (9°)
*	:ref:`Dominare Persone<inc_dominare_persone>` (11°)
*	:ref:`Legame Planare<inc_legame_planare>` (solo diavoli e creature con l'archetipo immondo) (13°) 
*	:ref:`Teletrasporto Superiore<inc_teletrasporto_superiore>` (15°)
*	:ref:`Parola del Potere: Stordire<inc_parola_del_potere_stordire>` (17°)
*	:ref:`Sciame di Meteore<inc_sciame_di_meteore>` (19°)

Talenti Bonus
**************
* :ref:`Abilità Focalizzata<tal_abilità_focalizzata>` (:ref:`Conoscenze<conoscenze>` [piani]])
* :ref:`Combattere alla Cieca<tal_combattere_alla_cieca>`
* :ref:`Disarmare Migliorato<tal_disarmare_migliorato>`
* :ref:`Incantesimi Estesi<tal_incantesimi_estesi>`
* :ref:`Incantesimi Inarrestabili<tal_incantesimi_inarrestabili>`
* :ref:`Ingannevole<tal_ingannevole>`
* :ref:`Maestria in Combattimento<tal_maestria_in_combattimento>`
* :ref:`Volontà di Ferro<tal_volontà_di_ferro>`

Magia della Stirpe
***********************
Ogni volta che si lancia un incantesimo della sottoscuola di charme, si aumenta la CD
dell’incantesimo di +2.

Poteri della Stirpe
*********************
Si può attingere ai poteri dell’Inferno anche se bisogna essere prudenti a causa della sua influenza di corruzione. Tale potere ha sempre un prezzo. 

Tocco di Corruzione (Mag)
*************************
:Tipo: :ref:`Azione Standard<azione_standard>`

Si può rendere :ref:`Scossa<scosso>` una creatura con un :ref:`Attacco di Contatto in
Mischia<azione_attacco_di_contatto_in_mischia>` che dura per 0.5 * Liv Stregone (minimo 1) round.

Le creature scosse da questa capacità irradiano un’aura di malvagità, come se fossero un esterno malvagio (vedi :ref:`Individuazione del Male<inc_individuazione_del_male>`). 

Si può utilizzare questo tocco 3 + Mod Car volte al giorno e i tocchi multipli non si cumulano, ma ripristinano la durata. 

****************************
Resistenze Infernali (Str)
****************************
:Liv Min: 3

Si ottiene resistenza al fuoco 5 e bonus +2 ai TS contro che aumentano a 10 e +4 al 9° livello.

***************************** 
Fuoco Infernale (Mag)
*****************************
:Liv Min: 9

Una volta al giorno si può invocare una colonna di fuoco infernale: questo esplosione di fuoco di 3 metri di raggio infligge 1d6 * Liv Stregone danni da fuoco e coloro che si trovano nell’area dell’esplosione
ricevono un TS su Riflessi per dimezzare il danno subito con **CD = 10 + 0.5* Liv Stregone + Mod Car**. Le creature buone che falliscono il tiro sono :ref:`Scosse<scosso>` per un numero di round pari al proprio Liv Stregone.

Al 17° e al 20° livello si ottiene un utilizzo giornaliero extra. Questo potere ha un raggio di
azione di 18 metri.

***************************
Ali di Tenebra (Sop)
***************************
:Liv Min: 15
:Tipo: :ref:`Azione Standard<azione_standard>`

Si possono far spuntare delle ali spaventose da pipistrello, che danno una velocità di volare di 18 metri e manovrabilità media. 

Si possono far sparire le ali con un’:ref:`Azione Gratuita<azione_gratuita>`.

*************************
Potere degli Inferi (Sop)
*************************
:Liv Min: 20

Il proprio corpo viene infuso di fosca potenza: si ottiene immunità a fuoco e veleni, resistenza 10 a acido e freddo e la capacità vedere perfettamente al buio (normale o magico)
in un raggio di 18 metri.

.. _stregone_non_morta:

Non Morta
--------------------
La corruzione della morte scorre nella famiglia dello stregone: forse uno dei suoi antenati è diventato un lich o un vampiro potente, o forse è nato morto prima di ritornare improvvisamente in vita. In ogni caso, le forze della morte si muovono in lui ed in ogni sua azione.

Abilità di classe
*****************
:ref:`Conoscenze<conoscenze>` (religioni)

Incantesimi Bonus
******************
*	:ref:`Tocco Gelido<inc_tocco_gelido>` (3°)
*	:ref:`Vita Falsata<inc_vita_falsata>` (5°)
*	:ref:`Tocco del Vampiro<inc_tocco_del_vampiro>` (7°)
*	:ref:`Animare Morti<inc_animare_morti>` (9°)
*	:ref:`Onde di Affaticamento<inc_onde_di_affaticamento>` (11°)
*	:ref:`Non Morto a Morto<inc_non_morto_a_morto>` (13°) 
*	:ref:`Dito della Morte<inc_dito_della_morte>` (15°)
*	:ref:`Orrido Avvizzimento<inc_orrido_avvizzimento>` (17°)
*	:ref:`Risucchio di Energia<inc_risucchio_di_energia>` (19°)

Talenti Bonus
**************
* :ref:`Abilità Focalizzata<tal_abilità_focalizzata>` (:ref:`Conoscenze<conoscenze>` [religioni])
* :ref:`Duro a Morire<tal_duro_a_morire>`
* :ref:`Incantare in Combattimento<tal_incantare_in_combattimento>`
* :ref:`Incantesimi Focalizzati<tal_incantesimi_focalizzati>`
* :ref:`Incantesimi Immobili<tal_incantesimi_immobili>`
* :ref:`Resistenza Fisica<tal_resistenza_fisica>`
* :ref:`Robustezza<tal_robustezza>`
* :ref:`Volontà di Ferro<tal_volontà_di_ferro>`

Magia della Stirpe
***********************
Si rendono alcuni non morti suscettibili ai propri incantesimi di influenza mentale: i non morti corporei che una volta erano umanoidi sono trattati come tali al fine di determinare quali incantesimi abbiano effetto su di loro.

Poteri della Stirpe
*********************
Si è in grado di invocare i ripugnanti poteri dell’aldilà. 

Purtroppo, più li si attira e più l’unione con la morte stessa si avvicina.

Tocco della Tomba (Mag)
*************************
:Tipo: :ref:`Azione Standard<azione_standard>`

Si può compiere un :ref:`Attacco di Contatto in
Mischia<azione_attacco_di_contatto_in_mischia>` che rende una creatura vivente :ref:`Scossa<scosso>` per 0.5 * Liv Stregone (minimo 1) round. 

Se si tocca una creatura già :ref:`Scossa<scosso>` essa diventa :ref:`Spaventata<spaventato>` per 1 round se ha meno Dadi Vita del proprio Liv Stregone. 

Si può utilizzare questa capacità 3 + Mod Car volte al giorno.

****************************
Dono della Morte (Sop)
****************************
:Liv Min: 3

Si ottiene resistenza al freddo 5 e RD 5/— contro il danno non letale che salgono a 10 e 10/- al 9° livello.

***************************** 
Presa della Morte (Mag)
*****************************
:Liv Min: 9

Una volta al giorno si può far scaturire dal terreno uno sciame di braccia scheletriche per squarciare e
lacerare i nemici per 1 round: le braccia scaturiscono dal terreno in un’esplosione di 6 metri di raggio e chiunque si trovi in questa zona subisce 1d6 * Liv Stregone danni taglienti.

Coloro che si trovano nell’area dell’esplosione ricevono un TS su Riflessi, con **CD = 10 + 0.5 * Liv Stregone + Mod Car**, per dimezzare il danno subìto e se lo falliscono sono incapaci di muoversi per 1 round. 

Al 17° e al 20° livello si ottiene un utilizzo extra di questa capacità. Questo
potere ha un raggio di azione di 18 metri.

***************************
Forma Incorporea (Mag)
***************************
:Liv Min: 15

Una volta al giorno si può diventare incorporei per Liv Stregone round: mentre si è in questa forma, si ottiene il sottotipo incorporeo e si subisce solo metà danno da fonti materiali magiche (non si
subisce danno da armi e oggetti non magici). 

.. Caution::
    Mentre si è incorporei i propri incantesimi infliggono solo metà danno alle creature corporee, ma gli incantesimi ed altri effetti che non infliggono danno funzionano normalmente.

*************************
Uno di Noi (Str)
*************************
:Liv Min: 20

Il proprio corpo comincia a decomporsi (si può decidere l’aspetto di questo decadimento
come si vuole) e si passa inosservati ai non morti: non si viene notati dai non morti non intelligenti a meno che non li si attacchi.

Si ottiene RD 5/— e immunità a freddo, danno non letale, paralisi ed sonno.
 
Si riceve bonus morale +4 ai TS effettuati contro incantesimi e capacità magiche dei
non morti.