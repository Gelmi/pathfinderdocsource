
.. _regole_aggiuntive:

=================================================
Regole Addizionali
=================================================

.. figure:: Immagini/RegoleAddizionali.png

Questo capitolo presenta le regole per alcuni aspetti delgioco, come l’allineamento, l’età dei personaggi e l’ingombro, e quelle relative all’esplorazione, compreso il movimento via terra, le fonti di luce e rompere gli oggetti.

.. contents:: Contenuti
	:local:
	:depth: 3

Allineamento
==============
.. contents:: In breve:
	:local:

Le proprie convinzioni morali e il comportamento personale sono rappresentati dall’allineamento: legale buono, neutrale buono, caotico buono, legale neutrale, neutrale, caotico neutrale, legale malvagio, neutrale malvagio e caotico malvagio.

L’allineamento è uno strumento per definire l’identità del personaggio, non qualcosa che lo debba limitare: ciascuno rappresenta un’ampia gamma di personalità o filosofie personali, in modo tale che due personaggi dello stesso allineamento siano comunque diversi l’uno dall’altro. 

.. Note::
	Inoltre, sono poche le persone perfettamente coerenti con se stesse.

Tutte le creature hanno un allineamento che determina persino l’efficacia di alcuni incantesimi e oggetti magici. Gli animali e le altre creature incapaci di azioni dettate dalla morale sono neutrali: anche le vipere più letali o le tigri mangiatrici di uomini sono da ritenersi neutrali, in quanto mancano della capacità di distinguere un comportamento giusto da uno sbagliato sul piano morale. I cani potranno anche essere obbedienti e i gatti spiriti liberi, ma mancano delle capacità per essere veramente legali o caotici.

------------------
Bene Contro Male
------------------
I personaggi e le creature buone lottano per proteggere la vita degli innocenti, quelli malvagi invece tendono a disprezzare o a distruggere le vite innocenti, sia per soddisfazione che per guadagno personale.

Il **bene** comprende l’altruismo, il rispetto per la vita e per la dignità di tutti gli esseri senzienti. I personaggi buoni si sacrificano per aiutare gli altri.

Il **male** comporta il ferire, l’opprimere e l’uccidere il prossimo. Per alcune creature malvagie è praticamente impossibile provare compassione per gli altri: uccidono senza pensarci due volte, se ciò si rivela conveniente. Altri invece perseguono il male attivamente, uccidendo per gusto personale o al servizio di qualche padrone o divinità malvagia.

Quelli che si pongono in una posizione di **neutralità** nei confronti del bene e del male hanno degli scrupoli nell’uccidere gli innocenti ma non si sentono in obbligo di fare sacrifici personali per proteggerli o aiutarli.

------------------
Legge Contro Caos
------------------
I personaggi legali dicono la verità, mantengono la parola data, rispettano l’autorità, onorano le tradizioni e si ergono a giudici di chi non mantiene i propri impegni. I personaggi caotici seguono solo la propria coscienza, rifiutano che venga loro imposto cosa fare, preferiscono le nuove idee alla tradizione e rispettano le proprie promesse in base all’umore del momento.

La **legge** implica l’onore, l’essere degni di fiducia, l’obbedienza all’autorità e l’affidabilità. La legalità però può anche voler dire chiusura mentale, attaccamento reazionario alla tradizione, tendenza a giudicare e mancanza di adattabilità. Quanti promuovono la legalità sostengono che solo un comportamento legale è in grado di creare una società in cui le persone possono fidarsi l’una dell’altra e prendere le decisioni giuste, sicuri in tutto e per tutto che gli altri agiranno come dovrebbero.

Il **caos** comporta libertà, adattabilità e flessibilità, ma può voler dire anche avventatezza, disprezzo per l’autorità, arbitrarietà nelle decisioni e irresponsabilità. Quelli che hanno consciamente un comportamento caotico sostengono che solo la libertà definitiva da ogni costrizione permette all’individuo di esprimersi pienamente e prediligono il potenziale che ogni individuo ha dentro di sé rispetto ai benefici che la società comporta.

Le persone **neutrali** fra la legge e il caos mantengono un certo rispetto per le autorità e non sentono né il bisogno di obbedire né quello di ribellarsi. Pur essendo fondamentalmente oneste, sono spesso tentate di mentire o di ingannare gli altri.

-------
Gradi
-------
.. figure:: Immagini/GradiAllineamento.png
	:width: 300
	:align: right

Spesso le regole fanno riferimento a *gradi* quando trattano dell’allineamento. In questi casi *gradi* si riferisce al numero di caselle di allineamento tra due allineamenti, come mostrato nel diagramma a lato. 

.. Note::
	Un *grado* in diagonale conta come due gradi. 

	Per esempio: un personaggio legale neutrale è ad un grado dall’allineamento legale buono e a tre gradi da caotico malvagio. 

L’allineamento di un :ref:`chierico` deve essere entro un grado da quello della sua divinità.

---------------------------
I Nove Allineamenti
---------------------------
Nove diversi allineamenti descrivono tutte le combinazioni di legge-caos, bene-male. 

Ogni descrizione illustra il personaggio tipico appartenente a quell’allineamento, ma non si dimentichi che i singoli individui possono variare rispetto alla norma, e che di giorno in giorno un personaggio può agire più o meno in accordo con il suo allineamento. 

È quindi meglio usare queste descrizioni come guida, non come un copione.

I primi sei allineamenti, da legale buono a caotico neutrale, sono gli allineamenti standard dei personaggi. I tre allineamenti malvagi appartengono invece ai mostri e ai nemici. Con il permesso del GM, un giocatore può assegnare un allineamento malvagio al suo personaggio, ma tali personaggi sono spesso fonti di distruzione e conflitto con i membri del gruppo buoni e neutrali. 

Il GM è incoraggiato a considerare attentamente come un PG malvagio potrebbe influire sulla campagna prima di ammetterlo.

Legale Buono
--------------
Un personaggio legale buono si comporta esattamente come ci si aspetta che agisca una persona di indole buona. 

Unisce un forte sentimento di opposizione nei confronti del male alla disciplina necessaria per combatterlo senza tregua: dice la verità, mantiene la parola data, aiuta i bisognosi e si scaglia contro ogni ingiustizia. 

Un personaggio legale buono odia vedere i colpevoli farla franca e legale buono è l’allineamento migliore per chi vuole mettere assieme onore e compassione.

Neutrale Buono
---------------
Un personaggio neutrale buono fa sempre del suo meglio, nei limiti delle sue possibilità. 

Sente l’impulso di aiutare gli altri, lavora volentieri con re e magistrati, ma non sente alcun legame verso di loro.

Neutrale buono è l’allineamento migliore per chi vuol fare del bene senza avere pregiudizi a favore o contro l’autorità.

Caotico Buono
-------------
Un personaggio caotico buono segue soltanto la propria coscienza, senza preoccuparsi di ciò che gli altri si aspettano da lui. 

Segue la sua strada, ma riesce comunque ad essere gentile e bendisposto. Crede nel bene e nella giustizia, ma non ha molto rispetto per leggi e regole. Odia quando qualcuno cerca di intimorire gli altri e di imporre loro cosa fare. Segue la sua morale che, anche se positiva, potrebbe non andare d’accordo con quella della società.

Caotico buono è l’allineamento migliore per chi vuole conciliare lo spirito libero con la bontà di cuore.

Legale Neutrale
----------------
Un personaggio legale neutrale agisce come gli suggeriscono le leggi, le tradizioni o il suo codice d’onore personale. 

L’ordine e l’organizzazione sono di importanza vitale per lui. Può credere nell’ordine per se stesso e vivere quindi secondo un codice preciso, oppure credere nell’ordine per tutti e quindi essere dalla parte di un governo forte e organizzato.

Legale neutrale è l’allineamento migliore per chi vuole essere affidabile e onorevole senza diventare un fanatico.

Neutrale
----------
Un personaggio neutrale fa sempre, ciò che gli sembra essere una buona idea. Quando si tratta del male e del bene, della legge e del caos non ha particolari tendenze dall’una o dall’altra parte. Nella maggior parte dei casi si tratta più di una mancanza di convinzioni o di inclinazioni morali che di una reale devozione alla neutralità. 

Un personaggio con questo allineamento considera il bene migliore del male (tutto sommato, preferisce avere compagni e regnanti buoni piuttosto che malvagi), ma non si sente obbligato a perseguire il bene in modo teorico e universale. D’altra parte alcuni personaggi perseguono la neutralità, dal punto di vista filosofico: vedono il bene, il male, la legge e il caos come degli estremi pericolosi e quindi sostengono la causa della via di mezzo neutrale ritenendola la strada migliore e più equilibrata a lungo andare
.
Neutrale è l’allineamento migliore per chi vuole agire spontaneamente, senza pregiudizi né costrizioni.

Caotico Neutrale
-----------------
Un personaggio caotico segue esclusivamente il suo arbitrio. 

È l’individualista definitivo. Mette la sua libertà sopra ogni altra cosa ma non lotta per difendere quella degli altri, respinge l’autorità, odia le costrizioni e si scaglia contro le tradizioni. Un personaggio caotico neutrale non tenta di distruggere l’ordine intenzionalmente, come parte di una campagna tesa all’anarchia. Per fare questo dovrebbe essere motivato dal bene (guidato da un desiderio di liberare gli altri) o dal male (spinto da un desiderio di far soffrire chi è diverso da sé). Si tenga presente che un personaggio caotico neutrale può essere imprevedibile, ma che le sue azioni non sono dettate dal caso. Difficilmente salterà giù da un ponte quando può invece attraversarlo.

Caotico neutrale è l’allineamento migliore per chi cerca la completa libertà sia dalle restrizioni della società che dallo zelo dei benefattori.

.. figure:: Immagini/AllineamentoMalvagio.png
	:width: 400
	:align: left

Legale Malvagio
-----------------
Un individuo legale malvagio prende ciò che vuole, seguendo un suo codice morale, ma senza alcun riguardo per chi ferisce nel farlo. È attaccato alla tradizione, alla lealtà e all’ordine ma non rispetta la libertà, la dignità o la vita. Gioca secondo le regole, ma senza alcuna pietà o compassione. Si trova a suo agio all’interno di una gerarchia e ambisce a dominare, ma è comunque disposto a servire. Giudica gli altri non per le loro azioni, ma secondo la razza, la religione, la terra di origine e la posizione sociale. È restio a infrangere la legge o le promesse fatte, una riluttanza causata in parte dalla sua natura e in parte dal fatto che considera quest’ordine necessario per proteggersi da quanti si oppongono a lui sul piano morale.

Alcuni individui legali malvagi hanno particolari principi morali, come non uccidere mai a sangue freddo (lasciando eventualmente che siano i propri sottoposti a farlo), o non fare del male ai bambini (se si può evitare). Credono che questo atteggiamento li ponga al di sopra dei criminali senza scrupoli. Alcuni legali malvagi sono votati al male con uno zelo paragonabile a quello con cui un crociato è votato al bene. Oltre ad essere disposti a far male al prossimo per i loro scopi, si compiacciono nel diffondere il male come fine a se stesso. Potrebbero anche considerare il male che causano come parte del loro dovere verso un padrone o una divinità malvagia. 

Legale malvagio potrebbe essere l’allineamento più pericoloso poiché rappresenta una malvagità metodica, intenzionale, ed organizzata.

Neutrale Malvagio
------------------
Un individuo neutrale malvagio è disposto a fare di tutto, finché gli è consentito di cavarsela: pensa solo a se stesso. 

Non versa lacrime per quelli che uccide, non importa se per guadagno, gusto personale o convenienza; non ha una particolare propensione per l’ordine, né si illude che seguire le leggi, le tradizioni e i codici morali lo rendano migliore o più nobile. D’altra parte, non è perennemente insoddisfatto di natura e non ha lo stesso amore per il conflitto che un caotico malvagio potrebbe avere.

Alcuni neutrali malvagi sono votati al male come ideale e lo praticano come fine a se stesso. Spesso questo genere di persone venera divinità malvagie o appartiene a società segrete. 

Neutrale malvagio potrebbe essere l’allineamento più pericoloso poiché rappresenta il male allo stato puro, senza onore e senza volubilità.

Caotico Malvagio
-----------------
Un personaggio caotico malvagio fa solo ciò che la sua ingordigia, il suo odio e il suo  desiderio di distruzione lo spingono a fare. 

È facile all’ira, immorale, ingiustificatamente violento e imprevedibile. Quando cerca di impossessarsi di ciò che può avere, si comporta in modo brutale e senza scrupoli. Se invece è votato alla diffusione del male e del caos, è anche peggio. Fortunatamente i suoi piani tendono a
essere assurdi, e ogni gruppo che formi o a cui decida di unirsi è assai poco organizzato. 

Solitamente, i caotici malvagi possono essere costretti a lavorare assieme solo con la forza, e chi li comanda si mantiene al potere solo finché riesce a contrastare i loro tentativi di sostituirlo o assassinarlo.

Caotico malvagio potrebbe essere l’allineamento più pericoloso poiché rappresenta la nemesi non solo della vita e della bellezza, ma anche dell’ordine dal quale la vita e la bellezza dipendono.

-------------------------
Cambiare Allineamento
-------------------------
L’allineamento è uno strumento, una convenienza che si può usare per ricapitolare l’atteggiamento generale di PNG, regioni, religioni, organizzazioni, mostri, o persino oggetti magici.

Determinate :ref:`classi` elencano delle ripercussioni per coloro che non aderiscono ad un allineamento specifico ed alcuni incantesimi e oggetti magici hanno effetti differenti sui bersagli secondo l’allineamento, ma oltre a ciò non è necessario preoccuparsi troppo se qualcuno si sta comportando diversamente dal suo allineamento. 

Alla fine, il Game Master è la persona che decide se qualcosa è conforme all’allineamento, in base alle descrizioni ricevute e sulla sua idea di interpretazione: il GM deve soltanto essere coerente su ciò che costituisce la differenza fra gli allineamenti come il caotico neutrale e il caotico malvagio. Non ci sono meccaniche con cui si possa misurare l’allineamento: a differenza dei punti ferita, dei gradi di abilità o della Classe Armatura, l’allineamento è controllato personalmente dal GM.

È meglio che i giocatori interpretino i loro personaggi come vogliono. Se un giocatore si sta comportando in un modo che, come GM, ritenete non adatto al suo allineamento, diteglielo in modo amichevole. Se un personaggio vuole cambiare il suo allineamento, fateglielo fare: nella maggior parte dei casi, questo dovrebbe comportare solo un cambiamento di personalità, o in alcuni casi, nessun cambiamento se il cambiamento di allineamento è solo una modifica formale ad un’interpretazione già avviata dal giocatore usando questo allineamento. 

In alcuni casi, cambiare l’allineamento può urtare le capacità di un personaggio: alcune :ref:`classi` hanno delle limitazioni in proposito. Un incantesimo :ref:`Espiazione<inc_espiazione>` può essere necessario per rimediare ai danni inflitti dal cambio di allineamento in seguito a cause involontarie o errori momentanei di personalità.

I giocatori che cambiano frequentemente l’allineamento del personaggio dovrebbero con ogni probabilità giocare personaggi caotici neutrali.

Statistiche Base
=================
.. contents:: In breve:
	:local:

Questa sezione offre dei consigli su come determinare l’età, l’altezza e il peso del personaggio. 

.. Note::
	:ref:`Razza<razze>` e :ref:`Classe<classi>` influenzano queste statistiche, ma il GM può sempre permettere la creazione di un personaggio che non si conforma a queste statistiche.

------
Età
------
È possibile scegliere l’età o generarla casualmente. 

.. figure:: Immagini/EtaIniziale.png
	:width: 300
	:align: right

	Età iniziale minima e casuale.

Se viene scelta, deve rientrare nel margine minimo di età per :ref:`Razza<razze>` e :ref:`Classe<classi>` del personaggio, riportate in tabella. In alternativa la tabella può essere utilizzata per determinare quanti anni ha il personaggio, tirando i dadi indicati e aggiungere il risultato
all’età minima di maturità per la razza scelta.

Man mano che il personaggio invecchia i punteggi delle sue caratteristiche fisiche diminuiscono e quelli delle sue caratteristiche mentali aumentano, come riportato nella :ref:`Tabella<tabella_invecchiamento>` a seguire. 

.. Important::
	Gli effetti del passare del tempo sono cumulativi, ma i punteggi delle caratteristiche di un personaggio non possono scendere sotto all’1 in questo modo. 

Quando un personaggio diventa venerabile, il GM deve tirare in segreto la sua età massima e il risultato non deve essere rivelato al giocatore: quando quel personaggio raggiunge l’età così stabilita, egli muore di vecchiaia nel corso di quell’anno. 

Le età massime riguardano i PG. Gran parte della gente nel resto del mondo muore per malattie, incidenti, infezioni o violenza prima di raggiungere la propria età venerabile.

.. _tabella_invecchiamento:

.. figure:: Immagini/Invecchiamento.png
	:width: 700

	Effetti dell'invecchiamento sui personaggi e loro fasi di età.

----------------
Altezza e Peso
----------------
.. figure:: Immagini/AltezzaPeso.png
	:width: 300
	:align: right

	Parametri per il calcolo di altezza e peso casuali.

Per determinare l’**altezza** del personaggio, si tira il modificatore indicato nella tabella a lato, lo si moltiplica per **2,5 cm**, e si aggiunge il risultato all’altezza base in base alla razza e al sesso del personaggio. 

Per quanto riguarda il peso del personaggio, si moltiplica il risultato del modificatore per il moltiplicatore del peso e si aggiunge il risultato al peso base a seconda della razza e del sesso del personaggio.

.. _capacità_di_trasporto:

------------------------
Capacità di Trasporto
------------------------
Le regole sull’ingombro determinano quanto l’equipaggiamento di un personaggio lo rallenti. 

L’ingombro si divide in due parti: :ref:`ingombro_armatura` e :ref:`ingombro_peso` totale.

.. _ingombro_armatura:

Ingombro dell’Armatura
------------------------
L’armatura di un personaggio definisce il suo bonus massimo di Destrezza alla CA, la sua penalità di armatura alla prova, la sua velocità e quanto si muove velocemente quando corre, secondo quanto riportato nella :ref:`tabella delle armature<tabella_armature>`. 

A meno che il personaggio non sia troppo debole o non stia trasportando molta attrezzatura, questo è tutto quello che si deve sapere. L’attrezzatura extra che trasporta, come armi e corda, non lo rallenterà più di quanto non faccia già la sua armatura. 

Tuttavia, se il personaggio è debole o sta trasportando molta attrezzatura, allora bisogna calcolare l’:ref:`ingombro_peso`. Farlo è molto importante quando il personaggio sta tentando
di trasportare qualche oggetto pesante.

.. _ingombro_peso:

Ingombro del Peso
-------------------

.. _tabella_capacità_trasporto:

.. figure:: Immagini/CapacitaTrasporto.png
	:width: 350
	:align: center

	Capacità di trasporto. 

	Queste valgono per personaggi bipedi di taglia Media, per gli altri consultare la :ref:`Sezione apposita<capacità_trasporto_altre>`.

Nel caso si intenda determinare se l’attrezzatura di un personaggio sia abbastanza pesante da rallentarlo più di quanto non faccia già la sua armatura, bisogna calcolare il peso totale di armatura, armi e attrezzatura, riportati nel capitolo :ref:`Equipaggiamento<equip>` e confrontare nella tabella a lato questo totale con la Forza del personaggio. 

In base a come il peso si confronta con la capacità di trasporto, il personaggio sta trasportando un carico leggero, medio o pesante.

.. _tabella_ingombro:

.. figure:: Immagini/Ingombro.png
	:width: 300
	:align: right

	Effetti dell'ingombro.

Come l’armatura, anche il carico fornisce al personaggio un bonus massimo di Destrezza alla CA, una penalità alla prova (che funziona come una penalità di armatura alla prova), una velocità e un fattore di corsa, riportate nella :ref:`Tabella dell'ingombro<tabella_ingombro>` a lato.

Un carico medio o pesante influisce sulle abilità del personaggio come quando indossa un’armatura media o pesante. Trasportare un peso leggero, invece, non lo ingombra affatto.

.. Caution::
	Se il personaggio indossa un’armatura, bisogna usare il valore peggiore (dell’armatura o del peso) per ogni categoria: non bisogna sommare le penalità.



Sollevare e Trascinare
-----------------------
Un personaggio può sollevare sopra la testa fino al carico massimo, che è il valore più alto indicato per la Forza del personaggio nella :ref:`Colonna carico pesante<tabella_capacità_trasporto>`.

Un personaggio può sollevare dal terreno fino al doppio del carico massimo, ma può solo barcollare con esso e mentre è sovraccaricato in questo modo perde qualsiasi bonus di Destrezza alla CA e può solo muoversi di 1,5 metri per round, come :ref:`Azione di Round Completo<azione_di_round_completo>`.

Un personaggio generalmente può spingere o trascinare sul terreno fino a cinque volte il carico massimo e circostanze favorevoli possono raddoppiare queste cifre, così come quelle sfavorevoli possono ridurle a una volta e mezza o meno.

.. _capacità_trasporto_altre:

Creature più Grandi e più Piccole
----------------------------------
I valori nella :ref:`Tabella delle Capacità<tabella_capacità_trasporto>` sono per creature bipedi di taglia Media. 

Creature bipedi **più grandi** possono trasportare più peso in base alla categoria di
taglia: Grande ×2, Enorme ×4, Mastodontica ×8 e Colossale ×16. 
Creature bipedi **più piccole** possono trasportare meno peso in base alla categoria di taglia: Piccola ×3/4, Minuscola ×1/2, Minuta ×1/4 e Piccolissima ×1/8.

Le creature **quadrupedi** possono trasportare pesi superiori; al posto dei precedenti modificatori, bisogna moltiplicare il valore corrispondente al punteggio di Forza in Tabella come
segue: Piccolissima ×1/4, Minuta ×1/2, Minuscola ×3/4, Piccola ×1, Media ×1,5, Grande ×3, Enorme ×6, Mastodontica ×12 e Colossale ×24.

Forza Spaventosa
-------------------
Per punteggi di Forza non elencati nella :ref:`Tabella delle Capacità<tabella_capacità_trasporto>` basta trovare il punteggio di Forza tra 20 e 29 che abbia la stessa unità decimale del punteggio di Forza della creatura e moltiplicare questo valore per 4 per ogni 10 punti in più della Forza della creatura rispetto al valore indicato nella riga.

----------------------------------------------
Armatura e Ingombro per altre Velocità di Base
----------------------------------------------

.. figure:: Immagini/VelocitaIngombro.png
	:width: 300
	:align: right

Questa tabella fornisce le velocità ridotte per tutte le velocità base da 1,5 m fino a 36 m (in incrementi di 1,5 m).

.. Note::
	In generale questo valore si ottiene dividendo la velocità per 1.5 e approssimando il risultato per eccesso al primo multiplo di 1.5. Per esempio: 1.5/1.5 = 1, che si approssima a 1.5, 10.5/1.5 = 7, che si approssima a 7.5, e 16.5/1.5 = 11, che si approssima a 12.

Movimento
=============
.. contents:: In breve:
	:local:

Ci sono tre scale di movimento nel gioco:

• **Tattico**: per il combattimento, misurato in metri (o quadretti di 1,5 m) per round.
• **Locale**, per esplorare una zona, misurato in metri al minuto.
• **Via Terra**, per muoversi da un posto all’altro, misurato in km all’ora o al giorno.

------------------
Tipi di Movimento
------------------

.. _tabella_movimento:

.. figure:: Immagini/Movimento.png
	:width: 300
	:align: right

	Movimento e Distanza.

Quando si muovono nelle differenti scale di movimento le creature generalmente camminano,
vanno veloci o corrono.

Camminare
	Camminare rappresenta un movimento non affrettato ma deciso di 4,5 km all’ora per un umano senza ingombro.
Andare Veloci
	Andare veloci è un’andatura che corrisponde a un movimento di 9 km all’ora per un umano senza ingombro. Un personaggio che si muove di due volte la sua velocità in un round, o che si muove della sua velocità mentre sta compiendo un’:ref:`Azione Standard<azione_standard>` o un’altra :ref:`Azione di Movimento<azione_movimento>`, sta andando veloce.
Correre (×3)
	Muoversi al triplo della propria velocità standard è un passo di corsa per un personaggio in armatura pesante. Significa muoversi di 13,5 km all’ora per un umano in armatura completa.
Correre (×4)
	Muoversi al quadruplo della propria velocità standard è un passo di corsa per un personaggio in armatura leggera, media o senza armatura. Significa muoversi di 18 km all’ora per un umano senza ingombro, o a 12 km all’ora per un umano con cotta di maglia. 

.. Important::
	La :ref:`Tabella del Movimento<tabella_movimento>` riassume le velocità in ciascuna condizione.

-----------------
Movimento Tattico
-----------------
Durante un combattimento si utilizza la velocità tattica. I personaggi generalmente non camminano durante il combattimento: si muovono veloci o corrono. 

Un personaggio che si muove alla sua velocità e compie qualche azione si sta muovendo veloce per circa metà del round e compiendo qualcos’altro per l’altra metà.

Movimento Ostacolato
----------------------

.. _tabella_movimento_ostacolato:

.. figure:: Immagini/MovimentoOstacolato.png
	:width: 300
	:align: right

	Costi in caso di movimento ostacolato.

Terreno difficile, ostacoli o scarsa visibilità possono impedire i movimenti, come riportato nella :ref:`Tabella<tabella_movimento_ostacolato>`. 

Quando il movimento è ostacolato ogni quadretto in cui si entra vale come due, quindi si riduce l’effettiva distanza che si può coprire con un’:ref:`Azione di Movimento<azione_movimento>`.

.. Caution::
	Se esiste più di una condizione particolare, tutti i costi aggiuntivi applicabili vanno moltiplicati tra loro. 

	Questa è un’eccezione alle normali regole per raddoppiare.

In alcune situazioni il movimento è talmente ostacolato che la velocità è ridotta a meno di 1,5 metri e in tal caso si può utilizzare un’:ref:`Azione di Round Completo<azione_di_round_completo>` per muoversi di 1,5m in qualsiasi direzione, anche diagonale. 

Questo movimento assomiglia a un passo di 1,5 metri, ma non lo è, quindi provoca i normali :ref:`Attacchi di Opportunità<azione_attacco_di_opportunità>`. 

.. Caution::
	Questa regola non si può applicare attraversare terreni impraticabili o per muoversi quando non è possibile farlo in alcun modo.

-----------------
Movimento Locale
-----------------
I personaggi che esplorano una zona usano il movimento locale, misurato in metri al minuto.

Camminare
	Un personaggio può camminare senza problemi in scala locale. 

Andare Veloci
	Si può andare veloce senza problemi in scala locale. Vedi :ref:`Movimento_via_terra` per il movimento in km all’ora.
Correre
	Un personaggio può correre per un numero di round pari al proprio punteggio di Costituzione su scala locale senza bisogno di riposarsi. Oltre questo limite la corsa diventa stancante e :ref:`continuare a correre<azione_correre>` diventa impegnativo.

.. _movimento_via_terra:

-------------------
Movimento Via Terra
-------------------
I personaggi che percorrono lunghe distanze usano il movimento via terra, che si misura in ore o
giorni. 

Un giorno in generale rappresenta 8 ore di tempo di viaggio reale, per imbarcazioni a remi, significa remare per 10 ore, per navi a vela, rappresenta 24 ore.

Camminare
	Si può camminare per 8 ore in un giorno di viaggio senza problemi, ma camminare più a lungo può sfinire, diventando :ref:`Marcia_Forzata`.
Andare Veloci
	Si può andare veloci per 1 ora senza problemi. 

	Andare veloci per una seconda ora compresa tra due cicli di sonno provoca 1 danno non letale, e ogni ora aggiuntiva provoca il doppio dei danni subiti nell’ora precedente. Un personaggio che subisce danni non letali da andatura veloce è considerato :ref:`Affaticato<affaticato>`: non può correre o caricare e subisce penalità –2 a Forza e Destrezza. 

	Il recupero dei punti ferita non letali permette anche il recupero dalla fatica.
Correre
	Non è possibile correre per un lungo periodo di tempo. Tentativi di correre e riposarsi a cicli funzionano come andare veloci.

Terreno
--------

.. _tabella_movimento_via_terra:

.. figure:: Immagini/MovimentoViaTerra.png
	:width: 300
	:align: right

	Terreni e corrispondenti movimenti via terra.

Il terreno su cui si viaggia influenza quale distanza viene percorsa in un’ora o in un giorno, come indicato in :ref:`Tabella<tabella_movimento_via_terra>`.

Una **strada maestra** è una strada principale, dritta e lastricata. 

Una **strada comune** è solitamente un cammino impervio. 

Un **sentiero** è come una strada comune tranne per il fatto che permette di viaggiare solo in fila indiana e non avvantaggia un gruppo che viaggia con veicoli. 

Un **terreno libero** è una zona selvaggia senza sentieri segnati.

.. _marcia_forzata:

Marcia Forzata
--------------
In un giorno di cammino normale, si può camminare per 8 ore e il resto del giorno viene sfruttato per fare e disfare il campo, riposarsi e mangiare.

È possibile camminare per più di 8 ore in un giorno compiendo una *Marcia Forzata*: per ogni ora di marcia extra, è necessario effettuare una prova di Costituzione con **CD = 10 + 2 per ogni ora**. Se la prova fallisce, si subiscono 1d6 danni non letali e si è considerati :ref:`Affaticati<affaticato>`. Il recupero dei punti ferita non letali permette anche il recupero dalla fatica. 

È possibile per un personaggio marciare fino a perdere conoscenza se ci si spinge troppo oltre ai propri limiti.

Movimento in Sella
------------------

.. _tabella_cavalcature:

.. figure:: Immagini/CavalcatureVeicoli.png
	:width: 300
	:align: right

	Cavalcature e veicoli.

Una cavalcatura che porta un cavaliere può muoversi con andatura veloce, tuttavia i danni che subisce sono danni normali invece che non letali. 

Può anche essere costretta a una :ref:`Marcia_Forzata`, ma le sue prove di Costituzione falliscono automaticamente e di nuovo i danni che subisce sono danni normali e anche in questo caso si considera :ref:`Affaticata<affaticato>`.

La :ref:`Tabella<tabella_cavalcature>` riporta le velocità in sella e velocità di mezzi
trainati da animali da tiro.

Movimento Marittimo
--------------------
Nella :ref:`Tabella<tabella_cavalcature>` si possono trovare le velocità delle imbarcazioni.

------------------------
Fuga e Inseguimento
------------------------
Nel movimento round per round, dove si contano semplicemente le caselle coperte, è impossibile per un personaggio lento sfuggire ad un personaggio veloce senza qualche tipo di aiuto. Allo stesso modo, non è un problema per un personaggio veloce sfuggire ad uno più lento.

Quando la velocità dei due personaggi coinvolti è uguale, c’è un metodo abbastanza semplice per risolvere un inseguimento: se una creatura sta inseguendo un’altra ed entrambe si muovono
alla stessa velocità, e l’inseguimento prosegue almeno per alcuni round, occorre effettuare prove contrapposte di Destrezza per vedere chi si muove più in fretta in questi round. 

Se la creatura inseguita vince, riesce a fuggire. Se è l’inseguitore a vincere, cattura la creatura in fuga.

A volte un inseguimento si svolge via terra e potrebbe durare per una giornata intera, con entrambe le parti che riescono solo a scorgersi a distanza. Nel caso di un lungo inseguimento, una prova contrapposta di Costituzione determina quale delle due parti può mantenere più a lungo il ritmo. Se la creatura inseguita ottiene il risultato più alto riesce a fuggire, altrimenti l’inseguitore riesce a raggiungere la sua preda, sopraffacendola grazie alla propria resistenza.

Esplorazione
=====================

.. figure:: Immagini/RegoleAddizionali2.png

Poche regole sono d’importanza vitale per il successo degli avventurieri come quelle che riguardano la visibilità, la luce e come rompere gli oggetti. 

Le regole per ciascun argomento vengono spiegate sotto.

.. contents:: In breve:
	:local:

.. _visibilità_luce:

-------------------
Visibilità e Luce
-------------------

.. _tabella_fonti_luce:

.. figure:: Immagini/Illuminazione.png
	:width: 300
	:align: right

	Fonti di luce e Illuminazione.

Nani e mezzorchi hanno la :ref:`Scurovisione<scurovisione>`, ma tutte le altre :ref:`razze_base` hanno bisogno di luce per vedere.

In :ref:`Tabella<tabella_fonti_luce>` sono riportati il raggio dell’area che una sorgente di luce illumina e per quanto tempo dura. La voce aumentata indica un’area esterna al raggio di luce in cui il livello di luminosità viene incrementato di un grado; da buio a luce fioca, per esempio).

In un’area di **luce intensa** tutti i personaggi vedono distintamente, ma alcune creature, come quelle con sensibilità alla luce e cecità alla luce, subiscono penalità mentre si trovano in un’area di luce intensa. 

Una creatura non può usare :ref:`Furtività<furtività>` in un’area di luce intensa a meno che non sia invisibile o dietro una :ref:`Copertura<copertura>`. 

Le aree di luce intensa comprendono la luce diretta del sole e l’interno dell’area dell’incantesimo :ref:`Luce Diurna<inc_luce_diurna>`.

La **luce normale** funziona come la luce intensa, ma i personaggi con sensibilità o cecità alla luce non subiscono penalità. 

Le aree di luce normale comprendono l’essere all’interno di un bosco durante il giorno, entro 6 metri da una torcia e nell’area dell’incantesimo :ref:`Luce<inc_luce>`.

In un’area di **luce fioca** tutti i personaggi vedono come in penombra e le creature nell’area godono di :ref:`Occultamento<occultamento>` (20% di essere mancate in combattimento) nei confronti di avversari senza :ref:`Scurovisione<scurovisione>` o la capacità di vedere al buio. 

Una creatura in un’area con illuminazione fioca può effettuare una prova di :ref:`Furtività<furtività>` per nascondersi. 

Le aree di luce fioca comprendono trovarsi all’esterno sotto la luce della luna, delle stelle, e tra i 6 e i 12 m da una torcia.

In un’area di **buio** le creature senza :ref:`Scurovisione<scurovisione>` sono considerate :ref:`Accecate<accecato>`. In aggiunta ai normali effetti del buio, una creatura `Accecata<accecato>` ha una probabilità del 50% di mancare il bersaglio in combattimento (gli avversari godono di :ref:`Occultamento Totale<occultamento_totale>`), perde il bonus di Destrezza alla CA, subisce penalità –2 alla CA, si muove a velocità dimezzata e ha penalità –4 a tutte le prove di :ref:`Percezione<percezione>` che si basano sulla vista e alla maggior parte delle abilità basate su Forza e Destrezza.

Le aree di buio comprendono trovarsi in una stanza di un sotterraneo non illuminato, in caverne, e all’esterno sotto un cielo nuvoloso e senza luna.

I personaggi con :ref:`Visione Crepuscolare<visione_crepuscolare>` (:ref:`Elfi`, :ref:`gnomi` e
:ref:`mezzelfi`) possono vedere oggetti due volte più lontano del raggio indicato, bisogna quindi raddoppiare il raggio effettivo di luce intensa, normale e fioca per questi personaggi.

I personaggi con :ref:`Scurovisione<scurovisione>` (:ref:`mezzorchi` e :ref:`nani`) possono
vedere normalmente zone illuminate così come zone buie entro un raggio di 18 metri. Una creatura entro un raggio di 18 metri da un personaggio con :ref:`Scurovisione<scurovisione>` non si può nascondere a meno che non sia invisibile o dietro una :ref:`Copertura<copertura>`.

---------------------
Sfondare ed Entrare
---------------------
Quando si tenta di spaccare un oggetto le scelte sono due: colpirlo con un’arma o romperlo con la forza bruta.

Colpire un Oggetto
--------------------
Il tentativo di colpire un’arma o uno scudo con un’arma tagliente o contundente è accompagnato dalla manovra speciale di combattimento :ref:`Spezzare<azione_spezzare>`. 

Colpire un oggetto è simile a :ref:`Spezzare<azione_spezzare>` un’arma o uno scudo, eccetto che la propria prova di manovra di combattimento è contrapposta dalla CA dell’oggetto. Generalmente è possibile colpire e rompere un oggetto solo con un’arma tagliente o contundente.

.. _tabella_taglia_oggetti:
	
.. figure:: Immagini/TagliaOggetti.png
	:width: 300
	:align: right

	Taglia e Ca degli Oggetti.

Classe Armatura
	Gli oggetti sono più facili da colpire delle creature poiché di solito non si muovono, ma molti sono abbastanza resistenti da scrollarsi di dosso qualche danno ad ogni colpo. 

	La Classe Armatura di un oggetto è pari a 10 + il suo Mod Taglia (riportato in :ref:`Tabella<tabella_taglia_oggetti>`) + il suo Mod Des. Un oggetto inanimato non solo ha Destrezza 0 (penalità –5 alla CA) ma subisce anche penalità aggiuntiva –2 alla CA.

	Se si compie un’:ref:`Azione di Round Completo<azione_di_round_completo>` per prendere la mira si colpisce automaticamente con un’arma da mischia e si ottiene bonus +5 con un’arma a distanza.

.. _tabella_punti_ferita_sostanze:

.. figure:: Immagini/PuntiFeritaSostanze.png
	:width: 300
	:align: left

	Durezza e Punti Ferita di Sostanze.

.. _durezza:

Durezza
	Ogni oggetto ha una durezza che rappresenta quanto riesce a resistere al danno: ogni volta che un oggetto subisce dei danni, bisogna sottrarre la sua durezza dai danni e solo i danni che eccedono la sua durezza vengono sottratti ai suoi punti ferita. Consultare le tabelle per :ref:`Equipaggiamento<tabella_punti_ferita_equipaggiamento>`, :ref:`Sostanze<tabella_punti_ferita_sostanze>` e :ref:`Oggetti<tabella_punti_ferita_oggetti>`.

Punti ferita
	Il totale dei punti ferita di un oggetto dipende dal materiale di cui è fatto e dalle sue dimensioni. Gli oggetti che subiscono un danno pari o superiore alla metà dei loro punti ferita totali ottengono la condizione :ref:`Rotto<rotto>`.

	Quando i punti ferita arrivano a 0 un oggetto è :ref:`Distrutto<distrutto>`. 

	.. Note::
		Gli oggetti molto grandi hanno punti ferita totali separati per sezioni diverse.

.. _tabella_punti_ferita_equipaggiamento:

.. figure:: Immagini/PuntiFeritaEquipaggiamento.png
	:width: 300
	:align: right 

	Durezza e Punti Ferita di Armature, Armi e Scudi Comuni.

***************************
Attacchi di Energia
***************************
Gli attacchi di energia infliggono metà danno alla maggior parte degli oggetti, che vanno quindi divisi per 2 prima di applicare la durezza. 

Alcuni tipi di energia possono essere particolarmente efficaci contro certi oggetti, a discrezione del GM. Per esempio, il fuoco potrebbe infliggere danno pieno a pergamene, stoffa e altri oggetti che bruciano facilmente. Un attacco sonoro potrebbe causare danno pieno ad oggetti di vetro e cristallo.

***************************
Danni da Arma a Distanza
***************************
Gli oggetti subiscono la metà dei danni da arma a distanza (tranne che per le :ref:`Macchine da Assedio<macchine_da_assedio>` e simili), da applicare prima della durezza dell’oggetto.

*****************
Armi Inefficaci
*****************
Certe armi semplicemente non possono infliggere danni a certi oggetti. Per esempio, un’arma contundente non è in grado di tagliare una corda. Allo stesso modo è decisamente difficile abbattere una porta o un muro di pietra con la maggior parte delle armi da mischia, a meno che non siano specificamente ideate per farlo, come picconi e martelli.

.. _tabella_punti_ferita_oggetti:

.. figure:: Immagini/PuntiFeritaOggetti.png
	:width: 300
	:align: right

	Durezza e Punti Ferita degli Oggetti.

********
Immunità
********
Gli oggetti inanimati sono immuni ai danni non letali e ai colpi critici. Anche gli oggetti animati, altrimenti considerati come delle creature, hanno queste immunità.

******************************
Armi, Armature e Scudi Magici
******************************
Ogni +1 di bonus di potenziamento aggiunge anche 2 alla durezza ad armi, armature e scudi, e +10 ai punti ferita all’oggetto.

*******************************
Vulnerabilità a Certi Attacchi
*******************************
Certi attacchi possono essere particolarmente efficaci contro alcuni oggetti. 

In questi casi gli attacchi infliggono danni raddoppiati e possono ignorare la durezza dell’oggetto.

********************
Oggetti danneggiati
********************
Un oggetto danneggiato rimane pienamente funzionale con la condizione :ref:`Rotto<rotto>` fino a quando i punti ferita non arrivano a 0, e a quel punto è considerato :ref:`Distrutto<distrutto>`. 

Gli oggetti danneggiati (ma non quelli distrutti) possono essere riparati con l’abilità :ref:`Artigianato<artigianato>` e tramite alcuni incantesimi.

*************
Tiri Salvezza
*************
Oggetti non magici **incustoditi** non effettuano mai TS: si considera che abbiano fallito i loro TS e quindi siano sempre soggetti ad incantesimi ed altri attacchi che ammettono un TS per resistere o negare. 

Un oggetto **custodito** da un personaggio (che lo tiene in mano, lo tocca o lo indossa) ottiene un TS proprio come se lo stesse effettuando il personaggio stesso, cioè usando il bonus al TS del personaggio.

Gli oggetti **magici** hanno sempre TS. I bonus ai tiri salvezza su Tempra, Riflessi e Volontà di un oggetto magico sono pari a 2 + 0.5 * Liv Incantatore. 

Gli oggetti **magici custoditi** effettuano i TS come il loro possessore oppure usano i loro TS, qualunque sia il migliore.

***************
Oggetti Animati
***************
Gli oggetti animati contano come creature per determinarne la CA, quindi non devono essere considerati oggetti inanimati.

Rompere Oggetti
-----------------
Quando si tenta di rompere qualcosa con forza improvvisa piuttosto che infliggendo danni regolari, bisogna effettuare una prova di Forza, invece di un tiro per colpire e per il
danno come per :ref:`Spezzare<azione_spezzare>`, per vedere se ci si riesce. 

.. _tabella_rompere_oggetti:

.. figure:: Immagini/ForzareOggetti.png
	:width: 300
	:align: right

	CD per Rompere o Forzare gli Oggetti.

Poiché la durezza non influisce sulla CD per rompere l’oggetto, questo valore dipende più dal modo in cui è costruito l’oggetto che non dal materiale. In :ref:`Tabella<tabella_rompere_oggetti>` è riportata una lista delle CD più comuni relative al rompere gli oggetti.

Se un oggetto ha perso metà o più dei suoi punti ferita, ottiene la condizione :ref:`Rotto<rotto>` e la CD per romperlo scende di 2.

Creature di taglia superiore o inferiore a quella Media hanno bonus o penalità sulla prova di Forza per sfondare una porta, come segue: Piccolissima –16, Minuta –12, Minuscola –8, Piccola –4, Grande +4, Enorme +8, Mastodontica +12, Colossale +16.

.. Note::
	Un piede di porco o un ariete portatile aumentano la probabilità del personaggio di sfondare una porta.


