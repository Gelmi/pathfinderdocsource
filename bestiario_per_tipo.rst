
.. _bestiario_per_tipo:

=================================================
Mostri per Tipo
=================================================

Qui vengono raccolti tutti i mostri, organizzati per tipo. I dati sui mostri sono raccolti da diverse sorgenti (Bestiari vari, e altro).

Questa fa anche da indicizzazione principale dei mostri, perchè ritenuta la più efficace per la semplice consultazione secondo mio insindacabile giudizio!

.. contents:: Contenuti
    :depth: 3

Aberrazioni
============

.. contents:: Contenuti
    :local:

.. _bestiario_progenie_del_peccato:

--------------------
Progenie del Peccato
--------------------
.. figure:: Immagini/Bestiario/ProgenieDelPeccato.png
    :align: left
    :width: 400

Questo glabro umanoide barcolla su delle zampe canine curvate all'indietro, la spaventosa bocca contornata da piccole braccia con mani a tre dita.

.. rubric:: Base

:GS: 2
:Tipo: Aberrazione
:Terreno: Rovine/Dungeon
:Clima: Temperato
:Punti Esperienza: 600
:Allineamento: NM
:Taglia: Media
:Iniziativa: +5
:Sensi:
    *   :ref:`Percepire Peccato<progenie_percepire_peccato>`
    *	:ref:`Scurovisione<scurovisione>` 18m
    * 	:ref:`percezione` +7

.. rubric:: Difesa

:CA: +1 Des, +1 Schivare, +2 Naturale

    *	Base 14
    *	Contatto 12
    *	Impreparato 12

:pf: 19 (3d8+6)
:Tempra: +3
:Riflessi: +2
:Volontà: +4

.. rubric:: Attacco

:Velocità: 12m
:Mischia:
    |   Corsesca +3 (2d4+1/x3), Morso -2 (1d6 con :ref:`Morso Peccaminoso<progenie_morso_peccaminoso>`) o
    |   2 artigli +3 (1d4+1), Morso +3 (1d6 con :ref:`Morso Peccaminoso<progenie_morso_peccaminoso>`)

.. rubric:: Statistiche

:For: 13
:Des: 13
:Cos: 14
:Int: 14
:Sag: 13
:Car: 12
:Att Base: +2
:BMC: +3
:DMC: 15
:Talenti: :ref:`tal_iniziativa_migliorata`, :ref:`tal_schivare`
:Abilità:
    :ref:`furtività` +7, :ref:`intimidire` +7, :ref:`percezione` +7, :ref:`sopravvivenza` +7
:Linguaggi: Aklo

.. rubric:: Ecologia

:Ambiente: Qualsiasi Rovina
:Organizzazione: Solitario, Coppia o Culto (3-8)
:Tesori: Standard (:ref:`equip_corsesca`, altro tesoro)

.. rubric:: Capacità Speciali

:Competenza Marziale (Str):
    La progenie del peccato è competente in tutte le armi semplici e da guerra, nelle armature e negli scudi (eccetto gli scudi torre).

.. _progenie_morso_peccaminoso:

:Morso Peccaminoso (Sop):
    Una creatura morsa da una progenie del peccato viene sopraffatta da pensieri peccaminosi (Volontà CD 12 nega).

    Queste emozioni sono così intense che il bersaglio diventa :ref:`Infermo<infermo>` per 1d6 minuti e in caso venga morso una seconda volta, :ref:`Barcollante<barcollante>` per 1 round, a meno del Tiro Salvezza.
    :ref:`inc_calmare_emozioni`, :ref:`inc_rimuovi_maledizione` o :ref:`Spezzare Incantamento<inc_spezzare_incantamento>` negano l’effetto del morso peccaminoso. La CD del Tiro Salvezza è basata su Carisma.

.. _progenie_percepire_peccato:

:Percepire Peccato (Sop):
    La progenie del peccato ha un fiuto in grado di percepire le creature dalla natura affine a quella del suo peccato.
    Per esempio, una progenie dell’ira può percepire le creature che utilizzano gli effetti dell’ira. Il GM dovrebbe stabilire quali creature una particolare progenie del peccato è in grado di percepire.

Descrizione
-----------------
Le progenie del peccato sono il prodotto corrotto della magia usata da antichi incantatori come truppe d’assalto per le loro armate.

Letteralmente la personificazione di un peccato fatto carne, sono degli abomini senzienti di ectoplasma distillato marchiati con l’immagine dell’anima delle creature morte che prediligevano un particolare peccato.

Sette tipi di Progenie
-------------------------
Le statistiche rappresentate si riferiscono alle *Progenie dell'Ira*, il tipo più comune di queste creature.
Ogni tipo possiede modificatori delle caratteristiche unici:

:Accidia: +2 Sag, -2 Des

    Spessi rotoli di pelle in eccesso adornano la loro curva figura. Spesso diventano :ref:`Chierici<chierico>`.
:Avidità: +2 Des, -2 Sag

    Alti più di 2 metri e 20, hanno le vene color dorato e spesso diventano :ref:`Ladri<ladro>`.
:Gola: +2 Cos, -2 Des

    Obesi, bensì solidi e forti, spesso diventano :ref:`Guerrieri<guerriero>`.
:Invidia: +2 For, -2 Car

    Bassi e magri, spesso diventano :ref:`ranger`.
:Ira: Spesso diventano :ref:`Barbari<barbaro>`.
:Lussuria: +4 Car, -2 Cos, -2 Sag

    Con corpi dalle proporzioni che grottescamente si adattano alle loro mostruose facce e artigli, spesso diventano :ref:`Stregoni<stregone>`.
:Orgoglio: +4 Int, -2 Sag, -2 Car

    Unici per le loro folte chiome, sono quasi scheletrici nella loro anoressia e spesso diventano :ref:`Maghi<mago>`.

Descrizione GM (Mia)
-------------------------------
Una magra figura umanoide dai lunghi artigli grondanti di sangue fresco posa lo sguardo su di voi. Gli occhi pieni di una furia antica.

Le sue gambe curvate all'indietro ne trasformano il movimento in una grottesca danza, ma quando ruggisce non è un canto che ne esce dalla bocca.
E non è nemmeno una bocca: tre esili dita si agitano sul termine di piccole braccia incorniciate da denti squaleschi, là dove ce la si sarebbe aspettata.
La lingua ne saetta fuori a saggiare l'aria.

Animale
=====================

.. contents:: Contenuti
    :local:


.. _bestiario_cammello:
.. _bestiario_lupo:
.. _bestiario_pipistrello:
.. _bestiario_squalo:
.. _bestiario_tasso:
.. _bestiario_topo_crudele:
.. _bestiario_vipera:

.. _bestiario_aquila:

--------
Aquila
--------
Questo magnifico uccello predatore ha il piumaggio scuro salvo sulla testa, dove è bianco candido.

Scheda
-------
.. figure:: Immagini/Bestiario/Aquila.png
    :align: right
    :width: 250

.. rubric:: Base

:GS: 1/2
:Tipo: Animale
:Terreno: Montagne
:Clima: Temperato
:Punti Esperienza: 200
:Allineamento: N
:Taglia: Piccolo
:Iniziativa: +2
:Sensi:

    *	:ref:`Visione Crepuscolare<bestiario_visione_crepuscolare>`
    * 	:ref:`percezione` +10

.. rubric:: Difesa

:CA: +2 Des, +1 Naturale, +1 Taglia

    *	Base 14
    *	Contatto 13
    *	Impreparato 12
:pf: 5 (1d8+1)
:Tempra: +3
:Riflessi: +4
:Volontà: +2

.. rubric:: Attacco

:Velocità: 3m, Volare 24m (Media)
:Mischia: 2 Artigli +3 (1d4), Morso +3 (1d4)
:Spazio: 75cm
:Portata: 0m

.. rubric:: Statistiche

:For: 10
:Des: 15
:Cos: 12
:Int: 2
:Sag: 15
:Car: 7
:Att Base: +0
:BMC: -1
:DMC: 11
:Talenti: :ref:`tal_arma_accurata`
:Abilità: :ref:`percezione` +10, :ref:`volare` +8
:Modificatori Razziali: +8 :ref:`percezione`

.. rubric:: Ecologia

:Ambiente: Montagne Temperate
:Organizzazione: Solitario o Coppia
:Tesori: Nessuno

Descrizione
-----------------
Tra i rapaci più maestosi, questi uccelli predatori ghermiscono pesci da fiumi e laghi,
piombano sui roditori e piccoli mammiferi sui prati alpini e sono noti anche per rapire giovani capre dalla presunta sicurezza dei loro picchi.

Queste creature, come tutti gli uccelli da preda, hanno potenti e affilate grinfie artigliate e un becco adunco perfetto per strappare la carne.
La loro vista potenziata gli consente di individuare la preda da grandi distanze, e tipicamente cacciano volando in ampi cerchi sopra il terreno.

Le aquile in genere costruiscono i loro massicci nidi sulle cime degli alberi o tra i dirupi rocciosi di picchi scoscesi. Durante la stagione degli amori,
un'aquila depone due uova, ma normalmente sopravvive solo un pulcino, poiché il più forte dei due di solito uccide e mangia il più debole.

Le aquile generalmente pesano tra i 4 e i 7.5Kg, a seconda della specie, con un'apertura alare di oltre 2.1 metri.

.. _bestiario_cane:

--------------------
Cane
--------------------
.. figure:: Immagini/Bestiario/Cane.png
    :align: right
    :width: 250

Questo piccolo cane ha un pelo ispido e uno sguardo affamato nei suoi occhi marrone scuro.

.. rubric:: Base

:GS: 1/3
:Tipo: Animale
:Terreno: Rovine/Dungeon
:Clima: Temperato
:Punti Esperienza: 135
:Allineamento: N
:Taglia: Piccola
:Iniziativa: +1
:Sensi:
    *   :ref:`Fiuto<bestiario_fiuto>`
    *	:ref:`Visione Crepuscolare<bestiario_visione_crepuscolare>`
    * 	:ref:`percezione` +8

.. rubric:: Difesa

:CA: +1 Des, +1 Naturale, +1 Taglia

    *	Base 13
    *	Contatto 12
    *	Impreparato 12

:pf: 6 (1d8+2)
:Tempra: +4
:Riflessi: +3
:Volontà: +1

.. rubric:: Attacco

:Velocità: 12m
:Mischia: Morso +2 (1d4 + 1)

.. rubric:: Statistiche

:For: 13
:Des: 13
:Cos: 15
:Int: 2
:Sag: 12
:Car: 6
:Att Base: +0
:BMC: +0
:DMC: 11 (15 contro :ref:`azione_sbilanciare`)
:Talenti: :ref:`tal_abilità_focalizzata` (:ref:`percezione`)
:Abilità:
    :ref:`acrobazia` +1 (+9 saltare), :ref:`percezione` +8, :ref:`sopravvivenza` +1 (+5 seguire tracce con il fiuto)
:Modificatori Razziali:
    +4 :ref:`acrobazia` per saltare, +4 :ref:`sopravvivenza` per seguire tracce con il fiuto

.. rubric:: Ecologia

:Ambiente: Qualsiasi
:Organizzazione: Solitario, Coppia o Branco (3-12)
:Tesori: Nessuno

.. _cane_rabbia:

Rabbia
------------

:Tipo: Malattia—Ferimento
:TS: Tempra CD 14
:Insorgenza: 2d6 Settimane
:Frequenza: 1/giorno
:Effetto: 1 danno a Cos più 1d3 danni a Sag (riduzione massima a Sag 1)
:Cura: 2 TS consecutivi

Descrizione
-----------------
Le statistiche del cane presentate qui descrivono qualsiasi piccolo cane del peso approssimativo di 10-25Kg, ma possono essere usate anche per piccoli canidi selvatici come coyote, sciacalli e cani selvaggi.

Nelle terre selvagge i cani sono creature crudeli e territoriali, eppure un cane rabbioso è ancora più terribile di un branco di cani selvatici.

La rabbia colpisce spesso animali come :ref:`pipistrelli<bestiario_pipistrello>`, ghiottoni e ratti, ma la trasformazione di un normale animale domestico familiare li fa passare in secondo piano, rendendo il cane il più noto tra i veicoli di infezione classici della malattia.

Una creatura rabbiosa con un morso può inoculare la rabbia in una vittima. Il suo GS aumenta di 1 (o di un passo, nel caso di una creatura con GS inferiore a 1).

.. _bestiario_cane_da_galoppo:

--------------------
Cane da Galoppo
--------------------
.. figure:: Immagini/Bestiario/CaneDaGaloppo.png
    :align: right
    :width: 300

Questo cane muscoloso è fornito di una piccola sella. Un ringhio basso e minaccioso fuoriesce dal suo torace.

.. rubric:: Base

:GS: 1
:Tipo: Animale
:Terreno: Rovine/Dungeon
:Clima: Temperato
:Punti Esperienza: 400
:Allineamento: N
:Taglia: Medio
:Iniziativa: +2
:Sensi:
    *   :ref:`Fiuto<bestiario_fiuto>`
    *	:ref:`Visione Crepuscolare<bestiario_visione_crepuscolare>`
    * 	:ref:`percezione` +8

.. rubric:: Difesa

:CA: +2 Des, +1 Naturale

    *	Base 13
    *	Contatto 12
    *	Impreparato 11

:pf: 13 (2d8+4)
:Tempra: +5
:Riflessi: +5
:Volontà: +1

.. rubric:: Attacco

:Velocità: 12m
:Mischia: Morso +3 (1d6 + 3 e :ref:`equip_sbilanciare`)

.. rubric:: Statistiche

:For: 15
:Des: 15
:Cos: 15
:Int: 2
:Sag: 12
:Car: 6
:Att Base: +1
:BMC: +3
:DMC: 15 (19 contro :ref:`azione_sbilanciare`)
:Talenti: :ref:`tal_abilità_focalizzata` (:ref:`percezione`)
:Abilità:
    :ref:`acrobazia` +6 (+14 saltare), :ref:`percezione` +8, :ref:`sopravvivenza` +1 (+5 seguire tracce con il fiuto)
:Modificatori Razziali:
    +4 :ref:`acrobazia` per saltare, +4 :ref:`sopravvivenza` per seguire tracce con il fiuto

.. rubric:: Ecologia

:Ambiente: Qualsiasi
:Organizzazione: Solitario, Coppia o Branco (3-12)
:Tesori: Nessuno

Descrizione
-----------------
Tra i cani più grandi (noti tra le razze più piccole quali :ref:`halfling` e :ref:`gnomi` come cani da galoppo) sono comprese razze resistenti come husky, mastini e cani da caccia.

Un cane da galoppo può combattere con un cavaliere, ma questi non può attaccare a sua volta a meno che non superi una prova di :ref:`cavalcare` con CD 10.

.. _bestiario_cane_goblin:

---------------------
Cane Goblin
---------------------
.. figure:: Immagini/Bestiario/CaneGoblin.png
    :align: right
    :width: 300

Il muso di questo canide rognoso ha il naso piatto, gli occhi a capocchia di spillo e i denti sporgenti di un ratto grottescamente cresciuto.

.. rubric:: Base

:GS: 1
:Tipo: Animale 
:Terreno: Foresta/Giungla
:Clima: Temperato
:Punti Esperienza: 400
:Allineamento: N
:Taglia: Media
:Iniziativa: +2
:Sensi: 

    *	:ref:`Fiuto<capacità_fiuto>`
    * 	:ref:`percezione` +1
    *	:ref:`Visione Crepuscolare<bestiario_visione_crepuscolare>`

.. rubric:: Difesa

:CA: +2 Des, +1 Naturale

    *	Base 13
    *	Contatto 12
    *	Impreparato 11
:pf: 9 (1d8+5)
:Tempra: +4
:Riflessi: +4 
:Volontà: +1
:Immunità: Malattia

.. rubric:: Attacco

:Velocità: 15m
:Mischia: Morso +2 (1d6+3 più reazione allergica)

.. rubric:: Statistiche

:For: 15
:Des: 14
:Cos: 16
:Int: 2
:Sag: 12
:Car: 8
:Att Base: +0
:BMC: +2
:DMC: 14
:Talenti: :ref:`tal_robustezza`
:Abilità: 
    :ref:`furtività` +6

.. rubric:: Ecologia

:Ambiente: Foreste Temperate, Paludi o Sotterranei
:Organizzazione: Solitario o Branco (2-12)
:Tesori: Nessuno

.. rubric:: Capacità Speciali

:Reazione Allergica (Str):
    La forfora di un cane goblin è terribilmente irritante per tutte le creature non del sottotipo goblinoide. Una creatura non goblinoide ferita dal morso di un cane goblin, che infligge danni ad un cane goblin con un'arma naturale o un attacco senz'armi, o che entra in contatto con un cane goblin (incluse prove di lottare o cavalcare la creatura) deve superare un TS su Tempra con CD 12 (basata su Cos) o si riempie di eruzioni cutanee pruriginose.

    Una creatura infetta da questa infiammazione subisce penalità -2 a Destrezza e Carisma per 1 giorno (reazioni allergiche multiple non si sommano). :ref:`Rimuovi Malattia<inc_rimuovi_malattia>` o un'altra forma di guarigione magica rimuovono istantaneamente l'infiammazione. Questo è un effetto di malattia.

Descrizione
-----------------
Detestati dagli uomini e dalle bestie, i cani goblin sono disgustosi, fetidi, vigliacchi e cattivi. Non sorprende che i goblin trovino affinità con queste disgustose bestie evitate da tutti. Costantemente afflitto da una rogna irritante esacerbata dalla produzione abbondante di forfora, anche il più sano dei cani goblin appare ammalato e affamato.

Nonostante il suo nome, il cane goblin è in realtà una specie di roditore mostruosamente cresciuto. Il corpo dalle lunghe zampe e la tendenza a cacciare e correre in branchi gli hanno procurato questo nome popolare, un nome sul quale molti goblin non concordano, perché ritengono che le loro cavalcature preferite non abbiano nulla a che fare con i cani veri. Naturalmente, essendo goblin, non si sono preoccupati di trovare un nome alternativo per i cani goblin. Forse non si rendono conto che potrebbero farlo. Il contatto con l'infestata pelle rognosa di un cane goblin causa alla maggior parte delle creature una reazione allergica nota come "orticaria goblin". I goblinoidi sembrano essere immuni a questo disturbo e sono contenti di tenere i cani goblin come guardiani e cavalcature.

Nonostante le condizioni e la malattia della loro pelle, i cani goblin sono molto resistenti alle malattie. Il loro cibo preferito sono le carogne: più putrefatte sono, meglio è. Il fatto che molte tribù goblin lascino liberi i loro cani goblin nelle loro tane, è l'unica ragione per cui la maggior parte delle abitazioni dei goblin non è così sudicia quanto in realtà dovrebbe; costantemente affamato, un cane goblin mangia qualsiasi cosa possa rosicchiare, cose che palati più raffinati rifiuterebbero.

Un cane goblin è lungo 1.5m, ma pesa solo 37.5kg.

Compagno animale Cane Goblin
------------------------------
:Statistiche iniziali:
    :Taglia: Piccola
    :Velocità: 15m
    :Attacco: Morso (1d4)
    :Caratteristiche:
        :For: 11
        :Des: 16
        :Cos: 11
        :Int: 2
        :Sag: 12
        :Car: 8
    :Qualità Speciali:

        *	:ref:`Fiuto<capacità_fiuto>`
        * 	Reazione Allergica (vedi scheda)
        *	:ref:`Visione Crepuscolare<bestiario_visione_crepuscolare>`

:Aumento al 4° livello:
    :Taglia: Media
    :CA: +1 Armatura Naturale
    :Attacco: Morso (1d6)
    :Caratteristiche:
        :For: +4
        :Des: -2
        :Cos: +4

Descrizione GM (Mia)
-------------------------------
Lasciate un cane senza mangiare per giorni, nel fango, con la rogna. Il risultato non sarà neanche lontanamente ripugnante come l'aspetto di questo essere. Una sorta di gigantesco ratto disgustosamente fetido e rognoso. 

La coda lunga, fine e glabra frusta l'aria, mentre si inarca preparando un attacco.

.. _bestiario_cavallo:

--------------------
Cavallo
--------------------
.. figure:: Immagini/Bestiario/Cavallo.png
    :align: right
    :width: 350

Questo fiero cavallo romba attraverso le pianure erbose con una fluida grazia, con il vento che lo trascina a briglia sciolta.

.. rubric:: Base

:GS: 1
:Tipo: Animale
:Terreno: Pianure
:Clima: Temperato
:Punti Esperienza: 400
:Allineamento: N
:Taglia: Grande
:Iniziativa: +2
:Sensi:
    *   :ref:`Fiuto<bestiario_fiuto>`
    *	:ref:`Visione Crepuscolare<bestiario_visione_crepuscolare>`
    * 	:ref:`percezione` +6

.. rubric:: Difesa

:CA: +2 Des, -1 Taglia

    *	Base 11
    *	Contatto 11
    *	Impreparato 9

:pf: 15 (2d8 + 6)
:Tempra: +6
:Riflessi: +5
:Volontà: +1

.. rubric:: Attacco

:Velocità: 15m
:Mischia: 2 Zoccoli -2 (1d4 + 1)
:Spazio: 3m
:Portata: 1.5m

.. rubric:: Statistiche

:For: 16
:Des: 14
:Cos: 17
:Int: 2
:Sag: 13
:Car: 7
:Att Base: +1
:BMC: +5
:DMC: 17 (21 contro :ref:`azione_sbilanciare`)
:Talenti: :ref:`tal_correre`, :ref:`tal_resistenza_fisica`
:Abilità:
    :ref:`percezione` +6
:Qualità Speciali:
    :ref:`Docile<cavallo_docile>`

.. rubric:: Ecologia

:Ambiente: Pianure Temperate
:Organizzazione: Solitario, Coppia o Mandria (3-30)
:Tesori: Nessuno

.. rubric:: Capacità Speciali

.. _cavallo_docile:

:Docile (Str):
    Se non è :ref:`addestrato al combattimento<addestrato_al_combattimento>`, gli zoccoli di un cavallo vengono considerati come :ref:`attacchi secondari<attacchi_secondari>`.

Descrizione
-----------------
I cavalli sono alti al garrese tra 1.5 e 1.8 metri e pesano tra 500 e 750Kg.

Le statistiche riportate qui sono per un tipico cavallo da corsa, detto anche *cavallo leggero*.

Alcuni cavalli sono più grandi e robusti, allevati per il lavoro come trainare aratri o carrozze.
Questi sono chiamati *cavalli pesanti* e le loro statistiche sono leggermente diverse.

Cavallo Pesante
-----------------
Un cavallo pesante ottiene l'archetipo semplice :ref:`avanzato<archetipo_avanzato>`, un attacco con il morso per 1d4 danni e il danno degli zoccoli aumenta a 1d6.

.. Note::
    I cavalli (leggeri o pesanti), possono essere addestrati per il combattimento con :ref:`addestrare_animali`.


.. _bestiario_pony:

--------------------
Cavallo, Pony
--------------------

Questo equino tarchiato si muove con passo pesante e occhi curiosi. Avvicinandosi, allunga il muso, aspettandosi una carezza.

.. rubric:: Base

:GS: 1/2
:Tipo: Animale
:Terreno: Pianure
:Clima: Temperato
:Punti Esperienza: 200
:Allineamento: N
:Taglia: Media
:Iniziativa: +1
:Sensi:
    *   :ref:`Fiuto<bestiario_fiuto>`
    *	:ref:`Visione Crepuscolare<bestiario_visione_crepuscolare>`
    * 	:ref:`percezione` +5

.. rubric:: Difesa

:CA: +1 Des

    *	Base 11
    *	Contatto 11
    *	Impreparato 10

:pf: 13 (2d8 + 4)
:Tempra: +5
:Riflessi: +4
:Volontà: +0

.. rubric:: Attacco

:Velocità: 12m
:Mischia: 2 Zoccoli -3 (1d3)

.. rubric:: Statistiche

:For: 13
:Des: 13
:Cos: 14
:Int: 2
:Sag: 11
:Car: 4
:Att Base: +1
:BMC: +2
:DMC: 13 (17 contro :ref:`azione_sbilanciare`)
:Talenti: :ref:`tal_correre`, :ref:`tal_resistenza_fisica`
:Abilità:
    :ref:`percezione` +5
:Qualità Speciali:
    :ref:`Docile<cavallo_docile>`

.. rubric:: Ecologia

:Ambiente: Pianure Temperate
:Organizzazione: Solitario, Coppia o Mandria (3-30)
:Tesori: Nessuno

Descrizione
-----------------
I pony sono una razza più piccola di cavalli che si adattano meglio ad essere cavalcati da :ref:`halfling`, :ref:`gnomi` e :ref:`nani`, ma sono amati anche dagli :ref:`umani`.

Alti tra i 90 e i 120 centimetri, pesano circa 300Kg. Per loro un peso leggero è fino a 50Kg, medio fino a 100Kg e pesante fino a 150Kg, ma possono trasportare fino a 750Kg.

Le statistiche qui riportate si riferiscono ad un tipico pony, ma esistono pony più robusti a cui viene applicato l'archetipo semplice :ref:`avanzato<archetipo_avanzato>`, ma sono piuttosto
rari e, diversamente dai cavalli, non vengono chiamati "pesanti".

.. Note::
    Come i cavalli, i pony possono essere addestrati per il combattimento con :ref:`addestrare_animali` e possono fungere da cavalcature in combattimento per :ref:`halfling`, :ref:`gnomi` e altre razze piccole.

.. _bestiario_cinghiale:

--------------
Cinghiale
--------------
.. figure:: Immagini/Bestiario/Cinghiale.png
    :align: right
    :width: 400

I piccoli occhi iniettati di sangue di questa bestia feroce luccicano furiosi sopra una bocca da cui escono zanne aguzze

.. rubric:: Base

:GS: 2
:Tipo: Animale
:Terreno: Foresta/Giungla
:Clima: Temperato
:Punti Esperienza: 600
:Allineamento: N
:Taglia: Media
:Iniziativa: +0
:Sensi: 

    *	:ref:`Fiuto<capacità_fiuto>`
    *	:ref:`Visione Crepuscolare<bestiario_visione_crepuscolare>`
    * 	:ref:`percezione` +6

.. rubric:: Difesa

:CA: +4 Naturale

    *	Base 14
    *	Contatto 10
    *	Impreparato 14

:pf: 18 (2d8+9)
:Tempra: +6
:Riflessi: +3 
:Volontà: +1
:Capacità Difensive: :ref:`Ferocia<bestiario_ferocia>`

.. rubric:: Attacco

:Velocità: 12m
:Mischia: Corno +4 (1d8+4)

.. rubric:: Statistiche

:For: 17
:Des: 10
:Cos: 17
:Int: 2
:Sag: 13
:Car: 4
:Att Base: +1
:BMC: +4
:DMC: 14
:Talenti: :ref:`tal_robustezza`
:Abilità: 
    :ref:`percezione` +6

.. rubric:: Ecologia

:Ambiente: Foreste Temperate o Tropicali
:Organizzazione: Solitario, Coppia o Branco (3-8) 
:Tesori: Nessuno

Descrizione
-----------------
Molto più irascibili e pericolosi dei loro simili addomesticati, i cinghiali sono creature onnivore comuni nelle foreste temperate, sebbene siano abituati al clima tropicale. 

Esistono anche altre varianti, come i facoceri che popolano le pianure tropicali e le savane. I cinghiali vengono spesso uccisi per la loro carne, considerata prelibata da gran parte degli umanoidi. I cinghiali vengono spesso cacciati con una lancia speciale dotata di una trave traversale usata per evitare che un cinghiale trafitto cerchi di incornare il suo cacciatore.

L'indole caparbia del cinghiale e l'abitudine di mangiare anche le ossa lo rendono adatto come animale da compagnia per alcune persone. Molte gilde di ladri tengono i cinghiali per lo smaltimento dei corpi, mentre alcune tribù di orchi li lasciano correre liberamente nelle tane per ripulirle dai rifiuti. 

Un cinghiale è lungo 1.2m e pesa circa 100kg.

.. _bestiario_corvo:

--------
Corvo
--------

Scheda
-------
.. figure:: Immagini/Bestiario/Corvo.png
    :align: right
    :width: 400

.. rubric:: Base

:GS: 1/6
:Tipo: Animale 
:Terreno: Foresta/Giungla
:Clima: Temperato
:Punti Esperienza: 65
:Allineamento: N
:Taglia: Minuscolo
:Iniziativa: +2
:Sensi: 

    *	:ref:`Visione Crepuscolare<bestiario_visione_crepuscolare>`
    * 	:ref:`percezione` +6

.. rubric:: Difesa

:CA: +2 Des, +2 Taglia

    *	Base 14
    *	Contatto 14
    *	Impreparato 12
:pf: 3 (1d8-1)
:Tempra: +1
:Riflessi: +4 
:Volontà: +2

.. rubric:: Attacco

:Velocità: 3m, Volare 12m (Media)
:Mischia: Morso +4 (1d3-4)
:Spazio: 75cm
:Portata: 0m

.. rubric:: Statistiche

:For: 2
:Des: 15
:Cos: 8
:Int: 2
:Sag: 15
:Car: 7
:Att Base: +0
:BMC: +0
:DMC: 6
:Talenti: :ref:`tal_abilità_focalizzata` (:ref:`percezione`), :ref:`tal_arma_accurata`
:Abilità: :ref:`percezione` +6, :ref:`volare` +6

.. rubric:: Ecologia

:Ambiente: Qualsiasi Temperato
:Organizzazione: Solitario, Coppia, Stormo (3-12), Infestazione (13-100)
:Tesori: Nessuno

Famiglio
---------------
Il maestro di un corvo :ref:`Famiglio<famiglio_mago>` guadagna +3 alle prove di :ref:`Valutare<valutare>`

Descrizione
-----------------
Il corvo è un saprofago onnivoro che si nutre di carogne, insetti, rifiuti alimentari, bacche e anche piccoli animali.

Bestie Magiche
==============

.. contents:: Contenuti
    :local:

.. _bestiario_bunyip:

--------------------
Bunyip
--------------------
.. figure:: Immagini/Bestiario/Bunyip.png
    :align: right
    :width: 400

Un inquietante incrocio tra uno squalo e una foca, questa creatura coperta di pelliccia marroncina ha la bocca riempita di denti affilati come rasoi.

.. rubric:: Base

:GS: 3
:Tipo: Bestia Magica
:Sottotipo: :ref:`Acquatico<sottotipo_acquatico>`
:Terreno: Acqua
:Clima: Temperato
:Punti Esperienza: 800
:Allineamento: N
:Taglia: Media
:Iniziativa: +3
:Sensi:
    *	:ref:`Scurovisione<scurovisione>` 18m
    *   :ref:`Visione Crepuscolare<bestiario_visione_crepuscolare>`
    *   :ref:`Olfatto Acuto<bestiario_olfatto_acuto>` 54m
    * 	:ref:`percezione` +8

.. rubric:: Difesa

:CA: +3 Des, +2 Naturale

    *	Base 15
    *	Contatto 13
    *	Impreparato 12

:pf: 32 (5d10+5)
:Tempra: +5
:Riflessi: +7
:Volontà: +1

.. rubric:: Attacco

:Velocità: 3m, Nuotare 15m
:Mischia: Morso +7 (1d8+1/19-20 e :ref:`Sanguinamento<sanguinamento>`)
:Attacchi Speciali:
    :ref:`Frenesia Sanguinaria<bunyip_frenesia_sanguinaria>`, :ref:`Ruggito<bunyip_ruggito>`, :ref:`Sanguinamento<sanguinamento>` (1d6)

.. rubric:: Statistiche

:For: 13
:Des: 16
:Cos: 13
:Int: 2
:Sag: 11
:Car: 7
:Att Base: +5
:BMC: +6
:DMC: 19
:Talenti: :ref:`tal_abilità_focalizzata` (:ref:`furtività`, :ref:`percezione`), :ref:`tal_arma_focalizzata` (morso) :ref:`tal_critico_migliorato` (morso)
:Abilità:
    :ref:`artista_della_fuga` +5, :ref:`furtività` +10, :ref:`nuotare` +9, :ref:`percezione` +8
:Qualità Speciali: :ref:`Anfibio<bestiario_anfibio>`

.. rubric:: Ecologia

:Ambiente: Qualsiasi Acquatico
:Organizzazione: Solitario o Coppia
:Tesori: Nessuno

.. rubric:: Capacità Speciali

.. _bunyip_frenesia_sanguinaria:

:Frenesia Sanguinaria (Str):
    Si attiva quando il mostro rileva sangue in acqua utilizzando il suo :ref:`Olfatto Acuto<bestiario_olfatto_acuto>`, ma per il resto funziona come l'omonima :ref:`regola universale dei mostri<bestiario_frenesia_sanguinaria>`.

.. _bunyip_ruggito:

:Ruggito (Sop):
    Il ruggito di un bunyip è incredibilmente forte e terrificante.

    Quando un bunyip ruggisce (:ref:`azione_standard` che la creatura può effettuare a volontà), tutte le creature con 4 o meno DV entro 30 metri che lo sentono devono superare un TS
    su Volontà con CD 13 o cadono :ref:`In Preda al Panico<in_preda_al_panico>` per 2d4 round.

    Che il Tiro Salvezza riesca o meno, le creature nell’area divengono immuni al ruggito di quel bunyip per 24 ore.

    .. Note::
        Questo è un effetto di paura, sonoro e influenza mentale.

        La CD del TS è basata su Costituzione.

Descrizione
-----------------
Il bunyip è un cacciatore feroce e famelico, in possesso di una crudeltà primordiale da apparire quasi malvagio nella sua rapacità.

Un bunyip di solito vive in grandi insenature di acqua dolce o in caverne costiere riparate dove il cibo è abbondante e si trova bene sia in acqua dolce che salata.
Preferisce nutrirsi di animali di taglia Piccola o più grandi, anche se non disdegna all’occasione gli umanoidi.

I bunyip sono piuttosto territoriali, e attaccano all’istante gli intrusi che minacciano i loro territori di caccia.
Si accoppiano solo in tarda primavera e durante questo periodo diventano più aggressivi; dopo l’accoppiamento, le coppie si dividono, e le femmine vanno in cerca di un luogo
per partorire una piccola cucciolata dai quattro ai sei elementi.
Le femmine vegliano sui loro cuccioli per alcuni giorni, finché questi non diventano abbastanza indipendenti da consentire alle madri di andarsene.

Segnalazioni di avvistamenti di bunyip giungono da ogni angolo del mondo e, anche se la veridicità di tali voci è dubbia, ci sono sufficienti riscontri attendibili a confermare la loro notevole capacità di adattamento:
proliferano in ambienti diversi, dai gelidi fiordi polari alle idilliache lagune tropicali.
Il bunyip non è una creatura delle profondità, ed evita anche i bacini d’acqua dolce più estesi, preferendo appostarsi vicino alle coste dove il suo cibo preferito è più diffuso.

Sebbene i bunyip varino nell’aspetto, hanno la stessa struttura fisica di base: la testa ha marcate caratteristiche da foca, eccetto per le sue fauci da squalo, il tronco superiore è robusto e muscoloso, con lunghi arti simili a pinne.
Alcune specie hanno anche una singola pinna dorsale da squalo e la restante parte del corpo prosegue in una lunga coda.
Quelli con pelliccia di solito sviluppano soltanto un rado manto sul corpo superiore con tonalità grigio pallido, marrone, o nera.

Esterno
=======
.. contents:: Contenuti
    :local:

.. _bestiario_elementale_fuoco:

--------------------
Elementale del Fuoco
--------------------

.. figure:: Immagini/Bestiario/TabellaElementaliFuoco.png
    :align: right
    :width: 300

    Grandezze degli elementali del fuoco.

Questa creatura sembra un falò vivente semovente, con lingue di fuoco che fuoriescono in cerca di qualcosa da bruciare.

:Tipo: Esterno
:Sottotipo: :ref:`Elementale<sottotipo_elementale>`, :ref:`Extraplanare<sottotipo_extraplanare>`, :ref:`Fuoco<sottotipo_fuoco>`
:Terreno: Rovine/Dungeon
:Clima: Extraplanare
:Linguaggi: Infernale

.. rubric:: Ecologia

:Ambiente: Qualsiasi (Piano del fuoco)
:Organizzazione: Solitario, Coppia o Gruppo (3-6)
:Tesori: Nessuno

Descrizione
-----------------
Gli elementali del fuoco sono creature veloci e crudeli fatte di fiamme viventi che si divertono a spaventare quelli più deboli di loro e terrorizzano qualsiasi creatura che possano incendiare.

Un elementale del fuoco non può entrare nell'acqua o in qualsiasi liquido non infiammabile: una massa d'acqua è una barriera impenetrabile a meno che l'elementale possa scavalcarla o saltarla, oppure venga coperta con un materiale infiammabile, come uno strato d'olio.

Gli elementali del fuoco hanno un aspetto variabile; in genere si manifestano in forma di spire serpentine fatte di fumo e fiamme, ma alcuni prendono sembianze più simili a quelle di umani, demoni o altri mostri per aumentare il terrore quando compaiono improvvisamente.
Il loro corpo sembra fatto di fumo, cenere, fiamme o sbuffi di scintille semistabili.

.. _bestiario_elementale_fuoco_piccolo:

----------------------------
Elementale del Fuoco Piccolo
----------------------------

.. figure:: Immagini/Bestiario/SmallFireElemental.png
    :align: left
    :width: 200

.. Note::
    Creatura evocabile tramite :ref:`inc_evoca_mostri_II`.

.. rubric:: Base

:GS: 1
:Punti Esperienza: 400
:Allineamento: N
:Taglia: Piccola
:Iniziativa: +5
:Sensi:
    *	:ref:`Scurovisione<scurovisione>` 18m
    * 	:ref:`percezione` +7

.. rubric:: Difesa

:CA: +1 Des, +3 Naturale, +1 Schivare, +1 Taglia

    *	Base 16
    *	Contatto 13
    *	Impreparato 14

:pf: 11 (2d10)
:Tempra: +3
:Riflessi: +4
:Volontà: 0
:Immunità: Fuoco, Tratti degli elementali
:Debolezze: Vulnerabilità al freddo

.. rubric:: Attacco

:Velocità: 15m
:Mischia: Schianto +4 (1d4 più bruciare)
:Attacchi Speciali:
    Bruciare (1d4, CD 11)

.. rubric:: Statistiche

:For: 10
:Des: 13
:Cos: 10
:Int: 4
:Sag: 11
:Car: 11
:Att Base: +2
:BMC: +1
:DMC: 13
:Talenti: :ref:`tal_arma_accurata`, :ref:`tal_iniziativa_migliorata`, :ref:`tal_schivare`
:Abilità:
    :ref:`acrobazia` +5 :ref:`artista_della_fuga` +5, :ref:`conoscenze` (piani) +1, :ref:`intimidire` +4, :ref:`percezione` +4, :ref:`scalare` +4


.. _bestiario_vargouille:

--------------------
Vargouille
--------------------
.. figure:: Immagini/Bestiario/Vargouille.png
    :align: left
    :width: 250

Questa creatura è una testa d'immondo con ali da pipistrello, tentacoli penzolanti da mento e scalpo e una bocca zannuta.

.. rubric:: Base

:GS: 2
:Tipo: Esterno
:Sottotipo: :ref:`Extraplanare<sottotipo_extraplanare>`, :ref:`Malvagio<sottotipo_malvagio>`
:Terreno: Rovine/Dungeon
:Clima: Temperato
:Punti Esperienza: 600
:Allineamento: NM
:Taglia: Piccola
:Iniziativa: +1
:Sensi:
    *	:ref:`Scurovisione<scurovisione>` 18m
    * 	:ref:`percezione` +7

.. rubric:: Difesa

:CA: +1 Des, +3 Naturale, +1 Taglia

    *	Base 15
    *	Contatto 12
    *	Impreparato 14

:pf: 19 (3d10+3)
:Tempra: +4
:Riflessi: +4
:Volontà: +2

.. rubric:: Attacco

:Velocità: Volare 9m (buona)
:Mischia: Morso +5 (1d4 più veleno)
:Attacchi Speciali:
    :ref:`Bacio<vargouille_bacio>`, :ref:`Stridio<vargouille_stridio>`, :ref:`Veleno<vargouille_veleno>`

.. rubric:: Statistiche

:For: 10
:Des: 13
:Cos: 13
:Int: 5
:Sag: 12
:Car: 8
:Att Base: +3
:BMC: +2
:DMC: 13
:Talenti: :ref:`tal_abilità_focalizzata` (:ref:`furtività`), :ref:`tal_arma_accurata`
:Abilità:
    :ref:`furtività` +8, :ref:`intimidire` +5, :ref:`percezione` +7, :ref:`volare` +13
:Linguaggi: Infernale

.. rubric:: Ecologia

:Ambiente: Qualsiasi
:Organizzazione: Coppia, Gruppo (3-6) o Manipolo (7-12)
:Tesori: Nessuno

.. rubric:: Capacità Speciali

.. _vargouille_bacio:

:Bacio (Sop):
    Un vargouille può baciare un bersaglio :ref:`Indifeso<indifeso>` con un :ref:`attacco_contatto` in mischia, che provoca :ref:`Attacchi d'Opportunità<attacco_opportunità>`.

    Un avversario baciato deve superare un Tiro Salvezza su Tempra con CD 16 (basata su Costituzione, include bonus razziale +4) o inizia una terribile trasformazione che muta la creatura in un vargouille entro 24 ore o addirittura più in fretta.

        * Inizialmente, nell'arco di 1d6 ore, tutti i capelli della vittima cadono.
        * In altre 1d6 ore le orecchie crescono diventando ali membranose, spuntano dei tentacoli sul mento e sullo scalpo e i denti diventano lunghe zanne appuntite.
        * Durante le successive 1d6 ore, la vittima subisce un risucchio di Intelligenza e Carisma pari a 1 punto ogni ora (fino ad un minimo di 3).
        * La trasformazione si completa in altre 1d6 ore, quando la testa della vittima si stacca dal corpo, che muore istantaneamente, e diventa un vargouille.

    .. Important::
        La **progressione** della trasformazione viene interrotta dalla luce solare o da qualsiasi incantesimo di luce di 3° livello o superiore, ma fermare la trasformazione richiede :ref:`Rimuovi Malattia<inc_rimuovi_malattia>` o un effetto simile.

        La trasformazione è un effetto di malattia.

.. _vargouille_stridio:

:Stridio (Sop):
    Anziché mordere, un vargouille può spalancare la bocca per stridere.

    Chiunque (tranne altre vargouille) entro 18 metri ascolti lo stridio e possa vedere chiaramente la creatura deve superare un Tiro Salvezza su Tempra con CD 12 (basata su Costituzione) o resta :ref:`Paralizzato<paralizzato>` per 2d4 round o finché il mostro l’attacca, esce dal raggio di azione o smette di guardarlo.
    Una creatura che supera il Tiro Salvezza non può essere influenzata nuovamente dallo stridio dello stesso vargouille per 24 ore.

.. _vargouille_veleno:

Veleno (Sop)
------------
:Tipo: Morso—ferimento
:TS: Tempra CD 12 (basata su Costituzione)
:Frequenza: una volta
:Effetto: i danni causati dal morso possono essere curati solo con la magia, superando una prova di livello dell’incantatore con CD 20.
:Cura: 1 TS

Descrizione
-----------------
Un vargouille non è più grande di una testa umana, tipicamente tra i 30 e i 50 cm di altezza con un’apertura alare tra i 37 e i 122 cm. I vargouille non sono nativi del Piano Materiale, tuttavia vi si possono trovare spesso, ad infestare cimiteri, antichi campi di battaglia o qualsiasi luogo dove si possano trovare resti morti o putrefatti.

Questi orribili mostri vengono dai piani esterni immondi, dove svolazzano attraverso cieli bizzarri e stregati alla costante ricerca di anime fresche da tormentare. In questo reame da incubo, i vargouille ricoprono un ruolo simile a quello di corvi o avvoltoi, sebbene recitino questa parte traendo un malevolo piacere nel causare panico e angoscia che nessun uccello necrofago potrebbe mai sperare di eguagliare.

I vargouille attaccano piombando sui loro avversari, stridendo per paralizzarli e poi mordendoli con i loro denti affilati come rasoi. Quando più vargouille fanno causa comune e combattono come alleati, sopraffanno le loro vittime con morsi e stridii, facendole a brandelli.

Lo scarso potere e l’orripilante metodo riproduttivo delle vargouille sono una combinazione pericolosa. I vargouille sono relativamente semplici da evocare sul Piano Materiale e, una volta lì, si possono riprodurre rapidamente, creando sempre più loro simili dalle vittime che soccombono al loro bacio. I vargouille creati in questo modo sul Piano Materiale sono sempre creature extraplanari, e come tali possono essere esiliate con la giusta magia.

Descrizione GM (Mia)
-------------------------------
Un volto esce dall'ombra sussultando, con mostruose orecchie che sbattono ritmicamente.

Ci vuole poco perchè vi rendiate conto che quelle non sono orecchie, bensì ali, attaccate ad una testa senza corpo, con viscidi tentacoli che penzolano da mento e scalpo, agitandosi al ritmo delle grottesche ali.
La creatura emette un gemito acuto e si fionda su di voi.

Non Morto
==============
.. contents:: Contenuti
    :local:

.. _bestiario_scheletro_umano:

---------------
Scheletro Umano
---------------
.. figure:: Immagini/Bestiario/ScheletroUmano.png
    :align: right
    :width: 400

Il cumulo di ossa si muove improvvisamente, alzandosi e assumendo forma umana. Le sue lunghe dita ossute cercano di artigliare i viventi.

.. rubric:: Base

:GS: 1/3
:Tipo: Non Morto
:Terreno: Rovine/Dungeon
:Clima: Temperato
:Punti Esperienza: 135 
:Allineamento: NM
:Taglia: Media
:Iniziativa: +6
:Sensi: 

    *	:ref:`Scurovisione<scurovisione>` 18m
    * 	:ref:`percezione` +0

.. rubric:: Difesa

:CA: +2 Armatura, +2 Des, +2 Naturale

    *	Base 16
    *	Contatto 12
    *	Impreparato 14

:pf: 4 (1d8)
:Tempra: +0
:Riflessi: +2 
:Volontà: +2
:RD: 5/Contundente
:Immunità: Freddo, tratti dei non morti

.. rubric:: Attacco

:Velocità: 9m
:Mischia: Scimitarra rotta +0 (1d6), artiglio -3 (1d4+1) o 2 artigli +2 (1d4+2)

.. rubric:: Statistiche

:For: 15
:Des: 14
:Cos: -
:Int: -
:Sag: 10
:Car: 10
:Att Base: +0
:BMC: +2
:DMC: 14
:Talenti: :ref:`Iniziativa Migliorata<tal_iniziativa_migliorata>`
:Equipaggiamento: Cotta di Maglia Rotta, Scimitarra Rotta

.. rubric:: Ecologia

:Ambiente: Qualsiasi
:Organizzazione: Qualsiasi
:Tesori: Nessuno

Descrizione
-----------------
Gli scheletri sono ossa di morti animate, portate alla non vita da magie sacrileghe. Per la maggior parte, gli scheletri sono automi privi di volontà, ma possiedono un'astuzia malvagia concessa loro dalla forza che li anima: un'astuzia che permette loro di portare armi e indossare armature. 

Creare uno scheletro
--------------------
TODO

Varianti
---------
TODO

.. _bestiario_zombie:

---------------
Zombie
---------------
.. figure:: Immagini/Bestiario/Zombie.png
    :align: right
    :width: 400

Questo cadavere indossa stracci consunti, e brandelli di carne penzolano dalle ossa mentre barcolla con le braccia sollevate in avanti.

.. rubric:: Base

:GS: 1/2
:Tipo: Non Morto
:Terreno: Rovine/Dungeon
:Clima: Temperato
:Punti Esperienza: 200
:Allineamento: NM
:Taglia: Media
:Iniziativa: +0
:Sensi:

    *	:ref:`Scurovisione<scurovisione>` 18m
    * 	:ref:`percezione` +0

.. rubric:: Difesa

:CA: +2 Naturale

    *	Base 12
    *	Contatto 10
    *	Impreparato 12

:pf: 12 (2d8+3)
:Tempra: +0
:Riflessi: +0
:Volontà: +3
:RD: 5/Tagliente
:Immunità: Tratti dei Non Morti

.. rubric:: Attacco

:Velocità: 9m
:Mischia: Schianto +4 (1d6+4)

.. rubric:: Statistiche

:For: 17
:Des: 10
:Cos: -
:Int: -
:Sag: 10
:Car: 10
:Att Base: +1
:BMC: +4
:DMC: 14
:Talenti: :ref:`Robustezza<tal_iniziativa_migliorata>`

.. rubric:: Capacità Speciali

:Barcollante (Str):
    Gli zombi hanno riflessi scarsi e possono effettuare soltanto una :ref:`azione_movimento` o un’:ref:`azione_standard` ogni round.
    Uno zombi può muoversi alla propria velocità ed attaccare soltanto effettuando una :ref:`Carica<azione_carica>`.

.. rubric:: Ecologia

:Ambiente: Qualsiasi
:Organizzazione: Qualsiasi
:Tesori: Nessuno

Descrizione
-----------------
Gli zombi sono i cadaveri animati di creature morte, costretti a muoversi da magie necromantiche come :ref:`Animare Morti<inc_animare_morti>`.
Anche se gli zombi incontrati di norma sono lenti e robusti, altri possiedono tratti differenti, che permettono loro di diffondere una malattia o di muoversi più rapidi.

Gli zombi sono automi senza mente e non possono fare altro che seguire gli ordini. Se lasciati a loro stessi, attendono immobili o si spostano alla ricerca di creature viventi da massacrare e divorare.

.. Note::
    Gli zombi attaccano fino alla distruzione, senza curarsi della loro sicurezza.

Sebbene siano in grado di seguire gli ordini, gli zombi vengono spesso lasciati liberi con l’ordine di uccidere tutte le creature viventi e incontrati in branchi che infestano le terre frequentate dai viventi, in cerca di preda.
La maggior parte degli zombi viene creata attraverso :ref:`Animare Morti<inc_animare_morti>`. Simili zombi sono sempre standard, a meno che il creatore lanci anche :ref:`Velocità<inc_velocità>` o :ref:`Rimuovi Paralisi<inc_rimuovi_paralisi>` per creare Zombi Rapidi o :ref:`Contagio<inc_contagio>` per creare Zombi Infetti.

Creare uno Zombie
--------------------
TODO

Varianti
---------
TODO

Parassita
==============
.. contents:: Contenuti
    :local:

.. _bestiario_scarabeo_fuoco:

------------------
Scarabeo di Fuoco
------------------
.. figure:: Immagini/Bestiario/ScarabeoFuoco.png
    :align: left
    :width: 400

Questo scarabeo dalle dimensioni di un gatto è di un marrone opaco reso brillante da due punti splendenti verde gialli sul carapace.

.. Note::
    Creatura evocabile tramite :ref:`inc_evoca_mostri`.

.. rubric:: Base

:GS: 1/3
:Tipo: Parassita
:Sottotipo: :ref:`Parassita<sottotipo_parassita>`
:Terreno: Sotterraneo
:Clima: Temperato
:Punti Esperienza: 135
:Allineamento: N
:Taglia: Piccolo
:Iniziativa: +0
:Sensi:

    *	:ref:`Visione Crepuscolare<bestiario_visione_crepuscolare>`
    * 	:ref:`percezione` +0

.. rubric:: Difesa

:CA: +2 Naturale, +1 Taglia

    *	Base 12
    *	Contatto 11
    *	Impreparato 12

:pf: 4 (1d8)
:Tempra: +2
:Riflessi: +0
:Volontà: +0
:Immunità: Influenza mentale

.. rubric:: Attacco

:Velocità: 9m
:Mischia: Morso +1 (1d4)

.. rubric:: Statistiche

:For: 10
:Des: 11
:Cos: 11
:Int: -
:Sag: 10
:Car: 7
:Att Base: +0
:BMC: -1
:DMC: 9 (17 contro :ref:`azione_sbilanciare`)
:Abilità:
    :ref:`volare` -2

.. rubric:: Ecologia

:Ambiente: Qualsiasi
:Organizzazione: Solitario, Gruppo (2-6) o Colonia (7-12)
:Tesori: Nessuno

.. rubric:: Capacità Speciali

:Luminescenza (Str):
    Le ghiandole di uno scarabeo del fuoco sprigionano luce in un raggio di 3 metri e continuano a risplendere anche 1d6 giorni dopo la sua morte.

Descrizione
-----------------
Sebbene sia notturno, lo scarabeo del fuoco non possiede scurovisione: per l'illuminazione dispone di ghiandole fosforescenti.

Gli scarabei del fuoco messi in gabbia sono una fonte di illuminazione duratura per le persone eccentriche e i minatori.

Varianti
---------

.. rubric::  Scarabeo Lampo (GS 1/2)

Uno scarabeo lampo è uno scarabeo del fuoco avanzato che crea un forte bagliore di luce una volta all'ora.

Quando uno scarabeo lampo emana la luce, tutte le creature nel raggio di 3 metri devono effettuare un TS su Tempra con CD 12 (basata su Cos) o sono abbagliate per 1d3 round.

.. rubric::  Scarabeo Scavatore (GS 1/2)

Uno scarabeo lampo è uno scarabeo del fuoco avanzato con l'archetipo semplice :ref:`Avanzato<archetipo_avanzato>` e una velocità di scavare di 6 metri.

Umanoide
==========

.. contents:: Contenuti
    :local:

.. _bestiario_goblin:

--------------
Goblin
--------------
.. figure:: Immagini/Bestiario/Goblin.png
    :align: right
    :width: 400

Questa creatura è alta poco meno un metro e il suo magro corpo umanoide è reso ancor più piccolo da un'ampia testa sgraziata.

.. rubric:: Base

:GS: 1/3
:Tipo: Umanoide
:Terreno: Foresta/Giungla
:Clima: Temperato
:Punti Esperienza: 135 
:Classe: Goblin Combattente 1
:Allineamento: NM
:Taglia: Piccolo (goblinoide)
:Iniziativa: +6
:Sensi: 

    *	:ref:`Scurovisione<scurovisione>` 18m
    * 	:ref:`percezione` -1

.. rubric:: Difesa

:CA: +2 armatura, +2 Des, +1 scudo, +2 Taglia

    *	Base 16
    *	Contatto 13
    *	Impreparato 14
:pf: 6 (1d10+1)
:Tempra: +3
:Riflessi: +2 
:Volontà: -1

.. rubric:: Attacco

:Velocità: 9m
:Mischia: Spada Corta +2 (1d4/19-20)
:Distanza:  Arco Corto +4 (1d4/x3)

.. rubric:: Statistiche

:For: 11
:Des: 15
:Cos: 12
:Int: 10
:Sag: 9
:Car: 6
:Att Base: +1
:BMC: +0
:DMC: 12
:Talenti: :ref:`Iniziativa Migliorata<tal_iniziativa_migliorata>`
:Abilità: 
    :ref:`Cavalcare<cavalcare>` +10, :ref:`Furtività<furtività>` +10, :ref:`Nuotare<nuotare>` +4

    :Modificatori Razziali: +4 :ref:`Cavalcare<cavalcare>`, +4 :ref:`Furtività<furtività>`
:Linguaggi: Goblin

.. rubric:: Ecologia

:Ambiente: Foreste e pianure temperate, normalmente regioni costiere.
:Organizzazione: gruppo (4-9), banda di guerra (10-16 con cavalcature cani goblin) o tribù (17 + più 100% non combattenti; 1 sergebte di 3° livello ogni 20 adulti; 1 o 2 luogotenenti di 4° o 5° livello; 1 capo di 6°-8° livello e 10-40 cani goblin, lupi o worg)
:Tesori: equipaggiamento da PNG: :ref:`Armatura di Cuoio<equip_armatura_cuoio>`, :ref:`Scudo Leggero di Legno<equip_scudo_leggero_legno>`, :ref:`Spada Corta<equip_spada_corta>`, :ref:`Arco Corto<equip_arco_corto>` con 20 frecce, altro tesoro.

Descrizione
-----------------
I goblin preferiscono vivere nelle caverne, fra grandi e fitti cespugli di rovi, o in strutture abbandonate da altre creature. Pochi goblin sentono l'istinto di costruire. Preferiscono le cose, dato che amano molto frugare nei detriti portati dalla marea in cerca di tesori fra i rifiuti delle razze civilizzate. 

L'odio dei goblin è profondo, ma poco attira le loro ire come gli gnomi (che da sempre li combattono), i cavalli (che li spaventano terribilmente) e i cani (che vedono come pallide imitazioni dei cani goblin).

I goblin sono molto superstizioni e vedono la magia con un misto di adorazione e paura. Vedono magia anche nelle cose più comuni, soprattutto nel fuoco e nella scrittura. Il fuoco è molto amato dai goblin, per la sua capacità di portare grande distruzione e perché non richiede forza o altezza per essere usato, mentre la scrittura viene detestata. I goblin credono che scrivere rubi le parole dalla mente, e come risultato la maggior parte di loro è analfabeta.

I goblin sono voraci e possono mangiare una quantità di cibo pari al loro peso senza ingrassare. Le tane dei goblin hanno di solito molti magazzini e dispense. Anche se preferiscono mangiare la carne degli umani e degli gnomi, i goblin non rinunciano a mangiare nulla, tranne forse la verdura.

Personaggi Goblin
-------------------
I goblin sono definiti in base ai loro livelli di classe: non posseggono Dadi Vita razziali. Tutti i goblin hanno i seguenti tratti razziali.

:Caratteristiche: For -2, Des +4, Car –2

    I goblin sono veloci, ma deboli e sgradevoli.

:Taglia: Piccolo
:Veloce: I goblin sono veloci per la loro taglia e hanno una velocità base sul terreno di 9m.
:Scurovisione: I goblin possono vedere al buio fino a 18m.
:Esperto: Bonus razziale di +4 a :ref:`Cavalcare<cavalcare>` e +4 a :ref:`Furtività<furtività>`.
:Linguaggi: I goblin iniziano il gioco parlando il Goblin e possono scegliere tra Comune, Draconico, Gnoll, Gnomesco, Halfling, Nanico e Orchesco a seconda del loro Mod Int.

Descrizione GM (Mia)
-------------------------------
Il suono vi raggiunge prima della vista: un ridacchiare frenetico e sinistro, uno stridente chiaccericcio. Dall'ombra si intravedono i denti sottili e fitti in una piccola bocca aperta oltre misura. La testa larga incornicia degli occhi rossi che trasmettono una ferocia irrazionale, psicopatica. Le orecchie a punta decorate da numerosi gioielli d'ossa.

Non c'è bambino in Varisia che non abbia sentito delle persone arse vive nel sonno, delle lente torture fatte sugli infanti, il cui pianto non fa che eccitare queste creature così come le urla di chi si sveglia sentento la pelle staccarsi dalle ossa. 

Piccoli ma letali. Vi trovate di fronte a dei Goblin!

.. _bestiario_goblin_cantore_di_guerra:

--------------------------
Goblin Cantore di Guerra
--------------------------
.. figure:: Immagini/Bestiario/GoblinCantore.png
    :align: right
    :width: 400

.. rubric:: Base

:GS: 1/2
:Tipo: Umanoide
:Terreno: Foresta/Giungla
:Clima: Temperato
:Punti Esperienza: 200 
:Classe: Goblin Bardo 1
:Allineamento: NM
:Taglia: Piccolo (goblinoide)
:Iniziativa: +4
:Sensi: 

    *	:ref:`Scurovisione<scurovisione>` 18m
    * 	:ref:`percezione` +5

.. rubric:: Difesa

:CA: +3 armatura, +4 Des, +1 Taglia

    *	Base 18
    *	Contatto 15
    *	Impreparato 14
:pf: 9 (1d8+1)
:Tempra: +1
:Riflessi: +6 
:Volontà: +3 (+1 contro paura e charme)

.. rubric:: Attacco

:Velocità: 9m
:Mischia: Affettacani +1 (1d4/19-20) o Frusta +1 (1d2 non letale)
:Distanza:  Arco Corto +6 (1d4+1/x3)
:Attacchi Speciali:
    :ref:`esibizione_bardica` 5 round/giorno (:ref:`Affascinare<bardo_affascinare>`, :ref:`Controcanto<bardo_controcanto>`, :ref:`Distrazione<bardo_distrazione>`, :ref:`Ispirare Coraggio<bardo_ispirare_coraggio>` +1)
:Incantesimi Conosciuti:
    (LI 1; Concentrazione +2)

    |	0.	(A volontà) - :ref:`inc_frastornare` (CD 11), :ref:`inc_mano_magica`, :ref:`inc_messaggio`, :ref:`inc_suono_fantasma` (CD 11)
    |	1.	(2/giorno) - :ref:`inc_cura_ferite_leggere`, :ref:`inc_risata_incontenibile` (CD 12)

.. rubric:: Statistiche

:For: 8
:Des: 18
:Cos: 13
:Int: 8
:Sag: 12
:Car: 13
:Att Base: +0
:BMC: -2
:DMC: 12
:Talenti: :ref:`tal_competenza_nelle_armi_da_guerra` (affettacani)
:Abilità: 
    :ref:`acrobazia` +5, :ref:`cavalcare` +8, :ref:`furtività` +15, :ref:`intrattenere` (canto) +5, :ref:`linguistica` +3, :ref:`percezione` +5
:Linguaggi: Comune, Goblin
:QS: :ref:`Conoscenze Bardiche<conoscenze_bardiche>` +1
:Dotazioni da combattimento: Pozione di :ref:`inc_cura_ferite_leggere`
:Proprietà: Armatura di cuoio borchiato, Affettacani, Arco corto con 20 frecce, Frusta, 20 mo

Descrizione GM (Mia)
-------------------------------
Alle spalle dei suoi compagni più stupidi, un cantore canta a squarciagola una dannata canzoncina, riempiendo tutti i goblin della zona di un euforia combattiva e folle. Al canto si accompagna il movimento ipnotico della sua frusta. All'estremità si intravede qualcosa di grigio e appiccicoso: ciò che resta del cervello di uno sventurato che ha avuto la sfortuna di trovarsi alla portata dell'arma.

Il goblin vi guarda, e il suo canto si fa più forte.

.. _bestiario_goblin_incursore:

--------------------------
Goblin Incursore
--------------------------

.. rubric:: Base

:GS: 1/2
:Tipo: Umanoide
:Terreno: Foresta/Giungla
:Clima: Temperato
:Punti Esperienza: 200
:Classe: Goblin Ranger 1
:Allineamento: NM
:Taglia: Piccolo (goblinoide)
:Iniziativa: +3
:Sensi: 

    *	:ref:`Scurovisione<scurovisione>` 18m
    * 	:ref:`percezione` +5

.. rubric:: Difesa

:CA: +3 armatura, +3 Des, +1 Taglia

    *	Base 17
    *	Contatto 14
    *	Impreparato 14
:pf: 12 (1d10+2)
:Tempra: +4
:Riflessi: +5 
:Volontà: +2

.. rubric:: Attacco

:Velocità: 9m
:Mischia: Squartacavalli prf +4 (1d8+1)
:Distanza:  Arco Corto +5 (1d4/x3)
:Attacchi Speciali:
    :ref:`Nemico Prescelto<nemico_prescelto>` (animali +2)

.. rubric:: Statistiche

:For: 12
:Des: 17
:Cos: 15
:Int: 8
:Sag: 12
:Car: 8
:Att Base: +1
:BMC: +1
:DMC: 14
:Talenti: :ref:`Combattere in Sella<tal_combattere_in_sella>`
:Abilità: 
    :ref:`addestrare_animali` +3, :ref:`cavalcare` +9, :ref:`furtività` +13, :ref:`linguistica` +0, :ref:`percezione` +5, :ref:`sopravvivenza` +5
:Linguaggi: Comune, Goblin
:QS: :ref:`Empatia Selvatica<empatia_selvatica_ranger>` +0, :ref:`Seguire Tracce<seguire_tracce>` +1
:Dotazioni da combattimento: Pozione di :ref:`inc_Cura_Ferite_Moderate`
:Proprietà: Armatura di cuoio borchiato, Squartacavalli Perfetta, Arco corto con 20 frecce

Descrizione GM (Mia)
-------------------------------
Uno spietato condottiero. A differenza degli altri goblin preferisce tenersi lontano dalla mischia sfruttando le cavalcature, a cui tiene moltissimo. 

Con un ghigno soddisfatto lecca la sua lama, saggiando il sapore del sangue ancora caldo dell'ultimo cane trucidato. Ma la sua fame, non è mai soddisfatta.